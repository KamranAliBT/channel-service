/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2011.
 * All Rights Reserved.
 * $Id: DtoFactoryUtils.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.brighttalk.service.channel.presentation.dto.CategoryDto;
import com.brighttalk.service.channel.presentation.dto.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.ChannelPublicDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionContextDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.WebcastProviderDto;

/**
 * Set of utility methods for creating and populating presentation/DTO objects for use in tests.
 */
public final class DtoFactoryUtils {
  private static final long TEST_CHANNEL_ID = 9999;
  private static final long TEST_COMMUNICATION_ID = 123;
  private static final String CHANNEL_TITLE = "Channel Test Title";
  private static final String CHANNEL_ORGANISATION = "Channel Test Organisation";
  private static final String CHANNEL_STRAPLINE = "Channel Test Strapline";
  private static final String CHANNEL_DESCRIPTION = "Channel Test Description";
  private static final Long SUBSCRIPTION_ID = 123878L;
  private static final String LEAD_TYPE = "summit";
  private static final String LEAD_CONTEXT = "1111";
  private static final String ENGAGEMENT_SCORE = "88";
  private static final Long USER_ID = 378823l;

  private DtoFactoryUtils() {
  }

  /**
   * Creates a test Webcast DTO.
   * 
   * @return Test Webcast DTO.
   */
  public static WebcastDto createWebcastDTO() {
    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(TEST_COMMUNICATION_ID);
    webcastDto.setChannel(new ChannelPublicDto(TEST_CHANNEL_ID));
    webcastDto.setTitle("Communication Title");
    webcastDto.setDescription("Communication Description");
    webcastDto.setKeywords("one, two, three, four");
    webcastDto.setPresenter("Presenter");
    webcastDto.setDuration(100);
    webcastDto.setTimeZone("Europe/London");
    webcastDto.setVisibility("public");
    webcastDto.setProvider(new WebcastProviderDto("brighttalkhd"));
    webcastDto.setFormat("video");
    webcastDto.setAllowAnonymous("true");
    webcastDto.setExcludeFromChannelCapacity("true");
    webcastDto.setShowChannelSurvey("true");
    webcastDto.setClientBookingReference("Reference 225584");
    List<CategoryDto> categoryDtos = new ArrayList<CategoryDto>();
    categoryDtos.add(new CategoryDto(new Long(1)));
    categoryDtos.add(new CategoryDto(new Long(2)));
    webcastDto.setCategories(categoryDtos);
    return webcastDto;
  }

  /**
   * @return Test
   */
  public static ChannelDto createChannelDto() {
    ChannelDto channelDto = new ChannelDto(TEST_CHANNEL_ID);
    channelDto.setTitle(CHANNEL_TITLE);
    channelDto.setOrganisation(CHANNEL_ORGANISATION);
    channelDto.setKeywords("some, keywords");
    channelDto.setKeywordsList(Arrays.asList("some", "keywords"));
    channelDto.setStrapline(CHANNEL_STRAPLINE);
    channelDto.setDescription(CHANNEL_DESCRIPTION);

    return channelDto;
  }

  /**
   * Creates a fully populated instance of {@link SubscriptionDto}.
   * 
   * @return The created Subscription DTO.
   */
  public static SubscriptionDto createSubscriptionDto() {
    SubscriptionDto subscription = new SubscriptionDto();
    subscription.setId(SUBSCRIPTION_ID);
    subscription.setUser(createUserDto());
    subscription.setContext(createSubscriptionContextDto());
    subscription.setReferral("paid");
    return subscription;
  }

  /**
   * Creates a fully populated instance of {@link SubscriptionContextDto}.
   * 
   * @return The created subscription context DTO.
   */
  public static SubscriptionContextDto createSubscriptionContextDto() {
    SubscriptionContextDto context = new SubscriptionContextDto();
    context.setLeadType(LEAD_TYPE);
    context.setLeadContext(LEAD_CONTEXT);
    context.setEngagementScore(ENGAGEMENT_SCORE);
    return context;
  }

  /**
   * Creates an instance of {@link UserDto}.
   * 
   * @return An instance of {@link UserDto}
   */
  public static UserDto createUserDto() {
    UserDto user = new UserDto();
    user.setId(USER_ID);
    return user;
  }
}
