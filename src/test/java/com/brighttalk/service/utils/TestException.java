/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: TestException.java 84272 2014-10-07 08:22:56Z nbrown $
 * ****************************************************************************
 */
package com.brighttalk.service.utils;

/**
 * Class of exception that can be used generically report an exception which occurs in a test, including setup failures, 
 * and unexpected states / conditions. (Where possible, e.g. for reporting assertion failures, prefer the use of the 
 * exceptions provided by the test framework).
 */
@SuppressWarnings("serial")
public class TestException extends RuntimeException {
  
  /**
   * @param message The detail message.
   */
  public TestException(String message) {
    super(message);
  }
  
  /**
   * @param message The detail message.
   * @param cause The causal Throwable. 
   */  
  public TestException(String message, Throwable cause) {
    super(message, cause);
  }   
}