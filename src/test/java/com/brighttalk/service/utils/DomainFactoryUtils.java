/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: DomainFactoryUtils.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Category;
import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Channel.Searchable;
import com.brighttalk.service.channel.business.domain.channel.ChannelStatistics;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.presentation.dto.ContactDetailsPublicDto;

/**
 * Set of utility methods for creating and populating business/domain objects for use in tests.
 */
public final class DomainFactoryUtils {

  private static Long CHANNEL_ID = 5L;
  private static String CHANNEL_TITLE = "Channel Test Title";
  private static String CHANNEL_KEYWORDS = "Channel Test Keywords";
  private static String CHANNEL_ORGANISATION = "Channel Test Organisation";
  private static String CHANNEL_STRAPLINE = "Channel Test Strapline";
  private static String CHANNEL_DESCRIPTION = "Channel Test Description";
  private static String CHANNEL_URL = "http://channel.test.com";
  private static long CHANNEL_VIEWED_FOR = 1006L;
  private static Long CHANNEL_USER_ID = 16L;
  private static long SUBSCRIBERS = 10L;
  private static long UPCOMING_COMMUNICATIONS = 10L;
  private static long LIVE_COMMUNICATIONS = 12L;
  private static long RECORDED_COMMUNICATIONS = 10L;
  private static float RATING = 1.5F;
  private static final Long SUBSCRIPTION_ID = 123878L;
  private static final String LEAD_CONTEXT = "1111";
  private static final String ENGAGEMENT_SCORE = "88";

  private static final Long USER_ID = 378823l;
  private static final String FIRST_NAME = "firstNameTest";
  private static final String LAST_NAME = "lastNameTest";
  private static final String EMAIL = "email@test.net";
  private static final String TELEPHONE = "078333333";
  private static final String JOB_TITLE = "dev";
  private static final String COMPANY_NAME = "BrightTALK";
  private static final String ADDRESS1 = "address1";
  private static final String ADDRESS2 = "address2";
  private static final String CITY = "RandomCity";
  private static final String STATE_REGION = "Rainforest";
  private static final String POSTCODE = "EC1A 4NA";
  private static final String COUNTRY = "Neverland";

  private DomainFactoryUtils() {
  }

  /**
   * Creates Test {@link Channel} domain object.
   * 
   * @return {@link Channel} domain object.
   */
  public static Channel createChannel() {

    Channel channel = new Channel(CHANNEL_ID);
    channel.setTitle(CHANNEL_TITLE);
    channel.setOrganisation(CHANNEL_ORGANISATION);
    channel.setKeywords(Arrays.asList(CHANNEL_KEYWORDS));
    channel.setStrapline(CHANNEL_STRAPLINE);
    channel.setDescription(CHANNEL_DESCRIPTION);
    channel.setUrl(CHANNEL_URL);
    channel.setOwnerUserId(CHANNEL_USER_ID);
    channel.setSearchable(Searchable.INCLUDED);
    channel.setPromotedOnHomepage(true);

    ChannelStatistics statistics = new ChannelStatistics(CHANNEL_ID);
    statistics.setViewedFor(CHANNEL_VIEWED_FOR);
    statistics.setRating(RATING);
    statistics.setNumberOfSubscribers(SUBSCRIBERS);
    statistics.setNumberOfUpcomingCommunications(UPCOMING_COMMUNICATIONS);
    statistics.setNumberOfLiveCommunications(LIVE_COMMUNICATIONS);
    statistics.setNumberOfRecordedCommunications(RECORDED_COMMUNICATIONS);

    channel.setStatistics(statistics);
    channel.setSearchable(Searchable.INCLUDED);
    channel.setCreated(new Date());
    channel.setLastUpdated(new Date());
    channel.setType(new ChannelType(9999L));
    channel.setCreatedType(new ChannelType(9998L));
    channel.setFeatures(new ArrayList<Feature>());
    channel.setCategories(new ArrayList<Category>());

    return channel;
  }

  /**
   * Creates Test {@link Channel} domain object with no Optional Fields.
   * 
   * @return {@link Channel} domain object.
   */
  public static Channel createChannelWithNoOptionalFields() {

    Channel channel = new Channel(CHANNEL_ID);
    channel.setTitle(CHANNEL_TITLE);
    channel.setOrganisation(CHANNEL_ORGANISATION);
    channel.setKeywords(Arrays.asList(CHANNEL_KEYWORDS));
    channel.setStrapline(CHANNEL_STRAPLINE);
    channel.setDescription(CHANNEL_DESCRIPTION);
    channel.setOwnerUserId(CHANNEL_USER_ID);
    channel.setSearchable(Searchable.INCLUDED);
    channel.setPromotedOnHomepage(true);

    ChannelStatistics statistics = new ChannelStatistics(CHANNEL_ID);
    statistics.setViewedFor(CHANNEL_VIEWED_FOR);
    channel.setStatistics(statistics);

    channel.setCreated(new Date());
    channel.setLastUpdated(new Date());
    channel.setType(new ChannelType(9999L));
    channel.setCreatedType(new ChannelType(9998L));

    return channel;
  }

  /**
   * Creates Test PaginatedList of {@link Channel} domain objects.
   * 
   * @return {@link PaginatedList} of {@link Channel} domain objects.
   */
  public static PaginatedList<Channel> createChannels() {
    PaginatedList<Channel> channels = new PaginatedList<Channel>();
    channels.add(createChannel());
    channels.add(createChannel());

    return channels;
  }

  /**
   * Creates Test list of {@link Channel} domain objects.
   * 
   * @return list of {@link Channel} domain objects.
   */
  public static List<Communication> createFeaturedCommunications() {
    Communication communication = new Communication();
    communication.setId(666l);
    communication.setStatus(Status.LIVE);
    communication.setTitle("communicationTitle");
    communication.setScheduledDateTime(new Date());
    communication.setDuration(200);
    communication.setThumbnailUrl("http://thumnail.com");

    List<Communication> featuredCommunications = new ArrayList<Communication>();
    featuredCommunications.add(communication);

    return featuredCommunications;
  }

  /**
   * Creates a instance of {@link ContactDetails}.
   * 
   * @return An instance of {@link ContactDetails}.
   */
  public static ContactDetails createContactDetails() {

    ContactDetails contactDetails = new ContactDetails();
    contactDetails.setFirstName(FIRST_NAME);
    contactDetails.setLastName(LAST_NAME);
    contactDetails.setEmail(EMAIL);
    contactDetails.setTelephone(TELEPHONE);
    contactDetails.setJobTitle(JOB_TITLE);
    contactDetails.setCompany(COMPANY_NAME);
    contactDetails.setAddress1(ADDRESS1);
    contactDetails.setAddress2(ADDRESS2);
    contactDetails.setCity(CITY);
    contactDetails.setState(STATE_REGION);
    contactDetails.setPostcode(POSTCODE);
    contactDetails.setCountry(COUNTRY);

    return contactDetails;
  }

  /**
   * Creates a instance of {@link ContactDetailsPublicDto}.
   * 
   * @return An instance of {@link ContactDetailsPublicDto}.
   */
  public static ContactDetailsPublicDto createContactDetailsPublicDto() {

    ContactDetailsPublicDto contactDetailsPublicDto = new ContactDetailsPublicDto();
    contactDetailsPublicDto.setFirstName(FIRST_NAME);
    contactDetailsPublicDto.setLastName(LAST_NAME);
    contactDetailsPublicDto.setEmail(EMAIL);
    contactDetailsPublicDto.setTelephone(TELEPHONE);
    contactDetailsPublicDto.setJobTitle(JOB_TITLE);
    contactDetailsPublicDto.setCompanyName(COMPANY_NAME);
    contactDetailsPublicDto.setAddress1(ADDRESS1);
    contactDetailsPublicDto.setAddress2(ADDRESS2);
    contactDetailsPublicDto.setCity(CITY);
    contactDetailsPublicDto.setStateRegion(STATE_REGION);
    contactDetailsPublicDto.setPostcode(POSTCODE);
    contactDetailsPublicDto.setCountry(COUNTRY);

    return contactDetailsPublicDto;
  }

  /**
   * Creates a fully populated instance of {@link SubscriptionContext}.
   * 
   * @return The created subscription context.
   */
  public static SubscriptionContext createSubscriptionContext() {
    SubscriptionContext context = new SubscriptionContext();
    context.setSubscriptionId(SUBSCRIPTION_ID);
    context.setLeadType(SubscriptionLeadType.SUMMIT);
    context.setLeadContext(LEAD_CONTEXT);
    context.setEngagementScore(ENGAGEMENT_SCORE);
    return context;
  }

  /**
   * Creates an instance of {@link Subscription}.
   * 
   * @return The created subscription.
   */
  public static Subscription createSubscription() {
    Subscription subscription = new Subscription();
    subscription.setId(SUBSCRIPTION_ID);
    subscription.setUser(createUser());
    subscription.setContext(createSubscriptionContext());
    return subscription;
  }

  /**
   * Creates an instance of {@link User}.
   * 
   * @return An instance of {@link User}
   */
  public static User createUser() {
    User user = new User(USER_ID);
    user.setEmail(EMAIL);
    user.setFirstName(FIRST_NAME);
    user.setLastName(LAST_NAME);
    // TODO - add more user field as needed.
    return user;
  }

}
