/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EasyMockFactoryBean.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.utils;

import org.easymock.EasyMock;
import org.springframework.beans.factory.FactoryBean;

/**
 * Implementation of a Spring Factory Bean to create EasyMock instances of a given class.
 * 
 * @see {http://stackoverflow.com/questions/6340007/autowiring-of-beans-generated-by-easymock-factory-method}
 * @param <T> 
 */
public class EasyMockFactoryBean<T> implements FactoryBean<T> {

  private Class<T> mockedClass;

  @SuppressWarnings({ "rawtypes", "unchecked" })
  public void setMockedClass(Class mockedClass) {
    this.mockedClass = mockedClass;
  }

  public T getObject() throws Exception {
    return EasyMock.createMock(mockedClass);
  }

  public Class<T> getObjectType() {
    return mockedClass;
  }

  public boolean isSingleton() {
    return true;
  }
}
