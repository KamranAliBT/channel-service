/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: UserErrorMessageMatcher.java 62868 2013-04-09 09:13:50Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.utils;

import org.hamcrest.Description;
import org.junit.internal.matchers.TypeSafeMatcher;

import com.brighttalk.common.error.ApplicationException;

public class UserErrorMessageMatcher extends TypeSafeMatcher<ApplicationException> {

  public static UserErrorMessageMatcher hasMessage(final String message) {
    return new UserErrorMessageMatcher(message);
  }

  private String foundErrorMessage;
  private final String expectedErrorMessage;

  private UserErrorMessageMatcher(final String expectedErrorMessage) {
    this.expectedErrorMessage = expectedErrorMessage;
  }

  @Override
  public void describeTo(final Description description) {
    description.appendValue(foundErrorMessage).appendText(" was found instead of ").appendValue(expectedErrorMessage);
  }

  @Override
  public boolean matchesSafely(final ApplicationException exception) {

    foundErrorMessage = exception.getUserErrorMessage();
    return expectedErrorMessage.equalsIgnoreCase(foundErrorMessage);
  }
}