/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsCreateValidatorTest.java 91908 2015-03-18 16:46:30Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.error.SubscriptionRequestException;
import com.brighttalk.service.channel.presentation.dto.SubscriptionContextDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.utils.DtoFactoryUtils;

public class SubscriptionsCreateValidatorTest extends AbstractResourceValidatorTest {

  private static final Long USER_ID = 33L;
  private static final String EMAIL = "email@brighttalk.com";
  private static final String LEAD_TYPE = SubscriptionLeadType.SUMMIT.getType();
  private static final String LEAD_CONTEXT = "1111";
  private static final String ENGAGEMENT_SCORE = "88";
  private static final String EXPECT_EXCEPTION_MSG = "Expected an exception to be thrown.";

  private SubscriptionsCreateValidator uut;

  @Before
  public void setUp() {
    uut = new SubscriptionsCreateValidator();

  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having subscriptions list exceeding the
   * allowed subscription max number.
   */
  @Test
  public void validateWhileMaxSubscriptionsExceeded() {

    // Set up
    List<SubscriptionDto> subscriptionDtos = new ArrayList<>();
    for (int i = 0; i <= 100; i++) {
      subscriptionDtos.add(new SubscriptionDto());
    }

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptionDtos);

    // Expectations

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.SUBSCRIPTION_MAX_LIMIT, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having no subscriptions list.
   */
  @Test
  public void validateWithNoSubscriptions() {

    // Set up
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();

    // Expectations

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.MISSING_SUBSCRIPTION, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having empty subscriptions list.
   */
  @Test
  public void validateWithEmptySubscriptions() {

    // Set up
    List<SubscriptionDto> subscriptions = Collections.emptyList();

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Expectations

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.MISSING_SUBSCRIPTION, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having one subscription with missing user
   * element or user Id or email.
   */
  @Test
  public void validateWhileSubscriptionMissingUserIdAndUserEmail() {

    // Set up
    SubscriptionDto subscription = new SubscriptionDto();

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Expectations

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.MISSING_USER, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having one subscription with user element
   * containing empty email.
   */
  @Test
  public void validateWhileSubscriptionUserEmailEmpty() {

    // Set up
    String email = "";

    UserDto user = new UserDto();
    user.setEmail(email);

    SubscriptionDto subscription = new SubscriptionDto();
    subscription.setUser(user);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Expectations

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.MISSING_USER, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having several subscriptions with
   * duplicate user elements.
   */
  @Test
  public void validateWhileSubscriptionsHaveDuplicateUsers() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.DUPLICATE_RESOURCE, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having one subscription with missing
   * referral element.
   */
  @Test
  public void validateWhileSubscriptionReferralMissing() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setReferral(null);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.MISSING_REFERRAL, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having one subscription with referral
   * element containing invalid value.
   */
  @Test
  public void validateWhileSubscriptionReferralInvalid() {
    // Set up
    String referral = "invalid";
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setReferral(referral);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.INVALID_REFERRAL, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having one subscription with user element
   * having both user Id and email.
   */
  @Test
  public void validateWhileSubscriptionUserIdAndEmailProvided() {

    // Set up
    UserDto user = new UserDto();
    user.setId(USER_ID);
    user.setEmail(EMAIL);

    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setUser(user);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.SUBSCRIPTION_USER_ID_EMAIL_COMBINATION, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of having two subscriptions with one having
   * user Id and other user email.
   * <p>
   * The supplied subscriptions user element are only allowed to have either user Id or user email address.
   */
  @Test
  public void validateWhileMulitpleSubscriptionHaveUserIdAndUserEmail() {

    // Set up
    UserDto user1 = new UserDto();
    user1.setId(USER_ID);

    UserDto user2 = new UserDto();
    user2.setEmail(EMAIL);

    SubscriptionDto subscription1 = DtoFactoryUtils.createSubscriptionDto();
    subscription1.setUser(user1);

    SubscriptionDto subscription2 = DtoFactoryUtils.createSubscriptionDto();
    subscription2.setUser(user2);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription1);
    subscriptions.add(subscription2);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      // Assertions
      assertEquals(ErrorCode.SUBSCRIPTION_USER_ID_EMAIL_COMBINATION, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the success case of providing subscription with user element
   * containing valid email address.
   */
  @Test
  public void validateWhileSubscriptionUserEmailProvided() {

    // Set up
    UserDto user = new UserDto();
    user.setEmail(EMAIL);

    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setUser(user);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Expectations

    // Do test
    uut.validate(subscriptionsDto);
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the success case of providing subscription with user element
   * containing valid user Id.
   */
  @Test
  public void validateWhileSubscriptionUserIdProvided() {

    // Set up
    UserDto user = new UserDto();
    user.setId(USER_ID);

    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setUser(user);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Expectations

    // Do test
    uut.validate(subscriptionsDto);
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the success case of providing valid subscription context.
   */
  @Test
  public void validateSubscriptionWithValidContext() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    uut.validate(subscriptionsDto);
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of missing subscription context.
   */
  @Test
  public void validateSubscriptionWithMissingContext() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setContext(null);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);
    // Setting Subscription context is required.
    uut.setContextRequired(true);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.MISSING_CONTEXT, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of providing invalid subscription context
   * with missing lead type.
   */
  @Test
  public void validateSubscriptionWithInvalidContextMissingLeadType() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    SubscriptionContextDto testContext = new SubscriptionContextDto();
    testContext.setLeadContext(LEAD_CONTEXT);
    testContext.setEngagementScore(ENGAGEMENT_SCORE);
    subscription.setContext(testContext);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.MISSING_LEAD_TYPE, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of providing invalid subscription context
   * with invalid lead type.
   */
  @Test
  public void validateSubscriptionWithInvalidContextInvalidLeadType() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    SubscriptionContextDto testContext = new SubscriptionContextDto();
    testContext.setLeadType("test");
    testContext.setLeadContext(LEAD_CONTEXT);
    testContext.setEngagementScore(ENGAGEMENT_SCORE);
    subscription.setContext(testContext);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.INVALID_LEAD_TYPE, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of providing invalid subscription context
   * with missing lead context.
   */
  @Test
  public void validateSubscriptionWithInvalidContextMissingLeadContext() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    SubscriptionContextDto testContext = new SubscriptionContextDto();
    testContext.setLeadType(LEAD_TYPE);
    testContext.setEngagementScore(ENGAGEMENT_SCORE);
    subscription.setContext(testContext);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.MISSING_LEAD_CONTEXT, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of providing invalid subscription context
   * with invalid lead context. - Lead Context must a valid integer when the context lead type is either "summit" or
   * "content".
   */
  @Test
  public void validateSubscriptionWithInvalidContextInvalidLeadContext() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    SubscriptionContextDto testContext = new SubscriptionContextDto();
    testContext.setLeadType(LEAD_TYPE);
    testContext.setLeadContext("test");
    testContext.setEngagementScore(ENGAGEMENT_SCORE);
    subscription.setContext(testContext);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.INVALID_LEAD_CONTEXT, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of providing subscription of lead type
   * "summit" and missing Engagement Score - Engagement Score is mandatory for lead type "summit" and "keyword".
   */
  @Test
  public void validateSubscriptionWithInvalidContextMissingEngagementScore() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    SubscriptionContextDto testContext = new SubscriptionContextDto();
    testContext.setLeadType(LEAD_TYPE);
    testContext.setLeadContext(LEAD_CONTEXT);
    subscription.setContext(testContext);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.MISSING_ENGAGEMENT_SCORE, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the success case of providing valid subscription of lead
   * type "content" and missing Engagement Score.
   */
  @Test
  public void validateSubscriptionWithValidContextMissingEngagementScore() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    SubscriptionContextDto testContext = new SubscriptionContextDto();
    testContext.setLeadType(SubscriptionLeadType.CONTENT.getType());
    testContext.setLeadContext(LEAD_CONTEXT);
    subscription.setContext(testContext);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    uut.validate(subscriptionsDto);

  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the success case of providing invalid subscription context
   * Engagement Score - value that is not an integer.
   */
  @Test
  public void validateSubscriptionWithValidContextInvalidEngagementScore() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    SubscriptionContextDto testContext = new SubscriptionContextDto();
    testContext.setLeadType(SubscriptionLeadType.CONTENT.getType());
    testContext.setLeadContext(LEAD_CONTEXT);
    testContext.setEngagementScore("test");
    subscription.setContext(testContext);

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.INVALID_ENGAGEMENT_SCORE, ex.getErrorCode());
    }

  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the success case of validating subscription with missing
   * context and setting the subscription context required flag to false.
   * <p>
   * Expected result: The subscription are validated without errors even when the context is not provided.
   */
  @Test
  public void validateSubscriptionWithContextRequiredFlagSetToFalse() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setContext(null);
    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);
    uut.setContextRequired(false);

    // Do test
    uut.validate(subscriptionsDto);
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the error case of validating subscription with missing
   * context and setting the subscription context required flag to true.
   * <p>
   * Expected result: The subscription are validated with missing context error.
   */
  @Test
  public void validateSubscriptionWithContextRequiredFlagSetToTrue() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setContext(null);
    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);
    uut.setContextRequired(true);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.MISSING_CONTEXT, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsCreateValidator#validate} in the success case of validating subscription with missing
   * context contain (e.g. missing lead type) and setting the subscription context required flag to false.
   * <p>
   * Expected result: The subscription are validated with missing leadType error as the context is provided and the lead
   * type is missing.
   */
  @Test
  public void validateSubscriptionWithContextRequiredFlagSetToFalseAndMissingContextContain() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.getContext().setLeadType(null);
    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);
    uut.setContextRequired(false);

    // Do test
    try {
      uut.validate(subscriptionsDto);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.MISSING_LEAD_TYPE, ex.getErrorCode());
    }
  }

}
