/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: WebcastSyndicationsControllerTest.java 99279 2015-08-18 12:58:41Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.junit.Assert.assertNotNull;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndications;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationSyndicationService;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationsDto;
import com.brighttalk.service.channel.presentation.dto.syndication.converter.WebcastSyndicationConverter;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.InvalidParamException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

/**
 * Unit Test for {@link WebcastSyndicationsController}.
 */
public class WebcastSyndicationsControllerTest {

  private static final String CHANNEL_ID = "12345";
  private static final String COMMUNICATION_ID = "7657";
  private static final Long USER_ID = 2345l;
  private WebcastSyndicationsController uut;

  private ChannelFinder mockChannelFinder;
  private CommunicationFinder mockCommunicationFinder;
  private CommunicationSyndicationService mockCommSyndicationService;
  private WebcastSyndicationConverter mockCommSyndicationsConverter;
  private DateTimeFormatter mockFormatter;

  /**
   * Test Setup.
   */
  @Before
  public void setUp() {
    uut = new WebcastSyndicationsController();

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);
    mockCommunicationFinder = EasyMock.createMock(CommunicationFinder.class);
    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);
    mockCommSyndicationService = EasyMock.createMock(CommunicationSyndicationService.class);
    ReflectionTestUtils.setField(uut, "commSyndicationService", mockCommSyndicationService);
    mockCommSyndicationsConverter = EasyMock.createMock(WebcastSyndicationConverter.class);
    ReflectionTestUtils.setField(uut, "syndicationsConverter", mockCommSyndicationsConverter);
    mockFormatter = EasyMock.createMock(DateTimeFormatter.class);
  }

  /**
   * Tests {@link WebcastSyndicationsController#get()} in the error case of supplying invalid channel Id format (none
   * integer).
   */
  @Test(expected = InvalidParamException.class)
  public void getSyndicationsWithInvalidChannelIdNoneInteger() {
    // Do Test.
    this.uut.get(new User(USER_ID), "test", COMMUNICATION_ID, null);
  }

  /**
   * Tests {@link WebcastSyndicationsController#get()} in the error case of supplying invalid channel Id format
   * (negative number ).
   */
  @Test(expected = InvalidParamException.class)
  public void getSyndicationsWithInvalidChannelIdNegativeInteger() {
    // Do Test.
    this.uut.get(new User(USER_ID), "-1", COMMUNICATION_ID, null);
  }

  /**
   * Tests {@link WebcastSyndicationsController#get()} in the error case of supplying invalid communication Id format
   * (none integer).
   */
  @Test(expected = InvalidParamException.class)
  public void getSyndicationsWithInvalidCommunicationIdNoneInteger() {
    // Do Test.
    this.uut.get(new User(USER_ID), CHANNEL_ID, "test", null);
  }

  /**
   * Tests {@link WebcastSyndicationsController#get()} in the error case of supplying invalid communication Id format
   * (negative number ).
   */
  @Test(expected = InvalidParamException.class)
  public void getSyndicationsWithInvalidCommunicationIdNegativeInteger() {
    // Do Test.
    this.uut.get(new User(USER_ID), CHANNEL_ID, "-1", null);
  }

  /**
   * Tests {@link WebcastSyndicationsController#get} in the error case of retrieving communication syndication and the
   * supplied communication does not identify an existing communication.
   */
  @Test(expected = ResourceNotFoundException.class)
  public void getSyndicationWhenCommunicationNotFound() {
    // Expectation
    EasyMock.expect(mockChannelFinder.find(EasyMock.isA(User.class), EasyMock.isA(Long.class))).andReturn(new Channel());
    EasyMock.expect(mockCommunicationFinder.find(EasyMock.isA(Channel.class), EasyMock.isA(Long.class))).andThrow(
        new ResourceNotFoundException("", ErrorCode.RESOURCE_NOT_FOUND, new Object[] {}));
    EasyMock.replay(mockChannelFinder, mockCommunicationFinder);

    // Do Test.
    this.uut.get(new User(USER_ID), CHANNEL_ID, COMMUNICATION_ID, null);
  }

  /**
   * Tests {@link WebcastSyndicationsController#get} in the error case of retrieving communication syndication and the
   * supplied channel does not identify an existing channel.
   */
  @Test(expected = ResourceNotFoundException.class)
  public void getSyndicationWhenChannelNotFound() {
    // Expectation
    EasyMock.expect(mockChannelFinder.find(EasyMock.isA(User.class), EasyMock.isA(Long.class))).andThrow(
        new ResourceNotFoundException("", ErrorCode.RESOURCE_NOT_FOUND, new Object[] {}));
    EasyMock.replay(mockChannelFinder);

    // Do Test.
    this.uut.get(new User(USER_ID), CHANNEL_ID, COMMUNICATION_ID, null);
  }

  /**
   * Tests {@link WebcastSyndicationsController#get} in the error case of retrieving communication syndication and the
   * supplied user not authorised for the supplied channel.
   */
  @Test(expected = UserAuthorisationException.class)
  public void getSyndicationWithUserNotAuthorised() {
    // Expectation
    EasyMock.expect(mockChannelFinder.find(EasyMock.isA(User.class), EasyMock.isA(Long.class))).andThrow(
        new UserAuthorisationException(""));
    EasyMock.replay(mockChannelFinder);

    // Do Test.
    this.uut.get(new User(USER_ID), CHANNEL_ID, COMMUNICATION_ID, null);
  }

  /**
   * Tests {@link WebcastSyndicationsController#get} in the success case of retrieving communication syndication.
   */
  @Test
  public void getSyndicationsSuccess() {
    // Expectation
    EasyMock.expect(mockChannelFinder.find(EasyMock.isA(User.class), EasyMock.isA(Long.class))).andReturn(new Channel());
    EasyMock.expect(mockCommunicationFinder.find(EasyMock.isA(Channel.class), EasyMock.isA(Long.class))).andReturn(
        new Communication());
    CommunicationSyndications syndications = new CommunicationSyndications();
    EasyMock.expect(mockCommSyndicationService.get(EasyMock.isA(Channel.class),
        EasyMock.isA(Communication.class))).andReturn(syndications);
    EasyMock.expect(mockCommSyndicationsConverter.convert(syndications, mockFormatter)).andReturn(
        new WebcastSyndicationsDto());
    EasyMock.replay(mockChannelFinder, mockCommunicationFinder, mockCommSyndicationService,
        mockCommSyndicationsConverter);

    // Do Test -
    WebcastSyndicationsDto communicationSyndicationsDto = uut.get(new User(USER_ID), CHANNEL_ID,
        COMMUNICATION_ID, mockFormatter);
    EasyMock.verify(mockChannelFinder, mockCommunicationFinder, mockCommSyndicationService,
        mockCommSyndicationsConverter);

    assertNotNull(communicationSyndicationsDto);
  }
}
