/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsConverterTest.java 91908 2015-03-18 16:46:30Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.presentation.dto.SubscriptionContextDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;
import com.brighttalk.service.utils.DtoFactoryUtils;

public class SubscriptionsConverterTest {

  private static final Long USER_ID = 33L;
  private static final Long SUBSCRIPTION_ID = 999L;
  private static final String EMAIL = "a@b.com";
  private static final String REFERRAL = "paid";
  private static final String LEAD_TYPE = "summit";
  private static final String LEAD_CONTEXT = "1111";
  private static final String ENGAGEMENT_SCORE = "88";

  private SubscriptionsConverter uut;

  @Before
  public void setUp() {
    uut = new SubscriptionsConverter();
  }

  @Test
  public void convertWhileNoSubscriptions() {

    // Set up
    List<SubscriptionDto> subscriptionDtos = new ArrayList<>();

    // Do test
    List<Subscription> result = uut.convert(subscriptionDtos);

    // Assertions
    assertNotNull(result);
    assertTrue(result.isEmpty());
  }

  @Test
  public void convert() {

    // Set up

    UserDto user = new UserDto();
    user.setId(USER_ID);
    user.setEmail(EMAIL);

    SubscriptionDto subscription = new SubscriptionDto();
    subscription.setId(SUBSCRIPTION_ID);
    subscription.setUser(user);
    subscription.setReferral(REFERRAL);
    subscription.setContext(new SubscriptionContextDto(LEAD_TYPE, LEAD_CONTEXT, ENGAGEMENT_SCORE));

    List<SubscriptionDto> subscriptions = new ArrayList<>();
    subscriptions.add(subscription);

    // Expectations
    Integer expectedSize = 1;

    // Do test
    List<Subscription> result = uut.convert(subscriptions);

    // Assertions
    assertNotNull(result);
    assertFalse(result.isEmpty());
    assertTrue(expectedSize.equals(result.size()));

    Subscription expectedSubscription = result.iterator().next();
    assertEquals(SUBSCRIPTION_ID, expectedSubscription.getId());
    assertEquals(USER_ID, expectedSubscription.getUser().getId());
    assertEquals(EMAIL, expectedSubscription.getUser().getEmail());
    assertEquals(REFERRAL, expectedSubscription.getReferral().name());
    assertNull(expectedSubscription.getLastSubscribed());
    SubscriptionContext context = expectedSubscription.getContext();
    assertNotNull(context);
    assertEquals(LEAD_TYPE, context.getLeadType().name().toLowerCase());
    assertEquals(LEAD_CONTEXT, context.getLeadContext());
    assertEquals(ENGAGEMENT_SCORE, context.getEngagementScore());
  }

  /**
   * Tests {@link SubscriptionsConverter#convert(List)} in success case of converting subscription DTO with subscription
   * context's lead type set to "webinar".
   * <p>
   * Expected Result: The subscription is converted with "content" lead type.
   */
  @Test
  public void convertWithWebinarLeadType() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setContext(new SubscriptionContextDto("webinar", LEAD_CONTEXT, ENGAGEMENT_SCORE));
    List<SubscriptionDto> subscriptions = Arrays.asList(subscription);

    // Do test
    List<Subscription> result = uut.convert(subscriptions);

    // Assertions:
    Subscription expectedSubscription = result.iterator().next();
    SubscriptionContext context = expectedSubscription.getContext();
    assertNotNull(context);
    assertEquals(SubscriptionLeadType.CONTENT, context.getLeadType());
  }

  /**
   * Tests {@link SubscriptionsConverter#convert(List)} in success case of converting subscription DTO with subscription
   * context's lead type set to "content".
   * <p>
   * Expected Result: The subscription is converted with "content" lead type.
   */
  @Test
  public void convertWithContentLeadType() {
    // Set up
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setContext(new SubscriptionContextDto("content", LEAD_CONTEXT, ENGAGEMENT_SCORE));
    List<SubscriptionDto> subscriptions = Arrays.asList(subscription);

    // Do test
    List<Subscription> result = uut.convert(subscriptions);

    // Assertions:
    Subscription expectedSubscription = result.iterator().next();
    SubscriptionContext context = expectedSubscription.getContext();
    assertNotNull(context);
    assertEquals(SubscriptionLeadType.CONTENT, context.getLeadType());
  }

}
