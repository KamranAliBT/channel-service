/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelConverterTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.time.Iso8601DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.Category;
import com.brighttalk.service.channel.business.domain.DefaultKeywords;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.PresentationCriteria;
import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Channel.Searchable;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.presentation.dto.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.ChannelPublicDto;
import com.brighttalk.service.channel.presentation.dto.ChannelsPublicDto;
import com.brighttalk.service.channel.presentation.dto.FeatureDto;
import com.brighttalk.service.channel.presentation.dto.FeaturedWebcastDto;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.error.UnsupportedFeatureException;
import com.brighttalk.service.utils.DomainFactoryUtils;
import com.brighttalk.service.utils.DtoFactoryUtils;

/**
 * Unit Tests for {@link ChannelConverter}.
 */
public class ChannelConverterTest {

  private static final Long CHANNEL_ID = 5L;

  private static final String CHANNEL_TITLE = "Channel Test Title";

  private static final String CHANNEL_ORGANISATION = "Channel Test Organisation";

  private static final String CHANNEL_STRAPLINE = "Channel Test Strapline";

  private static final String CHANNEL_DESCRIPTION = "Channel Test Description";

  private static final String CHANNEL_URL = "http://channel.test.com";

  private static final long CHANNEL_VIEWED_FOR = 1006L;

  private static final Long CHANNEL_USER_ID = 16L;

  private static final String FEATURE_NAME = "allowedRealms";

  private static final String FEATURE_VALUE = "Feature value";

  private static final String CATEGORY_NAME = "Category name";

  private static final long CATEGORY_ID = 999L;

  private static final long SURVEY_ID = 9991L;

  private static final long SUBSCRIBERS = 10L;

  private static final long UPCOMING_COMMUNICATIONS = 10L;

  private static final long LIVE_COMMUNICATIONS = 12L;

  private static final long RECORDED_COMMUNICATIONS = 10L;

  private static final float RATING = 1.5F;

  private static final String CHANNEL_KEYWORDS = "Channel Test Keywords";

  private static final String KEYWORDS_SEPARATOR = ",";

  private ChannelConverter uut;

  private DateTimeFormatter mockFormatter;

  private ConverterContext mockConverterContext;

  private FeatureConverter mockFeatureConverter;

  private Set<Feature.Type> mockPublicFeatures;

  private PresentationCriteria mockPresentationCriteria;

  @Before
  @SuppressWarnings("unchecked")
  public void setUp() throws Exception {
    uut = new ChannelConverter();

    mockConverterContext = createMock(ConverterContext.class);
    mockFormatter = createMock(DateTimeFormatter.class);
    mockFeatureConverter = createMock(FeatureConverter.class);
    mockPublicFeatures = createMock(Set.class);
    mockPresentationCriteria = createMock(PresentationCriteria.class);

    ReflectionTestUtils.setField(uut, "keywords", new DefaultKeywords());
    ReflectionTestUtils.setField(uut, "featureConverter", mockFeatureConverter);
    ReflectionTestUtils.setField(uut, "publicFeatures", mockPublicFeatures);
    ReflectionTestUtils.setField(mockConverterContext, "dateTimeFormatter", mockFormatter);
  }

  @Test
  public void convertId() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(CHANNEL_ID, result.getId());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertTitle() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(CHANNEL_TITLE, result.getTitle());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertOrganisation() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(CHANNEL_ORGANISATION, result.getOrganisation());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertStrapline() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(CHANNEL_STRAPLINE, result.getStrapline());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertDescription() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(CHANNEL_DESCRIPTION, result.getDescription());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertUrl() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(CHANNEL_URL, result.getUrl());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertSearchVisibility() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(Searchable.INCLUDED.name().toLowerCase(), result.getSearchVisibility());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertPromotable() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(Boolean.TRUE, result.getPromoteable());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertCreatedUnixDate() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();
    long epochDate = 1000000l;
    Date date = new Date(epochDate);
    channel.setCreated(date);

    // expectations
    String expectedDate = String.valueOf(epochDate / 1000l);
    expect(mockFormatter.convert(isA(Date.class))).andReturn(expectedDate).times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(expectedDate, result.getCreated());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertCreatedIsoDate() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();
    long epochDate = 1000000l;
    Date date = new Date(epochDate);
    channel.setCreated(date);
    mockFormatter = createMock(Iso8601DateTimeFormatter.class);

    // expectations
    String expectedDate = "1970-01-01T00:16:40Z";
    expect(mockFormatter.convert(isA(Date.class))).andReturn(expectedDate).times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(expectedDate, result.getCreated());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertLastUpdatedUnixDate() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();
    long epochDate = 1000000l;
    Date date = new Date(epochDate);
    channel.setLastUpdated(date);

    // expectations
    String expectedDate = String.valueOf(epochDate / 1000l);
    expect(mockFormatter.convert(isA(Date.class))).andReturn(expectedDate).times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(expectedDate, result.getLastUpdated());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertLastUpdatedIsoDate() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();
    long epochDate = 1000000l;
    Date date = new Date(epochDate);
    channel.setLastUpdated(date);
    mockFormatter = createMock(Iso8601DateTimeFormatter.class);

    // expectations
    String expectedDate = "1970-01-01T00:16:40Z";
    expect(mockFormatter.convert(isA(Date.class))).andReturn(expectedDate).times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(expectedDate, result.getLastUpdated());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertChannelStatisticsFields() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(CHANNEL_VIEWED_FOR, result.getStatistics().getViewedSeconds());
    assertEquals(SUBSCRIBERS, result.getStatistics().getSubscribers());
    assertEquals(UPCOMING_COMMUNICATIONS, result.getStatistics().getUpcomingCommunications());
    assertEquals(RECORDED_COMMUNICATIONS, result.getStatistics().getRecordedCommunications());
    assertEquals(RATING, result.getRating(), 0);
    EasyMock.verify(mockFormatter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertFeaturesForChannelWithEmptyFeatures() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(new ArrayList<FeatureDto>()).times(1);
    EasyMock.replay(mockFormatter, mockFeatureConverter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(0, result.getFeatures().size());
    EasyMock.verify(mockFormatter, mockFeatureConverter);
  }

  @Test
  public void convertFeaturesForChannelWithNoFeatures() {
    // set up
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertNull(result.getFeatures());
    EasyMock.verify(mockFormatter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void cvonvertFeaturesForChannelWithOneFeatures() throws UnsupportedFeatureException {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    FeatureDto featureDto = new FeatureDto();
    featureDto.setName(FEATURE_NAME);
    featureDto.setValue(FEATURE_VALUE);
    featureDto.setEnabled(true);

    List<FeatureDto> featureDtos = new ArrayList<>();
    featureDtos.add(featureDto);

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(featureDtos).times(1);
    EasyMock.replay(mockFormatter, mockFeatureConverter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(1, result.getFeatures().size());
    assertEquals(FEATURE_NAME, result.getFeatures().get(0).getName());
    assertEquals(FEATURE_VALUE, result.getFeatures().get(0).getValue());
    assertEquals(Boolean.TRUE, result.getFeatures().get(0).getEnabled());
    EasyMock.verify(mockFormatter, mockFeatureConverter);
  }

  @Test
  public void convertCategoriesForChannelWithEmptyCategory() {
    // set up
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertNull(result.getCategories());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertCategoriesForChannelWithNoCategory() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(0, result.getCategories().size());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertCategoriesForChannelWithOneCategory() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();
    Category category = new Category(CATEGORY_ID, CHANNEL_ID, CATEGORY_NAME);
    channel.getCategories().add(category);

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertEquals(1, result.getCategories().size());
    assertEquals(CATEGORY_ID, (long) result.getCategories().get(0).getId());
    assertEquals(CATEGORY_NAME, result.getCategories().get(0).getTerm());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertSurveyForChannelWithNoSurvey() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertNull(result.getSurvey());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertSurveyForChannelWithActiveSurvey() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();
    Survey survey = new Survey(SURVEY_ID, CHANNEL_ID, true);
    channel.setSurvey(survey);

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertNotNull(result.getSurvey());
    assertEquals(SURVEY_ID, (long) result.getSurvey().getId());
    assertTrue(result.getSurvey().getIsActive());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertSurveyForChannelWithNotActiveSurvey() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();
    Survey survey = new Survey(SURVEY_ID, CHANNEL_ID, false);
    channel.setSurvey(survey);

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertNotNull(result.getSurvey());
    assertEquals(SURVEY_ID, (long) result.getSurvey().getId());
    assertFalse(result.getSurvey().getIsActive());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertChannelOwner() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto result = uut.convert(channel, mockFormatter);

    // assertions
    assertNotNull(result.getUser());
    assertEquals(CHANNEL_USER_ID, result.getUser().getId());
    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertIdForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.FALSE).once();
    EasyMock.replay(mockPresentationCriteria);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.getId());
    EasyMock.verify(mockPresentationCriteria);
  }

  @Test
  public void convertTitleForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(CHANNEL_TITLE, result.getTitle());
  }

  @Test
  public void convertDescriptionForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(CHANNEL_DESCRIPTION, result.getDescription());
  }

  @Test
  public void convertStraplineForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(CHANNEL_STRAPLINE, result.getStrapline());
  }

  @Test
  public void convertOrganisationForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(CHANNEL_ORGANISATION, result.getOrganisation());
  }

  @Test
  public void convertAtomLinkForPublic() {
    // setup
    String expectedAtomFeedUrl = "atomFeedUrl";
    Channel channel = DomainFactoryUtils.createChannel();
    channel.setFeedUrlAtom(expectedAtomFeedUrl);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    LinkDto resultLink = result.getLink();
    assertNotNull(resultLink);
    assertEquals(expectedAtomFeedUrl, resultLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_SELF, resultLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultLink.getType());
    assertNull(resultLink.getTitle());
  }

  @Test
  public void convertViewedSecondsForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(CHANNEL_VIEWED_FOR, result.getStatistics().getViewedSeconds());
  }

  @Test
  public void convertRatingForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(RATING, result.getRating(), 0);
  }

  @Test
  public void convertSubscribersForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(SUBSCRIBERS, result.getStatistics().getSubscribers());
  }

  @Test
  public void convertUpcomingCommunicationsForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(UPCOMING_COMMUNICATIONS, result.getStatistics().getUpcomingCommunications());
  }

  @Test
  public void convertLiveCommunicationsForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(LIVE_COMMUNICATIONS, result.getStatistics().getLiveCommunications());
  }

  @Test
  public void convertRecordedCommunicationsForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(RECORDED_COMMUNICATIONS, result.getStatistics().getRecordedCommunications());
  }

  @Test
  public void convertSearchVisibilityForPublic() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(Searchable.INCLUDED.name().toLowerCase(), result.getSearchVisibility());
  }

  @Test
  public void convertFeaturedCommunicationsForPublic() {
    // setup
    List<Communication> featuredCommunications = new ArrayList<Communication>();
    Date scheduledDateTime = new Date();
    Long expectedCommunicationId = 666l;
    String expectedCommunicationTitle = "communicationTitle";
    Integer expectedDuration = 200;
    String expectedThumbnailUrl = "http://thumnail.com";
    String expectedStatus = "live";
    String expectedStart = "expectedStart";

    Communication communication = new Communication();
    communication.setId(expectedCommunicationId);
    communication.setStatus(Status.LIVE);
    communication.setTitle(expectedCommunicationTitle);
    communication.setScheduledDateTime(scheduledDateTime);
    communication.setDuration(expectedDuration);
    communication.setThumbnailUrl(expectedThumbnailUrl);

    featuredCommunications.add(communication);

    Channel channel = DomainFactoryUtils.createChannel();
    channel.setFeaturedCommunications(featuredCommunications);

    // expectations
    expect(mockFormatter.convert(scheduledDateTime)).andReturn(expectedStart);
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.TRUE).once();
    EasyMock.replay(mockFormatter, mockPresentationCriteria);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);
    FeaturedWebcastDto resultWebcastDto = result.getFeaturedWebcasts().get(0);

    // assertions
    assertNotNull(result.getFeaturedWebcasts());
    assertEquals(1, result.getFeaturedWebcasts().size());

    assertNotNull(resultWebcastDto);
    assertEquals(expectedCommunicationId, resultWebcastDto.getId());
    assertEquals(expectedCommunicationTitle, resultWebcastDto.getTitle());
    assertEquals(expectedDuration, resultWebcastDto.getDuration());
    assertEquals(expectedStatus, resultWebcastDto.getStatus());
    assertEquals(expectedStart, resultWebcastDto.getStart());

    assertNotNull(resultWebcastDto.getThumbnailLink());
    assertEquals(expectedThumbnailUrl, resultWebcastDto.getThumbnailLink().getHref());
    assertEquals(LinkDto.TYPE_IMAGE_PNG, resultWebcastDto.getThumbnailLink().getType());
    assertEquals(LinkDto.RELATIONSHIP_ENCLOSURE, resultWebcastDto.getThumbnailLink().getRel());
    assertEquals(LinkDto.TITLE_THUMBNAIL, resultWebcastDto.getThumbnailLink().getTitle());
    EasyMock.verify(mockFormatter, mockPresentationCriteria);
  }

  @Test
  public void convertForPublicShouldReturnEmptyFeaturedCommunicationsElement() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.TRUE).once();
    EasyMock.replay(mockPresentationCriteria);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertNotNull(result.getFeaturedWebcasts());
    assertEquals(0, result.getFeaturedWebcasts().size());
    EasyMock.verify(mockPresentationCriteria);
  }

  @Test
  public void convertForPublicShouldReturnFeaturedCommunicationsElement() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();
    channel.setFeaturedCommunications(DomainFactoryUtils.createFeaturedCommunications());

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").once();
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.TRUE).once();
    EasyMock.replay(mockFormatter, mockPresentationCriteria);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertNotNull(result.getFeaturedWebcasts());
    assertEquals(1, result.getFeaturedWebcasts().size());
    EasyMock.verify(mockFormatter, mockPresentationCriteria);
  }

  @Test
  public void convertForPublicShouldNotReturnFeaturedCommunicationsElement() {
    // setup
    Channel channel = DomainFactoryUtils.createChannel();
    channel.setFeaturedCommunications(DomainFactoryUtils.createFeaturedCommunications());

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertNull(result.getFeaturedWebcasts());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicShouldReturnEmptyFeaturesWhenChannelWithEmptyFeatures() {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(new ArrayList<FeatureDto>()).times(1);
    EasyMock.replay(mockFeatureConverter);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(0, result.getFeatures().size());
    EasyMock.verify(mockFeatureConverter);
  }

  @Test
  public void convertForPublicShouldReturnEmptyFeaturesWhenChannelWithNoFeatures() {
    // set up
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertNull(result.getFeatures());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicShouldReturnEmptyFeaturesWhenChannelWithOneFeaturesWhichIsNotPublic()
    throws UnsupportedFeatureException {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    // expectations
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(new ArrayList<FeatureDto>()).times(1);
    EasyMock.replay(mockFeatureConverter);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(0, result.getFeatures().size());
    EasyMock.verify(mockFeatureConverter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicShouldReturnFeatureWhenChannelWithShareSocialiseFeature()
    throws UnsupportedFeatureException {
    // set up
    Channel channel = DomainFactoryUtils.createChannel();

    String featureName = "shareSocialise";

    FeatureDto featureDto = new FeatureDto();
    featureDto.setName(featureName);
    featureDto.setEnabled(true);

    List<FeatureDto> featureDtos = new ArrayList<>();
    featureDtos.add(featureDto);

    // expectations
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(featureDtos).times(1);
    EasyMock.replay(mockFeatureConverter);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(1, result.getFeatures().size());
    assertEquals(featureName, result.getFeatures().get(0).getName());
    assertNull(result.getFeatures().get(0).getValue());
    assertEquals(Boolean.TRUE, result.getFeatures().get(0).getEnabled());
    EasyMock.verify(mockFeatureConverter);
  }

  /**
   * Tests that a channel when converted to public has the
   * {@link com.brighttalk.service.channel.business.domain.Feature.Type#ATTENDEE_NETWORKING ATTENDEE_NETWORKING}
   * feature.
   * 
   * @throws UnsupportedFeatureException
   */
  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicShouldReturnFeatureWhenChannelWithAttendeeNetworking() throws UnsupportedFeatureException {
    // set up
    String featureName = "attendeeNetworking";

    Channel channel = DomainFactoryUtils.createChannel();

    FeatureDto featureDto = new FeatureDto();
    featureDto.setName(featureName);
    featureDto.setEnabled(true);

    List<FeatureDto> featureDtos = new ArrayList<>();
    featureDtos.add(featureDto);

    // expectations
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(featureDtos).times(1);
    EasyMock.replay(mockFeatureConverter);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(1, result.getFeatures().size());
    assertEquals("attendeeNetworking", result.getFeatures().get(0).getName());
    assertNull(result.getFeatures().get(0).getValue());
    assertEquals(Boolean.TRUE, result.getFeatures().get(0).getEnabled());
    EasyMock.verify(mockFeatureConverter);
  }

  /**
   * Tests that a channel when converted to public has the
   * {@link com.brighttalk.service.channel.business.domain.Feature.Type#ADS_DISPLAYED ADS_DISPLAYED} feature.
   * 
   * @throws UnsupportedFeatureException
   */
  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicShouldReturnFeatureWhenChannelWithAdsDisplayed() throws UnsupportedFeatureException {
    // set up
    String featureName = "adsDisplayed";

    Channel channel = DomainFactoryUtils.createChannel();

    FeatureDto featureDto = new FeatureDto();
    featureDto.setName(featureName);
    featureDto.setEnabled(true);

    List<FeatureDto> featureDtos = new ArrayList<>();
    featureDtos.add(featureDto);

    // expectations
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(featureDtos).times(1);
    EasyMock.replay(mockFeatureConverter);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(1, result.getFeatures().size());
    assertEquals("adsDisplayed", result.getFeatures().get(0).getName());
    assertNull(result.getFeatures().get(0).getValue());
    assertEquals(Boolean.TRUE, result.getFeatures().get(0).getEnabled());
    EasyMock.verify(mockFeatureConverter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicShouldReturnFeatureWhenChannelWithshareEmbedFeature() throws UnsupportedFeatureException {
    // set up
    String featureName = "shareEmbed";

    Channel channel = DomainFactoryUtils.createChannel();

    FeatureDto featureDto = new FeatureDto();
    featureDto.setName(featureName);
    featureDto.setEnabled(true);

    List<FeatureDto> featureDtos = new ArrayList<>();
    featureDtos.add(featureDto);

    // expectations
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(featureDtos).times(1);
    EasyMock.replay(mockFeatureConverter);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(1, result.getFeatures().size());
    assertEquals("shareEmbed", result.getFeatures().get(0).getName());
    assertNull(result.getFeatures().get(0).getValue());
    assertEquals(Boolean.TRUE, result.getFeatures().get(0).getEnabled());
    EasyMock.verify(mockFeatureConverter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicShouldReturnFeatureWhenChannelWithSharePrerollFeature()
    throws UnsupportedFeatureException {
    // set up
    String featureName = "sharePreroll";

    Channel channel = DomainFactoryUtils.createChannel();

    FeatureDto featureDto = new FeatureDto();
    featureDto.setName(featureName);
    featureDto.setEnabled(true);

    List<FeatureDto> featureDtos = new ArrayList<>();
    featureDtos.add(featureDto);

    // expectations
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(featureDtos).times(1);
    EasyMock.replay(mockFeatureConverter);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(1, result.getFeatures().size());
    assertEquals("sharePreroll", result.getFeatures().get(0).getName());
    assertNull(result.getFeatures().get(0).getValue());
    assertEquals(Boolean.TRUE, result.getFeatures().get(0).getEnabled());
    EasyMock.verify(mockFeatureConverter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicShouldReturnFeatureWhenChannelWithshareEmailFeature() throws UnsupportedFeatureException,
    MalformedURLException {
    // set up
    String featureName = "shareEmail";

    Channel channel = DomainFactoryUtils.createChannel();

    FeatureDto featureDto = new FeatureDto();
    featureDto.setName(featureName);
    featureDto.setEnabled(true);

    List<FeatureDto> featureDtos = new ArrayList<>();
    featureDtos.add(featureDto);

    // expectations
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(featureDtos).times(1);
    EasyMock.replay(mockFeatureConverter);

    // do test
    ChannelPublicDto result = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(1, result.getFeatures().size());
    assertEquals("shareEmail", result.getFeatures().get(0).getName());
    assertNull(result.getFeatures().get(0).getValue());
    assertEquals(Boolean.TRUE, result.getFeatures().get(0).getEnabled());
    EasyMock.verify(mockFeatureConverter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicDontIncludeLinks() throws MalformedURLException {
    // set up
    PaginatedList<Channel> channels = DomainFactoryUtils.createChannels();
    channels.setIsFinalPage(true);
    channels.setCurrentPageNumber(1);

    // expectations
    expect(mockConverterContext.getDateTimeFormatter()).andReturn(mockFormatter).times(2);
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.TRUE).times(2);
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(new ArrayList<FeatureDto>()).times(2);
    EasyMock.replay(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);

    // do test
    ChannelsPublicDto result = uut.convertForPublic(channels, mockConverterContext, mockPresentationCriteria);

    // assertions
    assertNotNull(result);
    assertEquals(2, result.getChannels().size());
    assertNull(result.getLinks());
    EasyMock.verify(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicIncludeLinksWhenPreviousAvailable() throws MalformedURLException {
    // set up
    PaginatedList<Channel> channels = DomainFactoryUtils.createChannels();
    channels.setIsFinalPage(true);
    channels.setCurrentPageNumber(2);
    URL requestUrl = new URL("http://www.local.brighttalk.net");
    Set<String> allowedQueryParamsFilter = new HashSet<String>();

    // expectations
    expect(mockConverterContext.getDateTimeFormatter()).andReturn(mockFormatter).times(2);
    expect(mockConverterContext.getRequestUrl()).andReturn(requestUrl).once();
    expect(mockConverterContext.getAllowedQueryParamsFilter()).andReturn(allowedQueryParamsFilter).once();
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.TRUE).times(2);
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(new ArrayList<FeatureDto>()).times(2);
    EasyMock.replay(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);

    // do test
    ChannelsPublicDto result = uut.convertForPublic(channels, mockConverterContext, mockPresentationCriteria);

    // assertions
    assertNotNull(result.getLinks());
    assertEquals(1, result.getLinks().size());
    EasyMock.verify(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicPreviousLink() throws Exception {
    // set up
    PaginatedList<Channel> channels = DomainFactoryUtils.createChannels();
    channels.setIsFinalPage(true);
    channels.setCurrentPageNumber(2);
    channels.setPageSize(25);
    URL requestUrl = new URL("http://www.local.brighttalk.net?page=2");
    String expectedHref = "http://www.local.brighttalk.net?page=1&pageSize=25";
    Set<String> allowedQueryParamsFilter = new HashSet<String>();

    // expectations
    expect(mockConverterContext.getDateTimeFormatter()).andReturn(mockFormatter).times(2);
    expect(mockConverterContext.getRequestUrl()).andReturn(requestUrl).once();
    expect(mockConverterContext.getAllowedQueryParamsFilter()).andReturn(allowedQueryParamsFilter).once();
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.TRUE).times(2);
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(new ArrayList<FeatureDto>()).times(2);
    EasyMock.replay(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);

    // do test
    ChannelsPublicDto result = uut.convertForPublic(channels, mockConverterContext, mockPresentationCriteria);

    // assertions
    LinkDto resultPreviousLink = result.getLinks().get(0);
    assertNotNull(resultPreviousLink);
    assertNull(resultPreviousLink.getTitle());
    assertEquals(expectedHref, resultPreviousLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, resultPreviousLink.getRel());
    EasyMock.verify(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicIncludeLinksWhenNextAvailable() throws MalformedURLException {
    // set up
    PaginatedList<Channel> channels = DomainFactoryUtils.createChannels();
    channels.setIsFinalPage(false);
    channels.setCurrentPageNumber(1);
    URL requestUrl = new URL("http://www.local.brighttalk.net");
    Set<String> allowedQueryParamsFilter = new HashSet<String>();

    // expectations
    expect(mockConverterContext.getDateTimeFormatter()).andReturn(mockFormatter).times(2);
    expect(mockConverterContext.getRequestUrl()).andReturn(requestUrl).once();
    expect(mockConverterContext.getAllowedQueryParamsFilter()).andReturn(allowedQueryParamsFilter).once();
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.TRUE).times(2);
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(new ArrayList<FeatureDto>()).times(2);
    EasyMock.replay(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);

    // do test
    ChannelsPublicDto result = uut.convertForPublic(channels, mockConverterContext, mockPresentationCriteria);

    // assertions
    assertNotNull(result.getLinks());
    assertEquals(1, result.getLinks().size());
    EasyMock.verify(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void convertForPublicNextLink() throws Exception {
    // set up
    PaginatedList<Channel> channels = DomainFactoryUtils.createChannels();
    channels.setIsFinalPage(false);
    channels.setCurrentPageNumber(1);
    channels.setPageSize(1);
    URL requestUrl = new URL("http://www.local.brighttalk.net?page=1&pageSize=2");
    String expectedHref = "http://www.local.brighttalk.net?page=2&pageSize=1";
    Set<String> allowedQueryParamsFilter = new HashSet<String>();

    // expectations
    expect(mockConverterContext.getDateTimeFormatter()).andReturn(mockFormatter).times(2);
    expect(mockConverterContext.getRequestUrl()).andReturn(requestUrl).once();
    expect(mockConverterContext.getAllowedQueryParamsFilter()).andReturn(allowedQueryParamsFilter).once();
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.TRUE).times(2);
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(new ArrayList<FeatureDto>()).times(2);
    EasyMock.replay(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);

    // do test
    ChannelsPublicDto result = uut.convertForPublic(channels, mockConverterContext, mockPresentationCriteria);

    // assertions
    LinkDto resultNextLink = result.getLinks().get(0);
    assertNotNull(resultNextLink);
    assertNull(resultNextLink.getTitle());
    assertEquals(expectedHref, resultNextLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_NEXT, resultNextLink.getRel());
    EasyMock.verify(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void convertForPublicIncludeLinksWhenPreviousAndNextAvailable() throws MalformedURLException {
    // set up
    PaginatedList<Channel> channels = DomainFactoryUtils.createChannels();
    channels.setIsFinalPage(false);
    channels.setCurrentPageNumber(2);
    URL requestUrl = new URL("http://www.local.brighttalk.net");
    Set<String> allowedQueryParamsFilter = new HashSet<String>();

    // expectations
    expect(mockConverterContext.getDateTimeFormatter()).andReturn(mockFormatter).times(2);
    expect(mockConverterContext.getRequestUrl()).andReturn(requestUrl).times(2);
    expect(mockConverterContext.getAllowedQueryParamsFilter()).andReturn(allowedQueryParamsFilter).times(2);
    expect(mockPresentationCriteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)).andReturn(
        Boolean.TRUE).times(2);
    expect(mockFeatureConverter.convertFeatures(isA(List.class))).andReturn(new ArrayList<FeatureDto>()).times(2);
    EasyMock.replay(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);

    // do test
    ChannelsPublicDto result = uut.convertForPublic(channels, mockConverterContext, mockPresentationCriteria);

    // assertions
    assertNotNull(result.getLinks());
    assertEquals(2, result.getLinks().size());
    EasyMock.verify(mockConverterContext, mockPresentationCriteria, mockFeatureConverter);
  }

  // -------------------------------- Test Channel Keywords conversion --------------------------------

  /**
   * Tests {@link ChannelConverter#convertForPublic} in the case of converting {@link Channel} domain object keywords to
   * {@link ChannelPublicDto}.
   */
  @Test
  public void convertKeywordsForPublic() {
    // setup
    String expectedKeywordsStr = CHANNEL_KEYWORDS + ",keywords1,keywords2";
    List<String> expectedKeywordsList = Arrays.asList(expectedKeywordsStr.split(KEYWORDS_SEPARATOR));
    Channel channel = DomainFactoryUtils.createChannel();
    channel.setKeywords(expectedKeywordsList);

    // do test
    ChannelPublicDto channelPublicDto = uut.convertForPublic(channel, mockFormatter, mockPresentationCriteria);

    // assertions
    assertEquals(expectedKeywordsStr, channelPublicDto.getKeywords());
  }

  /**
   * Tests {@link ChannelConverter#convert(Channel channel, DateTimeFormatter formatter)} in the case of converting
   * {@link Channel} domain object keywords to {@link ChannelDto}.
   */
  @Test
  public void convertFromChannelKeywords() {
    // set up
    String expectedKeywordsStr = CHANNEL_KEYWORDS + ",keywords1,keywords2";
    List<String> expectedKeywordsList = Arrays.asList(expectedKeywordsStr.split(KEYWORDS_SEPARATOR));
    Channel channel = DomainFactoryUtils.createChannel();
    channel.setKeywords(expectedKeywordsList);

    // expectations
    expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    // do test
    ChannelDto channelDto = uut.convert(channel, mockFormatter);
    String keywordsStr = channelDto.getKeywords();
    List<String> keywordsList = channelDto.getKeywordsList();

    // assertions
    assertEquals(expectedKeywordsStr, keywordsStr);
    assertEquals(expectedKeywordsList, keywordsList);
    EasyMock.verify(mockFormatter);
  }

  /**
   * Tests {@link ChannelConverter#convert(ChannelDto channelDto)} in the case of converting {@link ChannelDto} keywords
   * to {@link Channel} domain object.
   */
  @Test
  public void convertFromChannelDtoKeywords() {
    // set up
    String expectedKeywordsStr = CHANNEL_KEYWORDS + ",keywords1,keywords2";
    List<String> expectedKeywordsList = Arrays.asList(expectedKeywordsStr.split(KEYWORDS_SEPARATOR));
    ChannelDto channelDto = DtoFactoryUtils.createChannelDto();
    channelDto.setKeywordsList(expectedKeywordsList);

    // do test
    Channel channel = uut.convert(channelDto);
    List<String> keywordsList = channel.getKeywords();

    // assertions
    assertEquals(expectedKeywordsList, keywordsList);
  }

}
