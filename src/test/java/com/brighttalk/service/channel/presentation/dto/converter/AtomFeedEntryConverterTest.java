/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AtomFeedEntryConverterTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.Communication.Format;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration.Visibility;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.atom.AtomFeedDto;
import com.brighttalk.service.channel.presentation.dto.atom.CategoryDto;
import com.brighttalk.service.channel.presentation.dto.atom.FeedEntryDto;

public class AtomFeedEntryConverterTest {

  private static final Long COMMUNICATION_ID = 55L;

  private static final Long CHANNEL_ID = 666L;
  
  private static final String COMMUNICATION_TITLE = "title1";

  private static final String COMMUNICATION_DESCRIPTION = "commDescription1";

  private static final Float COMMUNICATION_RATING = 4.36234f;

  private static final String PRESENTER_NAME = "presenterName1";
  
  private static final int CREATED_YEAR = 1994;

  private static final int LAST_UPDATED_YEAR = 2004;
  
  private static final int LAST_UPDATED_MONTH = 2;
  
  private static final int LAST_UPDATED_DAY = 13;
  
  private static final int LAST_UPDATED_HOUR = 10;

  private static final int LAST_UPDATED_MINUTE = 21;
  
  private static final int LAST_UPDATED_SECOND = 0;
  
  private static final int DURATION = 180;
  
  private static final String KEYWORD_1 = "keyword1";

  private static final String KEYWORD_2 = "keyword2";
  
  private static final String CATEGORY_DESCRIPTION_1 = "description1";

  private static final String CATEGORY_DESCRIPTION_2 = "description2";
  
  private static final int ONE_SECOND = 1000;
  
  private static final String COMMUNICATION_URL = "http://www.local.brighttalk.net/communicationUrl";
  
  
  private AtomFeedEntryConverter uut;
  
  private Communication communication;
  
  
  @Before
  public void setUp() {
    
    uut = new AtomFeedEntryConverter();
    communication = createCommunication();
  }
  
  @Test
  public void convert() {
   
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    assertNotNull( result );
  }

  @Test
  public void convertNoHidden() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    assertNull( result.getHidden() );
  }

  @Test
  public void convertHidden() {
    
    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setVisibility( Visibility.FEEDONLY );
    communication.setConfiguration( configuration );
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    assertTrue( result.getHidden() );
  }
  
  @Test
  public void convertId() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    String expectedId = AtomFeedDto.BRIGHTTALK_TAG + CREATED_YEAR + ":communication:" + COMMUNICATION_ID;
    assertEquals( expectedId, result.getId() );
  }

  @Test
  public void convertTitle() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    assertEquals( COMMUNICATION_TITLE, result.getTitle() );
  }

  @Test
  public void convertCommunicationStatus() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    String expectedStatus = Status.UPCOMING.toString().toLowerCase(); 
    assertEquals( expectedStatus, result.getStatus() );
  }
  
  @Test
  public void convertSummary() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    assertEquals( COMMUNICATION_DESCRIPTION, result.getSummary() );
  }

  @Test
  public void convertFormat() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    String expectedFormat = Format.VIDEO.toString().toLowerCase();
    assertEquals( expectedFormat, result.getFormat() );
  }

  @Test
  public void convertDuration() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    assertEquals( DURATION, result.getDuration(), 0 );
  }
  
  @Test
  public void convertRating() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    Float expectedRating = 4.4f;
    assertEquals( expectedRating, result.getRating() );
  }

  @Test
  public void convertCommunication() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    assertEquals( COMMUNICATION_ID, result.getCommunication().getId() );
  }

  @Test
  public void convertChannel() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    assertEquals( CHANNEL_ID, result.getChannel().getId() );
  }

  @Test
  public void convertAuthor() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    assertEquals( PRESENTER_NAME, result.getAuthor().getName() );
  }

  @Test
  public void convertUpdated() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    //assertions
    StringBuilder builder = new StringBuilder();
    builder.append( LAST_UPDATED_YEAR ).append("-0");
    builder.append( LAST_UPDATED_MONTH + 1 ).append("-");
    builder.append( LAST_UPDATED_DAY ).append("T");
    builder.append( LAST_UPDATED_HOUR ).append(":");
    builder.append( LAST_UPDATED_MINUTE ).append(":");
    builder.append( LAST_UPDATED_SECOND ).append("0Z");

    final String expectedUpdated = builder.toString(); 
    assertEquals( expectedUpdated, result.getUpdated() );
  }
  
  @Test
  public void convertCategories() {
    
    String keywords = KEYWORD_1 + " , " + KEYWORD_2; 
    communication.setKeywords( keywords );
    
    CommunicationCategory category1 = new CommunicationCategory(1l, CHANNEL_ID, COMMUNICATION_ID, CATEGORY_DESCRIPTION_1, true);
    communication.addCategory(category1);

    CommunicationCategory category2 = new CommunicationCategory(1l, CHANNEL_ID, COMMUNICATION_ID, CATEGORY_DESCRIPTION_2, true);
    communication.addCategory(category2);
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    assertEquals( 4, result.getCategories().size() );
    assertEquals( CategoryDto.CATEGORY_SCHEMA_KEYWORD, result.getCategories().get(0).getScheme() );
    assertEquals( KEYWORD_1, result.getCategories().get(0).getTerm() );
    assertEquals( CategoryDto.CATEGORY_SCHEMA_KEYWORD, result.getCategories().get(1).getScheme() );
    assertEquals( KEYWORD_2, result.getCategories().get(1).getTerm() );
    
    assertEquals( CategoryDto.CATEGORY_SCHEMA_CATEGORY, result.getCategories().get(2).getScheme() );
    assertEquals( CATEGORY_DESCRIPTION_1, result.getCategories().get(2).getTerm() );
    assertEquals( CategoryDto.CATEGORY_SCHEMA_CATEGORY, result.getCategories().get(3).getScheme() );
    assertEquals( CATEGORY_DESCRIPTION_2, result.getCategories().get(3).getTerm() );
  }

  @Test
  public void convertNoCategories() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    assertNull( result.getCategories() );
  }
  
  @Test
  public void convertNotFeatured() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    assertNull( result.getFeatured() );
  }

  @Test
  public void convertFeatured() {
    
    communication.setFeatured();
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    assertTrue( result.getFeatured() );
  }
  
  @Test
  public void convertStartTIme() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    final long expectedStart = communication.getScheduledDateTime().getTime() / ONE_SECOND;
    assertEquals( expectedStart, result.getStart() );
  }
  
  @Test
  public void convertEntryTime() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    final long expectedEntry = communication.getEntryTime().getTime() / ONE_SECOND;
    assertEquals( expectedEntry, result.getEntryTime(), 0 );
  }
  
  @Test
  public void convertCloseTime() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    final long expectedClose = communication.getCloseTime().getTime() / ONE_SECOND;
    assertEquals( expectedClose, result.getCloseTime(), 0 );
  }
  
  @Test
  public void convertCommunicationUrl() {
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    assertNotNull( result.getLinks() );
    assertEquals( 1, result.getLinks().size() );
    
    LinkDto resultCommunicationLink = result.getLinks().get( 0 );
    assertEquals( COMMUNICATION_URL, resultCommunicationLink.getHref() );
    assertEquals( LinkDto.RELATIONSHIP_ALTERNATE, resultCommunicationLink.getRel() );
    assertEquals( LinkDto.TYPE_TEXT_HTML, resultCommunicationLink.getType() );
    assertNull( resultCommunicationLink.getTitle() );
  }
  
  @Test
  public void convertThumbnailUrl() {
    
    final String expectedThumbnailUrl = "http://www.local.brighttalk.net/thumbnail";
    communication.setThumbnailUrl( expectedThumbnailUrl );
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    assertEquals( 2, result.getLinks().size() );
    
    LinkDto resultThumbnailLink = result.getLinks().get( 1 );
    assertEquals( expectedThumbnailUrl, resultThumbnailLink.getHref() );
    assertEquals( LinkDto.RELATIONSHIP_ENCLOSURE, resultThumbnailLink.getRel() );
    assertEquals( LinkDto.TYPE_IMAGE_PNG, resultThumbnailLink.getType() );
    assertEquals( AtomFeedEntryConverter.THUMBNAIL_LINK_TITLE, resultThumbnailLink.getTitle() );
  }
  
  @Test
  public void convertPreviewUrl() {
    
    final String expectedPreviewUrl = "http://www.local.brighttalk.net/preview";
    communication.setPreviewUrl( expectedPreviewUrl );
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    assertEquals( 2, result.getLinks().size() );
    
    LinkDto resultPreviewLink = result.getLinks().get( 1 );
    assertEquals( expectedPreviewUrl, resultPreviewLink.getHref() );
    assertEquals( LinkDto.RELATIONSHIP_RELATED, resultPreviewLink.getRel() );
    assertEquals( LinkDto.TYPE_IMAGE_PNG, resultPreviewLink.getType() );
    assertEquals( AtomFeedEntryConverter.PREVIEW_LINK_TITLE, resultPreviewLink.getTitle() );
  }
  
  @Test
  public void convertCalendarLink() {
    
    final String calendarUrl = "http://www.local.brighttalk.net/calendarUrl";
    communication.setCalendarUrl( calendarUrl );
    
    //do test
    FeedEntryDto result = uut.convert( communication );
    
    assertEquals( 2, result.getLinks().size() );
    
    LinkDto resultCalendarLink = result.getLinks().get( 1 );
    assertEquals( calendarUrl, resultCalendarLink.getHref() );
    assertEquals( LinkDto.RELATIONSHIP_RELATED, resultCalendarLink.getRel() );
    assertEquals( LinkDto.TYPE_TEXT_CALENDAR, resultCalendarLink.getType() );
    assertEquals( AtomFeedEntryConverter.CALENDAR_LINK_TITLE, resultCalendarLink.getTitle() );
  }
  
  private Communication createCommunication() {
    
    Calendar calendar = Calendar.getInstance();
    
    Communication communication = new Communication();
    communication.setId( COMMUNICATION_ID );
    communication.setChannelId( CHANNEL_ID );
    
    calendar.set( CREATED_YEAR, 1, 1 );
    Date created = calendar.getTime();
    communication.setCreated( created );
    
    communication.setDuration( DURATION );
    
    communication.setTitle( COMMUNICATION_TITLE );
    communication.setDescription( COMMUNICATION_DESCRIPTION );
    communication.setStatus( Status.UPCOMING );
    communication.setFormat( Format.VIDEO );
    communication.setUrl( COMMUNICATION_URL );
    
    calendar.set(LAST_UPDATED_YEAR, LAST_UPDATED_MONTH, LAST_UPDATED_DAY, LAST_UPDATED_HOUR, LAST_UPDATED_MINUTE, LAST_UPDATED_SECOND );
    
    Date lastUpdated = calendar.getTime();
    communication.setLastUpdated( lastUpdated );
    
    calendar.set(LAST_UPDATED_YEAR + 1, LAST_UPDATED_MONTH + 1, LAST_UPDATED_DAY + 1, LAST_UPDATED_HOUR + 1, LAST_UPDATED_MINUTE + 1, LAST_UPDATED_SECOND + 1);
    Date scheduled = calendar.getTime();
    communication.setScheduledDateTime( scheduled );
    
    communication.setAuthors( PRESENTER_NAME );
    
    CommunicationStatistics statistics = new CommunicationStatistics(
        COMMUNICATION_ID, 0, 0, 1, COMMUNICATION_RATING );
    communication.setCommunicationStatistics( statistics );
    
    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setVisibility( Visibility.PUBLIC );
    communication.setConfiguration( configuration );
    
    return communication;
  }

}