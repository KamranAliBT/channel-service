package com.brighttalk.service.channel.presentation.controller.external;

import static junit.framework.Assert.assertNotNull;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.isA;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.presentation.dto.converter.context.builder.ExternalConverterContextBuilder;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria.SortBy;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.error.MaxChannelManagersFeatureException;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;
import com.brighttalk.service.channel.business.service.channel.ChannelManagerFinder;
import com.brighttalk.service.channel.business.service.channel.ChannelManagerService;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersPublicDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelManagerConverter;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;
import com.brighttalk.service.channel.presentation.util.ChannelManagersSearchCriteriaBuilder;

public class ChannelManagersControllerTest {

  private static final Long CHANNEL_ID = 888L;

  private static final Long CHANNEL_OWNER_ID = 666L;

  private static final Integer PAGE_NUMBER = 0;

  private static final Integer PAGE_SIZE = 10;

  private static final SortBy SORT_BY = SortBy.EMAIL_ADDRESS;

  private static final SortOrder SORT_ORDER = SortOrder.ASC;

  private static final Integer MAX_CHANNEL_MANAGERS = 5;

  private ChannelManagersController uut;

  private ChannelManagerService mockChannelManagerService;

  private ChannelManagerConverter mockChannelManagerConverter;

  private ChannelManagersSearchCriteriaBuilder mockSearchCriteriaBuilder;

  private ChannelManagerFinder mockChannelManagerFinder;

  private ExternalConverterContextBuilder mockContextBuilder;

  private HttpServletRequest mockRequest;

  private HttpServletResponse mockResponse;

  private DateTimeFormatter mockDateTimeFormatter;

  private Set<Object> mocks;

  @Before
  public void setUp() {

    uut = new ChannelManagersController();

    mockChannelManagerConverter = EasyMock.createMock(ChannelManagerConverter.class);
    mockChannelManagerService = EasyMock.createMock(ChannelManagerService.class);
    mockSearchCriteriaBuilder = EasyMock.createMock(ChannelManagersSearchCriteriaBuilder.class);
    mockChannelManagerFinder = EasyMock.createMock(ChannelManagerFinder.class);
    mockContextBuilder = EasyMock.createMock(ExternalConverterContextBuilder.class);
    mockRequest = EasyMock.createMock(HttpServletRequest.class);
    mockResponse = EasyMock.createMock(HttpServletResponse.class);
    mockDateTimeFormatter = createMock(DateTimeFormatter.class);

    ReflectionTestUtils.setField(uut, "converter", mockChannelManagerConverter);
    ReflectionTestUtils.setField(uut, "channelManagerService", mockChannelManagerService);
    ReflectionTestUtils.setField(uut, "searchCriteriaBuilder", mockSearchCriteriaBuilder);
    ReflectionTestUtils.setField(uut, "channelManagerFinder", mockChannelManagerFinder);
    ReflectionTestUtils.setField(uut, "contextBuilder", mockContextBuilder);

    mocks = new HashSet<Object>();
  }

  @Test
  public void testCreate() {

    // Set up
    String email1 = "email1@brighttalk.com";
    String email2 = "email2@brighttalk.com";
    String email3 = "email3@brighttalk.com";

    com.brighttalk.common.user.User channelOwner = buildChannelOwner(CHANNEL_OWNER_ID);

    ChannelManagersDto channelManagersDto = buildChannelManagersDto(email1, email2, email3);

    List<ChannelManager> channelManagers = buildChannelManagers(channelManagersDto);

    ChannelManagersPublicDto channelManagersPublicDto = new ChannelManagersPublicDto();

    // Expectations
    expectChannelManagerConverter(channelManagers);
    expectChannelManagerServiceWhenCreating(channelManagers);
    expectChannelManagerConverter(channelManagersPublicDto);
    replay();

    // Do test
    ChannelManagersPublicDto result = uut.create(channelOwner, CHANNEL_ID, channelManagersDto, mockResponse);

    // Assertions
    assertNotNull(result);
    verify();
  }

  @Test(expected = BadRequestException.class)
  public void testCreateWhenUserEmailAddressMissing() {

    // Set up
    String email1 = "email1@brighttalk.com";
    String email2 = null;
    String email3 = "email3@brighttalk.com";

    com.brighttalk.common.user.User channelOwner = buildChannelOwner(CHANNEL_OWNER_ID);

    ChannelManagersDto channelManagersDto = buildChannelManagersDto(email1, email2, email3);

    // Expectations

    // Do test
    uut.create(channelOwner, CHANNEL_ID, channelManagersDto, mockResponse);

    // Assertions
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testCreateWhenChannelNotFound() {

    // Set up
    String email1 = "email1@brighttalk.com";

    com.brighttalk.common.user.User channelOwner = buildChannelOwner(CHANNEL_OWNER_ID);

    ChannelManagersDto channelManagersDto = buildChannelManagersDto(email1);

    List<ChannelManager> channelManagers = buildChannelManagers(channelManagersDto);

    // Expectations
    expectChannelManagerConverter(channelManagers);
    expectChannelManagerServiceToThrowExceptionWhenCreating(new ChannelNotFoundException(CHANNEL_ID));
    replay();

    // Do test
    uut.create(channelOwner, CHANNEL_ID, channelManagersDto, mockResponse);

    // Assertions
    verify();
  }

  @Test(expected = NotAuthorisedException.class)
  public void testCreateWhenChannelManagersfeatureDisabled() {

    // Set up
    String email1 = "email1@brighttalk.com";

    com.brighttalk.common.user.User channelOwner = buildChannelOwner(CHANNEL_OWNER_ID);

    ChannelManagersDto channelManagersDto = buildChannelManagersDto(email1);

    List<ChannelManager> channelManagers = buildChannelManagers(channelManagersDto);

    // Expectations
    expectChannelManagerConverter(channelManagers);
    expectChannelManagerServiceToThrowExceptionWhenCreating(new FeatureNotEnabledException(""));
    replay();

    // Do test
    uut.create(channelOwner, CHANNEL_ID, channelManagersDto, mockResponse);

    // Assertions
    verify();
  }

  @Test(expected = NotAuthorisedException.class)
  public void testCreateWhenChannelManagersLimitReached() {

    // Set up
    String email1 = "email1@brighttalk.com";

    com.brighttalk.common.user.User channelOwner = buildChannelOwner(CHANNEL_OWNER_ID);

    ChannelManagersDto channelManagersDto = buildChannelManagersDto(email1);

    List<ChannelManager> channelManagers = buildChannelManagers(channelManagersDto);

    // Expectations
    expectChannelManagerConverter(channelManagers);
    expectChannelManagerServiceToThrowExceptionWhenCreating(new MaxChannelManagersFeatureException(CHANNEL_ID,
        MAX_CHANNEL_MANAGERS, channelManagers.size()));
    replay();

    // Do test
    uut.create(channelOwner, CHANNEL_ID, channelManagersDto, mockResponse);

    // Assertions
    verify();
  }

  @Test
  public void testFind() {

    // Set up
    com.brighttalk.common.user.User channelOwner = buildChannelOwner(CHANNEL_OWNER_ID);

    ChannelManagersSearchCriteria criteria = buildChannelManagersSearchCriteria();

    // Expectations
    expectChannelManagersSearchCriteriaWithDefaultValues(criteria);
    expectChannelManagerFinder();
    expectChannelManagerConverterWithFilter(new ChannelManagersPublicDto());
    replay();

    // Do test
    ChannelManagersPublicDto result = uut.find(channelOwner, CHANNEL_ID, mockRequest, mockDateTimeFormatter);

    // Assertions
    assertNotNull(result);
    verify();
  }

  @Test(expected = BadRequestException.class)
  public void testFindWhenInvalidSearchCriteria() {

    // Set up
    com.brighttalk.common.user.User channelOwner = buildChannelOwner(CHANNEL_OWNER_ID);

    // Expectations
    expectChannelManagersSearchCriteriaToThrowException();
    replay();

    // Do test
    uut.find(channelOwner, CHANNEL_ID, mockRequest, mockDateTimeFormatter);

    // Assertions
    verify();
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testFindWhenChannelNotFound() {

    // Set up
    com.brighttalk.common.user.User channelOwner = buildChannelOwner(CHANNEL_OWNER_ID);

    ChannelManagersSearchCriteria criteria = buildChannelManagersSearchCriteria();

    // Expectations
    expectChannelManagersSearchCriteriaWithDefaultValues(criteria);
    expectChannelManagerFinderToThrowExceptionWhenFiding(new ChannelNotFoundException(CHANNEL_ID));
    replay();

    // Do test
    uut.find(channelOwner, CHANNEL_ID, mockRequest, mockDateTimeFormatter);

    // Assertions
    verify();
  }

  @Test(expected = NotAuthorisedException.class)
  public void testFindWhenChannelManagersfeatureDisabled() {

    // Set up
    com.brighttalk.common.user.User channelOwner = buildChannelOwner(CHANNEL_OWNER_ID);

    ChannelManagersSearchCriteria criteria = buildChannelManagersSearchCriteria();

    // Expectations
    expectChannelManagersSearchCriteriaWithDefaultValues(criteria);
    expectChannelManagerFinderToThrowExceptionWhenFiding(new FeatureNotEnabledException(""));
    replay();

    // Do test
    uut.find(channelOwner, CHANNEL_ID, mockRequest, mockDateTimeFormatter);

    // Assertions
    verify();
  }

  private void expectChannelManagerConverter(final List<ChannelManager> channelManagers) {
    EasyMock.expect(mockChannelManagerConverter.convert(isA(ChannelManagersDto.class))).andReturn(channelManagers);
    expect(mockChannelManagerConverter);
  }

  private void expectChannelManagersSearchCriteriaWithDefaultValues(final ChannelManagersSearchCriteria criteria) {
    EasyMock.expect(mockSearchCriteriaBuilder.buildWithDefaultValues(isA(HttpServletRequest.class))).andReturn(criteria);
    expect(mockSearchCriteriaBuilder);
  }

  private void expectChannelManagersSearchCriteriaToThrowException() {
    EasyMock.expect(mockSearchCriteriaBuilder.buildWithDefaultValues(isA(HttpServletRequest.class))).andThrow(
        new SearchCriteriaException(null, null));
    expect(mockSearchCriteriaBuilder);
  }

  @SuppressWarnings("unchecked")
  private void expectChannelManagerConverter(final ChannelManagersPublicDto channelManagersPublicDto) {
    EasyMock.expect(mockChannelManagerConverter.convertForPublic(isA(List.class), isA(List.class))).andReturn(
        channelManagersPublicDto);
    expect(mockChannelManagerConverter);
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  private void expectChannelManagerConverterWithFilter(final ChannelManagersPublicDto channelManagersPublicDto) {
    ConverterContext converterContext = null;
    EasyMock.expect(mockChannelManagerConverter.convertForPublic(new PaginatedList(), converterContext)).andReturn(
        channelManagersPublicDto);
    expect(mockChannelManagerConverter);
  }

  @SuppressWarnings("unchecked")
  private void expectChannelManagerServiceWhenCreating(final List<ChannelManager> channelManagers) {
    EasyMock.expect(
        mockChannelManagerService.create(isA(com.brighttalk.common.user.User.class), isA(Long.class), isA(List.class))).andReturn(
            channelManagers);
    expect(mockChannelManagerService);
  }

  @SuppressWarnings("unchecked")
  private void expectChannelManagerServiceToThrowExceptionWhenCreating(final Throwable cause) {
    EasyMock.expect(
        mockChannelManagerService.create(isA(com.brighttalk.common.user.User.class), isA(Long.class), isA(List.class))).andThrow(
            cause);
    expect(mockChannelManagerService);
  }

  private void expectChannelManagerFinder() {
    EasyMock.expect(
        mockChannelManagerFinder.find(isA(com.brighttalk.common.user.User.class), isA(Long.class),
            isA(ChannelManagersSearchCriteria.class))).andReturn(new PaginatedList<ChannelManager>());
    expect(mockChannelManagerFinder);
  }

  private void expectChannelManagerFinderToThrowExceptionWhenFiding(final Throwable cause) {
    EasyMock.expect(
        mockChannelManagerFinder.find(isA(com.brighttalk.common.user.User.class), isA(Long.class),
            isA(ChannelManagersSearchCriteria.class))).andThrow(cause);
    expect(mockChannelManagerFinder);
  }

  private void expect(final Object obj) {
    mocks.add(obj);
  }

  private void replay() {
    EasyMock.replay(mocks.toArray());
  }

  private void verify() {
    EasyMock.verify(mocks.toArray());
  }

  private ChannelManagersSearchCriteria buildChannelManagersSearchCriteria() {
    ChannelManagersSearchCriteria criteria = new ChannelManagersSearchCriteria();
    criteria.setPageNumber(PAGE_NUMBER);
    criteria.setPageSize(PAGE_SIZE);
    criteria.setSortBy(SORT_BY);
    criteria.setSortOrder(SORT_ORDER);

    return criteria;
  }

  private List<ChannelManager> buildChannelManagers(final ChannelManagersDto channelManagersDto) {
    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    ChannelManager channelManager;
    User user;

    for (ChannelManagerDto channelManagerDto : channelManagersDto.getChannelManagers()) {
      user = new User();
      user.setEmail(channelManagerDto.getUser().getEmail());

      channelManager = new ChannelManager();
      channelManager.setUser(user);
      channelManager.setEmailAlerts(channelManagerDto.getEmailAlerts());

      channelManagers.add(channelManager);
    }

    return channelManagers;
  }

  private ChannelManagersDto buildChannelManagersDto(final String... emails) {

    ChannelManagersDto channelManagersDto = new ChannelManagersDto();

    for (String email : emails) {
      channelManagersDto.addChannelManager(buildChannelManagerDto(email));
    }

    return channelManagersDto;
  }

  private ChannelManagerDto buildChannelManagerDto(final String email) {

    UserDto userDto = new UserDto();
    userDto.setEmail(email);

    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setUser(userDto);

    return channelManagerDto;
  }

  // private ChannelManager buildChannelManager() {
  //
  // User user = new User();
  // user.setId(USER_ID);
  // user.setEmail(EMAIL_ADDRESS);
  //
  // ChannelManager channelManager = new ChannelManager();
  // channelManager.setUser(user);
  //
  // return channelManager;
  // }

  private com.brighttalk.common.user.User buildChannelOwner(final Long id) {

    com.brighttalk.common.user.User channelOwner = new com.brighttalk.common.user.User();
    channelOwner.setId(id);

    return channelOwner;
  }

}
