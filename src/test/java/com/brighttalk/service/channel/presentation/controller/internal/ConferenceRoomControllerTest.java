/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ConferenceRoomControllerTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import static org.easymock.EasyMock.createMock;

import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.DialledInTooEarlyException;
import com.brighttalk.service.channel.business.error.DialledInTooLateException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;

/**
 * Unit tests for {@link ConferenceRoomController}.
 */
public class ConferenceRoomControllerTest {

  private static Long COMMUNICATION_ID = 132L;
  private static String MOCK_CONFERENCE_ROOM_URL = "conferenceRoomUrlTest/{communicationId}";

  private ConferenceRoomController uut;

  private CommunicationFinder mockCommunicationFinder;

  @Before
  public void setUp() {

    uut = new ConferenceRoomController();

    mockCommunicationFinder = createMock(CommunicationFinder.class);

    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);

  }

  @Test
  public void testGetConferenceRoomReturnsConferenceRoomUrl() {

    // setup
    String pin = "01234567";
    String providerString = "brighttalkhd";

    Communication communication = new Communication();
    communication.setId(COMMUNICATION_ID);
    String expectedConferenceRoomUrl = MOCK_CONFERENCE_ROOM_URL.replace("{communicationId}",
        communication.getId().toString());

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(pin, providerString)).andReturn(expectedConferenceRoomUrl);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    String result = uut.getConferenceRoom(pin, providerString, new MockHttpServletResponse());

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(expectedConferenceRoomUrl, result);
    EasyMock.verify(mockCommunicationFinder);
  }

  @Test
  public void testGetConferenceRoomReturnsBadRequestWhenPinIsNotSet() {

    // setup
    String pin = "";
    String providerString = "brighttalkhd";

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(pin, providerString)).andThrow(new NotFoundException("NotFound"));
    HttpServletResponse response = createMock(HttpServletResponse.class);
    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockCommunicationFinder, response);

    // do test
    String result = uut.getConferenceRoom(pin, providerString, response);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(NotFoundException.ERROR_CODE_NOT_FOUND_ERROR, result);
    EasyMock.verify(mockCommunicationFinder, response);
  }

  @Test
  public void testGetConferenceRoomReturnsBadRequestWhenPinIsInvalid() {

    // setup
    String pin = "invalidPin";
    String providerString = "brighttalkhd";

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(pin, providerString)).andThrow(new NotFoundException("NotFound"));
    HttpServletResponse response = createMock(HttpServletResponse.class);
    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockCommunicationFinder, response);

    // do test
    String result = uut.getConferenceRoom(pin, providerString, response);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(NotFoundException.ERROR_CODE_NOT_FOUND_ERROR, result);
    EasyMock.verify(mockCommunicationFinder, response);
  }

  @Test
  public void testGetConferenceRoomReturnsTooEarlyErrorCode() {

    // setup
    String pin = "01234567";
    String providerString = "brighttalkhd";

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(pin, providerString)).andThrow(
        new DialledInTooEarlyException("Dialled in too Early"));
    HttpServletResponse response = createMock(HttpServletResponse.class);
    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockCommunicationFinder, response);

    // do test
    String result = uut.getConferenceRoom(pin, providerString, response);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(DialledInTooEarlyException.ERROR_CODE_DIALLED_IN_TOO_EARLY_ERROR, result);

    EasyMock.verify(mockCommunicationFinder, response);
  }

  @Test
  public void testGetConferenceRoomReturnsTooLateErrorCode() {

    // setup
    String pin = "01234567";
    String providerString = "brighttalkhd";

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(pin, providerString)).andThrow(
        new DialledInTooLateException("Dialled in too Early"));
    HttpServletResponse response = createMock(HttpServletResponse.class);
    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockCommunicationFinder, response);

    // do test
    String result = uut.getConferenceRoom(pin, providerString, response);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(DialledInTooLateException.ERROR_CODE_DIALLED_IN_TOO_LATE_ERROR, result);

    EasyMock.verify(mockCommunicationFinder, response);
  }

  @Test
  public void testGetConferenceRoomReturnsNotFoundErrorCode() {

    // setup
    String pin = "01234567";
    String providerString = "brighttalkhd";

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(pin, providerString)).andThrow(
        new NotFoundException("Communication not found"));

    HttpServletResponse response = createMock(HttpServletResponse.class);
    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockCommunicationFinder, response);

    // do test
    String result = uut.getConferenceRoom(pin, providerString, response);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(NotFoundException.ERROR_CODE_NOT_FOUND_ERROR, result);

    EasyMock.verify(mockCommunicationFinder, response);
  }

}
