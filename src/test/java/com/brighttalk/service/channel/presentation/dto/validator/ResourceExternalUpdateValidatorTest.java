/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceExternalUpdateValidatorTest.java 63781 2013-04-29 10:40:35Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.error.InvalidResourceException;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourceLinkDto;

/**
 * Unit tests for {@link ResourceExternalUpdateValidator}.
 */
public class ResourceExternalUpdateValidatorTest extends AbstractResourceValidatorTest {

  private ResourceExternalUpdateValidator uut;

  @Before
  public void setUp() {
    uut = new ResourceExternalUpdateValidator(new Long(RESOURCE_ID));

  }

  /**
   * Test validateExternal of external file resource Dto when updating title .
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithTitle() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(RESOURCE_TITLE_2, null, null, null, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating title with empty value .
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithEmptyTitle() {

    // setup
    ResourceDto resourceDto = createExternalFileResource("", null, null, null, null);
    resourceDto.setId(RESOURCE_ID);
    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceTitleMissing", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating title with null value .
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithTitleNull() {

    // setup

    ResourceDto resourceDto = createExternalFileResource(null, null, null, null, null);
    resourceDto.setId(RESOURCE_ID);
    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating title with too long value .
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithTooLongTitle() {

    // setup
    ResourceDto resourceDto = createExternalFileResource("", null, null, null, null);
    resourceDto.setId(RESOURCE_ID);
    resourceDto.setTitle("test:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:test");

    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceTitleTooLong", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating description.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithDescription() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, null, RESOURCE_DESCRIPTION_2, null, null);
    resourceDto.setId(RESOURCE_ID);
    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating description with empty description.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithEmptyDescription() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, null, "", null, null);
    resourceDto.setId(RESOURCE_ID);
    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * * Test validateExternal of external file resource Dto when updating description with null value.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithDescriptionNull() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, null, null, null, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating description with too long value.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithTooLongDescription() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, null, null, null, null);
    resourceDto.setId(RESOURCE_ID);
    resourceDto.setDescription("test:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:test");

    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceDescriptionTooLong", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating url.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithUrl() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, RESOURCE_URL_2, null, null, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating url being empty value.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithEmptyUrl() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, "", null, null, null);
    resourceDto.setId(RESOURCE_ID);
    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlMissing", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating url with null value.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithUrlNull() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, null, null, null, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating url with too long value.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithTooLongUrl() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, "", null, null, null);
    resourceDto.setId(RESOURCE_ID);
    String href = StringUtils.repeat("test", 1000);
    resourceDto.getFile().setHref(href);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlTooLong", ie.getUserErrorMessage());
    }
  }

  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithValidUrlContaingHttp() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, "http://google.com", null, null, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithValidUrlContaingHttps() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, "https://google.com", null, null, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithInvalidUrl() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, "javascript:alert('xss')", null, null, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlIsInvalid", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating mime type.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithMimeType() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, null, null, RESOURCE_MIME_TYPE, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating mime type with too long value.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithTooLongMimeType() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, null, null, null, null);
    resourceDto.setId(RESOURCE_ID);
    resourceDto.getFile().setMimeType(
            "test:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:test");

    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceMimeTypeTooLong", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateExternal of external file resource Dto when updating size.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingExternalFileResourceWithSize() {

    // setup
    ResourceDto resourceDto = createExternalFileResource(null, null, null, null, RESOURCE_FILE_SIZE);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of internal file resource Dto when updating title .
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingInternalFileResourceWithTitle() {

    // setup
    ResourceDto resourceDto = createInternalFileResource(RESOURCE_TITLE_2, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of internal file resource Dto when updating description .
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingInternalFileResourceWithDescription() {

    // setup
    ResourceDto resourceDto = createInternalFileResource(null, RESOURCE_DESCRIPTION_2);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of internal file resource Dto when updating not allowed field url .
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingInternalFileResourceWithNotAllowedUrl() {

    // setup
    ResourceDto resourceDto = createInternalFileResource(null, null);
    resourceDto.setId(RESOURCE_ID);
    resourceDto.getFile().setHref(RESOURCE_URL_2);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlNotAllowed", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateExternal of internal file resource Dto when updating not allowed field mime type .
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingInternalFileResourceWithNotAllowedMimeType() {

    // setup
    ResourceDto resourceDto = createInternalFileResource(null, null);
    resourceDto.setId(RESOURCE_ID);
    resourceDto.getFile().setMimeType(RESOURCE_MIME_TYPE);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceMimeTypeNotAllowed", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateExternal of internal file resource Dto when updating not allowed field size.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingInternalFileResourceWithNotAllowedSize() {

    // setup
    ResourceDto resourceDto = createInternalFileResource(null, null);
    resourceDto.setId(RESOURCE_ID);
    resourceDto.getFile().setSize(RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceSizeNotAllowed", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateExternal of file resource Dto when updating hosted with invalid value .
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithInvalidHosted() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, null, null);
    resourceDto.getFile().setHosted("test");

    // do test
    try {
      uut.validate(resourceDto);
      // assert

    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of link resource Dto when updating title.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingLinkResourceWithTitle() {

    // setup
    ResourceDto resourceDto = createLinkResource(RESOURCE_TITLE_2, null, null);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of link resource Dto when updating description.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingLinkResourceWithDescription() {

    // setup
    ResourceDto resourceDto = createLinkResource(null, null, RESOURCE_DESCRIPTION_2);
    resourceDto.setId(RESOURCE_ID);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of link resource Dto when updating url.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingLinkResourceWithUrl() {

    // setup
    ResourceDto resourceDto = createLinkResource(null, RESOURCE_URL_2, null);
    resourceDto.setId(RESOURCE_ID);
    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateExternal of link resource Dto when updating url with too long value.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingLinkResourceWithTooLongUrl() {

    // setup
    ResourceDto resourceDto = createLinkResource(null, null, null);
    resourceDto.setId(RESOURCE_ID);
    resourceDto.getLink().setHref(
            "test:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
                + "testtest:testtest:test");

    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlTooLong", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validate external of resource Dto with link and file elements provided.
   */
  @Test
  public void testValidateDissallowInternalFileForResourceDtoBeingResourceWithLinkAndFileSpecified() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, RESOURCE_TITLE, RESOURCE_DESCRIPTION);
    resourceDto.setLink(new ResourceLinkDto());

    // do test
    try {
      uut.validate(resourceDto);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("InvalidResource", ie.getUserErrorCode());
    }
  }

  /**
   * Test validate external of resource Dto with resourceId and id resourceDto.id not matching.
   */
  @Test
  public void testValidateAllowInternalFileForResourceDtoAndResourceIdNotMatching() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(999L, RESOURCE_TITLE, RESOURCE_DESCRIPTION);

    // do test
    try {
      uut.validate(resourceDto);
      Assert.fail();
    } catch (ApplicationException ie) {
      // assert
      Assert.assertEquals("InvalidResource", ie.getUserErrorCode());
    }
  }
}
