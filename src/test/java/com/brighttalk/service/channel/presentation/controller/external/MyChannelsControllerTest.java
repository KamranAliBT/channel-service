/**
 * **************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MyChannelsControllerTest.java 72818 2014-01-21 15:28:21Z ikerbib $
 * **************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import java.util.LinkedHashMap;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.presentation.controller.internal.MySubscribedChannelsController;
import com.brighttalk.service.channel.presentation.dto.WebcastsTotalsDto;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastsTotalsConverter;

/**
 * Unit tests for {@link MySubscribedChannelsController}.
 */
public class MyChannelsControllerTest {

  private MyChannelsController uut;

  private User user;

  private CommunicationFinder mockCommunicationFinder;

  private WebcastsTotalsConverter mockWebcastsTotalsConverter;

  private static final Long USER_ID = 1L;

  @Before
  public void setUp() {
    uut = new MyChannelsController();

    user = new User(USER_ID);
    mockCommunicationFinder = EasyMock.createMock(CommunicationFinder.class);
    mockWebcastsTotalsConverter = EasyMock.createMock(WebcastsTotalsConverter.class);

    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);
    ReflectionTestUtils.setField(uut, "webcastsTotalsConverter", mockWebcastsTotalsConverter);
  }

  @Test
  public void getSubscribedChannelsWebcastTotalsShouldReturnWebcastTotalsDto() {

    LinkedHashMap<String, Long> webcastTotals = new LinkedHashMap<String, Long>();

    EasyMock.expect(mockCommunicationFinder.findSubscribedChannelsCommunicationsTotals(USER_ID)).andReturn(
        webcastTotals);

    EasyMock.expect(mockWebcastsTotalsConverter.convert(webcastTotals)).andReturn(new WebcastsTotalsDto());

    EasyMock.replay(mockCommunicationFinder, mockWebcastsTotalsConverter);

    uut.getSubscribedChannelsWebcastTotals(user);

    EasyMock.verify(mockCommunicationFinder, mockWebcastsTotalsConverter);
  }
}
