/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourcesControllerTest.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.easymock.EasyMock.createMock;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.service.ResourceService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourcesDto;
import com.brighttalk.service.channel.presentation.dto.converter.ResourceConverter;

/**
 * Unit tests for {@link ResourcesController}.
 */
public class ResourcesControllerTest {

  private static final Long USER_ID = 999L;
  private static final Long CHANNEL_ID = 5L;
  private static final Long COMMUNICATION_ID = 15L;
  private static final Long RESOURCE_ID = 115L;
  private static final Long RESOURCE_ID_2 = 1151L;

  private ResourcesController uut;
  
  private ChannelFinder mockChannelFinder;

  private ResourceService mockResourceService;

  private ResourceConverter resourceConverter;

  @Before
  public void setUp() {
    this.uut = new ResourcesController();
    this.resourceConverter = new ResourceConverter();
    this.mockChannelFinder = createMock(ChannelFinder.class);
    this.mockResourceService = createMock(ResourceService.class);

    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);
    ReflectionTestUtils.setField(uut, "converter", resourceConverter);
    ReflectionTestUtils.setField(uut, "resourceService", mockResourceService);

  }

  @Test
  public void testGetResources() {
    // setup
    User user = createUser();
    Channel channel = new Channel(CHANNEL_ID);

    List<Resource> resources = new ArrayList<Resource>();
    resources.add(new Resource(RESOURCE_ID));
    resources.add(new Resource(RESOURCE_ID_2));

    // expectations
    EasyMock.expect(mockChannelFinder.find(EasyMock.anyLong())).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    // expectations
    EasyMock.expect(
        mockResourceService.findResources(EasyMock.anyObject(User.class), EasyMock.anyObject(Channel.class),
            EasyMock.anyLong())).andReturn(
        resources);
    EasyMock.replay(mockResourceService);

    // do test
    ResourcesDto result = uut.getResources(user, CHANNEL_ID, COMMUNICATION_ID);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(RESOURCE_ID, result.getResources().get(0).getId());
    Assert.assertEquals(RESOURCE_ID_2, result.getResources().get(1).getId());

  }

  @SuppressWarnings("unchecked")
  @Test
  public void testOrderResource() {
    // setup
    User user = createUser();
    Channel channel = new Channel(CHANNEL_ID);

    List<Resource> resources = new ArrayList<Resource>();
    resources.add(new Resource(RESOURCE_ID));
    resources.add(new Resource(RESOURCE_ID_2));

    ResourcesDto resourcesDto = new ResourcesDto();
    List<ResourceDto> resourcesDtoList = new ArrayList<ResourceDto>();
    ResourceDto resourceDto1 = new ResourceDto();
    resourceDto1.setId(RESOURCE_ID_2);
    ResourceDto resourceDto2 = new ResourceDto();
    resourceDto2.setId(RESOURCE_ID);

    resourcesDtoList.add(resourceDto1);
    resourcesDtoList.add(resourceDto2);

    resourcesDto.setResources(resourcesDtoList);

    // expectations
    EasyMock.expect(mockChannelFinder.find(EasyMock.anyLong())).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    // expectations
    EasyMock.expect(
        mockResourceService.updateResourcesOrder(EasyMock.anyObject(User.class),
            EasyMock.anyObject(Channel.class), EasyMock.anyLong(),
            EasyMock.anyObject(ArrayList.class))).andReturn(resources);
    EasyMock.replay(mockResourceService);

    // do test
    ResourcesDto result = uut.updateResourcesOrder(user, CHANNEL_ID, COMMUNICATION_ID, resourcesDto);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(RESOURCE_ID, result.getResources().get(0).getId());
    Assert.assertEquals(RESOURCE_ID_2, result.getResources().get(1).getId());

  }

  private User createUser() {
    User user = new User();
    user.setId(USER_ID);
    return user;
  }
}
