/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceInternalControllerTest.java 63781 2013-04-29 10:40:35Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import static org.easymock.EasyMock.createMock;
import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.service.ResourceService;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourceLinkDto;
import com.brighttalk.service.channel.presentation.dto.converter.ResourceConverter;

/**
 * Unit tests for {@link ResourceInternalController}.
 */
public class ResourceInternalControllerTest {

  private static final Long RESOURCE_ID = 115L;
  private static final String RESOURCE_URL = "http://test.com";
  private static final String RESOURCE_URL_2 = "http://test2.com";

  private ResourceInternalController uut;

  private ResourceService mockResourceService;

  private ResourceConverter resourceConverter;

  @Before
  public void setUp() {

    this.uut = new ResourceInternalController();

    this.resourceConverter = new ResourceConverter();
    this.mockResourceService = createMock(ResourceService.class);

    ReflectionTestUtils.setField(uut, "converter", resourceConverter);
    ReflectionTestUtils.setField(uut, "resourceService", mockResourceService);

  }

  @Test
  public void testUpdateResource() {

    // setup
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);
    ResourceLinkDto resourceLinkDto = new ResourceLinkDto();
    resourceLinkDto.setHref(RESOURCE_URL);
    resourceDto.setLink(resourceLinkDto);
    Resource resource = new Resource(RESOURCE_ID);
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL_2);

    // expectations
    EasyMock.expect(mockResourceService.updateResource(EasyMock.anyObject(Resource.class))).andReturn(resource);
    EasyMock.replay(mockResourceService);

    // do test
    ResourceDto result = uut.updateResource(RESOURCE_ID, resourceDto);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_URL_2, result.getLink().getHref());

  }

  @Test(expected = ApplicationException.class)
  public void testUpdateResourceInvalidResourceId() {
    // setup
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);

    // do test
    uut.updateResource(999999L, resourceDto);

    // assertions
  }
}
