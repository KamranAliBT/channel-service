/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MyCommunicationsSearchCriteriaBuilderTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria.CommunicationStatusFilterType;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria.SortBy;
import com.brighttalk.service.channel.presentation.ApiRequestParam;

public class MyCommunicationsSearchCriteriaBuilderTest {

  private MyCommunicationsSearchCriteriaBuilder uut;

  private HttpServletRequest mockRequest;

  @Before
  public void setUp() {

    mockRequest = EasyMock.createMock(HttpServletRequest.class);

    uut = new MyCommunicationsSearchCriteriaBuilder();
  }

  @Test
  public void build() {

    setEmptyRulesForMockRequeast(null);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNotNull(result);
  }

  @Test
  public void buildDefaultPageNumber() {

    setEmptyRulesForMockRequeast(null);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNull(result.getPageNumber());
  }

  @Test
  public void buildPageNumber() {

    setEmptyRulesForMockRequeast(ApiRequestParam.PAGE_NUMBER);

    final String pageNumberString = "10";
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(pageNumberString);

    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    final Integer expectedPageNumber = 10;
    assertEquals(expectedPageNumber, result.getPageNumber());
  }

  @Test
  public void buildDefaultPageSize() {

    setEmptyRulesForMockRequeast(null);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNull(result.getPageSize());
  }

  @Test
  public void buildPageSize() {

    setEmptyRulesForMockRequeast(ApiRequestParam.PAGE_SIZE);

    final String pageSizeString = "2";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(pageSizeString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    final Integer expectedPageSize = 2;
    assertEquals(expectedPageSize, result.getPageSize());
  }

  @Test
  public void buildDefaultSortBy() {

    setEmptyRulesForMockRequeast(null);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertEquals(SortBy.SCHEDULEDDATE, result.getSortBy());
  }

  @Test
  public void buildSortByTotalViewings() {

    setEmptyRulesForMockRequeast(ApiRequestParam.SORT_BY);

    final String sortByString = "totalViewings";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(sortByString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertEquals(SortBy.TOTALVIEWINGS, result.getSortBy());
  }

  @Test
  public void buildSortByScheduledDate() {

    setEmptyRulesForMockRequeast(ApiRequestParam.SORT_BY);

    final String sortByString = "scheduledDate";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(sortByString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertEquals(SortBy.SCHEDULEDDATE, result.getSortBy());
  }

  @Test
  public void buildSortByAvarageRating() {

    setEmptyRulesForMockRequeast(ApiRequestParam.SORT_BY);

    final String sortByString = "averageRating";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(sortByString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertEquals(SortBy.AVERAGERATING, result.getSortBy());
  }

  @Test(expected = ApplicationException.class)
  public void buildSortByInvalidValue() {

    setEmptyRulesForMockRequeast(ApiRequestParam.SORT_BY);

    final String sortByString = "someInvalidValue";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(sortByString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertEquals(SortBy.AVERAGERATING, result.getSortBy());
  }

  @Test
  public void buildDefaultSortOrder() {

    setEmptyRulesForMockRequeast(null);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertEquals(SortOrder.DESC, result.getSortOrder());
  }

  @Test
  public void buildSortOrder() {

    setEmptyRulesForMockRequeast(ApiRequestParam.SORT_ORDER);

    final String sortOrderString = "asc";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(sortOrderString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertEquals(SortOrder.ASC, result.getSortOrder());
  }

  @Test
  public void buildRegisteredDefaultValue() {

    setEmptyRulesForMockRequeast(ApiRequestParam.REGISTERED);

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.REGISTERED)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNull(result.getRegistered());
    assertFalse(result.hasRegistered());
  }

  @Test(expected = ApplicationException.class)
  public void buildRegisteredEmptyValue() {

    setEmptyRulesForMockRequeast(ApiRequestParam.REGISTERED);

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.REGISTERED)).andReturn("");
    EasyMock.replay(mockRequest);

    // do test
    uut.build(mockRequest);
  }

  @Test(expected = ApplicationException.class)
  public void buildRegisteredInvalidValue() {

    setEmptyRulesForMockRequeast(ApiRequestParam.REGISTERED);

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.REGISTERED)).andReturn("invalidValue");
    EasyMock.replay(mockRequest);

    // do test
    uut.build(mockRequest);
  }

  @Test
  public void buildRegisteredTrue() {

    setEmptyRulesForMockRequeast(ApiRequestParam.REGISTERED);

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.REGISTERED)).andReturn("true");
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertTrue(result.hasRegistered());
    assertTrue(result.getRegistered());
  }

  @Test(expected = ApplicationException.class)
  public void buildRegisteredFalse() {

    setEmptyRulesForMockRequeast(ApiRequestParam.REGISTERED);

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.REGISTERED)).andReturn("false");
    EasyMock.replay(mockRequest);

    // do test
    uut.build(mockRequest);
  }

  @Test
  public void buildDefaultWebcastStatusFilter() {

    setEmptyRulesForMockRequeast(null);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNull(result.getCommunicationStatusFilter());
    assertFalse(result.hasCommunicationsStatusFilter());
  }

  @Test(expected = ApplicationException.class)
  public void buildWebcastStatusFilterInvalidValue() {

    setEmptyRulesForMockRequeast(ApiRequestParam.WEBCAST_STATUS);

    final String webcastStatusString = "someInvalidValue";
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.WEBCAST_STATUS)).andReturn(webcastStatusString);
    EasyMock.replay(mockRequest);

    // do test
    uut.build(mockRequest);
  }

  @Test
  public void buildWebcastStatusFilterEmptyValue() {

    setEmptyRulesForMockRequeast(ApiRequestParam.WEBCAST_STATUS);

    final String webcastStatusString = "";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.WEBCAST_STATUS)).andReturn(webcastStatusString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNull(result.getCommunicationStatusFilter());
  }

  @Test
  public void buildWebcastStatusFilterUpcoming() {

    setEmptyRulesForMockRequeast(ApiRequestParam.WEBCAST_STATUS);

    final String webcastStatusString = "upcoming";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.WEBCAST_STATUS)).andReturn(webcastStatusString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNotNull(result.getCommunicationStatusFilter());
    assertTrue(result.hasCommunicationsStatusFilter());
    assertEquals(1, result.getCommunicationStatusFilter().size());
    assertEquals(CommunicationStatusFilterType.UPCOMING, result.getCommunicationStatusFilter().get(0));
  }

  @Test
  public void buildWebcastStatusFilterLive() {

    setEmptyRulesForMockRequeast(ApiRequestParam.WEBCAST_STATUS);

    final String webcastStatusString = "live";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.WEBCAST_STATUS)).andReturn(webcastStatusString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNotNull(result.getCommunicationStatusFilter());
    assertEquals(1, result.getCommunicationStatusFilter().size());
    assertEquals(CommunicationStatusFilterType.LIVE, result.getCommunicationStatusFilter().get(0));
  }

  @Test
  public void buildWebcastStatusFilterRecorded() {

    setEmptyRulesForMockRequeast(ApiRequestParam.WEBCAST_STATUS);

    final String webcastStatusString = "recorded";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.WEBCAST_STATUS)).andReturn(webcastStatusString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNotNull(result.getCommunicationStatusFilter());
    assertEquals(1, result.getCommunicationStatusFilter().size());
    assertEquals(CommunicationStatusFilterType.RECORDED, result.getCommunicationStatusFilter().get(0));
  }

  @Test
  public void buildWebcastStatusFilterLiveAndRecorded() {

    setEmptyRulesForMockRequeast(ApiRequestParam.WEBCAST_STATUS);

    final String webcastStatusString = "live,recorded";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.WEBCAST_STATUS)).andReturn(webcastStatusString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNotNull(result.getCommunicationStatusFilter());
    assertEquals(2, result.getCommunicationStatusFilter().size());
    assertEquals(CommunicationStatusFilterType.LIVE, result.getCommunicationStatusFilter().get(0));
    assertEquals(CommunicationStatusFilterType.RECORDED, result.getCommunicationStatusFilter().get(1));
  }

  @Test
  public void buildWebcastStatusFilterLiveAndRecordedAndUpcomingWithTrim() {

    setEmptyRulesForMockRequeast(ApiRequestParam.WEBCAST_STATUS);

    final String webcastStatusString = "live,recorded, upcoming";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.WEBCAST_STATUS)).andReturn(webcastStatusString);
    EasyMock.replay(mockRequest);

    // do test
    MyCommunicationsSearchCriteria result = uut.build(mockRequest);

    assertNotNull(result.getCommunicationStatusFilter());
    assertEquals(3, result.getCommunicationStatusFilter().size());
    assertEquals(CommunicationStatusFilterType.LIVE, result.getCommunicationStatusFilter().get(0));
    assertEquals(CommunicationStatusFilterType.RECORDED, result.getCommunicationStatusFilter().get(1));
    assertEquals(CommunicationStatusFilterType.UPCOMING, result.getCommunicationStatusFilter().get(2));
  }

  private void setEmptyRulesForMockRequeast(String excludeParameter) {

    if (!ApiRequestParam.SORT_BY.equals(excludeParameter)) {
      EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null);
    }

    if (!ApiRequestParam.SORT_ORDER.equals(excludeParameter)) {
      EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null);
    }

    if (!ApiRequestParam.PAGE_NUMBER.equals(excludeParameter)) {
      EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    }

    if (!ApiRequestParam.PAGE_SIZE.equals(excludeParameter)) {
      EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    }

    if (!ApiRequestParam.WEBCAST_STATUS.equals(excludeParameter)) {
      EasyMock.expect(mockRequest.getParameter(ApiRequestParam.WEBCAST_STATUS)).andReturn(null);
    }

    if (!ApiRequestParam.REGISTERED.equals(excludeParameter)) {
      EasyMock.expect(mockRequest.getParameter(ApiRequestParam.REGISTERED)).andReturn(null);
    }
  }
}