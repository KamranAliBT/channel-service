/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastPublicConverterTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationStatisticsBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.WebcastPublicDto;

public class WebcastPublicConverterTest {

  private static final Long COMMUNICATION_ID = 666l;
  private static final Long CHANNEL_ID = 13l;

  private WebcastPublicConverter uut;

  private DateTimeFormatter mockFormatter;

  private Communication communication;

  @Before
  public void setUp() {

    uut = new WebcastPublicConverter();

    mockFormatter = EasyMock.createMock(DateTimeFormatter.class);

    communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);
    communication.setChannelId(CHANNEL_ID);
  }

  @Test
  public void convertWhenNull() {

    // do test
    WebcastPublicDto result = uut.convert(null, mockFormatter);

    assertNotNull(result);
    assertNull(result.getId());
    assertNull(result.getChannel());
    assertNull(result.getDescription());
    assertNull(result.getDuration());
    assertNull(result.getStart());
    assertNull(result.getStatus());
    assertNull(result.getLinks());
    assertNull(result.getTitle());
    assertNull(result.getRating());
    assertNull(result.getTotalViewings());
  }

  @Test
  public void convert() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertNotNull(result);
  }

  @Test
  public void convertId() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertEquals(COMMUNICATION_ID, result.getId());
  }

  @Test
  public void convertChannel() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertNotNull(result.getChannel());
    assertEquals(CHANNEL_ID, result.getChannel().getId());
  }

  @Test
  public void convertDescription() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_DESCRIPTION, result.getDescription());
  }

  @Test
  public void convertPresenter() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_PRESENTER, result.getPresenter());
  }

  @Test
  public void convertDuration() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_DURATION, result.getDuration());
  }

  @Test
  public void convertStart() {

    String expectedStart = "expectedStart";

    EasyMock.expect(mockFormatter.convert(communication.getScheduledDateTime())).andReturn(expectedStart);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertEquals(expectedStart, result.getStart());
  }

  @Test
  public void convertStatus() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    final String expectedStatus = CommunicationBuilder.DEFAULT_STATUS.toString().toLowerCase();
    assertEquals(expectedStatus, result.getStatus());
  }

  @Test
  public void convertTitle() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_TITLE, result.getTitle());
  }

  @Test
  public void convertThumbnailWhenNoUrl() {

    communication.setThumbnailUrl(null);

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertNull(result.getLinks());
  }

  @Test
  public void convertThumbnail() {

    final String expectedThumbnailLinkHref = "expectedThumbnailLinkHref";
    communication.setThumbnailUrl(expectedThumbnailLinkHref);

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertNotNull(result.getLinks().get(0));
    assertEquals(expectedThumbnailLinkHref, result.getLinks().get(0).getHref());
    assertEquals(LinkDto.TYPE_IMAGE_PNG, result.getLinks().get(0).getType());
    assertEquals(LinkDto.RELATIONSHIP_ENCLOSURE, result.getLinks().get(0).getRel());
    assertEquals(LinkDto.TITLE_THUMBNAIL, result.getLinks().get(0).getTitle());
  }

  @Test
  public void convertRating() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationStatisticsBuilder.DEFAULT_AVERAGE_RATING, result.getRating(), 0);
  }

  @Test
  public void convertTotalViewings() {

    // do test
    WebcastPublicDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationStatisticsBuilder.DEFAULT_NUMBER_OF_VIEWINGS, result.getTotalViewings(), 0);
  }

  @Test
  public void convertPublic() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertNotNull(result);
  }

  @Test
  public void convertPublicId() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(COMMUNICATION_ID, result.getId());
  }

  @Test
  public void convertPublicChannel() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertNotNull(result.getChannel());
    assertEquals(CHANNEL_ID, result.getChannel().getId());
  }

  @Test
  public void convertPublicDescription() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_DESCRIPTION, result.getDescription());
  }

  @Test
  public void convertPublicKeywords() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_KEYWORDS, result.getKeywords());
  }

  @Test
  public void convertPublicFormat() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_FORMAT.toString().toLowerCase(), result.getFormat());
  }

  @Test
  public void convertPublicPresenter() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_PRESENTER, result.getPresenter());
  }

  @Test
  public void convertPublicDuration() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_DURATION, result.getDuration());
  }

  @Test
  public void convertPublicStart() {

    String expectedStart = "expectedStart";

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn(expectedStart).times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(expectedStart, result.getStart());
  }

  @Test
  public void convertPublicEntryTime() {

    String expectedEntry = "expectedEntry";

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn(expectedEntry).times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(expectedEntry, result.getEntryTime());
  }

  @Test
  public void convertPublicCloseTime() {

    String expectedClose = "expectedClose";

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn(expectedClose).times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(expectedClose, result.getCloseTime());
  }

  @Test
  public void convertCreated() {

    String expectedCreated = "expectedCreated";

    Date created = new Date();
    communication.setCreated(created);
    communication.setLastUpdated(null);

    EasyMock.expect(mockFormatter.convert(communication.getCreated())).andReturn(expectedCreated).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getEntryTime())).andReturn(null).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getCloseTime())).andReturn(null).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getScheduledDateTime())).andReturn(null).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getLastUpdated())).andReturn(null).times(1);

    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(expectedCreated, result.getCreated());
  }

  @Test
  public void convertLastUpdated() {

    String expectedLastUpdated = "expectedLastUpdated";

    Date lastUpdated = new Date();
    communication.setCreated(null);
    communication.setLastUpdated(lastUpdated);

    EasyMock.expect(mockFormatter.convert(communication.getLastUpdated())).andReturn(expectedLastUpdated).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getCreated())).andReturn(null).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getEntryTime())).andReturn(null).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getCloseTime())).andReturn(null).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getScheduledDateTime())).andReturn(null).times(1);

    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(expectedLastUpdated, result.getLastUpdated());
  }

  @Test
  public void convertPublicStatusIsNull() {

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);// do test

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertNull(result.getStatus());
  }

  @Test
  public void convertPublicTitle() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_TITLE, result.getTitle());
  }

  @Test
  public void convertPublicWhenNoUrl() {

    communication.setThumbnailUrl(null);
    communication.setPreviewUrl(null);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertNull(result.getLinks());
  }

  @Test
  public void convertPublicThumbnail() {

    final String expectedThumbnailLinkHref = "expectedThumbnailLinkHref";
    communication.setThumbnailUrl(expectedThumbnailLinkHref);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertNotNull(result.getLinks().get(0));
    assertEquals(expectedThumbnailLinkHref, result.getLinks().get(0).getHref());
    assertEquals(LinkDto.TYPE_IMAGE_PNG, result.getLinks().get(0).getType());
    assertEquals(LinkDto.RELATIONSHIP_ENCLOSURE, result.getLinks().get(0).getRel());
    assertEquals(LinkDto.TITLE_THUMBNAIL, result.getLinks().get(0).getTitle());
  }

  @Test
  public void convertPublicPreviewUrl() {

    final String expectedPreviewLinkHref = "expectedPreviewLinkHref";
    communication.setThumbnailUrl(null);
    communication.setPreviewUrl(expectedPreviewLinkHref);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertNotNull(result.getLinks().get(0));
    assertEquals(expectedPreviewLinkHref, result.getLinks().get(0).getHref());
    assertEquals(LinkDto.TYPE_IMAGE_PNG, result.getLinks().get(0).getType());
    assertEquals(LinkDto.RELATIONSHIP_RELATED, result.getLinks().get(0).getRel());
    assertEquals(LinkDto.TITLE_PREVIEW, result.getLinks().get(0).getTitle());
  }

  @Test
  public void convertPublicCalendarUrl() {

    final String expectedCalendarLinkHref = "expectedPreviewLinkHref";
    communication.setThumbnailUrl(null);
    communication.setPreviewUrl(null);
    communication.setCalendarUrl(expectedCalendarLinkHref);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertNotNull(result.getLinks().get(0));
    assertEquals(expectedCalendarLinkHref, result.getLinks().get(0).getHref());
    assertEquals(LinkDto.TYPE_TEXT_CALENDAR, result.getLinks().get(0).getType());
    assertEquals(LinkDto.RELATIONSHIP_RELATED, result.getLinks().get(0).getRel());
    assertEquals(LinkDto.TITLE_CALENDAR, result.getLinks().get(0).getTitle());
  }

  @Test
  public void convertPublicWithLinksCalendarUrl() {

    final String expectedCalendarLinkHref = "expectedPreviewLinkHref";
    final String expectedPreviewLinkHref = "expectedPreviewLinkHref";
    final String expectedThumbnailLinkHref = "expectedThumbnailLinkHref";
    communication.setThumbnailUrl(expectedThumbnailLinkHref);
    communication.setPreviewUrl(expectedPreviewLinkHref);
    communication.setCalendarUrl(expectedCalendarLinkHref);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertNotNull(result.getLinks());
    assertEquals(3, result.getLinks().size());
  }

  @Test
  public void convertPublicRating() {

    // do test

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertEquals(CommunicationStatisticsBuilder.DEFAULT_AVERAGE_RATING, result.getRating(), 0);
  }

  @Test
  public void convertPublicTotalViewings() {

    // do test

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastPublicDto result = uut.convertPublic(communication, mockFormatter);

    assertNull(result.getTotalViewings());
  }
}