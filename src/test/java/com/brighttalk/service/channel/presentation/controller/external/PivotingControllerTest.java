/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastControllerTest.java 85329 2014-10-29 10:22:21Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.easymock.EasyMock.createMock;
import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.ProviderNotSupportedException;
import com.brighttalk.service.channel.business.service.communication.CommunicationPivotService;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.presentation.dto.WebcastProviderDto;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastProviderConverter;

/**
 * Unit tests for {@link PivotingController}.
 */
public class PivotingControllerTest {

  private static final Long WEBCAST_ID = 115L;

  private PivotingController uut;

  private WebcastProviderConverter mockProviderConverter;

  private CommunicationDbDao mockCommunicationDbDao;

  private CommunicationPivotService mockAudioSlidesPivotService;
  private CommunicationPivotService mockMediazoneToVideoPivotService;

  private BrighttalkValidator mockBrighttalkValidator;

  @Before
  public void setUp() {

    uut = new PivotingController();

    mockProviderConverter = createMock(WebcastProviderConverter.class);
    ReflectionTestUtils.setField(uut, "providerConverter", mockProviderConverter);

    mockCommunicationDbDao = createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDbDao);

    mockAudioSlidesPivotService = createMock(CommunicationPivotService.class);
    ReflectionTestUtils.setField(uut, "audioSlidesToMediazonePivotService", mockAudioSlidesPivotService);

    mockMediazoneToVideoPivotService = createMock(CommunicationPivotService.class);
    ReflectionTestUtils.setField(uut, "mediazoneToVideoPivotService", mockMediazoneToVideoPivotService);

    mockBrighttalkValidator = createMock(BrighttalkValidator.class);
    ReflectionTestUtils.setField(uut, "validator", mockBrighttalkValidator);

  }

  @Test
  public void pivotSuccessAudioSlidesToMediazone() {
    // setup
    User user = new User();
    Provider newProvider = new Provider(Provider.MEDIAZONE);
    Provider currentProvider = new Provider(Provider.BRIGHTTALK);
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(Provider.MEDIAZONE);

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withProvider(
        currentProvider).build();

    // expectations
    mockBrighttalkValidator.validate(webcastProviderDto);
    EasyMock.expectLastCall();
    EasyMock.expect(mockProviderConverter.convert(webcastProviderDto)).andReturn(newProvider);
    EasyMock.expect(mockCommunicationDbDao.find(WEBCAST_ID)).andReturn(communication);
    EasyMock.expect(mockAudioSlidesPivotService.pivot(communication, newProvider)).andReturn(newProvider);
    EasyMock.expect(mockProviderConverter.convert(newProvider)).andReturn(webcastProviderDto);

    EasyMock.replay(mockProviderConverter, mockCommunicationDbDao, mockAudioSlidesPivotService);

    // do test
    WebcastProviderDto responseDto = uut.pivot(user, webcastProviderDto, WEBCAST_ID);

    // assertions
    Assert.assertNotNull(responseDto);
    EasyMock.verify(mockProviderConverter, mockCommunicationDbDao, mockAudioSlidesPivotService);

  }

  @Test
  public void pivotSuccessMediazoneToVideo() {
    // setup
    User user = new User();
    Provider newProvider = new Provider(Provider.VIDEO);
    Provider currentProvider = new Provider(Provider.MEDIAZONE);
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(Provider.VIDEO);

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withProvider(
        currentProvider).build();

    // expectations
    mockBrighttalkValidator.validate(webcastProviderDto);
    EasyMock.expectLastCall();
    EasyMock.expect(mockProviderConverter.convert(webcastProviderDto)).andReturn(newProvider);
    EasyMock.expect(mockCommunicationDbDao.find(WEBCAST_ID)).andReturn(communication);
    EasyMock.expect(mockMediazoneToVideoPivotService.pivot(communication, newProvider)).andReturn(newProvider);
    EasyMock.expect(mockProviderConverter.convert(newProvider)).andReturn(webcastProviderDto);

    EasyMock.replay(mockProviderConverter, mockCommunicationDbDao, mockMediazoneToVideoPivotService);

    // do test
    WebcastProviderDto responseDto = uut.pivot(user, webcastProviderDto, WEBCAST_ID);

    // assertions
    Assert.assertNotNull(responseDto);
    EasyMock.verify(mockProviderConverter, mockCommunicationDbDao, mockMediazoneToVideoPivotService);

  }

  @Test
  public void pivotSuccessVideoToMediazone() {
    // setup
    User user = new User();
    Provider newProvider = new Provider(Provider.MEDIAZONE);
    Provider currentProvider = new Provider(Provider.VIDEO);
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(Provider.MEDIAZONE);

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withProvider(
        currentProvider).build();

    // expectations
    mockBrighttalkValidator.validate(webcastProviderDto);
    EasyMock.expectLastCall();
    EasyMock.expect(mockProviderConverter.convert(webcastProviderDto)).andReturn(newProvider);
    EasyMock.expect(mockCommunicationDbDao.find(WEBCAST_ID)).andReturn(communication);
    EasyMock.expect(mockMediazoneToVideoPivotService.pivot(communication, newProvider)).andReturn(newProvider);
    EasyMock.expect(mockProviderConverter.convert(newProvider)).andReturn(webcastProviderDto);

    EasyMock.replay(mockProviderConverter, mockCommunicationDbDao, mockMediazoneToVideoPivotService);

    // do test
    WebcastProviderDto responseDto = uut.pivot(user, webcastProviderDto, WEBCAST_ID);

    // assertions
    Assert.assertNotNull(responseDto);
    EasyMock.verify(mockProviderConverter, mockCommunicationDbDao, mockMediazoneToVideoPivotService);

  }

  @Test(expected = ProviderNotSupportedException.class)
  public void pivotFailForInvalidProvider() {
    // setup
    User user = new User();
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(Provider.BRIGHTTALK_HD);

    // expectations
    mockBrighttalkValidator.validate(webcastProviderDto);
    EasyMock.expectLastCall().andThrow(new ProviderNotSupportedException("invalid provider"));

    EasyMock.replay(mockBrighttalkValidator);

    // do test
    uut.pivot(user, webcastProviderDto, WEBCAST_ID);

    // assertions
    EasyMock.verify(mockBrighttalkValidator);

  }
}
