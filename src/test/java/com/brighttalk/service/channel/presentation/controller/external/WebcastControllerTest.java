/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastControllerTest.java 70141 2013-10-23 15:46:02Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.queue.AuditQueue;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationCancelService;
import com.brighttalk.service.channel.business.service.communication.CommunicationCreateService;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationUpdateService;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.WebcastAuditRecordDtoBuilder;
import com.brighttalk.service.channel.presentation.dto.ChannelPublicDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastConverter;
import com.brighttalk.service.channel.presentation.dto.validator.HDWebcastCreateValidator;
import com.brighttalk.service.channel.presentation.dto.validator.WebcastUpdateValidator;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.InvalidParamException;
import com.brighttalk.service.channel.presentation.error.InvalidWebcastException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

/**
 * Unit tests for {@link WebcastController}.
 */
public class WebcastControllerTest {

  private static final Long USER_ID = 1L;
  private static final Long CHANNEL_ID = 111L;
  private static final Long WEBCAST_ID = 222L;

  private static final String CHANNEL_ID_1 = "1234";
  private static final String WEBCAST_ID_1 = "4321";

  /** Action description for audit record when webcast is cancelled **/
  private static final String CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION =
      "Cancelled through channel (Java) external API call";

  private WebcastController uut;

  private ChannelFinder mockChannelFinder;

  private CommunicationCreateService mockCommunicationCreateService;

  private CommunicationUpdateService mockCommunicationUpdateService;

  private CommunicationCancelService mockCommunicationCancelService;

  private CommunicationFinder mockCommunicationFinder;

  private WebcastConverter mockWebcastConverter;

  private DateTimeFormatter mockFormatter;

  private HDWebcastCreateValidator mockHDWebcastCreateValidator;

  private WebcastUpdateValidator mockWebcastUpdateValidator;

  private AuditQueue mockAuditQueue;

  private AuditRecordDtoBuilder mockWebcastAuditRecordDtoBuilder;

  @Before
  public void setUp() {

    uut = new WebcastController();

    mockChannelFinder = createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockCommunicationCreateService = createMock(CommunicationCreateService.class);
    ReflectionTestUtils.setField(uut, "communicationCreateService", mockCommunicationCreateService);

    mockCommunicationUpdateService = createMock(CommunicationUpdateService.class);
    ReflectionTestUtils.setField(uut, "hdCommunicationUpdateService", mockCommunicationUpdateService);

    mockCommunicationCancelService = createMock(CommunicationCancelService.class);
    ReflectionTestUtils.setField(uut, "communicationCancelService", mockCommunicationCancelService);

    mockCommunicationFinder = createMock(CommunicationFinder.class);
    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);

    mockWebcastConverter = EasyMock.createMock(WebcastConverter.class);
    ReflectionTestUtils.setField(uut, "webcastConverter", mockWebcastConverter);

    mockHDWebcastCreateValidator = EasyMock.createMock(HDWebcastCreateValidator.class);
    ReflectionTestUtils.setField(uut, "hDWebcastCreateValidator", mockHDWebcastCreateValidator);

    mockWebcastUpdateValidator = EasyMock.createMock(WebcastUpdateValidator.class);
    ReflectionTestUtils.setField(uut, "webcastUpdateValidator", mockWebcastUpdateValidator);

    mockFormatter = EasyMock.createMock(DateTimeFormatter.class);

    mockAuditQueue = createMock(AuditQueue.class);
    ReflectionTestUtils.setField(uut, "auditQueue", mockAuditQueue);

    mockWebcastAuditRecordDtoBuilder = EasyMock.createMock(WebcastAuditRecordDtoBuilder.class);
    ReflectionTestUtils.setField(uut, "auditRecordDtoBuilder", mockWebcastAuditRecordDtoBuilder);

  }

  @Test
  public void testGetWebcastDetailsByChannelIdAndWebcastIdSuccess() {
    // setup
    User user = new User(USER_ID);
    Channel channel = new ChannelBuilder().withDefaultValues().build();
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    WebcastDto expectedDto = new WebcastDto();

    // expectations
    EasyMock.expect(mockChannelFinder.find(new Long(CHANNEL_ID_1))).andReturn(channel);
    EasyMock.expect(mockCommunicationFinder.find(channel, new Long(WEBCAST_ID_1))).andReturn(communication);
    EasyMock.expect(mockWebcastConverter.convert(communication, mockFormatter)).andReturn(expectedDto);

    EasyMock.replay(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    WebcastDto result = uut.get(user, CHANNEL_ID_1, WEBCAST_ID_1, mockFormatter);

    // assertions
    Assert.assertNotNull(result);
    assertEquals(expectedDto, result);
    EasyMock.verify(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testGetWebcastDetailsByChannelIdAndWebcastIdFailsWhenChannelNotFound() {
    // setup
    User user = new User(USER_ID);

    // expectations
    EasyMock.expect(mockChannelFinder.find(new Long(CHANNEL_ID_1))).andThrow(
        new ChannelNotFoundException(new Long(CHANNEL_ID_1)));

    EasyMock.replay(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    uut.get(user, CHANNEL_ID_1, WEBCAST_ID_1, mockFormatter);

    EasyMock.verify(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // assertions
    fail();

  }

  @Test(expected = ResourceNotFoundException.class)
  public void testGetWebcastDetailsByChannelIdAndWebcastIdFailsWhenWebcastNotFound() {
    // setup
    User user = new User(USER_ID);
    Channel channel = new ChannelBuilder().withDefaultValues().build();

    // expectations
    EasyMock.expect(mockChannelFinder.find(new Long(CHANNEL_ID_1))).andReturn(channel);
    EasyMock.expect(mockCommunicationFinder.find(channel, new Long(WEBCAST_ID_1))).andThrow(
        new CommunicationNotFoundException(WEBCAST_ID));

    EasyMock.replay(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    uut.get(user, CHANNEL_ID_1, WEBCAST_ID_1, mockFormatter);

    EasyMock.verify(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // assertions
    fail();

  }

  /**
   * Tests {@link WebcastController#get(User, String, String, DateTimeFormatter)} in the case of supplying invalid
   * format channel Id - invalid number format (not a number).
   */
  @Test
  public void getWebcastDetailsWhenChannelIdInvalidNumber() {
    // Setup -
    String testChannelId = "test1234";
    User user = new User(USER_ID);

    // Do test -
    try {
      uut.get(user, testChannelId, WEBCAST_ID_1, mockFormatter);
    } catch (InvalidParamException ipe) {
      assertEquals(ErrorCode.INVALID_CHANNEL_ID.getName(), ipe.getUserErrorCode());
    }
  }

  /**
   * Tests {@link WebcastController#get(User, String, String, DateTimeFormatter)} in the case of supplying invalid
   * format channel Id - negative number.
   */
  @Test
  public void getWebcastDetailsWhenChannelIdNegativeNumber() {
    // Setup -
    String testChannelId = "-1";
    User user = new User(USER_ID);

    // Do test -
    try {
      uut.get(user, testChannelId, WEBCAST_ID_1, mockFormatter);
    } catch (InvalidParamException ipe) {
      assertEquals(ErrorCode.INVALID_CHANNEL_ID.getName(), ipe.getUserErrorCode());
    }
  }

  /**
   * Tests {@link WebcastController#get(User, String, String, DateTimeFormatter)} in the case of supplying invalid
   * format webcast Id - invalid number format (not a number).
   */
  @Test
  public void getWebcastDetailsWhenWebcastIdInvalidNumber() {
    // Setup -
    String testWebcastId = "test1234";
    User user = new User(USER_ID);

    // Do test -
    try {
      uut.get(user, CHANNEL_ID_1, testWebcastId, mockFormatter);
    } catch (InvalidParamException ipe) {
      assertEquals(ErrorCode.INVALID_WEBCAST_ID.getName(), ipe.getUserErrorCode());
    }
  }

  /**
   * Tests {@link WebcastController#get(User, String, String, DateTimeFormatter)} in the case of supplying invalid
   * format webcast Id - negative number.
   */
  @Test
  public void getWebcastDetailsWhenWebcastIdNegativeNumber() {
    // Setup -
    String testWebcastId = "-1";
    User user = new User(USER_ID);

    // Do test -
    try {
      uut.get(user, CHANNEL_ID_1, testWebcastId, mockFormatter);
    } catch (InvalidParamException ipe) {
      assertEquals(ErrorCode.INVALID_WEBCAST_ID.getName(), ipe.getUserErrorCode());
    }
  }

  @Test
  public void createCommunicationSuccess() throws Exception {
    // setup
    User user = new User(USER_ID);
    Channel channel = new ChannelBuilder().withDefaultValues().build();
    WebcastDto returnedWebcastDto = new WebcastDto();
    returnedWebcastDto.setId(WEBCAST_ID);
    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).build();
    AuditRecordDto auditRecordDto = new WebcastAuditRecordDtoBuilder().create(user.getId(), CHANNEL_ID,
        returnedWebcastDto.getId(), null);

    // expectations
    mockHDWebcastCreateValidator.validate(isA(WebcastDto.class));
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(channel);
    EasyMock.expect(mockWebcastConverter.convert(returnedWebcastDto)).andReturn(communication);
    EasyMock.expect(mockCommunicationCreateService.create(user, channel, communication)).andReturn(WEBCAST_ID);
    EasyMock.expect(mockCommunicationFinder.find(channel, WEBCAST_ID)).andReturn(communication);
    EasyMock.expect(mockWebcastConverter.convert(communication, mockFormatter)).andReturn(returnedWebcastDto);

    EasyMock.expect(
        mockWebcastAuditRecordDtoBuilder.create(user.getId(), CHANNEL_ID, returnedWebcastDto.getId(),
            null)).andReturn(auditRecordDto);

    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockHDWebcastCreateValidator, mockChannelFinder, mockCommunicationCreateService,
        mockCommunicationFinder, mockWebcastConverter, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // do test
    WebcastDto webcastDto = uut.create(user, CHANNEL_ID, returnedWebcastDto, mockFormatter);

    // assertions
    Assert.assertNotNull(webcastDto);
    Assert.assertEquals(WEBCAST_ID, webcastDto.getId());
    EasyMock.verify(mockHDWebcastCreateValidator, mockChannelFinder, mockCommunicationCreateService,
        mockCommunicationFinder, mockWebcastConverter, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  @Test(expected = BadRequestException.class)
  public void createCommunicationThrowsBadRequestExceptionWhenChannelIdsAreDifferent() throws Exception {
    // setup
    User user = new User();
    ChannelPublicDto channelPublicDto = new ChannelPublicDto();
    channelPublicDto.setId(123L);
    WebcastDto returnedWebcastDto = new WebcastDto();
    returnedWebcastDto.setId(WEBCAST_ID);
    returnedWebcastDto.setChannel(channelPublicDto);

    // expectations

    // do test
    uut.create(user, CHANNEL_ID, returnedWebcastDto, mockFormatter);

    // assertions
  }

  @Test(expected = InvalidWebcastException.class)
  public void createCommunicationThrowsInvalidWebcastExceptionWhenValidationFails() throws Exception {
    // setup
    User user = new User();
    WebcastDto returnedWebcastDto = new WebcastDto();
    returnedWebcastDto.setId(WEBCAST_ID);

    // expectations
    mockHDWebcastCreateValidator.validate(isA(WebcastDto.class));
    EasyMock.expectLastCall().andThrow(new InvalidWebcastException("Validation Failed", "InvalidWebcastException"));

    EasyMock.replay(mockHDWebcastCreateValidator, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // do test
    uut.create(user, CHANNEL_ID, returnedWebcastDto, mockFormatter);

    // assertions
    EasyMock.verify(mockHDWebcastCreateValidator, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void createCommunicationThrowsNotFoundExceptionWhenChannelHasNotBeenFound() throws Exception {
    // setup
    User user = new User();
    WebcastDto returnedWebcastDto = new WebcastDto();
    returnedWebcastDto.setId(WEBCAST_ID);

    // expectations
    mockHDWebcastCreateValidator.validate(isA(WebcastDto.class));
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andThrow(new ChannelNotFoundException(CHANNEL_ID));

    EasyMock.replay(mockHDWebcastCreateValidator, mockChannelFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // do test
    WebcastDto webcastDto = uut.create(user, CHANNEL_ID, returnedWebcastDto, mockFormatter);

    // assertions
    Assert.assertNotNull(webcastDto);
    Assert.assertEquals(WEBCAST_ID, webcastDto.getId());
    EasyMock.verify(mockHDWebcastCreateValidator, mockChannelFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  /**
   * Test when updating a communication is successful.
   * 
   * @throws Exception
   */
  @Test
  public void updateCommunicationSuccess() throws Exception {
    // setup
    User user = new User(USER_ID);

    Channel channel = new ChannelBuilder().withDefaultValues().build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).build();
    communication.setStatus(Communication.Status.PENDING);

    // expectations
    mockWebcastUpdateValidator.validate(isA(WebcastDto.class));
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(channel);
    EasyMock.expect(mockWebcastConverter.convert(webcastDto)).andReturn(communication);
    EasyMock.expect(mockCommunicationUpdateService.update(user, channel, communication)).andReturn(WEBCAST_ID);
    EasyMock.expect(mockCommunicationFinder.find(channel, WEBCAST_ID)).andReturn(communication);
    EasyMock.expect(mockWebcastConverter.convert(communication, mockFormatter)).andReturn(webcastDto);

    EasyMock.replay(mockWebcastUpdateValidator, mockChannelFinder, mockCommunicationUpdateService,
        mockCommunicationCancelService, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    WebcastDto returnedWebcastDto = uut.update(user, CHANNEL_ID, WEBCAST_ID, webcastDto, mockFormatter);

    // assertions
    Assert.assertNotNull(returnedWebcastDto);
    Assert.assertEquals(WEBCAST_ID, returnedWebcastDto.getId());
    EasyMock.verify(mockWebcastUpdateValidator, mockChannelFinder, mockCommunicationUpdateService,
        mockCommunicationCancelService, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);
  }

  /**
   * Test when updating a communication to the cancelled state is successful.
   * 
   * @throws Exception
   */
  @Test
  public void updateCommunicationToCancelledStatusSuccess() throws Exception {
    // setup
    User user = new User(USER_ID);

    Channel channel = new ChannelBuilder().withDefaultValues().build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(Communication.Status.CANCELLED.name());

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).build();
    communication.setStatus(Communication.Status.CANCELLED);

    AuditRecordDto auditRecordDto = new WebcastAuditRecordDtoBuilder().cancel(user.getId(), CHANNEL_ID,
        webcastDto.getId(), CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION);

    // expectations
    mockWebcastUpdateValidator.validate(isA(WebcastDto.class));
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(channel);
    EasyMock.expect(mockWebcastConverter.convert(webcastDto)).andReturn(communication);
    EasyMock.expect(mockCommunicationCancelService.cancel(communication)).andReturn(WEBCAST_ID);
    EasyMock.expect(mockCommunicationFinder.find(channel, WEBCAST_ID)).andReturn(communication);
    EasyMock.expect(mockWebcastConverter.convert(communication, mockFormatter)).andReturn(webcastDto);

    EasyMock.expect(
        mockWebcastAuditRecordDtoBuilder.cancel(user.getId(), CHANNEL_ID, webcastDto.getId(),
            CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION)).andReturn(auditRecordDto);

    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockWebcastUpdateValidator, mockChannelFinder, mockCommunicationUpdateService,
        mockCommunicationCancelService, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    WebcastDto returnedWebcastDto = uut.update(user, CHANNEL_ID, WEBCAST_ID, webcastDto, mockFormatter);

    // assertions
    Assert.assertNotNull(returnedWebcastDto);
    Assert.assertEquals(WEBCAST_ID, returnedWebcastDto.getId());
    Assert.assertEquals(Communication.Status.CANCELLED.name(), webcastDto.getStatus());
    EasyMock.verify(mockWebcastUpdateValidator, mockChannelFinder, mockCommunicationUpdateService,
        mockCommunicationCancelService, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);
  }

  /**
   * Test updating a communication that will fail because the channel ID specified is different to the communication's
   * channel ID.
   * 
   * @throws Exception
   */
  @Test(expected = BadRequestException.class)
  public void updateCommunicationFailureWhenChannelIdsAreDifferent() throws Exception {
    // setup
    User user = new User(USER_ID);

    ChannelPublicDto channelPublicDto = new ChannelPublicDto();
    channelPublicDto.setId(123L);

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setChannel(channelPublicDto);

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).build();
    communication.setStatus(Communication.Status.CANCELLED);

    // expectations
    EasyMock.replay(mockWebcastUpdateValidator, mockChannelFinder, mockCommunicationUpdateService,
        mockCommunicationCancelService, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    uut.update(user, CHANNEL_ID, WEBCAST_ID, webcastDto, mockFormatter);

    // assertions
    EasyMock.verify(mockWebcastUpdateValidator, mockChannelFinder, mockCommunicationUpdateService,
        mockCommunicationCancelService, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);
  }
}
