/**
 * **************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastsTotalsConverterTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * **************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.dto.WebcastsTotalsDto;

public class WebcastsTotalsConverterTest {

  private WebcastsTotalsConverter uut;

  @Before
  public void setUp() {

    uut = new WebcastsTotalsConverter();
  }

  @Test
  public void convertShouldConvertToWebcatsTotalsDtoWhenTotalsExistingForAllStatuses() {

    LinkedHashMap<String, Long> webcatsTotalsMap = new LinkedHashMap<String, Long>();
    webcatsTotalsMap.put(Communication.Status.LIVE.toString().toLowerCase(), 3L);
    webcatsTotalsMap.put(Communication.Status.PROCESSING.toString().toLowerCase(), 7L);
    webcatsTotalsMap.put(Communication.Status.RECORDED.toString().toLowerCase(), 21L);
    webcatsTotalsMap.put(Communication.Status.UPCOMING.toString().toLowerCase(), 9L);

    WebcastsTotalsDto webcastTotalsDto = uut.convert(webcatsTotalsMap);

    assertEquals(new Long(3), webcastTotalsDto.getLive());
    assertEquals(new Long(7), webcastTotalsDto.getProcessing());
    assertEquals(new Long(21), webcastTotalsDto.getRecorded());
    assertEquals(new Long(9), webcastTotalsDto.getUpcoming());
  }

  @Test
  public void convertShouldConvertToWebcatsTotalsDtoWhenTotalsExistingNotForAllStatuses() {

    LinkedHashMap<String, Long> webcatsTotalsMap = new LinkedHashMap<String, Long>();
    webcatsTotalsMap.put(Communication.Status.LIVE.toString().toLowerCase(), 3L);
    webcatsTotalsMap.put(Communication.Status.UPCOMING.toString().toLowerCase(), 9L);

    WebcastsTotalsDto webcastTotalsDto = uut.convert(webcatsTotalsMap);

    assertEquals(new Long(3), webcastTotalsDto.getLive());
    assertEquals(new Long(9), webcastTotalsDto.getUpcoming());
  }
}