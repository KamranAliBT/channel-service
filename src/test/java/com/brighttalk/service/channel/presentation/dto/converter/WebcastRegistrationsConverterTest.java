/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastRegistrationsConverterTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.PaginatedListWithTotalPages;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.registrations.WebcastRegistrationDto;
import com.brighttalk.service.channel.presentation.dto.registrations.WebcastRegistrationsDto;

public class WebcastRegistrationsConverterTest {

  private static final String LINK_BASE = "http://local.brighttalk.com/linkBase";

  private WebcastRegistrationsConverter uut;

  private DateTimeFormatter mockFormatter;

  private ConverterContext converterContext;

  @Before
  public void setUp() throws Exception {

    uut = new WebcastRegistrationsConverter();
    mockFormatter = EasyMock.createMock(DateTimeFormatter.class);

    converterContext = new ConverterContext();
    converterContext.setDateTimeFormatter(mockFormatter);
    converterContext.setRequestUrl(new URL(LINK_BASE));
  }

  @Test
  public void convert() {

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    assertNotNull(result);
    assertNull(result.getLinks());
    assertNull(result.getRegistrations());

    final Integer expectedTotal = 0;
    assertEquals(expectedTotal, result.getTotal());
  }

  @Test
  public void convertTotal() {

    final int expectedNumberOfTotals = 9;
    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.setUnpaginatedTotal(expectedNumberOfTotals);

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    assertEquals(expectedNumberOfTotals, result.getTotal(), 0);
  }

  @Test
  public void convertRegistrations() {

    final int unpaginatedTotal = 1;
    final Long channelId = 666L;
    final Long communicationId = 13L;
    final Long registrationId = 1L;
    final Date registrationDate = new Date();
    final String expectedFormattedDate = "expectedFormattedDate";

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    CommunicationRegistration registration = new CommunicationRegistration();
    registration.setId(registrationId);
    registration.setChannelId(channelId);
    registration.setCommunicationId(communicationId);
    registration.setRegistered(registrationDate);

    communicationRegistrations.add(registration);

    // Expectations
    EasyMock.expect(mockFormatter.convert(registrationDate)).andReturn(expectedFormattedDate);
    EasyMock.replay(mockFormatter);

    final int expectedRegistrationsCount = unpaginatedTotal;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    // Assertions
    assertNotNull(result.getRegistrations());
    assertEquals(expectedRegistrationsCount, result.getRegistrations().size(), 0);

    final WebcastRegistrationDto resultRegistration = result.getRegistrations().get(0);

    assertNotNull(resultRegistration);
    assertEquals(registrationId, resultRegistration.getId());

    assertNotNull(resultRegistration.getChannel());
    assertEquals(channelId, resultRegistration.getChannel().getId());

    assertNotNull(resultRegistration.getWebcast());
    assertEquals(communicationId, resultRegistration.getWebcast().getId());

    assertEquals(expectedFormattedDate, resultRegistration.getRegistered());
  }

  @Test
  public void convertMultipleRegistrations() {

    final int unpaginatedTotal = 2;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    CommunicationRegistration registration = new CommunicationRegistration();
    registration.setRegistered(new Date());
    communicationRegistrations.add(registration);

    CommunicationRegistration registration2 = new CommunicationRegistration();
    registration2.setRegistered(new Date());
    communicationRegistrations.add(registration2);

    // Expectations
    EasyMock.expect(mockFormatter.convert(isA(Date.class))).andReturn("").times(2);
    EasyMock.replay(mockFormatter);

    final int expectedRegistrationsCount = unpaginatedTotal;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    // Assertions
    assertNotNull(result.getRegistrations());

    assertEquals(expectedRegistrationsCount, result.getRegistrations().size(), 0);
  }

  @Test
  public void convertValidateLinksOutOfBoundsPage() {

    final int unpaginatedTotal = 2;
    final int pageNumber = 2;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);

    final int expectedLinksCount = 0;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    assertNotNull(result.getLinks());

    assertEquals(expectedLinksCount, result.getLinks().size());
  }

  /**
   * No Pagination links for empty result set
   */
  @Test
  public void convertValidateLinksSinglePageNoCommunications() {

    final int unpaginatedTotal = 0;
    final int pageNumber = 1;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    final List<LinkDto> expectedLinks = null;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    assertEquals(expectedLinks, result.getLinks());
  }

  /**
   * No Pagination links for result set smaller than a single page
   */
  @Test
  public void convertValidateLinksSinglePage() {

    final int unpaginatedTotal = 1;
    final int pageNumber = 1;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    final int expectedLinksCount = 0;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    assertEquals(expectedLinksCount, result.getLinks().size());
  }

  @Test
  public void convertValidateLinksFirstPage() {

    final int unpaginatedTotal = 12;
    final int pageNumber = 1;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    // Expectations
    final int expectedLinksCount = 2;
    final boolean expectFirstLink = false;
    final boolean expectLastLink = true;
    final boolean expectNextLink = true;
    final boolean expectPreviousLink = false;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    // Assertions
    assertEquals(expectedLinksCount, result.getLinks().size());

    assertEquals(expectFirstLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_FIRST));
    assertEquals(expectLastLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_LAST));
    assertEquals(expectNextLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_NEXT));
    assertEquals(expectPreviousLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_PREVIOUS));
  }

  @Test
  public void convertValidateLinksForMiddlePage() {

    final int unpaginatedTotal = 12;
    final int pageNumber = 2;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    // Expectations
    final int expectedLinksCount = 4;
    final boolean expectFirstLink = true;
    final boolean expectLastLink = true;
    final boolean expectNextLink = true;
    final boolean expectPreviousLink = true;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    // Assertions
    assertEquals(expectedLinksCount, result.getLinks().size());

    assertEquals(expectFirstLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_FIRST));
    assertEquals(expectLastLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_LAST));
    assertEquals(expectNextLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_NEXT));
    assertEquals(expectPreviousLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_PREVIOUS));
  }

  @Test
  public void convertValidateLinksForLastPage() {

    final int unpaginatedTotal = 12;
    final int pageNumber = 3;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    // Expectations
    final int expectedLinksCount = 2;
    final boolean expectFirstLink = true;
    final boolean expectLastLink = false;
    final boolean expectNextLink = false;
    final boolean expectPreviousLink = true;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    // Assertions
    assertEquals(expectedLinksCount, result.getLinks().size());

    assertEquals(expectFirstLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_FIRST));
    assertEquals(expectLastLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_LAST));
    assertEquals(expectNextLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_NEXT));
    assertEquals(expectPreviousLink, linksContainsRel(result.getLinks(), LinkDto.RELATIONSHIP_PREVIOUS));
  }

  @Test
  public void convertValidateFirstLink() {
    final int unpaginatedTotal = 12;
    final int pageNumber = 2;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    // Expectations
    final int expectedPageNumber = 1;
    final String expectedLinkUrl = LINK_BASE + "?page=" + expectedPageNumber + "&pageSize=" + pageSize;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    LinkDto resultFirstPageLink = extractLinkRel(result.getLinks(), LinkDto.RELATIONSHIP_FIRST);
    assertEquals(expectedLinkUrl, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_FIRST, resultFirstPageLink.getRel());
    assertNull(resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  @Test
  public void convertValidateLastLink() {
    final int unpaginatedTotal = 12;
    final int pageNumber = 2;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    // Expectations
    final int expectedPageNumber = (int) Math.ceil((double) unpaginatedTotal / (double) pageSize);
    final String expectedLinkUrl = LINK_BASE + "?page=" + expectedPageNumber + "&pageSize=" + pageSize;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    LinkDto resultFirstPageLink = extractLinkRel(result.getLinks(), LinkDto.RELATIONSHIP_LAST);
    assertEquals(expectedLinkUrl, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_LAST, resultFirstPageLink.getRel());
    assertNull(resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  @Test
  public void convertValidateNextLink() {
    final int unpaginatedTotal = 12;
    final int pageNumber = 2;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    // Expectations
    final int expectedPageNumber = pageNumber + 1;
    final String expectedLinkUrl = LINK_BASE + "?page=" + expectedPageNumber + "&pageSize=" + pageSize;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    LinkDto resultFirstPageLink = extractLinkRel(result.getLinks(), LinkDto.RELATIONSHIP_NEXT);
    assertEquals(expectedLinkUrl, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_NEXT, resultFirstPageLink.getRel());
    assertNull(resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  @Test
  public void convertValidatePreviousLink() {
    final int unpaginatedTotal = 12;
    final int pageNumber = 2;
    final int pageSize = 5;

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    // Expectations
    final int expectedPageNumber = pageNumber - 1;
    final String expectedLinkUrl = LINK_BASE + "?page=" + expectedPageNumber + "&pageSize=" + pageSize;

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    LinkDto resultFirstPageLink = extractLinkRel(result.getLinks(), LinkDto.RELATIONSHIP_PREVIOUS);
    assertEquals(expectedLinkUrl, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, resultFirstPageLink.getRel());
    assertNull(resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  @Test
  public void convertValidateLinksCurrentPageWithQueryInBaseUrl() throws Exception {

    final int unpaginatedTotal = 12;
    final int pageNumber = 2;
    final int pageSize = 5;
    final String baseURL = LINK_BASE + "?webcast_status=recorded";

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    // Expectations
    final int expectedPageNumber = pageNumber - 1;
    final String expectedLinkUrl = baseURL + "&page=" + expectedPageNumber + "&pageSize=" + pageSize;

    URL urlRequest = new URL(expectedLinkUrl);
    converterContext.setRequestUrl(urlRequest);

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    LinkDto resultFirstPageLink = extractLinkRel(result.getLinks(), LinkDto.RELATIONSHIP_PREVIOUS);
    assertEquals(expectedLinkUrl, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, resultFirstPageLink.getRel());
    assertNull(resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  @Test
  public void convertValidateLinksCurrentPageWithInvalidPropertiesInRequest() throws Exception {

    final int unpaginatedTotal = 12;
    final int pageNumber = 2;
    final int pageSize = 5;
    final String baseURL = LINK_BASE + "?webcast_status=recorded";

    PaginatedListWithTotalPages<CommunicationRegistration> communicationRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    communicationRegistrations.add(new CommunicationRegistration());

    communicationRegistrations.setPageSize(pageSize);
    communicationRegistrations.setCurrentPageNumber(pageNumber);
    communicationRegistrations.setUnpaginatedTotal(unpaginatedTotal);

    // Expectations
    final int expectedPageNumber = pageNumber - 1;
    final String expectedLinkUrl = baseURL + "&page=" + expectedPageNumber + "&pageSize=" + pageSize;

    URL urlRequest = new URL(expectedLinkUrl + "&someValue=12");
    converterContext.setRequestUrl(urlRequest);

    final Set<String> allowedQueryParamsFilter = new HashSet<String>(Arrays.asList(ApiRequestParam.DATE_TIME_FORMAT,
        ApiRequestParam.PAGE_NUMBER, ApiRequestParam.PAGE_SIZE, "webcast_status"));

    converterContext.setAllowedQueryParamsFilter(allowedQueryParamsFilter);

    // do test
    WebcastRegistrationsDto result = uut.convert(communicationRegistrations, converterContext);

    LinkDto resultFirstPageLink = extractLinkRel(result.getLinks(), LinkDto.RELATIONSHIP_PREVIOUS);
    assertEquals(expectedLinkUrl, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, resultFirstPageLink.getRel());
    assertNull(resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  private boolean linksContainsRel(final List<LinkDto> links, final String relType) {

    for (LinkDto linkDto : links) {
      if (linkDto.getRel().equals(relType)) {
        return true;
      }
    }
    return false;
  }

  private LinkDto extractLinkRel(final List<LinkDto> links, final String relType) {
    for (LinkDto linkDto : links) {
      if (linkDto.getRel().equals(relType)) {
        return linkDto;
      }
    }
    Assert.fail("Can't find link of Rel [" + relType + "]");
    return null;
  }
}