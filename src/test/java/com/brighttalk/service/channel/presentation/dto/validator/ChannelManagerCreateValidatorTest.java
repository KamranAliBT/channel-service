package com.brighttalk.service.channel.presentation.dto.validator;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.error.InvalidChannelManagerException;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;

public class ChannelManagerCreateValidatorTest {

  private static final String EMAIL_ADDRESS = "email@brighttalk.com";

  private ChannelManagerCreateValidator uut;

  @Before
  public void setUp() {

    uut = new ChannelManagerCreateValidator();
  }

  @Test
  public void testWhenManagerEmailAddressProvided() {

    // Set up
    UserDto userDto = new UserDto();
    userDto.setEmail(EMAIL_ADDRESS);

    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setUser(userDto);

    ChannelManagersDto channelManagersDto = new ChannelManagersDto();
    channelManagersDto.addChannelManager(channelManagerDto);

    // Do test
    uut.validate(channelManagersDto);
  }

  @Test(expected = InvalidChannelManagerException.class)
  public void testWhenMissingUser() {

    // Set up
    ChannelManagersDto channelManagersDto = new ChannelManagersDto();
    channelManagersDto.addChannelManager(new ChannelManagerDto());

    // Do test
    uut.validate(channelManagersDto);
  }

  @Test(expected = InvalidChannelManagerException.class)
  public void testWhenMissingEmailAddress() {

    // Set up
    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setUser(new UserDto());

    ChannelManagersDto channelManagersDto = new ChannelManagersDto();
    channelManagersDto.addChannelManager(channelManagerDto);

    // Do test
    uut.validate(channelManagersDto);
  }

}
