/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationAuthorisationControllerTest.java 75656 2014-03-25 12:24:02Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.service.AuthorisationService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;

/**
 * Unit tests for {@link CommunicationAuthorisationController}.
 */
public class CommunicationAuthorisationControllerTest {

  private static final Long USER_ID = 999L;
  private static final Long CHANNEL_ID = 5L;
  private static final Long COMMUNICATION_ID = 15L;

  private CommunicationAuthorisationController uut;

  private ChannelFinder mockChannelFinder;

  private AuthorisationService mockAuthorisationService;

  private CommunicationFinder mockCommunicationFinder;

  @Before
  public void setUp() {

    uut = new CommunicationAuthorisationController();

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    mockAuthorisationService = EasyMock.createMock(AuthorisationService.class);
    mockCommunicationFinder = EasyMock.createMock(CommunicationFinder.class);

    uut.setChannelFinder(mockChannelFinder);
    uut.setAuthorisationService(mockAuthorisationService);
    uut.setCommunicationFinder(mockCommunicationFinder);
  }

  /**
   * Test authorise controller to access communication for successful scenario.
   */
  @Test
  public void testAuthorise() {
    // setup
    User user = createUser();
    Channel channel = new Channel(CHANNEL_ID);
    Communication communication = new Communication(COMMUNICATION_ID);

    // expectations
    EasyMock.expect(mockChannelFinder.find(EasyMock.anyLong())).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockCommunicationFinder.find(COMMUNICATION_ID)).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    mockAuthorisationService.authoriseCommunicationAccess(EasyMock.anyObject(User.class),
        EasyMock.anyObject(Channel.class), EasyMock.anyObject(Communication.class));
    EasyMock.expectLastCall();
    EasyMock.replay(mockAuthorisationService);

    // do test
    uut.authorise(user, COMMUNICATION_ID);

    // assertions
    EasyMock.verify(mockAuthorisationService, mockCommunicationFinder, mockChannelFinder);
  }

  /**
   * Test authorise controller to access communication when communication is not found.
   */
  @Test(expected = UserAuthorisationException.class)
  public void testAuthoriseCommunicationNotFound() {
    // setup
    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(EasyMock.anyLong())).andThrow(
        new CommunicationNotFoundException(COMMUNICATION_ID));
    EasyMock.replay(mockAuthorisationService, mockCommunicationFinder);

    // do test
    uut.authorise(user, COMMUNICATION_ID);

    // assertions
  }

  /**
   * Test authorise controller to access communication when channel is not found.
   */
  @Test(expected = UserAuthorisationException.class)
  public void testAuthoriseCommunicationWhenChannelNotFound() {
    // setup
    User user = createUser();
    Communication communication = new Communication(COMMUNICATION_ID);

    // expectations
    EasyMock.expect(mockChannelFinder.find(EasyMock.anyLong())).andThrow(new ChannelNotFoundException(CHANNEL_ID));
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockCommunicationFinder.find(EasyMock.anyLong())).andReturn(communication);
    EasyMock.replay(mockAuthorisationService, mockCommunicationFinder);

    // do test
    uut.authorise(user, COMMUNICATION_ID);

    // assertions
  }

  private User createUser() {
    User user = new User();
    user.setId(USER_ID);
    return user;
  }
}
