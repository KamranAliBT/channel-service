/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceInternalUpdateValidatorTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.error.InvalidResourceException;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;

/**
 * Unit tests for {@link ResourceExternalUpdateValidator}.
 */
public class ResourceInternalUpdateValidatorTest extends AbstractResourceValidatorTest {

  private ResourceInternalUpdateValidator uut;

  @Before
  public void setUp() {
    uut = new ResourceInternalUpdateValidator(new Long(RESOURCE_ID));

  }

  

 
  /**
   * Test validateInternal of internal file resource Dto when updating title .
   */
  @Test
  public void testValidateAllowInternalFileForResourceDtoWithTitle() {

    // setup

    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, RESOURCE_TITLE_2, null);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateInternal of internal file resource Dto when updating title with empty value .
   */
  @Test
  public void testValidateAllowInternalFileForResourceDtoBeingFileResourceWithEmptyTitle() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, "", null);

    // do test
    try {
      uut.validate(resourceDto);
      Assert.fail();
      // assert
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceTitleMissing", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validateInternal of internal file resource Dto when updating description .
   */
  @Test
  public void testValidateAllowInternalFileForResourceDtoBeingFileResourceWithDescription() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, null, RESOURCE_DESCRIPTION_2);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateInternal of internal file resource Dto when updating ur; .
   */
  @Test
  public void testValidateAllowInternalFileForResourceDtoBeingFileResourceWithUrl() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, null, null);
    resourceDto.getFile().setHref(RESOURCE_URL_2);

    // do test
    try {
      uut.validate(resourceDto);
      // assert

    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateInternal of internal file resource Dto when updating mime type .
   */
  @Test
  public void testValidateAllowInternalFileForResourceDtoBeingResourceWithNotAllowedMimeType() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, null, null);
    resourceDto.getFile().setMimeType(RESOURCE_MIME_TYPE);

    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validateInternal of internal file resource Dto when updating file size .
   */
  @Test
  public void testValidateAllowInternalFileForResourceDtoBeingFileResourceWithSize() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, null, null);
    resourceDto.getFile().setSize(RESOURCE_FILE_SIZE);
    // do test
    try {
      uut.validate(resourceDto);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validation external for resource Dto with invalid hosted value.
   */
  @Test
  public void testValidateAllowInternalFileForResourceDtoBeingResourceWithInvalidHostedValue() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, null, null);
    resourceDto.getFile().setHosted("test");

    // do test
    try {
      uut.validate(resourceDto);
      // assert

    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }
  
  /**
   * Test validation external for resource Dto with invalid hosted value.
   */
  @Test
  public void testValidateAllowInternalFileForResourceDtoBeingResourceWithNullHostedValue() {

    // setup
    ResourceDto resourceDto = createExistingInternalFileResource(RESOURCE_ID, null, null);
    resourceDto.getFile().setHosted(null);

    // do test
    try {
      uut.validate(resourceDto);
      // assert

    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }
}
