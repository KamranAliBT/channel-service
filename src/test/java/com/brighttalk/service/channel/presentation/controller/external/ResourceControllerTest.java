/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceControllerTest.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.easymock.EasyMock.createMock;
import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.service.ResourceService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourceLinkDto;
import com.brighttalk.service.channel.presentation.dto.converter.ResourceConverter;

/**
 * Unit tests for {@link ResourceController}.
 */
public class ResourceControllerTest {

  private static final Long USER_ID = 999L;
  private static final Long CHANNEL_ID = 5L;
  private static final Long COMMUNICATION_ID = 15L;
  private static final Long RESOURCE_ID = 115L;
  private static final String RESOURCE_URL = "http://test.com";
  private static final String RESOURCE_URL_2 = "http://test2.com";
  private static final String RESOURCE_TITLE = "Title test";

  private ResourceController uut;
  
  private ChannelFinder mockChannelFinder;

  private ResourceService mockResourceService;

  private ResourceConverter resourceConverter;

  @Before
  public void setUp() {

    this.uut = new ResourceController();

    this.resourceConverter = new ResourceConverter();
    this.mockChannelFinder = createMock(ChannelFinder.class);
    this.mockResourceService = createMock(ResourceService.class);

    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);
    ReflectionTestUtils.setField(uut, "converter", resourceConverter);
    ReflectionTestUtils.setField(uut, "resourceService", mockResourceService);
  }

  @Test
  public void testGetResource() {
    // setup
    User user = createUser();
    Channel channel = new Channel(CHANNEL_ID);

    Resource resource = new Resource(RESOURCE_ID);

    // expectations
    EasyMock.expect(mockChannelFinder.find(EasyMock.anyLong())).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    // expectations
    EasyMock.expect(
        mockResourceService.findResource(EasyMock.anyObject(User.class), EasyMock.anyObject(Channel.class),
            EasyMock.anyLong(), EasyMock.anyLong())).andReturn(resource);
    EasyMock.replay(mockResourceService);

    // do test
    ResourceDto result = uut.find(user, CHANNEL_ID, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(RESOURCE_ID, result.getId());
  }

  @Test
  public void testCreateResource() {
    // setup
    User user = createUser();
    Channel channel = new Channel(CHANNEL_ID);

    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setTitle(RESOURCE_TITLE);
    ResourceLinkDto resourceLinkDto = new ResourceLinkDto();
    resourceLinkDto.setHref(RESOURCE_URL);
    resourceDto.setLink(resourceLinkDto);

    Resource resource = new Resource(RESOURCE_ID);

    // expectations
    EasyMock.expect(mockChannelFinder.find(EasyMock.anyLong())).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    // expectations
    EasyMock.expect(
        mockResourceService.createResource(EasyMock.anyObject(User.class), EasyMock.anyObject(Channel.class),
            EasyMock.anyLong(), EasyMock.anyObject(Resource.class))).andReturn(resource);
    EasyMock.replay(mockResourceService);

    // do test
    ResourceDto result = uut.create(user, CHANNEL_ID, COMMUNICATION_ID, resourceDto);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(RESOURCE_ID, result.getId());
  }

  @Test
  public void testEditResource() {
    // setup
    User user = createUser();
    Channel channel = new Channel(CHANNEL_ID);

    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);
    ResourceLinkDto resourceLinkDto = new ResourceLinkDto();
    resourceLinkDto.setHref(RESOURCE_URL);
    resourceDto.setLink(resourceLinkDto);
    Resource resource = new Resource(RESOURCE_ID);
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL_2);

    // expectations
    EasyMock.expect(mockChannelFinder.find(EasyMock.anyLong())).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    // expectations
    EasyMock.expect(
        mockResourceService.updateResource(EasyMock.anyObject(User.class), EasyMock.anyObject(Channel.class),
            EasyMock.anyLong(), EasyMock.anyObject(Resource.class))).andReturn(resource);
    EasyMock.replay(mockResourceService);

    // do test
    ResourceDto result = uut.edit(user, CHANNEL_ID, COMMUNICATION_ID, RESOURCE_ID, resourceDto);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_URL_2, result.getLink().getHref());
  }

  @Test(expected = ApplicationException.class)
  public void testEditResourceInvalidResourceId() {
    // setup
    User user = createUser();

    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);

    // do test
    uut.edit(user, CHANNEL_ID, COMMUNICATION_ID, 999999L, resourceDto);

    // assertions
  }

  @Test
  public void testDeleteResource() {
    // setup

    User user = createUser();
    Channel channel = new Channel(CHANNEL_ID);

    // expectations
    EasyMock.expect(mockChannelFinder.find(EasyMock.anyLong())).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    // expectations
    mockResourceService.deleteResource(EasyMock.anyObject(User.class), EasyMock.anyObject(Channel.class),
        EasyMock.anyLong(), EasyMock.anyLong());
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceService);

    // do test
    uut.delete(user, CHANNEL_ID, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    EasyMock.verify(mockResourceService);
  }

  private User createUser() {
    User user = new User();
    user.setId(USER_ID);
    return user;
  }
}
