/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SubscriptionControllerTest.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.easymock.EasyMock.createMock;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.service.channel.ChannelService;

/**
 * Unit tests for {@link SubscriptionController}.
 */
public class SubscriptionControllerTest {

  private static final Long USER_ID = 999L;

  private static final Long CHANNEL_ID = 5L;

  private SubscriptionController uut;

  private ChannelService mockChannelService;

  @Before
  public void setUp() {
    this.uut = new SubscriptionController();
    this.mockChannelService = createMock(ChannelService.class);
    ReflectionTestUtils.setField(uut, "channelService", mockChannelService);
  }

  @Test
  public void unsubscribeUserFromChannel() {
    // setup
    User user = createUser();

    // expectations
    mockChannelService.unsubscribeUserFromChannel(user, CHANNEL_ID);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelService);

    // do test
    uut.delete(user, CHANNEL_ID);

    // assertions
    EasyMock.verify(mockChannelService);
  }

  private User createUser() {
    User user = new User();
    user.setId(USER_ID);
    return user;
  }
}
