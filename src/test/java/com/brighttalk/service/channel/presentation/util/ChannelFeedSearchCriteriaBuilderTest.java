/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelFeedSearchCriteriaBuilderTest.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;

/**
 * Unit tests for {@link ChannelFeedSearchCriteriaBuilder}.
 */
public class ChannelFeedSearchCriteriaBuilderTest {

  private ChannelFeedSearchCriteriaBuilder uut;

  private final Integer DEFAULT_PAGE_SIZE = 5;
  private final Integer DEFAULT_MAX_PAGE_SIZE = 10;
  private final Integer DEFAULT_PAGE_NUMBER = 1;

  @Before
  public void setUp() {

    uut = new ChannelFeedSearchCriteriaBuilder();

    ReflectionTestUtils.setField(uut, "defaultPageSize", DEFAULT_PAGE_SIZE);
    ReflectionTestUtils.setField(uut, "defaultMaxPageSize", DEFAULT_MAX_PAGE_SIZE);
    ReflectionTestUtils.setField(uut, "defaultPageNumber", DEFAULT_PAGE_NUMBER);
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using {@link ChannelFeedSearchCriteriaBuilder##build()}.
   */
  @Test
  public void build() {

    // Set up
    Integer pageSize = null;
    Integer pageNumber = null;
    String filterType = null;
    List<CommunicationStatusFilter> statusFilter = null;

    ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria = buildChannelFeedSearchCriteria(pageSize, pageNumber,
        filterType, statusFilter);

    // Expectations

    // Do test
    ChannelFeedSearchCriteria actualChannelFeedSearchCriteria = uut.build();

    // Assertions
    assertChannelFeedSearchCriteriaAreEqual(expectedChannelFeedSearchCriteria, actualChannelFeedSearchCriteria);
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using
   * {@link ChannelFeedSearchCriteriaBuilder#build(Integer, Integer, String, String)} where all arguments are null.
   */
  @Test
  public void buildChannelFeedSearchCriteriaWithAllNullValues() {

    // Set up
    Integer pageSize = null;
    Integer pageNumber = null;
    String filterType = null;
    List<CommunicationStatusFilter> statusFilter = null;
    String statusFilterAsString = null;

    ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria = buildChannelFeedSearchCriteria(pageSize, pageNumber,
        filterType, statusFilter);

    // Expectations

    // Do test
    ChannelFeedSearchCriteria actualChannelFeedSearchCriteria = uut.build(pageSize, pageNumber, filterType,
        statusFilterAsString);

    // Assertions
    assertChannelFeedSearchCriteriaAreEqual(expectedChannelFeedSearchCriteria, actualChannelFeedSearchCriteria);
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using
   * {@link ChannelFeedSearchCriteriaBuilder#build(Integer, Integer, String, String)} where all arguments are not null.
   */
  @Test
  public void buildChannelFeedSearchCriteriaWithNoNullValues() {

    // Set up
    Integer pageNumber = 1;
    Integer pageSize = 2;
    String filterType = "closesttonow";
    List<CommunicationStatusFilter> statusFilter = Arrays.asList(CommunicationStatusFilter.LIVE,
        CommunicationStatusFilter.UPCOMING);
    String statusFilterAsString = "live,upcoming";

    ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria = buildChannelFeedSearchCriteria(pageNumber, pageSize,
        filterType, statusFilter);

    // Expectations

    // Do test
    ChannelFeedSearchCriteria actualChannelFeedSearchCriteria = uut.build(pageNumber, pageSize, filterType,
        statusFilterAsString);

    // Assertions
    assertChannelFeedSearchCriteriaAreEqual(expectedChannelFeedSearchCriteria, actualChannelFeedSearchCriteria);
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using
   * {@link ChannelFeedSearchCriteriaBuilder#build(Integer, Integer, String, String)} where the status filter list
   * contains an invalid value.
   */
  @Test(expected = SearchCriteriaException.class)
  public void buildChannelFeedSearchCriteriaWithInvalidStatusValue() {

    // Set up
    Integer pageNumber = 1;
    Integer pageSize = 2;
    String filterType = "closesttonow";
    String statusFilterAsString = "live,rubbish";

    // Expectations

    // Do test
    uut.build(pageNumber, pageSize, filterType, statusFilterAsString);

    // Assertions
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using
   * {@link ChannelFeedSearchCriteriaBuilder#buildWithDefaults(Integer, Integer, String, String)} where all arguments
   * are null, allowing them to be defaulted.
   */
  @Test
  public void buildWithDefaultsWhereAllArgumentsAreNull() {

    // Set up
    Integer pageSize = null;
    Integer pageNumber = null;
    String filterType = null;
    List<CommunicationStatusFilter> statusFilter = null;
    String statusFilterAsString = null;

    ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria = buildChannelFeedSearchCriteria(DEFAULT_PAGE_NUMBER,
        DEFAULT_PAGE_SIZE, filterType, statusFilter);

    // Expectations

    // Do test
    ChannelFeedSearchCriteria actualChannelFeedSearchCriteria = uut.buildWithDefaults(pageNumber, pageSize, filterType,
        statusFilterAsString);

    // Assertions
    assertChannelFeedSearchCriteriaAreEqual(expectedChannelFeedSearchCriteria, actualChannelFeedSearchCriteria);
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using
   * {@link ChannelFeedSearchCriteriaBuilder#buildWithDefaults(Integer, Integer, String, String)} where no arguments are
   * null, so none will be defaulted.
   */
  @Test
  public void buildWithDefaultsWhereNoArgumentsAreNull() {

    // Set up
    Integer pageNumber = 1;
    Integer pageSize = 2;
    String filterType = "closesttonow";
    List<CommunicationStatusFilter> statusFilter = Arrays.asList(CommunicationStatusFilter.LIVE,
        CommunicationStatusFilter.UPCOMING);
    String statusFilterAsString = "live,upcoming";

    ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria = buildChannelFeedSearchCriteria(pageNumber, pageSize,
        filterType, statusFilter);

    // Expectations

    // Do test
    ChannelFeedSearchCriteria actualChannelFeedSearchCriteria = uut.buildWithDefaults(pageNumber, pageSize, filterType,
        statusFilterAsString);

    // Assertions
    assertChannelFeedSearchCriteriaAreEqual(expectedChannelFeedSearchCriteria, actualChannelFeedSearchCriteria);
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using
   * {@link ChannelFeedSearchCriteriaBuilder#buildWithDefaults(Integer, Integer, String, String)} where the page size
   * argument exceeds the configured default max page size, which will result in the page size in the criteria being
   * capped to the max page size.
   */
  @Test
  public void buildWithDefaultsWherePageSizeExceedsMaxPageSize() {

    // Set up
    String filterType = null;
    List<CommunicationStatusFilter> statusFilter = null;

    ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria = buildChannelFeedSearchCriteria(DEFAULT_PAGE_NUMBER,
        DEFAULT_PAGE_SIZE, filterType, statusFilter);

    // Expectations

    // Do test
    ChannelFeedSearchCriteria actualChannelFeedSearchCriteria = uut.buildWithDefaults();

    // Assertions
    assertChannelFeedSearchCriteriaAreEqual(expectedChannelFeedSearchCriteria, actualChannelFeedSearchCriteria);
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using
   * {@link ChannelFeedSearchCriteriaBuilder#buildWithDefaults()} where the instance is initialized with defaults.
   */
  @Test
  public void buildWithDefaults() {

    // Set up
    Integer pageNumber = 1;
    Integer pageSize = DEFAULT_MAX_PAGE_SIZE + 1;
    String filterType = "closesttonow";
    List<CommunicationStatusFilter> statusFilter = Arrays.asList(CommunicationStatusFilter.LIVE,
        CommunicationStatusFilter.UPCOMING);
    String statusFilterAsString = "live,upcoming";

    ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria = buildChannelFeedSearchCriteria(pageNumber,
        DEFAULT_MAX_PAGE_SIZE, filterType, statusFilter);

    // Expectations

    // Do test
    ChannelFeedSearchCriteria actualChannelFeedSearchCriteria = uut.buildWithDefaults(pageNumber, pageSize, filterType,
        statusFilterAsString);

    // Assertions
    assertChannelFeedSearchCriteriaAreEqual(expectedChannelFeedSearchCriteria, actualChannelFeedSearchCriteria);
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using
   * {@link ChannelFeedSearchCriteriaBuilder#build(ChannelFeedSearchCriteria)} where the instance is initialised from
   * the supplied instance of {@link ChannelFeedSearchCriteria}.
   */
  @Test
  public void buildWithChannelFeedSearchCriteria() {

    // Set up
    Integer pageNumber = 1;
    Integer pageSize = DEFAULT_MAX_PAGE_SIZE + 1;
    String filterType = "closesttonow";
    List<CommunicationStatusFilter> statusFilter = Arrays.asList(CommunicationStatusFilter.LIVE,
        CommunicationStatusFilter.UPCOMING);

    ChannelFeedSearchCriteria channelFeedSearchCriteria = buildChannelFeedSearchCriteria(pageNumber, pageSize,
        filterType, statusFilter);

    // Expectations

    // Do test
    ChannelFeedSearchCriteria actualChannelFeedSearchCriteria = uut.build(channelFeedSearchCriteria);

    // Assertions
    assertChannelFeedSearchCriteriaAreEqual(channelFeedSearchCriteria, actualChannelFeedSearchCriteria);
  }

  /**
   * Tests building a {@link ChannelFeedSearchCriteria} using
   * {@link ChannelFeedSearchCriteriaBuilder#build(ChannelFeedSearchCriteria)} where an instance will fail to be
   * initialised because the supplied instance of {@link ChannelFeedSearchCriteria} is null.
   */
  @Test(expected = IllegalStateException.class)
  public void buildWithNullChannelFeedSearchCriteria() {

    // Set up

    // Expectations

    // Do test
    uut.build(null);

    // Assertions
  }

  private ChannelFeedSearchCriteria buildChannelFeedSearchCriteria(Integer pageNumber, Integer pageSize,
      String filterType, List<CommunicationStatusFilter> statusFilter) {

    ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria = new ChannelFeedSearchCriteria(pageNumber, pageSize,
        filterType);

    expectedChannelFeedSearchCriteria.setCommunicationStatusFilter(statusFilter);

    return expectedChannelFeedSearchCriteria;
  }

  private void assertChannelFeedSearchCriteriaAreEqual(ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria,
      ChannelFeedSearchCriteria actualChannelFeedSearchCriteria) {

    // If only one is null, then test has failed
    assertFalse(expectedChannelFeedSearchCriteria == null ^ actualChannelFeedSearchCriteria == null);

    assertEquals(expectedChannelFeedSearchCriteria.getPageSize(), actualChannelFeedSearchCriteria.getPageSize());
    assertEquals(expectedChannelFeedSearchCriteria.getPageNumber(), actualChannelFeedSearchCriteria.getPageNumber());
    assertEquals(expectedChannelFeedSearchCriteria.getFilterType(), actualChannelFeedSearchCriteria.getFilterType());
    assertEquals(expectedChannelFeedSearchCriteria.getCommunicationStatusFilter(),
        actualChannelFeedSearchCriteria.getCommunicationStatusFilter());
  }
}
