/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastControllerTest.java 97420 2015-07-01 10:37:21Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.queue.AuditQueue;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationCancelService;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationUpdateService;
import com.brighttalk.service.channel.business.service.communication.VideoCommunicationUpdateService;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.WebcastAuditRecordDtoBuilder;
import com.brighttalk.service.channel.presentation.dto.ChannelPublicDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.WebcastProviderDto;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastConverter;
import com.brighttalk.service.channel.presentation.dto.validator.WebcastUpdateValidator;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

/**
 * Unit tests for {@link WebcastController}.
 */
public class WebcastControllerTest {

  private static final Long CHANNEL_ID = 550L;
  private static final Long WEBCAST_ID = 115L;

  /** Action description for audit record when webcast is cancelled **/
  private static final String CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION =
      "Cancelled through channel (Java) internal API call";

  private WebcastController uut;

  private ChannelFinder mockChannelFinder;

  private CommunicationFinder mockCommunicationFinder;

  private CommunicationUpdateService mockHDCommunicationUpdateService;

  private VideoCommunicationUpdateService mockVideoCommunicationUpdateService;

  private CommunicationCancelService mockCommunicationCancelService;

  private WebcastConverter mockWebcastConverter;

  private DateTimeFormatter mockFormatter;

  private AuditQueue mockAuditQueue;

  private AuditRecordDtoBuilder mockWebcastAuditRecordDtoBuilder;

  private WebcastUpdateValidator mockWebcastUpdateValidator;

  @Before
  public void setUp() {

    uut = new WebcastController();

    mockChannelFinder = createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockCommunicationFinder = createMock(CommunicationFinder.class);
    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);

    mockHDCommunicationUpdateService = createMock(CommunicationUpdateService.class);
    ReflectionTestUtils.setField(uut, "hDCommunicationUpdateService", mockHDCommunicationUpdateService);

    mockVideoCommunicationUpdateService = createMock(VideoCommunicationUpdateService.class);
    ReflectionTestUtils.setField(uut, "videoCommunicationUpdateService", mockVideoCommunicationUpdateService);

    mockCommunicationCancelService = createMock(CommunicationCancelService.class);
    ReflectionTestUtils.setField(uut, "communicationCancelService", mockCommunicationCancelService);

    mockWebcastConverter = EasyMock.createMock(WebcastConverter.class);
    ReflectionTestUtils.setField(uut, "webcastConverter", mockWebcastConverter);

    mockFormatter = EasyMock.createMock(DateTimeFormatter.class);

    mockAuditQueue = EasyMock.createMock(AuditQueue.class);
    ReflectionTestUtils.setField(uut, "auditQueue", mockAuditQueue);

    mockWebcastAuditRecordDtoBuilder = EasyMock.createMock(WebcastAuditRecordDtoBuilder.class);
    ReflectionTestUtils.setField(uut, "auditRecordDtoBuilder", mockWebcastAuditRecordDtoBuilder);

    mockWebcastUpdateValidator = EasyMock.createMock(WebcastUpdateValidator.class);
    ReflectionTestUtils.setField(uut, "webcastUpdateValidator", mockWebcastUpdateValidator);
  }

  @Test
  public void testGetWebcastDetailsByChannelIdAndWebcastIdSuccess() {
    // setup
    Channel channel = new ChannelBuilder().withDefaultValues().build();
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    WebcastDto expectedDto = new WebcastDto();

    // expectations
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(channel);
    EasyMock.expect(mockCommunicationFinder.find(channel, WEBCAST_ID)).andReturn(communication);
    EasyMock.expect(mockWebcastConverter.convert(communication, channel, mockFormatter)).andReturn(expectedDto);

    EasyMock.replay(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    WebcastDto result = uut.get(CHANNEL_ID, WEBCAST_ID, mockFormatter);

    // assertions
    Assert.assertNotNull(result);
    assertEquals(expectedDto, result);
    EasyMock.verify(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);
  }

  @Test
  public void testGetWebcastDetailsByChannelIdAndWebcastIdFailsWhenChannelNotFound() {
    // setup

    // expectations
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andThrow(new NotFoundException("NotFound"));

    EasyMock.replay(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    try {
      uut.get(CHANNEL_ID, WEBCAST_ID, mockFormatter);
    } catch (NotFoundException nfe) {
      EasyMock.verify(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
          mockWebcastAuditRecordDtoBuilder);
      return;
    }

    // assertions
    fail();
  }

  @Test
  public void testGetWebcastDetailsByChannelIdAndWebcastIdFailsWhenWebcastNotFound() {
    // setup
    Channel channel = new ChannelBuilder().withDefaultValues().build();

    // expectations
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(channel);
    EasyMock.expect(mockCommunicationFinder.find(channel, WEBCAST_ID)).andThrow(new NotFoundException("NotFound"));

    EasyMock.replay(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    try {
      uut.get(CHANNEL_ID, WEBCAST_ID, mockFormatter);
    } catch (NotFoundException nfe) {
      EasyMock.verify(mockChannelFinder, mockCommunicationFinder, mockWebcastConverter, mockAuditQueue,
          mockWebcastAuditRecordDtoBuilder);
      return;
    }

    // assertions
    fail();

  }

  @Test
  public void testGetWebcastDetailsByWebcastIdSuccess() {
    // setup
    Channel channel = new ChannelBuilder().withDefaultValues().build();
    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).build();
    WebcastDto expectedDto = new WebcastDto();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(WEBCAST_ID)).andReturn(communication);
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(channel);

    EasyMock.expect(mockWebcastConverter.convert(communication, channel, mockFormatter)).andReturn(expectedDto);

    EasyMock.replay(mockCommunicationFinder, mockChannelFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // do test
    WebcastDto result = uut.get(WEBCAST_ID, mockFormatter);

    // assertions
    Assert.assertNotNull(result);
    assertEquals(expectedDto, result);
    EasyMock.verify(mockCommunicationFinder, mockChannelFinder, mockWebcastConverter, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);
  }

  @Test
  public void testGetWebcastDetailsByWebcastIdFailsWhenWebcastNotFound() {
    // setup

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(WEBCAST_ID)).andThrow(new NotFoundException("NotFound"));

    EasyMock.replay(mockCommunicationFinder, mockWebcastConverter, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // do test
    try {
      uut.get(WEBCAST_ID, mockFormatter);
    } catch (NotFoundException nfe) {
      EasyMock.verify(mockCommunicationFinder, mockWebcastConverter, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
      return;
    }

    // assertions
    fail();

  }

  @Test
  public void testCancelWebcastForUpcomingWebcast() {
    // Set up
    Status communicationStatus = Status.UPCOMING;
    Status requestStatus = Status.CANCELLED;

    Long userId = null;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
        CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).build();

    Communication communicationToUpdate =
        new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
            CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(requestStatus).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());

    // Create audit record that will be expected
    AuditRecordDto auditRecordDto = new WebcastAuditRecordDtoBuilder().cancel(userId, CHANNEL_ID,
        webcastDto.getId(), CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    EasyMock.expect(mockCommunicationCancelService.cancel(communicationToUpdate)).andReturn(WEBCAST_ID);

    // Expect call to create webcast cancellation audit record
    EasyMock.expect(
        mockWebcastAuditRecordDtoBuilder.cancel(userId, CHANNEL_ID, webcastDto.getId(),
            CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION)).andReturn(auditRecordDto);

    // Expect call to the audit queue
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationFinder, mockCommunicationCancelService, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockCommunicationCancelService, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);
  }

  /**
   * Tests cancelling a webcast that is already cancelled. This is not permitted so is expected to fail.
   */
  @Test(expected = BadRequestException.class)
  public void testCancelWebcastForCancelledWebcastFails() {
    // Set up
    Status communicationStatus = Status.CANCELLED;
    Status requestStatus = Status.CANCELLED;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
        CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).build();

    Communication communicationToUpdate =
        new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
            CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(requestStatus).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    EasyMock.expect(mockCommunicationCancelService.cancel(communicationToUpdate)).andReturn(WEBCAST_ID);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationCancelService, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockCommunicationCancelService, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);
  }

  @Test(expected = BadRequestException.class)
  public void testCancelWebcastForLiveWebcastFails() {
    // Set up
    Status communicationStatus = Status.LIVE;
    Status requestStatus = Status.CANCELLED;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
        CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    EasyMock.replay(mockCommunicationFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  @Test(expected = BadRequestException.class)
  public void testCancelWebcastForProcessingWebcastFails() {
    // Set up
    Status communicationStatus = Status.PROCESSING;
    Status requestStatus = Status.CANCELLED;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
        CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  @Test(expected = BadRequestException.class)
  public void testCancelWebcastForRecordingWebcastFails() {
    // Set up
    Status communicationStatus = Status.RECORDED;
    Status requestStatus = Status.CANCELLED;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
        CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  @Test(expected = BadRequestException.class)
  public void testStatusFailsWhenWebcastIdsAreDifferent() {
    // Set up
    Status requestStatus = Status.CANCELLED;

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());

    // expectations

    // exercise
    uut.status(123L, webcastDto);

    // assertions
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testUpdateStatusFailsWhenCommunicationNotFound() {
    // Set up
    Status communicationStatus = Status.RECORDED;
    Status requestStatus = Status.CANCELLED;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
        CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andThrow(
        new ResourceNotFoundException("NotFound", ErrorCode.RESOURCE_NOT_FOUND.getName(), new Object[] { WEBCAST_ID }));
    EasyMock.replay(mockCommunicationFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testUpdateStatusFailsWhenChannelNotFound() {
    // Set up
    Status communicationStatus = Status.UPCOMING;
    Status requestStatus = Status.LIVE;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
        CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andThrow(
        new ResourceNotFoundException("ChannelNotFound", ErrorCode.RESOURCE_NOT_FOUND.getName(),
            new Object[] { CHANNEL_ID }));
    EasyMock.replay(mockCommunicationFinder, mockChannelFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockChannelFinder, mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  @Test
  public void testUpdateWebcastFromUpcomingToLiveWebcast() {
    // Set up
    Status communicationStatus = Status.UPCOMING;
    Status requestStatus = Status.LIVE;
    String provider = Provider.BRIGHTTALK_HD;

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
            CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).withProvider(
            new Provider(provider)).build();

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).build();
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(provider);

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());
    webcastDto.setProvider(webcastProviderDto);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(channel);
    EasyMock.expect(mockWebcastConverter.convertBasicFields(webcastDto)).andReturn(communication);

    mockHDCommunicationUpdateService.updateStatus(isA(Channel.class), isA(Communication.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockChannelFinder, mockHDCommunicationUpdateService, mockWebcastConverter,
        mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockChannelFinder, mockHDCommunicationUpdateService, mockWebcastConverter,
        mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  @Test
  public void testUpdateWebcastFromLiveToProcessingWebcast() {
    // Set up
    Status communicationStatus = Status.LIVE;
    Status requestStatus = Status.PROCESSING;
    String provider = Provider.BRIGHTTALK_HD;

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
            CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).withProvider(
            new Provider(provider)).build();

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).build();
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(provider);

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());
    webcastDto.setProvider(webcastProviderDto);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(channel);
    EasyMock.expect(mockWebcastConverter.convertBasicFields(webcastDto)).andReturn(communication);

    mockHDCommunicationUpdateService.updateStatus(isA(Channel.class), isA(Communication.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockChannelFinder, mockHDCommunicationUpdateService, mockWebcastConverter,
        mockAuditQueue, mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockChannelFinder, mockHDCommunicationUpdateService, mockWebcastConverter,
        mockAuditQueue, mockWebcastAuditRecordDtoBuilder);
  }

  @Test(expected = BadRequestException.class)
  public void updateStatusThrowsExceptionWhenWebcastIdIsNotSet() {
    // Set up
    WebcastDto webcastDto = new WebcastDto();

    // expectations
    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
  }

  @Test(expected = BadRequestException.class)
  public void updateStatusThrowsExceptionWhenStatusIsNotSet() {
    // Set up
    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);

    // expectations
    // exercise
    uut.status(WEBCAST_ID, webcastDto);

    // assertions
  }

  /**
   * Tests cancelling an upcoming webcast.
   */
  @Test
  public void updateStatusWithChannelIdFromUpcomingToCancelled() {
    // Set up
    Status communicationStatus = Status.UPCOMING;
    Status requestStatus = Status.CANCELLED;

    Long userId = null;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
        CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).build();

    Communication communicationToUpdate =
        new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
            CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(requestStatus).build();

    ChannelPublicDto channelPublicDto = new ChannelPublicDto(CHANNEL_ID);

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());
    webcastDto.setChannel(channelPublicDto);

    // Create audit record that will be expected
    AuditRecordDto auditRecordDto = new WebcastAuditRecordDtoBuilder().cancel(userId, CHANNEL_ID,
        webcastDto.getId(), CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    EasyMock.expect(mockCommunicationCancelService.cancel(communicationToUpdate)).andReturn(WEBCAST_ID);

    // Expect call to create webcast cancellation audit record
    EasyMock.expect(
        mockWebcastAuditRecordDtoBuilder.cancel(userId, CHANNEL_ID, webcastDto.getId(),
            CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION)).andReturn(auditRecordDto);

    // Expect call to the audit queue
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationFinder, mockCommunicationCancelService, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(CHANNEL_ID, WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockCommunicationCancelService, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);
  }

  /**
   * Tests cancelling a webcast that is already cancelled. This is not permitted so is expected to fail.
   */
  @Test(expected = BadRequestException.class)
  public void updateStatusWithChannelIdFromCancelledToCancelledFails() {
    // Set up
    Status communicationStatus = Status.CANCELLED;
    Status requestStatus = Status.CANCELLED;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
        CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).build();

    Communication communicationToUpdate =
        new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
            CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(requestStatus).build();

    ChannelPublicDto channelPublicDto = new ChannelPublicDto(CHANNEL_ID);

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());
    webcastDto.setChannel(channelPublicDto);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    EasyMock.expect(mockCommunicationCancelService.cancel(communicationToUpdate)).andReturn(WEBCAST_ID);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationCancelService, mockAuditQueue,
        mockWebcastAuditRecordDtoBuilder);

    // exercise
    uut.status(CHANNEL_ID, WEBCAST_ID, webcastDto);

    // assertions
  }

  @Test(expected = BadRequestException.class)
  public void updateStatusWithChannelIdThrowsExceptionWhenWebcastIdIsNotSet() {
    // Set up
    WebcastDto webcastDto = new WebcastDto();

    // expectations
    // exercise
    uut.status(CHANNEL_ID, WEBCAST_ID, webcastDto);

    // assertions
  }

  @Test(expected = BadRequestException.class)
  public void updateStatusWithChannelIdThrowsExceptionWhenChannelIdIsNotSet() {
    // Set up
    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);

    // expectations
    // exercise
    uut.status(CHANNEL_ID, WEBCAST_ID, webcastDto);

    // assertions
  }

  @Test(expected = BadRequestException.class)
  public void updateStatusWithChannelIdThrowsExceptionWhenStatusIsNotSet() {
    // Set up
    ChannelPublicDto channel = new ChannelPublicDto();
    channel.setId(CHANNEL_ID);

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setChannel(channel);

    // expectations
    // exercise
    uut.status(CHANNEL_ID, WEBCAST_ID, webcastDto);

    // assertions
  }

  /**
   * Tests update webcast.
   * 
   * @throws Exception
   */
  @Test
  public void updateWebcastSuccess() throws Exception {
    // Set up
    Status communicationStatus = Status.PROCESSING;
    Status requestStatus = Status.RECORDED;
    String provider = Provider.VIDEO;
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(provider);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
            CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).withProvider(
            new Provider(provider)).build();

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());
    webcastDto.setProvider(webcastProviderDto);

    // expectations
    EasyMock.expect(mockWebcastConverter.convertBasicFields(webcastDto)).andReturn(communication);

    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    EasyMock.expect(mockChannelFinder.find(communication.getChannelId())).andReturn(channel);

    EasyMock.expect(mockVideoCommunicationUpdateService.update(isA(Channel.class), isA(Communication.class))).andReturn(
        WEBCAST_ID);

    mockWebcastUpdateValidator.validate(isA(WebcastDto.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockWebcastConverter, mockCommunicationFinder, mockChannelFinder,
        mockVideoCommunicationUpdateService, mockWebcastUpdateValidator);

    // exercise
    uut.update(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockWebcastConverter, mockCommunicationFinder, mockChannelFinder,
        mockVideoCommunicationUpdateService, mockWebcastUpdateValidator);
  }

  /**
   * Tests update webcast fails when the webcast id from the payload and request differ.
   * 
   * @throws Exception
   */
  @Test(expected = BadRequestException.class)
  public void updateWebcastFailsForDifferentWebcastIds() throws Exception {
    // Set up
    Status requestStatus = Status.RECORDED;
    String provider = Provider.VIDEO;
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(provider);

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());
    webcastDto.setProvider(webcastProviderDto);

    // expectations

    // exercise
    uut.update(5555L, webcastDto);

    // assertions
  }

  /**
   * Tests update webcast fails when the communication has not been found.
   * 
   * @throws Exception
   */
  @Test(expected = ResourceNotFoundException.class)
  public void updateWebcastFailsWhenWebcastNotFound() throws Exception {
    // Set up
    Status communicationStatus = Status.PROCESSING;
    Status requestStatus = Status.RECORDED;
    String provider = Provider.VIDEO;
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(provider);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
            CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).withProvider(
            new Provider(provider)).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());
    webcastDto.setProvider(webcastProviderDto);

    // expectations
    EasyMock.expect(mockWebcastConverter.convertBasicFields(webcastDto)).andReturn(communication);

    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andThrow(
        new CommunicationNotFoundException(WEBCAST_ID));

    EasyMock.replay(mockWebcastConverter, mockCommunicationFinder);

    // exercise
    uut.update(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockWebcastConverter, mockCommunicationFinder);
  }

  /**
   * Tests update webcast fails when the channel has not been found.
   * 
   * @throws Exception
   */
  @Test(expected = ResourceNotFoundException.class)
  public void updateWebcastFailsWhenChannelNotFound() throws Exception {
    // Set up
    Status communicationStatus = Status.PROCESSING;
    Status requestStatus = Status.RECORDED;
    String provider = Provider.VIDEO;
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(provider);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withId(WEBCAST_ID).withChannelId(
            CHANNEL_ID).withMasterChannelId(CHANNEL_ID).withStatus(communicationStatus).withProvider(
            new Provider(provider)).build();

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(WEBCAST_ID);
    webcastDto.setStatus(requestStatus.name().toLowerCase());
    webcastDto.setProvider(webcastProviderDto);

    // expectations
    EasyMock.expect(mockWebcastConverter.convertBasicFields(webcastDto)).andReturn(communication);

    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    EasyMock.expect(mockChannelFinder.find(communication.getChannelId())).andThrow(
        new ChannelNotFoundException(CHANNEL_ID));

    EasyMock.replay(mockWebcastConverter, mockCommunicationFinder, mockChannelFinder);

    // exercise
    uut.update(WEBCAST_ID, webcastDto);

    // assertions
    EasyMock.verify(mockWebcastConverter, mockCommunicationFinder, mockChannelFinder);
  }
}
