/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationsSearchCriteriaBuilderTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.util.CommunicationsSearchCriteriaBuilder;

public class CommunicationsSearchCriteriaBuilderTest {

  private CommunicationsSearchCriteriaBuilder uut;

  private HttpServletRequest mockRequest;

  @Before
  public void setUp() {

    mockRequest = EasyMock.createMock(HttpServletRequest.class);

    uut = new CommunicationsSearchCriteriaBuilder();
  }

  @Test
  public void buildPageNumber() {

    final String pageNumberString = "10";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.EXPAND)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(pageNumberString);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    CommunicationsSearchCriteria result = uut.build(mockRequest);

    final Integer expectedPageNumber = 10;
    assertEquals(expectedPageNumber, result.getPageNumber());
  }

  @Test
  public void buildPageSize() {

    final String pageSizeString = "2";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.EXPAND)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(pageSizeString);
    EasyMock.replay(mockRequest);

    // do test
    CommunicationsSearchCriteria result = uut.build(mockRequest);

    final Integer expectedPageSize = 2;
    assertEquals(expectedPageSize, result.getPageSize());
  }

  @Test
  public void buildWithSingleId() {

    String ids = "100-10";
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(ids);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.EXPAND)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    CommunicationsSearchCriteria result = uut.build(mockRequest);

    assertEquals(ids, result.getChanelAndCommunicationsIds().get(0));
  }

  @Test
  public void buildWithExpandBeingFeaturedWebcasts() {

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.EXPAND)).andReturn("categories");
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    CommunicationsSearchCriteria result = uut.build(mockRequest);

    assertTrue(result.includeCategories());
  }

}