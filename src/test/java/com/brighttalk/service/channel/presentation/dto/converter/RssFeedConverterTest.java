/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RssFeedConverterTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.PaginatedChannelFeed;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.rss.FeedItemDto;
import com.brighttalk.service.channel.presentation.dto.rss.RssFeedDto;

public class RssFeedConverterTest {

  private static final String CHANNEL_TITLE = "channelTitle";
  
  private static final String CHANNEL_DESCRIPTION = "channelDescription";
  
  private static final String CHANNEL_STRAPLINE = "channelStrapline";
  
  private static final String CHANNEL_URL = "http://www.local.brighttalk.com/channel";
  
  private static final String CHANNEL_FEED_ATOM_URL = "http://www.local.brighttalk.com/channelAtom";

  
  private RssFeedConverter uut;
  
  private PaginatedChannelFeed feed;
  
  private RssFeedItemConverter mockItemConverter;
  
  
  @Before
  public void setUp() {
    
    uut = new RssFeedConverter();
    
    mockItemConverter = EasyMock.createMock( RssFeedItemConverter.class );
    uut.setItemConverter( mockItemConverter );
    
    feed = createChannelFeed();
  }
  
  @Test
  public void convertBase() {
    
    //do test
    RssFeedDto result = uut.convert( feed );
    
    assertEquals( RssFeedConverter.RSS_VERSION, result.getVersion(), 0 );
    assertNotNull( result.getChannel() );
  }
  
  @Test
  public void convertTitle() {
   
    //do test
    RssFeedDto result = uut.convert( feed );
    
    assertEquals( CHANNEL_TITLE, result.getChannel().getTitle() );
  }

  @Test
  public void convertDescription() {
    
    //do test
    RssFeedDto result = uut.convert( feed );
    
    assertEquals( CHANNEL_DESCRIPTION, result.getChannel().getDescription() );
  }

  @Test
  public void convertStrapline() {
    
    //do test
    RssFeedDto result = uut.convert( feed );
    
    assertEquals( CHANNEL_STRAPLINE, result.getChannel().getStrapline() );
  }

  @Test
  public void convertChannelLink() {
    
    //do test
    RssFeedDto result = uut.convert( feed );
    
    assertEquals( CHANNEL_URL, result.getChannel().getChannelLink() );
  }

  @Test
  public void convertAtomLink() {
    
    //do test
    RssFeedDto result = uut.convert( feed );
    
    LinkDto resultAtomLink = result.getChannel().getAtomLink();
    assertNotNull( resultAtomLink );
    assertEquals( CHANNEL_FEED_ATOM_URL, resultAtomLink.getHref() );
    assertEquals( LinkDto.RELATIONSHIP_SELF, resultAtomLink.getRel() );
    assertEquals( LinkDto.TYPE_ATOM_XML, resultAtomLink.getType() );
  }
  
  @Test
  public void convertItems() {
    
    List<Communication> communications = new ArrayList<Communication>();
    communications.add( new Communication() );
    communications.add( new Communication() );
    
    feed.setAllCommunications( communications );
    
    EasyMock.expect( mockItemConverter.convert( isA( Communication.class ) ) ).andReturn( new FeedItemDto() ).times( 2 );
    EasyMock.replay( mockItemConverter );
    
    //do test
    RssFeedDto result = uut.convert( feed );
    
    assertNotNull( result.getChannel().getItems() );
    
    final int expectedItemsCount = 2;
    assertEquals( expectedItemsCount, result.getChannel().getItems().size() );
    
    EasyMock.verify( mockItemConverter );
  }
  
  private PaginatedChannelFeed createChannelFeed() {
    
    Channel channel = new Channel();
    channel.setTitle( CHANNEL_TITLE );
    channel.setDescription( CHANNEL_DESCRIPTION );
    channel.setStrapline( CHANNEL_STRAPLINE );
    
    channel.setUrl( CHANNEL_URL );
    channel.setFeedUrlAtom( CHANNEL_FEED_ATOM_URL );
    
    PaginatedChannelFeed feed = new PaginatedChannelFeed( channel );
    
    return feed;
  }
  
}