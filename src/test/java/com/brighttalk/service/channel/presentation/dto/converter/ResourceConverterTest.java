/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceConverterTest.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.Entity;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourceFileDto;
import com.brighttalk.service.channel.presentation.dto.ResourceLinkDto;
import com.brighttalk.service.channel.presentation.dto.ResourcesDto;

/**
 * Tests {link ResourceConverter}.
 */
public class ResourceConverterTest {

  private static final Long RESOURCE_ID = 5L;

  private static final String RESOURCE_TITLE = "Resource Test Title";

  private static final String RESOURCE_DESCRIPTION = "Resource Test Description";

  private static final String RESOURCE_URL = "http://test.brighttalk.net/resource_005.pdf";

  private static final Long RESOURCE_SIZE = 1000L;

  private static final String RESOURCE_MIME_TYPE = "application/pdf";

  private ResourceConverter uut;

  @Before
  public void setUp() {

    uut = new ResourceConverter();
  }

  @Test
  public void testConvertResourceLinkToResourceDto() {
    // set up
    Resource resource = new Resource(RESOURCE_ID);
    resource.setTitle(RESOURCE_TITLE);
    resource.setDescription(RESOURCE_DESCRIPTION);
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL);

    // do test
    ResourceDto result = uut.convert(resource);

    // assertions
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getLink().getHref());
  }

  @Test
  public void testConvertResourceFileInternalToResourceDto() {
    // set up
    Resource resource = new Resource(RESOURCE_ID);
    resource.setTitle(RESOURCE_TITLE);
    resource.setDescription(RESOURCE_DESCRIPTION);
    resource.setType(Resource.Type.FILE);
    resource.setHosted(Resource.Hosted.INTERNAL);
    resource.setUrl(RESOURCE_URL);
    resource.setSize(RESOURCE_SIZE);
    resource.setMimeType(RESOURCE_MIME_TYPE);

    // do test
    ResourceDto result = uut.convert(resource);

    // assertions
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getFile().getHref());
    Assert.assertEquals(RESOURCE_SIZE, result.getFile().getSize());
    Assert.assertEquals(RESOURCE_MIME_TYPE, result.getFile().getMimeType());
    Assert.assertEquals("internal", result.getFile().getHosted());
  }

  @Test
  public void testConvertResourceFileInternalToResourceDtoSizeZero() {
    // set up
    Resource resource = new Resource(RESOURCE_ID);
    resource.setTitle(RESOURCE_TITLE);
    resource.setDescription(RESOURCE_DESCRIPTION);
    resource.setType(Resource.Type.FILE);
    resource.setHosted(Resource.Hosted.INTERNAL);

    // do test
    ResourceDto result = uut.convert(resource);

    // assertions
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertNull(result.getFile().getHref());
    Assert.assertNull(result.getFile().getSize());
    Assert.assertNull(result.getFile().getMimeType());
  }

  @Test
  public void testConvertResourceFileExternalToResourceDto() {
    // set up
    Resource resource = new Resource(RESOURCE_ID);
    resource.setTitle(RESOURCE_TITLE);
    resource.setDescription(RESOURCE_DESCRIPTION);
    resource.setType(Resource.Type.FILE);
    resource.setHosted(Resource.Hosted.EXTERNAL);
    resource.setUrl(RESOURCE_URL);
    resource.setSize(RESOURCE_SIZE);
    resource.setMimeType(RESOURCE_MIME_TYPE);

    // do test
    ResourceDto result = uut.convert(resource);

    // assertions
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getFile().getHref());
    Assert.assertEquals(RESOURCE_SIZE, result.getFile().getSize());
    Assert.assertEquals(RESOURCE_MIME_TYPE, result.getFile().getMimeType());
    Assert.assertEquals("external", result.getFile().getHosted());

  }

  @Test
  public void testConvertForCreateResourceDtoFileInternalToResource() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setTitle(RESOURCE_TITLE);
    resourceDto.setDescription(RESOURCE_DESCRIPTION);

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setHref(RESOURCE_URL);
    resourceFileDto.setSize(RESOURCE_SIZE);
    resourceFileDto.setMimeType(RESOURCE_MIME_TYPE);
    resourceFileDto.setHosted("internal");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForCreate(resourceDto);

    // assertions
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getUrl());
    Assert.assertEquals(Resource.Type.FILE, result.getType());
    Assert.assertTrue(result.isHostedInternal());
    Assert.assertEquals(RESOURCE_SIZE, result.getSize());
    Assert.assertEquals(RESOURCE_MIME_TYPE, result.getMimeType());
  }

  @Test
  public void testConvertForCreateResourceDtoFileExternalToResource() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setTitle(RESOURCE_TITLE);
    resourceDto.setDescription(RESOURCE_DESCRIPTION);

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setHref(RESOURCE_URL);
    resourceFileDto.setSize(RESOURCE_SIZE);
    resourceFileDto.setMimeType(RESOURCE_MIME_TYPE);
    resourceFileDto.setHosted("external");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForCreate(resourceDto);

    // assertions
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getUrl());
    Assert.assertEquals(Resource.Type.FILE, result.getType());
    Assert.assertTrue(result.isHostedExternal());
    Assert.assertEquals(RESOURCE_SIZE, result.getSize());
    Assert.assertEquals(RESOURCE_MIME_TYPE, result.getMimeType());
  }

  @Test
  public void testConvertForCreateResourceDtoLink() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setTitle(RESOURCE_TITLE);
    resourceDto.setDescription(RESOURCE_DESCRIPTION);

    ResourceLinkDto resourceLinkDto = new ResourceLinkDto();
    resourceLinkDto.setHref(RESOURCE_URL);
    resourceDto.setLink(resourceLinkDto);

    // do test
    Resource result = uut.convertForCreate(resourceDto);

    // assertions
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getUrl());
    Assert.assertEquals(Resource.Type.LINK, result.getType());
  }

  @Test
  public void testConvertForCreateForResourceDtoWithEmptyDescription() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setDescription("");

    // do test
    Resource result = uut.convertForCreate(resourceDto);

    // assertions
    Assert.assertEquals(null, result.getDescription());
  }

  @Test
  public void testConvertForCreateForResourceDtoWithNullDescription() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setDescription(null);

    // do test
    Resource result = uut.convertForCreate(resourceDto);

    // assertions
    Assert.assertEquals(null, result.getDescription());
  }

  @Test
  public void testConvertForCreateForResourceFileDtoWithEmptyMimeType() {
    // set up
    ResourceDto resourceDto = new ResourceDto();

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setMimeType("");
    resourceFileDto.setHosted("internal");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForCreate(resourceDto);

    // assertions
    Assert.assertNull(result.getMimeType());
  }

  @Test
  public void testConvertForCreateForResourceFileDtoWithNullMimeType() {
    // set up
    ResourceDto resourceDto = new ResourceDto();

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setMimeType(null);
    resourceFileDto.setHosted("internal");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForCreate(resourceDto);

    // assertions
    Assert.assertNull(result.getMimeType());
  }

  @Test
  public void testConvertForCreateResourceDtoFileWithEmptyValuesIndicator() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);
    resourceDto.setTitle("");
    resourceDto.setDescription("");

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setHref("");
    resourceFileDto.setMimeType("");
    resourceFileDto.setHosted("external");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForCreate(resourceDto);

    // assertions
    Assert.assertNull(result.getTitle());
    Assert.assertNull(result.getDescription());
    Assert.assertNull(result.getUrl());
    Assert.assertNull(result.getMimeType());
  }

  @Test
  public void testConvertForUpdateForResourceDtoFileInternal() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);
    resourceDto.setTitle(RESOURCE_TITLE);
    resourceDto.setDescription(RESOURCE_DESCRIPTION);

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setHref(RESOURCE_URL);
    resourceFileDto.setSize(RESOURCE_SIZE);
    resourceFileDto.setMimeType(RESOURCE_MIME_TYPE);
    resourceFileDto.setHosted("internal");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getUrl());
    Assert.assertEquals(Resource.Type.FILE, result.getType());
    Assert.assertEquals(RESOURCE_SIZE, result.getSize());
    Assert.assertEquals(RESOURCE_MIME_TYPE, result.getMimeType());
  }

  @Test
  public void testConvertForUpdateForResourceDtoFileExternal() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);
    resourceDto.setTitle(RESOURCE_TITLE);
    resourceDto.setDescription(RESOURCE_DESCRIPTION);

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setHref(RESOURCE_URL);
    resourceFileDto.setSize(RESOURCE_SIZE);
    resourceFileDto.setMimeType(RESOURCE_MIME_TYPE);
    resourceFileDto.setHosted("external");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getUrl());
    Assert.assertEquals(Resource.Type.FILE, result.getType());
    Assert.assertTrue(result.isHostedExternal());
    Assert.assertEquals(RESOURCE_SIZE, result.getSize());
    Assert.assertEquals(RESOURCE_MIME_TYPE, result.getMimeType());
  }

  @Test
  public void testConvertForUpdateForResourceDtoLink() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);
    resourceDto.setTitle(RESOURCE_TITLE);
    resourceDto.setDescription(RESOURCE_DESCRIPTION);

    ResourceLinkDto resourceLinkDto = new ResourceLinkDto();
    resourceLinkDto.setHref(RESOURCE_URL);
    resourceDto.setLink(resourceLinkDto);

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getUrl());
    Assert.assertTrue(result.isLink());
  }

  @Test
  public void testConvertForUpdateForResourceDtoWithEmptyDescription() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setDescription("");

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(Entity.DELETE_INDICATOR, result.getDescription());
  }

  @Test
  public void testConvertForUpdateForResourceDtoWithNullDescription() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setDescription(null);

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(null, result.getDescription());
  }

  @Test
  public void testConvertForUpdateForResourceFileDtoWithEmptyMimeType() {
    // set up
    ResourceDto resourceDto = new ResourceDto();

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setMimeType("");
    resourceFileDto.setHosted("internal");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(Entity.DELETE_INDICATOR, result.getMimeType());
  }

  @Test
  public void testConvertForUpdateForResourceFileDtoWithNullMimeType() {
    // set up
    ResourceDto resourceDto = new ResourceDto();

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setMimeType(null);
    resourceFileDto.setHosted("internal");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(null, result.getMimeType());
  }

  @Test
  public void testConvertForUpdateForResourceDtoFileInternalWithSizeZero() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);
    resourceDto.setTitle(RESOURCE_TITLE);
    resourceDto.setDescription(RESOURCE_DESCRIPTION);

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setHosted("internal");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertNull(result.getUrl());
    Assert.assertEquals(Resource.Type.FILE, result.getType());
    Assert.assertEquals(Resource.Hosted.INTERNAL, result.getHosted());
    Assert.assertNull(result.getSize());
    Assert.assertNull(result.getMimeType());
  }

  @Test
  public void testConvertForUpdateForResourceDtoFileWithEmptyValuesIndicator() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);
    resourceDto.setTitle("");
    resourceDto.setDescription("");

    ResourceFileDto resourceFileDto = new ResourceFileDto();
    resourceFileDto.setHref("");
    resourceFileDto.setMimeType("");
    resourceFileDto.setHosted("external");
    resourceDto.setFile(resourceFileDto);

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(Entity.DELETE_INDICATOR, result.getTitle());
    Assert.assertEquals(Entity.DELETE_INDICATOR, result.getDescription());
    Assert.assertEquals(Entity.DELETE_INDICATOR, result.getUrl());
    Assert.assertEquals(Entity.DELETE_INDICATOR, result.getMimeType());
  }

  @Test
  public void testUpdateResourceDtoLinkWithHtmlDescriptionToResource() {
    // set up
    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);
    resourceDto.setTitle(RESOURCE_TITLE);
    resourceDto.setDescription("<script>alert('This is what an alert message looks like.');</script>");

    ResourceLinkDto resourceLinkDto = new ResourceLinkDto();
    resourceLinkDto.setHref(RESOURCE_URL);
    resourceDto.setLink(resourceLinkDto);

    // do test
    Resource result = uut.convertForUpdate(resourceDto);

    // assertions
    Assert.assertEquals(RESOURCE_ID, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals("<script>alert('This is what an alert message looks like.');</script>", result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getUrl());
    Assert.assertEquals(Resource.Type.LINK, result.getType());
  }

  @Test
  public void testConvertResourcesDtoToListOfResource() {
    // set up

    ResourcesDto resourcesDto = new ResourcesDto();
    resourcesDto.setResources(new ArrayList<ResourceDto>());

    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(RESOURCE_ID);

    ResourceLinkDto resourceLinkDto = new ResourceLinkDto();
    resourceLinkDto.setHref(RESOURCE_URL);
    resourceDto.setLink(resourceLinkDto);
    resourcesDto.getResources().add(resourceDto);

    // do test
    List<Resource> result = uut.convertResources(resourcesDto);

    // assertions
    Assert.assertEquals(1, result.size());
    Assert.assertEquals(RESOURCE_ID, result.get(0).getId());
    Assert.assertEquals(RESOURCE_URL, result.get(0).getUrl());
    Assert.assertEquals(Resource.Type.LINK, result.get(0).getType());
  }

  @Test
  public void testConvertEmptyResourcesDtoToListOfResource() {
    // set up
    ResourcesDto resourcesDto = new ResourcesDto();

    // do test
    List<Resource> result = uut.convertResources(resourcesDto);

    // assertions
    Assert.assertEquals(0, result.size());
  }

  @Test
  public void testConvertNullResourcesDtoToListOfResource() {
    // set up
    ResourcesDto resourcesDto = null;

    // do test
    List<Resource> result = uut.convertResources(resourcesDto);

    // assertions
    Assert.assertEquals(0, result.size());
  }

  @Test
  public void testConvertEmptyListOfResourceToResourcesDto() {
    // set up
    List<Resource> resources = new ArrayList<Resource>();

    // do test
    ResourcesDto result = uut.convertResources(resources);

    // assertions
    Assert.assertEquals(0, result.getResources().size());
  }

  @Test
  public void testConvertNullListOfResourceToResourcesDto() {
    // set up
    List<Resource> resources = null;

    // do test
    ResourcesDto result = uut.convertResources(resources);

    // assertions
    Assert.assertEquals(0, result.getResources().size());
  }
  
  @Test
  public void testConvertNullValueToDeleteIndicator() {
    Assert.assertNull(uut.convertStringToDeleteIndicator(null));
  }
  
  @Test
  public void testRealValueToDeleteIndicator() {
    Assert.assertEquals("real value",uut.convertStringToDeleteIndicator("real value"));
  }
  
  @Test
  public void testConvertEmptyValueToDeleteIndicator() {
    Assert.assertEquals(Entity.DELETE_INDICATOR,uut.convertStringToDeleteIndicator(""));
  }
  
  
  @Test
  public void testConvertEmptyStringWithNullValue() {
    Assert.assertNull(uut.convertEmptyStringToNull(null));
  }
  
  @Test
  public void testConvertEmptyStringWithRealValue() {
    Assert.assertEquals("real value",uut.convertEmptyStringToNull("real value"));
  }
  
  @Test
  public void testConvertEmptyStringWithEmptyValue() {
    Assert.assertNull(uut.convertEmptyStringToNull(""));
  }
}
