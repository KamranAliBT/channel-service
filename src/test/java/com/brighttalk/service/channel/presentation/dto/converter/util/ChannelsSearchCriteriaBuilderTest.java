/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelsSearchCriteriaBuilderTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.util.ChannelsSearchCriteriaBuilder;

public class ChannelsSearchCriteriaBuilderTest {

  private ChannelsSearchCriteriaBuilder uut;

  private HttpServletRequest mockRequest;

  @Before
  public void setUp() {

    mockRequest = EasyMock.createMock(HttpServletRequest.class);

    uut = new ChannelsSearchCriteriaBuilder();
  }

  @Test
  public void build() {

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    ChannelsSearchCriteria result = uut.build(mockRequest);

    assertNotNull(result);
  }

  @Test
  public void buildDefaultPageNumber() {

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    ChannelsSearchCriteria result = uut.build(mockRequest);

    assertNull(result.getPageNumber());
  }

  @Test
  public void buildPageNumber() {

    final String pageNumberString = "10";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(pageNumberString);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    ChannelsSearchCriteria result = uut.build(mockRequest);

    final Integer expectedPageNumber = 10;
    assertEquals(expectedPageNumber, result.getPageNumber());
  }

  @Test
  public void buildDefaultPageSize() {

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    ChannelsSearchCriteria result = uut.build(mockRequest);

    assertNull(result.getPageSize());
  }

  @Test
  public void buildPageSize() {

    final String pageSizeString = "2";

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(pageSizeString);
    EasyMock.replay(mockRequest);

    // do test
    ChannelsSearchCriteria result = uut.build(mockRequest);

    final Integer expectedPageSize = 2;
    assertEquals(expectedPageSize, result.getPageSize());
  }

  @Test
  public void buildWithSingleId() {

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn("10");
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    ChannelsSearchCriteria result = uut.build(mockRequest);

    assertEquals(Long.valueOf(10l), result.getChanelIds().get(0));
  }

  @Test
  public void buildWithTwoIds() {

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn("10,12");
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    ChannelsSearchCriteria result = uut.build(mockRequest);

    assertEquals(Long.valueOf(10l), result.getChanelIds().get(0));
    assertEquals(Long.valueOf(12l), result.getChanelIds().get(1));
  }

  @Test
  public void buildWithPrivateWebcastsIncludedisFalse() {

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.IDS)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.replay(mockRequest);

    // do test
    ChannelsSearchCriteria result = uut.build(mockRequest);

    assertFalse(result.includePrivateWebcasts());
  }
}