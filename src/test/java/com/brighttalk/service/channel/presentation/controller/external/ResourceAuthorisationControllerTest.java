/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceAuthorisationControllerTest.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.easymock.EasyMock.createMock;

import java.util.HashMap;
import java.util.Map;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.service.AuthorisationService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;

/**
 * Unit tests for {@link ResourceAuthorisationController}.
 */
public class ResourceAuthorisationControllerTest {

  private static final Long USER_ID = 999L;
  private static final Long CHANNEL_ID = 5L;
  private static final Long COMMUNICATION_ID = 15L;
  private static final Long RESOURCE_ID = 115L;

  private ResourceAuthorisationController uut;

  private ChannelFinder mockChannelFinder;

  private CommunicationFinder mockCommunicationFinder;

  private AuthorisationService mockAuthorisationService;

  private ResourceDbDao mockResourceDbDao;

  @Before
  public void setUp() {

    uut = new ResourceAuthorisationController();

    mockChannelFinder = createMock(ChannelFinder.class);
    mockAuthorisationService = createMock(AuthorisationService.class);
    mockCommunicationFinder = createMock(CommunicationFinder.class);
    mockResourceDbDao = createMock(ResourceDbDao.class);

    uut.setChannelFinder(mockChannelFinder);
    uut.setAuthorisationService(mockAuthorisationService);
    uut.setCommunicationFinder(mockCommunicationFinder);
    uut.setResourceDbDao(mockResourceDbDao);
  }

  /**
   * Test authorise controller to access resource for successful scenario.
   */
  @Test
  public void testAuthorise() {
    // setup
    User user = createUser();
    Channel channel = new Channel(CHANNEL_ID);
    Communication communication = new Communication(COMMUNICATION_ID);

    Map<String, Long> details = new HashMap<String, Long>();
    details.put("communicationId", COMMUNICATION_ID);
    details.put("channelId", CHANNEL_ID);

    // expectations
    EasyMock.expect(mockResourceDbDao.findChannelIdAndCommunicationId(EasyMock.anyLong())).andReturn(details);
    EasyMock.replay(mockResourceDbDao);

    // expectations
    EasyMock.expect(mockChannelFinder.find(EasyMock.anyLong())).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockCommunicationFinder.find(EasyMock.anyObject(Channel.class), EasyMock.anyLong())).andReturn(
        communication);
    mockAuthorisationService.authoriseCommunicationAccess(EasyMock.anyObject(User.class),
        EasyMock.anyObject(Channel.class), EasyMock.anyObject(Communication.class));
    EasyMock.expectLastCall();
    EasyMock.replay(mockAuthorisationService, mockCommunicationFinder);

    // do test
    uut.authorise(user, RESOURCE_ID);

    // assertions
    EasyMock.verify(mockAuthorisationService, mockCommunicationFinder, mockResourceDbDao, mockChannelFinder);
  }

  /**
   * Test authorise controller to access resource when resource is not found.
   */
  @Test(expected = UserAuthorisationException.class)
  public void testAuthoriseNotFound() {
    // setup
    User user = createUser();

    // expectations
    EasyMock.expect(mockResourceDbDao.findChannelIdAndCommunicationId(EasyMock.anyLong())).andThrow(
        new NotFoundException("test"));
    EasyMock.replay(mockResourceDbDao);

    // do test
    uut.authorise(user, RESOURCE_ID);

    // assertions
  }

  private User createUser() {
    User user = new User();
    user.setId(USER_ID);
    return user;
  }
}
