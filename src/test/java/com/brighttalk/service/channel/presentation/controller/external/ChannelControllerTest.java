/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelControllerTest.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.easymock.EasyMock.createMock;
import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.ChannelUpdateAuthorisationException;
import com.brighttalk.service.channel.business.error.InvalidChannelException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.error.UserNotFoundException;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.channel.ChannelService;
import com.brighttalk.service.channel.presentation.dto.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelConverter;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateChannel;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;
import com.brighttalk.service.utils.DomainFactoryUtils;

/**
 * Unit tests for {@link ChannelController}.
 */
public class ChannelControllerTest {

  private static final Long USER_ID = 999L;

  private static final Long CHANNEL_ID = 5L;

  private ChannelController uut;

  private ChannelFinder mockChannelFinder;

  private ChannelService mockChannelService;

  private BrighttalkValidator mockBrighttalkValidator;

  private DateTimeFormatter mockDateTimeFormatter;

  private ChannelConverter mockChannelConverter;

  @Before
  public void setUp() {
    uut = new ChannelController();

    mockDateTimeFormatter = createMock(DateTimeFormatter.class);

    mockChannelConverter = createMock(ChannelConverter.class);
    ReflectionTestUtils.setField(uut, "channelConverter", mockChannelConverter);

    mockChannelFinder = createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockChannelService = createMock(ChannelService.class);
    ReflectionTestUtils.setField(uut, "channelService", mockChannelService);

    mockBrighttalkValidator = createMock(BrighttalkValidator.class);
    ReflectionTestUtils.setField(uut, "validator", mockBrighttalkValidator);
  }

  @Test
  public void testFindChannel() {
    // setup
    User user = createUser();
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();
    ChannelDto channelDto = new ChannelDto();

    // expectations
    EasyMock.expect(mockChannelFinder.find(user, CHANNEL_ID)).andReturn(channel);
    EasyMock.expect(mockChannelConverter.convert(channel, mockDateTimeFormatter)).andReturn(channelDto);
    EasyMock.replay(mockChannelConverter, mockChannelFinder);

    // do test
    ChannelDto result = uut.find(user, CHANNEL_ID, mockDateTimeFormatter);

    // assertions
    Assert.assertEquals(channelDto, result);

    EasyMock.verify(mockChannelFinder, mockChannelConverter);
  }

  @Test(expected = UserAuthorisationException.class)
  public void testFindChannelForNotExistingChannel() {
    // setup
    User user = createUser();

    // expectations
    EasyMock.expect(mockChannelFinder.find(user, CHANNEL_ID)).andThrow(new NotFoundException("test"));
    EasyMock.replay(mockChannelFinder);

    // do test
    uut.find(user, CHANNEL_ID, mockDateTimeFormatter);

    // assertions
  }

  @Test
  public void testUpdateChannel() {
    // Set up
    User user = createUser();
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();
    ChannelDto channelDto = new ChannelDto();

    // Expectations
    mockBrighttalkValidator.validate(channelDto, UpdateChannel.class);
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelConverter.convert(channelDto)).andReturn(channel);
    EasyMock.expect(mockChannelService.update(user, channel)).andReturn(channel);
    EasyMock.expect(mockChannelConverter.convert(channel, mockDateTimeFormatter)).andReturn(channelDto);
    EasyMock.replay(mockBrighttalkValidator, mockChannelConverter, mockChannelService);

    // Do test
    ChannelDto result = uut.update(user, CHANNEL_ID, channelDto, mockDateTimeFormatter);

    // Assertions
    Assert.assertEquals(channelDto, result);
    EasyMock.verify(mockBrighttalkValidator, mockChannelConverter, mockChannelService);
  }

  @Test(expected = ValidationException.class)
  public void testUpdateChannelWithInvalidChannel() {
    // Set up
    User user = createUser();
    ChannelDto channelDto = new ChannelDto();
    String errorMessage = StringUtils.EMPTY;
    String errorUserMessage = StringUtils.EMPTY;

    // Expectations
    mockBrighttalkValidator.validate(channelDto, UpdateChannel.class);
    EasyMock.expectLastCall().andThrow(new ValidationException(errorMessage, errorUserMessage));
    EasyMock.replay(mockBrighttalkValidator);

    // Do test
    ChannelDto result = uut.update(user, CHANNEL_ID, channelDto, mockDateTimeFormatter);

    // Assertions
    Assert.assertEquals(channelDto, result);
    EasyMock.verify(mockBrighttalkValidator);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testUpdateWhenChannelNotFound() {
    // Set up
    User user = createUser();
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();
    ChannelDto channelDto = new ChannelDto();

    // Expectations
    mockBrighttalkValidator.validate(channelDto, UpdateChannel.class);
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelConverter.convert(channelDto)).andReturn(channel);
    EasyMock.expect(mockChannelService.update(user, channel)).andThrow(new ChannelNotFoundException(CHANNEL_ID));
    EasyMock.replay(mockBrighttalkValidator, mockChannelConverter, mockChannelService);

    // Do test
    ChannelDto result = uut.update(user, CHANNEL_ID, channelDto, mockDateTimeFormatter);

    // Assertions
    Assert.assertEquals(channelDto, result);
    EasyMock.verify(mockBrighttalkValidator, mockChannelConverter, mockChannelService);
  }

  @Test(expected = BadRequestException.class)
  public void testUpdateWhenChannelTitleInUse() {
    // Set up
    User user = createUser();
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();
    ChannelDto channelDtoV2 = new ChannelDto();
    String errorMessage = StringUtils.EMPTY;
    String errorCode = StringUtils.EMPTY;

    // Expectations
    mockBrighttalkValidator.validate(channelDtoV2, UpdateChannel.class);
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelConverter.convert(channelDtoV2)).andReturn(channel);
    EasyMock.expect(mockChannelService.update(user, channel)).andThrow(
        new InvalidChannelException(errorMessage, errorCode));
    EasyMock.replay(mockBrighttalkValidator, mockChannelConverter, mockChannelService);

    // Do test
    ChannelDto result = uut.update(user, CHANNEL_ID, channelDtoV2, mockDateTimeFormatter);

    // Assertions
    Assert.assertEquals(channelDtoV2, result);
    EasyMock.verify(mockBrighttalkValidator, mockChannelConverter, mockChannelService);
  }

  @Test(expected = ResourceNotFoundException.class)
  public void testUpdateWhenChannelUserNotFound() {
    // Set up
    User user = createUser();
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();
    ChannelDto channelDtoV2 = new ChannelDto();

    // Expectations
    mockBrighttalkValidator.validate(channelDtoV2, UpdateChannel.class);
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelConverter.convert(channelDtoV2)).andReturn(channel);
    EasyMock.expect(mockChannelService.update(user, channel)).andThrow(new UserNotFoundException(user.getId()));
    EasyMock.replay(mockBrighttalkValidator, mockChannelConverter, mockChannelService);

    // Do test
    ChannelDto result = uut.update(user, CHANNEL_ID, channelDtoV2, mockDateTimeFormatter);

    // Assertions
    Assert.assertEquals(channelDtoV2, result);
    EasyMock.verify(mockBrighttalkValidator, mockChannelConverter, mockChannelService);
  }

  @Test(expected = NotAuthorisedException.class)
  public void testUpdateWhenUserNotAuthorisedToUpdate() {
    // Set up
    User user = createUser();
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();
    ChannelDto channelDtoV2 = new ChannelDto();

    // Expectations
    mockBrighttalkValidator.validate(channelDtoV2, UpdateChannel.class);
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelConverter.convert(channelDtoV2)).andReturn(channel);
    EasyMock.expect(mockChannelService.update(user, channel)).andThrow(
        new ChannelUpdateAuthorisationException(user.getId()));
    EasyMock.replay(mockBrighttalkValidator, mockChannelConverter, mockChannelService);

    // Do test
    ChannelDto result = uut.update(user, CHANNEL_ID, channelDtoV2, mockDateTimeFormatter);

    // Assertions
    Assert.assertEquals(channelDtoV2, result);
    EasyMock.verify(mockBrighttalkValidator, mockChannelConverter, mockChannelService);
  }

  @Test(expected = NotAuthorisedException.class)
  public void testUpdateWhenUserNotAuthorised() {
    // Set up
    User user = createUser();
    Channel channel = DomainFactoryUtils.createChannelWithNoOptionalFields();
    ChannelDto channelDtoV2 = new ChannelDto();
    String errorMessage = StringUtils.EMPTY;

    // Expectations
    mockBrighttalkValidator.validate(channelDtoV2, UpdateChannel.class);
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelConverter.convert(channelDtoV2)).andReturn(channel);
    EasyMock.expect(mockChannelService.update(user, channel)).andThrow(new UserAuthorisationException(errorMessage));
    EasyMock.replay(mockBrighttalkValidator, mockChannelConverter, mockChannelService);

    // Do test
    ChannelDto result = uut.update(user, CHANNEL_ID, channelDtoV2, mockDateTimeFormatter);

    // Assertions
    Assert.assertEquals(channelDtoV2, result);
    EasyMock.verify(mockBrighttalkValidator, mockChannelConverter, mockChannelService);
  }

  private User createUser() {
    User user = new User();
    user.setId(USER_ID);
    return user;
  }
}
