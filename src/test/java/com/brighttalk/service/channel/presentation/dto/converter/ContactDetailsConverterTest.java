/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContactDetailsConverterTest.java 88863 2015-01-27 11:35:19Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.presentation.dto.ContactDetailsPublicDto;
import com.brighttalk.service.utils.DomainFactoryUtils;

/**
 * Unit Tests for {@link ChannelConverter}.
 */
public class ContactDetailsConverterTest {

  private ContactDetailsConverter uut;

  @Before
  public void setUp() throws Exception {
    uut = new ContactDetailsConverter();
  }

  @Test
  public void convertIdForPublic() {
    
    // Set up
    ContactDetails contactDetails = DomainFactoryUtils.createContactDetails();
    
    // Expectations
    
    // Do test
    ContactDetailsPublicDto contactDetailsPublicDto = uut.convertForPublic(contactDetails);
    
    // Assertions
    assertContactDetailsAreEqual(contactDetails, contactDetailsPublicDto);
  }
  
  @Test
  public void convertFromPublic() {
    
    // Set up
    ContactDetailsPublicDto contactDetailsPublicDto = DomainFactoryUtils.createContactDetailsPublicDto();
    
    // Expectations
    
    // Do test
    ContactDetails contactDetails = uut.convertFromPublic(contactDetailsPublicDto);
    
    // Assertions
    assertContactDetailsAreEqual(contactDetailsPublicDto, contactDetails);
  }
  
  private void assertContactDetailsAreEqual(ContactDetails expected, ContactDetailsPublicDto actual) {
    
    assertEquals(expected.getFirstName(), actual.getFirstName());
    assertEquals(expected.getLastName(), actual.getLastName());
    assertEquals(expected.getEmail(), actual.getEmail());
    assertEquals(expected.getTelephone(), actual.getTelephone());
    assertEquals(expected.getJobTitle(), actual.getJobTitle());
    assertEquals(expected.getCompany(), actual.getCompanyName());
    assertEquals(expected.getAddress1(), actual.getAddress1());
    assertEquals(expected.getAddress2(), actual.getAddress2());
    assertEquals(expected.getCity(), actual.getCity());
    assertEquals(expected.getState(), actual.getStateRegion());
    assertEquals(expected.getPostcode(), actual.getPostcode());
    assertEquals(expected.getCountry(), actual.getCountry());
  }
  
  private void assertContactDetailsAreEqual(ContactDetailsPublicDto expected, ContactDetails actual) {
    
    assertEquals(expected.getFirstName(), actual.getFirstName());
    assertEquals(expected.getLastName(), actual.getLastName());
    assertEquals(expected.getEmail(), actual.getEmail());
    assertEquals(expected.getTelephone(), actual.getTelephone());
    assertEquals(expected.getJobTitle(), actual.getJobTitle());
    assertEquals(expected.getCompanyName(), actual.getCompany());
    assertEquals(expected.getAddress1(), actual.getAddress1());
    assertEquals(expected.getAddress2(), actual.getAddress2());
    assertEquals(expected.getCity(), actual.getCity());
    assertEquals(expected.getStateRegion(), actual.getState());
    assertEquals(expected.getPostcode(), actual.getPostcode());
    assertEquals(expected.getCountry(), actual.getCountry());
  }
}
