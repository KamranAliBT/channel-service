/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SurveyControllerTest.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import static org.easymock.EasyMock.createMock;
import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.form.converter.FormConverter;
import com.brighttalk.common.form.domain.Form;
import com.brighttalk.common.form.dto.FormDto;
import com.brighttalk.common.integration.exception.ServiceBadRequestException;
import com.brighttalk.common.integration.exception.ServiceConnectionFailedException;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.error.InvalidRequestingUserIdException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.error.SurveyNotFoundException;
import com.brighttalk.service.channel.business.service.AuthorisationService;
import com.brighttalk.service.channel.business.service.SurveyService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;

/**
 * Unit tests for {@link SurveyController}.
 */
public class SurveyControllerTest {

  private SurveyController uut;
  
  private ChannelFinder mockChannelFinder;

  private CommunicationFinder mockCommunicationFinder;

  private SurveyService mockSurveyService;

  private FormConverter mockConverter;

  private AuthorisationService mockAuthorisationService;

  @Before
  public void setUp() {

    uut = new SurveyController();

    mockChannelFinder = createMock(ChannelFinder.class);
    mockCommunicationFinder = createMock(CommunicationFinder.class);
    mockSurveyService = createMock(SurveyService.class);
    mockConverter = createMock(FormConverter.class);
    mockAuthorisationService = createMock(AuthorisationService.class);

    uut.setChannelFinder(mockChannelFinder);
    uut.setCommunicationFinder(mockCommunicationFinder);
    uut.setSurveyService(mockSurveyService);
    uut.setFormConverter(mockConverter);
    uut.setAuthorisationService(mockAuthorisationService);

  }

  @Test
  public void getSurveyUnauthorisedSurvey() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;
    Long channelId = 99988L;
    Survey survey = new Survey(surveyId, new Form("" + surveyId));
    Survey channelSurvey = new Survey(surveyId, channelId, true);
    FormDto formDto = new FormDto();

    // expectations
    EasyMock.expect(mockSurveyService.findSurvey(surveyId)).andReturn(channelSurvey);
    EasyMock.expect(mockSurveyService.loadSurvey(channelSurvey)).andReturn(survey);
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockConverter.convert(survey.getForm())).andReturn(formDto);
    EasyMock.replay(mockConverter);

    // do test
    FormDto result = uut.getSurvey(requestUserId, surveyId);

    // assertions
    Assert.assertEquals(formDto, result);
    EasyMock.verify(mockConverter, mockSurveyService);

  }

  @Test
  public void getSurveyUnauthorisedSurveyRetrunsNotFoundSurvey() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;

    // expectations
    EasyMock.expect(mockSurveyService.findSurvey(surveyId)).andThrow(new SurveyNotFoundException("not found"));
    EasyMock.replay(mockSurveyService);

    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getSurvey(requestUserId, surveyId);
    } catch (Exception e) {
      // assertions
      Assert.assertTrue(e instanceof SurveyNotFoundException);

    }
    // assertions
    EasyMock.verify(mockConverter, mockSurveyService);

  }

  @Test
  public void getSurveyConnectionErrorReturnsFromSurveyService() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;

    // expectations
    EasyMock.expect(mockSurveyService.findSurvey(surveyId)).andThrow(
        new ServiceConnectionFailedException("Connection error."));
    EasyMock.replay(mockSurveyService);

    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getSurvey(requestUserId, surveyId);
    } catch (Exception e) {
      Assert.assertTrue(e instanceof ServiceConnectionFailedException);

    }

    // assertions
    EasyMock.verify(mockConverter, mockSurveyService);
  }

  @Test
  public void getSurveyClientErrorReturnsFromSurveyService() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;

    // expectations
    EasyMock.expect(mockSurveyService.findSurvey(surveyId)).andThrow(new ServiceBadRequestException("Client error."));
    EasyMock.replay(mockSurveyService);

    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getSurvey(requestUserId, surveyId);
    } catch (Exception e) {
      Assert.assertTrue(e instanceof ServiceBadRequestException);

    }

    // assertions
    EasyMock.verify(mockConverter, mockSurveyService);

  }

  @Test
  public void getSurveyAuthorisedSurvey() {

    // setup
    String requestUserId = "99";
    Long surveyId = 999L;
    Long channelId = 99L;

    Survey survey = new Survey(surveyId, new Form("" + surveyId));
    Survey channelSurvey = new Survey(surveyId, channelId, true);
    Channel channel = new Channel(channelId);
    FormDto formDto = new FormDto();

    // expectations
    EasyMock.expect(mockSurveyService.findSurvey(surveyId)).andReturn(channelSurvey);
    EasyMock.expect(mockSurveyService.loadSurvey(channelSurvey)).andReturn(survey);

    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    mockAuthorisationService.channelByUserId(EasyMock.anyObject(Channel.class), EasyMock.anyObject(User.class));
    EasyMock.expectLastCall();
    EasyMock.replay(mockAuthorisationService);

    EasyMock.expect(mockConverter.convert(survey.getForm())).andReturn(formDto);
    EasyMock.replay(mockConverter);

    // do test
    FormDto result = uut.getSurvey(requestUserId, surveyId);

    // assertions
    Assert.assertEquals(formDto, result);
    EasyMock.verify(mockConverter, mockSurveyService, mockChannelFinder, mockAuthorisationService);

  }

  @Test
  public void getSurveyAuthorisedThrowsException() {

    // setup
    String requestUserId = "9999";
    Long surveyId = 999L;
    Long channelId = 99L;
    Survey channelSurvey = new Survey(surveyId, channelId, true);
    Channel channel = new Channel(channelId);

    // expectations
    EasyMock.expect(mockSurveyService.findSurvey(surveyId)).andReturn(channelSurvey);
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    mockAuthorisationService.channelByUserId(EasyMock.anyObject(Channel.class), EasyMock.anyObject(User.class));
    EasyMock.expectLastCall().andThrow(new UserAuthorisationException("unauthorised user"));
    EasyMock.replay(mockAuthorisationService);

    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getSurvey(requestUserId, surveyId);
    } catch (Exception e) {
      Assert.assertTrue(e instanceof UserAuthorisationException);

    }
    EasyMock.verify(mockConverter, mockSurveyService, mockChannelFinder, mockAuthorisationService);

  }

  @Test(expected = InvalidRequestingUserIdException.class)
  public void getSurveyInvalidRequestingUserIdHeader() {

    // setup
    String requestUserId = "abc";
    Long surveyId = 999L;

    // do test
    uut.getSurvey(requestUserId, surveyId);
  }

  @Test(expected = SurveyNotFoundException.class)
  public void getSurveyNotFoundException() {

    // setup
    String requestUserId = "9999";
    Long surveyId = 999L;
    Long channelId = 99L;
    Survey channelSurvey = new Survey(surveyId, channelId, true);

    // expectations
    EasyMock.expect(mockSurveyService.findSurvey(surveyId)).andReturn(channelSurvey);
    EasyMock.expect(mockChannelFinder.find(channelId)).andThrow(new NotFoundException("no channel"));

    EasyMock.replay(mockSurveyService, mockChannelFinder);

    // do test
    uut.getSurvey(requestUserId, surveyId);
  }

  @Test
  public void getChannelSurveyUnauthorisedUser() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;
    Long channelId = 99L;
    Survey survey = new Survey(surveyId, new Form("" + surveyId));
    Survey channelSurvey = new Survey(surveyId);
    Channel channel = new Channel(channelId);
    channel.setSurvey(channelSurvey);
    FormDto formDto = new FormDto();

    // expectations
    EasyMock.expect(mockSurveyService.loadSurvey(channelSurvey)).andReturn(survey);
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockConverter.convert(survey.getForm())).andReturn(formDto);
    EasyMock.replay(mockConverter);

    // do test
    FormDto result = uut.getChannelSurvey(requestUserId, channelId);

    // assertions
    Assert.assertEquals(formDto, result);
    EasyMock.verify(mockConverter, mockSurveyService);

  }

  @Test
  public void getChannelSurveyChannelIsNotFound() {

    // setup
    String requestUserId = null;
    Long channelId = 99L;

    // expectations
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andThrow(new NotFoundException("channel not Found"));
    EasyMock.replay(mockChannelFinder);

    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getChannelSurvey(requestUserId, channelId);
    } catch (Exception e) {

      // assertions
      Assert.assertTrue(e instanceof NotFoundException);

    }
    EasyMock.verify(mockConverter, mockSurveyService, mockChannelFinder);

  }

  @Test
  public void getChannelSurveyChannelReturnsSurveyNull() {

    // setup
    String requestUserId = null;
    Long channelId = 99L;
    Channel channel = new Channel(channelId);

    // expectations
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getChannelSurvey(requestUserId, channelId);
    } catch (Exception e) {

      // assertions
      Assert.assertTrue(e instanceof SurveyNotFoundException);

    }
    EasyMock.verify(mockConverter, mockSurveyService, mockChannelFinder);

  }

  @Test
  public void getChannelSurveyConnectionErrorReturnsFromSurveyService() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;
    Long channelId = 99L;
    Survey channelSurvey = new Survey(surveyId);
    Channel channel = new Channel(channelId);
    channel.setSurvey(channelSurvey);

    // expectations
    EasyMock.expect(mockSurveyService.loadSurvey(channelSurvey)).andThrow(
        new ServiceConnectionFailedException("Connection error."));
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getChannelSurvey(requestUserId, channelId);
    } catch (Exception e) {
      // assertions

      Assert.assertTrue(e instanceof ServiceConnectionFailedException);

    }

    EasyMock.verify(mockConverter, mockSurveyService, mockConverter);

  }

  @Test
  public void getChannelSurveyClientErrorReturnsFromSurveyService() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;
    Long channelId = 99L;
    Survey channelSurvey = new Survey(surveyId);
    Channel channel = new Channel(channelId);
    channel.setSurvey(channelSurvey);

    // expectations
    EasyMock.expect(mockSurveyService.loadSurvey(channelSurvey)).andThrow(
        new ServiceBadRequestException("Client error."));
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getChannelSurvey(requestUserId, channelId);
    } catch (Exception e) {
      // assertions

      Assert.assertTrue(e instanceof ServiceBadRequestException);

    }

    EasyMock.verify(mockConverter, mockSurveyService, mockConverter);

  }

  @Test
  public void getChannelSurveyAuthorisedUser() {

    // setup
    String requestUserId = "99999";
    Long surveyId = 999L;
    Long channelId = 99L;
    Survey survey = new Survey(surveyId, new Form("" + surveyId));
    Survey channelSurvey = new Survey(surveyId);
    Channel channel = new Channel(channelId);
    channel.setSurvey(channelSurvey);
    FormDto formDto = new FormDto();

    // expectations
    EasyMock.expect(mockSurveyService.loadSurvey(channelSurvey)).andReturn(survey);
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockConverter.convert(survey.getForm())).andReturn(formDto);
    EasyMock.replay(mockConverter);

    mockAuthorisationService.channelByUserId(EasyMock.anyObject(Channel.class), EasyMock.anyObject(User.class));
    EasyMock.expectLastCall();
    EasyMock.replay(mockAuthorisationService);

    // do test
    FormDto result = uut.getChannelSurvey(requestUserId, channelId);

    // assertions
    Assert.assertEquals(formDto, result);
    EasyMock.verify(mockConverter, mockSurveyService, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getChannelSurveyAuthorisedUserThrowsException() {

    // setup
    String requestUserId = "9999";
    Long surveyId = 999L;
    Long channelId = 99L;
    Survey channelSurvey = new Survey(surveyId);
    Channel channel = new Channel(channelId);
    channel.setSurvey(channelSurvey);

    // expectations
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.replay(mockConverter);

    mockAuthorisationService.channelByUserId(EasyMock.anyObject(Channel.class), EasyMock.anyObject(User.class));
    EasyMock.expectLastCall().andThrow(new UserAuthorisationException("unauthorised user"));
    EasyMock.replay(mockAuthorisationService);

    // do test
    try {
      uut.getChannelSurvey(requestUserId, channelId);
    } catch (Exception e) {
      // assertion
      Assert.assertTrue(e instanceof UserAuthorisationException);

    }

    EasyMock.verify(mockConverter, mockSurveyService, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getChannelSurveyInvalidRequestingUserIdHeader() {

    // setup
    String requestUserId = "abc";
    Long channelId = 99L;

    // expectations
    EasyMock.replay(mockSurveyService);
    EasyMock.replay(mockChannelFinder);
    EasyMock.replay(mockConverter);
    EasyMock.replay(mockAuthorisationService);

    // do test
    try {
      uut.getChannelSurvey(requestUserId, channelId);
    } catch (Exception e) {
      // assertion
      Assert.assertTrue(e instanceof InvalidRequestingUserIdException);

    }

    EasyMock.verify(mockConverter, mockSurveyService, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getCommunicationSurveyUnauthorisedUser() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;
    Long channelId = 99L;
    Long communicationId = 9L;
    Survey survey = new Survey(surveyId, new Form("" + surveyId));
    Survey communicationSurvey = new Survey(surveyId);
    Channel channel = new Channel(channelId);

    Communication communication = new Communication(communicationId);
    communication.setConfiguration(new CommunicationConfiguration(communicationId));
    communication.setSurvey(communicationSurvey);

    FormDto formDto = new FormDto();

    // expectations
    EasyMock.expect(mockSurveyService.loadSurvey(communicationSurvey)).andReturn(survey);
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockCommunicationFinder.find(channel, communicationId)).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockConverter.convert(survey.getForm())).andReturn(formDto);
    EasyMock.replay(mockConverter);

    EasyMock.replay(mockAuthorisationService);

    // do test
    FormDto result = uut.getCommunicationSurvey(requestUserId, channelId, communicationId);

    // assertions
    Assert.assertEquals(formDto, result);
    EasyMock.verify(mockConverter, mockSurveyService, mockCommunicationFinder, mockConverter, mockAuthorisationService);
  }

  @Test
  public void getCommunicationSurveyChannelIsNotFound() {

    // setup
    String requestUserId = null;
    Long channelId = 99L;
    Long communicationId = 9L;

    // expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andThrow(new NotFoundException("channel not Found"));
    EasyMock.replay(mockChannelFinder);

    EasyMock.replay(mockSurveyService);
    EasyMock.replay(mockCommunicationFinder);
    EasyMock.replay(mockAuthorisationService);
    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getCommunicationSurvey(requestUserId, channelId, communicationId);
    } catch (Exception e) {

      // assertions
      Assert.assertTrue(e instanceof NotFoundException);

    }
    EasyMock.verify(mockConverter, mockSurveyService, mockCommunicationFinder, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getCommunicationSurveyCommunicationIsNotFound() {

    // setup
    String requestUserId = null;
    Long channelId = 99L;
    Long communicationId = 9L;
    Channel channel = new Channel(channelId);

    // expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockCommunicationFinder.find(channel, communicationId)).andThrow(
        new NotFoundException("communication not Found"));
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.replay(mockSurveyService);
    EasyMock.replay(mockAuthorisationService);
    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getCommunicationSurvey(requestUserId, channelId, communicationId);
    } catch (Exception e) {

      // assertions
      Assert.assertTrue(e instanceof NotFoundException);

    }
    EasyMock.verify(mockConverter, mockSurveyService, mockCommunicationFinder, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getCommunicationSurveyCommunicationReturnsSurveyNull() {

    // setup
    String requestUserId = null;
    Long channelId = 99L;
    Long communicationId = 9L;
    Channel channel = new Channel(channelId);
    Communication communication = new Communication(communicationId);
    communication.setConfiguration(new CommunicationConfiguration(communicationId));
    communication.setSurvey(null);

    // expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockCommunicationFinder.find(channel, communicationId)).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.replay(mockSurveyService);
    EasyMock.replay(mockAuthorisationService);
    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getCommunicationSurvey(requestUserId, channelId, communicationId);
    } catch (Exception e) {
      // assertions
      Assert.assertTrue(e instanceof SurveyNotFoundException);
    }
    EasyMock.verify(mockConverter, mockSurveyService, mockCommunicationFinder, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getCommunicationSurveyConnectionErrorReturnsFromSurveyService() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;
    Long channelId = 99L;
    Long communicationId = 99L;
    Survey communicationSurvey = new Survey(surveyId);
    Channel channel = new Channel(channelId);
    Communication communication = new Communication(communicationId);
    communication.setConfiguration(new CommunicationConfiguration(communicationId));
    communication.setSurvey(communicationSurvey);

    // expectations
    EasyMock.expect(mockSurveyService.loadSurvey(communicationSurvey)).andThrow(
        new ServiceConnectionFailedException("Connection error."));
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockCommunicationFinder.find(channel, communicationId)).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.replay(mockAuthorisationService);
    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getCommunicationSurvey(requestUserId, channelId, communicationId);
    } catch (Exception e) {
      // assertions

      Assert.assertTrue(e instanceof ServiceConnectionFailedException);

    }

    EasyMock.verify(mockConverter, mockSurveyService, mockCommunicationFinder, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getCommunicationSurveyClientErrorReturnsFromSurveyService() {

    // setup
    String requestUserId = null;
    Long surveyId = 999L;
    Long channelId = 99L;
    Long communicationId = 99L;
    Survey communicationSurvey = new Survey(surveyId);
    Channel channel = new Channel(channelId);
    Communication communication = new Communication(communicationId);
    communication.setSurvey(communicationSurvey);

    // expectations
    EasyMock.expect(mockSurveyService.loadSurvey(communicationSurvey)).andThrow(
        new ServiceBadRequestException("Client error."));
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockCommunicationFinder.find(channel, communicationId)).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.replay(mockAuthorisationService);
    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getCommunicationSurvey(requestUserId, channelId, communicationId);
    } catch (Exception e) {
      // assertions

      Assert.assertTrue(e instanceof ServiceBadRequestException);

    }

    EasyMock.verify(mockConverter, mockSurveyService, mockCommunicationFinder, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getCommunicationSurveyAuthorisedUser() {

    // setup
    String requestUserId = "99999";
    Long surveyId = 999L;
    Long channelId = 99L;
    Long communicationId = 99L;
    Survey survey = new Survey(surveyId, new Form("" + surveyId));
    Survey communicationSurvey = new Survey(surveyId);
    Channel channel = new Channel(channelId);
    Communication communication = new Communication(communicationId);
    communication.setSurvey(communicationSurvey);

    FormDto formDto = new FormDto();

    // expectations
    EasyMock.expect(mockSurveyService.loadSurvey(communicationSurvey)).andReturn(survey);
    EasyMock.replay(mockSurveyService);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    EasyMock.expect(mockCommunicationFinder.find(channel, communicationId)).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockConverter.convert(survey.getForm())).andReturn(formDto);
    EasyMock.replay(mockConverter);

    mockAuthorisationService.channelByUserId(EasyMock.anyObject(Channel.class), EasyMock.anyObject(User.class));
    EasyMock.expectLastCall();
    EasyMock.replay(mockAuthorisationService);

    // do test
    FormDto result = uut.getCommunicationSurvey(requestUserId, channelId, communicationId);

    // assertions
    Assert.assertEquals(formDto, result);
    EasyMock.verify(mockConverter, mockSurveyService, mockCommunicationFinder, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getCommunicationSurveyAuthorisedUserThrowsException() {

    // setup
    String requestUserId = "99999";
    Long channelId = 99L;
    Long communicationId = 99L;
    Channel channel = new Channel(channelId);

    // expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder);

    mockAuthorisationService.channelByUserId(EasyMock.anyObject(Channel.class), EasyMock.anyObject(User.class));
    EasyMock.expectLastCall().andThrow(new UserAuthorisationException("unauthorised user"));
    EasyMock.replay(mockAuthorisationService);

    EasyMock.replay(mockSurveyService);
    EasyMock.replay(mockCommunicationFinder);
    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getCommunicationSurvey(requestUserId, channelId, communicationId);
    } catch (Exception e) {
      // assertion
      Assert.assertTrue(e instanceof UserAuthorisationException);
    }

    EasyMock.verify(mockConverter);
    EasyMock.verify(mockConverter, mockSurveyService, mockCommunicationFinder, mockConverter, mockAuthorisationService);

  }

  @Test
  public void getCommunicationSurveyInvalidRequestingUserIdHeader() {

    // setup
    String requestUserId = "abc";
    Long channelId = 99L;
    Long communicationId = 99L;

    // expectations
    EasyMock.replay(mockChannelFinder);
    EasyMock.replay(mockAuthorisationService);
    EasyMock.replay(mockSurveyService);
    EasyMock.replay(mockCommunicationFinder);
    EasyMock.replay(mockConverter);

    // do test
    try {
      uut.getCommunicationSurvey(requestUserId, channelId, communicationId);
    } catch (Exception e) {
      // assertion
      Assert.assertTrue(e instanceof InvalidRequestingUserIdException);
    }

    EasyMock.verify(mockConverter);
    EasyMock.verify(mockConverter, mockSurveyService, mockCommunicationFinder, mockConverter, mockAuthorisationService);

  }

}
