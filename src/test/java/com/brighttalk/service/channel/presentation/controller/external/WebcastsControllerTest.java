/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastsControllerTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.easymock.EasyMock.createMock;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.presentation.dto.converter.context.builder.ExternalConverterContextBuilder;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.presentation.dto.WebcastsPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastsPublicConverter;
import com.brighttalk.service.channel.presentation.util.CommunicationsSearchCriteriaBuilder;

/**
 * Unit tests for {@link WebcastsController}.
 */
public class WebcastsControllerTest {

  private WebcastsController uut;

  private CommunicationFinder mockCommunicationFinder;

  private WebcastsPublicConverter mockWebcastsPublicConverter;

  private ExternalConverterContextBuilder mockContextBuilder;

  private BrighttalkValidator mockValidator;

  private CommunicationsSearchCriteriaBuilder mockCommunicationsSearchCriteriaBuilder;

  @Before
  public void setUp() {
    uut = new WebcastsController();

    mockWebcastsPublicConverter = createMock(WebcastsPublicConverter.class);
    mockCommunicationFinder = createMock(CommunicationFinder.class);
    mockValidator = createMock(BrighttalkValidator.class);
    mockContextBuilder = createMock(ExternalConverterContextBuilder.class);
    mockCommunicationsSearchCriteriaBuilder = createMock(CommunicationsSearchCriteriaBuilder.class);

    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);
    ReflectionTestUtils.setField(uut, "webcastsPublicConverter", mockWebcastsPublicConverter);
    ReflectionTestUtils.setField(uut, "validator", mockValidator);
    ReflectionTestUtils.setField(uut, "contextBuilder", mockContextBuilder);
    ReflectionTestUtils.setField(uut, "communicationsSearchCriteriaBuilder", mockCommunicationsSearchCriteriaBuilder);

  }

  @Test
  public void getPublicWebcastsWhenOneWebcastIsReturned() {
    // setup

    DateTimeFormatter formatter = createMock(DateTimeFormatter.class);
    HttpServletRequest request = createMock(HttpServletRequest.class);

    Communication channel = new CommunicationBuilder().withDefaultValues().build();

    PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(channel);

    CommunicationsSearchCriteria communicationsSearchCriteria = new CommunicationsSearchCriteria();

    // expectations
    EasyMock.expect(mockCommunicationsSearchCriteriaBuilder.build(request)).andReturn(communicationsSearchCriteria);

    mockValidator.validate(communicationsSearchCriteria, ValidationException.class);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockCommunicationFinder.find(communicationsSearchCriteria)).andReturn(communications);

    EasyMock.replay(mockCommunicationsSearchCriteriaBuilder, mockValidator, mockCommunicationFinder, formatter, request);

    // do test
    uut.getPublicWebcasts(request, formatter);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockValidator, mockCommunicationsSearchCriteriaBuilder, request, formatter);
  }

  @Test(expected = ValidationException.class)
  public void getPublicChannelsWhenRequestThrowsValidatonError() {
    // setup

    DateTimeFormatter formatter = createMock(DateTimeFormatter.class);
    HttpServletRequest request = createMock(HttpServletRequest.class);

    Communication channel = new CommunicationBuilder().withDefaultValues().build();

    PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(channel);

    CommunicationsSearchCriteria communicationsSearchCriteria = new CommunicationsSearchCriteria();

    ConverterContext converterContext = new ConverterContext();
    // expectations

    EasyMock.expect(mockCommunicationsSearchCriteriaBuilder.build(request)).andReturn(communicationsSearchCriteria);

    mockValidator.validate(communicationsSearchCriteria, ValidationException.class);
    EasyMock.expectLastCall().andThrow(new ValidationException("test", "usertest"));

    EasyMock.expectLastCall().once();

    EasyMock.expect(mockCommunicationFinder.find(communicationsSearchCriteria)).andReturn(communications);

    EasyMock.expect(mockContextBuilder.build(request, formatter, WebcastsController.ALLOWED_REQUEST_PARAMS)).andReturn(
        converterContext);

    EasyMock.expect(mockWebcastsPublicConverter.convertPublic(communications, converterContext)).andReturn(
        new WebcastsPublicDto());

    EasyMock.replay(mockCommunicationsSearchCriteriaBuilder, mockValidator, mockCommunicationFinder,
        mockContextBuilder, mockWebcastsPublicConverter, formatter, request);

    // do test
    uut.getPublicWebcasts(request, formatter);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockValidator, mockCommunicationsSearchCriteriaBuilder,
        mockContextBuilder, mockWebcastsPublicConverter, request, formatter);

  }

}
