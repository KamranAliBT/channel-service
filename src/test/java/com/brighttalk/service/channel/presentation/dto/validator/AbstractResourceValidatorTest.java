/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AbstractResourceValidatorTest.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourceFileDto;
import com.brighttalk.service.channel.presentation.dto.ResourceLinkDto;

/**
 * Abstract Resource test provides functionality to create various types of resources.
 */
public class AbstractResourceValidatorTest {

  protected static final Long RESOURCE_ID = 5L;

  protected static final Long RESOURCE_ID_2 = 6L;

  protected static final String RESOURCE_URL = "http://test";

  protected static final String RESOURCE_URL_2 = "http://test2";

  protected static final String RESOURCE_TITLE = "Resource Test Title";

  protected static final String RESOURCE_TITLE_2 = "Resource Test Title 2";

  protected static final String RESOURCE_DESCRIPTION = "Resource Test Description";

  protected static final String RESOURCE_DESCRIPTION_2 = "Resource Test Description 2";

  protected static final String RESOURCE_MIME_TYPE = "application/xml";

  protected static final Long RESOURCE_FILE_SIZE = 50L;

  protected ResourceDto createExternalFileResource(String title, String url, String description, String mimeType,
      Long size) {

    ResourceDto resource = new ResourceDto();
    resource.setTitle(title);
    resource.setDescription(description);
    resource.setFile(new ResourceFileDto());

    resource.getFile().setHosted(Resource.Hosted.EXTERNAL.name().toLowerCase());

    resource.getFile().setHref(url);

    resource.getFile().setSize(size);
    resource.getFile().setMimeType(mimeType);

    return resource;
  }

  protected ResourceDto createInternalFileResource(String title, String description) {

    ResourceDto resource = new ResourceDto();
    resource.setTitle(title);
    resource.setDescription(description);
    resource.setFile(new ResourceFileDto());

    resource.getFile().setHosted(Resource.Hosted.INTERNAL.name().toLowerCase());

    return resource;
  }

  protected ResourceDto createLinkResource(String title, String url, String description) {

    ResourceDto resource = new ResourceDto();
    resource.setTitle(title);
    resource.setDescription(description);
    resource.setLink(new ResourceLinkDto());
    resource.getLink().setHref(url);

    return resource;
  }

  protected ResourceDto createExistingInternalFileResource(Long id, String title, String description) {

    ResourceDto resource = createInternalFileResource(title, description);
    resource.setId(id);

    return resource;
  }
  
  protected ResourceDto createExistingExternalFileResource(Long id, String title, String url, String description, String mimeType,
      Long size) {

    ResourceDto resource = createExternalFileResource(title,url, description,mimeType,size);
    resource.setId(id);

    return resource;
  }

  protected ResourceDto createLinkResource(Long id, String title, String url, String description) {

    ResourceDto resource = createLinkResource(title, url, description);
    resource.setId(id);

    return resource;
  }

}
