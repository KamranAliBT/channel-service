package com.brighttalk.service.channel.presentation.controller.external;

import static junit.framework.Assert.assertNotNull;
import static org.easymock.EasyMock.isA;

import java.util.HashSet;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.error.ChannelManagerNotFoundException;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.service.channel.ChannelManagerService;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelManagerConverter;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

public class ChannelManagerControllerTest {

  private static final Long USER_ID = 999L;

  private static final Long CHANNEL_ID = 888L;

  private static final Long CHANNEL_MANAGER_ID = 777L;

  private static final Long CHANNEL_OWNER_ID = 666L;

  private static final boolean ACTIVE = true;

  private static final String EMAIL_ADDRESS = "email@brighttalk.com";

  private ChannelManagerController uut;

  private ChannelManagerService mockChannelManagerService;

  private ChannelManagerConverter mockChannelManagerConverter;

  private Set<Object> mocks;

  @Before
  public void setUp() {

    uut = new ChannelManagerController();

    mockChannelManagerConverter = EasyMock.createMock(ChannelManagerConverter.class);
    mockChannelManagerService = EasyMock.createMock(ChannelManagerService.class);

    ReflectionTestUtils.setField(uut, "converter", mockChannelManagerConverter);
    ReflectionTestUtils.setField(uut, "channelManagerService", mockChannelManagerService);

    mocks = new HashSet<Object>();
  }

  @Test
  public void tesUpdate() {

    // Set up
    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setId(CHANNEL_MANAGER_ID);
    channelManagerDto.setEmailAlerts(ACTIVE);

    com.brighttalk.common.user.User channelOwner = buildChannelOwner();

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelManagerConverter(channelManager);
    expectChannelManagerServiceToUpdate(channelManager);
    expectChannelManagerConverter(new ChannelManagerPublicDto());
    expect();

    // Do test
    ChannelManagerPublicDto result = uut.update(channelOwner, CHANNEL_ID, CHANNEL_MANAGER_ID, channelManagerDto);

    // Assertions
    assertNotNull(result);
    verify();
  }

  @Test(expected = BadRequestException.class)
  public void testUpdateWhenChannelManagerIdsMismatch() {

    // Set up
    Long channelManagerId = 000L;

    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setId(CHANNEL_MANAGER_ID);

    com.brighttalk.common.user.User channelOwner = buildChannelOwner();

    // Expectations

    // Do test
    uut.update(channelOwner, CHANNEL_ID, channelManagerId, channelManagerDto);

    // Assertions
  }

  @Test(expected = ResourceNotFoundException.class)
  public void tesUpdateWhenChannelNotFound() {

    // Set up
    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setId(CHANNEL_MANAGER_ID);
    channelManagerDto.setEmailAlerts(ACTIVE);

    com.brighttalk.common.user.User channelOwner = buildChannelOwner();

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelManagerConverter(channelManager);
    expectChannelManagerServiceToThrowExceptionWhenUpdating(new ChannelNotFoundException(CHANNEL_ID));
    expect();

    // Do test
    uut.update(channelOwner, CHANNEL_ID, CHANNEL_MANAGER_ID, channelManagerDto);

    // Assertions
    verify();
  }

  @Test(expected = ResourceNotFoundException.class)
  public void tesUpdateWhenChannelManagerNotFound() {

    // Set up
    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setId(CHANNEL_MANAGER_ID);
    channelManagerDto.setEmailAlerts(ACTIVE);

    com.brighttalk.common.user.User channelOwner = buildChannelOwner();

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelManagerConverter(channelManager);
    expectChannelManagerServiceToThrowExceptionWhenUpdating(new ChannelManagerNotFoundException(USER_ID));
    expect();

    // Do test
    uut.update(channelOwner, CHANNEL_ID, CHANNEL_MANAGER_ID, channelManagerDto);

    // Assertions
    verify();
  }

  @Test(expected = BadRequestException.class)
  public void tesUpdateWhenChannelManagersfeatureDisabled() {

    // Set up
    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setEmailAlerts(ACTIVE);

    com.brighttalk.common.user.User channelOwner = buildChannelOwner();

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelManagerConverter(channelManager);
    expectChannelManagerServiceToThrowExceptionWhenUpdating(new FeatureNotEnabledException(""));
    expect();

    // Do test
    uut.update(channelOwner, CHANNEL_ID, CHANNEL_MANAGER_ID, channelManagerDto);

    // Assertions
    verify();
  }

  @Test
  public void tesDelete() {

    // Set up
    ChannelManagerPublicDto channelManagerDto = new ChannelManagerPublicDto();
    channelManagerDto.setEmailAlerts(ACTIVE);

    com.brighttalk.common.user.User channelOwner = buildChannelOwner();

    // Expectations
    expectChannelManagerServiceToDelete();
    expect();

    // Do test
    uut.delete(channelOwner, CHANNEL_ID, CHANNEL_MANAGER_ID);

    // Assertions
    verify();
  }

  @Test(expected = ResourceNotFoundException.class)
  public void tesDeleteWhenChannelNotFound() {

    // Set up
    ChannelManagerPublicDto channelManagerDto = new ChannelManagerPublicDto();
    channelManagerDto.setEmailAlerts(ACTIVE);

    com.brighttalk.common.user.User channelOwner = buildChannelOwner();

    // Expectations
    expectChannelManagerServiceToThrowExceptionWhenDeleting(new ChannelNotFoundException(CHANNEL_ID));
    expect();

    // Do test
    uut.delete(channelOwner, CHANNEL_ID, CHANNEL_MANAGER_ID);

    // Assertions
    verify();
  }

  @Test(expected = ResourceNotFoundException.class)
  public void tesDeleteWhenChannelManagerNotFound() {

    // Set up
    ChannelManagerPublicDto channelManagerDto = new ChannelManagerPublicDto();
    channelManagerDto.setEmailAlerts(ACTIVE);

    com.brighttalk.common.user.User channelOwner = buildChannelOwner();

    // Expectations
    expectChannelManagerServiceToThrowExceptionWhenDeleting(new ChannelManagerNotFoundException(CHANNEL_ID));
    expect();

    // Do test
    uut.delete(channelOwner, CHANNEL_ID, CHANNEL_MANAGER_ID);

    // Assertions
    verify();
  }

  @Test(expected = NotAuthorisedException.class)
  public void tesDeleteWhenChannelManagersfeatureDisabled() {

    // Set up
    ChannelManagerPublicDto channelManagerDto = new ChannelManagerPublicDto();
    channelManagerDto.setEmailAlerts(ACTIVE);

    com.brighttalk.common.user.User channelOwner = buildChannelOwner();

    // Expectations
    expectChannelManagerServiceToThrowExceptionWhenDeleting(new FeatureNotEnabledException(""));
    expect();

    // Do test
    uut.delete(channelOwner, CHANNEL_ID, CHANNEL_MANAGER_ID);

    // Assertions
    verify();
  }

  private void expectChannelManagerServiceToUpdate(final ChannelManager channelManager) {
    EasyMock.expect(
        mockChannelManagerService.update(isA(com.brighttalk.common.user.User.class), isA(Long.class),
            isA(ChannelManager.class))).andReturn(channelManager);
    expect(mockChannelManagerService);
  }

  private void expectChannelManagerServiceToThrowExceptionWhenUpdating(final Throwable cause) {
    EasyMock.expect(
        mockChannelManagerService.update(isA(com.brighttalk.common.user.User.class), isA(Long.class),
            isA(ChannelManager.class))).andThrow(cause);
    expect(mockChannelManagerService);
  }

  private void expectChannelManagerServiceToDelete() {
    mockChannelManagerService.delete(isA(com.brighttalk.common.user.User.class), isA(Long.class), isA(Long.class));
    EasyMock.expectLastCall();
    expect(mockChannelManagerService);
  }

  private void expectChannelManagerServiceToThrowExceptionWhenDeleting(final Throwable cause) {
    mockChannelManagerService.delete(isA(com.brighttalk.common.user.User.class), isA(Long.class), isA(Long.class));
    EasyMock.expectLastCall().andThrow(cause);
    expect(mockChannelManagerService);
  }

  private void expectChannelManagerConverter(final ChannelManager channelManager) {
    EasyMock.expect(mockChannelManagerConverter.convert(isA(ChannelManagerDto.class))).andReturn(channelManager);
    expect(mockChannelManagerConverter);
  }

  private void expectChannelManagerConverter(final ChannelManagerPublicDto channelManagerPublicDto) {
    EasyMock.expect(mockChannelManagerConverter.convertForPublic(isA(ChannelManager.class))).andReturn(
        channelManagerPublicDto);
    expect(mockChannelManagerConverter);
  }

  private void expect(final Object obj) {
    mocks.add(obj);
  }

  private void expect() {
    EasyMock.replay(mocks.toArray());
  }

  private void verify() {
    EasyMock.verify(mocks.toArray());
  }

  private ChannelManager buildChannelManager() {

    User user = new User();
    user.setId(USER_ID);
    user.setEmail(EMAIL_ADDRESS);

    ChannelManager channelManager = new ChannelManager();
    channelManager.setId(CHANNEL_MANAGER_ID);
    channelManager.setUser(user);
    channelManager.setEmailAlerts(ACTIVE);

    return channelManager;
  }

  private com.brighttalk.common.user.User buildChannelOwner() {

    com.brighttalk.common.user.User channelOwner = new com.brighttalk.common.user.User();
    channelOwner.setId(CHANNEL_OWNER_ID);

    return channelOwner;
  }

}
