/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelsControllerTest.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.easymock.EasyMock.createMock;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.presentation.dto.converter.context.builder.ExternalConverterContextBuilder;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.PresentationCriteria;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelConverter;
import com.brighttalk.service.channel.presentation.util.ChannelsSearchCriteriaBuilder;
import com.brighttalk.service.channel.presentation.util.PresentationCriteriaBuilder;

/**
 * Unit tests for {@link ChannelsController}.
 */
public class ChannelsControllerTest {

  private static final Long USER_ID = 999L;

  private ChannelsController uut;
  
  private ChannelFinder mockChannelFinder;

  private ChannelConverter mockChannelConverter;

  private ExternalConverterContextBuilder mockContextBuilder;

  private BrighttalkValidator mockValidator;

  private ChannelsSearchCriteriaBuilder mockChannelsSearchCriteriaBuilder;

  private ConverterContext mockConverterContext;

  private DateTimeFormatter mockDateTimeFormatter;

  private HttpServletRequest mockRequest;

  private PresentationCriteriaBuilder mockPresentationCriteriaBuilder;

  @Before
  public void setUp() {
    uut = new ChannelsController();

    mockChannelConverter = createMock(ChannelConverter.class);
    mockChannelFinder = createMock(ChannelFinder.class);
    mockValidator = createMock(BrighttalkValidator.class);
    mockContextBuilder = createMock(ExternalConverterContextBuilder.class);
    mockChannelsSearchCriteriaBuilder = createMock(ChannelsSearchCriteriaBuilder.class);
    mockConverterContext = createMock(ConverterContext.class);
    mockDateTimeFormatter = createMock(DateTimeFormatter.class);
    mockRequest = EasyMock.createMock(HttpServletRequest.class);
    mockPresentationCriteriaBuilder = EasyMock.createMock(PresentationCriteriaBuilder.class);

    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);
    ReflectionTestUtils.setField(uut, "channelConverter", mockChannelConverter);
    ReflectionTestUtils.setField(uut, "validator", mockValidator);
    ReflectionTestUtils.setField(uut, "contextBuilder", mockContextBuilder);
    ReflectionTestUtils.setField(uut, "channelsSearchCriteriaBuilder", mockChannelsSearchCriteriaBuilder);
    ReflectionTestUtils.setField(uut, "presentationCriteriaBuilder", mockPresentationCriteriaBuilder);
  }

  @SuppressWarnings("deprecation")
  @Test
  public void getSubscribedChannelsWhenUserIsAuthorizedAndOneChannelAreReturned() {
    // setup
    User user = createUser();
    Channel channel = new ChannelBuilder().withDefaultValues().build();
    List<Channel> channels = Arrays.asList(channel);

    // expectations
    EasyMock.expect(mockChannelFinder.findSubscribedChannels(user)).andReturn(channels);
    // EasyMock.expect(mockExpandBuilder.buildWithDefaultExpandedFeaturedWebcasts(mockRequest)).andReturn(criteria);

    EasyMock.replay(mockChannelFinder);

    // do test
    uut.getSubscribedChannels(user, mockRequest, mockDateTimeFormatter);

    // assertions
    EasyMock.verify(mockChannelFinder);
  }

  @Test
  public void getPublicChannelsWhenOneChannelAreReturned() {
    // setup
    Channel channel = new ChannelBuilder().withDefaultValues().build();
    PresentationCriteria criteria = createExpandCriteria();
    PaginatedList<Channel> channels = new PaginatedList<Channel>();
    channels.add(channel);

    ChannelsSearchCriteria findChannelsRequestDetails = new ChannelsSearchCriteria();

    // expectations
    EasyMock.expect(mockChannelsSearchCriteriaBuilder.build(mockRequest)).andReturn(findChannelsRequestDetails);

    mockValidator.validate(findChannelsRequestDetails, ValidationException.class);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockPresentationCriteriaBuilder.build(mockRequest)).andReturn(criteria);
    EasyMock.replay(mockPresentationCriteriaBuilder);

    mockValidator.validate(criteria, ValidationException.class);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockChannelFinder.find(findChannelsRequestDetails)).andReturn(channels);

    EasyMock.expect(
        mockContextBuilder.build(mockRequest, mockDateTimeFormatter, ChannelsController.ALLOWED_REQUEST_PARAMS)).andReturn(
        mockConverterContext);

    EasyMock.replay(mockChannelsSearchCriteriaBuilder, mockValidator, mockChannelFinder, mockContextBuilder,
        mockDateTimeFormatter, mockRequest);

    // do test
    uut.getPublicChannels(mockRequest, mockDateTimeFormatter);

    // assertions
    EasyMock.verify(mockChannelFinder, mockValidator, mockChannelsSearchCriteriaBuilder, mockContextBuilder,
        mockRequest, mockDateTimeFormatter, mockPresentationCriteriaBuilder);
  }

  @Test(expected = ValidationException.class)
  public void getPublicChannelsWhenRequestThrowsValidatonError() {
    // setup
    DateTimeFormatter formatter = createMock(DateTimeFormatter.class);
    HttpServletRequest request = createMock(HttpServletRequest.class);

    Channel channel = new ChannelBuilder().withDefaultValues().build();

    PaginatedList<Channel> channels = new PaginatedList<Channel>();
    channels.add(channel);

    ChannelsSearchCriteria findChannelsRequestDetails = new ChannelsSearchCriteria();

    // expectations
    EasyMock.expect(mockChannelsSearchCriteriaBuilder.build(request)).andReturn(findChannelsRequestDetails);

    mockValidator.validate(findChannelsRequestDetails, ValidationException.class);
    EasyMock.expectLastCall().andThrow(new ValidationException("test", "usertest"));

    EasyMock.replay(mockChannelsSearchCriteriaBuilder, mockValidator, mockChannelFinder, mockContextBuilder,
        mockChannelConverter, formatter, request);

    // do test
    uut.getPublicChannels(request, formatter);

    // assertions
    EasyMock.verify(mockChannelFinder, mockValidator, mockChannelsSearchCriteriaBuilder, mockContextBuilder,
        mockChannelConverter, request, formatter);
  }

  private User createUser() {
    User user = new User();
    user.setId(USER_ID);
    return user;
  }

  private PresentationCriteria createExpandCriteria() {
    String featuredWebcasts = "featuredWebcasts";
    PresentationCriteria criteria = new PresentationCriteria();
    criteria.setExpand(featuredWebcasts);
    return criteria;
  }
}
