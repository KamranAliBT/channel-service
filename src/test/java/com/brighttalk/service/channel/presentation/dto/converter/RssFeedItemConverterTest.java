/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RssFeedItemConverterTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.Communication.Format;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.rss.EnclosureDto;
import com.brighttalk.service.channel.presentation.dto.rss.FeedCommunicationDto;
import com.brighttalk.service.channel.presentation.dto.rss.FeedItemDto;
import com.brighttalk.service.channel.presentation.dto.rss.GuidDto;

public class RssFeedItemConverterTest {

  private static final Long COMMUNICATION_ID = 342l;
  
  private static final String COMMUNICATION_TITLE = "communicationTitle";

  private static final String COMMUNICATION_DESCRIPTION = "communicationDescription";

  private static final String PRESENTER = "presenterName";
  
  private static final String COMMUNICATION_URL = "http://www.local.brighttalk.net/urlofCommunication";

  private static final String CALENDAR_URL = "http://www.local.brighttalk.net/calendarUrl";

  private static final int COMMUNICATION_DURATION = 1800; 
  
  private static final String KEYWORD_1 = "keyword1";

  private static final String KEYWORD_2 = "keyword2";
  
  private static final int SCHEDULED_YEAR = 2004;
  
  private static final int SCHEDULED_MONTH = 2;

  private static final String SCHEDULED_MONTH_NAME = "Mar";
  
  private static final int SCHEDULED_DAY = 13;
  
  private static final int SCHEDULED_HOUR = 10;

  private static final int SCHEDULED_MINUTE = 21;
  
  private static final int SCHEDULED_SECOND = 11;
  
  private static final String SCHEDULED_DAY_NAME = "Sat";
  
  private static final Float COMMUNICATION_RATING = 4.36234f;
  
  
  private RssFeedItemConverter uut;
  
  private Communication communication;
  
  @Before
  public void setUp() {

    uut = new RssFeedItemConverter();
    
    communication = createCommunication();
  }
  
  @Test
  public void convertItemId() {
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    assertNotNull( result.getItemId() );
    assertEquals( COMMUNICATION_ID, result.getItemId().getId() );
  }

  @Test
  public void convertTitle() {
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    assertEquals( COMMUNICATION_TITLE, result.getTitle() );
  }

  @Test
  public void convertDescription() {
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    assertEquals( COMMUNICATION_DESCRIPTION, result.getDescription() );
  }

  @Test
  public void convertPublishDate() {
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    assertNotNull( result.getPubDate() );
    
    final String expectedPublishDate = createExpectedPublishedDate();
    assertEquals( expectedPublishDate, result.getPubDate() );
  }
  
  @Test
  public void convertGuid() {
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    GuidDto resultGuidDto = result.getGuid();
    assertNotNull( resultGuidDto );
    assertTrue( resultGuidDto.getIsPermaLink() );
    assertEquals( COMMUNICATION_URL, resultGuidDto.getValue() );
  }
  
  @Test
  public void convertPresenter() {
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    assertEquals( PRESENTER, result.getPresenter() );
  }
  
  @Test
  public void convertCommunicationWhenNoThumbnail() {
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    FeedCommunicationDto resultCommunication = result.getCommunication();
    assertNotNull( resultCommunication );
    assertEquals( COMMUNICATION_ID, resultCommunication.getId() );
    assertNull( resultCommunication.getThumbnailUrl() );
    assertEquals( COMMUNICATION_DURATION, resultCommunication.getDuration() );
    
    final String expectedFormat = Format.AUDIO.toString().toLowerCase();
    assertEquals( expectedFormat, resultCommunication.getFormat() );
    
    final String expectedCommunicationStatus = Status.LIVE.toString().toLowerCase();
    assertEquals( expectedCommunicationStatus, resultCommunication.getStatus() );
    
    final long expectedUtc = communication.getScheduledDateTime().getTime() / 1000;
    assertEquals( expectedUtc, resultCommunication.getUtc() );
    
    final float expectedRating = 4.4f;
    assertEquals( expectedRating, resultCommunication.getRating(), 0 );
  }
  
  @Test 
  public void convertCommunication() {
    
    final String expectedThumbnailUrl = "thumbnailUrl";
    communication.setThumbnailUrl( expectedThumbnailUrl );
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    FeedCommunicationDto resultCommunication = result.getCommunication();
    assertNotNull( resultCommunication );
    assertEquals( COMMUNICATION_ID, resultCommunication.getId() );
    assertEquals( expectedThumbnailUrl, resultCommunication.getThumbnailUrl() );
    assertEquals( COMMUNICATION_DURATION, resultCommunication.getDuration() );
    
    final String expectedFormat = Format.AUDIO.toString().toLowerCase();
    assertEquals( expectedFormat, resultCommunication.getFormat() );
    
    final String expectedCommunicationStatus = Status.LIVE.toString().toLowerCase();
    assertEquals( expectedCommunicationStatus, resultCommunication.getStatus() );
    
    final long expectedUtc = communication.getScheduledDateTime().getTime() / 1000;
    assertEquals( expectedUtc, resultCommunication.getUtc() );
    
    final float expectedRating = 4.4f;
    assertEquals( expectedRating, resultCommunication.getRating(), 0 );
  }
  
  @Test
  public void convertCommunicationLink() {
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    assertEquals( COMMUNICATION_URL, result.getLink() );
  }
  
  @Test
  public void convertEnclosure() {
    
    final String expectedThumbnailUrl = "thumbnailUrl";
    communication.setThumbnailUrl( expectedThumbnailUrl );
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    EnclosureDto enclosure = result.getEnclosure();
    assertNotNull( enclosure );
    
    assertEquals( expectedThumbnailUrl, enclosure.getUrl() );
    assertEquals( LinkDto.TYPE_IMAGE_PNG, enclosure.getType() );
    assertEquals( RssFeedItemConverter.IMAGE_LENGTH_DEFAULT, enclosure.getLength() );
  }

  @Test
  public void convertEnclosureWhenNoThumbnail() {
    
    //do test
    FeedItemDto result = uut.convert( communication );

    assertNull( result.getEnclosure() );
  }
  
  @Test
  public void convertKeywords() {
    
    String keywords = KEYWORD_1 + " , " + KEYWORD_2; 
    communication.setKeywords( keywords );
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    List<String> categories = result.getCategories();
    assertNotNull( categories );
    
    assertEquals( KEYWORD_1, categories.get( 0 ) );
    assertEquals( KEYWORD_2, categories.get( 1 ) );
  }

  @Test
  public void convertKeywordsWhenNoneSet() {
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    List<String> categories = result.getCategories();
    assertNull( categories );
  }
  
  @Test
  public void convertCalendarUrlForUpcoming() {
    
    communication.setStatus( Status.UPCOMING );
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    assertEquals( CALENDAR_URL, result.getCalendar() );
  }

  @Test
  public void convertCalendarUrlForRecorded() {
    
    communication.setStatus( Status.RECORDED );
    
    //do test
    FeedItemDto result = uut.convert( communication );
    
    assertNull( result.getCalendar() );
  }
  
  private Communication createCommunication() {
    
    Communication communication = new Communication();
    communication.setId( COMMUNICATION_ID );
    communication.setTitle( COMMUNICATION_TITLE );
    communication.setDescription( COMMUNICATION_DESCRIPTION );
    communication.setAuthors( PRESENTER );
    communication.setUrl( COMMUNICATION_URL );
    communication.setCalendarUrl( CALENDAR_URL );
    communication.setStatus( Status.LIVE );
    communication.setDuration( COMMUNICATION_DURATION );
    communication.setFormat( Format.AUDIO );
    
    Calendar calendar = Calendar.getInstance();
    calendar.set( SCHEDULED_YEAR, SCHEDULED_MONTH, SCHEDULED_DAY, SCHEDULED_HOUR, SCHEDULED_MINUTE, SCHEDULED_SECOND );
    
    Date scheduledDate = calendar.getTime();
    communication.setScheduledDateTime( scheduledDate );
    
    CommunicationStatistics statistics = new CommunicationStatistics(
        COMMUNICATION_ID, 0, 0, 1, COMMUNICATION_RATING );
    communication.setCommunicationStatistics( statistics );
    
    return communication;
  }
  
  private String createExpectedPublishedDate() {
    
    // for example : Tue, 23 Jun 2009 15:34:33 +0000
    StringBuilder builder = new StringBuilder();
    builder.append( SCHEDULED_DAY_NAME ).append(", ");
    builder.append( SCHEDULED_DAY ).append( " " );
    builder.append( SCHEDULED_MONTH_NAME ).append( " " );
    builder.append( SCHEDULED_YEAR ).append( " " );
    builder.append( SCHEDULED_HOUR ).append(":");
    builder.append( SCHEDULED_MINUTE ).append(":");
    builder.append( SCHEDULED_SECOND ).append(" +0000");
    
    return builder.toString();
  }
}