/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsControllerTest.java 92672 2015-04-01 08:27:25Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.Subscription.Referral;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError.SubscriptionErrorCode;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionsSummary;
import com.brighttalk.service.channel.business.service.channel.SubscriptionsService;
import com.brighttalk.service.channel.presentation.dto.SubscriptionContextDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionErrorDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsSummaryDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;
import com.brighttalk.service.channel.presentation.dto.converter.SubscriptionsConverter;
import com.brighttalk.service.channel.presentation.dto.converter.SubscriptionsSummaryConverter;
import com.brighttalk.service.channel.presentation.dto.validator.SubscriptionsCreateValidator;
import com.brighttalk.service.channel.presentation.dto.validator.SubscriptionsUpdateValidator;

public class SubscriptionsControllerTest {

  private static final Long CHANNEL_ID = 33L;

  private static final Boolean DRY_RUN_FALSE = false;

  private static final String EMAIL = "test@brighttalk.com";
  private static final String EMAIL2 = "test2@brighttalk.com";

  private static final String REFERRAL_PAID = "paid";
  private static final String LEAD_CONTEXT = "1111";
  private static final String ENGAGEMENT_SCORE = "88";
  private static final String LAST_SUBSCRIBED_DATE = "2015-02-02T12:12:12Z";
  private static final Long SUBSCRIPTION_ID = 99L;

  private SubscriptionsCreateValidator mockCreateValidator;

  private SubscriptionsUpdateValidator mockUpdateValidator;

  private SubscriptionsConverter mockSubscriptionsConverter;

  private SubscriptionsSummaryConverter mockSubscriptionsSummaryConverter;

  private SubscriptionsService mockSubscriptionsService;

  private DateTimeFormatter mockFormatter;

  private SubscriptionsController uut;

  @Before
  public void setUp() {

    uut = new SubscriptionsController();

    mockFormatter = EasyMock.createMock(DateTimeFormatter.class);

    mockCreateValidator = new SubscriptionsCreateValidator();
    ReflectionTestUtils.setField(uut, "createValidator", mockCreateValidator);

    mockUpdateValidator = new SubscriptionsUpdateValidator();
    ReflectionTestUtils.setField(uut, "updateValidator", mockUpdateValidator);

    mockSubscriptionsConverter = new SubscriptionsConverter();
    ReflectionTestUtils.setField(uut, "subscriptionsConverter", mockSubscriptionsConverter);

    mockSubscriptionsSummaryConverter = new SubscriptionsSummaryConverter();
    ReflectionTestUtils.setField(uut, "subscriptionsSummaryConverter", mockSubscriptionsSummaryConverter);

    mockSubscriptionsService = EasyMock.createMock(SubscriptionsService.class);
    ReflectionTestUtils.setField(uut, "subscriptionsService", mockSubscriptionsService);

  }

  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWhileOnlyErrors() {

    // Set up
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(Collections.singletonList(buildSubscriptionDto(EMAIL)));

    // Expectations
    SubscriptionsSummary subscriptionsSummary = new SubscriptionsSummary();
    subscriptionsSummary.setErrors(Arrays.asList(buildSubscriptionError()));

    EasyMock.expect(
        mockSubscriptionsService.create(isA(Long.class), isA(List.class), isA(Boolean.class))).andReturn(
        subscriptionsSummary);
    EasyMock.replay(mockSubscriptionsService);

    // Do test
    ResponseEntity<SubscriptionsSummaryDto> result =
        uut.createSubscriptions(CHANNEL_ID, subscriptionsDto, DRY_RUN_FALSE, mockFormatter);
    SubscriptionsSummaryDto summaryDto = result.getBody();

    // Assertions
    assertNull(summaryDto.getSubscriptions());
    assertFalse(summaryDto.getErrors().isEmpty());
    assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());

    EasyMock.verify(mockSubscriptionsService);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWhileOnlySuccesses() {

    // Set up
    List<SubscriptionDto> subscriptionDtos = Collections.singletonList(buildSubscriptionDto(EMAIL));
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptionDtos);

    // Expectations
    SubscriptionsSummary subscriptionsSummary = new SubscriptionsSummary();
    subscriptionsSummary.setSubscriptions(Arrays.asList(buildSubscription()));

    EasyMock.expect(
        mockSubscriptionsService.create(isA(Long.class), isA(List.class), isA(Boolean.class))).andReturn(
        subscriptionsSummary);
    EasyMock.replay(mockSubscriptionsService);

    // Do test
    ResponseEntity<SubscriptionsSummaryDto> result =
        uut.createSubscriptions(CHANNEL_ID, subscriptionsDto, DRY_RUN_FALSE, mockFormatter);
    SubscriptionsSummaryDto summaryDto = result.getBody();

    // Assertions
    assertFalse(summaryDto.getSubscriptions().isEmpty());
    assertNull(summaryDto.getErrors());
    assertEquals(HttpStatus.OK, result.getStatusCode());

    EasyMock.verify(mockSubscriptionsService);
  }

  /**
   * Tests {@link SubscriptionsController#createSubscriptions} in the case of returning both subscriptions and errors.
   * <p>
   * This test also testing error section containing subscription details when the error code is set to
   * "AlreadySubscribed" should contain the subscription detail of the subscription has caused the error.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWhileErrorsAndSuccesses() {

    // Set up
    List<SubscriptionDto> subscriptionDtos = new ArrayList<>();
    subscriptionDtos.add(buildSubscriptionDto(EMAIL));
    subscriptionDtos.add(buildSubscriptionDto(EMAIL2));

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptionDtos);

    // Expectations
    SubscriptionsSummary subscriptionsSummary = new SubscriptionsSummary();
    subscriptionsSummary.setSubscriptions(Arrays.asList(buildSubscription()));
    subscriptionsSummary.setErrors(Arrays.asList(buildSubscriptionError()));

    EasyMock.expect(
        mockSubscriptionsService.create(isA(Long.class), isA(List.class), isA(Boolean.class))).andReturn(
        subscriptionsSummary);
    EasyMock.expect(mockFormatter.convert(isA(Date.class))).andReturn(LAST_SUBSCRIBED_DATE).anyTimes();
    EasyMock.replay(mockSubscriptionsService, mockFormatter);

    // Do test
    ResponseEntity<SubscriptionsSummaryDto> result =
        uut.createSubscriptions(CHANNEL_ID, subscriptionsDto, DRY_RUN_FALSE, mockFormatter);
    SubscriptionsSummaryDto summaryDto = result.getBody();

    // Assertions
    assertFalse(summaryDto.getSubscriptions().isEmpty());
    assertFalse(summaryDto.getErrors().isEmpty());
    // Assert the reported error is AlreadySubscribed and contain the subscription details.
    SubscriptionErrorDto subscriptionErrorDto = summaryDto.getErrors().get(0);
    assertEquals(SubscriptionErrorCode.ALREADY_SUBSCRIBED.getCode(), subscriptionErrorDto.getCodes().get(0));
    assertNotNull(subscriptionErrorDto.getSubscription());
    SubscriptionDto subscriptionDto = subscriptionErrorDto.getSubscription();
    assertEquals(EMAIL2, subscriptionDto.getUser().getEmail());
    assertEquals(REFERRAL_PAID, subscriptionDto.getReferral());
    assertEquals(LAST_SUBSCRIBED_DATE, subscriptionDto.getLastSubscribed());
    assertEquals(HttpStatus.OK, result.getStatusCode());

    EasyMock.verify(mockSubscriptionsService, mockFormatter);
  }

  /**
   * Tests {@link SubscriptionsController#updateSubscriptions()} in the error case of updating subscription and the
   * supplied subscriptions all in error.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionsWhileOnlyErrors() {

    // Set up
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(Collections.singletonList(buildSubscriptionDto(EMAIL)));

    // Expectations
    SubscriptionsSummary subscriptionsSummary = new SubscriptionsSummary();
    subscriptionsSummary.setErrors(Arrays.asList(buildSubscriptionUpdateErrors()));

    EasyMock.expect(mockSubscriptionsService.update(isA(List.class), isA(Boolean.class))).andReturn(
        subscriptionsSummary);
    EasyMock.replay(mockSubscriptionsService);

    // Do test
    ResponseEntity<SubscriptionsSummaryDto> result = uut.updateSubscriptions(subscriptionsDto, DRY_RUN_FALSE,
        mockFormatter);
    SubscriptionsSummaryDto summaryDto = result.getBody();

    // Assertions
    assertNull(summaryDto.getSubscriptions());
    assertFalse(summaryDto.getErrors().isEmpty());
    SubscriptionErrorDto error = summaryDto.getErrors().iterator().next();
    assertEquals(2, error.getCodes().size());
    assertEquals(SubscriptionErrorCode.CONTEXT_ALREADY_EXIST.getCode(), error.getCodes().get(0));
    assertEquals(SubscriptionErrorCode.INVALID_LEAD_CONTEXT.getCode(), error.getCodes().get(1));
    assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    EasyMock.verify(mockSubscriptionsService);
  }

  /**
   * Tests {@link SubscriptionsController#updateSubscriptions()} in the success case of updating subscriptions with no
   * errors.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionsWhileOnlySuccesses() {

    // Set up
    List<SubscriptionDto> subscriptionDtos = Collections.singletonList(buildSubscriptionDto(EMAIL));
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptionDtos);

    // Expectations
    SubscriptionsSummary subscriptionsSummary = new SubscriptionsSummary();
    subscriptionsSummary.setSubscriptions(Arrays.asList(buildSubscription()));

    EasyMock.expect(mockSubscriptionsService.update(isA(List.class), isA(Boolean.class))).andReturn(
        subscriptionsSummary);
    EasyMock.replay(mockSubscriptionsService);

    // Do test
    ResponseEntity<SubscriptionsSummaryDto> result = uut.updateSubscriptions(subscriptionsDto, DRY_RUN_FALSE,
        mockFormatter);
    SubscriptionsSummaryDto summaryDto = result.getBody();

    // Assertions
    assertFalse(summaryDto.getSubscriptions().isEmpty());
    assertNull(summaryDto.getErrors());
    assertEquals(HttpStatus.OK, result.getStatusCode());
    EasyMock.verify(mockSubscriptionsService);
  }

  /**
   * Tests {@link SubscriptionsController#updateSubscriptions()} in the case of updating subscription and the supplied
   * subscriptions some in error and some in success.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionsWhileSuccessesAndErrors() {

    // Set up
    List<SubscriptionDto> subscriptionDtos = new ArrayList<>();
    SubscriptionDto subscriptionDto = buildSubscriptionDto(EMAIL);
    subscriptionDtos.add(subscriptionDto);
    SubscriptionDto subscriptionDto2 = buildSubscriptionDto(EMAIL2);
    subscriptionDto2.setId(SUBSCRIPTION_ID + 1L);
    subscriptionDtos.add(subscriptionDto2);

    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptionDtos);

    // Expectations
    SubscriptionsSummary subscriptionsSummary = new SubscriptionsSummary();
    subscriptionsSummary.setErrors(Arrays.asList(buildSubscriptionUpdateErrors()));
    subscriptionsSummary.setSubscriptions(Arrays.asList(buildSubscription()));

    EasyMock.expect(mockSubscriptionsService.update(isA(List.class), isA(Boolean.class))).andReturn(
        subscriptionsSummary);
    EasyMock.expect(mockFormatter.convert(isA(Date.class))).andReturn(LAST_SUBSCRIBED_DATE).anyTimes();
    EasyMock.replay(mockSubscriptionsService, mockFormatter);

    // Do test
    ResponseEntity<SubscriptionsSummaryDto> result = uut.updateSubscriptions(subscriptionsDto, DRY_RUN_FALSE,
        mockFormatter);
    SubscriptionsSummaryDto summaryDto = result.getBody();

    // Assertions
    assertFalse(summaryDto.getSubscriptions().isEmpty());
    assertFalse(summaryDto.getErrors().isEmpty());
    SubscriptionDto updateSubscription = summaryDto.getSubscriptions().iterator().next();
    assertNotNull(updateSubscription);
    assertEquals(EMAIL2, updateSubscription.getUser().getEmail());
    assertEquals(REFERRAL_PAID, updateSubscription.getReferral());
    assertEquals(LAST_SUBSCRIBED_DATE, updateSubscription.getLastSubscribed());

    SubscriptionErrorDto error = summaryDto.getErrors().iterator().next();
    assertEquals(2, error.getCodes().size());
    assertEquals(SubscriptionErrorCode.CONTEXT_ALREADY_EXIST.getCode(), error.getCodes().get(0));
    assertEquals(SubscriptionErrorCode.INVALID_LEAD_CONTEXT.getCode(), error.getCodes().get(1));
    assertEquals(HttpStatus.OK, result.getStatusCode());
    EasyMock.verify(mockSubscriptionsService, mockFormatter);
  }

  private SubscriptionError buildSubscriptionError() {
    SubscriptionError subscriptionError = new SubscriptionError();
    subscriptionError.setSubscription(buildSubscription());
    subscriptionError.addErrorCode(SubscriptionErrorCode.ALREADY_SUBSCRIBED);
    return subscriptionError;
  }

  private SubscriptionError buildSubscriptionUpdateErrors() {
    SubscriptionError subscriptionError = new SubscriptionError();
    subscriptionError.setSubscription(buildSubscription());
    subscriptionError.addErrorCode(SubscriptionErrorCode.CONTEXT_ALREADY_EXIST);
    subscriptionError.addErrorCode(SubscriptionErrorCode.INVALID_LEAD_CONTEXT);
    return subscriptionError;
  }

  private SubscriptionDto buildSubscriptionDto(final String email) {
    UserDto userDto = new UserDto();
    userDto.setEmail(email);

    SubscriptionDto subscriptionDto = new SubscriptionDto();
    subscriptionDto.setId(SUBSCRIPTION_ID);
    subscriptionDto.setUser(userDto);
    subscriptionDto.setReferral(REFERRAL_PAID);
    subscriptionDto.setContext(new SubscriptionContextDto(SubscriptionLeadType.KEYWORD.getType(), LEAD_CONTEXT,
        ENGAGEMENT_SCORE));
    return subscriptionDto;
  }

  private Subscription buildSubscription() {
    Subscription subscription = new Subscription();
    User user = new User();
    user.setEmail(EMAIL2);
    subscription.setUser(user);
    subscription.setReferral(Referral.paid);
    subscription.setLastSubscribed(new Date());
    subscription.setContext(new SubscriptionContext(SubscriptionLeadType.KEYWORD, LEAD_CONTEXT,
        ENGAGEMENT_SCORE));
    return subscription;
  }

}
