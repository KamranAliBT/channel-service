/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastConverterTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.Keywords;
import com.brighttalk.service.channel.business.domain.MediazoneConfig;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationStatisticsBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.presentation.dto.CategoryDto;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.RenditionDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.WebcastProviderDto;
import com.brighttalk.service.channel.presentation.dto.syndication.converter.WebcastSyndicationConverter;
import com.brighttalk.service.utils.DtoFactoryUtils;

public class WebcastConverterTest {

  private static final Long COMMUNICATION_ID = 666l;
  private static final Long CHANNEL_ID = 13l;

  private WebcastConverter uut;

  private DateTimeFormatter mockFormatter;

  private Keywords mockKeywords;

  private Communication communication;

  private WebcastSyndicationConverter mockWebcastSyndicationConverter;

  @Before
  public void setUp() {

    uut = new WebcastConverter();

    mockFormatter = EasyMock.createMock(DateTimeFormatter.class);

    mockKeywords = EasyMock.createMock(Keywords.class);
    ReflectionTestUtils.setField(uut, "keywords", mockKeywords);

    mockWebcastSyndicationConverter = EasyMock.createMock(WebcastSyndicationConverter.class);
    ReflectionTestUtils.setField(uut, "webcastSyndicationConverter", mockWebcastSyndicationConverter);

    communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);
    communication.setChannelId(CHANNEL_ID);
  }

  // -------------------------------- Tests for Convert Webcast DTO to Communication
  // ------------------------------------
  /**
   * Tests {@link WebcastConverter#convert(WebcastDto)} in case where the supplied webcast DTO is null.
   * <p>
   * Expected Result: An empty Communication object with all properties are null.
   */
  @Test
  public final void testConvertToCommunicationWhenWebcastDTONull() {
    // do test
    Communication communication = uut.convert(null);
    assertNotNull(communication);

    this.assertCommunicationFieldAreAllNull(communication);
  }

  /**
   * Tests {@link WebcastConverter#convert(WebcastDto)} in case where the supplied webcast DTO has full details.
   * <p>
   * Expected Result: A Communication object with full details.
   */
  @Test
  public final void testConvertToCommunicationFullDetails() {
    WebcastDto testWebcastDto = DtoFactoryUtils.createWebcastDTO();

    // do test
    Communication communication = uut.convert(testWebcastDto);
    assertNotNull(communication);

    this.assertAllCommunicationFields(communication, testWebcastDto);
  }

  // -------------------------------- Tests for Convert Communication to Webcast DTO
  // ------------------------------------
  @Test
  public void convertWhenNull() {

    // do test
    WebcastDto result = uut.convert(null, mockFormatter);

    assertNotNull(result);
    assertNull(result.getId());
    assertNull(result.getChannel());
    assertNull(result.getDescription());
    assertNull(result.getDuration());
    assertNull(result.getStart());
    assertNull(result.getStatus());
    assertNull(result.getLinks());
    assertNull(result.getTitle());
    assertNull(result.getRating());
    assertNull(result.getTotalViewings());
  }

  @Test
  public void convert() {
    // set up
    String keywords = "tag 1, tag 2";
    communication.setKeywords(keywords);

    // Expectations
    EasyMock.expect(mockKeywords.clean(keywords)).andReturn(keywords);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(4);
    EasyMock.replay(mockFormatter, mockKeywords);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertNotNull(result);
    EasyMock.verify(mockFormatter, mockKeywords);
  }

  @Test
  public void convertId() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(COMMUNICATION_ID, result.getId());
  }

  @Test
  public void convertChannel() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertNotNull(result.getChannel());
    assertEquals(CHANNEL_ID, result.getChannel().getId());
  }

  @Test
  public void convertDescription() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_DESCRIPTION, result.getDescription());
  }

  @Test
  public void convertPresenter() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_PRESENTER, result.getPresenter());
  }

  @Test
  public void convertDuration() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_DURATION, result.getDuration());
  }

  @Test
  public void convertStart() {

    String expectedStart = "expectedStart";

    // Expectations
    EasyMock.expect(mockFormatter.convert(communication.getScheduledDateTime())).andReturn(expectedStart);
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(4);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(expectedStart, result.getStart());
  }

  @Test
  public void convertStatus() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    final String expectedStatus = CommunicationBuilder.DEFAULT_STATUS.toString().toLowerCase();
    assertEquals(expectedStatus, result.getStatus());
  }

  @Test
  public void convertTitle() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_TITLE, result.getTitle());
  }

  @Test
  public void convertThumbnail() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    final String expectedThumbnailLinkHref = "expectedThumbnailLinkHref";
    communication.setThumbnailUrl(expectedThumbnailLinkHref);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertNotNull(result.getLinks().get(0));
    assertEquals(expectedThumbnailLinkHref, result.getLinks().get(0).getHref());
    assertEquals(LinkDto.TYPE_IMAGE_PNG, result.getLinks().get(0).getType());
    assertEquals(LinkDto.RELATIONSHIP_ENCLOSURE, result.getLinks().get(0).getRel());
    assertEquals(LinkDto.TITLE_THUMBNAIL, result.getLinks().get(0).getTitle());
  }

  @Test
  public void convertRating() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationStatisticsBuilder.DEFAULT_AVERAGE_RATING, result.getRating(), 0);
  }

  @Test
  public void convertTotalViewings() {

    // Expectations
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationStatisticsBuilder.DEFAULT_NUMBER_OF_VIEWINGS, result.getTotalViewings(), 0);
  }

  @Test
  public void convertKeywords() {

    // set up
    String keywords = "tag 1, tag 2";
    communication.setKeywords(keywords);

    // do test
    EasyMock.expect(mockKeywords.clean(keywords)).andReturn(keywords);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter, mockKeywords);

    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(keywords, result.getKeywords());
  }

  @Test
  public void convertFormat() {

    // do test
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(CommunicationBuilder.DEFAULT_FORMAT.toString().toLowerCase(), result.getFormat());
  }

  @Test
  public void convertEntryTime() {

    String expectedEntry = "expectedEntry";

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn(expectedEntry).times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(expectedEntry, result.getEntryTime());
  }

  @Test
  public void convertCloseTime() {

    String expectedClose = "expectedClose";

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn(expectedClose).times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(expectedClose, result.getCloseTime());
  }

  @Test
  public void convertCreated() {

    String expectedCreated = "expectedCreated";

    Date created = new Date();
    communication.setCreated(created);
    communication.setLastUpdated(null);

    EasyMock.expect(mockFormatter.convert(communication.getCreated())).andReturn(expectedCreated).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getEntryTime())).andReturn(null).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getCloseTime())).andReturn(null).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getScheduledDateTime())).andReturn(null).times(1);
    EasyMock.expect(mockFormatter.convert(communication.getLastUpdated())).andReturn(null).times(1);

    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(expectedCreated, result.getCreated());
  }

  @Test
  public void convertWhenNoUrl() {

    communication.setThumbnailUrl(null);
    communication.setPreviewUrl(null);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertNull(result.getLinks());
  }

  @Test
  public void convertPreviewUrl() {

    final String expectedPreviewLinkHref = "expectedPreviewLinkHref";
    communication.setThumbnailUrl(null);
    communication.setPreviewUrl(expectedPreviewLinkHref);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertNotNull(result.getLinks().get(0));
    assertEquals(expectedPreviewLinkHref, result.getLinks().get(0).getHref());
    assertEquals(LinkDto.TYPE_IMAGE_PNG, result.getLinks().get(0).getType());
    assertEquals(LinkDto.RELATIONSHIP_RELATED, result.getLinks().get(0).getRel());
    assertEquals(LinkDto.TITLE_PREVIEW, result.getLinks().get(0).getTitle());
  }

  @Test
  public void convertCalendarUrl() {

    final String expectedCalendarLinkHref = "expectedPreviewLinkHref";
    communication.setThumbnailUrl(null);
    communication.setPreviewUrl(null);
    communication.setCalendarUrl(expectedCalendarLinkHref);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertNotNull(result.getLinks().get(0));
    assertEquals(expectedCalendarLinkHref, result.getLinks().get(0).getHref());
    assertEquals(LinkDto.TYPE_TEXT_CALENDAR, result.getLinks().get(0).getType());
    assertEquals(LinkDto.RELATIONSHIP_RELATED, result.getLinks().get(0).getRel());
    assertEquals(LinkDto.TITLE_CALENDAR, result.getLinks().get(0).getTitle());
  }

  @Test
  public void convertWithLinksCalendarUrl() {

    final String expectedCalendarLinkHref = "expectedPreviewLinkHref";
    final String expectedPreviewLinkHref = "expectedPreviewLinkHref";
    final String expectedThumbnailLinkHref = "expectedThumbnailLinkHref";
    communication.setThumbnailUrl(expectedThumbnailLinkHref);
    communication.setPreviewUrl(expectedPreviewLinkHref);
    communication.setCalendarUrl(expectedCalendarLinkHref);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertNotNull(result.getLinks());
    assertEquals(3, result.getLinks().size());
  }

  @Test
  public void convertWithProviderVideo() {

    Provider provider = new Provider(Provider.VIDEO);
    communication.setProvider(provider);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(Provider.VIDEO, result.getProvider().getName());
  }

  @Test
  public void convertWithRenditions() {

    Provider provider = new Provider(Provider.VIDEO);
    communication.setProvider(provider);

    Rendition rendition1 = new Rendition();
    rendition1.setBitrate(123l);
    rendition1.setCodecs("codec");
    rendition1.setContainer("container");
    rendition1.setIsActive(true);
    rendition1.setType("video");
    rendition1.setUrl("url");
    rendition1.setWidth(456l);

    List<Rendition> renditions = new ArrayList<Rendition>();
    renditions.add(rendition1);
    communication.setRenditions(renditions);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    List<RenditionDto> renditionsDto = result.getProvider().getRenditions();

    assertEquals(rendition1.getBitrate(), renditionsDto.get(0).getBitrate());
    assertEquals(rendition1.getCodecs(), renditionsDto.get(0).getCodecs());
    assertEquals(rendition1.getContainer(), renditionsDto.get(0).getContainer());
    assertEquals(rendition1.getIsActive().toString(), renditionsDto.get(0).getIsActive());
    assertEquals(rendition1.getType(), renditionsDto.get(0).getType());
    assertEquals(rendition1.getUrl(), renditionsDto.get(0).getUrl());
    assertEquals(rendition1.getWidth(), renditionsDto.get(0).getWidth());

    assertNotNull(result.getProvider().getRenditions());
  }

  @Test
  public void convertWithProviderMediazoneWithNoConfig() {

    MediazoneConfig mediazoneConfig = null;
    Provider provider = new Provider(Provider.MEDIAZONE);
    communication.setProvider(provider);
    communication.setMediazoneConfig(mediazoneConfig);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(Provider.MEDIAZONE, result.getProvider().getName());
  }

  @Test
  public void convertWithMediazoneConfig() {

    String configUlr = "http://mediazone.config.url";
    String mediazoneId = "10";

    Provider provider = new Provider(Provider.MEDIAZONE);
    communication.setProvider(provider);

    MediazoneConfig mediazoneConfig = new MediazoneConfig();
    mediazoneConfig.setId(COMMUNICATION_ID);
    mediazoneConfig.setTargetMediazoneConfigUrl(configUlr);
    mediazoneConfig.setTargetMediazoneId(mediazoneId);
    mediazoneConfig.setLastUpdated(new Date());

    communication.setMediazoneConfig(mediazoneConfig);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(result.getProvider().getName(), Provider.MEDIAZONE);
    assertEquals(result.getProvider().getLink().getHref(), configUlr);
    assertEquals(result.getProvider().getLink().getType(), MediazoneConfig.MEDIAZONE_CONFIG);
    assertEquals(result.getProvider().getResource().getId().toString(), mediazoneId);
    assertEquals(result.getProvider().getResource().getType(), MediazoneConfig.MEDIAZONE_WEBCAST);
    assertNotNull(result.getProvider().getUpdated());

  }

  @Test
  public void convertWithPinNumberWhenStatusIsUpcoming() {

    String expectedPinNumber = "0456870";
    communication.setPinNumber(expectedPinNumber);
    communication.setStatus(Status.UPCOMING);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(expectedPinNumber, result.getPinNumber());
  }

  @Test
  public void convertWithPinNumberWhenStatusIsRecorded() {

    String expectedPinNumber = "0456870";
    communication.setPinNumber(expectedPinNumber);
    communication.setStatus(Status.RECORDED);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertNull(result.getPinNumber());
  }

  @Test
  public void convertWithLivePhoneNumberWhenStatusIsUpcoming() {

    String expectedLivePhoneNumber = "0045685570";
    communication.setPinNumber(expectedLivePhoneNumber);
    communication.setStatus(Status.UPCOMING);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertEquals(expectedLivePhoneNumber, result.getPinNumber());
  }

  @Test
  public void convertWithLivePhoneNumberWhenStatusIsRecorded() {

    String expectedLivePhoneNumber = "0045685570";
    communication.setPinNumber(expectedLivePhoneNumber);
    communication.setStatus(Status.RECORDED);

    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn("").times(5);
    EasyMock.replay(mockFormatter);

    // do test
    WebcastDto result = uut.convert(communication, mockFormatter);

    assertNull(result.getPinNumber());
  }

  @Test
  public void convertBasicFields() {

    Long bitrate = 10L;
    Long width = 10L;

    RenditionDto renditionDto = new RenditionDto();
    renditionDto.setBitrate(bitrate);
    renditionDto.setCodecs("codecs");
    renditionDto.setContainer("container");
    renditionDto.setType("type");
    renditionDto.setUrl("url");
    renditionDto.setWidth(width);

    List<RenditionDto> renditionDtos = new ArrayList<>();
    renditionDtos.add(renditionDto);

    WebcastProviderDto webcastProviderDto = new WebcastProviderDto(Provider.BRIGHTTALK_HD);
    webcastProviderDto.setRenditions(renditionDtos);

    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(COMMUNICATION_ID);
    webcastDto.setStatus(Communication.Status.RECORDED.toString());
    webcastDto.setProvider(webcastProviderDto);

    Communication communication = uut.convertBasicFields(webcastDto);

    assertEquals(communication.getId(), webcastDto.getId());
    assertEquals(communication.getStatus().toString(), webcastDto.getStatus().toString());
    assertEquals(communication.getProvider().getName(), webcastDto.getProvider().getName());
    assertEquals(communication.getRenditions().size(), renditionDtos.size());
    assertEquals(communication.getRenditions().get(0).getBitrate(), bitrate);
    assertEquals(communication.getRenditions().get(0).getWidth(), width);
  }

  /**
   * @param communication The communication to assert.
   */
  private void assertCommunicationFieldAreAllNull(final Communication communication) {
    assertNull(communication.getId());
    assertNull(communication.getChannelId());
    assertNull(communication.getTitle());
    assertNull(communication.getDescription());
    assertNull(communication.getDuration());
    assertNull(communication.getKeywords());
    assertNull(communication.getAuthors());
    assertNull(communication.getScheduledDateTime());
    assertNull(communication.getTimeZone());
    assertFalse(communication.hasFormat());
    assertFalse(communication.hasConfiguration());
    assertFalse(communication.hasCategories());
  }

  /**
   * @param expectedCommunication The expected communication to assert.
   * @param actualWebcastDto The actual webcast DTO.
   */
  private void assertAllCommunicationFields(final Communication expectedCommunication, final WebcastDto actualWebcastDto) {
    assertNotNull(expectedCommunication);
    assertNotNull(actualWebcastDto);

    assertEquals(expectedCommunication.getId(), actualWebcastDto.getId());
    assertEquals(expectedCommunication.getChannelId(), actualWebcastDto.getChannel().getId());
    assertEquals(expectedCommunication.getTitle(), actualWebcastDto.getTitle());
    assertEquals(expectedCommunication.getDescription(), actualWebcastDto.getDescription());
    assertEquals(expectedCommunication.getKeywords(), actualWebcastDto.getKeywords());
    assertEquals(expectedCommunication.getAuthors(), actualWebcastDto.getPresenter());
    assertEquals(expectedCommunication.getDuration(), actualWebcastDto.getDuration());
    assertEquals(expectedCommunication.getTimeZone(), actualWebcastDto.getTimeZone());
    assertEquals(expectedCommunication.getFormat().name(), actualWebcastDto.getFormat().toUpperCase());
    // Assert communication configuration details.
    CommunicationConfiguration configuration = expectedCommunication.getConfiguration();
    assertNotNull(configuration);
    assertEquals(configuration.getChannelId(), actualWebcastDto.getChannel().getId());
    assertEquals(configuration.getVisibility().name(), actualWebcastDto.getVisibility().toUpperCase());
    assertEquals(configuration.allowAnonymousViewings(), new Boolean(actualWebcastDto.getAllowAnonymous()));
    assertEquals(configuration.excludeFromChannelContentPlan(),
        new Boolean(actualWebcastDto.getExcludeFromChannelCapacity()));
    assertEquals(configuration.showChannelSurvey(), new Boolean(actualWebcastDto.getShowChannelSurvey()));
    assertEquals(configuration.getClientBookingReference(), actualWebcastDto.getClientBookingReference());
    // Assert communication categories.
    List<CommunicationCategory> expectedCategories = expectedCommunication.getCategories();
    List<CategoryDto> actualCategories = actualWebcastDto.getCategories();
    assertEquals(expectedCategories.size(), actualCategories.size());
    for (int i = 0; i < expectedCategories.size(); i++) {
      assertEquals(expectedCategories.get(i).getId(), actualCategories.get(i).getId());
    }
  }
}