/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodConverterTest.java 101292 2015-10-09 12:50:41Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.time.DateTimeParser;
import com.brighttalk.common.time.Iso8601DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.presentation.dto.ContractPeriodDto;
import com.brighttalk.service.channel.presentation.dto.ContractPeriodsDto;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ContractPeriodConverterTest {

  // Unit under test
  private ContractPeriodConverter uut;

  //Dependencies
  private DateTimeFormatter formatter;
  private ContractPeriod contractPeriod;
  private ContractPeriodDto contractPeriodDto;

  /**
   * Test the conversion of a {@link ContractPeriod} to a {@link ContractPeriodDto}
   *
   * @throws Exception
   */
  @Test
  public void testConvertContractPeriodToContractPeriodDto() throws Exception {
    //Test
    ContractPeriodDto converted = uut.convert(contractPeriod, formatter);

    //Verify
    assertEquals(contractPeriod.getChannelId().longValue(), Long.parseLong(converted.getChannelId()));
    assertEquals(contractPeriod.getId().toString(), converted.getId());
    assertEquals(formatter.convert(contractPeriod.getStart()), converted.getStart());
    assertEquals(formatter.convert(contractPeriod.getEnd()), converted.getEnd());
  }

  /**
   * Test the conversion of a {@link List} of {@link ContractPeriod}s to a {@link ContractPeriodsDto}
   *
   * @throws Exception
   */
  @Test
  public void testConvertContractPeriodListToContractPeriodsDto() throws Exception {
    //Setup
    List<ContractPeriod> contractPeriods = new ArrayList<>(3);
    contractPeriods.add(contractPeriod);
    contractPeriods.add(contractPeriod);
    contractPeriods.add(contractPeriod);

    //Test
    ContractPeriodsDto convertedList = uut.convert(contractPeriods, formatter);

    //Verify
    assertEquals(contractPeriods.size(), convertedList.getContractPeriods().size());
    for (ContractPeriodDto converted : convertedList.getContractPeriods()) {
      assertEquals(contractPeriod.getChannelId().longValue(), Long.parseLong(converted.getChannelId()));
      assertEquals(contractPeriod.getId().toString(), converted.getId());
      assertEquals(formatter.convert(contractPeriod.getStart()), converted.getStart());
      assertEquals(formatter.convert(contractPeriod.getEnd()), converted.getEnd());
    }

  }

  /**
   * Test the conversion of a {@link ContractPeriodDto} to a {@link ContractPeriod}
   *
   * @throws Exception
   */
  @Test
  public void testConvertFromContractPeriodDtoToContractPeriod() throws Exception {
    //Test
    ContractPeriod converted = uut.convert(contractPeriodDto);

    //Verify
    assertEquals(contractPeriodDto.getChannelId(), Long.toString(converted.getChannelId()));
    assertEquals(contractPeriodDto.getId(), converted.getId().toString());
    assertEquals(contractPeriodDto.getEnd(), formatter.convert(converted.getEnd()));
    assertEquals(contractPeriodDto.getStart(), formatter.convert(converted.getStart()));
  }

  /**
   * Setup the unit under test, it's dependencies, and test data
   *
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    //Dependencies
    formatter = new Iso8601DateTimeFormatter();

    // Unit under test
    uut = new ContractPeriodConverter(new DateTimeParser());

    // Test objects
    //  Test time
    DateTime time = DateTime.now(DateTimeZone.UTC);
    //  Test contract period, equivalent of the below
    contractPeriod = new ContractPeriod(123l);
    contractPeriod.setActive(true);
    contractPeriod.setChannelId(456l);
    contractPeriod.setStart(time.minusDays(1).toDate());
    contractPeriod.setEnd(time.plusDays(1).toDate());
    contractPeriod.setCreated(time.minusDays(10).toDate());
    contractPeriod.setLastUpdated(time.minusDays(10).toDate());

    //  Test contract period DTO, equivalent of the above
    contractPeriodDto = new ContractPeriodDto();
    contractPeriodDto.setId("123");
    contractPeriodDto.setChannelId("456");
    contractPeriodDto.setStart(formatter.convert(contractPeriod.getStart()));
    contractPeriodDto.setEnd(formatter.convert(contractPeriod.getEnd()));
  }
}