/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ChannelManagersSearchCriteriaBuilderTest.java 82312 2014-08-27 13:05:29Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria.SortBy;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;
import com.brighttalk.service.channel.presentation.ApiRequestParam;

public class ChannelManagersSearchCriteriaBuilderTest {

  private static final Integer DEFAULT_PAGE_NUMBER = 1;

  private static final Integer DEFAULT_PAGE_SIZE = 25;

  private static final Integer MIN_PAGE_SIZE = 1;

  private static final Integer MAX_PAGE_SIZE = 99;

  private ChannelManagersSearchCriteriaBuilder uut;

  private HttpServletRequest mockRequest;

  @Before
  public void setUp() {

    mockRequest = EasyMock.createMock(HttpServletRequest.class);

    uut = new ChannelManagersSearchCriteriaBuilder();

    ReflectionTestUtils.setField(uut, "defaultPageNumber", DEFAULT_PAGE_NUMBER);
    ReflectionTestUtils.setField(uut, "defaultPageSize", DEFAULT_PAGE_SIZE);
    ReflectionTestUtils.setField(uut, "maxPageSize", MAX_PAGE_SIZE);
    ReflectionTestUtils.setField(uut, "minPageSize", MIN_PAGE_SIZE);
  }

  @Test
  public void buildWithDefaultValues() {
    // Set up

    // Expectations
    Integer expectedPageNumber = 1;
    Integer expectedPageSize = 25;
    SortBy expectedSortBy = SortBy.LAST_NAME;
    SortOrder expectedSortOrder = SortOrder.ASC;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null);
    EasyMock.replay(mockRequest);

    // Do test
    ChannelManagersSearchCriteria result = uut.buildWithDefaultValues(mockRequest);

    // Assertions
    assertNotNull(result);
    assertEquals(expectedPageNumber, result.getPageNumber());
    assertEquals(expectedPageSize, result.getPageSize());
    assertEquals(expectedSortBy, result.getSortBy());
    assertEquals(expectedSortOrder, result.getSortOrder());
  }

  @Test
  public void buildWithDefaultValuesWithPageNumber() {
    // Set up
    String pageNumber = "5";

    // Expectations
    Integer expectedPageNumber = 5;
    Integer expectedPageSize = 25;
    SortBy expectedSortBy = SortBy.LAST_NAME;
    SortOrder expectedSortOrder = SortOrder.ASC;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(pageNumber);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null);
    EasyMock.replay(mockRequest);

    // Do test
    ChannelManagersSearchCriteria result = uut.buildWithDefaultValues(mockRequest);

    // Assertions
    assertNotNull(result);
    assertEquals(expectedPageNumber, result.getPageNumber());
    assertEquals(expectedPageSize, result.getPageSize());
    assertEquals(expectedSortBy, result.getSortBy());
    assertEquals(expectedSortOrder, result.getSortOrder());
  }

  @Test
  public void buildWithDefaultValuesWithInvalidPageNumber() {
    // Set up
    String pageNumber = "-1";

    // Expectations
    Integer expectedPageNumber = 1;
    Integer expectedPageSize = 25;
    SortBy expectedSortBy = SortBy.LAST_NAME;
    SortOrder expectedSortOrder = SortOrder.ASC;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(pageNumber);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null);
    EasyMock.replay(mockRequest);

    // Do test
    ChannelManagersSearchCriteria result = uut.buildWithDefaultValues(mockRequest);

    // Assertions
    assertNotNull(result);
    assertEquals(expectedPageNumber, result.getPageNumber());
    assertEquals(expectedPageSize, result.getPageSize());
    assertEquals(expectedSortBy, result.getSortBy());
    assertEquals(expectedSortOrder, result.getSortOrder());
  }

  @Test
  public void buildWithDefaultValuesWithPageSize() {
    // Set up
    String pageSize = "5";

    // Expectations
    Integer expectedPageNumber = 1;
    Integer expectedPageSize = 5;
    SortBy expectedSortBy = SortBy.LAST_NAME;
    SortOrder expectedSortOrder = SortOrder.ASC;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(pageSize);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null);
    EasyMock.replay(mockRequest);

    // Do test
    ChannelManagersSearchCriteria result = uut.buildWithDefaultValues(mockRequest);

    // Assertions
    assertNotNull(result);
    assertEquals(expectedPageNumber, result.getPageNumber());
    assertEquals(expectedPageSize, result.getPageSize());
    assertEquals(expectedSortBy, result.getSortBy());
    assertEquals(expectedSortOrder, result.getSortOrder());
  }

  @Test
  public void buildWithDefaultValuesWithPageSizeSmallerThanMinLimit() {
    // Set up
    String pageSize = "0";

    // Expectations
    Integer expectedPageNumber = 1;
    Integer expectedPageSize = 1;
    SortBy expectedSortBy = SortBy.LAST_NAME;
    SortOrder expectedSortOrder = SortOrder.ASC;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(pageSize);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null);
    EasyMock.replay(mockRequest);

    // Do test
    ChannelManagersSearchCriteria result = uut.buildWithDefaultValues(mockRequest);

    // Assertions
    assertNotNull(result);
    assertEquals(expectedPageNumber, result.getPageNumber());
    assertEquals(expectedPageSize, result.getPageSize());
    assertEquals(expectedSortBy, result.getSortBy());
    assertEquals(expectedSortOrder, result.getSortOrder());
  }

  @Test
  public void buildWithDefaultValuesWithPageSizeGreaterThanMaxLimit() {
    // Set up
    String pageSize = "100";

    // Expectations
    Integer expectedPageNumber = 1;
    Integer expectedPageSize = 99;
    SortBy expectedSortBy = SortBy.LAST_NAME;
    SortOrder expectedSortOrder = SortOrder.ASC;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(pageSize);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null);
    EasyMock.replay(mockRequest);

    // Do test
    ChannelManagersSearchCriteria result = uut.buildWithDefaultValues(mockRequest);

    // Assertions
    assertNotNull(result);
    assertEquals(expectedPageNumber, result.getPageNumber());
    assertEquals(expectedPageSize, result.getPageSize());
    assertEquals(expectedSortBy, result.getSortBy());
    assertEquals(expectedSortOrder, result.getSortOrder());
  }

  @Test
  public void buildWithDefaultValuesWithSortBy() {
    // Set up
    String sortBy = "email";

    // Expectations
    Integer expectedPageNumber = 1;
    Integer expectedPageSize = 25;
    SortBy expectedSortBy = SortBy.EMAIL_ADDRESS;
    SortOrder expectedSortOrder = SortOrder.ASC;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(sortBy);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null);
    EasyMock.replay(mockRequest);

    // Do test
    ChannelManagersSearchCriteria result = uut.buildWithDefaultValues(mockRequest);

    // Assertions
    assertNotNull(result);
    assertEquals(expectedPageNumber, result.getPageNumber());
    assertEquals(expectedPageSize, result.getPageSize());
    assertEquals(expectedSortBy, result.getSortBy());
    assertEquals(expectedSortOrder, result.getSortOrder());
  }

  @Test(expected = SearchCriteriaException.class)
  public void buildWithDefaultValuesWithInvalidSortBy() {
    // Set up
    String sortBy = "invalid";

    // Expectations
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(sortBy);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null);
    EasyMock.replay(mockRequest);

    // Do test
    uut.buildWithDefaultValues(mockRequest);

    // Assertions
  }

  @Test
  public void buildWithDefaultValuesWithSortOrder() {
    // Set up
    String sortOrder = "desc";

    // Expectations
    Integer expectedPageNumber = 1;
    Integer expectedPageSize = 25;
    SortBy expectedSortBy = SortBy.LAST_NAME;
    SortOrder expectedSortOrder = SortOrder.DESC;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(sortOrder);
    EasyMock.replay(mockRequest);

    // Do test
    ChannelManagersSearchCriteria result = uut.buildWithDefaultValues(mockRequest);

    // Assertions
    assertNotNull(result);
    assertEquals(expectedPageNumber, result.getPageNumber());
    assertEquals(expectedPageSize, result.getPageSize());
    assertEquals(expectedSortBy, result.getSortBy());
    assertEquals(expectedSortOrder, result.getSortOrder());
  }

  @Test(expected = SearchCriteriaException.class)
  public void buildWithDefaultValuesWithInvalidSortOrder() {
    // Set up
    String sortOrder = "invalid";

    // Expectations
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(sortOrder);
    EasyMock.replay(mockRequest);

    // Do test
    uut.buildWithDefaultValues(mockRequest);

    // Assertions
  }

}