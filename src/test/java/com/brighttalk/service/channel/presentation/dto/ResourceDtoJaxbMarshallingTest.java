/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ResourceDtoJaxbMarshallingTest.java 84272 2014-10-07 08:22:56Z nbrown $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamResult;

import junit.framework.AssertionFailedError;

import org.apache.log4j.Logger;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.utils.TestException;

/**
 * Tests the marshalling of {@link ResourceDto} performed in the presentation layer of the application using JAXB, as
 * relied upon by APIs which support creating and updating 'resource' (aka attachment) entities.
 * <p>
 * Implemented as a JUnit TestCase. Provides a quick way to test the (JAXB) annotations on the classes being marshalled.
 * <p>
 * Originally written to support investigation of bug that caused marshalling of certain properties to intermmittently
 * fail. See https://brighttalktech.jira.com/browse/AP-607.
 */
public class ResourceDtoJaxbMarshallingTest {

  private static final Logger logger = Logger.getLogger(ResourceDtoJaxbMarshallingTest.class);

  private Marshaller marshaller;

  @Before
  public void setUp() throws Exception {
    setUpMarshaller();
    setUpXmlUnit();
  }

  /**
   * Tests marshalling (serialization) of an instance of {@link ResourceDto} which represents a link type resource to
   * its expected XML representation.
   * 
   * @throws Exception If an unexpected error occurs on execution of this test.
   */
  @Test
  public void testMarshallLink() throws Exception {
    final ResourceDto resource = new ResourceDto();
    resource.setId(1L);
    resource.setTitle("Title");
    resource.setDescription("Description");
    final ResourceLinkDto link = new ResourceLinkDto();
    link.setHref("http://www.google.com");
    resource.setLink(link);

    final StringWriter writer = new StringWriter();
    this.marshaller.marshal(resource, new StreamResult(writer));
    final String actualXml = writer.toString();

    //@formatter:off
    final String expectedXml = 
      "<resource id='" + resource.getId() + "'>" + 
        "<title>" + resource.getTitle() + "</title>" +
        "<description>" + resource.getDescription() + "</description>" + 
        "<link href='" + resource.getLink().getHref() + "'/>" +
      "</resource>"; 
    //@formatter:on 

    try {
      XMLAssert.assertXMLEqual(expectedXml, actualXml);
    } catch (AssertionFailedError e) {
      // Wrap and rethrow assertion failure to include the expected and actual XML, which XMLUnit doesn't include
      throw new TestException("XML not equal. \nExpected [" + expectedXml + "]. \nActual [" + actualXml + "]", e);
    }
  }

  /**
   * Executes {@link #testMarshallLink()} a multiple (fixed) number of times. Written to help investigate intermmittent
   * failures in marshalling the 'link' property as a child element.
   * 
   * @throws Exception If an unexpected error occurs on execution of this test.
   */
  @Test
  public void testMarshallLink_Repeatedly() throws Exception {
    final Integer numberOfTimes = 10;
    for (int i = 0; i < numberOfTimes; i++) {
      logger.info("Executing testMarshalLink(). Execution count [" + i + "]...");
      testMarshallLink();
    }
  }

  private void setUpMarshaller() throws JAXBException {
    Class<?>[] classesToBeBound = new Class<?>[] { ResourceDto.class, ResourceLinkDto.class };
    JAXBContext jc = JAXBContext.newInstance(classesToBeBound);
    this.marshaller = jc.createMarshaller();
  }

  private void setUpXmlUnit() {
    // Assertions performed by XMLUnit should be relatively lenient
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreAttributeOrder(true);
  }
}