/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeedControllerTest.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.PaginatedChannelFeed;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;
import com.brighttalk.service.channel.business.service.channel.ChannelFeedService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.presentation.dto.atom.AtomFeedDto;
import com.brighttalk.service.channel.presentation.dto.converter.AtomFeedConverter;
import com.brighttalk.service.channel.presentation.dto.converter.RssFeedConverter;
import com.brighttalk.service.channel.presentation.dto.rss.RssFeedDto;
import com.brighttalk.service.channel.presentation.util.ChannelFeedSearchCriteriaBuilder;

/**
 * Unit tests for the {@link FeedController}.
 */
public class FeedControllerTest {

  private static final Long COMMUNICATION_ID = 1L;
  private static final Long CHANNEL_ID = 2L;
  private static final Integer PAGE_NUMBER = 1;
  private static final Integer PAGE_SIZE = 50;
  private static final String FILTER_TYPE_AS_STRING = "closesttonow";

  private FeedController uut;

  private AtomFeedConverter mockAtomFeedConverter;
  private RssFeedConverter mockRssFeedConverter;
  private ChannelFinder mockChannelFinder;
  private ChannelFeedService mockChannelFeedService;
  private ChannelFeedSearchCriteriaBuilder mockChannelFeedSearchCriteriaBuilder;

  @Before
  public void setUp() {

    uut = new FeedController();

    mockChannelFeedSearchCriteriaBuilder = EasyMock.createMock(ChannelFeedSearchCriteriaBuilder.class);
    ReflectionTestUtils.setField(uut, "channelFeedSearchCriteriaBuilder", mockChannelFeedSearchCriteriaBuilder);

    mockAtomFeedConverter = EasyMock.createMock(AtomFeedConverter.class);
    ReflectionTestUtils.setField(uut, "atomFeedConverter", mockAtomFeedConverter);

    mockRssFeedConverter = EasyMock.createMock(RssFeedConverter.class);
    ReflectionTestUtils.setField(uut, "rssFeedConverter", mockRssFeedConverter);

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockChannelFeedService = EasyMock.createMock(ChannelFeedService.class);
    ReflectionTestUtils.setField(uut, "channelFeedService", mockChannelFeedService);
  }

  /**
   * Tests {@link FeedController#getAtom(String, Integer, Integer, String, String)} without passing the status filter.
   */
  @Test
  public void getAtomWithNoStatusFilter() {

    // Set up
    Long communicationId = COMMUNICATION_ID;
    Long channelId = CHANNEL_ID;
    Integer pageNumber = PAGE_NUMBER;
    Integer pageSize = PAGE_SIZE;
    String filterTypeAsString = FILTER_TYPE_AS_STRING;
    String statusFilterAsString = null;
    int totalNumberOfCommunications = 1;

    Channel channel = buildChannel(channelId);

    ChannelFeedSearchCriteria searchCriteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, filterTypeAsString,
        statusFilterAsString);

    Communication communication = buildCommunication(communicationId);
    List<Communication> communications = Arrays.asList(communication);

    PaginatedChannelFeed feed = buildPaginatedChannelFeed(searchCriteria, totalNumberOfCommunications, channel,
        communications);

    AtomFeedDto expectedAtomFeedDto = new AtomFeedDto();

    // Expectations
    EasyMock.expect(
        mockChannelFeedSearchCriteriaBuilder.buildWithDefaults(pageNumber, pageSize, filterTypeAsString,
            statusFilterAsString)).andReturn(searchCriteria);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);

    EasyMock.expect(mockChannelFeedService.getFeed(channel, searchCriteria)).andReturn(feed);

    EasyMock.expect(mockAtomFeedConverter.convert(feed)).andReturn(expectedAtomFeedDto);

    EasyMock.replay(mockChannelFeedSearchCriteriaBuilder, mockChannelFinder, mockChannelFeedService,
        mockAtomFeedConverter);

    // Do test
    AtomFeedDto actualAtomFeedDto = uut.getAtom(channelId.toString(), pageNumber, pageSize, filterTypeAsString,
        statusFilterAsString);

    // Assertions
    EasyMock.verify(mockChannelFeedSearchCriteriaBuilder, mockChannelFinder, mockChannelFeedService,
        mockAtomFeedConverter);

    assertEquals(expectedAtomFeedDto, actualAtomFeedDto);
  }

  /**
   * Tests {@link FeedController#getAtom(String, Integer, Integer, String, String)} with a status filter.
   */
  @Test
  public void getAtomWithStatusFilter() {

    // Set up
    Long communicationId = COMMUNICATION_ID;
    Long channelId = CHANNEL_ID;
    Integer pageNumber = PAGE_NUMBER;
    Integer pageSize = PAGE_SIZE;
    String filterTypeAsString = FILTER_TYPE_AS_STRING;
    String statusFilterAsString = "live,upcoming";
    int totalNumberOfCommunications = 1;

    Channel channel = buildChannel(channelId);

    ChannelFeedSearchCriteria searchCriteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, filterTypeAsString,
        statusFilterAsString);

    Communication communication = buildCommunication(communicationId);
    List<Communication> communications = Arrays.asList(communication);

    PaginatedChannelFeed feed = buildPaginatedChannelFeed(searchCriteria, totalNumberOfCommunications, channel,
        communications);

    AtomFeedDto expectedAtomFeedDto = new AtomFeedDto();

    // Expectations
    EasyMock.expect(
        mockChannelFeedSearchCriteriaBuilder.buildWithDefaults(pageNumber, pageSize, filterTypeAsString,
            statusFilterAsString)).andReturn(searchCriteria);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);

    EasyMock.expect(mockChannelFeedService.getFeed(channel, searchCriteria)).andReturn(feed);

    EasyMock.expect(mockAtomFeedConverter.convert(feed)).andReturn(expectedAtomFeedDto);

    EasyMock.replay(mockChannelFeedSearchCriteriaBuilder, mockChannelFinder, mockChannelFeedService,
        mockAtomFeedConverter);

    // Do test
    AtomFeedDto actualAtomFeedDto = uut.getAtom(channelId.toString(), pageNumber, pageSize, filterTypeAsString,
        statusFilterAsString);

    // Assertions
    EasyMock.verify(mockChannelFeedSearchCriteriaBuilder, mockChannelFinder, mockChannelFeedService,
        mockAtomFeedConverter);

    assertEquals(expectedAtomFeedDto, actualAtomFeedDto);
  }

  /**
   * Tests {@link FeedController#getAtom(String, Integer, Integer, String, String)} with an invalid status filter.
   */
  @Test
  public void getAtomWithInvalidStatusFilter() {

    // Set up
    Long channelId = CHANNEL_ID;
    Integer pageNumber = PAGE_NUMBER;
    Integer pageSize = PAGE_SIZE;
    String filterTypeAsString = FILTER_TYPE_AS_STRING;
    String statusFilterAsString = "live,rubbish";

    AtomFeedDto expectedAtomFeedDto = new AtomFeedDto();

    // Expectations
    EasyMock.expect(
        mockChannelFeedSearchCriteriaBuilder.buildWithDefaults(pageNumber, pageSize, filterTypeAsString,
            statusFilterAsString)).andThrow(new SearchCriteriaException("webcastStatus", statusFilterAsString));

    EasyMock.expect(mockAtomFeedConverter.convert(EasyMock.isA(PaginatedChannelFeed.class))).andReturn(
        expectedAtomFeedDto);

    EasyMock.replay(mockChannelFeedSearchCriteriaBuilder, mockChannelFinder, mockChannelFeedService,
        mockAtomFeedConverter);

    // Do test
    AtomFeedDto actualAtomFeedDto = uut.getAtom(channelId.toString(), pageNumber, pageSize, filterTypeAsString,
        statusFilterAsString);

    // Assertions
    EasyMock.verify(mockChannelFeedSearchCriteriaBuilder, mockChannelFinder, mockChannelFeedService,
        mockAtomFeedConverter);

    assertEquals(expectedAtomFeedDto, actualAtomFeedDto);
  }

  /**
   * Tests {@link FeedController#getRss(String)}.
   */
  @Test
  public void getRss() {

    // Set up
    Long communicationId = COMMUNICATION_ID;
    Long channelId = CHANNEL_ID;
    Integer searchCriteriaPageNumber = PAGE_NUMBER;
    Integer searchCriteriaPageSize = PAGE_SIZE;
    String filterTypeAsString = null;
    String statusFilterAsString = null;
    int totalNumberOfCommunications = 1;

    Channel channel = buildChannel(channelId);

    ChannelFeedSearchCriteria searchCriteria = buildChannelFeedSearchCriteria(searchCriteriaPageNumber,
        searchCriteriaPageSize, filterTypeAsString, statusFilterAsString);

    Communication communication = buildCommunication(communicationId);
    List<Communication> communications = Arrays.asList(communication);

    PaginatedChannelFeed feed = buildPaginatedChannelFeed(searchCriteria, totalNumberOfCommunications, channel,
        communications);

    RssFeedDto expectedRssFeedDto = new RssFeedDto();

    // Expectations
    EasyMock.expect(mockChannelFeedSearchCriteriaBuilder.buildWithDefaults()).andReturn(searchCriteria);

    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);

    EasyMock.expect(mockChannelFeedService.getFeed(channel, searchCriteria)).andReturn(feed);

    EasyMock.expect(mockRssFeedConverter.convert(feed)).andReturn(expectedRssFeedDto);

    EasyMock.replay(mockChannelFeedSearchCriteriaBuilder, mockChannelFinder, mockChannelFeedService,
        mockRssFeedConverter);

    // Do test
    RssFeedDto rssFeedDto = uut.getRss(channelId.toString());

    // Assertions
    EasyMock.verify(mockChannelFeedSearchCriteriaBuilder, mockChannelFinder, mockChannelFeedService,
        mockRssFeedConverter);

    assertEquals(rssFeedDto, rssFeedDto);
  }

  private PaginatedChannelFeed buildPaginatedChannelFeed(ChannelFeedSearchCriteria searchCriteria,
      final int totalNumberOfCommunications, final Channel channel, final List<Communication> communications) {

    return new PaginatedChannelFeed(channel, communications, totalNumberOfCommunications, searchCriteria);
  }

  private ChannelFeedSearchCriteria buildChannelFeedSearchCriteria(final Integer pageNumber, final Integer pageSize,
      final String filterTypeAsString, final String statusFilterAsString) {

    return new ChannelFeedSearchCriteriaBuilder().build(pageNumber, pageSize, filterTypeAsString, statusFilterAsString);
  }

  private Channel buildChannel(final Long channelId) {

    Channel channel = new Channel(channelId);

    return channel;
  }

  private Communication buildCommunication(final Long communicationId) {

    Communication communication = new Communication(communicationId);

    return communication;
  }
}
