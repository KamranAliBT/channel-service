package com.brighttalk.service.channel.presentation.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerPublicDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersPublicDto;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;
import com.brighttalk.service.channel.presentation.dto.UserPublicDto;
import com.brighttalk.service.channel.presentation.dto.builder.LinksBuilder;

/**
 * Tests {link ChannelManagerConverter}.
 */
public class ChannelManagerConverterTest {

  private static final Long CHANNEL_MANAGER_ID = 999L;

  private static final String EMAIL_ADDRESS = "email@brighttalk.com";

  private static final boolean EMAIL_ALERTS = false;

  private static final Long USER_ID = 888L;

  private static final String USER_EMAIL_ADDRESS = "email@brighttalk.com";

  private static final String USER_FIRST_NAME = "FirstName";

  private static final String USER_LAST_NAME = "LastName";

  private static final String USER_JOB_TITLE = "JobTitle";

  private static final String USER_COMPANY_NAME = "CompanyName";

  private static final String USER_EXTERNAL_ID = "ExternalId";

  private static final boolean USER_IS_COMPLETE = true;

  private static final boolean USER_IS_ACTIVE = true;

  private static final String USER_REALM_USER_ID = "RealmId";

  private static final String USER_TIME_ZONE = "TimeZone";

  private static final String USER_API_KEY = "ApiKey";

  private static final Date USER_CREATED = new Date();

  private static final Date USER_LAST_LOGIN = new Date();

  private static final String USER_TELEPHONE = "123456789";

  private static final String USER_LEVEL = "Level";

  private static final String USER_INDUSTRY = "Industry";

  private static final String USER_COMPANY_SIZE = "1-10";

  private static final String USER_STATE_REGION = "StateRegion";

  private static final String USER_COUNTRY = "Country";

  private ConverterContext mockConverterContext;

  private LinksBuilder mockLinksBuilder;

  private ChannelManagerConverter uut;

  @Before
  public void setUp() {

    uut = new ChannelManagerConverter();

    mockLinksBuilder = new LinksBuilder();
    mockConverterContext = EasyMock.createMock(ConverterContext.class);

    ReflectionTestUtils.setField(uut, "linksBuilder", mockLinksBuilder);

  }

  @Test
  public void testConvertChannelManagerDto() {

    // Set up
    UserDto user = new UserDto();
    user.setEmail(EMAIL_ADDRESS);

    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setUser(user);

    // Do test
    ChannelManager result = uut.convert(channelManagerDto);

    // Assertions
    assertEquals(result.getUser().getEmail(), EMAIL_ADDRESS);
  }

  @Test
  public void testConvertChannelManagersDto() {

    // Set up
    UserDto user = new UserDto();
    user.setEmail(EMAIL_ADDRESS);

    ChannelManagerDto channelManagerDto = new ChannelManagerDto();
    channelManagerDto.setUser(user);

    List<ChannelManagerDto> channelManagersListDto = new ArrayList<ChannelManagerDto>();
    channelManagersListDto.add(channelManagerDto);

    ChannelManagersDto channelManagersDto = new ChannelManagersDto();
    channelManagersDto.setChannelManagers(channelManagersListDto);

    // Do test
    List<ChannelManager> result = uut.convert(channelManagersDto);

    // Assertions
    assertNotNull(result);
    assertFalse(result.isEmpty());
    assertFalse(result.get(0).getEmailAlerts());
    assertEquals(result.get(0).getUser().getEmail(), EMAIL_ADDRESS);
  }

  @Test
  public void testConvertChannelManager() {

    // Set up
    User user = buildUser();

    ChannelManager channelManager = buildDefaultChannelManager(user);

    // Do test
    ChannelManagerPublicDto result = uut.convertForPublic(channelManager);

    // Assertions
    assertChannelManagerPublicDto(result);
  }

  @Test
  public void testConvertPaginatedChannelManagersWhenFirstPage() throws MalformedURLException {

    // Set up
    User user = buildUser();

    ChannelManager channelManager = buildDefaultChannelManager(user);

    int pageSize = 1;
    int currentPageNumber = 1;
    boolean isFinalPage = false;

    PaginatedList<ChannelManager> channelManagers = buildPaginatedChannelManagers(pageSize, currentPageNumber,
        isFinalPage, channelManager);

    URL requestUrl = new URL("http://www.local.brighttalk.net");

    // Expectations
    EasyMock.expect(mockConverterContext.getAllowedQueryParamsFilter()).andReturn(new HashSet<String>()).once();
    EasyMock.expect(mockConverterContext.getRequestUrl()).andReturn(requestUrl).once();
    EasyMock.replay(mockConverterContext);

    // Do test
    ChannelManagersPublicDto result = uut.convertForPublic(channelManagers, mockConverterContext);

    // Assertions
    assertNotNull(result);
    assertNotNull(result.getChannelManagers());
    assertFalse(result.getChannelManagers().isEmpty());
    assertChannelManagerPublicDto(result.getChannelManagers().get(0));
    assertEquals(LinkDto.RELATIONSHIP_NEXT, result.getLinks().get(0).getRel());
    EasyMock.verify(mockConverterContext);
  }

  @Test
  public void testConvertPaginatedChannelManagersWhenSecondPage() throws MalformedURLException {

    // Set up
    User user = buildUser();

    ChannelManager channelManager = buildDefaultChannelManager(user);

    int pageSize = 1;
    int currentPageNumber = 2;
    boolean isFinalPage = false;

    PaginatedList<ChannelManager> channelManagers = buildPaginatedChannelManagers(pageSize, currentPageNumber,
        isFinalPage, channelManager);

    URL requestUrl = new URL("http://www.local.brighttalk.net");

    // Expectations
    EasyMock.expect(mockConverterContext.getAllowedQueryParamsFilter()).andReturn(new HashSet<String>()).times(2);
    EasyMock.expect(mockConverterContext.getRequestUrl()).andReturn(requestUrl).times(2);
    EasyMock.replay(mockConverterContext);

    // Do test
    ChannelManagersPublicDto result = uut.convertForPublic(channelManagers, mockConverterContext);

    // Assertions
    assertNotNull(result);
    assertNotNull(result.getChannelManagers());
    assertFalse(result.getChannelManagers().isEmpty());
    assertChannelManagerPublicDto(result.getChannelManagers().get(0));
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, result.getLinks().get(0).getRel());
    assertEquals(LinkDto.RELATIONSHIP_NEXT, result.getLinks().get(1).getRel());
    EasyMock.verify(mockConverterContext);
  }

  @Test
  public void testConvertPaginatedChannelManagersWhenFinalPage() throws MalformedURLException {

    // Set up
    User user = buildUser();

    ChannelManager channelManager = buildDefaultChannelManager(user);

    int pageSize = 1;
    int currentPageNumber = 3;
    boolean isFinalPage = true;

    PaginatedList<ChannelManager> channelManagers = buildPaginatedChannelManagers(pageSize, currentPageNumber,
        isFinalPage, channelManager);

    URL requestUrl = new URL("http://www.local.brighttalk.net");

    // Expectations
    EasyMock.expect(mockConverterContext.getAllowedQueryParamsFilter()).andReturn(new HashSet<String>()).once();
    EasyMock.expect(mockConverterContext.getRequestUrl()).andReturn(requestUrl).once();
    EasyMock.replay(mockConverterContext);

    // Do test
    ChannelManagersPublicDto result = uut.convertForPublic(channelManagers, mockConverterContext);

    // Assertions
    assertNotNull(result);
    assertNotNull(result.getChannelManagers());
    assertFalse(result.getChannelManagers().isEmpty());
    assertChannelManagerPublicDto(result.getChannelManagers().get(0));
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, result.getLinks().get(0).getRel());
    EasyMock.verify(mockConverterContext);
  }

  private ChannelManager buildDefaultChannelManager(final User user) {
    ChannelManager channelManager = new ChannelManager();
    channelManager.setId(CHANNEL_MANAGER_ID);
    channelManager.setUser(user);
    channelManager.setEmailAlerts(EMAIL_ALERTS);
    return channelManager;
  }

  private PaginatedList<ChannelManager> buildPaginatedChannelManagers(final int pageSize, final int currentPageNumber,
      final boolean isFinalPage, final ChannelManager... channelManagers) {

    PaginatedList<ChannelManager> paginatedList = new PaginatedList<ChannelManager>();
    paginatedList.addAll(Arrays.asList(channelManagers));
    paginatedList.setPageSize(pageSize);
    paginatedList.setCurrentPageNumber(currentPageNumber);
    paginatedList.setIsFinalPage(isFinalPage);

    return paginatedList;
  }

  private void assertChannelManagerPublicDto(final ChannelManagerPublicDto channelManagerDto) {

    assertEquals(channelManagerDto.getId(), CHANNEL_MANAGER_ID);
    assertEquals(channelManagerDto.getEmailAlerts(), EMAIL_ALERTS);
    assertUserPublicDto(channelManagerDto.getUser());
  }

  private void assertUserPublicDto(final UserPublicDto user) {

    assertEquals(user.getExternalId(), USER_EXTERNAL_ID);
    assertEquals(user.getFirstName(), USER_FIRST_NAME);
    assertEquals(user.getLastName(), USER_LAST_NAME);
    assertEquals(user.getEmail(), USER_EMAIL_ADDRESS);
    assertEquals(user.getTimeZone(), USER_TIME_ZONE);
    assertEquals(user.getJobTitle(), USER_JOB_TITLE);
    assertEquals(user.getLevel(), USER_LEVEL);
    assertEquals(user.getCompanyName(), USER_COMPANY_NAME);
    assertEquals(user.getIndustry(), USER_INDUSTRY);
    assertEquals(user.getCompanySize(), USER_COMPANY_SIZE);
    assertEquals(user.getStateRegion(), USER_STATE_REGION);
    assertEquals(user.getCountry(), USER_COUNTRY);
  }

  private User buildUser() {

    User user = new User();
    user.setId(USER_ID);
    user.setExternalId(USER_EXTERNAL_ID);
    user.setIsComplete(USER_IS_COMPLETE);
    user.setIsActive(USER_IS_ACTIVE);
    user.setRealmUserId(USER_REALM_USER_ID);
    user.setFirstName(USER_FIRST_NAME);
    user.setLastName(USER_LAST_NAME);
    user.setEmail(USER_EMAIL_ADDRESS);
    user.setTimeZone(USER_TIME_ZONE);
    user.setApiKey(USER_API_KEY);
    user.setCreated(USER_CREATED);
    user.setLastLogin(USER_LAST_LOGIN);
    user.setTelephone(USER_TELEPHONE);
    user.setJobTitle(USER_JOB_TITLE);
    user.setLevel(USER_LEVEL);
    user.setCompanyName(USER_COMPANY_NAME);
    user.setIndustry(USER_INDUSTRY);
    user.setCompanySize(USER_COMPANY_SIZE);
    user.setStateRegion(USER_STATE_REGION);
    user.setCountry(USER_COUNTRY);

    return user;
  }

}
