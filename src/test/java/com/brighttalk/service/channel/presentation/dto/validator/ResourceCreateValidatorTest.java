/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceCreateValidatorTest.java 63781 2013-04-29 10:40:35Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.error.InvalidResourceException;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourceLinkDto;

/**
 * Unit tests for {@link ResourceCreateValidator}.
 */
public class ResourceCreateValidatorTest extends AbstractResourceValidatorTest {

  private AbstractResourceValidator uut;

  @Before
  public void setUp() {
    uut = new ResourceCreateValidator();

  }

  /**
   * Test validation of external file resource on creation with missing title .
   */
  @Test
  public void testValidateResourceWithMissingTitle() {

    // setup
    ResourceDto resource = createExternalFileResource(null, RESOURCE_URL, RESOURCE_DESCRIPTION, RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      Assert.fail();
    } catch (InvalidResourceException ie) {
      // assert
      Assert.assertEquals("ResourceTitleMissing", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of external file resource on creation with empty title .
   */
  @Test
  public void testValidateResourceWithEmptyTitle() {

    // setup
    ResourceDto resource = createExternalFileResource("", RESOURCE_URL, RESOURCE_DESCRIPTION, RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceTitleMissing", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of external file resource on creation with title too long .
   */
  @Test
  public void testValidateResourceWithTooLongTitle() {

    // setup
    ResourceDto resource = createExternalFileResource("", RESOURCE_URL, RESOURCE_DESCRIPTION, RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);
    resource.setTitle("test:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest"
        + ":testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:test");

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceTitleTooLong", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of external file resource on creation with too long .
   */
  @Test
  public void testValidateResourceWithTooLongDescription() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, RESOURCE_URL, null, RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);
    resource.setDescription("test:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
        + "testtest:testtest:test");
    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceDescriptionTooLong", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of external file resource on creation with title and url.
   */
  @Test
  public void testValidateExternalFileResourceWithRequiredFieldTitleAndUrl() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, RESOURCE_URL, null, null, null);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validation of external file resource on creation with title,url, description, size and mimeType .
   */
  @Test
  public void testValidateExternalFileResourceWithOptionalFields() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, RESOURCE_URL, RESOURCE_DESCRIPTION,
        RESOURCE_MIME_TYPE, RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validation of external file resource on creation with missing url .
   */
  @Test
  public void testValidateExternalFileResourceWithMissingUrl() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, null, RESOURCE_DESCRIPTION, RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlMissing", ie.getUserErrorMessage());
    }
  }

  @Test
  public void testValidateExternalFileResourceWithValidUrlWithHttp() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, "http://google.com", RESOURCE_DESCRIPTION,
        RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();

    }
  }

  @Test
  public void testValidateExternalFileResourceWithValidUrlEndingWithPdfFile() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE,
        "http://google.com/3/4/file.pdf?q=123&p=22#start",
        RESOURCE_DESCRIPTION,
        RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();

    }
  }

  @Test
  public void testValidateExternalFileResourceWithValidUrlWithHttps() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, "https://google.com", RESOURCE_DESCRIPTION,
        RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();

    }
  }

  @Test
  public void testValidateExternalFileResourceWithInvalidUrl() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, "javascript:alert('xss')", RESOURCE_DESCRIPTION,
        RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlIsInvalid", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of external file resource on creation with missing url .
   */
  @Test
  public void testValidateExternalFileResourceWithEmptyUrl() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, "", RESOURCE_DESCRIPTION, RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlMissing", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of external file resource on creation with too url .
   */
  @Test
  public void testValidateExternalFileResourceWithTooLongUrl() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, null, RESOURCE_DESCRIPTION, RESOURCE_MIME_TYPE,
        RESOURCE_FILE_SIZE);
    String href = StringUtils.repeat("test", 1000);
    resource.getFile().setHref(href);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlTooLong", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of external file resource on creation with too mime type .
   */
  @Test
  public void testValidateExternalFileResourceWithTooLongMimeType() {

    // setup
    ResourceDto resource = createExternalFileResource(RESOURCE_TITLE, RESOURCE_URL, RESOURCE_DESCRIPTION, null,
        RESOURCE_FILE_SIZE);
    // CHECKSTYLE:OFF
    resource.getFile().setMimeType(
        "test:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:testtest:"
            + "testtest:testtest:test");
    // CHECKSTYLE:ON
    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceMimeTypeTooLong", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of internal file resource on creation with title.
   */
  @Test
  public void testValidateInternalFileResourceWithRequiredFieldsTitle() {

    // setup
    ResourceDto resource = createInternalFileResource(RESOURCE_TITLE, null);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validation of internal file resource on creation with title and description.
   */
  @Test
  public void testValidateInternalFileResourceWithOptionalFieldsDescription() {

    // setup
    ResourceDto resource = createInternalFileResource(RESOURCE_TITLE, RESOURCE_DESCRIPTION);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validation of internal file resource on creation with provided url .
   */
  @Test
  public void testValidateInternalFileResourceWithNotAllowedUrl() {

    // setup
    ResourceDto resource = createInternalFileResource(RESOURCE_TITLE, RESOURCE_DESCRIPTION);
    resource.getFile().setHref(RESOURCE_URL);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlNotAllowed", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of internal file resource on creation with provided file size .
   */
  @Test
  public void testValidateInternalFileResourceWithNotAllowedFileSize() {

    // setup
    ResourceDto resource = createInternalFileResource(RESOURCE_TITLE, RESOURCE_DESCRIPTION);
    resource.getFile().setSize(RESOURCE_FILE_SIZE);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceSizeNotAllowed", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of internal file resource on creation with provided mime type .
   */
  @Test
  public void testValidateInternalFileResourceWithNotAllowedMimeType() {

    // setup
    ResourceDto resource = createInternalFileResource(RESOURCE_TITLE, RESOURCE_DESCRIPTION);
    resource.getFile().setMimeType(RESOURCE_MIME_TYPE);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceMimeTypeNotAllowed", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of link resource on creation withall required fields title and url .
   */
  @Test
  public void testValidateLinkResourceWithRequiredFieldsTitleAndUrl() {

    // setup
    ResourceDto resource = createLinkResource(RESOURCE_TITLE, RESOURCE_URL, null);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validation of link resource on creation with optional fields description.
   */
  @Test
  public void testValidateLinkResourceWithOptionalFieldDescription() {

    // setup
    ResourceDto resource = createLinkResource(RESOURCE_TITLE, RESOURCE_URL, RESOURCE_DESCRIPTION);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();
    }
  }

  /**
   * Test validation of link resource on creation with url being empty .
   */
  @Test
  public void testValidateLinkResourceWithEmptyUrl() {

    // setup
    ResourceDto resource = createLinkResource(RESOURCE_TITLE, null, RESOURCE_DESCRIPTION);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlMissing", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of link resource on creation with missing url .
   */
  @Test
  public void testValidateLinkResourceWithMissingUrl() {

    // setup
    ResourceDto resource = createLinkResource(RESOURCE_TITLE, null, RESOURCE_DESCRIPTION);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlMissing", ie.getUserErrorMessage());
    }
  }

  @Test
  public void testValidateLinkResourceWithValidUrlWithHttp() {

    // setup
    ResourceDto resource = createLinkResource(RESOURCE_TITLE, "http://google.com", RESOURCE_DESCRIPTION);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();

    }
  }

  @Test
  public void testValidateLinkResourceWithValidUrlWithHttps() {

    // setup
    ResourceDto resource = createLinkResource(RESOURCE_TITLE, "https://google.com", RESOURCE_DESCRIPTION);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();

    }
  }

  @Test
  public void testValidateLinkResourceWithValidUrlEndingWithPdfFiles() {

    // setup
    ResourceDto resource = createLinkResource(RESOURCE_TITLE, "http://google.com/3/4/file.pdf?q=123&p=22#start",
        RESOURCE_DESCRIPTION);

    // do test
    try {
      uut.validate(resource);
      // assert
    } catch (InvalidResourceException ie) {
      Assert.fail();

    }
  }

  @Test
  public void testValidateLinkResourceWithinvalidValidUrl() {

    // setup
    ResourceDto resource = createLinkResource(RESOURCE_TITLE, "javascript:alert('xss')", RESOURCE_DESCRIPTION);

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlIsInvalid", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of link resource on creation with too long url .
   */
  @Test
  public void testValidateLinkResourceWithTooLongUrl() {

    // setup
    ResourceDto resource = createLinkResource(RESOURCE_TITLE, null, RESOURCE_DESCRIPTION);
    String href = StringUtils.repeat("test", 1000);
    resource.getLink().setHref(href);
    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("ResourceUrlTooLong", ie.getUserErrorMessage());
    }
  }

  /**
   * Test validation of  resource with link and file .
   */
  @Test
  public void testValidateResourceWithLinkAndFileSpecified() {

    // setup
    ResourceDto resource = createInternalFileResource(RESOURCE_TITLE, RESOURCE_DESCRIPTION);
    resource.setLink(new ResourceLinkDto());

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("InvalidResource", ie.getUserErrorCode());
    }
  }

  /**
   * Test validation of resource with invalid hosted value.
   */
  @Test
  public void testValidateResourceFileWithInvalidHostedValue() {

    // setup
    ResourceDto resource = createInternalFileResource(RESOURCE_TITLE, RESOURCE_DESCRIPTION);
    resource.getFile().setHosted("test");

    // do test
    try {
      uut.validate(resource);
      // assert
      Assert.fail();
    } catch (InvalidResourceException ie) {
      Assert.assertEquals("InvalidResource", ie.getUserErrorCode());
    }
  }

  /**
   * Test validation of resource with file details na dnull hosted value .
   */
  @Test
  public void testValidateResourceFileWithInvalidHostedName() {

    // setup
    ResourceDto resource = createInternalFileResource(RESOURCE_TITLE, RESOURCE_DESCRIPTION);
    resource.getFile().setHosted(null);

    // do test
    try {
      uut.validate(resource);
      Assert.fail();
    } catch (InvalidResourceException ie) {
      // assert
      Assert.assertEquals("InvalidResource", ie.getUserErrorCode());
    }
  }

}
