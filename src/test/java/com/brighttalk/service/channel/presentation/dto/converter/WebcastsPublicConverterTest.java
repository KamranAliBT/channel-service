/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastsPublicConverterTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.net.URL;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.WebcastPublicDto;
import com.brighttalk.service.channel.presentation.dto.WebcastsPublicDto;

public class WebcastsPublicConverterTest {

  private WebcastsPublicConverter uut;

  private WebcastPublicConverter mockWebcastPublicConverter;

  private DateTimeFormatter formatter;

  private ConverterContext converterContext;

  @Before
  public void setUp() throws Exception {

    uut = new WebcastsPublicConverter();

    mockWebcastPublicConverter = EasyMock.createMock(WebcastPublicConverter.class);
    ReflectionTestUtils.setField(uut, "webcastPublicConverter", mockWebcastPublicConverter);

    formatter = EasyMock.createMock(DateTimeFormatter.class);

    converterContext = new ConverterContext();
    converterContext.setRequestUrl(new URL("http://www.test.brighttalk.net"));
    converterContext.setDateTimeFormatter(formatter);
  }

  @Test
  public void convertWhenNullCommunications() {

    // do test
    WebcastsPublicDto result = uut.convert(null, converterContext);

    assertNotNull(result);
    assertNull(result.getWebcasts());
    assertNull(result.getLinks());
  }

  @Test
  public void convertWhenEmptyCommunications() {

    // do test
    WebcastsPublicDto result = uut.convert(new PaginatedList<Communication>(), converterContext);

    assertNotNull(result);
    assertNull(result.getWebcasts());
    assertNull(result.getLinks());
  }

  @Test
  public void convertWebcastDontIncludeLinks() {

    final PaginatedList<Communication> communications = new PaginatedList<Communication>();
    final Communication communication = new Communication();
    communications.add(communication);
    communications.setIsFinalPage(true);
    communications.setCurrentPageNumber(1);

    final WebcastPublicDto webcastDto = new WebcastPublicDto();
    EasyMock.expect(mockWebcastPublicConverter.convert(communication, formatter)).andReturn(webcastDto);
    EasyMock.replay(mockWebcastPublicConverter);

    // do test
    WebcastsPublicDto result = uut.convert(communications, converterContext);

    assertNotNull(result);
    assertNotNull(result.getWebcasts());
    assertEquals(1, result.getWebcasts().size());
    assertEquals(webcastDto, result.getWebcasts().get(0));
    assertNull(result.getLinks());
  }

  @Test
  public void convertMultipleWebcastsDontIncludeLinks() {

    Communication communication1 = new Communication();
    Communication communication2 = new Communication();
    final PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(communication1);
    communications.add(communication2);
    communications.setIsFinalPage(true);
    communications.setCurrentPageNumber(1);

    EasyMock.expect(mockWebcastPublicConverter.convert(communication1, formatter)).andReturn(
        new WebcastPublicDto());
    EasyMock.expect(mockWebcastPublicConverter.convert(communication2, formatter)).andReturn(
        new WebcastPublicDto());
    EasyMock.replay(mockWebcastPublicConverter);

    // do test
    WebcastsPublicDto result = uut.convert(communications, converterContext);

    assertNotNull(result);
    assertNotNull(result.getWebcasts());
    assertEquals(2, result.getWebcasts().size());
    assertNull(result.getLinks());

    EasyMock.verify(mockWebcastPublicConverter);
  }

  @Test
  public void convertIncludeLinksWhenNextAvailable() {

    final PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(new Communication());
    communications.setIsFinalPage(false);
    communications.setCurrentPageNumber(1);

    // do test
    final WebcastsPublicDto result = uut.convert(communications, converterContext);

    assertNotNull(result.getLinks());
    assertEquals(1, result.getLinks().size());
  }

  @Test
  public void convertIncludeLinksWhenPreviousAndNextAvailable() {

    final PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(new Communication());
    communications.setIsFinalPage(false);
    communications.setCurrentPageNumber(2);

    // do test
    final WebcastsPublicDto result = uut.convert(communications, converterContext);

    assertNotNull(result.getLinks());
    assertEquals(2, result.getLinks().size());
  }

  @Test
  public void convertPreviousLink() throws Exception {

    final PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(new Communication());
    communications.setIsFinalPage(true);
    communications.setCurrentPageNumber(2);
    communications.setPageSize(2);

    converterContext.setRequestUrl(new URL("http://www.local.brighttalk.net?page=2"));

    // do test
    WebcastsPublicDto result = uut.convert(communications, converterContext);

    assertNotNull(result.getLinks());

    final LinkDto resultPreviousLink = result.getLinks().get(0);

    assertNotNull(resultPreviousLink);
    assertNull(resultPreviousLink.getTitle());

    final String expectedHref = "http://www.local.brighttalk.net?page=1&pageSize=2";
    assertEquals(expectedHref, resultPreviousLink.getHref());

    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, resultPreviousLink.getRel());
  }

  @Test
  public void convertNextLink() throws Exception {

    final PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(new Communication());
    communications.setIsFinalPage(false);
    communications.setCurrentPageNumber(1);
    communications.setPageSize(1);

    converterContext.setRequestUrl(new URL("http://www.local.brighttalk.net?page=1&pageSize=1"));

    // do test
    WebcastsPublicDto result = uut.convert(communications, converterContext);

    assertNotNull(result.getLinks());

    final LinkDto resultPreviousLink = result.getLinks().get(0);

    assertNotNull(resultPreviousLink);
    assertNull(resultPreviousLink.getTitle());

    final String expectedHref = "http://www.local.brighttalk.net?page=2&pageSize=1";
    assertEquals(expectedHref, resultPreviousLink.getHref());

    assertEquals(LinkDto.RELATIONSHIP_NEXT, resultPreviousLink.getRel());
  }
}