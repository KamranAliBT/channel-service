/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EventControllerTest.java 81759 2014-08-11 10:53:40Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.isA;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.service.user.UserService;
import com.brighttalk.service.channel.presentation.dto.UserDto;

/**
 * Unit tests for {@link EventController}.
 */
public class EventControllerTest {

  private static final Long USER_ID = 999L;

  private UserService mockUserService;

  private EventController uut;

  @Before
  public void setUp() {
    uut = new EventController();
    mockUserService = createMock(UserService.class);
    uut.setUserService(mockUserService);
  }

  /**
   * Tests {@link EventController#notifyUserDeleted} in the success case of notifying the user has been deleted.
   */
  @Test
  public void testNotifyUserDeleted() {
    // Expectations
    mockUserService.delete(isA(Long.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockUserService);

    UserDto testUser = new UserDto(USER_ID);

    // Do test
    uut.notifyUserDeleted(testUser);

    // Assertions
    EasyMock.verify(mockUserService);
  }

}
