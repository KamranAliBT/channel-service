/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AtomFeedConverterTest.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.PaginatedChannelFeed;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.atom.AtomFeedDto;
import com.brighttalk.service.channel.presentation.dto.atom.FeedEntryDto;
import com.brighttalk.service.channel.presentation.util.ChannelFeedSearchCriteriaBuilder;

public class AtomFeedConverterTest {

  private static final String CHANNEL_TITLE = "channel-title";

  private static final Long CHANNEL_ID = 666L;

  private static final int CREATED_YEAR = 1994;

  private static final int LAST_UPDATED_YEAR = 2004;

  private static final int LAST_UPDATED_MONTH = 2;

  private static final int LAST_UPDATED_DAY = 13;

  private static final int LAST_UPDATED_HOUR = 10;

  private static final int LAST_UPDATED_MINUTE = 21;

  private static final int LAST_UPDATED_SECOND = 0;

  private static final String CHANNEL_DESCRIPTION = "channelDescription";

  private static final String CHANNEL_STRAPLINE = "channelStrapline";

  private static final String CHANNEL_URL = "http://www.local.brighttalk.net/feed/";

  private static final String CHANNEL_FEED_ATOM_URL = "http://www.local.brighttalk.net/atomFeed/";

  private AtomFeedConverter uut;

  private AtomFeedEntryConverter mockEntryConverter;

  @Before
  public void setUp() {

    uut = new AtomFeedConverter();

    mockEntryConverter = EasyMock.createMock(AtomFeedEntryConverter.class);
    uut.setEntryConverter(mockEntryConverter);
  }

  @Test
  public void convert() {

    // Set up
    List<Communication> communications = new ArrayList<>();
    int totalNumberOfCommunications = 0;
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertNotNull(result);
    assertNull(result.getEntries());
  }

  @Test
  public void convertId() {

    // Set up
    List<Communication> communications = new ArrayList<>();
    int totalNumberOfCommunications = 0;
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    final String expectedChannelId = AtomFeedDto.BRIGHTTALK_TAG + CREATED_YEAR + ":channel:" + CHANNEL_ID;

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertEquals(expectedChannelId, result.getId());
  }

  @Test
  public void convertTitle() {

    // Set up
    List<Communication> communications = new ArrayList<>();
    int totalNumberOfCommunications = 0;
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertEquals(CHANNEL_TITLE, result.getTitle());
  }

  @Test
  public void convertDescription() {

    // Set up
    List<Communication> communications = new ArrayList<>();
    int totalNumberOfCommunications = 0;
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertEquals(CHANNEL_DESCRIPTION, result.getDescription());
  }

  @Test
  public void convertStrapline() {

    // Set up
    List<Communication> communications = new ArrayList<>();
    int totalNumberOfCommunications = 0;
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertEquals(CHANNEL_STRAPLINE, result.getSubtitle());
  }

  @Test
  public void convertUpdated() {

    // Set up
    List<Communication> communications = new ArrayList<>();
    int totalNumberOfCommunications = 0;
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    final String expectedUpdated = createExpectedUpdated();

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertEquals(expectedUpdated, result.getUpdated());
  }

  @Test
  public void convertEntries() {

    // Set up
    List<Communication> communications = Arrays.asList(new Communication(), new Communication());
    int totalNumberOfCommunications = 0;
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    final int expectedEntriesCount = 2;

    // Expectations
    EasyMock.expect(mockEntryConverter.convert(isA(Communication.class))).andReturn(new FeedEntryDto()).times(2);
    EasyMock.replay(mockEntryConverter);

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertNotNull(result.getEntries());

    assertEquals(expectedEntriesCount, result.getEntries().size());

    EasyMock.verify(mockEntryConverter);
  }

  @Test
  public void convertLinksMinimalCase() {

    // Set up
    final int pageNumber = 2;
    final int pageSize = 5;

    List<Communication> communications = new ArrayList<>();
    int totalNumberOfCommunications = 0;

    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();
    searchCriteria.setPageNumber(pageNumber);
    searchCriteria.setPageSize(pageSize);

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    String expectedLinkHref = buildExpectedLinkHref(pageNumber, pageSize);
    final int expectedLinksCount = 1;

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertNotNull(result.getLinks());

    assertEquals(expectedLinksCount, result.getLinks().size());

    LinkDto resultChannelAtomFeedLink = result.getLinks().get(0);

    assertEquals(expectedLinkHref, resultChannelAtomFeedLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_SELF, resultChannelAtomFeedLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultChannelAtomFeedLink.getType());
    assertNull(resultChannelAtomFeedLink.getTitle());
  }

  @Test
  public void convertLinksWithChannelUrl() {

    // Set up
    List<Communication> communications = new ArrayList<>();
    int totalNumberOfCommunications = 0;

    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);
    feed.getChannel().setUrl(CHANNEL_URL);

    final int expectedLinksCount = 2;

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertNotNull(result.getLinks());

    assertEquals(expectedLinksCount, result.getLinks().size());

    LinkDto resultChannelUrlLink = result.getLinks().get(1);

    assertEquals(CHANNEL_URL, resultChannelUrlLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_ALTERNATE, resultChannelUrlLink.getRel());
    assertEquals(LinkDto.TYPE_TEXT_HTML, resultChannelUrlLink.getType());
    assertNull(resultChannelUrlLink.getTitle());
  }

  @Test
  public void convertLinksForFirstPage() {

    // Set up
    final int pageNumber = 1;
    final int pageSize = 5;
    final int totalNumberOfCommunications = pageSize * 3;
    List<Communication> communications = Arrays.asList(new Communication());

    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();
    searchCriteria.setPageNumber(pageNumber);
    searchCriteria.setPageSize(pageSize);

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    final int expectedLinksCount = 3;
    String expectedLastLinkHref = buildExpectedLinkHref(3, pageSize);
    String expectedNextLinkHref = buildExpectedLinkHref(2, pageSize);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertNotNull(result.getLinks());

    assertEquals(expectedLinksCount, result.getLinks().size());

    LinkDto resultLastPageLink = result.getLinks().get(1);

    assertEquals(expectedLastLinkHref, resultLastPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_LAST, resultLastPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultLastPageLink.getType());
    assertNull(resultLastPageLink.getTitle());

    LinkDto resultNextPageLink = result.getLinks().get(2);

    assertEquals(expectedNextLinkHref, resultNextPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_NEXT, resultNextPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultNextPageLink.getType());
    assertNull(resultNextPageLink.getTitle());
  }

  /**
   * Convert link for the first page where there is a webcast status filter set.
   */
  @Test
  public void convertLinksForFirstPageWithWebcastStatusFilter() {

    // Set up
    final int pageNumber = 1;
    final int pageSize = 5;
    final int totalNumberOfCommunications = pageSize * 3;
    List<Communication> communications = Arrays.asList(new Communication());

    String filterTypeAsString = null;
    String statusFilterAsString = "upcoming,live";
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().build(pageNumber, pageSize,
        filterTypeAsString, statusFilterAsString);

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    final int expectedLinksCount = 3;
    String expectedSelfLinkHref = buildExpectedLinkHref(1, pageSize, statusFilterAsString);
    String expectedNextLinkHref = buildExpectedLinkHref(2, pageSize, statusFilterAsString);
    String expectedLastLinkHref = buildExpectedLinkHref(3, pageSize, statusFilterAsString);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    assertNotNull(result.getLinks());

    assertEquals(expectedLinksCount, result.getLinks().size());

    LinkDto resultSelfPageLink = result.getLinks().get(0);

    assertEquals(expectedSelfLinkHref, resultSelfPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_SELF, resultSelfPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultSelfPageLink.getType());
    assertNull(resultSelfPageLink.getTitle());

    LinkDto resultLastPageLink = result.getLinks().get(1);

    assertEquals(expectedLastLinkHref, resultLastPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_LAST, resultLastPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultLastPageLink.getType());
    assertNull(resultLastPageLink.getTitle());

    LinkDto resultNextPageLink = result.getLinks().get(2);

    assertEquals(expectedNextLinkHref, resultNextPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_NEXT, resultNextPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultNextPageLink.getType());
    assertNull(resultNextPageLink.getTitle());
  }

  @Test
  public void convertLinksForMiddlePage() {

    // Set up
    final int pageNumber = 3;
    final int pageSize = 5;
    final int totalNumberOfCommunications = pageSize * 5;
    List<Communication> communications = Arrays.asList(new Communication());

    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();
    searchCriteria.setPageNumber(pageNumber);
    searchCriteria.setPageSize(pageSize);

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    final int expectedLinksCount = 5;
    String expectedFirstLinkHref = buildExpectedLinkHref(1, pageSize);
    String expectedLastLinkHref = buildExpectedLinkHref(5, pageSize);
    String expectedPreviousLinkHref = buildExpectedLinkHref(2, pageSize);
    String expectedNextLinkHref = buildExpectedLinkHref(4, pageSize);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertNotNull(result.getLinks());

    assertEquals(expectedLinksCount, result.getLinks().size());

    LinkDto resultFirstPageLink = result.getLinks().get(1);

    assertEquals(expectedFirstLinkHref, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_FIRST, resultFirstPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());

    LinkDto resultLastPageLink = result.getLinks().get(3);

    assertEquals(expectedLastLinkHref, resultLastPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_LAST, resultLastPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultLastPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());

    LinkDto resultPrevousPageLink = result.getLinks().get(2);

    assertEquals(expectedPreviousLinkHref, resultPrevousPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, resultPrevousPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultPrevousPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());

    LinkDto resultNextPageLink = result.getLinks().get(4);

    assertEquals(expectedNextLinkHref, resultNextPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_NEXT, resultNextPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultNextPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  /**
   * Convert link for the middle page where there is a webcast status filter set.
   */
  @Test
  public void convertLinksForMiddlePageWithWebcastStatusFilter() {

    // Set up
    final int pageNumber = 3;
    final int pageSize = 5;
    final int totalNumberOfCommunications = pageSize * 5;
    List<Communication> communications = Arrays.asList(new Communication());

    String filterTypeAsString = null;
    String statusFilterAsString = "upcoming,live";
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().build(pageNumber, pageSize,
        filterTypeAsString, statusFilterAsString);

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    final int expectedLinksCount = 5;
    String expectedSelfLinkHref = buildExpectedLinkHref(3, pageSize, statusFilterAsString);
    String expectedFirstLinkHref = buildExpectedLinkHref(1, pageSize, statusFilterAsString);
    String expectedLastLinkHref = buildExpectedLinkHref(5, pageSize, statusFilterAsString);
    String expectedPreviousLinkHref = buildExpectedLinkHref(2, pageSize, statusFilterAsString);
    String expectedNextLinkHref = buildExpectedLinkHref(4, pageSize, statusFilterAsString);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertNotNull(result.getLinks());

    assertEquals(expectedLinksCount, result.getLinks().size());

    LinkDto resultSelfPageLink = result.getLinks().get(0);

    assertEquals(expectedSelfLinkHref, resultSelfPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_SELF, resultSelfPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultSelfPageLink.getType());
    assertNull(resultSelfPageLink.getTitle());

    LinkDto resultFirstPageLink = result.getLinks().get(1);

    assertEquals(expectedFirstLinkHref, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_FIRST, resultFirstPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());

    LinkDto resultLastPageLink = result.getLinks().get(3);

    assertEquals(expectedLastLinkHref, resultLastPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_LAST, resultLastPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultLastPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());

    LinkDto resultPrevousPageLink = result.getLinks().get(2);

    assertEquals(expectedPreviousLinkHref, resultPrevousPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, resultPrevousPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultPrevousPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());

    LinkDto resultNextPageLink = result.getLinks().get(4);

    assertEquals(expectedNextLinkHref, resultNextPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_NEXT, resultNextPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultNextPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  @Test
  public void convertLinksForLastPage() {

    // Set up
    final int pageNumber = 3;
    final int pageSize = 5;
    final int totalNumberOfCommunications = pageSize * 3;
    List<Communication> communications = Arrays.asList(new Communication());

    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();
    searchCriteria.setPageNumber(pageNumber);
    searchCriteria.setPageSize(pageSize);

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    final int expectedLinksCount = 3;
    String expectedFirstLinkHref = buildExpectedLinkHref(1, pageSize);
    String expectedPreviousLinkHref = buildExpectedLinkHref(2, pageSize);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertNotNull(result.getLinks());

    assertEquals(expectedLinksCount, result.getLinks().size());

    LinkDto resultFirstPageLink = result.getLinks().get(1);

    assertEquals(expectedFirstLinkHref, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_FIRST, resultFirstPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());

    LinkDto resultPreviousPageLink = result.getLinks().get(2);

    assertEquals(expectedPreviousLinkHref, resultPreviousPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, resultPreviousPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultPreviousPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  /**
   * Convert link for the middle page where there is a webcast status filter set.
   */
  @Test
  public void convertLinksForLastPageWithWebcastStatusFilter() {

    // Set up
    final int pageNumber = 3;
    final int pageSize = 5;
    final int totalNumberOfCommunications = pageSize * 3;
    List<Communication> communications = Arrays.asList(new Communication());

    String filterTypeAsString = null;
    String statusFilterAsString = "upcoming,live";
    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().build(pageNumber, pageSize,
        filterTypeAsString, statusFilterAsString);

    PaginatedChannelFeed feed = createChannelFeed(communications, totalNumberOfCommunications, searchCriteria);

    final int expectedLinksCount = 3;
    String expectedSelfLinkHref = buildExpectedLinkHref(3, pageSize, statusFilterAsString);
    String expectedFirstLinkHref = buildExpectedLinkHref(1, pageSize, statusFilterAsString);
    String expectedPreviousLinkHref = buildExpectedLinkHref(2, pageSize, statusFilterAsString);

    // Expectations

    // Do test
    AtomFeedDto result = uut.convert(feed);

    // Assertions
    assertNotNull(result.getLinks());

    assertEquals(expectedLinksCount, result.getLinks().size());

    LinkDto resultSelfPageLink = result.getLinks().get(0);

    assertEquals(expectedSelfLinkHref, resultSelfPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_SELF, resultSelfPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultSelfPageLink.getType());
    assertNull(resultSelfPageLink.getTitle());

    LinkDto resultFirstPageLink = result.getLinks().get(1);

    assertEquals(expectedFirstLinkHref, resultFirstPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_FIRST, resultFirstPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultFirstPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());

    LinkDto resultPreviousPageLink = result.getLinks().get(2);

    assertEquals(expectedPreviousLinkHref, resultPreviousPageLink.getHref());
    assertEquals(LinkDto.RELATIONSHIP_PREVIOUS, resultPreviousPageLink.getRel());
    assertEquals(LinkDto.TYPE_ATOM_XML, resultPreviousPageLink.getType());
    assertNull(resultFirstPageLink.getTitle());
  }

  private String buildExpectedLinkHref(final int pageNumber, final int pageSize) {

    return CHANNEL_FEED_ATOM_URL + "?page=" + pageNumber + "&size=" + pageSize;
  }

  private String buildExpectedLinkHref(int pageNumber, int pageSize, String statusFilterAsString) {

    return buildExpectedLinkHref(pageNumber, pageSize) + "&webcastStatus=" + statusFilterAsString;
  }

  private PaginatedChannelFeed createChannelFeed(List<Communication> communications, int totalNumberOfCommunications,
      ChannelFeedSearchCriteria searchCriteria) {

    Channel channel = new Channel();
    channel.setTitle(CHANNEL_TITLE);
    channel.setId(CHANNEL_ID);
    channel.setDescription(CHANNEL_DESCRIPTION);
    channel.setStrapline(CHANNEL_STRAPLINE);

    List<Feature> features = new ArrayList<Feature>();

    Feature customUrlFeature = new Feature(Type.CUSTOM_URL);
    customUrlFeature.setIsEnabled(false);
    features.add(customUrlFeature);

    channel.setFeatures(features);

    Calendar calendar = Calendar.getInstance();

    calendar.set(CREATED_YEAR, 1, 1);
    Date created = calendar.getTime();
    channel.setCreated(created);

    calendar.set(LAST_UPDATED_YEAR, LAST_UPDATED_MONTH, LAST_UPDATED_DAY, LAST_UPDATED_HOUR, LAST_UPDATED_MINUTE,
        LAST_UPDATED_SECOND);

    Date lastUpdated = calendar.getTime();
    channel.setLastUpdated(lastUpdated);

    channel.setFeedUrlAtom(CHANNEL_FEED_ATOM_URL);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(channel, communications, totalNumberOfCommunications,
        searchCriteria);

    return feed;
  }

  private String createExpectedUpdated() {

    StringBuilder builder = new StringBuilder();
    builder.append(LAST_UPDATED_YEAR).append("-0");
    builder.append(LAST_UPDATED_MONTH + 1).append("-");
    builder.append(LAST_UPDATED_DAY).append("T");
    builder.append(LAST_UPDATED_HOUR).append(":");
    builder.append(LAST_UPDATED_MINUTE).append(":");
    builder.append(LAST_UPDATED_SECOND).append("0Z");

    return builder.toString();
  }
}