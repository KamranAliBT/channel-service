/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodControllerTest.java 101589 2015-10-19 11:41:52Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.time.DateTimeParser;
import com.brighttalk.common.time.Iso8601DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.ContractPeriodNotFoundException;
import com.brighttalk.service.channel.business.service.channel.ContractPeriodService;
import com.brighttalk.service.channel.presentation.dto.ContractPeriodDto;
import com.brighttalk.service.channel.presentation.dto.ContractPeriodsDto;
import com.brighttalk.service.channel.presentation.dto.converter.ContractPeriodConverter;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateContractPeriod;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test for {@link ContractPeriodController}. This controller is very thin so the tests focus on the input and
 * communication with the business layer.
 */
public class ContractPeriodControllerTest {

  //Unit under test
  private ContractPeriodController uut;

  //Dependencies
  private ContractPeriodService mockContractPeriodService;
  private BrighttalkValidator mockBrighttalkValidator;
  private ContractPeriodConverter contractPeriodConverter;

  // Test objects
  private User user;
  private ContractPeriodDto contractPeriodDto;
  private DateTimeFormatter dateTimeFormatter;
  private ContractPeriod contractPeriod;

  @Before
  public void setUp() throws Exception {
    mockContractPeriodService = createMock(ContractPeriodService.class);
    mockBrighttalkValidator = createNiceMock(BrighttalkValidator.class);
    contractPeriodConverter = new ContractPeriodConverter(new DateTimeParser());

    uut = new ContractPeriodController(mockContractPeriodService, mockBrighttalkValidator, contractPeriodConverter);

    user = new User(123l);
    contractPeriodDto = new ContractPeriodDto();
    dateTimeFormatter = new Iso8601DateTimeFormatter();

    //the contract period to send
    contractPeriodDto.setStart("2015-01-01T00:00:00Z");
    contractPeriodDto.setEnd("2016-01-01T00:00:00Z");
    contractPeriodDto.setChannelId("456");

    //the contract period to return from the business layer
    contractPeriod = contractPeriodConverter.convert(contractPeriodDto);
    contractPeriod.setId(789l);
  }

  /**
   * Tests {@link ContractPeriodController#create(User, ContractPeriodDto, DateTimeFormatter)} with a valid Contract
   * Period. The controller should make a call to {@link ContractPeriodService#create(User, ContractPeriod)} in the
   * business layer and return the {@link ContractPeriodDto} correctly transformed.
   *
   * @throws Exception
   */
  @Test
  public void testPostNewValidContractPeriodReturnsConvertedContractPeriodFromBusinessLayer() throws Exception {
    //setup
    expect(mockContractPeriodService.create(anyObject(User.class), anyObject(ContractPeriod.class))).andReturn(
        contractPeriod).times(1);
    replay(mockContractPeriodService);

    //test
    ContractPeriodDto createdContractPeriodDto = uut.create(user, contractPeriodDto, dateTimeFormatter);

    //assert
    assertEquals(contractPeriodDto.getStart(), createdContractPeriodDto.getStart());
    assertEquals(contractPeriodDto.getEnd(), createdContractPeriodDto.getEnd());
    assertEquals(contractPeriodDto.getChannelId(), createdContractPeriodDto.getChannelId());
    assertEquals("789", createdContractPeriodDto.getId());
  }

  /**
   * Tests {@link ContractPeriodController#create(User, ContractPeriodDto, DateTimeFormatter)} with a Contract Period
   * with an invalid channel ID. The controller should make a call to
   * {@link ContractPeriodService#create(User, ContractPeriod)} in the business layer and rethrow the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testPostNewContractPeriodWithInvalidChannelThrowsException() throws Exception {
    //setup
    ChannelNotFoundException cne = new ChannelNotFoundException(456L);
    expect(mockContractPeriodService.create(anyObject(User.class), anyObject(ContractPeriod.class))).andThrow(
        cne).times(1);
    replay(mockContractPeriodService);

    //test
    try {
      uut.create(user, contractPeriodDto, dateTimeFormatter);
      fail("Expected a ChannelNotFoundException");
    } catch (ChannelNotFoundException e) {
      assertEquals(ErrorCode.NOT_FOUND.getName(), e.getUserErrorCode());
      assertEquals(cne.getMessage(), e.getMessage());
    }
  }

  /**
   * Tests {@link ContractPeriodController#update(User, String, ContractPeriodDto, DateTimeFormatter)} with a valid
   * contract to be updated. The controller should call {@link ContractPeriodService#update(User, ContractPeriod)} in
   * the business layer and return the {@link ContractPeriodDto} correctly transformed.
   *
   * @throws Exception
   */
  @Test
  public void testUpdateContractPeriodReturnsConvertedContractPeriodFromBusinessLayer() throws Exception {
    //setup
    String contractPeriodId = "123";
    contractPeriod.setId(Long.parseLong(contractPeriodId));
    expect(mockContractPeriodService.update(anyObject(User.class), anyObject(ContractPeriod.class))).andReturn(
        contractPeriod).times(1);
    replay(mockContractPeriodService);

    ContractPeriodDto updatedCP = uut.update(user, contractPeriodId, contractPeriodDto, dateTimeFormatter);
    assertEquals(contractPeriodDto.getStart(), updatedCP.getStart());
    assertEquals(contractPeriodDto.getEnd(), updatedCP.getEnd());
    assertEquals(contractPeriodDto.getChannelId(), updatedCP.getChannelId());
    assertEquals(contractPeriodId.toString(), updatedCP.getId());
  }

  /**
   * Tests {@link ContractPeriodController#update(User, String, ContractPeriodDto, DateTimeFormatter)} with a
   * Contract Period with an invalid channel ID. The controller should make a call to
   * {@link ContractPeriodService#update(User, ContractPeriod)} in the business layer and rethrow the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testUpdateContractPeriodWithNonExistentChannelIdThrowsException() throws Exception {
    //setup
    ChannelNotFoundException cne = new ChannelNotFoundException(456L);
    expect(mockContractPeriodService.update(anyObject(User.class), anyObject(ContractPeriod.class))).andThrow(
        cne).times(
        1);
    replay(mockContractPeriodService);

    //test
    try {
      uut.update(user, "456", contractPeriodDto, dateTimeFormatter);
      fail("Expected a ChannelNotFoundException");
    } catch (ChannelNotFoundException e) {
      assertEquals(ErrorCode.NOT_FOUND.getName(), e.getUserErrorCode());
      assertEquals(cne.getMessage(), e.getMessage());
    }
  }

  /**
   * Tests {@link ContractPeriodController#findById(User, String, DateTimeFormatter)} with a valid contract period id. The
   * controller should call {@link ContractPeriodService#find(User, Long)} in the business layer and return the
   * {@link ContractPeriodDto} correctly transformed.
   *
   * @throws Exception
   */
  @Test
  public void testFindContractPeriodReturnsConvertedContractPeriodFromBusinessLayer() throws Exception {
    //setup
    Long contractPeriodId = 123l;
    contractPeriod.setId(contractPeriodId);
    expect(mockContractPeriodService.find(anyObject(User.class), anyObject(Long.class))).andReturn(
        contractPeriod).times(1);
    replay(mockContractPeriodService);

    ContractPeriodDto updatedCP = uut.findById(user, contractPeriodId.toString(), dateTimeFormatter);
    assertEquals(contractPeriodDto.getStart(), updatedCP.getStart());
    assertEquals(contractPeriodDto.getEnd(), updatedCP.getEnd());
    assertEquals(contractPeriodDto.getChannelId(), updatedCP.getChannelId());
    assertEquals(contractPeriodId.toString(), updatedCP.getId());
  }

  /**
   * Tests {@link ContractPeriodController#findById(User, String, DateTimeFormatter)} with an invalid Contract Period ID. The
   * controller should make a call to {@link ContractPeriodService#find(User, Long)} in the business layer and rethrow
   * the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testFindContractPeriodWithInvalidContractPeriodIdThrowsException() throws Exception {
    //setup
    replay(mockContractPeriodService);

    //test
    try {
      uut.findById(user, "456ghfhgj", dateTimeFormatter);
      fail("Expected a BadRequestException");
    } catch (BadRequestException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_ID.getName(), e.getUserErrorCode());
    }
  }


  /**
   * Tests {@link ContractPeriodController#findById(User, String, DateTimeFormatter)} with a negative Contract Period ID
   * .The controller should make a call to {@link ContractPeriodService#find(User, Long)} in the business layer and
   * rethrow the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testFindContractPeriodWithNegativeContractPeriodIdThrowsException() throws Exception {
    //setup
    replay(mockContractPeriodService);

    //test
    try {
      uut.findById(user, "-123", dateTimeFormatter);
      fail("Expected a BadRequestException");
    } catch (BadRequestException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_ID.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodController#findById(User, String, DateTimeFormatter)} with an invalid Contract Period ID. The
   * controller should make a call to {@link ContractPeriodService#find(User, Long)} in the business layer and rethrow
   * the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testFindContractPeriodWithNonExistentContractPeriodIdThrowsException() throws Exception {
    //setup
    ContractPeriodNotFoundException cpne = new ContractPeriodNotFoundException(456L);
    expect(mockContractPeriodService.find(anyObject(User.class), anyObject(Long.class))).andThrow(cpne).times(1);
    replay(mockContractPeriodService);

    //test
    try {
      uut.findById(user, "456", dateTimeFormatter);
      fail("Expected a ContractPeriodNotFoundException");
    } catch (ContractPeriodNotFoundException e) {
      assertEquals(ErrorCode.NOT_FOUND.getName(), e.getUserErrorCode());
      assertEquals(cpne.getMessage(), e.getMessage());
    }
  }

  /**
   * Tests {@link ContractPeriodController#findAllByChannel(User, String, DateTimeFormatter)} with an invalid channel ID.
   * The controller should make a call to {@link ContractPeriodService#findByChannelId(User, Long)} in the business layer
   * and rethrow the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testFindAllByChannelIdWithInvalidChannelIdThrowsException() throws Exception {
    //setup
    ChannelNotFoundException cne = new ChannelNotFoundException(456L);
    expect(mockContractPeriodService.findByChannelId(anyObject(User.class), anyObject(Long.class))).andThrow(cne).times(
        1);
    replay(mockContractPeriodService);

    //test
    try {
      uut.findAllByChannel(user, "456gdsfd", dateTimeFormatter);
      fail("Expected a BadRequestException");
    } catch (BadRequestException e) {
      assertEquals(ErrorCode.INVALID_CHANNEL_ID.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodController#findAllByChannel(User, String, DateTimeFormatter)} with a negative channel ID.
   * The controller should make a call to {@link ContractPeriodService#findByChannelId(User, Long)} in the business layer
   * and rethrow the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testFindAllByChannelIdWithNegativeChannelIdThrowsException() throws Exception {
    //setup
    replay(mockContractPeriodService);

    //test
    try {
      uut.findAllByChannel(user, "-123", dateTimeFormatter);
      fail("Expected a BadRequestException");
    } catch (BadRequestException e) {
      assertEquals(ErrorCode.INVALID_CHANNEL_ID.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodController#findAllByChannel(User, String, DateTimeFormatter)} with an invalid channel ID.
   * The controller should make a call to {@link ContractPeriodService#findByChannelId(User, Long)} in the business layer
   * and rethrow the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testFindAllByChannelIdWithNonExistentChannelIdThrowsException() throws Exception {
    //setup
    ChannelNotFoundException cne = new ChannelNotFoundException(456L);
    expect(mockContractPeriodService.findByChannelId(anyObject(User.class), anyObject(Long.class))).andThrow(cne).times(
        1);
    replay(mockContractPeriodService);

    //test
    try {
      uut.findAllByChannel(user, "456", dateTimeFormatter);
      fail("Expected a ChannelNotFoundException");
    } catch (ChannelNotFoundException e) {
      assertEquals(ErrorCode.NOT_FOUND.getName(), e.getUserErrorCode());
      assertEquals(cne.getMessage(), e.getMessage());
    }
  }

  /**
   * Tests {@link ContractPeriodController#findAllByChannel(User, String, DateTimeFormatter)} with a valid channel ID.
   * The controller should make a call to {@link ContractPeriodService#findByChannelId(User, Long)} in the business layer
   * and return the converted {@link com.brighttalk.service.channel.presentation.dto.ContractPeriodsDto}
   *
   * @throws Exception
   */
  @Test
  public void testFindAllByChannelIdWithValidChannelIdReturnsContractPeriods() throws Exception {
    //setup
    ArrayList<ContractPeriod> returnList = new ArrayList<>();
    returnList.add(contractPeriod);
    expect(mockContractPeriodService.findByChannelId(anyObject(User.class), anyObject(Long.class))).andReturn(
        returnList)
        .times(1);
    replay(mockContractPeriodService);

    //test
    ContractPeriodsDto allByChannel = uut.findAllByChannel(user, "456", dateTimeFormatter);
    assertEquals(returnList.size(), allByChannel.getContractPeriods().size());
    for (ContractPeriodDto periodDto : allByChannel.getContractPeriods()) {
      assertEquals(dateTimeFormatter.convert(contractPeriod.getStart()), periodDto.getStart());
      assertEquals(dateTimeFormatter.convert(contractPeriod.getEnd()), periodDto.getEnd());
      assertEquals(contractPeriod.getChannelId().toString(), periodDto.getChannelId());
      assertEquals(contractPeriod.getId().toString(), periodDto.getId());
    }
  }

  /**
   * Tests {@link ContractPeriodController#delete(User, String)} with an invalid Contract Period ID. The
   * controller should make a call to {@link ContractPeriodService#find(User, Long)} in the business layer and rethrow
   * the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testDeleteContractPeriodWithNonExistentContractPeriodIdThrowsException() throws Exception {
    //setup
    ContractPeriodNotFoundException cpne = new ContractPeriodNotFoundException(456L);
    mockContractPeriodService.delete(anyObject(User.class), anyLong());
    expectLastCall().andThrow(cpne);
    replay(mockContractPeriodService);

    //test
    try {
      uut.delete(user, "456");
      fail("Expected a ContractPeriodNotFoundException");
    } catch (ContractPeriodNotFoundException e) {
      assertEquals(ErrorCode.NOT_FOUND.getName(), e.getUserErrorCode());
      assertEquals(cpne.getMessage(), e.getMessage());
    }
  }

  /**
   * Tests {@link ContractPeriodController#delete(User, String)} with an invalid Contract Period ID. The
   * controller should make a call to {@link ContractPeriodService#find(User, Long)} in the business layer and rethrow
   * the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testDeleteContractPeriodWithInvalidContractPeriodIdThrowsException() throws Exception {
    //setup
    ContractPeriodNotFoundException cpne = new ContractPeriodNotFoundException(456L);
    mockContractPeriodService.delete(anyObject(User.class), anyLong());
    expectLastCall().andThrow(cpne);
    replay(mockContractPeriodService);

    //test
    try {
      uut.delete(user, "354678uygb");
      fail("Expected a BadRequestException");
    } catch (BadRequestException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_ID.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodController#delete(User, String)} with a negative Contract Period ID. The
   * controller should make a call to {@link ContractPeriodService#find(User, Long)} in the business layer and rethrow
   * the exception thrown.
   *
   * @throws Exception
   */
  @Test
  public void testDeleteContractPeriodWithNegativeContractPeriodIdThrowsException() throws Exception {
    //setup
    replay(mockContractPeriodService);

    //test
    try {
      uut.delete(user, "-123");
      fail("Expected a BadRequestException");
    } catch (BadRequestException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_ID.getName(), e.getUserErrorCode());
    }
  }
}