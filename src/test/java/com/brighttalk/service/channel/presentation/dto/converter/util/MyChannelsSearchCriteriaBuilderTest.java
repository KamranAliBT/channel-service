/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MyChannelsSearchCriteriaBuilderTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria.SortBy;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.util.MyChannelsSearchCriteriaBuilder;

public class MyChannelsSearchCriteriaBuilderTest {

  private MyChannelsSearchCriteriaBuilder uut;

  private HttpServletRequest mockRequest;

  @Before
  public void setUp() {
    uut = new MyChannelsSearchCriteriaBuilder();

    mockRequest = EasyMock.createMock(HttpServletRequest.class);
  }

  @Test
  public void build() {
    // expectations
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null).times(1);
    EasyMock.replay(mockRequest);

    // do test
    MyChannelsSearchCriteria result = uut.build(mockRequest);

    // assertions
    assertNotNull(result);
    EasyMock.verify(mockRequest);
  }

  @Test
  public void buildDefaultPageNumber() {
    // expectations
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null).times(1);
    EasyMock.replay(mockRequest);

    // do test
    MyChannelsSearchCriteria result = uut.build(mockRequest);

    // assertions
    assertNull(result.getPageNumber());
    EasyMock.verify(mockRequest);
  }

  @Test
  public void buildPageNumber() {
    // set up
    final String pageNumberString = "10";

    // expectations
    final Integer expectedPageNumber = 10;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(pageNumberString).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null).times(1);
    EasyMock.replay(mockRequest);

    // do test
    MyChannelsSearchCriteria result = uut.build(mockRequest);

    // assertions
    assertEquals(expectedPageNumber, result.getPageNumber());
    EasyMock.verify(mockRequest);
  }

  @Test
  public void buildDefaultPageSize() {
    // expectations
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null).times(1);
    EasyMock.replay(mockRequest);

    // do test
    MyChannelsSearchCriteria result = uut.build(mockRequest);

    // assertions
    assertNull(result.getPageSize());
    EasyMock.verify(mockRequest);
  }

  @Test
  public void buildPageSize() {
    // expectations
    final String pageSizeString = "2";
    final Integer expectedPageSize = 2;

    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(pageSizeString).times(1);
    EasyMock.replay(mockRequest);

    // do test
    MyChannelsSearchCriteria result = uut.build(mockRequest);

    // assertions
    assertEquals(expectedPageSize, result.getPageSize());
    EasyMock.verify(mockRequest);
  }

  @Test
  public void buildDefaultSortBy() {
    // expectations
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null).times(1);
    EasyMock.replay(mockRequest);

    // do test
    MyChannelsSearchCriteria result = uut.build(mockRequest);

    // assertions
    assertEquals(SortBy.CREATEDDATE, result.getSortBy());
    EasyMock.verify(mockRequest);
  }

  @Test
  public void buildSortBy() {
    // set up
    final String sortByString = "createdDate";

    // expectations
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(sortByString).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null).times(1);
    EasyMock.replay(mockRequest);

    // do test
    MyChannelsSearchCriteria result = uut.build(mockRequest);

    // assertions
    assertEquals(SortBy.CREATEDDATE, result.getSortBy());
    EasyMock.verify(mockRequest);
  }

  @Test
  public void buildDefaultSortOrder() {
    // expectations
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null).times(1);
    EasyMock.replay(mockRequest);

    // do test
    MyChannelsSearchCriteria result = uut.build(mockRequest);

    // assertions
    assertEquals(SortOrder.DESC, result.getSortOrder());
    EasyMock.verify(mockRequest);
  }

  @Test
  public void buildSortOrder() {
    // set up
    final String sortOrderString = "asc";

    // expectations
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_BY)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.SORT_ORDER)).andReturn(sortOrderString).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_NUMBER)).andReturn(null).times(1);
    EasyMock.expect(mockRequest.getParameter(ApiRequestParam.PAGE_SIZE)).andReturn(null).times(1);
    EasyMock.replay(mockRequest);

    // do test
    MyChannelsSearchCriteria result = uut.build(mockRequest);

    // assertions
    assertEquals(SortOrder.ASC, result.getSortOrder());
    EasyMock.verify(mockRequest);
  }

}