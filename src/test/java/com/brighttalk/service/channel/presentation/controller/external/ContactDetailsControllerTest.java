/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContactDetailsControllerTest.java 88863 2015-01-27 11:35:19Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;


import static org.easymock.EasyMock.createMock;
import static org.junit.Assert.assertEquals;

import javax.validation.ValidationException;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.service.channel.ChannelService;
import com.brighttalk.service.channel.presentation.dto.ContactDetailsPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.ContactDetailsConverter;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateContactDetails;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;
import com.brighttalk.service.utils.DomainFactoryUtils;

/**
 * Unit tests for {@link ContactDetailsController}.
 */
public class ContactDetailsControllerTest {

  private ContactDetailsController uut;

  private BrighttalkValidator mockBrighttalkValidator;
  private ChannelService mockChannelService;
  private ContactDetailsConverter mockContactDetailsConverter;
  
  /**
   * Rule for expected exceptions.
   */
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  /**
   * Test set up.
   */
  @Before
  public void setUp() {
    
    uut = new ContactDetailsController();
    
    mockBrighttalkValidator = createMock(BrighttalkValidator.class);
    
    mockChannelService = EasyMock.createMock(ChannelService.class);
    
    mockContactDetailsConverter = EasyMock.createMock(ContactDetailsConverter.class);

    ReflectionTestUtils.setField(uut, "validator", mockBrighttalkValidator);
    ReflectionTestUtils.setField(uut, "channelService", mockChannelService);
    ReflectionTestUtils.setField(uut, "contactDetailsConverter", mockContactDetailsConverter);
  }

  /**
   * Success case of using {@link ContactDetailsController#getContactDetails(User, Long)} to get contact details for a
   * channel.
   */
  @Test
  public void getContactDetails() {
    
    // Set up
    User user = new User(1L);
    Long channelId = 2L;
    
    ContactDetails contactDetails = DomainFactoryUtils.createContactDetails();
    ContactDetailsPublicDto contactDetailsPublicDto = DomainFactoryUtils.createContactDetailsPublicDto();
    
    // Expectations
    EasyMock.expect(mockChannelService.getContactDetails(user, channelId)).andReturn(contactDetails);
    EasyMock.expect(mockContactDetailsConverter.convertForPublic(contactDetails)).andReturn(contactDetailsPublicDto);
    EasyMock.replay(mockChannelService, mockContactDetailsConverter);
    
    // Do test
    ContactDetailsPublicDto result = uut.getContactDetails(user, channelId);
    
    // Assertions
    EasyMock.verify(mockChannelService, mockContactDetailsConverter);
    assertContactDetailsPublicDtoAreEqual(contactDetailsPublicDto, result);
  }
  
  /**
   * Using {@link ContactDetailsController#getContactDetails(User, Long)} to get contact details for a channel that does
   * not exist results in an exception being thrown by the {@link ChannelService}. The controller will in turn throw
   * an instance of {@link ResourceNotFoundException}.
   */
  @Test
  public void getContactDetailsWhenChannelNotFound() {
    
    // Set up
    User user = new User(1L);
    Long channelId = 2L;
        
    // Expectations
    EasyMock.expect(mockChannelService.getContactDetails(user, channelId)).andThrow(new ChannelNotFoundException(channelId));
    EasyMock.replay(mockChannelService, mockContactDetailsConverter);
    
    expectedException.expect(ResourceNotFoundException.class);
    
    // Do test
    uut.getContactDetails(user, channelId);
    
    // Assertions
  }
  
  /**
   * Using {@link ContactDetailsController#getContactDetails(User, Long)} to get contact details for a channel when the
   * user is not authorised to access the contact details for that channel, result in an exception being thrown by
   * the {@link ChannelService}. The controller will in turn throw an instance of {@link NotAuthorisedException}.
   */
  @Test
  public void getContactDetailsWhenUserNotAuthorised() {
    
    // Set up
    User user = new User(1L);
    Long channelId = 2L;
        
    // Expectations
    EasyMock.expect(mockChannelService.getContactDetails(user, channelId)).andThrow(new UserAuthorisationException(""));
    EasyMock.replay(mockChannelService, mockContactDetailsConverter);
    
    expectedException.expect(NotAuthorisedException.class);
    
    // Do test
    uut.getContactDetails(user, channelId);
    
    // Assertions
  }
  
  /**
   * Using {@link ContactDetailsController#updateContactDetails(User, Long, ContactDetailsPublicDto)} to update contact
   * details for a channel.
   */
  @Test
  public void updateContactDetails() {
    
    // Set up
    User user = new User(1L);
    Long channelId = 2L;
    
    ContactDetailsPublicDto contactDetailsPublicDto = DomainFactoryUtils.createContactDetailsPublicDto();
    ContactDetails contactDetails = DomainFactoryUtils.createContactDetails();
    
    // Expectations
    mockBrighttalkValidator.validate(contactDetailsPublicDto, UpdateContactDetails.class);
    EasyMock.expectLastCall();
    EasyMock.expect(mockContactDetailsConverter.convertFromPublic(contactDetailsPublicDto)).andReturn(contactDetails);
    EasyMock.expect(mockChannelService.updateContactDetails(user, channelId, contactDetails)).andReturn(contactDetails);
    EasyMock.expect(mockContactDetailsConverter.convertForPublic(contactDetails)).andReturn(contactDetailsPublicDto);
    EasyMock.replay(mockBrighttalkValidator, mockChannelService, mockContactDetailsConverter);
    
    // Do test
    ContactDetailsPublicDto result = uut.updateContactDetails(user, channelId, contactDetailsPublicDto);
    
    // Assertions
    EasyMock.verify(mockBrighttalkValidator, mockChannelService, mockContactDetailsConverter);
    assertContactDetailsPublicDtoAreEqual(contactDetailsPublicDto, result);
  }
  
  /**
   * Using {@link ContactDetailsController#updateContactDetails(User, Long, ContactDetailsPublicDto)} to update contact
   * details for a channel that does not exists, resulting in an exception being thrown by the {@link ChannelService}.
   * The controller will in return throw an instance of {@link ResourceNotFoundException}.
   */
  @Test
  public void updateContactDetailsWhenChannelNotFound() {
    
    // Set up
    User user = new User(1L);
    Long channelId = 2L;
    
    ContactDetailsPublicDto contactDetailsPublicDto = DomainFactoryUtils.createContactDetailsPublicDto();
    ContactDetails contactDetails = DomainFactoryUtils.createContactDetails();
    
    // Expectations
    mockBrighttalkValidator.validate(contactDetailsPublicDto, UpdateContactDetails.class);
    EasyMock.expectLastCall();
    EasyMock.expect(mockContactDetailsConverter.convertFromPublic(contactDetailsPublicDto)).andReturn(contactDetails);
    EasyMock.expect(mockChannelService.updateContactDetails(user, channelId, contactDetails)).andThrow(
        new ChannelNotFoundException(channelId));
    EasyMock.replay(mockBrighttalkValidator, mockChannelService, mockContactDetailsConverter);
    
    expectedException.expect(ResourceNotFoundException.class);
    
    // Do test
    uut.updateContactDetails(user, channelId, contactDetailsPublicDto);
    
    // Assertions
  }
  
  /**
   * Using {@link ContactDetailsController#updateContactDetails(User, Long, ContactDetailsPublicDto)} to update contact
   * details for a channel when the user is not authorised, resulting in an exception being thrown by the {@link ChannelService}.
   * The controller will in return throw an instance of {@link NotAuthorisedException}.
   */
  @Test
  public void updateContactDetailsWhenUserNotAuthorised() {
    
    // Set up
    User user = new User(1L);
    Long channelId = 2L;
    
    ContactDetailsPublicDto contactDetailsPublicDto = DomainFactoryUtils.createContactDetailsPublicDto();
    ContactDetails contactDetails = DomainFactoryUtils.createContactDetails();
    
    // Expectations
    mockBrighttalkValidator.validate(contactDetailsPublicDto, UpdateContactDetails.class);
    EasyMock.expectLastCall();
    EasyMock.expect(mockContactDetailsConverter.convertFromPublic(contactDetailsPublicDto)).andReturn(contactDetails);
    EasyMock.expect(mockChannelService.updateContactDetails(user, channelId, contactDetails)).andThrow(
        new UserAuthorisationException(""));
    EasyMock.replay(mockBrighttalkValidator, mockChannelService, mockContactDetailsConverter);
    
    expectedException.expect(NotAuthorisedException.class);
    
    // Do test
    uut.updateContactDetails(user, channelId, contactDetailsPublicDto);
    
    // Assertions
  }
  
  /**
   * Using {@link ContactDetailsController#updateContactDetails(User, Long, ContactDetailsPublicDto)} to update contact
   * details for a channel when the contact details are invalid, resulting in a validation exception being thrown.
   * The controller will in return throw an instance of {@link ValidationException}.
   */
  @Test
  public void updateContactDetailsWhenContactDetailsAreInvalid() {
    
    // Set up
    User user = new User(1L);
    Long channelId = 2L;
    
    ContactDetailsPublicDto contactDetailsPublicDto = DomainFactoryUtils.createContactDetailsPublicDto();
    
    // Expectations
    mockBrighttalkValidator.validate(contactDetailsPublicDto, UpdateContactDetails.class);
    EasyMock.expectLastCall().andThrow(new ValidationException());
    EasyMock.replay(mockBrighttalkValidator);
    
    expectedException.expect(ValidationException.class);
    
    // Do test
    uut.updateContactDetails(user, channelId, contactDetailsPublicDto);
    
    // Assertions
  }
  
  private void assertContactDetailsPublicDtoAreEqual(ContactDetailsPublicDto expected, ContactDetailsPublicDto actual) {
    
    assertEquals(expected.getFirstName(), actual.getFirstName());
    assertEquals(expected.getLastName(), actual.getLastName());
    assertEquals(expected.getEmail(), actual.getEmail());
    assertEquals(expected.getTelephone(), actual.getTelephone());
    assertEquals(expected.getJobTitle(), actual.getJobTitle());
    assertEquals(expected.getCompanyName(), actual.getCompanyName());
    assertEquals(expected.getAddress1(), actual.getAddress1());
    assertEquals(expected.getAddress2(), actual.getAddress2());
    assertEquals(expected.getCity(), actual.getCity());
    assertEquals(expected.getStateRegion(), actual.getStateRegion());
    assertEquals(expected.getPostcode(), actual.getPostcode());
    assertEquals(expected.getCountry(), actual.getCountry());
  }
}
