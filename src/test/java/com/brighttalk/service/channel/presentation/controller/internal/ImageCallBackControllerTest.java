/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastControllerTest.java 75238 2014-03-14 14:55:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import static org.easymock.EasyMock.createMock;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.domain.ImageConverterSettings;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.ImageConverterSettingsBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationUpdateService;
import com.brighttalk.service.channel.presentation.dto.TaskDto;
import com.brighttalk.service.channel.presentation.dto.converter.TaskConverter;

/**
 * Unit tests for {@link WebcastController}.
 */
public class ImageCallBackControllerTest {

  private static final Long WEBCAST_ID = 115L;

  private ImageCallBackController uut;

  private CommunicationFinder mockCommunicationFinder;

  private CommunicationUpdateService mockCommunicationUpdateService;

  private TaskConverter mockTaskConverter;

  @Before
  public void setUp() {

    uut = new ImageCallBackController();
    uut.setSanBaseUrl("/mns/san/sanBaseUrl/");
    uut.setWebBaseUrl("webBaseUrl");

    mockCommunicationFinder = createMock(CommunicationFinder.class);
    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);

    mockCommunicationUpdateService = createMock(CommunicationUpdateService.class);
    ReflectionTestUtils.setField(uut, "communicationUpdateService", mockCommunicationUpdateService);

    mockTaskConverter = EasyMock.createMock(TaskConverter.class);
    ReflectionTestUtils.setField(uut, "taskConverter", mockTaskConverter);
  }

  @Test
  public void testUpdateFeatureImageForPreviewSuccess() {
    // setup
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    TaskDto expectedDto = new TaskDto();
    expectedDto.setStatus(ImageConverterSettings.Status.COMPLETE.toString().toLowerCase());
    ImageConverterSettings imageConverterSettings = new ImageConverterSettingsBuilder().withDefaultValues().build();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(WEBCAST_ID)).andReturn(communication);
    EasyMock.expect(mockTaskConverter.convert(expectedDto)).andReturn(imageConverterSettings);
    mockCommunicationUpdateService.updateFeatureImage(communication);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockTaskConverter, mockCommunicationUpdateService);

    // do test
    uut.updateFeatureImage(WEBCAST_ID, "preview", expectedDto);

    // assertions
    EasyMock.verify(mockCommunicationFinder, mockTaskConverter, mockCommunicationUpdateService);
  }

  @Test
  public void testUpdateFeatureImageForNotProcessingWhenStatusIsDifferentThanComplete() {
    // setup
    TaskDto expectedDto = new TaskDto();
    expectedDto.setStatus(ImageConverterSettings.Status.FAILED.toString().toLowerCase());

    // expectations

    // do test
    uut.updateFeatureImage(WEBCAST_ID, "preview", expectedDto);

    // assertions
  }

}
