/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: WebcastSyndicationConverterTest.java 98862 2015-08-06 16:42:30Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.syndication.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationStatisticsBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationSyndicationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationAuthorisationStatus;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationAuthorisationType;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndications;
import com.brighttalk.service.channel.presentation.dto.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.statistics.CommunicationStatisticsDto;
import com.brighttalk.service.channel.presentation.dto.syndication.AuthorisationDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationsDto;

/**
 * Unit Test for {@link WebcastSyndicationConverter}.
 */
public class WebcastSyndicationConverterTest {

  private static final long MASTER_CHANNEL_ID = 8568658l;
  private static final String MASTER_CHANNEL_TITLE = "Test master channel title";
  private static final String MASTER_CHANNEL_ORGANISATION = "Test master channel organisation";
  private static final long CONSUMER_CHANNEL_ID = 687768l;
  private static final String CONSUMER_CHANNEL_TITLE = "Test consumer channel title";
  private static final String CONSUMER_CHANNEL_ORGANISATION = "Test consumer channel organisation";
  private static final String COMM_TITLE = "Test communication title";
  private static final String MASTER_APPROVAL_DATE = "2015-02-12T12:19:44";
  private static final String CONSUMER_APPROVAL_DATE = "2015-02-13T14:19:44";

  private WebcastSyndicationConverter uut;

  private ChannelBuilder channelBuilder;
  private CommunicationBuilder commBuilder;
  private CommunicationSyndicationBuilder commSyndicationBuilder;
  private CommunicationStatisticsBuilder commStatisticsBuilder;
  private DateTimeFormatter mockFormatter;

  /**
   * Test setup.
   */
  @Before
  public void setup() {
    mockFormatter = EasyMock.createMock(DateTimeFormatter.class);
    this.uut = new WebcastSyndicationConverter();
    this.channelBuilder = new ChannelBuilder();
    this.commBuilder = new CommunicationBuilder();
    this.commSyndicationBuilder = new CommunicationSyndicationBuilder();
    this.commStatisticsBuilder = new CommunicationStatisticsBuilder();
  }

  /**
   * Tests {@link CommunicationSyndicationConverter#convert} in the success case of converting
   * {@link CommunicationSyndications} to {@link WebcastSyndicationsDto}.
   */
  @Test
  public void convertCommunicationSyndicationsSuccess() {
    // Setup -
    CommunicationSyndications communicationSyndications = new CommunicationSyndications();
    Communication communication = this.commBuilder.withTitle(COMM_TITLE).build();
    communicationSyndications.setCommunication(communication);
    Channel masterChannel = this.channelBuilder.withId(MASTER_CHANNEL_ID).withTitle(MASTER_CHANNEL_TITLE).
        withOrganisation(MASTER_CHANNEL_ORGANISATION).build();
    communicationSyndications.setChannel(masterChannel);
    CommunicationSyndication communicationSyndication1 = this.commSyndicationBuilder.withDefaultValues().build();
    communicationSyndication1.setConsumerChannelSyndicationAuthorisationStatus(SyndicationAuthorisationStatus.PENDING);
    Channel consumerChannel = this.channelBuilder.withId(CONSUMER_CHANNEL_ID).withTitle(CONSUMER_CHANNEL_TITLE).
        withOrganisation(CONSUMER_CHANNEL_ORGANISATION).build();
    communicationSyndication1.setChannel(consumerChannel);
    CommunicationStatistics communicationStatistics = this.commStatisticsBuilder.withDefaultValues().build();
    communicationSyndication1.setCommunicationStatistics(communicationStatistics);
    communicationSyndications.addCommunicationSyndications(communicationSyndication1);

    // Expectations -
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn(MASTER_APPROVAL_DATE);
    EasyMock.expect(mockFormatter.convert(EasyMock.isA(Date.class))).andReturn(CONSUMER_APPROVAL_DATE);
    EasyMock.replay(mockFormatter);

    // Do Test -
    WebcastSyndicationsDto communicationSyndicationsDto = this.uut.convert(communicationSyndications,
        mockFormatter);
    EasyMock.verify(mockFormatter);

    // Assertion -
    assertNotNull(communicationSyndicationsDto);
    ChannelDto masterChannelDto = communicationSyndicationsDto.getChannel();
    assertNotNull(masterChannelDto);
    assertEquals(new Long(MASTER_CHANNEL_ID), masterChannelDto.getId());
    assertEquals(MASTER_CHANNEL_TITLE, masterChannelDto.getTitle());
    assertEquals(MASTER_CHANNEL_ORGANISATION, masterChannelDto.getOrganisation());
    WebcastDto webcastDto = communicationSyndicationsDto.getCommunication();
    assertNotNull(webcastDto);
    assertEquals(COMM_TITLE, webcastDto.getTitle());
    List<WebcastSyndicationDto> communicationSyndicationDtos = communicationSyndicationsDto.getSyndications();
    assertNotNull(communicationSyndicationDtos);
    assertEquals(1, communicationSyndicationDtos.size());
    WebcastSyndicationDto communicationSyndicationDto = communicationSyndicationDtos.get(0);
    ChannelDto consumerChannelDto = communicationSyndicationDto.getChannel();
    assertEquals(new Long(CONSUMER_CHANNEL_ID), consumerChannelDto.getId());
    assertEquals(CONSUMER_CHANNEL_TITLE, consumerChannelDto.getTitle());
    assertEquals(CONSUMER_CHANNEL_ORGANISATION, consumerChannelDto.getOrganisation());
    List<AuthorisationDto> authorisationDtos = communicationSyndicationDto.getAuthorisations();
    assertEquals(2, authorisationDtos.size());
    AuthorisationDto masterAuthorisationDto = authorisationDtos.get(0);
    assertNotNull(masterAuthorisationDto);
    assertEquals(SyndicationAuthorisationType.MASTER.getType(), masterAuthorisationDto.getType());
    assertEquals(SyndicationAuthorisationStatus.APPROVED.getStatus(), masterAuthorisationDto.getStatus());
    assertEquals(MASTER_APPROVAL_DATE, masterAuthorisationDto.getDate());
    AuthorisationDto consumerAuthorisationDto = authorisationDtos.get(1);
    assertNotNull(consumerAuthorisationDto);
    assertEquals(SyndicationAuthorisationType.CONSUMER.getType(), consumerAuthorisationDto.getType());
    assertEquals(SyndicationAuthorisationStatus.PENDING.getStatus(), consumerAuthorisationDto.getStatus());
    assertEquals(CONSUMER_APPROVAL_DATE, consumerAuthorisationDto.getDate());
    CommunicationStatisticsDto communicationStatisticsDto = communicationSyndicationDto.getCommunicationStatistics();
    assertNotNull(communicationStatisticsDto);
    assertCommunicationStatistics(communicationStatistics, communicationStatisticsDto);
  }

  /**
   * Tests {@link CommunicationSyndicationConverter#convert} in the success case of converting
   * {@link CommunicationSyndications} to {@link WebcastSyndicationsDto} without consumer channels syndication.
   */
  @Test
  public void convertCommunicationSyndicationsWithOutSyndication() {
    // Setup -
    CommunicationSyndications communicationSyndications = new CommunicationSyndications();
    Communication communication = this.commBuilder.withTitle(COMM_TITLE).build();
    communicationSyndications.setCommunication(communication);
    Channel masterChannel = this.channelBuilder.withId(MASTER_CHANNEL_ID).withTitle(MASTER_CHANNEL_TITLE).
        withOrganisation(MASTER_CHANNEL_ORGANISATION).build();
    communicationSyndications.setChannel(masterChannel);

    // Do Test -
    WebcastSyndicationsDto communicationSyndicationsDto = this.uut.convert(communicationSyndications,
        mockFormatter);

    // Assertion -
    assertNotNull(communicationSyndicationsDto);
    ChannelDto masterChannelDto = communicationSyndicationsDto.getChannel();
    assertNotNull(masterChannelDto);
    assertEquals(new Long(MASTER_CHANNEL_ID), masterChannelDto.getId());
    assertEquals(MASTER_CHANNEL_TITLE, masterChannelDto.getTitle());
    assertEquals(MASTER_CHANNEL_ORGANISATION, masterChannelDto.getOrganisation());
    WebcastDto webcastDto = communicationSyndicationsDto.getCommunication();
    assertNotNull(webcastDto);
    assertEquals(COMM_TITLE, webcastDto.getTitle());
    List<WebcastSyndicationDto> communicationSyndicationDtos = communicationSyndicationsDto.getSyndications();
    assertNotNull(communicationSyndicationDtos);
    assertTrue(communicationSyndicationDtos.isEmpty());
  }

  /**
   * Assert the created {@link CommunicationStatisticsDto} as expected.
   * 
   * @param communicationStatistics The expected {@link CommunicationStatistics} to assert from.
   * @param communicationStatisticsDto The actual {@link CommunicationStatisticsDto} to assert to.
   */
  private void assertCommunicationStatistics(CommunicationStatistics communicationStatistics,
      CommunicationStatisticsDto communicationStatisticsDto) {
    assertNotNull(communicationStatistics);
    assertNotNull(communicationStatisticsDto);

    assertEquals(new Float(communicationStatistics.getAverageRating()),
        (new Float(communicationStatisticsDto.getAverageRating())));
    assertEquals(communicationStatistics.getTotalRegistration(), communicationStatisticsDto.getTotalRegistration());
    assertEquals(new Float(communicationStatistics.getTotalViewingDuration()),
        new Float(communicationStatisticsDto.getTotalViewingDuration()));
    assertEquals(communicationStatistics.getNumberOfViewings(), communicationStatisticsDto.getTotalViews());
  }
}
