/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsUpdateValidatorTest.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.error.SubscriptionRequestException;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsDto;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.utils.DtoFactoryUtils;

/**
 * Unit test for {@link SubscriptionsUpdateValidator}.
 */
public class SubscriptionsUpdateValidatorTest {

  private static final Long TEST_SUBSCRIPTION_ID = 111L;

  private SubscriptionsUpdateValidator uut;

  /**
   * Test Setup.
   */
  @Before
  public void setup() {
    uut = new SubscriptionsUpdateValidator();
    uut.setContextRequired(true);
  }

  /**
   * Tests {@link SubscriptionsUpdateValidator#validate(SubscriptionsDto)} in the error case of validating subscriptions
   * with duplicate subscriptions with the same ids.
   */
  @Test
  public void validateWhileSubscriptionsContainDuplicate() {
    // Setup -
    SubscriptionDto subscription1 = DtoFactoryUtils.createSubscriptionDto();
    SubscriptionDto subscription2 = DtoFactoryUtils.createSubscriptionDto();
    subscription2.setId(TEST_SUBSCRIPTION_ID);
    SubscriptionDto subscription3 = DtoFactoryUtils.createSubscriptionDto();
    subscription3.setId(TEST_SUBSCRIPTION_ID);
    List<SubscriptionDto> subscriptions = Arrays.asList(subscription1, subscription2, subscription3);
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do Test -
    try {
      uut.validate(subscriptionsDto);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.DUPLICATE_RESOURCE, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsUpdateValidator#validate(SubscriptionsDto)} in the error case of validating subscriptions
   * with subscription that is missing id.
   */
  @Test
  public void validateWhileSubscriptionMissingId() {
    // Setup -
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setId(null);
    List<SubscriptionDto> subscriptions = Arrays.asList(subscription);
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do Test -
    try {
      uut.validate(subscriptionsDto);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.MISSING_SUBSCRIPTION_ID, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsUpdateValidator#validate(SubscriptionsDto)} in the error case of validating subscriptions
   * with subscription that has invalid integer id.
   */
  @Test
  public void validateWhileSubscriptionWithInvalidId() {
    // Setup -
    SubscriptionDto subscription = DtoFactoryUtils.createSubscriptionDto();
    subscription.setId(2378732732L);
    List<SubscriptionDto> subscriptions = Arrays.asList(subscription);
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do Test -
    try {
      uut.validate(subscriptionsDto);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.INVALID_SUBSCRIPTION_ID, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsUpdateValidator#validate(SubscriptionsDto)} in the error case of validating subscriptions
   * with subscription that has missing referral.
   */
  @Test
  public void validateWhileSubscriptionMissingReferral() {
    // Setup -
    SubscriptionDto subscription1 = DtoFactoryUtils.createSubscriptionDto();
    subscription1.setId(TEST_SUBSCRIPTION_ID);
    SubscriptionDto subscription2 = DtoFactoryUtils.createSubscriptionDto();
    subscription2.setReferral(null);
    List<SubscriptionDto> subscriptions = Arrays.asList(subscription1, subscription2);
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do Test -
    try {
      uut.validate(subscriptionsDto);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.MISSING_REFERRAL, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsUpdateValidator#validate(SubscriptionsDto)} in the error case of validating subscriptions
   * with subscription that has invalid referral - value set not to "paid".
   */
  @Test
  public void validateWhileSubscriptionWithInvalidReferral() {
    // Setup -
    SubscriptionDto subscription1 = DtoFactoryUtils.createSubscriptionDto();
    subscription1.setId(TEST_SUBSCRIPTION_ID);
    SubscriptionDto subscription2 = DtoFactoryUtils.createSubscriptionDto();
    subscription2.setReferral("test");
    List<SubscriptionDto> subscriptions = Arrays.asList(subscription1, subscription2);
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do Test -
    try {
      uut.validate(subscriptionsDto);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.INVALID_REFERRAL, ex.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionsUpdateValidator#validate(SubscriptionsDto)} in the error case of validating subscriptions
   * with subscription that has missing context.
   */
  @Test
  public void validateWhileSubscriptionWithMissingContext() {
    // Setup -
    SubscriptionDto subscription1 = DtoFactoryUtils.createSubscriptionDto();
    subscription1.setId(TEST_SUBSCRIPTION_ID);
    SubscriptionDto subscription2 = DtoFactoryUtils.createSubscriptionDto();
    subscription2.setContext(null);
    List<SubscriptionDto> subscriptions = Arrays.asList(subscription1, subscription2);
    SubscriptionsDto subscriptionsDto = new SubscriptionsDto();
    subscriptionsDto.setSubscriptions(subscriptions);

    // Do Test -
    try {
      uut.validate(subscriptionsDto);
    } catch (SubscriptionRequestException ex) {
      assertEquals(ErrorCode.MISSING_CONTEXT, ex.getErrorCode());
    }
  }
}
