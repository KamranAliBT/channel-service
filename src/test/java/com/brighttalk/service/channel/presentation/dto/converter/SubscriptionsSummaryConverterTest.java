/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsSummaryConverterTest.java 92672 2015-04-01 08:27:25Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.Subscription.Referral;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError.SubscriptionErrorCode;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionsSummary;
import com.brighttalk.service.channel.presentation.dto.SubscriptionContextDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionErrorDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsSummaryDto;

public class SubscriptionsSummaryConverterTest {

  private static final Long SUBSCRIPTION_ID = 33L;
  private static final Date LAST_SUBSCRIBED = new Date();
  private static final Long USER_ID = 33L;
  private static final String EMAIL = "a@b.com";
  private static final Referral REFERRAL = Referral.paid;
  private static final String LEAD_TYPE = "summit";
  private static final String LEAD_CONTEXT = "1111";
  private static final String ENGAGEMENT_SCORE = "88";
  private static final SubscriptionErrorCode ERROR_CODE = SubscriptionErrorCode.USER_NOT_FOUND;
  private static final String ERROR_MESSAGE = "message";

  private DateTimeFormatter mockFormatter;

  private SubscriptionsSummaryConverter uut;

  @Before
  public void setUp() {
    uut = new SubscriptionsSummaryConverter();

    mockFormatter = createMock(DateTimeFormatter.class);
  }

  @Test
  public void convertWithNoSubscriptionsOrErrors() {

    // Set up
    SubscriptionsSummary summary = new SubscriptionsSummary();

    // Do test
    SubscriptionsSummaryDto result = uut.convert(summary, mockFormatter);

    // Assertions
    assertNotNull(result);
    assertNull(result.getSubscriptions());
    assertNull(result.getErrors());
  }

  @Test
  public void convertWithSubscriptions() {

    // Set up
    List<Subscription> subscriptions = new ArrayList<>();
    subscriptions.add(buildSubscription());

    SubscriptionsSummary summary = new SubscriptionsSummary();
    summary.setSubscriptions(subscriptions);

    // Expectations
    String expectedDate = "2014-11-01T15:34:29Z";

    expect(mockFormatter.convert(isA(Date.class))).andReturn(expectedDate);
    EasyMock.replay(mockFormatter);

    // Do test
    SubscriptionsSummaryDto result = uut.convert(summary, mockFormatter);

    // Assertions
    assertNotNull(result);
    assertFalse(result.getSubscriptions().isEmpty());
    assertNull(result.getErrors());

    SubscriptionDto subscriptionDto = result.getSubscriptions().iterator().next();
    assertEquals(SUBSCRIPTION_ID, subscriptionDto.getId());
    assertEquals(USER_ID, subscriptionDto.getUser().getId());
    assertEquals(EMAIL, subscriptionDto.getUser().getEmail());
    assertEquals(REFERRAL.name(), subscriptionDto.getReferral());
    assertEquals(expectedDate, subscriptionDto.getLastSubscribed());

    SubscriptionContextDto contextDto = subscriptionDto.getContext();
    assertNotNull(contextDto);
    assertEquals(LEAD_TYPE, contextDto.getLeadType());
    assertEquals(LEAD_CONTEXT, contextDto.getLeadContext());
    assertEquals(ENGAGEMENT_SCORE, contextDto.getEngagementScore());

    EasyMock.verify(mockFormatter);
  }

  @Test
  public void convertWithErrors() {

    // Set up
    SubscriptionError subscriptionError = new SubscriptionError();
    subscriptionError.addErrorCode(ERROR_CODE);
    subscriptionError.setMessage(ERROR_MESSAGE);
    subscriptionError.setSubscription(buildSubscription());

    List<SubscriptionError> errors = new ArrayList<>();
    errors.add(subscriptionError);

    SubscriptionsSummary summary = new SubscriptionsSummary();
    summary.setErrors(errors);

    // Do test
    SubscriptionsSummaryDto result = uut.convert(summary, mockFormatter);

    // Assertions
    assertNotNull(result);
    assertNull(result.getSubscriptions());
    assertFalse(result.getErrors().isEmpty());

    SubscriptionErrorDto subscriptionErrorDto = result.getErrors().iterator().next();
    assertEquals(ERROR_CODE.getCode(), subscriptionErrorDto.getCodes().get(0));
    assertEquals(ERROR_MESSAGE, subscriptionErrorDto.getMessage());
    assertEquals(SUBSCRIPTION_ID, subscriptionErrorDto.getSubscription().getId());
    assertEquals(USER_ID, subscriptionErrorDto.getSubscription().getUser().getId());
    assertEquals(EMAIL, subscriptionErrorDto.getSubscription().getUser().getEmail());
    assertEquals(REFERRAL.name(), subscriptionErrorDto.getSubscription().getReferral());
    assertNotNull(subscriptionErrorDto.getSubscription().getContext());
    assertEquals(LEAD_TYPE, subscriptionErrorDto.getSubscription().getContext().getLeadType());
    assertEquals(LEAD_CONTEXT, subscriptionErrorDto.getSubscription().getContext().getLeadContext());
    assertEquals(ENGAGEMENT_SCORE, subscriptionErrorDto.getSubscription().getContext().getEngagementScore());
  }

  @Test
  public void convertWithMultipleErrors() {

    // Set up
    List<SubscriptionErrorCode> errorCodes =
        Arrays.asList(SubscriptionErrorCode.ALREADY_SUBSCRIBED, SubscriptionErrorCode.INVALID_LEAD_CONTEXT,
            SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE);
    SubscriptionError subscriptionError = new SubscriptionError();
    subscriptionError.setErrorCodes(errorCodes);
    subscriptionError.setSubscription(buildSubscription());

    List<SubscriptionError> errors = new ArrayList<>();
    errors.add(subscriptionError);

    SubscriptionsSummary summary = new SubscriptionsSummary();
    summary.setErrors(errors);

    // Do test
    SubscriptionsSummaryDto result = uut.convert(summary, mockFormatter);

    // Assertions
    assertNotNull(result);
    assertNull(result.getSubscriptions());
    assertFalse(result.getErrors().isEmpty());

    SubscriptionErrorDto subscriptionErrorDto = result.getErrors().iterator().next();
    List<String> errorCodeValues = subscriptionErrorDto.getCodes();
    assertEquals(3, errorCodeValues.size());
    assertEquals(SubscriptionErrorCode.ALREADY_SUBSCRIBED.getCode(), errorCodeValues.get(0));
    assertEquals(SubscriptionErrorCode.INVALID_LEAD_CONTEXT.getCode(), errorCodeValues.get(1));
    assertEquals(SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE.getCode(), errorCodeValues.get(2));

    assertEquals(SUBSCRIPTION_ID, subscriptionErrorDto.getSubscription().getId());
    assertEquals(USER_ID, subscriptionErrorDto.getSubscription().getUser().getId());
    assertEquals(EMAIL, subscriptionErrorDto.getSubscription().getUser().getEmail());
    assertEquals(REFERRAL.name(), subscriptionErrorDto.getSubscription().getReferral());
    assertNotNull(subscriptionErrorDto.getSubscription().getContext());
    assertEquals(LEAD_TYPE, subscriptionErrorDto.getSubscription().getContext().getLeadType());
    assertEquals(LEAD_CONTEXT, subscriptionErrorDto.getSubscription().getContext().getLeadContext());
    assertEquals(ENGAGEMENT_SCORE, subscriptionErrorDto.getSubscription().getContext().getEngagementScore());
  }

  @Test
  public void convertWithErrorsAndContext() {

    // Set up
    SubscriptionErrorCode errorCode = SubscriptionErrorCode.ALREADY_SUBSCRIBED;

    SubscriptionError subscriptionError = new SubscriptionError();
    subscriptionError.addErrorCode(errorCode);
    subscriptionError.setMessage(ERROR_MESSAGE);
    subscriptionError.setSubscription(buildSubscription());

    List<SubscriptionError> errors = new ArrayList<>();
    errors.add(subscriptionError);

    SubscriptionsSummary summary = new SubscriptionsSummary();
    summary.setErrors(errors);

    // Do test
    SubscriptionsSummaryDto result = uut.convert(summary, mockFormatter);

    // Assertions
    assertNotNull(result);
    assertNull(result.getSubscriptions());
    assertFalse(result.getErrors().isEmpty());

    SubscriptionErrorDto subscriptionErrorDto = result.getErrors().iterator().next();
    assertEquals(errorCode.getCode(), subscriptionErrorDto.getCodes().get(0));
    assertEquals(ERROR_MESSAGE, subscriptionErrorDto.getMessage());
    assertEquals(SUBSCRIPTION_ID, subscriptionErrorDto.getSubscription().getId());
    assertEquals(USER_ID, subscriptionErrorDto.getSubscription().getUser().getId());
    assertEquals(EMAIL, subscriptionErrorDto.getSubscription().getUser().getEmail());
    assertEquals(REFERRAL.name(), subscriptionErrorDto.getSubscription().getReferral());

    SubscriptionContextDto contextDto = subscriptionErrorDto.getSubscription().getContext();
    assertNotNull(contextDto);
    assertEquals(LEAD_TYPE, contextDto.getLeadType());
    assertEquals(LEAD_CONTEXT, contextDto.getLeadContext());
    assertEquals(ENGAGEMENT_SCORE, contextDto.getEngagementScore());
  }

  private Subscription buildSubscription() {
    User user = new User();
    user.setId(USER_ID);
    user.setEmail(EMAIL);

    Subscription subscription = new Subscription();
    subscription.setId(SUBSCRIPTION_ID);
    subscription.setUser(user);
    subscription.setReferral(REFERRAL);
    subscription.setLastSubscribed(LAST_SUBSCRIBED);
    subscription.setContext(new SubscriptionContext(SubscriptionLeadType.getType(LEAD_TYPE),
        LEAD_CONTEXT, ENGAGEMENT_SCORE));

    return subscription;
  }

}
