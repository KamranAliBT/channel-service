package com.brighttalk.service.channel.business.service.user;

import static org.easymock.EasyMock.isA;

import java.util.HashSet;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.service.channel.ChannelManagerService;
import com.brighttalk.service.channel.business.service.user.DefaultUserService;
import com.brighttalk.service.channel.business.service.user.UserService;

public class DefaultUserServiceTest {

  private static final Long USER_ID = 999L;

  private Set<Object> mocks;

  private ChannelManagerService mockChannelManagerService;

  private UserService uut;

  @Before
  public void setUp() {
    uut = new DefaultUserService();

    mockChannelManagerService = EasyMock.createMock(ChannelManagerService.class);
    ReflectionTestUtils.setField(uut, "channelManagerService", mockChannelManagerService);

    mocks = new HashSet<Object>();
  }

  @Test
  public void testDelete() {

    // Set up

    // Expectations
    expectChannelManagerService();
    replay();

    // Do test
    uut.delete(USER_ID);

    // Assertions
    verify();
  }

  private void expectChannelManagerService() {
    mockChannelManagerService.delete(isA(Long.class));
    EasyMock.expectLastCall();
    expect(mockChannelManagerService);
  }

  private void expect(final Object obj) {
    mocks.add(obj);
  }

  private void replay() {
    EasyMock.replay(mocks.toArray());
  }

  private void verify() {
    EasyMock.verify(mocks.toArray());
  }

}
