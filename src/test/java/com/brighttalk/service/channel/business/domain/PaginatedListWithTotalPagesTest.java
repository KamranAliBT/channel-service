/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: PaginatedListWithTotalPagesTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;

/**
 *
 * @author tomberthon
 */
public class PaginatedListWithTotalPagesTest {

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  /**
   * Test of getCurrentPageNumber method, of class PaginatedList.
   */
  @Test
  public void test() {
    // Set up
    PaginatedListWithTotalPages<CommunicationRegistration> registrations = new PaginatedListWithTotalPages<CommunicationRegistration>();

    // Expectations
    int expectedCount = 0;

    // Do Test
    int result = registrations.size();


    // Assertions
    assertEquals(expectedCount, result);
  }

  /**
   * Test of getCurrentPageNumber method, of class PaginatedList.
   */
  @Test
  public void testForEach() {
    // Set up
    PaginatedListWithTotalPages<CommunicationRegistration> registrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    registrations.add(new CommunicationRegistration());

    // Expectations
    int expectedCount = 1;

    // Do Test
    int result = 0;
    for (int i = 0; i < registrations.size(); i ++) {
      result ++;
    }

    // Assertions
    assertEquals(expectedCount, result);
  }
  
  @Test
  public void testGetNumberOfPagesNoPages() {
    // Set up
    PaginatedListWithTotalPages<CommunicationRegistration> registrations = new PaginatedListWithTotalPages<CommunicationRegistration>();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = registrations.getNumberOfPages();

    // Assertions
    assertEquals(expectedResult, result);
  }
  
  @Test
  public void testGetNumberOfPagesOnePages() {
    // Set up
    int total = 1;
    int pageSize = 2;
    
    PaginatedListWithTotalPages<CommunicationRegistration> registrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    registrations.setUnpaginatedTotal(total);
    registrations.setPageSize(pageSize);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = registrations.getNumberOfPages();

    // Assertions
    assertEquals(expectedResult, result);
  }
  
  @Test
  public void testGetNumberOfPagesOnePageSameTotalAsPageSize() {
    // Set up
    int total = 1;
    int pageSize = total;
    
    PaginatedListWithTotalPages<CommunicationRegistration> registrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    registrations.setUnpaginatedTotal(total);
    registrations.setPageSize(pageSize);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = registrations.getNumberOfPages();

    // Assertions
    assertEquals(expectedResult, result);
  }
  
  @Test
  public void testGetNumberOfPagesTwoPages() {
    // Set up
    int total = 2;
    int pageSize = 1;
    
    PaginatedListWithTotalPages<CommunicationRegistration> registrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    registrations.setUnpaginatedTotal(total);
    registrations.setPageSize(pageSize);

    // Expectations
    int expectedResult = 2;

    // Do Test
    int result = registrations.getNumberOfPages();

    // Assertions
    assertEquals(expectedResult, result);
  }
  
}
