/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $ Id: $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.FilterType;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;

/**
 * Unit tests for {@link ChannelFeedSearchCriteria}.
 */
public class ChannelFeedSearchCriteriaTest {

  @Test
  public void convertionOfFilterTypeValid() {
    // Set up
    String inputFilter = "CLOSESTTONOW";
    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();

    // Expectations
    ChannelFeedSearchCriteria.FilterType expectedResult = ChannelFeedSearchCriteria.FilterType.CLOSESTTONOW;

    // Do Test
    criteria.setFilterType(inputFilter);

    // Assertions
    assertEquals(expectedResult, criteria.getFilterType());
  }

  @Test
  public void convertionOfFilterTypeInValid() {
    // Set up
    String inputFilter = "not a type";
    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();

    // Expectations
    ChannelFeedSearchCriteria.FilterType expectedResult = null;

    // Do Test
    criteria.setFilterType(inputFilter);

    // Assertions
    assertEquals(expectedResult, criteria.getFilterType());
  }

  @Test
  public void convertionOfFilterTypeValidMixedCase() {
    // Set up
    String inputFilter = "ClOsEsTtOnOw";
    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();

    // Expectations
    ChannelFeedSearchCriteria.FilterType expectedResult = ChannelFeedSearchCriteria.FilterType.CLOSESTTONOW;

    // Do Test
    criteria.setFilterType(inputFilter);

    // Assertions
    assertEquals(expectedResult, criteria.getFilterType());
  }

  @Test
  public void convertionOfFilterTypeValidLowerCase() {
    // Set up
    String inputFilter = "closesttonow";
    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();

    // Expectations
    ChannelFeedSearchCriteria.FilterType expectedResult = ChannelFeedSearchCriteria.FilterType.CLOSESTTONOW;

    // Do Test
    criteria.setFilterType(inputFilter);

    // Assertions
    assertEquals(expectedResult, criteria.getFilterType());
  }

  @Test
  public void convertionOfFilterTypeValidWithPadding() {
    // Set up
    String inputFilter = "   closesttonow   ";
    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();

    // Expectations
    ChannelFeedSearchCriteria.FilterType expectedResult = ChannelFeedSearchCriteria.FilterType.CLOSESTTONOW;

    // Do Test
    criteria.setFilterType(inputFilter);

    // Assertions
    assertEquals(expectedResult, criteria.getFilterType());
  }

  @Test
  public void isClosestToNowFilterExpectTrue() {
    // Set up
    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    FilterType filterType = ChannelFeedSearchCriteria.FilterType.CLOSESTTONOW;

    criteria.setFilterType(filterType);

    // Expectations
    boolean expectedResult = true;

    // Do Test
    boolean result = criteria.isClosestToNowFilter();

    // Assertions
    assertEquals(expectedResult, result);
  }

  @Test
  public void isClosestToNowFilterExpectFalse() {
    // Set up
    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    FilterType filterType = null;

    criteria.setFilterType(filterType);

    // Expectations
    boolean expectedResult = false;

    // Do Test
    boolean result = criteria.isClosestToNowFilter();

    // Assertions
    assertEquals(expectedResult, result);
  }

  /**
   * Tests converting a string version of an enum in to an enum using {@link CommunicationStatusFilter#convert(String)}.
   */
  @Test
  public void convertCommunicationStatusFilterStringToEnum() {

    // Set up
    CommunicationStatusFilter expectedStatusFilter = CommunicationStatusFilter.UPCOMING;
    String statusFilterAsString = "upcoming";

    // Expectations

    // Do Test
    CommunicationStatusFilter actualStatusFilter = CommunicationStatusFilter.convert(statusFilterAsString);

    // Assertions
    assertEquals(expectedStatusFilter, actualStatusFilter);
  }

  /**
   * Tests converting an invalid string {@link CommunicationStatusFilter#convert(String)}.
   */
  @Test(expected = SearchCriteriaException.class)
  public void convertInvalidCommunicationStatusFilterStringToEnum() {

    // Set up
    String statusFilterAsString = "rubbish";

    // Expectations

    // Do Test
    CommunicationStatusFilter.convert(statusFilterAsString);

    // Assertions
  }

  /**
   * Tests setting and getting the page number.
   */
  @Test
  public void setAndGetPageNumber() {

    // Set up
    Integer pageNumber = 2;

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();

    // Expectations

    // Do test
    criteria.setPageNumber(pageNumber);
    Integer returnedPageNumber = criteria.getPageNumber();

    // Assertions
    assertEquals(pageNumber, returnedPageNumber);
  }

  /**
   * Tests setting and getting the page size.
   */
  @Test
  public void setAndGetPageSize() {

    // Set up
    Integer pageSize = 3;

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();

    // Expectations

    // Do test
    criteria.setPageSize(pageSize);
    Integer returnedPageSize = criteria.getPageSize();

    // Assertions
    assertEquals(pageSize, returnedPageSize);
  }

  /**
   * Tests setting and getting the communication status filter.
   */
  @Test
  public void setAndGetCommunicationStatusFilter() {

    // Set up
    List<CommunicationStatusFilter> expectedCommunicationStatusFilter = Arrays.asList(CommunicationStatusFilter.UPCOMING);
    List<CommunicationStatusFilter> communicationStatusFilter = Arrays.asList(CommunicationStatusFilter.UPCOMING);

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    // Expectations

    // Do Test
    List<CommunicationStatusFilter> actualCommunicationStatusFilter = criteria.getCommunicationStatusFilter();

    // Assertions
    assertEquals(expectedCommunicationStatusFilter, actualCommunicationStatusFilter);
  }

  /**
   * Tests that if an attempt to set the communication status filter with a null list, then null is returned from the
   * {@link ChannelFeedSearchCriteria#getCommunicationStatusFilter()}.
   */
  @Test
  public void setAndGetCommunicationStatusFilterWithNull() {

    // Set up
    List<CommunicationStatusFilter> expectedCommunicationStatusFilter = null;
    List<CommunicationStatusFilter> communicationStatusFilter = null;

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    // Expectations

    // Do Test
    List<CommunicationStatusFilter> actualCommunicationStatusFilter = criteria.getCommunicationStatusFilter();

    // Assertions
    assertEquals(expectedCommunicationStatusFilter, actualCommunicationStatusFilter);
  }

  /**
   * Tests that the {@link ChannelFeedSearchCriteria#getCommunicationStatusFilter()} will return the default empty list
   * if {@link ChannelFeedSearchCriteria#setCommunicationStatusFilter(List)} is not called.
   */
  @Test
  public void getDefaultCommunicationStatusFilter() {

    // Set up
    List<CommunicationStatusFilter> expectedCommunicationStatusFilter = null;

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();

    // Expectations

    // Do Test
    List<CommunicationStatusFilter> actualCommunicationStatusFilter = criteria.getCommunicationStatusFilter();

    // Assertions
    assertEquals(expectedCommunicationStatusFilter, actualCommunicationStatusFilter);
  }

  /**
   * Tests {@link ChannelFeedSearchCriteria#hasCommunicationStatusFilter} when it has not been set.
   */
  @Test
  public void hasCommunicationStatusFilterWithNoStatusFilterSet() {

    // Set up
    boolean expectedHasCommunicationStatusFilter = false;

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();

    // Expectations

    // Do Test
    boolean actualHasCommunicationStatusFilter = criteria.hasCommunicationStatusFilter();

    // Assertions
    assertEquals(expectedHasCommunicationStatusFilter, actualHasCommunicationStatusFilter);
  }

  /**
   * Tests {@link ChannelFeedSearchCriteria#hasCommunicationStatusFilter} when it has been set with a null list.
   */
  @Test
  public void hasCommunicationStatusFilterWithNullList() {

    // Set up
    boolean expectedHasCommunicationStatusFilter = false;
    List<CommunicationStatusFilter> communicationStatusFilter = null;

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    // Expectations

    // Do Test
    boolean actualHasCommunicationStatusFilter = criteria.hasCommunicationStatusFilter();

    // Assertions
    assertEquals(expectedHasCommunicationStatusFilter, actualHasCommunicationStatusFilter);
  }

  /**
   * Tests {@link ChannelFeedSearchCriteria#hasCommunicationStatusFilter} when it has been set with an empty list.
   */
  @Test
  public void hasCommunicationStatusFilterWhenSetWithEmptyList() {

    // Set up
    boolean expectedHasCommunicationStatusFilter = false;
    List<CommunicationStatusFilter> communicationStatusFilter = Collections.<CommunicationStatusFilter>emptyList();

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    // Expectations

    // Do Test
    boolean actualHasCommunicationStatusFilter = criteria.hasCommunicationStatusFilter();

    // Assertions
    assertEquals(expectedHasCommunicationStatusFilter, actualHasCommunicationStatusFilter);
  }

  /**
   * Tests {@link ChannelFeedSearchCriteria#hasCommunicationStatusFilter} when it has been set with a populated list.
   */
  @Test
  public void hasCommunicationStatusFilterWhenSetWithPopulatedList() {

    // Set up
    boolean expectedHasCommunicationStatusFilter = true;
    List<CommunicationStatusFilter> communicationStatusFilter = Arrays.asList(CommunicationStatusFilter.UPCOMING);

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    // Expectations

    // Do Test
    boolean actualHasCommunicationStatusFilter = criteria.hasCommunicationStatusFilter();

    // Assertions
    assertEquals(expectedHasCommunicationStatusFilter, actualHasCommunicationStatusFilter);
  }

  /**
   * Test the {@link ChannelFeedSearchCriteria#toString()} will return the correct string representation of the criteria
   * instance.
   */
  @Test
  public void criteriaToString() {

    // Set up
    int pageNumber = 1;
    int pageSize = 50;
    String inputFilter = "closesttonow";
    List<CommunicationStatusFilter> communicationStatusFilter = Arrays.asList(CommunicationStatusFilter.UPCOMING,
        CommunicationStatusFilter.LIVE);

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria(pageNumber, pageSize, inputFilter);
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    String expectedCriteriaAsString = buildCriteriaAsString(criteria);

    // Expectations

    // Do Test
    String actualCriteriaAsString = criteria.toString();

    // Assertions
    assertEquals(expectedCriteriaAsString, actualCriteriaAsString);
  }

  /**
   * Test the {@link ChannelFeedSearchCriteria#toString()} will return the correct string representation of the criteria
   * instance. In this case, filter type is null, so that we can test it still behaves as expected.
   */
  @Test
  public void criteriaToStringWithNullFilterType() {

    // Set up
    int pageNumber = 1;
    int pageSize = 50;
    String inputFilter = null;
    List<CommunicationStatusFilter> communicationStatusFilter = Arrays.asList(CommunicationStatusFilter.UPCOMING,
        CommunicationStatusFilter.LIVE);

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria(pageNumber, pageSize, inputFilter);
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    String expectedCriteriaAsString = buildCriteriaAsString(criteria);

    // Expectations

    // Do Test
    String actualCriteriaAsString = criteria.toString();

    // Assertions
    assertEquals(expectedCriteriaAsString, actualCriteriaAsString);
  }

  /**
   * Test the {@link ChannelFeedSearchCriteria#toString()} will return the correct string representation of the criteria
   * instance. In this case, communication status filter is null, so that we can test it still behaves as expected.
   */
  @Test
  public void criteriaToStringWithNullCommunicationStatusFilter() {

    // Set up
    int pageNumber = 1;
    int pageSize = 50;
    String inputFilter = null;
    List<CommunicationStatusFilter> communicationStatusFilter = null;

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria(pageNumber, pageSize, inputFilter);
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    String expectedCriteriaAsString = buildCriteriaAsString(criteria);

    // Expectations

    // Do Test
    String actualCriteriaAsString = criteria.toString();

    // Assertions
    assertEquals(expectedCriteriaAsString, actualCriteriaAsString);
  }

  /**
   * Tests {@link ChannelFeedSearchCriteria#getStatusFilterAsStringList()} when the status filter list is null.
   */
  @Test
  public void getStatusFilterAsStringListWhenStatusFilterListIsNull() {

    // Set up
    List<CommunicationStatusFilter> communicationStatusFilter = null;

    List<String> expectedCommunicationStatusFilterStringList = Collections.<String>emptyList();

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    // Expectations

    // Do Test
    List<String> actualCommunicationStatusFilterStringList = criteria.getStatusFilterAsStringList();

    // Assertions
    assertEquals(expectedCommunicationStatusFilterStringList, actualCommunicationStatusFilterStringList);
  }

  /**
   * Tests {@link ChannelFeedSearchCriteria#getStatusFilterAsStringList()} when the status filter list is empty.
   */
  @Test
  public void getStatusFilterAsStringListWhenStatusFilterListIsEmpty() {

    // Set up
    List<CommunicationStatusFilter> communicationStatusFilter = Collections.<CommunicationStatusFilter>emptyList();

    List<String> expectedCommunicationStatusFilterStringList = Collections.<String>emptyList();

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    // Expectations

    // Do Test
    List<String> actualCommunicationStatusFilterStringList = criteria.getStatusFilterAsStringList();

    // Assertions
    assertEquals(expectedCommunicationStatusFilterStringList, actualCommunicationStatusFilterStringList);
  }

  /**
   * Tests {@link ChannelFeedSearchCriteria#getStatusFilterAsStringList()} when the status filter list has elements.
   */
  @Test
  public void getStatusFilterAsStringListWhenStatusFilterListHasElements() {

    // Set up
    List<CommunicationStatusFilter> communicationStatusFilter = Arrays.asList(CommunicationStatusFilter.UPCOMING,
        CommunicationStatusFilter.LIVE);

    List<String> expectedCommunicationStatusFilterStringList = Arrays.asList("upcoming", "live");

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(communicationStatusFilter);

    // Expectations

    // Do Test
    List<String> actualCommunicationStatusFilterStringList = criteria.getStatusFilterAsStringList();

    // Assertions
    assertEquals(expectedCommunicationStatusFilterStringList, actualCommunicationStatusFilterStringList);
  }

  // Has elements in list

  private String buildCriteriaAsString(ChannelFeedSearchCriteria criteria) {

    StringBuilder builder = new StringBuilder();

    builder.append("[pageNumber=" + criteria.getPageNumber() + ", ");
    builder.append("pageSize=" + criteria.getPageSize() + ", ");

    String filterName = criteria.getFilterType() == null ? "<null>" : criteria.getFilterType().name();
    builder.append("filterType=" + filterName + ", ");

    String getCommunicationStatusFilter = criteria.getCommunicationStatusFilter() == null ? "<null>"
        : criteria.getCommunicationStatusFilter().toString();
    builder.append("communicationStatusFilter=" + getCommunicationStatusFilter + "]");

    return builder.toString();
  }
}
