/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelManagerServiceTest.java 70175 2013-10-24 10:09:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;
import com.brighttalk.service.channel.business.error.ChannelManagerNotFoundException;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.error.MaxChannelManagersFeatureException;
import com.brighttalk.service.channel.business.queue.AuditQueue;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.ChannelManagerAuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.database.ChannelManagerDbDao;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.util.UserSearchCriteriaBuilder;

public class DefaultChannelManagerServiceTest {

  private static final Long USER_ID = 999L;

  private static final Long CHANNEL_MANAGER_ID = 888L;

  private static final Long CHANNEL_ID = 5L;

  private static final String EMAIL_ADDRESS = "email@brighttalk.com";

  private static final boolean ENABLED = true;

  private static final boolean DISABLED = false;

  private static final String MAX_CHANNEL_MANAGERS = "5";

  private static final boolean EMAIL_ALERTS_ENABLED = true;

  private static final Integer PAGE_NUMBER = 0;

  private static final Integer PAGE_SIZE = 10;

  private DefaultChannelManagerService uut;

  private ChannelFinder mockChannelFinder;

  private ChannelManagerDbDao mockChannelManagerDbDao;

  private UserServiceDao mockUserServiceDao;

  private ChannelManagerValidator mockValidator;

  private UserSearchCriteriaBuilder mockUserSearchCriteriaBuilder;

  private AuditRecordDtoBuilder mockAuditRecordDtoBuilder;

  private AuditQueue mockAuditQueue;

  private Set<Object> mocks;

  @Before
  public void setUp() {
    uut = new DefaultChannelManagerService();

    mockChannelManagerDbDao = EasyMock.createMock(ChannelManagerDbDao.class);
    ReflectionTestUtils.setField(uut, "channelManagerDbDao", mockChannelManagerDbDao);

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockUserServiceDao = EasyMock.createMock(UserServiceDao.class);
    ReflectionTestUtils.setField(uut, "userServiceDao", mockUserServiceDao);

    mockValidator = EasyMock.createMock(ChannelManagerValidator.class);
    ReflectionTestUtils.setField(uut, "validator", mockValidator);

    mockUserSearchCriteriaBuilder = EasyMock.createMock(UserSearchCriteriaBuilder.class);
    ReflectionTestUtils.setField(uut, "userSearchCriteriaBuilder", mockUserSearchCriteriaBuilder);

    mockAuditRecordDtoBuilder = EasyMock.createMock(ChannelManagerAuditRecordDtoBuilder.class);
    ReflectionTestUtils.setField(uut, "auditRecordDtoBuilder", mockAuditRecordDtoBuilder);

    mockAuditQueue = EasyMock.createMock(AuditQueue.class);
    ReflectionTestUtils.setField(uut, "auditQueue", mockAuditQueue);

    mocks = new HashSet<Object>();
  }

  @Test
  public void testCreate() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    List<User> users = new ArrayList<User>();
    users.add(user);

    Channel channel = buildChannel();
    channel.addFeature(buildChannelManagerFeature(ENABLED, MAX_CHANNEL_MANAGERS));

    ChannelManager channelManager = buildChannelManager();

    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(channelManager);

    UsersSearchCriteria criteria = buildUserSearchCriteria();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorWhenCreating();
    expectChannelManagerDbDaoWhenCreating(channelManager);
    expectUserSearchCriteriaBuilder(criteria);
    expectUserServiceWhenFiding(users);
    expectCreateChannelManagerAuditRecordDto(USER_ID, channel.getId());
    replay();

    // Do test
    List<ChannelManager> newChannelManagers = uut.create(buildSessionUser(), channel.getId(), channelManagers);

    // Assertions
    assertEquals(channelManagers, newChannelManagers);
    verify();
  }

  @Test(expected = ChannelNotFoundException.class)
  public void testCreateWhenChannelDoesNotExist() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    List<User> users = new ArrayList<User>();
    users.add(user);

    List<ChannelManager> channelManagers = buildChannelManagers();

    // Expectations
    expectChannelFinderToThrowExceptionWhenFinding();
    replay();

    // Do test
    uut.create(buildSessionUser(), CHANNEL_ID, channelManagers);

    // Assertions
    verify();
  }

  @Test(expected = FeatureNotEnabledException.class)
  public void testCreateWhenChannelManagerFeatureDidabled() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    List<User> users = new ArrayList<User>();
    users.add(user);

    Channel channel = buildChannel();
    channel.addFeature(buildChannelManagerFeature(DISABLED, MAX_CHANNEL_MANAGERS));

    List<ChannelManager> channelManagers = buildChannelManagers();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorToThrowExcpetionWhenCreating(new FeatureNotEnabledException(""));
    replay();

    // Do test
    uut.create(buildSessionUser(), channel.getId(), channelManagers);

    // Assertions
    verify();
  }

  @Test(expected = MaxChannelManagersFeatureException.class)
  public void testCreateWhenChannelManagerLimitReached() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    List<User> users = new ArrayList<User>();
    users.add(user);

    Channel channel = buildChannel();
    channel.addFeature(buildChannelManagerFeature(ENABLED, MAX_CHANNEL_MANAGERS));

    List<ChannelManager> channelManagers = buildChannelManagers();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorToThrowExcpetionWhenCreating(new MaxChannelManagersFeatureException(channel.getId(),
        Integer.valueOf(MAX_CHANNEL_MANAGERS), channelManagers.size()));
    replay();

    // Do test
    uut.create(buildSessionUser(), channel.getId(), channelManagers);

    // Assertions
    verify();
  }

  @Test
  public void testUpdate() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    Channel channel = buildChannel();
    channel.addFeature(buildChannelManagerFeature(ENABLED, MAX_CHANNEL_MANAGERS));

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorWhenUpdating();
    expectChannelManagerDbDaoWhenUpdating(channelManager);
    expectUserServiceWhenFiding(user);
    expectUpdateChannelManagerAuditRecordDto(USER_ID, channel.getId());
    replay();

    // Do test
    ChannelManager updatedChannelManager = uut.update(buildSessionUser(), channel.getId(), channelManager);

    // Assertions
    assertEquals(channelManager, updatedChannelManager);
    verify();
  }

  @Test(expected = ChannelManagerNotFoundException.class)
  public void testUpdateWhenChannelManagerDoesNotExist() {

    // Set up
    Channel channel = buildChannel();

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorToThrowExcpetionWhenUpdating(new ChannelManagerNotFoundException(
        channelManager.getId()));
    replay();

    // Do test
    uut.update(buildSessionUser(), channel.getId(), channelManager);

    // Assertions
    verify();
  }

  @Test(expected = ChannelNotFoundException.class)
  public void testUpdateWhenChannelDoesNotExist() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    List<User> users = new ArrayList<User>();
    users.add(user);

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelFinderToThrowExceptionWhenFinding();
    replay();

    // Do test
    uut.update(buildSessionUser(), CHANNEL_ID, channelManager);

    // Assertions
    verify();
  }

  @Test(expected = FeatureNotEnabledException.class)
  public void testUpdateWhenChannelManagerFeatureDidabled() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    List<User> users = new ArrayList<User>();
    users.add(user);

    Channel channel = buildChannel();
    channel.addFeature(buildChannelManagerFeature(DISABLED, MAX_CHANNEL_MANAGERS));

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorToThrowExcpetionWhenUpdating(new FeatureNotEnabledException(""));
    replay();

    // Do test
    uut.update(buildSessionUser(), channel.getId(), channelManager);

    // Assertions
    verify();
  }

  @Test
  public void testDelete() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    Channel channel = buildChannel();
    channel.addFeature(buildChannelManagerFeature(ENABLED, MAX_CHANNEL_MANAGERS));

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorWhenDeleting();
    expectChannelManagerDbDaoWhenDeleting();
    expectChannelManagerDbDaoWhenFinding(channelManager);
    expectDeleteChannelManagerAuditRecordDto(USER_ID, channel.getId(), channelManager);
    replay();

    // Do test
    uut.delete(buildSessionUser(), channel.getId(), channelManager.getId());

    // Assertions
    verify();
  }

  @Test(expected = ChannelManagerNotFoundException.class)
  public void testDeleteWhenChannelManagerDoesNotExist() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    Channel channel = buildChannel();
    channel.addFeature(buildChannelManagerFeature(ENABLED, MAX_CHANNEL_MANAGERS));

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorToThrowExcpetionWhenDeleting(new ChannelManagerNotFoundException(
        channelManager.getId()));
    replay();

    // Do test
    uut.delete(buildSessionUser(), channel.getId(), channelManager.getId());

    // Assertions
    verify();
  }

  @Test(expected = ChannelNotFoundException.class)
  public void testDeleteWhenChannelDoesNotExist() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelFinderToThrowExceptionWhenFinding();
    replay();

    // Do test
    uut.delete(buildSessionUser(), CHANNEL_ID, channelManager.getId());

    // Assertions
    verify();
  }

  @Test(expected = FeatureNotEnabledException.class)
  public void testDeleteWhenChannelManagerFeatureDidabled() {

    // Set up
    User user = new User();
    user.setId(USER_ID);

    Channel channel = buildChannel();
    channel.addFeature(buildChannelManagerFeature(DISABLED, MAX_CHANNEL_MANAGERS));

    ChannelManager channelManager = buildChannelManager();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorToThrowExcpetionWhenDeleting(new FeatureNotEnabledException(""));
    replay();

    // Do test
    uut.delete(buildSessionUser(), channel.getId(), channelManager.getId());

    // Assertions
    verify();
  }

  @Test
  public void testDeleteByUser() {

    // Set up

    // Expectations

    // Do test
    uut.delete(USER_ID);

    // Assertions
  }

  private void expectUserSearchCriteriaBuilder(final UsersSearchCriteria criteria) {
    EasyMock.expect(mockUserSearchCriteriaBuilder.buildWithDefaultValues()).andReturn(criteria);
    expect(mockUserSearchCriteriaBuilder);
  }

  @SuppressWarnings("unchecked")
  private void expectChannelManagerValidatorWhenCreating() {
    mockValidator.assertCreate(isA(User.class), isA(Channel.class), isA(List.class));
    EasyMock.expectLastCall();
    expect(mockValidator);
  }

  @SuppressWarnings("unchecked")
  private void expectChannelManagerValidatorToThrowExcpetionWhenCreating(final Throwable cause) {
    mockValidator.assertCreate(isA(User.class), isA(Channel.class), isA(List.class));
    EasyMock.expectLastCall().andThrow(cause);
    expect(mockValidator);
  }

  private void expectChannelManagerValidatorWhenUpdating() {
    mockValidator.assertUpdate(isA(User.class), isA(Channel.class), isA(ChannelManager.class));
    EasyMock.expectLastCall();
    expect(mockValidator);
  }

  private void expectChannelManagerValidatorToThrowExcpetionWhenUpdating(final Throwable cause) {
    mockValidator.assertUpdate(isA(User.class), isA(Channel.class), isA(ChannelManager.class));
    EasyMock.expectLastCall().andThrow(cause);
    expect(mockValidator);
  }

  private void expectChannelManagerValidatorWhenDeleting() {
    mockValidator.assertDelete(isA(User.class), isA(Channel.class), isA(Long.class));
    EasyMock.expectLastCall();
    expect(mockValidator);
  }

  private void expectChannelManagerValidatorToThrowExcpetionWhenDeleting(final Throwable cause) {
    mockValidator.assertDelete(isA(User.class), isA(Channel.class), isA(Long.class));
    EasyMock.expectLastCall().andThrow(cause);
    expect(mockValidator);
  }

  private void expectChannelManagerDbDaoWhenFinding(ChannelManager channelManager) {
    EasyMock.expect(mockChannelManagerDbDao.find(EasyMock.isA(Long.class))).andReturn(channelManager);
    expect(mockChannelManagerDbDao);
  }

  private void expectChannelManagerDbDaoWhenDeleting() {
    mockChannelManagerDbDao.delete(isA(Long.class));
    EasyMock.expectLastCall();
    expect(mockChannelManagerDbDao);
  }

  private void expectChannelFinderToThrowExceptionWhenFinding() {
    EasyMock.expect(mockChannelFinder.find(isA(Long.class))).andThrow(new ChannelNotFoundException(CHANNEL_ID));
    expect(mockChannelFinder);
  }

  private void expectChannelManagerDbDaoWhenCreating(final ChannelManager channelManager) {
    EasyMock.expect(mockChannelManagerDbDao.create(isA(Channel.class), isA(ChannelManager.class))).andReturn(
        channelManager);
    expect(mockChannelManagerDbDao);
  }

  private void expectChannelManagerDbDaoWhenUpdating(final ChannelManager channelManager) {
    EasyMock.expect(mockChannelManagerDbDao.update(isA(ChannelManager.class))).andReturn(channelManager);
    expect(mockChannelManagerDbDao);
  }

  private void expectUserServiceWhenFiding(final User user) {
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    expect(mockUserServiceDao);
  }

  @SuppressWarnings("unchecked")
  private void expectUserServiceWhenFiding(final List<User> users) {
    EasyMock.expect(mockUserServiceDao.findByEmailAddress(isA(List.class), isA(UsersSearchCriteria.class))).andReturn(
        users);
    expect(mockUserServiceDao);
  }

  private void expectChannelFinderWhenFinding(final Channel channel) {
    EasyMock.expect(mockChannelFinder.find(isA(Long.class))).andReturn(channel);
    expect(mockChannelFinder);
  }

  private void expectCreateChannelManagerAuditRecordDto(final Long userId, final Long channelId) {
    AuditRecordDto auditRecordDto = new AuditRecordDto();
    String actionDescription = "User ids: [" + userId + "] created as channel managers.";
    EasyMock.expect(mockAuditRecordDtoBuilder.create(userId, channelId, null, actionDescription)).andReturn(
        auditRecordDto);
    expect(mockAuditRecordDtoBuilder);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();
    expect(mockAuditQueue);
  }

  private void expectUpdateChannelManagerAuditRecordDto(final Long userId, final Long channelId) {
    AuditRecordDto auditRecordDto = new AuditRecordDto();
    String actionDescription = "Updated user id [" + userId + "].";
    EasyMock.expect(mockAuditRecordDtoBuilder.update(userId, channelId, null, actionDescription)).andReturn(
        auditRecordDto);
    expect(mockAuditRecordDtoBuilder);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();
    expect(mockAuditQueue);
  }

  private void expectDeleteChannelManagerAuditRecordDto(final Long userId, final Long channelId,
      final ChannelManager channelManager) {
    AuditRecordDto auditRecordDto = new AuditRecordDto();
    String actionDescription = "Deleted channel manager with user id [" + channelManager.getUser().getId() + "].";
    EasyMock.expect(mockAuditRecordDtoBuilder.delete(userId, channelId, null, actionDescription)).andReturn(
        auditRecordDto);
    expect(mockAuditRecordDtoBuilder);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();
    expect(mockAuditQueue);
  }

  private void expect(final Object obj) {
    mocks.add(obj);
  }

  private void replay() {
    EasyMock.replay(mocks.toArray());
  }

  private void verify() {
    EasyMock.verify(mocks.toArray());
  }

  private User buildSessionUser() {
    return new User(USER_ID);
  }

  private UsersSearchCriteria buildUserSearchCriteria() {
    UsersSearchCriteria criteria = new UsersSearchCriteria();
    criteria.setPageNumber(PAGE_NUMBER);
    criteria.setPageSize(PAGE_SIZE);
    return criteria;
  }

  private Feature buildChannelManagerFeature(final boolean isEnabled, final String value) {
    return buildChannelFeature(Feature.Type.CHANNEL_MANAGERS, isEnabled, value);
  }

  private Feature buildChannelFeature(final Feature.Type type, final boolean isEnabled, final String value) {
    Feature channelFeature = new Feature(type);
    channelFeature.setIsEnabled(isEnabled);
    channelFeature.setValue(value);

    return channelFeature;
  }

  private Channel buildChannel() {
    return buildChannel(CHANNEL_ID, USER_ID);
  }

  private Channel buildChannel(final Long channelId, final Long ownerUserId) {

    Channel channel = new Channel(channelId);
    channel.setOwnerUserId(ownerUserId);

    return channel;
  }

  private List<ChannelManager> buildChannelManagers() {

    Channel channel = new Channel(CHANNEL_ID);

    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(buildChannelManager(channel));

    return channelManagers;
  }

  private ChannelManager buildChannelManager() {

    Channel channel = new Channel(CHANNEL_ID);

    return buildChannelManager(channel);
  }

  private ChannelManager buildChannelManager(final Channel channel) {

    User user = new User();
    user.setId(USER_ID);
    user.setEmail(EMAIL_ADDRESS);

    return buildChannelManager(channel, user, CHANNEL_MANAGER_ID, EMAIL_ALERTS_ENABLED);
  }

  private ChannelManager buildChannelManager(final Channel channel, final User user, final Long channelManagerId,
      final boolean emailAlerts) {

    ChannelManager channelManager = new ChannelManager();
    channelManager.setId(channelManagerId);
    channelManager.setUser(user);
    channelManager.setChannel(channel);
    channelManager.setEmailAlerts(emailAlerts);

    return channelManager;
  }

}
