/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationServiceTest.java 72595 2014-01-15 13:37:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.easymock.EasyMock.createMock;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.queue.CommunityQueuer;
import com.brighttalk.service.channel.business.queue.EmailQueuer;
import com.brighttalk.service.channel.business.queue.LesQueuer;
import com.brighttalk.service.channel.business.queue.SummitQueuer;
import com.brighttalk.service.channel.integration.booking.HDBookingServiceDao;
import com.brighttalk.service.channel.integration.community.CommunityServiceDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.ScheduledPublicationDbDao;
import com.brighttalk.service.channel.integration.email.EmailServiceDao;
import com.brighttalk.service.channel.integration.les.LiveEventServiceDao;
import com.brighttalk.service.channel.integration.summit.SummitServiceDao;

/**
 * Unit tests for {@link CommunicationCancelService}.
 */
public class HDCommunicationCancelServiceTest extends AbstractCommunicationServiceTest {

  private HDCommunicationCancelService uut;

  @Before
  public void setUp() {

    uut = new HDCommunicationCancelService();

    mockCommunicationFinder = createMock(CommunicationFinder.class);
    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);

    mockCommunicationDao = createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDao);

    mockEmailServiceDao = EasyMock.createMock(EmailServiceDao.class);
    ReflectionTestUtils.setField(uut, "emailServiceDao", mockEmailServiceDao);

    mockLiveEventServiceDao = EasyMock.createMock(LiveEventServiceDao.class);
    ReflectionTestUtils.setField(uut, "liveEventServiceDao", mockLiveEventServiceDao);

    mockSummitServiceDao = EasyMock.createMock(SummitServiceDao.class);
    ReflectionTestUtils.setField(uut, "summitServiceDao", mockSummitServiceDao);

    mockCommunityServiceDao = EasyMock.createMock(CommunityServiceDao.class);
    ReflectionTestUtils.setField(uut, "communityServiceDao", mockCommunityServiceDao);

    mockScheduledPublicationDbDao = EasyMock.createMock(ScheduledPublicationDbDao.class);
    ReflectionTestUtils.setField(uut, "scheduledPublicationDbDao", mockScheduledPublicationDbDao);

    mockCommunityQueuer = EasyMock.createMock(CommunityQueuer.class);
    ReflectionTestUtils.setField(uut, "communityQueuer", mockCommunityQueuer);

    mockEmailQueuer = EasyMock.createMock(EmailQueuer.class);
    ReflectionTestUtils.setField(uut, "emailQueuer", mockEmailQueuer);

    mockLesQueuer = EasyMock.createMock(LesQueuer.class);
    ReflectionTestUtils.setField(uut, "lesQueuer", mockLesQueuer);

    mockSummitQueuer = EasyMock.createMock(SummitQueuer.class);
    ReflectionTestUtils.setField(uut, "summitQueuer", mockSummitQueuer);

    mockHDBookingServiceDao = EasyMock.createMock(HDBookingServiceDao.class);
    ReflectionTestUtils.setField(uut, "hDBookingServiceDao", mockHDBookingServiceDao);
  }

  @Test
  public void testCancel() {
    // Set up
    Communication communication = createCommunication();

    // Expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    mockCommunicationDao.cancel(communication.getId());
    EasyMock.expectLastCall();

    mockHDBookingServiceDao.deleteBooking(communication);
    EasyMock.expectLastCall();

    mockScheduledPublicationDbDao.deactivatePublicationDate(communication);
    EasyMock.expectLastCall();

    mockEmailQueuer.cancel(communication.getId());
    EasyMock.expectLastCall();

    mockLesQueuer.cancel(communication.getId());
    EasyMock.expectLastCall();

    mockSummitQueuer.delete(communication.getId());
    EasyMock.expectLastCall();

    mockCommunityQueuer.delete(communication.getId());
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao, mockScheduledPublicationDbDao, mockEmailQueuer,
        mockLesQueuer, mockSummitQueuer, mockCommunityQueuer);

    // Excercise
    uut.cancel(communication);

    // verification
    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao, mockScheduledPublicationDbDao, mockEmailQueuer,
        mockLesQueuer, mockSummitQueuer, mockCommunityQueuer);
  }

  @Test
  public void testLesCancelCallback() {

    // Set up
    Communication communication = createCommunication();

    // Expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    mockLiveEventServiceDao.cancel(communication);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockLiveEventServiceDao);

    // Excercise
    uut.lesCancelCallback(communication.getId());

    // Assertions
    EasyMock.verify(mockCommunicationFinder, mockLiveEventServiceDao);
  }

  @Test
  public void testSummitDeleteCallback() {

    // Set up
    Communication communication = createCommunication();

    // Expectations

    mockSummitServiceDao.delete(communication.getId());
    EasyMock.expectLastCall();

    EasyMock.replay(mockSummitServiceDao);

    // Excercise
    uut.summitDeleteCallback(communication.getId());

    // Assertions
    EasyMock.verify(mockSummitServiceDao);
  }

  @Test
  public void testCommunityDeleteCallback() {

    // Set up
    Communication communication = createCommunication();
    List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add(communication.getId());

    // Expectations
    EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    mockCommunityServiceDao.deleteCommunications(communication.getChannelId(), communicationIds);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockCommunityServiceDao);

    // Excercise
    uut.communityDeleteCallback(communication.getId());

    // Assertions
    EasyMock.verify(mockCommunicationFinder, mockCommunityServiceDao);
  }

  @Test
  public void testEmailCancelCallback() {

    // Set up
    Communication communication = createCommunication();
    List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add(communication.getId());

    // Expectations
    // EasyMock.expect(mockCommunicationFinder.find(communication.getId())).andReturn(communication);

    // mockEmailServiceDao.sendCommunicationCancelConfirmation(communication);
    // EasyMock.expectLastCall();

    // EasyMock.replay(mockCommunicationFinder, mockEmailServiceDao);

    // Excercise
    // uut.emailCancelCallback(communication.getId());

    // Assertions
    // EasyMock.verify(mockCommunicationFinder, mockEmailServiceDao);
  }

  private Communication createCommunication() {
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    return communication;

  }
}