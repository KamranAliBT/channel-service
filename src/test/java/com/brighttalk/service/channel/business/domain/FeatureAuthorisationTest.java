/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeatureAuthorisationTest.java 100001 2015-09-08 08:53:13Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.User.Role;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;

public class FeatureAuthorisationTest {

  private static final Long USER_ID = 321L;

  private final Long OWNER_USER_ID = 123L;

  private FeatureAuthorisation uut;

  @Before
  public void setUp() {
    uut = new FeatureAuthorisation();
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating channel features
   * with Basic user no roles.
   * <p>
   * Expected Result - The user should not be authorised to update the features.
   */
  @Test(expected = UserAuthorisationException.class)
  public void authoriseBasicUser() {

    // Set up
    User user = new User();

    Channel channel = buildChannel();
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, false));

    List<Feature> featuresToUpdate = new ArrayList<>();

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating channel features
   * with BrightTALK manager role and channel feature is not in {@link Type#MAY_DISABLE_EMAILS_FEATURES}.
   * <p>
   * Expected Result - The user should be authorised to update the features.
   */
  @Test
  public void authoriseBrightTalkAdministrator() {

    // Set up
    User user = buildUser(Role.MANAGER);

    Channel channel = buildChannel();
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, false));

    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.ADS_DISPLAYED));

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating channel features
   * with BrightTALK manager role and {@link Type#MAY_DISABLE_EMAILS} feature set to false (disabled).
   * <p>
   * Expected Result - The user should be authorised to update the feature.
   */
  @Test
  public void authoriseBrightTalkAdministratorWhileMayDisableEmailsFeatureDisabled() {

    // Set up
    User user = buildUser(Role.MANAGER);

    Channel channel = buildChannel();
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, false));
    channel.addFeature(buildFeature(Type.MAY_DISABLE_EMAILS, false));

    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.EMAIL_CHANNEL_SUBSCRIPTION));

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating channel features
   * with BrightTALK manager role.
   * <p>
   * Expected Result - The user should be authorised to update the feature.
   */
  @Test
  public void authoriseBrightTalkAdministratorWhileMayDisableEmailsFeatureEnabled() {

    // Set up
    User user = buildUser(Role.MANAGER);

    Channel channel = buildChannel();
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, false));
    channel.addFeature(buildFeature(Type.MAY_DISABLE_EMAILS));

    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.EMAIL_CHANNEL_SUBSCRIPTION));

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating
   * {@link Type#ADS_DISPLAYED} with Channel Owner permission.
   * <p>
   * Expected Result - The user should not be authorised to update the feature, since {@link Type#ADS_DISPLAYED} channel
   * feature not allowed to be updated.
   */
  @Test(expected = UserAuthorisationException.class)
  public void authoriseChannelOwner() {

    // Set up
    User user = buildUser();

    Channel channel = buildChannel(user.getId());
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, false));

    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.ADS_DISPLAYED));

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating
   * {@link Type#EMAIL_CHANNEL_SUBSCRIPTION} with Channel Owner permission and {@link Type#MAY_DISABLE_EMAILS} feature
   * set to false (disabled).
   * <p>
   * Expected Result - The user should not be authorised to update the feature, since
   * {@link Type#EMAIL_CHANNEL_SUBSCRIPTION} channel feature not allowed to be disabled.
   */
  @Test(expected = UserAuthorisationException.class)
  public void authoriseChannelOwnerWhileMayDisableEmailsFeatureDisabled() {

    // Set up
    User user = buildUser();

    Channel channel = buildChannel(user.getId());
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, false));
    channel.addFeature(buildFeature(Type.MAY_DISABLE_EMAILS, false));

    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.EMAIL_CHANNEL_SUBSCRIPTION));

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating
   * {@link Type#EMAIL_CHANNEL_SUBSCRIPTION} with Channel Owner permission and {@link Type#MAY_DISABLE_EMAILS} feature
   * set to true.
   * <p>
   * Expected Result - The user should be authorised to update the feature.
   */
  @Test
  public void authoriseChannelOwnerWhileMayDisableEmailsFeatureEnabled() {

    // Set up
    User user = buildUser();

    Channel channel = buildChannel(user.getId());
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, false));
    channel.addFeature(buildFeature(Type.MAY_DISABLE_EMAILS));

    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.EMAIL_CHANNEL_SUBSCRIPTION));

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating
   * {@link Type#ADS_DISPLAYED} with Channel Manager permission enabled.
   * <p>
   * Expected Result - The user should not be authorised to update the feature, since channel manager role not
   * authorised to update {@link Type#ADS_DISPLAYED} channel feature.
   */
  @Test(expected = UserAuthorisationException.class)
  public void authoriseChannelManager() {

    // Set up
    User user = buildUser();

    Channel channel = buildChannel();
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, true));
    channel.setChannelManagers(buildChannelManagers(user));

    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.ADS_DISPLAYED));

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating
   * {@link Type#EMAIL_CHANNEL_SUBSCRIPTION} while {@link Type#MAY_DISABLE_EMAILS} is disabled.
   * <p>
   * Expected Result - The user should not be authorised to update the feature.
   */
  @Test(expected = UserAuthorisationException.class)
  public void authoriseChannelManagerWhileMayDisableEmailsFeatureDisabled() {

    // Set up
    User user = buildUser();

    Channel channel = buildChannel();
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, true));
    channel.setChannelManagers(buildChannelManagers(user));
    channel.addFeature(buildFeature(Type.MAY_DISABLE_EMAILS, false));

    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.EMAIL_CHANNEL_SUBSCRIPTION));

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating
   * {@link Type#EMAIL_CHANNEL_SUBSCRIPTION} while {@link Type#MAY_DISABLE_EMAILS} is enabled.
   * <p>
   * Expected Result - The user should be authorised to update the feature.
   */
  @Test
  public void authoriseChannelManagerWhileMayDisableEmailsFeatureEnabled() {

    // Set up
    User user = buildUser();

    Channel channel = buildChannel();
    channel.addFeature(buildFeature(Type.CHANNEL_MANAGERS, true));
    channel.setChannelManagers(buildChannelManagers(user));
    channel.addFeature(buildFeature(Type.MAY_DISABLE_EMAILS));

    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.EMAIL_CHANNEL_SUBSCRIPTION));

    // Expectations

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating
   * {@link Type#EMAIL_COMM_RERUN} while {@link Type#MAY_DISABLE_EMAILS} is disabled.
   * <p>
   * Expected Result - The user should not be authorised to update the feature.
   */
  @Test(expected = UserAuthorisationException.class)
  public void authoriseChannelOwnerWhileMayDisableEmailsFeatureDisabledAndUpdateEmailCommRerunFeature() {
    // Set up
    User user = buildUser();

    Channel channel = buildChannel(user.getId());
    channel.addFeature(buildFeature(Type.MAY_DISABLE_EMAILS, false));
    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.EMAIL_COMM_RERUN));

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  /**
   * Tests {@link FeatureAuthorisation#authoriseForUpdate(User, Channel, List)} in the case of updating
   * {@link Type#EMAIL_COMM_RERUN} while {@link Type#MAY_DISABLE_EMAILS} is enabled.
   * <p>
   * Expected Result - The user should be authorised to update the feature.
   */
  @Test
  public void authoriseChannelOwnerWhileMayDisableEmailsFeatureEnabledAndUpdateEmailCommRerunFeature() {
    // Set up
    User user = buildUser();

    Channel channel = buildChannel(user.getId());
    channel.addFeature(buildFeature(Type.MAY_DISABLE_EMAILS, true));
    List<Feature> featuresToUpdate = new ArrayList<>();
    featuresToUpdate.add(buildFeature(Type.EMAIL_COMM_RERUN));

    // Do test
    uut.authoriseForUpdate(user, channel, featuresToUpdate);
  }

  private List<ChannelManager> buildChannelManagers(final User... users) {
    List<ChannelManager> channelManagers = new ArrayList<>();

    for (User user : users) {
      ChannelManager channelManager = new ChannelManager();
      channelManager.setUser(user);
      channelManagers.add(channelManager);
    }

    return channelManagers;
  }

  private Feature buildFeature(final Type type) {
    return buildFeature(type, true);
  }

  private Feature buildFeature(final Type type, final boolean isEnabled) {
    Feature feature = new Feature(type);
    feature.setIsEnabled(isEnabled);
    return feature;
  }

  private User buildUser(final Role... roles) {
    return buildUser(USER_ID, roles);
  }

  private User buildUser(final Long userId, final Role... roles) {
    User user = new User(userId);
    user.setRoles(Arrays.asList(roles));
    return user;
  }

  private Channel buildChannel() {
    return buildChannel(OWNER_USER_ID);
  }

  private Channel buildChannel(final Long channelOwnerId) {
    Channel channel = new Channel();
    channel.setOwnerUserId(channelOwnerId);
    return channel;
  }

}