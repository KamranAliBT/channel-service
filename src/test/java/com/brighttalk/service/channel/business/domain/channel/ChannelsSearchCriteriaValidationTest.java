/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: ChannelsSearchCriteriaValidationTest.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;

import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;

@RunWith(Theories.class)
public class ChannelsSearchCriteriaValidationTest {

  private static Validator validator;

  private ChannelsSearchCriteria uut;

  @BeforeClass
  public static void createValidator() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Before
  public void setUp() {
    uut = new ChannelsSearchCriteria();
  }

  @Test
  public void failValidationWhenChannelIdsIsNull() {

    uut.setIds(null);

    // do test
    Set<ConstraintViolation<ChannelsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenChannelIdsIsEmpty() {

    uut.setIds("");

    // do test
    Set<ConstraintViolation<ChannelsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenChannelIdsHasLeters() {

    uut.setIds("abc");

    // do test
    Set<ConstraintViolation<ChannelsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenChannelIdsHasNumberAndLeters() {

    uut.setIds("10,abc");

    // do test
    Set<ConstraintViolation<ChannelsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenChannelIdsHasNumberAndSemicolonAsSeperator() {

    uut.setIds("10;15");

    // do test
    Set<ConstraintViolation<ChannelsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void pasValidationWhenChannelIdsHasNumbersAndColonAsSeperator() {

    uut.setIds("10,15");

    // do test
    Set<ConstraintViolation<ChannelsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(0, constraintViolations.size());
  }
}