/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MockDeleteChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Implementation of a email Queuer that does not actually use a queue.
 * 
 * The required processing starts straight away.
 * 
 * This is used for unit testing.
 */
public class MockEmailQueuer implements EmailQueuer {

  /**
   * {@inheritDoc}
   */
  @Override
  public void create(final Long webcastId) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void reschedule(final Long webcastId) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void cancel(final Long webcastId) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void videoUploadComplete(final Long webcastId, final String provider) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void videoUploadError(final Long webcastId, final String provider) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void missedYou(final Long webcastId) {

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void recordingPublished(final Long webcastId) {
  }

}