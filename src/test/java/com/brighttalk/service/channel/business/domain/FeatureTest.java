/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeatureTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Feature.Type;

/**
 * Unit Tests for {@link Feature}.
 */
public class FeatureTest {

  private Feature uut;

  @Before
  public void setUp() {

  }

  @Test(expected = ApplicationException.class)
  public void convertDbNameWhileInvalid() {

    // Set up

    // Expectations

    // Do test
    Feature.Type.convertDbName("invalid");

    // Assertions
  }

  @Test(expected = ApplicationException.class)
  public void convertApiNameWhileInvalid() {

    // Set up

    // Expectations

    // Do test
    Feature.Type.convertApiName("invalid");

    // Assertions
  }

  @Test
  public void convertApiNameWhileCaseInsensitive() {

    // Set up

    // Expectations

    // Do test
    Feature.Type.convertApiName("AdSdiSplAyED");

    // Assertions
  }

  @Test
  public void convertApiNameWithSuffix() {

    // Set up

    // Expectations

    // Do test
    Feature.Type.convertSuffix("email");

    // Assertions
  }

  @Test
  public void convertApiNameWithSuffixAndCaseInsensitive() {

    // Set up

    // Expectations

    // Do test
    Feature.Type.convertSuffix("EmAiL");

    // Assertions
  }

  @Test
  public void convertDbName() {

    // Set up

    // Expectations

    // Do test
    Feature.Type.convertDbName("ads_displayed");
    Feature.Type.convertDbName("allowed_realms");
    Feature.Type.convertDbName("attendee_networking");
    Feature.Type.convertDbName("block_user");
    Feature.Type.convertDbName("channel_managers");
    Feature.Type.convertDbName("custom_communication_url");
    Feature.Type.convertDbName("custom_email_address");
    Feature.Type.convertDbName("custom_email_name");
    Feature.Type.convertDbName("custom_registration_template");
    Feature.Type.convertDbName("custom_url");
    Feature.Type.convertDbName("download_reports");
    Feature.Type.convertDbName("email_channel_subscription");
    Feature.Type.convertDbName("email_comm_24hr_reminder");
    Feature.Type.convertDbName("email_comm_cancelled");
    Feature.Type.convertDbName("email_comm_missed_you");
    Feature.Type.convertDbName("email_comm_registration_confirmation");
    Feature.Type.convertDbName("email_comm_rerun");
    Feature.Type.convertDbName("email_comm_rescheduled");
    Feature.Type.convertDbName("email_comm_starting_now");
    Feature.Type.convertDbName("email_comm_viewer_followup");
    Feature.Type.convertDbName("email_opt_out_error_url");
    Feature.Type.convertDbName("email_opt_out_thankyou_url");
    Feature.Type.convertDbName("email_user_registration");
    Feature.Type.convertDbName("email_weekly_email");
    Feature.Type.convertDbName("embed_tracking");
    Feature.Type.convertDbName("enable_communication_custom_url");
    Feature.Type.convertDbName("enable_self_service_video_communications");
    Feature.Type.convertDbName("locale");
    Feature.Type.convertDbName("lockdown_url");
    Feature.Type.convertDbName("max_channels_for_user");
    Feature.Type.convertDbName("max_communications_first_month");
    Feature.Type.convertDbName("max_communications_period_length");
    Feature.Type.convertDbName("max_communications_per_period");
    Feature.Type.convertDbName("max_communication_length");
    Feature.Type.convertDbName("may_disable_emails");
    Feature.Type.convertDbName("may_edit_emails");
    Feature.Type.convertDbName("media_zones");
    Feature.Type.convertDbName("player_style_sheet");
    Feature.Type.convertDbName("pro_webinar");
    Feature.Type.convertDbName("pro_webinar_slides");
    Feature.Type.convertDbName("released_subscriber_data");
    Feature.Type.convertDbName("screen_share");
    Feature.Type.convertDbName("search_published_threshold");
    Feature.Type.convertDbName("self_syndication");
    Feature.Type.convertDbName("share_email");
    Feature.Type.convertDbName("share_embed");
    Feature.Type.convertDbName("share_preroll");
    Feature.Type.convertDbName("share_socialise");
    Feature.Type.convertDbName("show_media_zone_links");
    Feature.Type.convertDbName("support_service_level");
    Feature.Type.convertDbName("surveys");
    Feature.Type.convertDbName("syndications_included_in_content_plan");
    Feature.Type.convertDbName("syndication_overrides");

    // Assertions
  }

  @Test
  public void convertApiName() {

    // Set up

    // Expectations

    // Do test
    Feature.Type.convertApiName("adsDisplayed");
    Feature.Type.convertApiName("allowedRealms");
    Feature.Type.convertApiName("attendeeNetworking");
    Feature.Type.convertApiName("blockedUsers");
    Feature.Type.convertApiName("lockdownURL");
    Feature.Type.convertApiName("channelManagers");
    Feature.Type.convertApiName("customEmailName");
    Feature.Type.convertApiName("customEmailAddress");
    Feature.Type.convertApiName("customRegistrationTemplate");
    Feature.Type.convertApiName("customURL");
    Feature.Type.convertApiName("customCommunicationURL");
    Feature.Type.convertApiName("csvDownloads");
    Feature.Type.convertApiName("emailChannelSubscription");
    Feature.Type.convertApiName("emailComm24hrReminder");
    Feature.Type.convertApiName("emailCommCancelled");
    Feature.Type.convertApiName("emailCommViewerFollowup");
    Feature.Type.convertApiName("emailCommMissedYou");
    Feature.Type.convertApiName("emailCommRegistrationConfirmation");
    Feature.Type.convertApiName("emailCommRescheduled");
    Feature.Type.convertApiName("emailCommRerun");
    Feature.Type.convertApiName("emailCommStartingNow");
    Feature.Type.convertApiName("emailOptOutThankyouUrl");
    Feature.Type.convertApiName("emailOptOutErrorUrl");
    Feature.Type.convertApiName("emailWeeklyEmail");
    Feature.Type.convertApiName("emailUserRegistration");
    Feature.Type.convertApiName("embedTracking");
    Feature.Type.convertApiName("enableCommunicationCustomURL");
    Feature.Type.convertApiName("enableSelfServiceVideoCommunications");
    Feature.Type.convertApiName("lookupAnonUserForReports");
    Feature.Type.convertApiName("locale");
    Feature.Type.convertApiName("mayDisableEmails");
    Feature.Type.convertApiName("mayEditEmails");
    Feature.Type.convertApiName("channelsPerUser");
    Feature.Type.convertApiName("maxCommunicationsFirstMonth");
    Feature.Type.convertApiName("maxCommunicationsPerPeriod");
    Feature.Type.convertApiName("maxCommunicationsPeriodLength");
    Feature.Type.convertApiName("maxCommunicationDuration");
    Feature.Type.convertApiName("mediaZones");
    Feature.Type.convertApiName("playerStyleSheet");
    Feature.Type.convertApiName("portalPublishedThreshold");
    Feature.Type.convertApiName("subscriberRelease");
    Feature.Type.convertApiName("screenShare");
    Feature.Type.convertApiName("proWebinar");
    Feature.Type.convertApiName("proWebinarSlides");
    Feature.Type.convertApiName("selfSyndication");
    Feature.Type.convertApiName("shareEmail");
    Feature.Type.convertApiName("shareEmbed");
    Feature.Type.convertApiName("sharePreroll");
    Feature.Type.convertApiName("shareSocialise");
    Feature.Type.convertApiName("showMediaZoneLinks");
    Feature.Type.convertApiName("surveys");
    Feature.Type.convertApiName("supportServiceLevel");
    Feature.Type.convertApiName("syndicationOverrides");
    Feature.Type.convertApiName("syndicationsIncludedInContentPlan");

    // Assertions
  }

  @Test
  public void equalsWhenIsEnabledNotEqual() {
    // Setup
    uut = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    uut.setIsEnabled(false);
    Feature feature = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    feature.setIsEnabled(true);

    // Do Test
    assertFalse(uut.equals(feature));
  }

  @Test
  public void equalsWhenIsEnabledEqual() {
    // Setup
    uut = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    uut.setIsEnabled(false);
    Feature feature = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    feature.setIsEnabled(false);

    // Do Test
    assertTrue(uut.equals(feature));
  }

  @Test
  public void equalsWhenValueNotEqual() {
    // Setup
    uut = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    uut.setIsEnabled(true);
    uut.setValue("test1");
    Feature feature = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    feature.setIsEnabled(true);
    uut.setValue("test2");

    // Do Test
    assertFalse(uut.equals(feature));
  }

  @Test
  public void equalsWhenValueEqual() {
    // Setup
    uut = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    uut.setIsEnabled(false);
    uut.setValue("test3");
    Feature feature = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    feature.setIsEnabled(false);
    feature.setValue("test3");

    // Do Test
    assertTrue(uut.equals(feature));
  }

  @Test
  public void equalsWhenNameNotEqual() {
    // Setup
    uut = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    uut.setIsEnabled(true);
    uut.setValue("test4");
    Feature feature = new Feature(Type.ADS_DISPLAYED);
    feature.setIsEnabled(true);
    feature.setValue("test4");

    // Do Test
    assertFalse(uut.equals(feature));
  }

  @Test
  public void equalsWhenNameEqual() {
    // Setup
    uut = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    uut.setIsEnabled(false);
    uut.setValue("test5");
    Feature feature = new Feature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    feature.setIsEnabled(false);
    feature.setValue("test5");

    // Do Test
    assertTrue(uut.equals(feature));
  }

}