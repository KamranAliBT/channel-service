/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationResourceOrderingValidatorTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Unit tests for {@link CommunicationResourceOrderingValidator}.
 */
public class CommunicationResourceOrderingValidatorTest extends AbstractResourceTest {

  private CommunicationResourceOrderingValidator uut;

   /**
   * Test validation communication order resources successful.
   */
  @Test
  public void testValidateCommunicationOrderResourcesSuccessful() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    
    Resource resource1 = new Resource(RESOURCE_ID);
    resource1.setPrecedence(1);
    Resource resource2 = new Resource(RESOURCE_ID_2);
    resource2.setPrecedence(0);

    List<Resource> resourcesToBeReordered = new ArrayList<Resource>();
    resourcesToBeReordered.add(resource2);
    resourcesToBeReordered.add(resource1);
    
    uut = new CommunicationResourceOrderingValidator(resourcesToBeReordered);
   
    // do test
    try {
      uut.validate(communication);
      // assert
    } catch (ApplicationException e) {
      Assert.fail();
    }
  }
  
  /**
   * Test validation communication with missing resource in resource to reorder list.
   */
  @Test
  public void testValidateCommunicationOrderResourcesWithMissing() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    
    Resource resource1 = new Resource(RESOURCE_ID);
    resource1.setPrecedence(1);
   

    List<Resource> resourcesToBeReordered = new ArrayList<Resource>();
    resourcesToBeReordered.add(resource1);
    
    uut = new CommunicationResourceOrderingValidator(resourcesToBeReordered);
   
    // do test
    try {
      uut.validate(communication);
      // assert
    } catch (ApplicationException e) {
      Assert.assertEquals("InvalidList", e.getUserErrorCode());
    }
  }
  
  /**
   * Test validation communication with duplicate resource in resource to reorder list.
   */
  @Test
  public void testValidateCommunicationOrderResourcesWithDuplicateResourceId() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    
    Resource resource1 = new Resource(RESOURCE_ID);
    resource1.setPrecedence(0);
    Resource resource2 = new Resource(RESOURCE_ID);
    resource2.setPrecedence(1);
   

    List<Resource> resourcesToBeReordered = new ArrayList<Resource>();
    resourcesToBeReordered.add(resource1);
    resourcesToBeReordered.add(resource2);
    
    uut = new CommunicationResourceOrderingValidator(resourcesToBeReordered);
   
    // do test
    try {
      uut.validate(communication);
      // assert
    } catch (ApplicationException e) {
      Assert.assertEquals("InvalidList", e.getUserErrorCode());
    }
  }
  
  /**
   * Test validation communication with empty resource to reorder list.
   */
  @Test
  public void testValidateCommunicationOrderResourcesWithEmptyList() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    
    List<Resource> resourcesToBeReordered = new ArrayList<Resource>();
    
    uut = new CommunicationResourceOrderingValidator(resourcesToBeReordered);
   
    // do test
    try {
      uut.validate(communication);
      // assert
    } catch (ApplicationException e) {
      Assert.assertEquals("InvalidList", e.getUserErrorCode());
    }
  }
  
  /**
   * Test validation communication with not existing element.
   */
  @Test
  public void testValidateCommunicationOrderResourcesWithNotExistingElement() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    
    Resource resource1 = new Resource(RESOURCE_ID);
    resource1.setPrecedence(0);
    Resource resource2 = new Resource(999999L);
    resource2.setPrecedence(1);
   

    List<Resource> resourcesToBeReordered = new ArrayList<Resource>();
    resourcesToBeReordered.add(resource1);
    resourcesToBeReordered.add(resource2);
    
    uut = new CommunicationResourceOrderingValidator(resourcesToBeReordered);
   
    // do test
    try {
      uut.validate(communication);
      // assert
    } catch (ApplicationException e) {
      Assert.assertEquals("InvalidList", e.getUserErrorCode());
    }
  }
}
