/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: ChannelsSearchCriteriaTest.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;

/**
 * Juni test for {@link ChannelsSearchCriteria}.
 */
public class ChannelsSearchCriteriaTest {

  @Test
  public void getChanelIdsWhenIdsContainsEmptyString() {

    // setup
    String ids = "";

    ChannelsSearchCriteria uut = new ChannelsSearchCriteria();
    uut.setIds(ids);

    // setup
    List<Long> result = uut.getChanelIds();

    // do assert
    assertEquals(0, result.size());

  }

  @Test
  public void getChanelIdsWhenIdsContainsSingleNumber() {

    // setup
    String ids = "10";

    ChannelsSearchCriteria uut = new ChannelsSearchCriteria();
    uut.setIds(ids);

    // setup
    List<Long> result = uut.getChanelIds();

    // do assert
    assertEquals(1, result.size());
    assertEquals(10, result.get(0).longValue());

  }

  @Test
  public void getChanelIdsWhenIdsContainsTwoNumbers() {

    // setup
    String ids = "10,20";

    ChannelsSearchCriteria uut = new ChannelsSearchCriteria();
    uut.setIds(ids);

    // setup
    List<Long> result = uut.getChanelIds();

    // do assert
    assertEquals(2, result.size());
    assertEquals(10, result.get(0).longValue());
    assertEquals(20, result.get(1).longValue());

  }

  @Test
  public void getChanelIdsWhenIdsContainsTwoExactSameNumbers() {

    // setup
    String ids = "10,10";

    ChannelsSearchCriteria uut = new ChannelsSearchCriteria();
    uut.setIds(ids);

    // setup
    List<Long> result = uut.getChanelIds();

    // do assert
    assertEquals(1, result.size());
    assertEquals(10, result.get(0).longValue());

  }

  @Test
  public void getChanelIdsWhenIdsContainsTenNumbers() {

    // setup
    String ids = "10,20,31,32,33,34,35,44,55,66";

    ChannelsSearchCriteria uut = new ChannelsSearchCriteria();
    uut.setIds(ids);

    // setup
    List<Long> result = uut.getChanelIds();

    // do assert
    assertEquals(10, result.size());

  }

}
