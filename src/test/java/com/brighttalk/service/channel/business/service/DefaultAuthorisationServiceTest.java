/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultAuthorisationServiceTest.java 87022 2014-12-03 13:00:40Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.user.Session;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;

/**
 * Unit tests for {@link DefaultAuthorisationService}.
 */
public class DefaultAuthorisationServiceTest {

  private static final Long CHANNEL_ID = 1L;

  private static final Long COMMUNICATION_ID = 2L;

  private static final Long USER_ID = 999L;

  private static final String SESSION_ID = "sessionId";

  private DefaultAuthorisationService uut;

  @Before
  public void setUp() {
    uut = new DefaultAuthorisationService();

  }

  @Test
  public void channelByUserId() {
    // Set up
    Random r = new Random();
    long userId = r.nextInt(999999999);
    long channelId = r.nextInt(999999999);

    Channel channel = new Channel(channelId);
    channel.setOwnerUserId(userId);
    User user = new User(userId);

    // Do Test
    try {
      uut.channelByUserId(channel, user);
      // Assertions
    } catch (Exception e) {
      Assert.fail();
    }
  }

  @Test
  public void channelByUserIdFails() {
    // Set up
    Random r = new Random();
    long userId = 111L;
    long channelId = r.nextInt(999999999);

    Channel channel = new Channel(channelId);
    channel.setOwnerUserId(userId);
    User user = new User(112L);

    // Do Test
    try {
      uut.channelByUserId(channel, user);
      // Assertions
      Assert.fail();
    } catch (Exception e) {
      Assert.assertTrue(e instanceof UserAuthorisationException);
    }

  }

  @Test
  public void testAuthoriseCommunicationAccessAsManager() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    User user = createManager();

    // do test
    try {
      uut.authoriseCommunicationAccess(user, channel, communication);

      // assertions
    } catch (UserAuthorisationException ue) {
      fail();
    }
  }

  @Test
  public void testAuthoriseCommunicationAccessAsPresenter() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    User user = createPresenter();

    // do test
    try {
      uut.authoriseCommunicationAccess(user, channel, communication);

      // assertions
    } catch (UserAuthorisationException ue) {
      fail();
    }
  }

  @Test
  public void testAuthoriseCommunicationAccessAsChannelOwner() {
    // setup
    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);
    Communication communication = createCommmunicationInMasterChannel();
    User user = createUser();

    // do test
    try {
      uut.authoriseCommunicationAccess(user, channel, communication);

      // assertions
    } catch (UserAuthorisationException ue) {
      fail();
    }
  }

  @Test(expected = UserAuthorisationException.class)
  public void testAuthoriseCommunicationAccessAsChannelManager() {
    // setup
    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Long userId = 123L;
    User user = createUser(userId);

    Long channelManagerUserId = 321L;
    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(createChannelManager(channelManagerUserId));

    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);
    channel.setFeatures(features);
    channel.setChannelManagers(channelManagers);

    Communication communication = createCommmunicationInMasterChannel();

    // do test
    uut.authoriseCommunicationAccess(user, channel, communication);
  }

  @Test
  public void testAuthoriseCommunicationAccessAsNotAuthorized() {
    // setup
    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Channel channel = createChannel();
    channel.setOwnerUserId(777L);
    channel.setFeatures(features);

    Communication communication = createCommmunicationInMasterChannel();
    User user = createUser();

    // do test
    try {
      uut.authoriseCommunicationAccess(user, channel, communication);
      fail();
      // assertions
    } catch (UserAuthorisationException ue) {
      assertEquals("NotAuthorised", ue.getUserErrorCode());
    }
  }

  private Channel createChannel() {
    Channel channel = new Channel(CHANNEL_ID);
    return channel;
  }

  private Communication createCommmunicationInMasterChannel() {
    return createCommunication(true);
  }

  private Communication createCommunication(final boolean isMasterChannel) {

    Communication communication = new Communication(COMMUNICATION_ID);

    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(isMasterChannel);

    communication.setConfiguration(configuration);
    communication.setStatus(Communication.Status.UPCOMING);
    communication.setProvider(new Provider(Provider.BRIGHTTALK));

    Date date = new Date();
    date.setTime(date.getTime() + 60 * 60000);
    communication.setScheduledDateTime(date);
    communication.getPresenters().add(new Session(SESSION_ID));

    return communication;
  }

  private User createManager() {

    User user = new User();
    user.setId(USER_ID);
    List<User.Role> roles = new ArrayList<User.Role>();
    roles.add(User.Role.MANAGER);
    user.setRoles(roles);
    return user;
  }

  private User createUser() {
    return createUser(USER_ID);
  }

  private User createUser(final Long userId) {

    User user = new User();
    user.setId(userId);
    List<User.Role> roles = new ArrayList<User.Role>();
    roles.add(User.Role.USER);
    user.setRoles(roles);
    return user;
  }

  private ChannelManager createChannelManager(final Long userId) {

    User user = new User();
    user.setId(userId);

    ChannelManager channelManager = new ChannelManager();
    channelManager.setUser(user);

    return channelManager;
  }

  private User createPresenter() {

    User user = new User();
    user.setId(USER_ID);
    List<User.Role> roles = new ArrayList<User.Role>();
    roles.add(User.Role.USER);
    user.setRoles(roles);
    user.setSession(new Session(SESSION_ID));
    return user;
  }
}
