/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationConfigurationBuilder.java 86487 2014-11-21 15:15:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.builder;

import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration.Visibility;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationAuthorisationStatus;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationType;

public class CommunicationConfigurationBuilder {

  public static final Visibility DEFAULT_VISIBILITY = Visibility.PUBLIC;

  public static final String DEFAULT_CUSTOM_URL = "http://custom.url";

  private boolean isInMasterChannel;

  private Visibility visibility;

  private SyndicationType syndicationType;

  private SyndicationAuthorisationStatus consumerSyndicationAuthorisationStatus;

  private SyndicationAuthorisationStatus masterSyndicationAuthorisationStatus;

  private boolean excludeFromContentPlan = false;

  private String customUrl;

  public CommunicationConfigurationBuilder withDefaultValues() {

    visibility = DEFAULT_VISIBILITY;
    isInMasterChannel = true;
    excludeFromContentPlan = false;
    customUrl = DEFAULT_CUSTOM_URL;

    return this;
  }

  public CommunicationConfigurationBuilder withCustomUrl(final String value) {

    customUrl = value;
    return this;
  }

  public CommunicationConfigurationBuilder withVisibility(final Visibility value) {

    visibility = value;
    return this;
  }

  public CommunicationConfigurationBuilder withInMasterChannel(final boolean value) {

    isInMasterChannel = value;
    return this;
  }

  public CommunicationConfigurationBuilder withSyndicationType(final SyndicationType value) {

    syndicationType = value;
    return this;
  }

  public CommunicationConfigurationBuilder withConsumerChannelSyndicationAuthorisationStatus(
    final SyndicationAuthorisationStatus value) {

    consumerSyndicationAuthorisationStatus = value;
    return this;
  }

  public CommunicationConfigurationBuilder withMasterChannelSyndicationAuthorisationStatus(
    final SyndicationAuthorisationStatus value) {

    masterSyndicationAuthorisationStatus = value;
    return this;
  }

  public CommunicationConfigurationBuilder withExcludeFromContentPlan(final boolean value) {

    excludeFromContentPlan = value;
    return this;
  }

  public CommunicationConfiguration build() {

    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(isInMasterChannel);
    configuration.setVisibility(visibility);
    configuration.setCustomUrl(customUrl);

    configuration.setCommunicationSyndication(getCommunicationSyndication());

    configuration.setExcludeFromChannelContentPlan(excludeFromContentPlan);

    return configuration;
  }

  private CommunicationSyndication getCommunicationSyndication() {
    CommunicationSyndication communicationSyndication = new CommunicationSyndication();
    communicationSyndication.setSyndicationType(syndicationType);
    communicationSyndication.setConsumerChannelSyndicationAuthorisationStatus(consumerSyndicationAuthorisationStatus);
    communicationSyndication.setMasterChannelSyndicationAuthorisationStatus(masterSyndicationAuthorisationStatus);
    return communicationSyndication;
  }
}