/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: DefaultContractPeriodServiceTest.java 101521 2015-10-15 16:45:21Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.domain.validator.ContractPeriodCreateValidator;
import com.brighttalk.service.channel.business.domain.validator.ContractPeriodUpdateValidator;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.ContractPeriodNotFoundException;
import com.brighttalk.service.channel.business.error.InvalidContractPeriodException;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.database.ContractPeriodDbDao;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.brighttalk.service.channel.integration.error.ContractPeriodAlreadyExistsForChannel;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import org.easymock.EasyMock;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Tests for the {@link DefaultContractPeriodService}. Tests do not verify externally sourced business logic from
 * {@link ContractPeriodCreateValidator}, which is maintained separately.
 */
public class DefaultContractPeriodServiceTest {

  //Unit under test
  private ContractPeriodService uut;

  //Dependencies
  private ChannelDbDao mockChannelDbDao;
  private ContractPeriodDbDao mockContractPeriodDbDao;

  //Test objects
  private User user;
  ArrayList<User.Role> userRoles = new ArrayList<>();
  private ContractPeriod contractPeriod;

  @Before
  public void setUp() throws Exception {
    //Setup mocks
    mockContractPeriodDbDao = createMock(ContractPeriodDbDao.class);
    mockChannelDbDao = createMock(ChannelDbDao.class);

    // Setup unit under test
    uut = new DefaultContractPeriodService(mockContractPeriodDbDao, mockChannelDbDao);

    // Setup test objects
    user = new User();
    userRoles.add(User.Role.MANAGER);
    user.setRoles(userRoles);
    contractPeriod = new ContractPeriod();
    contractPeriod.setChannelId(123l);
    contractPeriod.setStart(new Date());
    contractPeriod.setEnd(DateTime.now().plusHours(5).toDate());

    // Setup mock expectations
    expect(mockChannelDbDao.exists(contractPeriod.getChannelId())).andReturn(true);
    expect(mockContractPeriodDbDao.exists(anyLong(), anyLong())).andReturn(true);
    expect(
        mockContractPeriodDbDao.contractPeriodOverlapsExisting(anyObject(ContractPeriod.class))).andReturn(
        false);
  }

  /**
   * Tests {@link ContractPeriodService#create(User, ContractPeriod)} with a {@link User} with the "User" and
   * "Operations" roles.
   * A {@link UserAuthorisationException} should be thrown.
   *
   * @throws Exception
   */
  @Test(expected = UserAuthorisationException.class)
  public void testCreateContractPeriodWithIncorrectRoleShouldFail() throws Exception {
    //Setup roles
    userRoles.clear();
    userRoles.add(User.Role.USER);
    userRoles.add(User.Role.OPERATIONS);

    //Test
    uut.create(user, contractPeriod);
  }

  /**
   * Tests {@link ContractPeriodService#create(User, ContractPeriod)} with a contract period that has an invalid channel
   * id. An exception should be returned.
   *
   * @throws Exception
   */
  @Test(expected = ChannelNotFoundException.class)
  public void testCreateContractPeriodForNonExistentChannelShouldFail() throws Exception {
    //Setup
    reset(mockChannelDbDao);
    expect(mockChannelDbDao.exists(contractPeriod.getChannelId())).andReturn(false);
    replay(mockChannelDbDao);

    //Test
    uut.create(user, contractPeriod);

    //Verify
    verify(mockChannelDbDao);
  }

  /**
   * Tests {@link ContractPeriodService#create(User, ContractPeriod)} to see if it validates a contract period
   *
   * @throws Exception
   */
  @Test
  public void testCreateContractPeriodValidatesContractPeriod() throws Exception {
    //Setup
    ContractPeriod mockContractPeriod = createMock(ContractPeriod.class);
    expect(mockContractPeriodDbDao.create(mockContractPeriod)).andReturn(mockContractPeriod).times(1);
    expect(mockContractPeriod.getChannelId()).andReturn(contractPeriod.getChannelId());

    mockContractPeriod.validate(EasyMock.isA(ContractPeriodCreateValidator.class));
    expectLastCall().times(1);// expect the validate method to be called
    replay(mockChannelDbDao, mockContractPeriodDbDao, mockContractPeriod);

    //Test
    uut.create(user, mockContractPeriod);

    //Verify
    verify(mockContractPeriod);
  }

  /**
   * Tests {@link ContractPeriodService#create(User, ContractPeriod)} to see if it validates a contract period before
   * insertion
   *
   * @throws Exception
   */
  @Test
  public void testCreateContractPeriodDoesNotCreateContractPeriodIfInvalid() throws Exception {
    //Setup
    ContractPeriod mockContractPeriod = createMock(ContractPeriod.class);
    expect(mockContractPeriod.getChannelId()).andReturn(contractPeriod.getChannelId());
    mockContractPeriod.validate(EasyMock.isA(ContractPeriodCreateValidator.class));
    expectLastCall().andThrow(
        new InvalidContractPeriodException("Test error", ErrorCode.INVALID_CONTRACT_PERIOD.getName())).times(1);
    replay(mockChannelDbDao, mockContractPeriodDbDao, mockContractPeriod);

    //Test
    try {
      uut.create(user, mockContractPeriod);
      fail("Expected exception");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }
    //Verify
    verify(mockContractPeriod);
  }

  /**
   * Tests {@link ContractPeriodService#create(User, ContractPeriod)} to see if it creates a valid contract period
   *
   * @throws Exception
   */
  @Test
  public void testCreateContractPeriodCreatesContractPeriodIfValid() throws Exception {
    //Setup
    reset(mockChannelDbDao, mockContractPeriodDbDao);
    expect(mockChannelDbDao.exists(anyLong())).andReturn(true);
    expect(mockContractPeriodDbDao.create(contractPeriod)).andReturn(contractPeriod);
    expect(mockContractPeriodDbDao.contractPeriodOverlapsExisting(contractPeriod)).andReturn(false);
    replay(mockChannelDbDao, mockContractPeriodDbDao);
    contractPeriod.setId(123l);

    //Test
    uut.create(user, this.contractPeriod);

    //Verify
    verify(mockContractPeriodDbDao);
  }

  /**
   * Tests {@link ContractPeriodService#create(User, ContractPeriod)} to see if it throws an exception if a duplicate
   * contract period is created
   *
   * @throws Exception
   */
  @Test
  public void testCreateContractPeriodFailsIfDuplicateContractPeriodSubmitted() throws Exception {
    //Setup
    reset(mockChannelDbDao, mockContractPeriodDbDao);
    expect(mockChannelDbDao.exists(anyLong())).andReturn(true).times(2);
    expect(mockContractPeriodDbDao.create(contractPeriod)).andReturn(contractPeriod);
    ContractPeriodAlreadyExistsForChannel exception = new ContractPeriodAlreadyExistsForChannel("Already exists");
    expect(mockContractPeriodDbDao.create(contractPeriod)).andThrow(exception);
    expect(mockContractPeriodDbDao.contractPeriodOverlapsExisting(contractPeriod)).andReturn(false).times(2);
    replay(mockChannelDbDao, mockContractPeriodDbDao);
    contractPeriod.setId(123l);
    uut.create(user, this.contractPeriod);

    //Test
    try {
      uut.create(user, this.contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }

    //Verify
    verify(mockContractPeriodDbDao);
  }

  /**
   * Tests {@link ContractPeriodService#update(User, ContractPeriod)} with a {@link User} with the "User" and
   * "Operations" roles.
   * A {@link UserAuthorisationException} should be thrown.
   *
   * @throws Exception
   */
  @Test(expected = UserAuthorisationException.class)
  public void testUpdateContractPeriodWithIncorrectRoleShouldFail() throws Exception {
    //Setup roles
    userRoles.clear();
    userRoles.add(User.Role.USER);
    userRoles.add(User.Role.OPERATIONS);

    //Test
    uut.update(user, contractPeriod);
  }

  /**
   * Tests {@link ContractPeriodService#update(User, ContractPeriod)} with a contract period that has an invalid channel
   * id. An exception should be returned.
   *
   * @throws Exception
   */
  @Test(expected = ChannelNotFoundException.class)
  public void testUpdateContractPeriodForNonExistentChannelShouldFail() throws Exception {
    //Setup
    reset(mockChannelDbDao);
    expect(mockChannelDbDao.exists(contractPeriod.getChannelId())).andReturn(false);
    replay(mockChannelDbDao);

    //Test
    uut.update(user, contractPeriod);

    //Verify
    verify(mockChannelDbDao);
  }

  /**
   * Tests {@link ContractPeriodService#update(User, ContractPeriod)} to see if it validates a contract period
   *
   * @throws Exception
   */
  @Test
  public void testUpdateContractPeriodValidatesContractPeriod() throws Exception {
    //Setup
    ContractPeriod mockContractPeriod = createMock(ContractPeriod.class);
    expect(mockContractPeriod.getChannelId()).andReturn(contractPeriod.getChannelId()).anyTimes();
    expect(mockContractPeriod.getId()).andReturn(123l).anyTimes();
    mockContractPeriod.validate(EasyMock.isA(ContractPeriodUpdateValidator.class));
    expectLastCall().times(1);// expect the validate method to be called for the object returned when looking up the
    // existing contract period
    expect(mockContractPeriodDbDao.find(anyLong())).andReturn(mockContractPeriod).times(1);
    expect(mockContractPeriodDbDao.update(mockContractPeriod)).andReturn(mockContractPeriod).times(1);
    replay(mockChannelDbDao, mockContractPeriodDbDao, mockContractPeriod);

    //Test
    uut.update(user, mockContractPeriod);

    //Verify
    verify(mockContractPeriod);
  }

  /**
   * Tests {@link ContractPeriodService#update(User, ContractPeriod)} to see if it validates a contract period before
   * insertion.
   *
   * @throws Exception
   */
  @Test
  public void testUpdateContractPeriodDoesNotCreateContractPeriodIfInvalid() throws Exception {
    //Setup
    ContractPeriod mockContractPeriod = createMock(ContractPeriod.class);
    expect(mockContractPeriod.getChannelId()).andReturn(contractPeriod.getChannelId()).anyTimes();
    expect(mockContractPeriod.getId()).andReturn(456l).anyTimes();
    expect(mockContractPeriodDbDao.find(anyLong())).andReturn(contractPeriod);

    mockContractPeriod.validate(EasyMock.isA(ContractPeriodUpdateValidator.class));
    expectLastCall().andThrow(
        new InvalidContractPeriodException("Test error", ErrorCode.INVALID_CONTRACT_PERIOD.getName())).times(1);
    replay(mockChannelDbDao, mockContractPeriodDbDao, mockContractPeriod);

    //Test
    try {
      uut.update(user, mockContractPeriod);
      fail("Expected exception");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }
    //Verify
    verify(mockContractPeriod);
  }

  /**
   * Tests {@link ContractPeriodService#update(User, ContractPeriod)} to see if it validates a contract period
   *
   * @throws Exception
   */
  @Test
  public void testUpdateContractPeriodCreatesContractPeriodIfValid() throws Exception {
    //Setup
    reset(mockContractPeriodDbDao);
    expect(mockContractPeriodDbDao.exists(anyLong(), anyLong())).andReturn(true).anyTimes();
    expect(mockContractPeriodDbDao.find(anyLong())).andReturn(contractPeriod);
    expect(mockContractPeriodDbDao.update(contractPeriod)).andReturn(contractPeriod);
    expect(mockContractPeriodDbDao.contractPeriodOverlapsExisting(contractPeriod)).andReturn(false);
    replay(mockChannelDbDao, mockContractPeriodDbDao);
    contractPeriod.setId(123l);

    //Test
    contractPeriod.setStart(new DateTime().plusDays(1).toDate());
    contractPeriod.setEnd(new DateTime().plusDays(2).toDate());
    ContractPeriod updatedCP = uut.update(user, contractPeriod);

    //Verify
    verify(mockContractPeriodDbDao);
    assertEquals(contractPeriod.getId(), updatedCP.getId());
    assertEquals(contractPeriod.getChannelId(), updatedCP.getChannelId());
    assertEquals(contractPeriod.getStart(), updatedCP.getStart());
    assertEquals(contractPeriod.getEnd(), updatedCP.getEnd());
  }

  /**
   * Tests {@link ContractPeriodService#update(User, ContractPeriod)} to see if it throws an exception if given a
   * contract period with an invalid ID
   */
  @Test
  public void testUpdateContractPeriodWithInvalidContractPeriodIdShouldFail() throws Exception {
    //Setup
    contractPeriod.setId(123l);
    reset(mockContractPeriodDbDao);
    expect(mockContractPeriodDbDao.find(anyLong())).andThrow(
        new ContractPeriodNotFoundException(contractPeriod.getId()));
    expect(mockContractPeriodDbDao.exists(anyLong(), anyLong())).andReturn(false).anyTimes();
    replay(mockContractPeriodDbDao, mockChannelDbDao);

    //Test
    try {
      uut.update(user, this.contractPeriod);
      fail("Expected an exception for the missing contract period.");
    } catch (ContractPeriodNotFoundException e) {
      assertEquals(ErrorCode.NOT_FOUND.getName(), e.getUserErrorCode());
      assertEquals(contractPeriod.getId(), e.getContractPeriodId());
    }
    //Verify
    verify(mockContractPeriodDbDao);
  }

  /**
   * Tests if {@link ContractPeriodService#delete(User, Long)} throws an exception if an invalid contract period id is
   * given
   *
   * @throws Exception
   */
  @Test
  public void testDeleteContractPeriodWithInvalidContractPeriodIdShouldPass() throws Exception {
    //Setup
    reset(mockContractPeriodDbDao);
    expect(mockContractPeriodDbDao.find(123l)).andThrow(new ContractPeriodNotFoundException(contractPeriod.getId()));
    replay(mockContractPeriodDbDao, mockChannelDbDao);

    //Test
    uut.delete(user, 123l);
    //Verify
    verify(mockContractPeriodDbDao);
  }

  /**
   * Tests {@link ContractPeriodService#delete(User, Long)} with a {@link User} with the "User" and "Operations" roles.
   * A {@link UserAuthorisationException} should be thrown.
   *
   * @throws Exception
   */
  @Test(expected = UserAuthorisationException.class)
  public void testDeleteContractPeriodWithIncorrectRoleShouldFail() throws Exception {
    //Setup roles
    userRoles.clear();
    userRoles.add(User.Role.USER);
    userRoles.add(User.Role.OPERATIONS);

    //Test
    uut.delete(user, 123l);
  }

  /**
   * Tests if {@link ContractPeriodService#delete(User, Long)} tries to delete a valid contract period
   *
   * @throws Exception
   */
  @Test
  public void testDeleteContractPeriodWithValidContractPeriodId() throws Exception {
    //Setup
    contractPeriod.setStart(DateTime.now().plusWeeks(1).toDate());
    contractPeriod.setEnd(DateTime.now().plusWeeks(2).toDate());
    reset(mockContractPeriodDbDao);
    expect(mockContractPeriodDbDao.find(123l)).andReturn(contractPeriod);
    mockContractPeriodDbDao.delete(isA(Long.class));
    expectLastCall().times(1);
    replay(mockContractPeriodDbDao, mockChannelDbDao);

    //Test
    uut.delete(user, 123l);

    //Verify
    verify(mockContractPeriodDbDao);
  }

  /**
   * Tests if {@link ContractPeriodService#delete(User, Long)} tries to delete a contract period that isnt eligible for
   * deletion due to it being current
   *
   * @throws Exception
   */
  @Test
  public void testDeleteContractPeriodThatIsIneligibleForDeletion() throws Exception {
    //Setup
    reset(mockContractPeriodDbDao);
    expect(mockContractPeriodDbDao.find(123l)).andReturn(contractPeriod);
    replay(mockContractPeriodDbDao, mockChannelDbDao);

    //Test
    try {
      uut.delete(user, 123l);
      fail("Expected InvalidContractPeriodException.");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.CONTRACT_PERIOD_INELIGIBLE_FOR_DELETION.getName(), e.getUserErrorCode());
    }

    //Verify
    verify(mockContractPeriodDbDao);
  }

  /**
   * Tests if {@link ContractPeriodService#find(User, Long)} throws an exception if an invalid contract period id is
   * given
   *
   * @throws Exception
   */
  @Test
  public void testFindContractPeriodWithInvalidContractPeriodIdShouldFail() throws Exception {
    //Setup
    reset(mockContractPeriodDbDao);
    expect(mockContractPeriodDbDao.find(123l)).andThrow(new ContractPeriodNotFoundException(contractPeriod.getId()));
    replay(mockContractPeriodDbDao, mockChannelDbDao);

    //Test
    try {
      uut.find(user, 123l);
      fail("Expected exception for invalid contract period id");
    } catch (ContractPeriodNotFoundException e) {

    }
    //Verify
    verify(mockContractPeriodDbDao);
  }

  /**
   * Tests {@link ContractPeriodService#find(User, Long)} with a {@link User} with the "User" and "Operations" role.
   * A {@link UserAuthorisationException} should be thrown.
   *
   * @throws Exception
   */
  @Test(expected = UserAuthorisationException.class)
  public void testFindContractPeriodWithIncorrectRoleShouldFail() throws Exception {
    //Setup roles
    userRoles.clear();
    userRoles.add(User.Role.USER);
    userRoles.add(User.Role.OPERATIONS);

    //Test
    uut.find(user, 123l);
  }

  /**
   * Tests if {@link ContractPeriodService#find(User, Long)} tries to delete a valid contract period
   *
   * @throws Exception
   */
  @Test
  public void testFindContractPeriodWithValidContractPeriodId() throws Exception {
    //Setup
    reset(mockContractPeriodDbDao);
    expect(mockContractPeriodDbDao.find(123l)).andReturn(contractPeriod);
    replay(mockContractPeriodDbDao, mockChannelDbDao);

    //Test
    uut.find(user, 123l);

    //Verify
    verify(mockContractPeriodDbDao);
  }

  /**
   * Tests if {@link ContractPeriodService#findByChannelId(User, Long)} throws an exception if an invalid channel id is
   * given
   *
   * @throws Exception
   */
  @Test
  public void testFindChannelContractPeriodsWithInvalidChannelIdShouldFail() throws Exception {
    //Setup
    reset(mockChannelDbDao, mockContractPeriodDbDao);
    expect(mockChannelDbDao.exists(123l)).andReturn(false);
    replay(mockContractPeriodDbDao, mockChannelDbDao);

    //Test
    try {
      uut.findByChannelId(user, 123l);
      fail("Expected exception for invalid channel id");
    } catch (ChannelNotFoundException e) {
    }
    //Verify
    verify(mockContractPeriodDbDao);
  }

  /**
   * Tests {@link ContractPeriodService#findByChannelId(User, Long)} with a {@link User} with the "User" and "Operations"
   * role.
   * A {@link UserAuthorisationException} should be thrown.
   *
   * @throws Exception
   */
  @Test(expected = UserAuthorisationException.class)
  public void testFindChannelContractPeriodWithIncorrectRoleShouldFail() throws Exception {
    //Setup roles
    userRoles.clear();
    userRoles.add(User.Role.USER);
    userRoles.add(User.Role.OPERATIONS);

    //Test
    uut.findByChannelId(user, 123l);
  }

  /**
   * Tests if {@link ContractPeriodService#findByChannelId(User, Long)} tries to delete a valid contract period
   *
   * @throws Exception
   */
  @Test
  public void testFindChannelContractPeriodWithValidChannelId() throws Exception {
    //Setup
    reset(mockContractPeriodDbDao);
    ArrayList<ContractPeriod> contractPeriods = new ArrayList<>();
    contractPeriods.add(contractPeriod);

    expect(mockContractPeriodDbDao.findAllByChannelId(123l)).andReturn(contractPeriods);
    replay(mockContractPeriodDbDao, mockChannelDbDao);

    //Test
    List<ContractPeriod> byChannel = uut.findByChannelId(user, 123l);

    //Verify
    verify(mockContractPeriodDbDao);
    assertEquals(contractPeriods.size(), byChannel.size());
  }

}