/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationStatisticsBuilder.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.builder;

import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;

/**
 * Utility test class to build a {@link CommunicationStatistics} domain object.
 */
public class CommunicationStatisticsBuilder {

  public final static int DEFAULT_NUMBER_OF_VIEWINGS = 4;
  public final static long DEFAULT_TOTAL_VIEWING_DURATION = 1000l;
  public final static int DEFAULT_NUMBER_OF_RATINGS = 2;
  public final static float DEFAULT_AVERAGE_RATING = 3.5f;
  public final static int DEFAULT_TOTAL_REGISTRATIONS = 10;

  private Long communicationId;

  private int numberOfViewings;

  private long totalViewingDuration;

  private int numberOfRatings;

  private float averageRating;

  private int totalRegistration;

  public CommunicationStatisticsBuilder withDefaultValues() {

    numberOfViewings = DEFAULT_NUMBER_OF_VIEWINGS;
    totalViewingDuration = DEFAULT_TOTAL_VIEWING_DURATION;
    numberOfRatings = DEFAULT_NUMBER_OF_RATINGS;
    averageRating = DEFAULT_AVERAGE_RATING;
    totalRegistration = DEFAULT_TOTAL_REGISTRATIONS;

    return this;
  }

  public CommunicationStatisticsBuilder withCommunicationId(Long value) {
    communicationId = value;
    return this;
  }

  public CommunicationStatisticsBuilder withNumberOfViewings(int value) {
    numberOfViewings = value;
    return this;
  }

  public CommunicationStatisticsBuilder withTotalViewingDuration(long value) {
    totalViewingDuration = value;
    return this;
  }

  public CommunicationStatisticsBuilder withTotalRegistration(int value) {
    totalRegistration = value;
    return this;
  }

  public CommunicationStatisticsBuilder withAverageRating(float value) {
    averageRating = value;
    return this;
  }

  public CommunicationStatisticsBuilder withNumberOfRating(int value) {
    numberOfRatings = value;
    return this;
  }

  public CommunicationStatistics build() {

    CommunicationStatistics commStatistics = new CommunicationStatistics(communicationId);
    commStatistics.setNumberOfViewings(numberOfViewings);
    commStatistics.setTotalViewingDuration(totalViewingDuration);
    commStatistics.setNumberOfRatings(numberOfRatings);
    commStatistics.setAverageRating(averageRating);
    commStatistics.setTotalRegistration(totalRegistration);

    return commStatistics;
  }
}