/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationPortalUrlGeneratorTest.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationPortalUrlGenerator;

public class CommunicationPortalUrlGeneratorTest {

  private static final String DEFAULT_URL_TEMPLATE = "http://www.local.brighttalk.net/channel/{channelId}/communication/{communication}/url";

  private static final Long CHANNEL_ID = 666l;

  private static final Long COMMUNICATION_ID = 11l;

  private CommunicationPortalUrlGenerator uut;

  @Before
  public void setUp() {

    uut = new CommunicationPortalUrlGenerator();
    uut.setCommunicationPortalUriTemplate(DEFAULT_URL_TEMPLATE);
  }

  @Test
  public void generateDefaultUrl() {

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);

    List<Feature> features = new ArrayList<Feature>();
    features.add(new Feature(Type.CUSTOM_URL));
    features.add(new Feature(Type.ENABLE_COMMUNICATION_CUSTOM_URL));
    features.add(new Feature(Type.CUSTOM_COMM_URL));
    channel.setFeatures(features);

    Communication communication = new Communication();
    communication.setId(COMMUNICATION_ID);

    final String channelTag = "{channelId}";
    final String communicationTag = "{communicationId}";

    // do test
    String result = uut.generate(channel, communication);

    final String expectedResult = DEFAULT_URL_TEMPLATE.
                                  replace(channelTag, CHANNEL_ID.toString()).
                                  replace(communicationTag, COMMUNICATION_ID.toString());
    assertEquals(expectedResult, result);
  }

  @Test
  public void generateDefaultUrlWhenCustomEnabledWithoutValue() {

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);

    List<Feature> features = new ArrayList<Feature>();
    features.add(new Feature(Type.ENABLE_COMMUNICATION_CUSTOM_URL));
    features.add(new Feature(Type.CUSTOM_COMM_URL));

    Feature customChannelUrlFeature = new Feature(Type.CUSTOM_URL);
    customChannelUrlFeature.setIsEnabled(true);
    features.add(customChannelUrlFeature);

    channel.setFeatures(features);

    Communication communication = new Communication();
    communication.setId(COMMUNICATION_ID);
  }

  @Test
  public void generateDefaultUrlWhenCustomEnabledWithNoUrlFlag() {

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);

    List<Feature> features = new ArrayList<Feature>();
    features.add(new Feature(Type.ENABLE_COMMUNICATION_CUSTOM_URL));
    features.add(new Feature(Type.CUSTOM_COMM_URL));

    Feature customChannelUrlFeature = new Feature(Type.CUSTOM_URL);
    customChannelUrlFeature.setIsEnabled(Boolean.TRUE);
    customChannelUrlFeature.setValue(CommunicationPortalUrlGenerator.CHANNEL_URL_EMPTY_VALUE);
    features.add(customChannelUrlFeature);

    channel.setFeatures(features);

    Communication communication = new Communication();
    communication.setId(COMMUNICATION_ID);
  }

  @Test
  public void generateCustomChannelUrl() {

    final String expectedCustomUrl = "http://www.local.brighttalk.net/channelGlobalCustomUrl";

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);

    List<Feature> features = new ArrayList<Feature>();
    features.add(new Feature(Type.ENABLE_COMMUNICATION_CUSTOM_URL));
    features.add(new Feature(Type.CUSTOM_COMM_URL));

    Feature customChannelUrlFeature = new Feature(Type.CUSTOM_URL);
    customChannelUrlFeature.setIsEnabled(Boolean.TRUE);
    customChannelUrlFeature.setValue(expectedCustomUrl);
    features.add(customChannelUrlFeature);

    channel.setFeatures(features);

    Communication communication = new Communication();

    // do test
    final String result = uut.generate(channel, communication);

    assertEquals(expectedCustomUrl, result);
  }

  @Test
  public void generateCustomChannelUrlWhenCommunicationUrlEnabledWithNoValue() {

    final String expectedCustomUrl = "http://www.local.brighttalk.net/channelGlobalCustomUrl";

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);

    List<Feature> features = new ArrayList<Feature>();
    features.add(new Feature(Type.ENABLE_COMMUNICATION_CUSTOM_URL));

    Feature customCommunicationUrlFeature = new Feature(Type.CUSTOM_COMM_URL);
    customCommunicationUrlFeature.setIsEnabled(Boolean.TRUE);
    features.add(customCommunicationUrlFeature);

    Feature customChannelUrlFeature = new Feature(Type.CUSTOM_URL);
    customChannelUrlFeature.setIsEnabled(Boolean.TRUE);
    customChannelUrlFeature.setValue(expectedCustomUrl);
    features.add(customChannelUrlFeature);

    channel.setFeatures(features);

    Communication communication = new Communication();

    // do test
    final String result = uut.generate(channel, communication);

    assertEquals(expectedCustomUrl, result);
  }

  @Test
  public void generateChannelLevelCustomCommunicationUrl() {

    final String commIdTag = "${commId}";
    final String customCommunicationUrlTemplate = "http://www.local.brighttalk.net/customCommunicationUrl/" + commIdTag;

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);

    List<Feature> features = new ArrayList<Feature>();
    features.add(new Feature(Type.ENABLE_COMMUNICATION_CUSTOM_URL));

    Feature customCommunicationUrlFeature = new Feature(Type.CUSTOM_COMM_URL);
    customCommunicationUrlFeature.setIsEnabled(Boolean.TRUE);
    customCommunicationUrlFeature.setValue(customCommunicationUrlTemplate);
    features.add(customCommunicationUrlFeature);

    Feature customChannelUrlFeature = new Feature(Type.CUSTOM_URL);
    customChannelUrlFeature.setIsEnabled(Boolean.TRUE);
    customChannelUrlFeature.setValue("someValue");
    features.add(customChannelUrlFeature);

    channel.setFeatures(features);

    Communication communication = new Communication();
    communication.setId(COMMUNICATION_ID);

    // do test
    final String result = uut.generate(channel, communication);

    final String expectedCustomUrl = customCommunicationUrlTemplate.replace(commIdTag, COMMUNICATION_ID.toString());
    assertEquals(expectedCustomUrl, result);
  }

  @Test
  public void generateChannelLevelCustomCommunicationUrlWhenNoCustomConfigurationUrlSet() {

    final String commIdTag = "${commId}";
    final String customCommunicationUrlTemplate = "http://www.local.brighttalk.net/customCommunicationUrl/" + commIdTag;

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);

    List<Feature> features = new ArrayList<Feature>();

    Feature customCommunicationUrlFeautre = new Feature(Type.ENABLE_COMMUNICATION_CUSTOM_URL);
    customCommunicationUrlFeautre.setIsEnabled(Boolean.TRUE);
    features.add(customCommunicationUrlFeautre);

    Feature customCommunicationUrlFeature = new Feature(Type.CUSTOM_COMM_URL);
    customCommunicationUrlFeature.setIsEnabled(Boolean.TRUE);
    customCommunicationUrlFeature.setValue(customCommunicationUrlTemplate);
    features.add(customCommunicationUrlFeature);

    channel.setFeatures(features);

    Communication communication = new Communication();
    communication.setId(COMMUNICATION_ID);

    CommunicationConfiguration configuration = new CommunicationConfiguration();
    communication.setConfiguration(configuration);

    // do test
    final String result = uut.generate(channel, communication);

    final String expectedCustomUrl = customCommunicationUrlTemplate.replace(commIdTag, COMMUNICATION_ID.toString());
    assertEquals(expectedCustomUrl, result);
  }

  @Test
  public void generateCommunicationLevelCustomCommunicationUrl() {

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);

    List<Feature> features = new ArrayList<Feature>();

    Feature customCommunicationUrlFeautre = new Feature(Type.ENABLE_COMMUNICATION_CUSTOM_URL);
    customCommunicationUrlFeautre.setIsEnabled(Boolean.TRUE);
    features.add(customCommunicationUrlFeautre);

    channel.setFeatures(features);

    Communication communication = new Communication();
    communication.setId(COMMUNICATION_ID);

    final String customCommunicationUrl = "http://www.local.brighttalk.net/customCommunicationUrl/";
    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setCustomUrl(customCommunicationUrl);
    communication.setConfiguration(configuration);

    // do test
    final String result = uut.generate(channel, communication);

    final String expectedCustomUrl = customCommunicationUrl;
    assertEquals(expectedCustomUrl, result);
  }

}