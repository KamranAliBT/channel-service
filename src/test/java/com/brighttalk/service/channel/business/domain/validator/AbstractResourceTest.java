/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AbstractResourceTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Resource;

/**
 * Abstract Resource test provides functionality to create various types of resources.
 */
public class AbstractResourceTest {

  protected static final Long RESOURCE_ID = 5L;

  protected static final Long RESOURCE_ID_2 = 6L;

  protected static final String RESOURCE_URL = "http://test";

  protected static final String RESOURCE_URL_2 = "http://test2";

  protected static final String RESOURCE_TITLE = "Resource Test Title";

  protected static final String RESOURCE_TITLE_2 = "Resource Test Title 2";

  protected static final String RESOURCE_DESCRIPTION = "Resource Test Description";

  protected static final String RESOURCE_DESCRIPTION_2 = "Resource Test Description 2";

  protected static final String RESOURCE_MIME_TYPE = "application/xml";

  protected static final Long RESOURCE_FILE_SIZE = 50L;

  protected Resource createExternalFileResource(final String title, final String url, final String description,
    final String mimeType, final Long size) {

    Resource resource = new Resource();
    resource.setType(Resource.Type.FILE);
    resource.setHosted(Resource.Hosted.EXTERNAL);
    resource.setTitle(title);
    resource.setUrl(url);
    resource.setDescription(description);
    resource.setSize(size);
    resource.setMimeType(mimeType);

    return resource;
  }

  protected Resource createInternalFileResource(final String title, final String description) {

    Resource resource = new Resource();
    resource.setType(Resource.Type.FILE);
    resource.setHosted(Resource.Hosted.INTERNAL);
    resource.setTitle(title);
    resource.setDescription(description);

    return resource;
  }

  protected Resource createLinkResource(final String title, final String url, final String description) {
    Resource resource = new Resource();
    resource.setType(Resource.Type.LINK);
    resource.setTitle(title);
    resource.setDescription(description);
    resource.setUrl(url);

    return resource;
  }

  protected Communication createCommunicationConsumerChannel() {

    return createCommunication(false);
  }

  protected Communication createCommunicationMasterChannel() {

    return createCommunication(true);
  }

  private Communication createCommunication(final boolean isMaster) {

    Communication communication = new Communication();
    communication.setProvider(new Provider(Provider.BRIGHTTALK));
    Date date = new Date();
    date.setTime(date.getTime() - 10 * 60000);
    communication.setScheduledDateTime(date);
    communication.setStatus(Status.RECORDED);
    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(isMaster);
    communication.setConfiguration(configuration);

    List<Resource> resources = new ArrayList<Resource>();
    Resource resource = new Resource(RESOURCE_ID);
    resource.setPrecedence(0);
    Resource resource2 = new Resource(RESOURCE_ID_2);
    resource.setPrecedence(1);
    resources.add(resource);
    resources.add(resource2);
    communication.setResources(resources);

    return communication;
  }

}
