/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationServiceTest.java 72595 2014-01-15 13:37:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.easymock.EasyMock.createMock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.utils.JobIdGenerator;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.ScheduledPublicationDbDao;

/**
 * Unit tests for {@link CronScheduledPublicationUpdateService}.
 */
public class CronScheduledPublicationUpdateServiceTest extends AbstractCommunicationServiceTest {

  private CronScheduledPublicationUpdateService uut;

  @Before
  public void setUp() {

    uut = new CronScheduledPublicationUpdateService();

    mockCommunicationDao = createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDao);

    mockScheduledPublicationDbDao = EasyMock.createMock(ScheduledPublicationDbDao.class);
    ReflectionTestUtils.setField(uut, "scheduledPublicationDbDao", mockScheduledPublicationDbDao);

    mockIdGenerator = EasyMock.createMock(JobIdGenerator.class);
    ReflectionTestUtils.setField(uut, "jobIdGenerator", mockIdGenerator);

  }

  @Test
  public void performScheduledPublicationStatusUpdate() {

    final String jobId = "testJobId";
    List<Long> publishCommunicationIds = new ArrayList<>();
    publishCommunicationIds.add(COMMUNICATION_ID);

    List<Long> unpublishCommunicationIds = new ArrayList<>();
    unpublishCommunicationIds.add(COMMUNICATION_ID);

    final Map<PublishStatus, List<Long>> updateCommunicationsMap = new HashMap<Communication.PublishStatus, List<Long>>();
    updateCommunicationsMap.put(PublishStatus.PUBLISHED, publishCommunicationIds);
    updateCommunicationsMap.put(PublishStatus.UNPUBLISHED, unpublishCommunicationIds);

    EasyMock.expect(mockIdGenerator.generateId()).andReturn(jobId);
    EasyMock.expect(mockScheduledPublicationDbDao.findScheduledCommunications(jobId)).andReturn(updateCommunicationsMap);
    mockCommunicationDao.updatePublishStatus(publishCommunicationIds, PublishStatus.PUBLISHED);
    EasyMock.expectLastCall();

    mockCommunicationDao.updatePublishStatus(unpublishCommunicationIds, PublishStatus.UNPUBLISHED);
    EasyMock.expectLastCall();

    mockScheduledPublicationDbDao.markAsCompleted(jobId);
    EasyMock.expectLastCall();

    EasyMock.replay(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);

    // do test
    uut.performScheduledPublicationStatusUpdate();

    EasyMock.verify(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);
  }

  @Test
  public void performScheduledPublicationStatusUpdateWhenNoCommunicationsFound() {

    final String jobId = "testJobId";
    final Map<PublishStatus, List<Long>> updateCommunicationsMap = new HashMap<Communication.PublishStatus, List<Long>>();
    updateCommunicationsMap.put(PublishStatus.PUBLISHED, null);
    updateCommunicationsMap.put(PublishStatus.UNPUBLISHED, null);

    EasyMock.expect(mockIdGenerator.generateId()).andReturn(jobId);
    EasyMock.expect(mockScheduledPublicationDbDao.findScheduledCommunications(jobId)).andReturn(updateCommunicationsMap);

    EasyMock.replay(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);

    // do test
    uut.performScheduledPublicationStatusUpdate();

    EasyMock.verify(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);
  }

  @Test
  public void performScheduledPublicationStatusUpdateWhenOnlyPublishedCommunicationsFound() {

    final String jobId = "testJobId";
    final List<Long> publishedComms = Arrays.asList(1l);

    final Map<PublishStatus, List<Long>> updateCommunicationsMap = new HashMap<Communication.PublishStatus, List<Long>>();
    updateCommunicationsMap.put(PublishStatus.PUBLISHED, publishedComms);
    updateCommunicationsMap.put(PublishStatus.UNPUBLISHED, new ArrayList<Long>());

    EasyMock.expect(mockIdGenerator.generateId()).andReturn(jobId);
    EasyMock.expect(mockScheduledPublicationDbDao.findScheduledCommunications(jobId)).andReturn(updateCommunicationsMap);

    mockScheduledPublicationDbDao.markAsCompleted(jobId);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.updatePublishStatus(publishedComms, PublishStatus.PUBLISHED);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);

    // do test
    uut.performScheduledPublicationStatusUpdate();

    EasyMock.verify(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);
  }

  @Test
  public void performScheduledPublicationStatusUpdateWhenOnlyUnpublishedCommunicationsFound() {

    final String jobId = "testJobId";
    final List<Long> unpublishedComms = Arrays.asList(2l);

    final Map<PublishStatus, List<Long>> updateCommunicationsMap = new HashMap<Communication.PublishStatus, List<Long>>();
    updateCommunicationsMap.put(PublishStatus.PUBLISHED, new ArrayList<Long>());
    updateCommunicationsMap.put(PublishStatus.UNPUBLISHED, unpublishedComms);

    EasyMock.expect(mockIdGenerator.generateId()).andReturn(jobId);
    EasyMock.expect(mockScheduledPublicationDbDao.findScheduledCommunications(jobId)).andReturn(updateCommunicationsMap);

    mockScheduledPublicationDbDao.markAsCompleted(jobId);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.updatePublishStatus(unpublishedComms, PublishStatus.UNPUBLISHED);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);

    // do test
    uut.performScheduledPublicationStatusUpdate();

    EasyMock.verify(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);
  }

  @Test
  public void performScheduledPublicationStatusUpdateWhenBothTypesOfCommunicationsFound() {

    final String jobId = "testJobId";
    final List<Long> publishedComms = Arrays.asList(1l);
    final List<Long> unpublishedComms = Arrays.asList(2l);

    final Map<PublishStatus, List<Long>> updateCommunicationsMap = new HashMap<Communication.PublishStatus, List<Long>>();
    updateCommunicationsMap.put(PublishStatus.PUBLISHED, publishedComms);
    updateCommunicationsMap.put(PublishStatus.UNPUBLISHED, unpublishedComms);

    EasyMock.expect(mockIdGenerator.generateId()).andReturn(jobId);
    EasyMock.expect(mockScheduledPublicationDbDao.findScheduledCommunications(jobId)).andReturn(updateCommunicationsMap);

    mockScheduledPublicationDbDao.markAsCompleted(jobId);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.updatePublishStatus(publishedComms, PublishStatus.PUBLISHED);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.updatePublishStatus(unpublishedComms, PublishStatus.UNPUBLISHED);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);

    // do test
    uut.performScheduledPublicationStatusUpdate();

    EasyMock.verify(mockIdGenerator, mockScheduledPublicationDbDao, mockCommunicationDao);
  }
}