/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationFinderTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.easymock.EasyMock.createMock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.Session;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationPortalUrlGenerator;
import com.brighttalk.service.channel.business.domain.communication.CommunicationScheduledPublication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationUrlGenerator;
import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.error.DialledInTooEarlyException;
import com.brighttalk.service.channel.business.error.DialledInTooLateException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationCategoryDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationOverrideDbDao;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;
import com.brighttalk.service.channel.integration.database.ScheduledPublicationDbDao;
import com.brighttalk.service.channel.integration.database.syndication.SyndicationDbDao;

/**
 * Unit tests for {@link DefaultCommunicationFinder}.
 */
public class DefaultCommunicationFinderTest {

  private static final Long CHANNEL_ID = 1L;
  private static final Long COMMUNICATION_ID = 6L;
  private static final Long USER_ID = 999L;
  private static final String MOCK_CONFERENCE_ROOM_URL = "conferenceRoomUrlTest/{communicationId}";
  private static final int DEFAULT_PAGE_SIZE = 10;
  private static final int DEFAULT_MAX_PAGE_SIZE = 50;
  private static final int DEFAULT_PAGE_NUMBER = 2;

  private DefaultCommunicationFinder uut;

  private CommunicationDbDao mockCommunicationDbDao;
  private CommunicationOverrideDbDao mockCommunicationOverrideDbDao;
  private ResourceDbDao mockResourceDbDao;
  private CategoryDbDao mockCategoryDbDao;
  private ChannelFinder mockChannelFinder;
  private CommunicationUrlGenerator mockCalendarUrlGenerator;
  private CommunicationUrlGenerator mockCommunicationUrlGenerator;
  private CommunicationPortalUrlGenerator mockCommunicationPortalUrlGenerator;
  private CommunicationCategoryDbDao mockCommunicationCategoryDbDao;
  private ScheduledPublicationDbDao mockScheduledPublicationDbDao;
  private SyndicationDbDao mockSyndicationDbDao;

  @Before
  public void setUp() {
    uut = new DefaultCommunicationFinder();

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockCommunicationDbDao = EasyMock.createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDbDao);

    mockCommunicationOverrideDbDao = EasyMock.createMock(CommunicationOverrideDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationOverrideDbDao", mockCommunicationOverrideDbDao);

    mockResourceDbDao = EasyMock.createMock(ResourceDbDao.class);
    ReflectionTestUtils.setField(uut, "resourceDbDao", mockResourceDbDao);

    mockCategoryDbDao = createMock(CategoryDbDao.class);
    ReflectionTestUtils.setField(uut, "categoryDbDao", mockCategoryDbDao);

    mockCalendarUrlGenerator = createMock(CommunicationUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "calendarUrlGenerator", mockCalendarUrlGenerator);

    mockCommunicationUrlGenerator = createMock(CommunicationUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "communicationUrlGenerator", mockCommunicationUrlGenerator);

    mockCommunicationPortalUrlGenerator = createMock(CommunicationPortalUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "communicationPortalUrlGenerator", mockCommunicationPortalUrlGenerator);

    mockCommunicationCategoryDbDao = createMock(CommunicationCategoryDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationCategoryDbDao", mockCommunicationCategoryDbDao);

    mockScheduledPublicationDbDao = createMock(ScheduledPublicationDbDao.class);
    ReflectionTestUtils.setField(uut, "scheduledPublicationDbDao", mockScheduledPublicationDbDao);

    mockSyndicationDbDao = createMock(SyndicationDbDao.class);
    ReflectionTestUtils.setField(uut, "syndicationDbDao", mockSyndicationDbDao);

    ReflectionTestUtils.setField(uut, "defaultPageSize", DEFAULT_PAGE_SIZE);
    ReflectionTestUtils.setField(uut, "maxPageSize", DEFAULT_MAX_PAGE_SIZE);
    ReflectionTestUtils.setField(uut, "defaultPageNumber", DEFAULT_PAGE_NUMBER);

  }

  @Test
  public void findCommunicationById() {
    // setup
    Provider provider = new Provider(Provider.VIDEO);
    Communication communication =
        new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(CHANNEL_ID).withProvider(
            provider).build();
    Channel channel = null;
    List<Session> presenters = new ArrayList<Session>();
    List<CommunicationStatistics> viewingStatistics = new ArrayList<CommunicationStatistics>();
    List<Resource> resources = new ArrayList<Resource>();
    List<Rendition> renditions = new ArrayList<Rendition>();
    HashMap<Long, CommunicationCategory> categories = new HashMap<Long, CommunicationCategory>();
    List<CommunicationSyndication> communicationSyndications = new ArrayList<CommunicationSyndication>();

    // expectations
    EasyMock.expect(mockCommunicationDbDao.find(EasyMock.anyLong())).andReturn(communication);
    EasyMock.expect(mockCommunicationDbDao.findPresenters(communication)).andReturn(presenters);
    EasyMock.expect(mockCommunicationDbDao.findCommunicationStatistics(communication)).andReturn(viewingStatistics);
    EasyMock.expect(mockResourceDbDao.findAll(EasyMock.anyLong())).andReturn(resources);
    EasyMock.expect(mockCommunicationDbDao.findRenditionAssets(EasyMock.anyLong())).andReturn(renditions);
    EasyMock.expect(mockCommunicationCategoryDbDao.find(communication.getChannelId(), communication.getId())).andReturn(
        categories);
    EasyMock.expect(mockScheduledPublicationDbDao.find(communication.getId())).andReturn(
        new CommunicationScheduledPublication());
    EasyMock.expect(mockSyndicationDbDao.findConsumerChannelsSyndication(communication.getId())).andReturn(
        communicationSyndications);
    mockCommunicationOverrideDbDao.setCommunicationDetailsOverride(communication);
    EasyMock.expectLastCall().once();
    EasyMock.expect(mockCommunicationPortalUrlGenerator.generate(channel, communication)).andReturn(
        "communication portal url");

    EasyMock.expect(mockCommunicationPortalUrlGenerator.getDefaultPortalChannelUrl(channel, communication)).andReturn(
        "communication portal url");

    EasyMock.replay(mockCommunicationDbDao, mockResourceDbDao, mockCommunicationCategoryDbDao,
        mockScheduledPublicationDbDao, mockSyndicationDbDao, mockCommunicationOverrideDbDao,
        mockCommunicationPortalUrlGenerator);
    // expectations

    // do test
    Communication result = uut.find(COMMUNICATION_ID);

    // assertions
    assertEquals(communication, result);
    EasyMock.verify(mockCommunicationDbDao, mockResourceDbDao, mockCommunicationCategoryDbDao,
        mockScheduledPublicationDbDao, mockSyndicationDbDao, mockCommunicationOverrideDbDao,
        mockCommunicationPortalUrlGenerator);
  }

  @Test(expected = NotFoundException.class)
  public void findCommunicationByIdThrowsNotFoundException() {
    // setup

    // expectations
    EasyMock.expect(mockCommunicationDbDao.find(EasyMock.anyLong())).andThrow(new NotFoundException("test"));
    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.find(COMMUNICATION_ID);

    // assertions
    EasyMock.verify(mockCommunicationDbDao);
  }

  @Test
  public void findCommunicationByChannelAndCommunicationId() {
    // setup
    Provider provider = new Provider(Provider.BRIGHTTALK);
    Channel channel = new Channel(CHANNEL_ID);
    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).withProvider(provider).build();
    List<Resource> resources = new ArrayList<Resource>();
    List<Session> presenters = new ArrayList<Session>();
    List<CommunicationStatistics> viewingStatistics = new ArrayList<CommunicationStatistics>();
    HashMap<Long, CommunicationCategory> categories = new HashMap<Long, CommunicationCategory>();

    // expectations
    EasyMock.expect(mockCommunicationDbDao.find(channel.getId(), COMMUNICATION_ID)).andReturn(communication);
    EasyMock.expect(mockCommunicationDbDao.findPresenters(communication)).andReturn(presenters);
    EasyMock.expect(mockCommunicationDbDao.findCommunicationStatistics(communication)).andReturn(viewingStatistics);
    EasyMock.expect(mockResourceDbDao.findAll(COMMUNICATION_ID)).andReturn(resources);
    EasyMock.expect(mockCommunicationCategoryDbDao.find(communication.getChannelId(), communication.getId())).andReturn(
        categories);
    EasyMock.expect(mockScheduledPublicationDbDao.find(communication.getId())).andReturn(
        new CommunicationScheduledPublication());
    EasyMock.expect(mockCommunicationPortalUrlGenerator.generate(channel, communication)).andReturn("communicationUrl");
    EasyMock.expect(mockCommunicationPortalUrlGenerator.getDefaultPortalChannelUrl(channel, communication)).andReturn(
        "communication portal url");

    EasyMock.replay(mockCommunicationDbDao, mockResourceDbDao, mockCommunicationCategoryDbDao,
        mockScheduledPublicationDbDao, mockCommunicationPortalUrlGenerator);

    // do test
    Communication result = uut.find(channel, COMMUNICATION_ID);

    // assertions
    assertNotNull(result);
    assertEquals(COMMUNICATION_ID, result.getId());
    EasyMock.verify(mockCommunicationDbDao, mockResourceDbDao, mockCommunicationCategoryDbDao,
        mockScheduledPublicationDbDao, mockCommunicationPortalUrlGenerator);
  }

  @Test(expected = NotFoundException.class)
  public void testFindCommunicationByCommunicationIdAndChannelIdWithNotExistingCommunication() {
    // setup
    Channel channel = new Channel(CHANNEL_ID);

    // expectations
    EasyMock.expect(mockCommunicationDbDao.find(EasyMock.anyLong(), EasyMock.anyLong())).andThrow(
        new NotFoundException("test"));
    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.find(channel, COMMUNICATION_ID);
  }

  @Test
  public void findCommunicationByPinAndProviderReturnsConferenceRoomUrl() {
    // setup
    ReflectionTestUtils.setField(uut, "conferenceRoomUrl", MOCK_CONFERENCE_ROOM_URL);

    String pin = "012345678";
    String provider = "brighttalkhd";
    Date scheduledDateTime = getScheduledTime(10);
    Integer duration = 120;

    Communication communication = new Communication(COMMUNICATION_ID);
    communication.setScheduledDateTime(scheduledDateTime);
    communication.setDuration(duration);
    communication.setStatus(Status.UPCOMING);
    String expectedConferenceRoomUrl = MOCK_CONFERENCE_ROOM_URL.replace("{communicationId}",
        communication.getId().toString());

    // expectations
    EasyMock.expect(mockCommunicationDbDao.find(pin, provider)).andReturn(communication);
    EasyMock.replay(mockCommunicationDbDao);

    // do test
    String result = uut.find(pin, provider);

    // assertions
    assertNotNull(result);
    assertEquals(expectedConferenceRoomUrl, result);
    EasyMock.verify(mockCommunicationDbDao);
  }

  @Test(expected = DialledInTooEarlyException.class)
  public void findCommunicationByPinAndProviderThrowsTooEarly() {
    // setup
    ReflectionTestUtils.setField(uut, "conferenceRoomUrl", MOCK_CONFERENCE_ROOM_URL);

    String pin = "012345678";
    String provider = "brighttalkhd";

    Date scheduledDateTime = getScheduledTime(1200);
    Integer duration = 120;

    Communication communication = new Communication(COMMUNICATION_ID);
    communication.setPinNumber(pin);
    communication.setScheduledDateTime(scheduledDateTime);
    communication.setDuration(duration);
    String expectedConferenceRoomUrl = MOCK_CONFERENCE_ROOM_URL.replace("{communicationId}",
        communication.getId().toString());
    communication.setConferenceRoomUrl(expectedConferenceRoomUrl);
    communication.setStatus(Communication.Status.UPCOMING);

    // expectations
    EasyMock.expect(mockCommunicationDbDao.find(pin, provider)).andReturn(communication);
    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.find(pin, provider);

    // assertions
    EasyMock.verify(mockCommunicationDbDao);
  }

  @Test(expected = DialledInTooLateException.class)
  public void findCommunicationByPinAndProviderThrowsTooLate() {
    // setup
    ReflectionTestUtils.setField(uut, "conferenceRoomUrl", MOCK_CONFERENCE_ROOM_URL);

    String pin = "012345678";
    String provider = "brighttalkhd";

    Date scheduledDateTime = getScheduledTime(-20);
    Integer duration = 120;

    Communication communication = new Communication(COMMUNICATION_ID);
    communication.setPinNumber(pin);
    communication.setScheduledDateTime(scheduledDateTime);
    communication.setDuration(duration);
    String expectedConferenceRoomUrl = MOCK_CONFERENCE_ROOM_URL.replace("{communicationId}",
        communication.getId().toString());
    communication.setConferenceRoomUrl(expectedConferenceRoomUrl);
    communication.setStatus(Communication.Status.UPCOMING);

    // expectations
    EasyMock.expect(mockCommunicationDbDao.find(pin, provider)).andReturn(communication);
    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.find(pin, provider);

    // assertions
    EasyMock.verify(mockCommunicationDbDao);
  }

  private Date getScheduledTime(final Integer minutes) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.MINUTE, minutes);
    return calendar.getTime();

  }

  @Test
  public void findSubscribedChannelsCommunications() {

    MyCommunicationsSearchCriteria searchCriteria = new MyCommunicationsSearchCriteria();

    User user = new User(USER_ID);

    PaginatedList<Communication> communications = new PaginatedList<Communication>();
    Communication communication = new Communication(COMMUNICATION_ID);
    communications.add(communication);

    EasyMock.expect(mockCommunicationDbDao.findPaginatedSubscribedChannelsCommunications(USER_ID, searchCriteria)).andReturn(
        communications);

    EasyMock.replay(mockCommunicationDbDao);

    // do test
    PaginatedList<Communication> result = uut.findSubscribedChannelsCommunications(user, searchCriteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(COMMUNICATION_ID, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsCommunicationsWhenPaginatedListContainsDuplications() {

    // Set up
    User user = new User(USER_ID);

    PaginatedList<Communication> communications = new PaginatedList<Communication>();

    CommunicationConfiguration configuration1 = new CommunicationConfiguration(COMMUNICATION_ID);
    configuration1.setInMasterChannel(true);

    Communication communication1 = new Communication(COMMUNICATION_ID);
    communication1.setConfiguration(configuration1);
    communications.add(communication1);

    CommunicationConfiguration configuration2 = new CommunicationConfiguration(COMMUNICATION_ID);
    configuration2.setInMasterChannel(false);

    Communication communication2 = new Communication(COMMUNICATION_ID);
    communication2.setConfiguration(configuration2);
    communications.add(communication2);

    MyCommunicationsSearchCriteria searchCriteria = new MyCommunicationsSearchCriteria();

    // Expectations
    EasyMock.expect(mockCommunicationDbDao.findPaginatedSubscribedChannelsCommunications(USER_ID, searchCriteria)).andReturn(
        communications).once();
    EasyMock.replay(mockCommunicationDbDao);

    // Do test
    PaginatedList<Communication> result = uut.findSubscribedChannelsCommunications(user, searchCriteria);

    // Assertions
    assertNotNull(result);
    assertEquals(1, result.size());
    assertTrue(result.get(0).getConfiguration().isInMasterChannel());

    EasyMock.verify(mockCommunicationDbDao);
  }

  @Test
  public void findSubscribedChannelsCommunicationsWithDefaultPagingSettings() {

    final int defaultPageNumber = 3;

    ReflectionTestUtils.setField(uut, "defaultPageNumber", defaultPageNumber);

    MyCommunicationsSearchCriteria searchCriteria = new MyCommunicationsSearchCriteria();

    User user = new User(USER_ID);

    EasyMock.expect(mockCommunicationDbDao.findPaginatedSubscribedChannelsCommunications(USER_ID, searchCriteria)).andReturn(
        new PaginatedList<Communication>());

    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.findSubscribedChannelsCommunications(user, searchCriteria);

    assertEquals(defaultPageNumber, (int) searchCriteria.getPageNumber());
    assertNull(searchCriteria.getPageSize());
  }

  @Test
  public void findSubscribedChannelsCommunicationsSearchCriteria() {

    final int expectedPageSize = 6;
    final int expectedPageNumber = 4;

    MyCommunicationsSearchCriteria searchCriteria = new MyCommunicationsSearchCriteria();
    searchCriteria.setPageSize(expectedPageSize);
    searchCriteria.setPageNumber(expectedPageNumber);

    User user = new User(USER_ID);

    EasyMock.expect(mockCommunicationDbDao.findPaginatedSubscribedChannelsCommunications(USER_ID, searchCriteria)).andReturn(
        new PaginatedList<Communication>());

    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.findSubscribedChannelsCommunications(user, searchCriteria);

    assertEquals(expectedPageSize, (int) searchCriteria.getPageSize());
    assertEquals(expectedPageNumber, (int) searchCriteria.getPageNumber());
  }

  @Test
  public void findChannelsCommunicationsWhenCommunicationDaoReturnsSingleCommunication() {

    CommunicationsSearchCriteria searchCriteria = new CommunicationsSearchCriteria();

    CommunicationConfiguration communicationConfiguration =
        new CommunicationConfigurationBuilder().withDefaultValues().withCustomUrl(
            null).build();

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
            CHANNEL_ID).withConfiguration(communicationConfiguration).build();
    PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(communication);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).build();
    List<Channel> channels = Arrays.asList(channel);

    EasyMock.expect(mockCommunicationDbDao.find(searchCriteria)).andReturn(communications);
    EasyMock.replay(mockCommunicationDbDao);

    EasyMock.expect(mockChannelFinder.find(Arrays.asList(CHANNEL_ID))).andReturn(channels);
    EasyMock.replay(mockChannelFinder);

    EasyMock.replay(mockCategoryDbDao);
    // do test
    PaginatedList<Communication> result = uut.find(searchCriteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(COMMUNICATION_ID, result.get(0).getId());

    EasyMock.verify(mockCommunicationDbDao, mockChannelFinder, mockCategoryDbDao);
  }

  @Test
  public void findChannelsCommunicationsWhenCommunicationDaoReturnsSingleCommunicationAndCriteriaIncludesCategories() {

    CommunicationsSearchCriteria searchCriteria = new CommunicationsSearchCriteria();
    searchCriteria.setExpand("categories");

    CommunicationConfiguration communicationConfiguration =
        new CommunicationConfigurationBuilder().withDefaultValues().withCustomUrl(
            null).build();

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
            CHANNEL_ID).withConfiguration(communicationConfiguration).build();
    PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(communication);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).build();
    List<Channel> channels = Arrays.asList(channel);

    EasyMock.expect(mockCommunicationDbDao.find(searchCriteria)).andReturn(communications);
    EasyMock.replay(mockCommunicationDbDao);

    EasyMock.expect(mockChannelFinder.find(Arrays.asList(CHANNEL_ID))).andReturn(channels);
    EasyMock.replay(mockChannelFinder);

    mockCategoryDbDao.loadCategories(CHANNEL_ID, communication);
    EasyMock.expectLastCall();
    EasyMock.replay(mockCategoryDbDao);

    // do test
    PaginatedList<Communication> result = uut.find(searchCriteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(COMMUNICATION_ID, result.get(0).getId());
    EasyMock.verify(mockCommunicationDbDao, mockChannelFinder, mockCategoryDbDao);
  }

  @Test
  public void findChannelsCommunicationsWhenCommunicationDaoReturnsSingleCommunicationWithDefaultPageNumber() {

    final int defaultPageNumber = 3;

    ReflectionTestUtils.setField(uut, "defaultPageNumber", defaultPageNumber);

    CommunicationsSearchCriteria searchCriteria = new CommunicationsSearchCriteria();

    EasyMock.expect(mockCommunicationDbDao.find(searchCriteria)).andReturn(new PaginatedList<Communication>());

    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.find(searchCriteria);

    assertEquals(defaultPageNumber, (int) searchCriteria.getPageNumber());
  }

  @Test
  public void findChannelsCommunicationsWhenCommunicationDaoReturnsSingleCommunicationWithDefaultPageSize() {

    final int defaultPageSize = 5;

    ReflectionTestUtils.setField(uut, "defaultPageSize", defaultPageSize);

    CommunicationsSearchCriteria searchCriteria = new CommunicationsSearchCriteria();

    EasyMock.expect(mockCommunicationDbDao.find(searchCriteria)).andReturn(new PaginatedList<Communication>());

    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.find(searchCriteria);

    assertEquals(defaultPageSize, (int) searchCriteria.getPageSize());
  }

  @Test
  public void findChannelsCommunicationsWhenPageSizeAndPageNumberProvided() {

    final int expectedPageSize = 6;
    final int expectedPageNumber = 4;

    CommunicationsSearchCriteria searchCriteria = new CommunicationsSearchCriteria();
    searchCriteria.setPageSize(expectedPageSize);
    searchCriteria.setPageNumber(expectedPageNumber);

    EasyMock.expect(mockCommunicationDbDao.find(searchCriteria)).andReturn(new PaginatedList<Communication>());

    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.find(searchCriteria);

    assertEquals(expectedPageSize, (int) searchCriteria.getPageSize());
    assertEquals(expectedPageNumber, (int) searchCriteria.getPageNumber());
  }

  @Test
  public void findChannelsCommunicationsWhenPageSizeBigerThenMax() {

    final int defaultPageSize = 5;
    ReflectionTestUtils.setField(uut, "maxPageSize", defaultPageSize);

    final int expectedPageSize = 6;

    CommunicationsSearchCriteria searchCriteria = new CommunicationsSearchCriteria();
    searchCriteria.setPageSize(expectedPageSize);

    EasyMock.expect(mockCommunicationDbDao.find(searchCriteria)).andReturn(new PaginatedList<Communication>());

    EasyMock.replay(mockCommunicationDbDao);

    // do test
    uut.find(searchCriteria);

    assertEquals(defaultPageSize, (int) searchCriteria.getPageSize());
  }

  @Test
  public void findChannelsCommunicationsWhenCommunicationDaoReturnsSingleCommunicationWithUrls() {

    CommunicationsSearchCriteria searchCriteria = new CommunicationsSearchCriteria();

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
            CHANNEL_ID).build();
    PaginatedList<Communication> communications = new PaginatedList<Communication>();
    communications.add(communication);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).build();
    List<Channel> channels = Arrays.asList(channel);

    EasyMock.expect(mockCommunicationDbDao.find(searchCriteria)).andReturn(communications);

    EasyMock.expect(mockChannelFinder.find(Arrays.asList(CHANNEL_ID))).andReturn(channels);

    EasyMock.expect(mockCalendarUrlGenerator.generate(channel, communication)).andReturn("calendarUrl");

    EasyMock.expect(mockCommunicationUrlGenerator.generate(channel, communication)).andReturn("communicationUrl");

    EasyMock.expect(mockCommunicationPortalUrlGenerator.generate(channel, communication)).andReturn(
        "communicationPortalUrl");

    EasyMock.expect(mockCommunicationPortalUrlGenerator.getDefaultPortalChannelUrl(channel, communication)).andReturn(
        "communication portal url");

    EasyMock.replay(mockCommunicationDbDao, mockChannelFinder, mockCalendarUrlGenerator,
        mockCommunicationPortalUrlGenerator, mockCommunicationUrlGenerator);

    // do test
    PaginatedList<Communication> result = uut.find(searchCriteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(COMMUNICATION_ID, result.get(0).getId());
    assertEquals("communicationPortalUrl", result.get(0).getUrl());
    assertEquals("calendarUrl", result.get(0).getCalendarUrl());

    EasyMock.verify(mockCommunicationDbDao, mockChannelFinder, mockCalendarUrlGenerator,
        mockCommunicationPortalUrlGenerator);

  }

}
