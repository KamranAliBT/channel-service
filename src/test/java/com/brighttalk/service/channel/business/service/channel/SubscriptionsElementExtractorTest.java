/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsElementExtractorTest.java 91908 2015-03-18 16:46:30Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.domain.extractor.SubscriptionsElementExtractor;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;
import com.brighttalk.service.channel.integration.communication.CommunicationServiceDao;
import com.brighttalk.service.channel.integration.communication.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.summit.SummitServiceDao;
import com.brighttalk.service.channel.integration.summit.dto.SummitDto;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.util.UserSearchCriteriaBuilder;
import com.brighttalk.service.utils.DomainFactoryUtils;

/**
 * Unit Test for {@link SubscriptionsElementExtractor}.
 */
public class SubscriptionsElementExtractorTest {

  private static final Long TEST_SUMMIT_ID_1 = 7876L;
  private static final Long TEST_SUMMIT_ID_2 = 4536L;
  private static final Long TEST_COMM_ID_1 = 4664L;
  private static final Long TEST_COMM_ID_2 = 28823L;
  private static final Long TEST_USER_ID_1 = 28823L;
  private static final Long TEST_USER_ID_2 = 7676L;
  private static final String TEST_USER_EMAIL_1 = "test@test.com";
  private static final String TEST_USER_EMAIL_2 = "test2@test.com";

  private SubscriptionsElementExtractor uut;

  private SummitServiceDao mockSummitServiceDao;

  private CommunicationServiceDao mockCommServiceDao;

  private UserSearchCriteriaBuilder mockUserSearchCriteriaBuilder;

  private UserServiceDao mockUserServiceDao;

  @Before
  public void setUp() {
    uut = new SubscriptionsElementExtractor();

    mockSummitServiceDao = EasyMock.createMock(SummitServiceDao.class);
    ReflectionTestUtils.setField(uut, "summitServiceDao", mockSummitServiceDao);

    mockCommServiceDao = EasyMock.createMock(CommunicationServiceDao.class);
    ReflectionTestUtils.setField(uut, "commServiceDao", mockCommServiceDao);

    mockUserSearchCriteriaBuilder = EasyMock.createMock(UserSearchCriteriaBuilder.class);
    ReflectionTestUtils.setField(uut, "userSearchCriteriaBuilder", mockUserSearchCriteriaBuilder);

    mockUserServiceDao = EasyMock.createMock(UserServiceDao.class);
    ReflectionTestUtils.setField(uut, "userServiceDao", mockUserServiceDao);

  }

  /**
   * Tests {@link SubscriptionsElementExtractor} in the success case of extracting list of summit from the supplied
   * subscriptions
   */
  @Test
  public void extractLeadContextSummitsSuccess() {
    // Setup -
    List<Long> summitIds = Arrays.asList(TEST_SUMMIT_ID_1, TEST_SUMMIT_ID_2);
    Subscription subscription1 = DomainFactoryUtils.createSubscription();
    subscription1.getContext().setLeadContext(TEST_SUMMIT_ID_1.toString());
    Subscription subscription2 = DomainFactoryUtils.createSubscription();
    subscription2.getContext().setLeadContext(TEST_SUMMIT_ID_2.toString());
    List<Subscription> subscriptions = Arrays.asList(subscription1, subscription2);

    // Expectations -
    List<SummitDto> summits = Arrays.asList(new SummitDto(TEST_SUMMIT_ID_1), new SummitDto(TEST_SUMMIT_ID_2));
    EasyMock.expect(mockSummitServiceDao.findPublicSummitsByIds(summitIds)).andReturn(summits);
    EasyMock.replay(mockSummitServiceDao);

    // Do Test -
    List<SummitDto> foundSummits = uut.extractLeadContextSummits(subscriptions);
    assertEquals(2, foundSummits.size());
    EasyMock.verify(mockSummitServiceDao);
  }

  /**
   * Tests {@link SubscriptionsElementExtractor} in the success case of extracting list of communication from the
   * supplied subscriptions
   */
  @Test
  public void extractLeadContextCommunicationsSuccess() {
    // Setup -
    List<Long> commIds = Arrays.asList(TEST_COMM_ID_1, TEST_COMM_ID_2);
    Subscription subscription1 = DomainFactoryUtils.createSubscription();
    subscription1.getContext().setLeadType(SubscriptionLeadType.CONTENT);
    subscription1.getContext().setLeadContext(TEST_COMM_ID_1.toString());
    Subscription subscription2 = DomainFactoryUtils.createSubscription();
    subscription2.getContext().setLeadType(SubscriptionLeadType.CONTENT);
    subscription2.getContext().setLeadContext(TEST_COMM_ID_2.toString());
    List<Subscription> subscriptions = Arrays.asList(subscription1, subscription2);

    // Expectations -
    List<CommunicationDto> comms = Arrays.asList(new CommunicationDto(TEST_COMM_ID_1),
        new CommunicationDto(TEST_COMM_ID_2));
    EasyMock.expect(mockCommServiceDao.findCommunicationsByIds(commIds)).andReturn(comms);
    EasyMock.replay(mockCommServiceDao);

    // Do Test -
    List<CommunicationDto> foundComms = uut.extractLeadContextCommunications(subscriptions);
    assertEquals(2, foundComms.size());
    EasyMock.verify(mockCommServiceDao);
  }

  /**
   * Tests {@link SubscriptionsElementExtractor} in the success case of extracting list of users by Id from the supplied
   * subscriptions
   */
  @Test
  @SuppressWarnings("unchecked")
  public void extractUsersByIdsSuccess() {
    // Setup -
    Subscription subscription1 = DomainFactoryUtils.createSubscription();
    subscription1.getUser().setId(TEST_USER_ID_1);
    Subscription subscription2 = DomainFactoryUtils.createSubscription();
    subscription2.getUser().setId(TEST_USER_ID_2);
    List<Subscription> subscriptions = Arrays.asList(subscription1, subscription2);

    // Expectations -
    List<User> users = Arrays.asList(new User(TEST_USER_ID_1), new User(TEST_USER_ID_2));
    EasyMock.expect(mockUserSearchCriteriaBuilder.build(isA(Integer.class), isA(Integer.class))).andReturn(
        new UsersSearchCriteria());
    EasyMock.expect(mockUserServiceDao.findByIdAndEmailAddress(isA(List.class), isA(List.class),
        isA(UsersSearchCriteria.class))).andReturn(users);
    EasyMock.replay(mockUserSearchCriteriaBuilder, mockUserServiceDao);

    // Do Test -
    List<User> foundUsers = uut.extractUsers(subscriptions);
    assertEquals(2, foundUsers.size());
    EasyMock.verify(mockUserSearchCriteriaBuilder, mockUserServiceDao);
  }

  /**
   * Tests {@link SubscriptionsElementExtractor} in the success case of extracting list of users by Email from the
   * supplied subscriptions
   */
  @Test
  @SuppressWarnings("unchecked")
  public void extractUsersByEmailsSuccess() {
    // Setup -
    Subscription subscription1 = DomainFactoryUtils.createSubscription();
    subscription1.getUser().setEmail(TEST_USER_EMAIL_1);
    Subscription subscription2 = DomainFactoryUtils.createSubscription();
    subscription2.getUser().setEmail(TEST_USER_EMAIL_2);
    List<Subscription> subscriptions = Arrays.asList(subscription1, subscription2);

    // Expectations -
    User user1 = new User();
    user1.setEmail(TEST_USER_EMAIL_1);
    User user2 = new User();
    user2.setEmail(TEST_USER_EMAIL_2);
    List<User> users = Arrays.asList(user1, user2);
    EasyMock.expect(mockUserSearchCriteriaBuilder.build(isA(Integer.class), isA(Integer.class))).andReturn(
        new UsersSearchCriteria());
    EasyMock.expect(mockUserServiceDao.findByIdAndEmailAddress(isA(List.class), isA(List.class),
        isA(UsersSearchCriteria.class))).andReturn(users);
    EasyMock.replay(mockUserSearchCriteriaBuilder, mockUserServiceDao);

    // Do Test -
    List<User> foundUsers = uut.extractUsers(subscriptions);
    assertEquals(2, foundUsers.size());
    EasyMock.verify(mockUserSearchCriteriaBuilder, mockUserServiceDao);
  }
}
