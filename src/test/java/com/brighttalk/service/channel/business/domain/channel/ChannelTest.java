/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelTest.java 91285 2015-03-09 13:05:18Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit Test for {@link Channel}.
 */
public class ChannelTest {

  private static final String CHANNEL_TITLE = "channelTitle";

  private static final String CHANNEL_DESCRIPTION = "channelDescription";

  private static final List<String> CHANNEL_KEYWORDS = Arrays.asList("keywords1", "keywords2");

  private static final String CHANNEL_ORGANISATION = "channelOrganisation";

  private static final String CHANNEL_STRAPLINE = "channelStrapline";

  private Channel uut;

  @Before
  public void setUp() {

    uut = new Channel();
    uut.setTitle(CHANNEL_TITLE);
    uut.setDescription(CHANNEL_DESCRIPTION);
    uut.setKeywords(CHANNEL_KEYWORDS);
    uut.setOrganisation(CHANNEL_ORGANISATION);
    uut.setStrapline(CHANNEL_STRAPLINE);
  }

  /**
   * Tests {@link Channel#merge(Channel)} in the success case of merging without updating any channel details.
   */
  @Test
  public void mergeWithoutChanges() {

    // do test
    uut.merge(new Channel());

    assertEquals(CHANNEL_TITLE, uut.getTitle());
    assertEquals(CHANNEL_DESCRIPTION, uut.getDescription());
    assertEquals(CHANNEL_KEYWORDS, uut.getKeywords());
    assertEquals(CHANNEL_ORGANISATION, uut.getOrganisation());
    assertEquals(CHANNEL_STRAPLINE, uut.getStrapline());
  }

  /**
   * Tests {@link Channel#merge(Channel)} in the success case of merging all channel details values.
   */
  @Test
  public void mergeWithChanges() {

    final String expectedTitle = "updateTitle";
    final String expectedDescription = "updateDescription";
    final List<String> expectedKeywords = Arrays.asList("updateKeywords");
    final String expectedOrganisation = "updateOrganisation";
    final String expectedStrapline = "updateStrapline";

    Channel updateChannel = new Channel();
    updateChannel.setTitle(expectedTitle);
    updateChannel.setDescription(expectedDescription);
    updateChannel.setKeywords(expectedKeywords);
    updateChannel.setOrganisation(expectedOrganisation);
    updateChannel.setStrapline(expectedStrapline);

    // do test
    uut.merge(updateChannel);

    assertEquals(expectedTitle, uut.getTitle());
    assertTrue(uut.isTitleChanged());
    assertEquals(expectedDescription, uut.getDescription());
    assertEquals(expectedKeywords, uut.getKeywords());
    assertEquals(expectedOrganisation, uut.getOrganisation());
    assertEquals(expectedStrapline, uut.getStrapline());

  }

  /**
   * Tests {@link Channel#merge(Channel)} in the success case of merging channel details without updating the channel
   * title.
   */
  @Test
  public void mergeWithoutChangedTitle() {
    final String expectedDescription = "updateDescription";
    final List<String> expectedKeywords = Arrays.asList("updateKeywords");
    final String expectedOrganisation = "updateOrganisation";
    final String expectedStrapline = "updateStrapline";

    Channel updateChannel = new Channel();
    updateChannel.setTitle(CHANNEL_TITLE);
    updateChannel.setDescription(expectedDescription);
    updateChannel.setKeywords(expectedKeywords);
    updateChannel.setOrganisation(expectedOrganisation);
    updateChannel.setStrapline(expectedStrapline);

    // do test
    uut.merge(updateChannel);

    assertEquals(CHANNEL_TITLE, uut.getTitle());
    assertFalse(uut.isTitleChanged());
    assertEquals(expectedDescription, uut.getDescription());
    assertEquals(expectedKeywords, uut.getKeywords());
    assertEquals(expectedOrganisation, uut.getOrganisation());
    assertEquals(expectedStrapline, uut.getStrapline());
  }
}