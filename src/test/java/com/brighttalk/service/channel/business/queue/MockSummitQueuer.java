/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MockDeleteChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Implementation of a summit Queuer that does not actually use a queue.
 * 
 * The required processing starts straight away.
 * 
 * This is used for unit testing.
 */
public class MockSummitQueuer implements SummitQueuer {

  /**
   * {@inheritDoc}
   */
  @Override
  public void reschedule(final Long webcastId) {
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void delete(final Long webcastId) {
  }

}