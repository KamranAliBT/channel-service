/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationProviderUpdateValidatorTest.java 96319 2015-06-09 12:25:26Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import static org.easymock.EasyMock.isA;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.brighttalk.common.configuration.Messages;
import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.error.CannotUpdateCurrentProviderException;
import com.brighttalk.service.channel.business.error.CommunicationOutsideAuthoringPeriodException;
import com.brighttalk.service.channel.business.error.CommunicationStatusNotSupportedException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.utils.UserErrorMessageMatcher;

@RunWith(Theories.class)
public class CommunicationProviderUpdateValidatorTest {

  private static final Long MASTER_CHANNEL_ID = 666l;

  @DataPoints
  public static final Status[] statuses = Status.values();

  @DataPoints
  public static final Provider[] providers = { new Provider(Provider.BRIGHTTALK), new Provider(Provider.MEDIAZONE),
      new Provider(Provider.VIDEO) };

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  private CommunicationProviderUpdateValidator uut;

  private Messages mockMessges;

  private ChannelFinder mockChannelFinder;

  private Communication communication;

  @Before
  public void setUp() {

    mockMessges = EasyMock.createMock(Messages.class);
    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);

    communication = new Communication();
    communication.setMasterChannelId(MASTER_CHANNEL_ID);

    Date now = new Date();
    communication.setScheduledDateTime(DateUtils.addHours(now, 1));

    uut = new CommunicationProviderUpdateValidator(communication, mockChannelFinder, mockMessges);
  }

  @Theory
  public void validateProviderCanBeChangedOnlyToMediazoneWhenCommunicationIsBrighttalk(final Provider targetProvider) {

    Provider provider = new Provider(Provider.BRIGHTTALK);

    communication.setProvider(provider);
    communication.setStatus(Status.UPCOMING);

    addImportMediaFeatureExpectations(true);

    // exception expectations
    if (!Provider.MEDIAZONE.equals(targetProvider.getName())) {

      final String expectedErrorMessage = "expectedErrorMessage";
      EasyMock.expect(mockMessges.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);
      EasyMock.replay(mockMessges);

      thrown.expect(CannotUpdateCurrentProviderException.class);
      thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));
    }

    // do test
    uut.validate(targetProvider);
  }

  @Theory
  public void validateProviderCanBeChangedForBrighttalkToMediazoneCommunicationOnlyWhenStatusIsUpcoming(
      final Status status) {

    Provider provider = new Provider(Provider.BRIGHTTALK);

    communication.setProvider(provider);
    communication.setStatus(status);

    addImportMediaFeatureExpectations(true);

    if (!Status.UPCOMING.equals(status)) {

      final String expectedErrorMessage = "expectedErrorMessage";
      EasyMock.expect(mockMessges.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);
      EasyMock.replay(mockMessges);

      thrown.expect(CommunicationStatusNotSupportedException.class);
      thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));
    }

    // do test
    uut.validate(new Provider(Provider.MEDIAZONE));
  }

  @Test
  public void validateWhenImportMediaFeatureIsNotEnabledAndUpdatingFromBrighttalkToMediazone() {

    Provider provider = new Provider(Provider.BRIGHTTALK);

    communication.setProvider(provider);
    communication.setStatus(Status.UPCOMING);

    final String expectedErrorMessage = "expectedErrorMessage";
    EasyMock.expect(
        mockMessges.getValue(CommunicationProviderUpdateValidator.IMPORT_MEDIA_FEATURE_NOT_ENABLED_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);

    addImportMediaFeatureExpectations(false);

    EasyMock.replay(mockMessges);

    thrown.expect(FeatureNotEnabledException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.validate(new Provider(Provider.MEDIAZONE));
  }

  @Test
  public void validateWhenImportMediaFeatureIsNotFoundAndUpdatingFromBrighttalkToMediazone() {

    Provider provider = new Provider(Provider.BRIGHTTALK);

    communication.setProvider(provider);
    communication.setStatus(Status.UPCOMING);

    Channel mockChannel = EasyMock.createMock(Channel.class);
    EasyMock.expect(mockChannel.getFeature(Type.MEDIA_ZONES)).andThrow(new ApplicationException("Test exception"));

    EasyMock.expect(mockChannelFinder.find(MASTER_CHANNEL_ID)).andReturn(mockChannel);

    final String expectedErrorMessage = "expectedErrorMessage";
    EasyMock.expect(
        mockMessges.getValue(CommunicationProviderUpdateValidator.IMPORT_MEDIA_FEATURE_NOT_ENABLED_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);

    EasyMock.replay(mockMessges, mockChannel, mockChannelFinder);

    thrown.expect(FeatureNotEnabledException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.validate(new Provider(Provider.MEDIAZONE));
  }

  @Test
  public void validateWhenMoreThan15MinutesToStart() {

    Provider provider = new Provider(Provider.BRIGHTTALK);

    communication.setProvider(provider);
    communication.setStatus(Status.UPCOMING);

    addImportMediaFeatureExpectations(true);

    Date now = new Date();
    communication.setScheduledDateTime(DateUtils.addMinutes(now, 16));

    // do test
    uut.validate(new Provider(Provider.MEDIAZONE));
  }

  @Test
  public void validateWhenLessThan15MinutesToStart() {

    Provider provider = new Provider(Provider.BRIGHTTALK);

    communication.setProvider(provider);
    communication.setStatus(Status.UPCOMING);

    addImportMediaFeatureExpectations(true);

    Date now = new Date();
    communication.setScheduledDateTime(DateUtils.addMinutes(now, 14));

    final String expectedMessage = "expectedErrorMessage";

    EasyMock.expect(mockMessges.getValue(CommunicationProviderUpdateValidator.OUTSIDE_OF_AUTHORING_PERIOD_MESSAGE_KEY)).andReturn(
        expectedMessage);
    EasyMock.replay(mockMessges);

    thrown.expect(CommunicationOutsideAuthoringPeriodException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedMessage));

    // do test
    uut.validate(new Provider(Provider.MEDIAZONE));
  }

  @Theory
  public void validateProviderCanBeChangedOnlyToVideoWhenCommunicationIsMediazone(final Provider targetProvider)
      throws Exception {

    Provider provider = new Provider(Provider.MEDIAZONE);

    communication.setProvider(provider);
    communication.setStatus(Status.PROCESSING);

    addVideoUploadFeatureExpectations(true);

    // exception expectations
    if (!Provider.VIDEO.equals(targetProvider.getName())) {

      final String expectedErrorMessage = "expectedErrorMessage";
      EasyMock.expect(mockMessges.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);
      EasyMock.replay(mockMessges);

      thrown.expect(CannotUpdateCurrentProviderException.class);
      thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));
    }

    // do test
    uut.validate(targetProvider);
  }

  @Theory
  public void validateProviderCanBeChangedForMediazoneCommunicationOnlyWhenStatusIsProcessing(final Status status) {

    Provider provider = new Provider(Provider.MEDIAZONE);

    communication.setProvider(provider);
    communication.setStatus(status);

    addVideoUploadFeatureExpectations(true);

    if (!Status.PROCESSING.equals(status)) {

      final String expectedErrorMessage = "expectedErrorMessage";
      EasyMock.expect(mockMessges.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);
      EasyMock.replay(mockMessges);

      thrown.expect(CommunicationStatusNotSupportedException.class);
      thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));
    }

    // do test
    uut.validate(new Provider(Provider.VIDEO));
  }

  @Test
  public void validateWhenVideoUploadFeatureIsNotEnabledAndUpdatingFromMediazoneToVideo() {

    Provider provider = new Provider(Provider.MEDIAZONE);

    communication.setProvider(provider);
    communication.setStatus(Status.PROCESSING);

    addVideoUploadFeatureExpectations(false);

    final String expectedErrorMessage = "expectedErrorMessage";
    EasyMock.expect(
        mockMessges.getValue(CommunicationProviderUpdateValidator.VIDEO_UPLOAD_FEATURE_NOT_ENABLED_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);
    EasyMock.replay(mockMessges);

    thrown.expect(FeatureNotEnabledException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.validate(new Provider(Provider.VIDEO));
  }

  @Test
  public void validateWhenVideoUploadFeatureIsNotFoundAndUpdatingFromMediazoneToVideo() {

    Provider provider = new Provider(Provider.MEDIAZONE);

    communication.setProvider(provider);
    communication.setStatus(Status.PROCESSING);

    Channel mockChannel = EasyMock.createMock(Channel.class);
    EasyMock.expect(mockChannel.getFeature(Type.ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS)).andThrow(
        new ApplicationException("Test exception"));

    EasyMock.expect(mockChannelFinder.find(MASTER_CHANNEL_ID)).andReturn(mockChannel);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockChannel, mockChannelFinder);

    final String expectedErrorMessage = "expectedErrorMessage";
    EasyMock.expect(
        mockMessges.getValue(CommunicationProviderUpdateValidator.VIDEO_UPLOAD_FEATURE_NOT_ENABLED_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);
    EasyMock.replay(mockMessges);

    thrown.expect(FeatureNotEnabledException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.validate(new Provider(Provider.VIDEO));
  }

  @Theory
  public void validateProviderCanBeChangedOnlyToMediazoneWhenCommunicationIsVideoUpload(final Provider targetProvider)
      throws Exception {

    Provider provider = new Provider(Provider.VIDEO);

    communication.setProvider(provider);
    communication.setStatus(Status.RECORDED);

    addImportMediaFeatureExpectations(true);

    // exception expectations
    if (!Provider.MEDIAZONE.equals(targetProvider.getName())) {

      final String expectedErrorMessage = "expectedErrorMessage";
      EasyMock.expect(mockMessges.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);
      EasyMock.replay(mockMessges);

      thrown.expect(CannotUpdateCurrentProviderException.class);
      thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));
    }

    // do test
    uut.validate(targetProvider);
  }

  @Theory
  public void validateProviderCanBeChangedForVideoUploadCommunicationOnlyWhenStatusIsRecorded(final Status status) {

    Provider provider = new Provider(Provider.VIDEO);

    communication.setProvider(provider);
    communication.setStatus(status);

    addImportMediaFeatureExpectations(true);

    if (!Status.RECORDED.equals(status)) {

      final String expectedErrorMessage = "expectedErrorMessage";
      EasyMock.expect(mockMessges.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);
      EasyMock.replay(mockMessges);

      thrown.expect(CommunicationStatusNotSupportedException.class);
      thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));
    }

    // do test
    uut.validate(new Provider(Provider.MEDIAZONE));
  }

  @Test
  public void validateProviderCannotBeUpdatedToRandomProvider() {

    Provider provider = new Provider(Provider.MEDIAZONE);

    communication.setProvider(provider);

    final String expectedErrorMessage = "expectedErrorMessage";
    EasyMock.expect(mockMessges.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);
    EasyMock.replay(mockMessges);

    thrown.expect(CannotUpdateCurrentProviderException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.validate(new Provider("someRandomProvider"));
  }

  @Test
  public void validateWhenImportMediaFeatureIsNotEnabledAndUpdatingFromMediazoneToVideo() {

    Provider provider = new Provider(Provider.MEDIAZONE);

    communication.setProvider(provider);
    communication.setStatus(Status.PROCESSING);

    Channel mockChannel = EasyMock.createMock(Channel.class);
    EasyMock.expect(mockChannel.getFeature(Type.ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS)).andThrow(
        new ApplicationException("Test exception"));

    EasyMock.expect(mockChannelFinder.find(MASTER_CHANNEL_ID)).andReturn(mockChannel);

    EasyMock.replay(mockChannel, mockChannelFinder);

    final String expectedErrorMessage = "expectedErrorMessage";
    EasyMock.expect(
        mockMessges.getValue(CommunicationProviderUpdateValidator.VIDEO_UPLOAD_FEATURE_NOT_ENABLED_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);
    EasyMock.replay(mockMessges);

    thrown.expect(FeatureNotEnabledException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.validate(new Provider(Provider.VIDEO));
  }

  private void addVideoUploadFeatureExpectations(final boolean isEnabled) {

    Channel mockChannel = EasyMock.createMock(Channel.class);
    Feature feature = new Feature(Type.ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS);
    feature.setIsEnabled(isEnabled);
    EasyMock.expect(mockChannel.getFeature(Type.ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS)).andReturn(feature);

    EasyMock.expect(mockChannelFinder.find(MASTER_CHANNEL_ID)).andReturn(mockChannel);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockChannel, mockChannelFinder);
  }

  private void addImportMediaFeatureExpectations(final boolean isEnabled) {

    Channel mockChannel = EasyMock.createMock(Channel.class);
    Feature feature = new Feature(Type.MEDIA_ZONES);
    feature.setIsEnabled(isEnabled);
    EasyMock.expect(mockChannel.getFeature(Type.MEDIA_ZONES)).andReturn(feature);

    EasyMock.expect(mockChannelFinder.find(MASTER_CHANNEL_ID)).andReturn(mockChannel);

    EasyMock.replay(mockChannel, mockChannelFinder);
  }

}