/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationCreateValidatorTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.InvalidCommunicationException;

public class CommunicationCreateValidatorTest {

  private CommunicationCreateValidator uut;

  @Test
  public void validationSuccess() {
    // Set up
    Feature maxDurationFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxDurationFeature.setIsEnabled(true);
    maxDurationFeature.setValue("900");
    List<Feature> features = new ArrayList<Feature>();
    features.add(maxDurationFeature);
    Channel channel = new ChannelBuilder().withDefaultValues().withFeatures(features).build();
    boolean isCommnunicationOverlapping = false;

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    uut = new CommunicationCreateValidator(channel, isCommnunicationOverlapping);

    // exercise
    uut.validate(communication);

  }

  @Test
  public void validationThrowsInvalidCommunicationExceptionWhenCommunicationDurationValidationFails() {
    // Set up
    Feature maxDurationFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxDurationFeature.setIsEnabled(true);
    maxDurationFeature.setValue("600");
    List<Feature> features = new ArrayList<Feature>();
    features.add(maxDurationFeature);
    Channel channel = new ChannelBuilder().withDefaultValues().withFeatures(features).build();
    boolean isCommnunicationOverlapping = false;

    Communication communication = new CommunicationBuilder().withDefaultValues().withDuration(900).build();
    uut = new CommunicationCreateValidator(channel, isCommnunicationOverlapping);
    String expectedErrorMessage = InvalidCommunicationException.ErrorCode.CommunicationTooLong.toString();

    // exercise
    try {
      uut.validate(communication);
    } catch (InvalidCommunicationException e) {
      Assert.assertEquals(expectedErrorMessage, e.getUserErrorMessage());
    }

  }

  @Test
  public void validationThrowsInvalidCommunicationExceptionWhenFutureOnlyValidationFails() {
    // Set up
    Feature maxDurationFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxDurationFeature.setIsEnabled(true);
    maxDurationFeature.setValue("900");
    List<Feature> features = new ArrayList<Feature>();
    features.add(maxDurationFeature);
    Channel channel = new ChannelBuilder().withDefaultValues().withFeatures(features).build();
    boolean isCommnunicationOverlapping = false;

    Date scheduledDateTime = new DateTime().minusDays(1).toDate();
    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateTime).build();
    uut = new CommunicationCreateValidator(channel, isCommnunicationOverlapping);
    String expectedErrorMessage = InvalidCommunicationException.ErrorCode.ScheduledTimeInTheFuture.toString();

    // exercise
    try {
      uut.validate(communication);
    } catch (InvalidCommunicationException e) {
      Assert.assertEquals(expectedErrorMessage, e.getUserErrorMessage());
    }
  }

  @Test
  public void validationThrowsInvalidCommunicationExceptionWhenCommunicationOverlaps() {
    // Set up
    Feature maxDurationFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxDurationFeature.setIsEnabled(true);
    maxDurationFeature.setValue("900");
    List<Feature> features = new ArrayList<Feature>();
    features.add(maxDurationFeature);
    Channel channel = new ChannelBuilder().withDefaultValues().withFeatures(features).build();
    boolean isCommnunicationOverlapping = true;

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    uut = new CommunicationCreateValidator(channel, isCommnunicationOverlapping);
    String expectedErrorMessage = InvalidCommunicationException.ErrorCode.OverlappingCommunication.toString();

    // exercise
    try {
      uut.validate(communication);
    } catch (InvalidCommunicationException e) {
      Assert.assertEquals(expectedErrorMessage, e.getUserErrorMessage());
    }
  }

}