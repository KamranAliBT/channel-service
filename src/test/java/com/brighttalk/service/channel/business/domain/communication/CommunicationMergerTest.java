/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationTest.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration.Visibility;

/**
 * Unit tests for {@link CommunicationMerger}.
 */
public class CommunicationMergerTest {

  private static final Long COMMUNICATION_ID = 2L;
  private static final Long CHANNEL_ID = 23L;
  private static final Long SURVEY_ID = 36L;

  private CommunicationMerger uut;

  @Before
  public void setUp() {
    uut = new CommunicationMerger();
  }

  @Test
  public void testMergeCommunication() {
    // SET UP
    String testStringA = "String A";
    String testStringB = "String B";
    Status statusA = Status.UPCOMING;
    Status statusB = Status.LIVE;
    Integer testIntegerA = 120;
    Integer testIntegerB = 240;
    PublishStatus publishStatusA = PublishStatus.PUBLISHED;
    PublishStatus publishStatusB = PublishStatus.UNPUBLISHED;
    Date scheduledDateTimeA = new Date(1420113600);
    Date scheduledDateTimeB = new Date();
    CommunicationCategory communicationCategoryA = new CommunicationCategory(1L, CHANNEL_ID, testStringA, true);
    CommunicationCategory communicationCategoryB = new CommunicationCategory(5L, CHANNEL_ID, testStringB, true);
    Boolean testBooleanA = true;
    Boolean testBooleanB = false;
    Visibility testVisibilityA = Visibility.PUBLIC;
    Visibility testVisibilityB = Visibility.PRIVATE;

    List<CommunicationCategory> categoriesA = new ArrayList<>();
    categoriesA.add(communicationCategoryA);

    List<CommunicationCategory> categoriesB = new ArrayList<>();
    categoriesB.add(communicationCategoryB);

    CommunicationConfiguration communicationConfigurationA = new CommunicationConfiguration();
    communicationConfigurationA.setAllowAnonymousViewings(testBooleanA);
    communicationConfigurationA.setClientBookingReference(testStringA);
    communicationConfigurationA.setCustomUrl(testStringA);
    communicationConfigurationA.setExcludeFromChannelContentPlan(testBooleanA);
    communicationConfigurationA.setShowChannelSurvey(testBooleanA);
    communicationConfigurationA.setSurveyActive(testBooleanA);
    communicationConfigurationA.setSurveyId(SURVEY_ID);
    communicationConfigurationA.setVisibility(testVisibilityA);

    CommunicationConfiguration communicationConfigurationB = new CommunicationConfiguration();
    communicationConfigurationB.setAllowAnonymousViewings(testBooleanB);
    communicationConfigurationB.setClientBookingReference(testStringB);
    communicationConfigurationB.setCustomUrl(testStringB);
    communicationConfigurationB.setExcludeFromChannelContentPlan(testBooleanB);
    communicationConfigurationB.setShowChannelSurvey(testBooleanB);
    communicationConfigurationB.setSurveyActive(testBooleanB);
    communicationConfigurationB.setSurveyId(SURVEY_ID);
    communicationConfigurationB.setVisibility(testVisibilityB);

    CommunicationScheduledPublication communicationScheduledPublicationA = new CommunicationScheduledPublication();
    communicationScheduledPublicationA.setPublicationDate(scheduledDateTimeA);
    communicationScheduledPublicationA.setUnpublicationDate(scheduledDateTimeA);
    CommunicationScheduledPublication communicationScheduledPublicationB = new CommunicationScheduledPublication();
    communicationScheduledPublicationB.setPublicationDate(scheduledDateTimeB);
    communicationScheduledPublicationB.setUnpublicationDate(scheduledDateTimeB);

    Rendition renditionA = new Rendition();
    renditionA.setBitrate(testIntegerA.longValue());
    renditionA.setCodecs(testStringA);
    renditionA.setContainer(testStringA);
    renditionA.setIsActive(testBooleanA);
    renditionA.setType(testStringA);
    renditionA.setUrl(testStringA);
    renditionA.setWidth(testIntegerA.longValue());

    Rendition renditionB = new Rendition();
    renditionB.setBitrate(testIntegerB.longValue());
    renditionB.setCodecs(testStringB);
    renditionB.setContainer(testStringB);
    renditionB.setIsActive(testBooleanB);
    renditionB.setType(testStringB);
    renditionB.setUrl(testStringB);
    renditionB.setWidth(testIntegerB.longValue());

    List<Rendition> renditionsA = new ArrayList<>();
    renditionsA.add(renditionA);

    List<Rendition> renditionsB = new ArrayList<>();
    renditionsB.add(renditionB);

    Communication communicationA = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withTitle(testStringA).withStatus(statusA).withScheduledDateTime(scheduledDateTimeA).withTimeZone(
        testStringA).withPresenter(testStringA).withDescription(testStringA).withKeywords(testStringA).withDuration(
        testIntegerA).withPublishStatus(publishStatusA).withThumbnailUrl(testStringA).withPreviewUrl(testStringA).withCategories(
        categoriesA).withConfiguration(communicationConfigurationA).withScheduledPublication(
        communicationScheduledPublicationA).withRenditions(renditionsA).build();

    Communication communicationB = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withTitle(testStringB).withStatus(statusB).withScheduledDateTime(scheduledDateTimeB).withTimeZone(
        testStringB).withPresenter(testStringB).withDescription(testStringB).withKeywords(testStringB).withDuration(
        testIntegerB).withPublishStatus(publishStatusB).withThumbnailUrl(testStringB).withPreviewUrl(testStringB).withCategories(
        categoriesB).withConfiguration(communicationConfigurationB).withScheduledPublication(
        communicationScheduledPublicationB).withRenditions(renditionsB).build();

    // Exercise
    uut.mergeCommunication(communicationA, communicationB);

    // assertions
    assertEquals(testStringB, communicationA.getTitle());
    assertEquals(testStringB, communicationA.getDescription());
    assertEquals(testStringB, communicationA.getKeywords());
    assertEquals(testStringB, communicationA.getAuthors());
    assertEquals(testStringB, communicationA.getTimeZone());
    assertEquals(testStringB, communicationA.getThumbnailUrl());
    assertEquals(testStringB, communicationA.getPreviewUrl());

    assertEquals(testIntegerB, communicationA.getDuration());
    // null as statusB needs to be upcoming
    assertEquals(null, communicationA.getBookingDuration());

    assertEquals(statusB, communicationA.getStatus());
    assertEquals(publishStatusB, communicationA.getPublishStatus());
    assertEquals(scheduledDateTimeB, communicationA.getScheduledDateTime());
    assertEquals(categoriesB, communicationA.getCategories());

    assertEquals(communicationConfigurationB.getVisibility(), communicationA.getConfiguration().getVisibility());
    assertEquals(communicationConfigurationB.allowAnonymousViewings(),
        communicationA.getConfiguration().allowAnonymousViewings());
    assertEquals(communicationConfigurationB.excludeFromChannelContentPlan(),
        communicationA.getConfiguration().excludeFromChannelContentPlan());
    assertEquals(communicationConfigurationB.showChannelSurvey(), communicationA.getConfiguration().showChannelSurvey());
    assertEquals(communicationConfigurationB.getCustomUrl(), communicationA.getConfiguration().getCustomUrl());
    assertEquals(communicationConfigurationB.getClientBookingReference(),
        communicationA.getConfiguration().getClientBookingReference());

    assertEquals(communicationScheduledPublicationB.getPublicationDate(),
        communicationA.getScheduledPublication().getPublicationDate());
    assertEquals(communicationScheduledPublicationB.getUnpublicationDate(),
        communicationA.getScheduledPublication().getUnpublicationDate());

    Rendition rendition = communicationA.getRenditions().get(0);
    assertEquals(renditionB.getBitrate(), rendition.getBitrate());
    assertEquals(renditionB.getCodecs(), rendition.getCodecs());
    assertEquals(renditionB.getContainer(), rendition.getContainer());
    assertEquals(renditionB.getIsActive(), rendition.getIsActive());
    assertEquals(renditionB.getType(), rendition.getType());
    assertEquals(renditionB.getUrl(), rendition.getUrl());
    assertEquals(renditionB.getWidth(), rendition.getWidth());
  }

  @Test
  public void testMergeCommunicationWhenFeatureImageIsDeleted() {
    // SET UP
    String testStringA = "String A";
    String testStringB = "String B";
    Status statusA = Status.UPCOMING;
    Status statusB = Status.LIVE;
    Integer testIntegerA = 120;
    Integer testIntegerB = 240;
    PublishStatus publishStatusA = PublishStatus.PUBLISHED;
    PublishStatus publishStatusB = PublishStatus.UNPUBLISHED;
    Date scheduledDateTimeA = new Date(1420113600);
    Date scheduledDateTimeB = new Date();

    Communication communicationA = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withTitle(testStringA).withStatus(statusA).withScheduledDateTime(scheduledDateTimeA).withTimeZone(
        testStringA).withPresenter(testStringA).withDescription(testStringA).withKeywords(testStringA).withDuration(
        testIntegerA).withPublishStatus(publishStatusA).withThumbnailUrl(testStringA).withPreviewUrl(testStringA).build();

    Communication communicationB = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withTitle(testStringB).withStatus(statusB).withScheduledDateTime(scheduledDateTimeB).withTimeZone(
        testStringB).withPresenter(testStringB).withDescription(testStringB).withKeywords(testStringB).withDuration(
        testIntegerB).withPublishStatus(publishStatusB).withThumbnailUrl(testStringB).withPreviewUrl(
        Communication.FEATURE_IMAGE_DELETED).build();

    // Exercise
    uut.mergeCommunication(communicationA, communicationB);

    // assertions
    assertEquals(testStringB, communicationA.getTitle());
    assertEquals(testStringB, communicationA.getDescription());
    assertEquals(testStringB, communicationA.getKeywords());
    assertEquals(testStringB, communicationA.getAuthors());
    assertEquals(testStringB, communicationA.getTimeZone());
    assertEquals(null, communicationA.getThumbnailUrl());
    assertEquals(null, communicationA.getPreviewUrl());

    assertEquals(testIntegerB, communicationA.getDuration());
    // null as statusB needs to be upcoming
    assertEquals(null, communicationA.getBookingDuration());

    assertEquals(statusB, communicationA.getStatus());
    assertEquals(publishStatusB, communicationA.getPublishStatus());
    assertEquals(scheduledDateTimeB, communicationA.getScheduledDateTime());
    assertEquals(communicationB.getCategories(), communicationA.getCategories());

    assertEquals(communicationB.getConfiguration().getVisibility(), communicationA.getConfiguration().getVisibility());
    assertEquals(communicationB.getConfiguration().allowAnonymousViewings(),
        communicationA.getConfiguration().allowAnonymousViewings());
    assertEquals(communicationB.getConfiguration().excludeFromChannelContentPlan(),
        communicationA.getConfiguration().excludeFromChannelContentPlan());
    assertEquals(communicationB.getConfiguration().showChannelSurvey(),
        communicationA.getConfiguration().showChannelSurvey());
    assertEquals(communicationB.getConfiguration().getCustomUrl(), communicationA.getConfiguration().getCustomUrl());
    assertEquals(communicationB.getConfiguration().getClientBookingReference(),
        communicationA.getConfiguration().getClientBookingReference());

    assertEquals(communicationB.getScheduledPublication().getPublicationDate(),
        communicationA.getScheduledPublication().getPublicationDate());
    assertEquals(communicationB.getScheduledPublication().getUnpublicationDate(),
        communicationA.getScheduledPublication().getUnpublicationDate());
  }

}