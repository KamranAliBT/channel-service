/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelFinderTest.java 70141 2013-10-23 15:46:02Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import java.util.Date;
import java.util.HashMap;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.time.DateTimeParser;
import com.brighttalk.service.channel.business.domain.DefaultTimestampToolbox;
import com.brighttalk.service.channel.business.domain.TimestampToolbox;

/**
 * Unit tests for {@link DefaultTimestampToolbox}.
 */
public class DefaultTimestampToolboxTest {

  private DefaultTimestampToolbox uut;

  @Before
  public void setUp() {
    uut = new DefaultTimestampToolbox();

  }

  @Test
  public void inFirstMonth_CreatedToday_ScheduledToday() {
    Date created = new Date();
    Date scheduled = uut.addSeconds(created, 60 * 60);

    boolean expectedResult = true;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedToday_ScheduledOneSecondAgo() {
    Date created = new Date();
    Date scheduled = uut.addSeconds(created, -1);
    boolean expectedResult = true;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedToday_ScheduledOneDayInPast() {
    Date created = new Date();
    Date scheduled = uut.addDays(created, -1);
    boolean expectedResult = false;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedToday_Scheduled32DaysFromNow() {
    Date created = new Date();
    Date scheduled = uut.addDays(created, 32);
    boolean expectedResult = false;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedToday_Scheduled27DaysFromNow() {
    Date created = new Date();
    Date scheduled = uut.addDays(created, 27);

    boolean expectedResult = true;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedJan01_ScheduledFeb01() {
    Date created = parseStringToDate("2011-01-01T12:00:00Z");
    Date scheduled = parseStringToDate("2011-02-01T12:00:00Z");

    boolean expectedResult = false;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);

  }

  @Test
  public void inFirstMonth_CreatedJan01_ScheduledJan31() {
    Date created = parseStringToDate("2011-01-01T12:00:00Z");
    Date scheduled = parseStringToDate("2011-01-31T12:00:00Z");

    boolean expectedResult = true;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedJan01_ScheduledDec31() {
    Date created = parseStringToDate("2011-01-01T12:00:00Z");
    Date scheduled = parseStringToDate("2010-12-31T12:00:00Z");

    boolean expectedResult = false;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedJan31_ScheduledJan31() {
    Date created = parseStringToDate("2011-01-31T12:00:00Z");
    Date scheduled = parseStringToDate("2011-01-31T12:00:00Z");

    boolean expectedResult = true;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedJan31_ScheduledFeb28() {
    Date created = parseStringToDate("2011-01-31T12:00:00Z");
    Date scheduled = parseStringToDate("2011-02-28T23:59:59Z");

    boolean expectedResult = true;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedJan31_ScheduledFeb29_LeapYear() {
    Date created = parseStringToDate("2012-01-31T12:00:00Z");
    Date scheduled = parseStringToDate("2012-02-29T23:59:59Z");

    boolean expectedResult = true;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedJan31_ScheduledMar01() {
    Date created = parseStringToDate("2011-01-31T12:00:00Z");
    Date scheduled = parseStringToDate("2011-03-01T00:00:00Z");

    boolean expectedResult = false;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedAug20_ScheduledSept20() {
    Date created = parseStringToDate("2011-08-20T12:00:00Z");
    Date scheduled = parseStringToDate("2011-09-20T00:00:00Z");

    boolean expectedResult = false;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedAug20_ScheduledSept19() {
    Date created = parseStringToDate("2011-08-20T12:00:00Z");
    Date scheduled = parseStringToDate("2011-09-19T00:00:00Z");

    boolean expectedResult = true;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedDec20_ScheduledJan19() {
    Date created = parseStringToDate("2011-12-20T12:00:00Z");
    Date scheduled = parseStringToDate("2012-01-19T00:00:00Z");

    boolean expectedResult = true;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedDec20_ScheduledJan20() {
    Date created = parseStringToDate("2011-12-20T12:00:00Z");
    Date scheduled = parseStringToDate("2012-01-20T00:00:00Z");

    boolean expectedResult = false;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  @Test
  public void inFirstMonth_CreatedJan31_ScheduledMar01Midnight() {
    Date created = parseStringToDate("2011-12-20T12:00:00Z");
    Date scheduled = parseStringToDate("2012-01-21T00:00:00Z");

    boolean expectedResult = false;

    // Do Test
    doInFirstMonthTest(created, scheduled, expectedResult);
  }

  private void doInFirstMonthTest(final Date created, final Date scheduled, final boolean expectedResult) {

    // Do test
    boolean result = uut.inFirstMonth(created, scheduled);

    // Assertions
    Assert.assertEquals(expectedResult, result);
  }

  @Test
  public void addMonth_Jan01_1Month() {
    // Set Up
    Date date = parseStringToDate("2011-01-01T12:00:00Z");
    int months = 1;

    // Expectations
    Date expectedResult = parseStringToDate("2011-02-01T12:00:00Z");

    // Do Test
    doAddMonthsTest(date, months, expectedResult);
  }

  @Test
  public void addMonth_Jan01_12Months() {
    // Set Up
    Date date = parseStringToDate("2011-01-01T12:00:00Z");
    int months = 12;

    // Expectations
    Date expectedResult = parseStringToDate("2012-01-01T12:00:00Z");

    // Do Test
    doAddMonthsTest(date, months, expectedResult);
  }

  @Test
  public void addMonth_Jan31_1Month() {
    // Set Up
    Date date = parseStringToDate("2011-01-31T12:00:00Z");
    int months = 1;

    // Expectations
    Date expectedResult = parseStringToDate("2011-02-28T23:59:59Z");

    // Do Test
    doAddMonthsTest(date, months, expectedResult);
  }

  @Test
  public void addMonth_Jan31_2Months() {
    // Set Up
    Date date = parseStringToDate("2011-01-31T12:00:00Z");
    int months = 2;

    // Expectations
    Date expectedResult = parseStringToDate("2011-03-31T12:00:00Z");

    // Do Test
    doAddMonthsTest(date, months, expectedResult);
  }

  @Test
  public void addMonth_Jan31_3Months() {
    // Set Up
    Date date = parseStringToDate("2011-01-31T12:00:00Z");
    int months = 3;

    // Expectations
    Date expectedResult = parseStringToDate("2011-04-30T23:59:59Z");

    // Do Test
    doAddMonthsTest(date, months, expectedResult);
  }

  @Test
  public void addMonth_LeapYear_Jan31_1Months() {
    // Set Up
    Date date = parseStringToDate("2012-01-31T12:00:00Z");
    int months = 1;

    // Expectations
    Date expectedResult = parseStringToDate("2012-02-29T23:59:59Z");

    // Do Test
    doAddMonthsTest(date, months, expectedResult);
  }

  private void doAddMonthsTest(final Date date, final int months, final Date expectedResult) {

    // Do test
    Date result = uut.addMonths(date, months);

    // Assertions
    Assert.assertEquals(expectedResult, result);
  }

  @Test
  public void getPeriod_CreatedJan01_3Months() {
    // Set up
    Date created = parseStringToDate("2011-01-01T12:00:00Z");
    Date scheduled = parseStringToDate("2011-02-01T12:00:00Z");
    int periodInMonths = 3;

    // Expectations
    Date expectedStart = parseStringToDate("2011-02-01T00:00:00Z");
    Date expectedEnd = parseStringToDate("2011-04-30T23:59:59Z");

    // Do Test
    doGetPeriodTest(created, scheduled, periodInMonths, expectedStart, expectedEnd);
  }

  @Test
  public void getPeriod_CreatedJan15_3Months() {
    // Set up
    Date created = parseStringToDate("2011-01-15T12:00:00Z");
    Date scheduled = parseStringToDate("2011-02-28T12:00:00Z");
    int periodInMonths = 3;

    // Expectations
    Date expectedStart = parseStringToDate("2011-02-15T00:00:00Z");
    Date expectedEnd = parseStringToDate("2011-05-14T23:59:59Z");

    // Do Test
    doGetPeriodTest(created, scheduled, periodInMonths, expectedStart, expectedEnd);
  }

  @Test
  public void getPeriod_CreatedJan15_12Months() {
    // Set up
    Date created = parseStringToDate("2011-01-15T12:00:00Z");
    Date scheduled = parseStringToDate("2011-02-28T12:00:00Z");
    int periodInMonths = 12;

    // Expectations
    Date expectedStart = parseStringToDate("2011-02-15T00:00:00Z");
    Date expectedEnd = parseStringToDate("2012-02-14T23:59:59Z");

    // Do Test
    doGetPeriodTest(created, scheduled, periodInMonths, expectedStart, expectedEnd);
  }

  @Test
  public void getPeriod_CreatedJan15_2Months_ScheduledNextYear() {
    // Set up
    Date created = parseStringToDate("2011-01-15T12:00:00Z");
    Date scheduled = parseStringToDate("2012-02-28T12:00:00Z");
    int periodInMonths = 2;

    // Expectations
    Date expectedStart = parseStringToDate("2012-02-15T00:00:00Z");
    Date expectedEnd = parseStringToDate("2012-04-14T23:59:59Z");

    // Do Test
    doGetPeriodTest(created, scheduled, periodInMonths, expectedStart, expectedEnd);
  }

  @Test
  public void getPeriod_CreatedJan15_12Months_ScheduledNextYear() {
    // Set up
    Date created = parseStringToDate("2011-01-15T12:00:00Z");
    Date scheduled = parseStringToDate("2012-02-28T12:00:00Z");
    int periodInMonths = 12;

    // Expectations
    Date expectedStart = parseStringToDate("2012-02-15T00:00:00Z");
    Date expectedEnd = parseStringToDate("2013-02-14T23:59:59Z");

    // Do Test
    doGetPeriodTest(created, scheduled, periodInMonths, expectedStart, expectedEnd);
  }

  @Test
  public void getPeriod_CreatedJan15_ScheduledPast_2Months_FirstPeriod() {
    // Set up
    Date created = parseStringToDate("2011-01-15T12:00:00Z");
    Date scheduled = parseStringToDate("2010-12-28T12:00:00Z");
    int periodInMonths = 2;

    // Expectations
    Date expectedStart = parseStringToDate("2010-11-15T00:00:00Z");
    Date expectedEnd = parseStringToDate("2011-01-14T23:59:59Z");

    // Do Test
    doGetPeriodTest(created, scheduled, periodInMonths, expectedStart, expectedEnd);
  }

  @Test
  public void getPeriod_CreatedJan15_ScheduledPast_2Months_SecondPeriod() {
    // Set up
    Date created = parseStringToDate("2011-01-15T12:00:00Z");
    Date scheduled = parseStringToDate("2009-12-28T12:00:00Z");
    int periodInMonths = 2;

    // Expectations
    Date expectedStart = parseStringToDate("2009-11-15T00:00:00Z");
    Date expectedEnd = parseStringToDate("2010-01-14T23:59:59Z");

    // Do Test
    doGetPeriodTest(created, scheduled, periodInMonths, expectedStart, expectedEnd);
  }

  private void doGetPeriodTest(final Date created, final Date scheduled, final int periodInMonths,
    final Date expectedStart, final Date expectedEnd) {

    // Do test
    HashMap<String, Date> result = uut.getPeriod(created, scheduled, periodInMonths);

    // Assertions
    Date startPeriod = result.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = result.get(TimestampToolbox.END_PERIOD);

    Assert.assertEquals(expectedStart, startPeriod);
    Assert.assertEquals(expectedEnd, endPeriod);
  }

  private Date parseStringToDate(final String date) {
    DateTimeParser dtp = new DateTimeParser();
    return dtp.parse(date);
  }
}
