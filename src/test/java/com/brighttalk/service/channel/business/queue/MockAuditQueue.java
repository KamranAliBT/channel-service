/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MockAuditQueue.java 78594 2014-05-28 15:11:50Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * Implementation of a audit queue that does not actually use a queue.
 * 
 * The required processing starts straight away.
 * 
 * This is used for unit testing.
 */
public class MockAuditQueue implements AuditQueue {

  /**
   * {@inheritDoc}
   */
  @Override
  public void create(final AuditRecordDto auditRecordDto) {
  }

}
