/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: CommunicationsSearchCriteriaValidationTest.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;

import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;

@RunWith(Theories.class)
public class CommunicationsSearchCriteriaValidationTest {

  private static Validator validator;

  private CommunicationsSearchCriteria uut;

  @BeforeClass
  public static void createValidator() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Before
  public void setUp() {
    uut = new CommunicationsSearchCriteria();
  }

  @Test
  public void failValidationWhenInvalidExpandAndEmptyIds() {

    uut.setExpand("invalidvalue");
    uut.setIds(null);

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(2, constraintViolations.size());
  }

  @Test
  public void failValidationWhenInvalidExpandAndSingleValueIds() {

    uut.setExpand("invalidvalue");
    uut.setIds("10-1");

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenIdsIsNull() {

    uut.setIds(null);

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenIdsIsEmpty() {

    uut.setIds("");

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenIdsHasNumberAndDash() {

    uut.setIds("10-");

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenIdsHasNumberDashAndLeter() {

    uut.setIds("10-a");

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhendsHasLeters() {

    uut.setIds("abc");

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenIdsHasNumberAndLeters() {

    uut.setIds("10-1,abc");

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void failValidationWhenIdsHasvalidpariOfIdsAndSemicolonAsSeperator() {

    uut.setIds("10-1;15-1");

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(1, constraintViolations.size());
  }

  @Test
  public void passValidationWhenIdsHasValidPairsAndColonAsSeperator() {

    uut.setIds("10-1,15-2");

    // do test
    Set<ConstraintViolation<CommunicationsSearchCriteria>> constraintViolations = validator.validate(uut);

    assertEquals(0, constraintViolations.size());
  }
}