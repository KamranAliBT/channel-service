/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodCreateValidatorTest.java 101521 2015-10-15 16:45:21Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.InvalidContractPeriodException;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.database.ContractPeriodDbDao;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

import static org.easymock.EasyMock.*;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ContractPeriodCreateValidatorTest {

  private ContractPeriodCreateValidator uut;

  private ChannelDbDao channelDaoMock;
  private ContractPeriodDbDao contractPeriodDaoMock;

  @Before
  public void setUp() throws Exception {
    //init mocks
    channelDaoMock = createMock(ChannelDbDao.class);
    contractPeriodDaoMock = createMock(ContractPeriodDbDao.class);

    //init unit under test
    uut = new ContractPeriodCreateValidator(contractPeriodDaoMock);

    //setup mocks
    expect(channelDaoMock.exists(anyLong())).andReturn(true);
    expect(contractPeriodDaoMock.contractPeriodOverlapsExisting(anyObject(ContractPeriod.class))).andReturn(false)
        .anyTimes();
    replay(channelDaoMock, contractPeriodDaoMock);
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with a start date after the end date to test
   * if an InvalidContractPeriodException will be thrown with the correct ErrorCode.
   *
   * @throws Exception
   */
  @Test
  public void testStartDateAfterEndDateShouldFailValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    contractPeriod.setStart(DateTime.now().plusDays(2).toDate());
    contractPeriod.setEnd(DateTime.now().plusDays(1).toDate());

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      //assert
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with a start date same as the end date to
   * test if an InvalidContractPeriodException will be thrown with the correct ErrorCode.
   *
   * @throws Exception
   */
  @Test
  public void testStartDateSameAsEndDateShouldFailValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    contractPeriod.setStart(DateTime.now().toDate());
    contractPeriod.setEnd(DateTime.now().toDate());

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      //assert
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with a null start date to test if a
   * InvalidContractPeriodException will be thrown with the correct ErrorCode.
   *
   * @throws Exception
   */
  @Test
  public void testNullStartDateShouldFailValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    contractPeriod.setStart(null);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      //assert
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_START.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with a start date that overlaps an existing
   * ContractPeriod (not the same as this one).
   * InvalidContractPeriodException should be thrown with the INVALID_CONTRACT_PERIOD ErrorCode.
   *
   * @throws Exception
   */
  @Test
  public void testOverlappingContractPeriodShouldFailValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    reset(contractPeriodDaoMock);
    expect(
        contractPeriodDaoMock.contractPeriodOverlapsExisting(contractPeriod)).andReturn(true);
    expect(contractPeriodDaoMock.exists(contractPeriod.getId(), contractPeriod.getChannelId())).andReturn(false);
    replay(contractPeriodDaoMock);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      //assert
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with a start date that overlaps an existing
   * ContractPeriod, without an ID. This case occurs when an overlapping contract period is POSTed
   * InvalidContractPeriodException should be thrown with the INVALID_CONTRACT_PERIOD ErrorCode.
   *
   * @throws Exception
   */
  @Test
  public void testNewOverlappingContractPeriodShouldFailValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    reset(contractPeriodDaoMock);
    expect(
        contractPeriodDaoMock.contractPeriodOverlapsExisting(contractPeriod)).andReturn(
        true);
    replay(contractPeriodDaoMock);

    //test
    try {
      //remove to ID so that this looks like a new contract period
      contractPeriod.setId(null);
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      //assert
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with a start date in the past to test if a
   * InvalidContractPeriodException will be thrown with the correct ErrorCode.
   *
   * @throws Exception
   */
  @Test
  public void testPastStartDateShouldFailValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    contractPeriod.setStart(DateTime.now().minusDays(1).toDate());

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      //assert
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_START.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with a null end date to test if a
   * InvalidContractPeriodException will be thrown with the correct ErrorCode.
   *
   * @throws Exception
   */
  @Test
  public void testNullEndDateShouldFailValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    contractPeriod.setEnd(null);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      //assert
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_END.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with a null contract period ID to test if a new
   * contract period passes the validation check that tests for existing contract periods.
   *
   * @throws Exception
   */
  @Test
  public void testNullContractPeriodIdShouldPassValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    contractPeriod.setId(null);

    //test
    uut.validate(contractPeriod);
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with an end date in the past to test if a
   * InvalidContractPeriodException will be thrown with the correct ErrorCode.
   *
   * @throws Exception
   */
  @Test
  public void testPastEndDateShouldFailValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    contractPeriod.setStart(DateTime.now().minusDays(2).toDate());
    contractPeriod.setEnd(DateTime.now().minusDays(1).toDate());

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      //assert
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_START.getName(), e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodCreateValidator#validate(ContractPeriod)} with a contract period that already exists.
   * Validation should fail because the contract period overlaps itself.
   *
   * @throws Exception
   */
  @Test
  public void testExistingContractPeriodShouldFailValidation() throws Exception {
    //setup
    ContractPeriod contractPeriod = createValidFutureContractPeriod();
    reset(contractPeriodDaoMock);
    expect(
        contractPeriodDaoMock.contractPeriodOverlapsExisting(contractPeriod)).andReturn(
        true);
    replay(contractPeriodDaoMock);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }

    verify(contractPeriodDaoMock);
  }

  /**
   * Create a sample ContractPeriod in the future.
   *
   * @return a ContractPeriod in the future
   */
  private ContractPeriod createValidFutureContractPeriod() {
    ContractPeriod contractPeriod = new ContractPeriod();
    contractPeriod.setStart(DateTime.now().plusDays(1).toDate());
    contractPeriod.setEnd(new DateTime().plusDays(2).toDate());
    contractPeriod.setChannelId(123l);
    contractPeriod.setId(456l);
    contractPeriod.setActive(true);
    return contractPeriod;
  }
}