package com.brighttalk.service.channel.business.domain.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.Date;

import org.junit.Test;

import com.brighttalk.common.user.User;

public class UserTest {

  private static final String USER_EMAIL_ADDRESS = "email@brighttalk.com";

  private static final Long USER_ID = 888L;

  private static final String USER_FIRST_NAME = "FirstName";

  private static final String USER_LAST_NAME = "LastName";

  private static final String USER_JOB_TITLE = "JobTitle";

  private static final String USER_COMPANY_NAME = "CompanyName";

  private static final String USER_EXTERNAL_ID = "ExternalId";

  private static final boolean USER_IS_COMPLETE = true;

  private static final boolean USER_IS_ACTIVE = true;

  private static final String USER_REALM_USER_ID = "RealmId";

  private static final String USER_TIME_ZONE = "TimeZone";

  private static final String USER_API_KEY = "ApiKey";

  private static final Date USER_CREATED = new Date();

  private static final Date USER_LAST_LOGIN = new Date();

  private static final String USER_TELEPHONE = "123456789";

  private static final String USER_LEVEL = "Level";

  private static final String USER_INDUSTRY = "Industry";

  private static final String USER_COMPANY_SIZE = "1-10";

  private static final String USER_STATE_REGION = "StateRegion";

  private static final String USER_COUNTRY = "Country";

  private User uut;

  @Test
  public void testFields() {

    // Set up
    uut = buildUser();

    // Assertions
    assertEquals(uut.getId(), USER_ID);
    assertEquals(uut.getExternalId(), USER_EXTERNAL_ID);
    assertEquals(uut.getIsComplete(), USER_IS_COMPLETE);
    assertEquals(uut.getIsActive(), USER_IS_ACTIVE);
    assertEquals(uut.getRealmUserId(), USER_REALM_USER_ID);
    assertEquals(uut.getFirstName(), USER_FIRST_NAME);
    assertEquals(uut.getLastName(), USER_LAST_NAME);
    assertEquals(uut.getEmail(), USER_EMAIL_ADDRESS);
    assertEquals(uut.getTimeZone(), USER_TIME_ZONE);
    assertEquals(uut.getApiKey(), USER_API_KEY);
    assertEquals(uut.getCreated(), USER_CREATED);
    assertEquals(uut.getLastLogin(), USER_LAST_LOGIN);
    assertEquals(uut.getTelephone(), USER_TELEPHONE);
    assertEquals(uut.getJobTitle(), USER_JOB_TITLE);
    assertEquals(uut.getLevel(), USER_LEVEL);
    assertEquals(uut.getCompanyName(), USER_COMPANY_NAME);
    assertEquals(uut.getIndustry(), USER_INDUSTRY);
    assertEquals(uut.getCompanySize(), USER_COMPANY_SIZE);
    assertEquals(uut.getStateRegion(), USER_STATE_REGION);
    assertEquals(uut.getCountry(), USER_COUNTRY);
  }

  @Test
  public void testEguals() {

    // Set up
    uut = buildUser();
    User user = buildUser();

    // Assertions
    assertEquals(uut, user);
  }

  @Test
  public void testEqualsWhenSameInstances() {

    // Set up
    uut = buildUser();
    User user = uut;

    // Assertions
    assertEquals(uut, user);
  }

  @Test
  public void testEgualsWhenDiferentInstances() {

    // Set up
    uut = buildUser();
    User user = buildUser();

    // Assertions
    assertEquals(uut, user);
  }

  @Test
  public void testEgualsWhenDiferent() {

    // Set up
    uut = buildUser();
    User user = new User();

    // Assertions
    assertNotSame(uut, user);
  }

  private User buildUser() {

    User user = new User();
    user.setId(USER_ID);
    user.setExternalId(USER_EXTERNAL_ID);
    user.setIsComplete(USER_IS_COMPLETE);
    user.setIsActive(USER_IS_ACTIVE);
    user.setRealmUserId(USER_REALM_USER_ID);
    user.setFirstName(USER_FIRST_NAME);
    user.setLastName(USER_LAST_NAME);
    user.setEmail(USER_EMAIL_ADDRESS);
    user.setTimeZone(USER_TIME_ZONE);
    user.setApiKey(USER_API_KEY);
    user.setCreated(USER_CREATED);
    user.setLastLogin(USER_LAST_LOGIN);
    user.setTelephone(USER_TELEPHONE);
    user.setJobTitle(USER_JOB_TITLE);
    user.setLevel(USER_LEVEL);
    user.setCompanyName(USER_COMPANY_NAME);
    user.setIndustry(USER_INDUSTRY);
    user.setCompanySize(USER_COMPANY_SIZE);
    user.setStateRegion(USER_STATE_REGION);
    user.setCountry(USER_COUNTRY);

    return user;
  }
}