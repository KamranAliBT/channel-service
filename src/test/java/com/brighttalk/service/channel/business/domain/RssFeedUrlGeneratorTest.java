/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RssFeedUrlGeneratorTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.Channel;

public class RssFeedUrlGeneratorTest {

  private static final Long CHANNEL_ID = 666l;
  
  private static final String CHANNEL_TAG = "{channelId}";
  
  private RssFeedUrlGenerator uut;
  
  @Before
  public void setUp() {
    
    uut = new RssFeedUrlGenerator();
  }
  
  @Test
  public void generate() {
    
    final String templateWithTag = "http://www.local.brighttalk.net/channel/{channelId}/rssFeed";
    uut.setChannelFeedRssUriTemplate( templateWithTag );
    
    Channel channel = new Channel( CHANNEL_ID );
    
    //do test
    String result = uut.generate( channel );
    
    final String expectedResult = templateWithTag.replace( CHANNEL_TAG, CHANNEL_ID.toString() );
    assertEquals( expectedResult, result );
  }
  
  @Test
  public void generateWithoutTags() {
    
    final String templateWithoutTag = "http://www.local.brighttalk.net/channel/rssFeed";
    uut.setChannelFeedRssUriTemplate( templateWithoutTag );
    
    Channel channel = new Channel( CHANNEL_ID );
    
    //do test
    String result = uut.generate( channel );
    
    final String expectedResult = templateWithoutTag;
    assertEquals( expectedResult, result );
  }
}