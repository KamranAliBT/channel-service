/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelFinderTest.java 70141 2013-10-23 15:46:02Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for {@link DefaultKeywords}.
 */
public class DefaultKeywordsTest {

  private static final String KEYWORDS_SEPARATOR = ",";
  private DefaultKeywords uut;

  @Before
  public void setUp() {
    uut = new DefaultKeywords();

  }

  @Test
  public void testClean() {
    String invalidString = "";
    String expected = "";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanOneTag() {
    String invalidString = "tag";
    String expected = "tag";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanOneTagWithComma() {
    String invalidString = "tag,";
    String expected = "tag";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanOneTagWithEndingSpace() {
    String invalidString = "tag ";
    String expected = "tag";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanOneTagWithQuotes() {
    String invalidString = "\"tag\",";
    String expected = "tag";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanOneTagWithSpace() {
    String invalidString = "\"tag 1\"";
    String expected = "tag 1";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTwoTagsCommaSeperated() {
    String invalidString = "tag1,tag2";
    String expected = "tag1,tag2";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTwoTagsSpaceSeperated() {
    String invalidString = "tag1 tag2";
    String expected = "tag1,tag2";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTwoTagsOneTagWithQuotesCommaSeperated() {
    String invalidString = "\"tag1\",tag2";
    String expected = "tag1,tag2";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTwoTagsOneTagInQuotesSpaceSeperated() {
    String invalidString = "\"tag1\" tag2";
    String expected = "tag1,tag2";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTwoTagsSecondTagInQuotes() {
    String invalidString = "tag1 \"tag2\"";
    String expected = "tag1,tag2";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTwoTagsBothInQuotes() {
    String invalidString = "\"tag1\" \"tag2\"";
    String expected = "tag1,tag2";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTwoTagsWithSpaces() {
    String invalidString = "\"tag 1\" \"tag 2\"";
    String expected = "tag 1,tag 2";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanThreeTagsWithSpaces() {
    String invalidString = "\"tag 1\" \"tag 2\" \"tag 3\"";
    String expected = "tag 1,tag 2,tag 3";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanThreeTagsWithSpacesCommaSeperated() {
    String invalidString = "\"tag 1\",\"tag 2\",\"tag 3\"";
    String expected = "tag 1,tag 2,tag 3";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanThreeTagsWithSpacesMixedSeperation() {
    String invalidString = "\"tag 1\" \"tag 2\",\"tag 3\"";
    String expected = "tag 1,tag 2,tag 3";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTagStarsWithComma() {
    String invalidString = ",tag1";
    String expected = "tag1";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTagStartsWithSpace() {
    String invalidString = " tag1";
    String expected = "tag1";
    Assert.assertEquals(expected, uut.clean(invalidString));
  }

  @Test
  public void testCleanTagWithMixedKeywords() {
    String invalidString = "one, two 2, \"three 3\", four five";
    String expected = "one, two,2, three 3, four,five";
    String result = uut.clean(invalidString);
    Assert.assertEquals(expected, result);
  }

  @Test
  public void testCleanTagWithMixedKeywords2() {
    String invalidString = "one, \"two 2\", \"\"three 3\"\", four,five";
    String expected = "one, two 2, three,3, four,five";
    String result = uut.clean(invalidString);
    Assert.assertEquals(expected, result);
  }

  @Test
  public void testAddQuotes() {
    String input = "";
    String expected = "";
    Assert.assertEquals(expected, uut.addQuotes(input));
  }

  @Test
  public void testAddQuotesOneTag() {
    String input = "tag1";
    String expected = "tag1";
    Assert.assertEquals(expected, uut.addQuotes(input));
  }

  @Test
  public void testAddQuotesOneTagWithSpace() {
    String input = "tag 1";
    String expected = "\"tag 1\"";
    Assert.assertEquals(expected, uut.addQuotes(input));
  }

  @Test
  public void testAddQuotesTwoTags() {
    String input = "tag1,tag2";
    String expected = "tag1, tag2";
    Assert.assertEquals(expected, uut.addQuotes(input));
  }

  @Test
  public void testAddQuotesTwoTagsWithSpaces() {
    String input = "tag 1,tag 2";
    String expected = "\"tag 1\", \"tag 2\"";
    Assert.assertEquals(expected, uut.addQuotes(input));
  }

  @Test
  public void testAddQuotesTwoTagsMixedSpaces() {
    String input = "tag1,tag 2";
    String expected = "tag1, \"tag 2\"";
    Assert.assertEquals(expected, uut.addQuotes(input));
  }

  @Test
  public void testAddQuotesMultipleTags() {
    String input = "tag1,tag 2,tag \"3,tag 44,tag.5";
    String expected = "tag1, \"tag 2\", \"tag \"3\", \"tag 44\", tag.5";
    Assert.assertEquals(expected, uut.addQuotes(input));
  }

  @Test
  public void testAddQuotesTagWithPadding() {
    String input = "          tag1         ";
    String expected = "tag1";
    Assert.assertEquals(expected, uut.addQuotes(input));
  }

  @Test
  public void testAddQuotesTagWithMixedTags() {
    String input = "one, two 2, \"three 3\", four";
    String expected = "one, \"two 2\", \"\"three 3\"\", four";
    String result = uut.addQuotes(input);
    Assert.assertEquals(expected, result);
  }

  @Test
  public void removeQuotesWithNullList() {
    // Set up
    List<String> keywords = null;

    // Expectations

    // Do test
    List<String> result = uut.removeQuotes(keywords);

    // Assertions
    assertNull(result);
  }

  @Test
  public void removeQuotesWithEmptyList() {
    // Set up
    List<String> keywords = new ArrayList<>();

    // Expectations

    // Do test
    List<String> result = uut.removeQuotes(keywords);

    // Assertions
    assertTrue(result.isEmpty());
  }

  @Test
  public void removeQuotesWhileNone() {
    // Set up
    List<String> keywords = new ArrayList<>();
    keywords.add("keyword");

    // Expectations
    String expectedKeyword = "keyword";

    // Do test
    List<String> result = uut.removeQuotes(keywords);

    // Assertions
    assertEquals(expectedKeyword, result.iterator().next());
  }

  @Test
  public void removeQuotesFromStart() {
    // Set up
    List<String> keywords = new ArrayList<>();
    keywords.add("\"keyword");

    // Expectations
    String expectedKeyword = "keyword";

    // Do test
    List<String> result = uut.removeQuotes(keywords);

    // Assertions
    assertEquals(expectedKeyword, result.iterator().next());
  }

  @Test
  public void removeQuotesFromEnd() {
    // Set up
    List<String> keywords = new ArrayList<>();
    keywords.add("keyword\"");

    // Expectations
    String expectedKeyword = "keyword";

    // Do test
    List<String> result = uut.removeQuotes(keywords);

    // Assertions
    assertEquals(expectedKeyword, result.iterator().next());
  }

  @Test
  public void removeAllQuotes() {
    // Set up
    List<String> keywords = new ArrayList<>();
    keywords.add("\"keyword\"");

    // Expectations
    String expectedKeyword = "keyword";

    // Do test
    List<String> result = uut.removeQuotes(keywords);

    // Assertions
    assertEquals(expectedKeyword, result.iterator().next());
  }

  @Test
  public void removeQuotesWithMixedTags() {
    // Set up
    List<String> keywords = new ArrayList<>();
    keywords.add("one");
    keywords.add("\"two");
    keywords.add("three\"");
    keywords.add("\"four\"");

    // Expectations
    String expectedOneKeyword = "one";
    String expectedTwoKeyword = "two";
    String expectedThreeKeyword = "three";
    String expectedFourKeyword = "four";

    // Do test
    List<String> result = uut.removeQuotes(keywords);

    // Assertions
    assertTrue(result.contains(expectedOneKeyword));
    assertTrue(result.contains(expectedTwoKeyword));
    assertTrue(result.contains(expectedThreeKeyword));
    assertTrue(result.contains(expectedFourKeyword));
  }

  // -------------------------------- Test Keywords conversion --------------------------------

  /**
   * Tests {@link DefaultKeywords#convertListOfKeywordsToKeywordsString} in the case of converting keywords string list
   * to keywords comma separated string.
   */
  @Test
  public void testConvertListOfKeywordsToKeywordsString() {
    // set up
    String expectedKeywords = "Keywords 1,Keywords 2,Keywords 3,Keywords 4";
    List<String> keywordsList = Arrays.asList(expectedKeywords.split(KEYWORDS_SEPARATOR));

    // do test
    String keywordsString = uut.convertListOfKeywordsToKeywordsString(keywordsList);

    // assertions
    assertEquals(expectedKeywords, keywordsString);
  }

  /**
   * Tests {@link DefaultKeywords#convertKeywordsStringToListOfKeywords} in the case of converting comma separated
   * keywords string to list of keywords.
   */
  @Test
  public void testConvertKeywordsStringToListOfKeywords() {
    // set up
    String expectedKeywords = "Keywords 1,Keywords 2,Keywords 3,Keywords 4";
    List<String> expectedKeywordsList = Arrays.asList(expectedKeywords.split(KEYWORDS_SEPARATOR));

    // do test
    List<String> keywordsList = uut.convertKeywordsStringToListOfKeywords(expectedKeywords);

    // assertions
    assertEquals(expectedKeywordsList, keywordsList);
  }

}
