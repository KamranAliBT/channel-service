/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelServiceTest.java 75381 2014-03-18 15:16:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import static org.easymock.EasyMock.createMock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.User.Role;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.business.domain.channel.ChannelUrlGenerator;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.validator.ChannelUpdateValidator;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.ChannelUpdateAuthorisationException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.queue.AuditQueue;
import com.brighttalk.service.channel.business.queue.CreateChannelQueue;
import com.brighttalk.service.channel.business.queue.DeleteChannelQueue;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.ChannelAuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.ContactDetailsAuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.booking.BookingServiceDao;
import com.brighttalk.service.channel.integration.booking.HDBookingServiceDao;
import com.brighttalk.service.channel.integration.community.CommunityServiceDao;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.database.ChannelTypeDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.ContactDetailsDbDao;
import com.brighttalk.service.channel.integration.database.SubscriptionDbDao;
import com.brighttalk.service.utils.DomainFactoryUtils;

/**
 * Unit tests for {@link DefaultChannelService}.
 */
public class DefaultChannelServiceTest {

  private static final Long USER_ID = 999L;
  private static final Long CHANNEL_ID = 5L;
  private static final String CHANNEL_TITLE = "Channel Test Title";

  private DefaultChannelService uut;

  private ChannelFinder mockChannelFinder;
  private ChannelDbDao mockChannelDbDao;
  private ChannelTypeDbDao mockChannelTypeDbDao;
  private SubscriptionDbDao mockSubscriptionDbDao;
  private CommunicationDbDao mockCommunicationDbDao;
  private CategoryDbDao mockCategoryDbDao;
  private ContactDetailsDbDao mockContactDetailsDbDao;

  private CommunityServiceDao mockCommunityServiceDao;
  private BookingServiceDao mockBookingServiceDao;
  private HDBookingServiceDao mockHDBookingServiceDao;

  private ChannelUrlGenerator mockChannelUrlGenerator;
  private ChannelUrlGenerator mockChannelAtomFeedUrlGenerator;
  private ChannelUrlGenerator mockChannelRssFeedUrlGenerator;

  private CreateChannelQueue mockCreateChannelQueue;
  private DeleteChannelQueue mockDeleteChannelQueue;

  private AuditQueue mockAuditQueue;
  private AuditRecordDtoBuilder mockChannelAuditRecordDtoBuilder;
  private AuditRecordDtoBuilder mockContactDetailsAuditRecordDtoBuilder;
  private ChannelUpdateValidator mockChannelUpdateValidator;

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Before
  public void setUp() {
    uut = new DefaultChannelService();

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockChannelDbDao = EasyMock.createMock(ChannelDbDao.class);
    ReflectionTestUtils.setField(uut, "channelDbDao", mockChannelDbDao);

    mockSubscriptionDbDao = EasyMock.createMock(SubscriptionDbDao.class);
    ReflectionTestUtils.setField(uut, "subscriptionDbDao", mockSubscriptionDbDao);

    mockCommunicationDbDao = EasyMock.createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDbDao);

    mockCategoryDbDao = EasyMock.createMock(CategoryDbDao.class);
    ReflectionTestUtils.setField(uut, "categoryDbDao", mockCategoryDbDao);

    mockContactDetailsDbDao = EasyMock.createMock(ContactDetailsDbDao.class);
    ReflectionTestUtils.setField(uut, "contactDetailsDbDao", mockContactDetailsDbDao);

    mockDeleteChannelQueue = EasyMock.createMock(DeleteChannelQueue.class);
    ReflectionTestUtils.setField(uut, "deleteChannelQueue", mockDeleteChannelQueue);

    mockCreateChannelQueue = EasyMock.createMock(CreateChannelQueue.class);
    ReflectionTestUtils.setField(uut, "createChannelQueue", mockCreateChannelQueue);

    mockCommunityServiceDao = EasyMock.createMock(CommunityServiceDao.class);
    ReflectionTestUtils.setField(uut, "communityServiceDao", mockCommunityServiceDao);

    mockBookingServiceDao = EasyMock.createMock(BookingServiceDao.class);
    ReflectionTestUtils.setField(uut, "bookingServiceDao", mockBookingServiceDao);

    mockHDBookingServiceDao = EasyMock.createMock(HDBookingServiceDao.class);
    ReflectionTestUtils.setField(uut, "hdBookingServiceDao", mockHDBookingServiceDao);

    mockChannelUrlGenerator = EasyMock.createMock(ChannelUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "channelUrlGenerator", mockChannelUrlGenerator);

    mockChannelAtomFeedUrlGenerator = EasyMock.createMock(ChannelUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "channelAtomFeedUrlGenerator", mockChannelAtomFeedUrlGenerator);

    mockChannelRssFeedUrlGenerator = EasyMock.createMock(ChannelUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "channelRssFeedUrlGenerator", mockChannelRssFeedUrlGenerator);

    mockChannelTypeDbDao = EasyMock.createMock(ChannelTypeDbDao.class);
    ReflectionTestUtils.setField(uut, "channelTypeDbDao", mockChannelTypeDbDao);

    mockChannelUpdateValidator = EasyMock.createMock(ChannelUpdateValidator.class);
    ReflectionTestUtils.setField(uut, "channelUpdateValidator", mockChannelUpdateValidator);

    mockChannelAuditRecordDtoBuilder = createMock(ChannelAuditRecordDtoBuilder.class);
    ReflectionTestUtils.setField(uut, "channelAuditRecordDtoBuilder", mockChannelAuditRecordDtoBuilder);

    mockContactDetailsAuditRecordDtoBuilder = createMock(ContactDetailsAuditRecordDtoBuilder.class);
    ReflectionTestUtils.setField(uut, "contactDetailsAuditRecordDtoBuilder", mockContactDetailsAuditRecordDtoBuilder);

    mockAuditQueue = createMock(AuditQueue.class);
    ReflectionTestUtils.setField(uut, "auditQueue", mockAuditQueue);
  }

  @Test(expected = UserAuthorisationException.class)
  public void notAuthorisedToDelete() {

    final User user = new User(USER_ID);
    List<Role> roles = new ArrayList<Role>();
    roles.add(Role.USER);
    user.setRoles(roles);

    uut.delete(user, CHANNEL_ID);
  }

  @Test
  public void delete() {

    final User user = new User(USER_ID);
    List<Role> roles = new ArrayList<Role>();
    roles.add(Role.MANAGER);
    user.setRoles(roles);

    AuditRecordDto auditRecordDto = new AuditRecordDto();

    EasyMock.expect(mockChannelDbDao.exists(CHANNEL_ID)).andReturn(true);
    mockChannelDbDao.delete(CHANNEL_ID);
    EasyMock.expectLastCall().once();

    mockDeleteChannelQueue.deleteChannel(CHANNEL_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(
        mockChannelAuditRecordDtoBuilder.delete(user.getId(), CHANNEL_ID, null, "User [" + user.getId()
            + "] requested a deletion of channel [" + CHANNEL_ID + "]")).andReturn(auditRecordDto);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();

    EasyMock.replay(mockChannelDbDao, mockSubscriptionDbDao, mockCommunicationDbDao, mockDeleteChannelQueue,
        mockChannelAuditRecordDtoBuilder, mockAuditQueue);

    // do test
    uut.delete(user, CHANNEL_ID);

    EasyMock.verify(mockChannelDbDao, mockSubscriptionDbDao, mockCommunicationDbDao, mockDeleteChannelQueue,
        mockChannelAuditRecordDtoBuilder, mockAuditQueue);
  }

  @Test(expected = NotFoundException.class)
  public void deleteInvalidChannelId() {

    final User user = new User(USER_ID);
    List<Role> roles = new ArrayList<Role>();
    roles.add(Role.MANAGER);
    user.setRoles(roles);

    EasyMock.expect(mockChannelDbDao.exists(CHANNEL_ID)).andReturn(false);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockChannelDbDao, mockSubscriptionDbDao, mockCommunicationDbDao, mockDeleteChannelQueue);

    // do test
    uut.delete(user, CHANNEL_ID);

    EasyMock.verify(mockChannelDbDao, mockSubscriptionDbDao, mockCommunicationDbDao, mockDeleteChannelQueue);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void deleteSubscriptions() {

    mockSubscriptionDbDao.unsubscribeUsers(CHANNEL_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDbDao.delete(EasyMock.anyObject(List.class));
    EasyMock.expectLastCall().once();
    EasyMock.expect(mockCommunicationDbDao.findAll(CHANNEL_ID)).andReturn(new ArrayList<Communication>());

    EasyMock.replay(mockSubscriptionDbDao, mockCommunicationDbDao);

    // do test
    uut.deleteCallback(CHANNEL_ID);

    EasyMock.verify(mockSubscriptionDbDao, mockCommunicationDbDao);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void deleteCommunities() {

    final Long communicationId = 123L;
    final List<Communication> communicationsToBeDeleted = new ArrayList<Communication>();
    final Communication communication = new Communication();
    communication.setId(communicationId);
    communication.setProvider(new Provider(Provider.MEDIAZONE));
    communicationsToBeDeleted.add(communication);

    EasyMock.expect(mockCommunicationDbDao.findAll(CHANNEL_ID)).andReturn(communicationsToBeDeleted);
    mockCommunicationDbDao.delete(EasyMock.anyObject(List.class));

    mockCommunityServiceDao.deleteChannel(CHANNEL_ID);
    EasyMock.expectLastCall().once();

    mockCommunityServiceDao.deleteCommunications(EasyMock.anyObject(Long.class), EasyMock.anyObject(List.class));
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDbDao, mockCommunityServiceDao);

    // do test
    uut.deleteCallback(CHANNEL_ID);

    // verification
    EasyMock.verify(mockCommunicationDbDao, mockCommunityServiceDao);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void deleteBookings() {

    final Long communicationId = 123L;
    final List<Communication> communicationsToBeDeleted = new ArrayList<Communication>();
    final Communication communication = new Communication();
    communication.setId(communicationId);
    communication.setProvider(new Provider(Provider.BRIGHTTALK));
    communicationsToBeDeleted.add(communication);

    EasyMock.expect(mockCommunicationDbDao.findAll(CHANNEL_ID)).andReturn(communicationsToBeDeleted);
    mockCommunicationDbDao.delete(EasyMock.anyObject(List.class));

    mockBookingServiceDao.deleteBookings(EasyMock.anyObject(List.class));
    EasyMock.expectLastCall();

    mockHDBookingServiceDao.deleteBookings(CHANNEL_ID);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationDbDao, mockBookingServiceDao, mockHDBookingServiceDao);

    // do test
    uut.deleteCallback(CHANNEL_ID);

    // verification
    EasyMock.verify(mockCommunicationDbDao, mockBookingServiceDao, mockHDBookingServiceDao);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void deleteBookingsWhenNoBrighttalkCommunications() {

    final Long communicationId = 123L;
    final List<Communication> communicationsToBeDeleted = new ArrayList<Communication>();
    final Communication communication = new Communication();
    communication.setId(communicationId);
    communication.setProvider(new Provider(Provider.MEDIAZONE));
    communicationsToBeDeleted.add(communication);

    EasyMock.expect(mockCommunicationDbDao.findAll(CHANNEL_ID)).andReturn(communicationsToBeDeleted);
    mockCommunicationDbDao.delete(EasyMock.anyObject(List.class));

    mockBookingServiceDao.deleteBookings(EasyMock.anyObject(List.class));
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDbDao, mockBookingServiceDao);

    // do test
    uut.deleteCallback(CHANNEL_ID);

    // verification
    EasyMock.verify(mockCommunicationDbDao, mockBookingServiceDao);
  }

  @Test
  public void create() {

    final Channel channel = new Channel();
    channel.setTitle(CHANNEL_TITLE);

    final String channelTypeName = "premium";
    final ChannelType channelType = new ChannelType();
    channelType.setName(channelTypeName);
    channel.setCreatedType(channelType);

    final Long createdChannelId = 1L;
    final Channel createdChannel = new Channel(createdChannelId);

    EasyMock.expect(mockChannelDbDao.create(channel)).andReturn(createdChannel);
    EasyMock.expect(mockChannelDbDao.titleInUse(CHANNEL_TITLE)).andReturn(false);

    EasyMock.expect(mockChannelTypeDbDao.findDefault()).andReturn(new ChannelType());
    EasyMock.expect(mockChannelTypeDbDao.find(channelTypeName)).andReturn(new ChannelType());

    mockCreateChannelQueue.createChannel(createdChannelId);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockChannelDbDao, mockChannelTypeDbDao, mockCreateChannelQueue, mockContactDetailsDbDao);

    // do test
    Channel result = uut.create(new User(), channel);

    assertEquals(createdChannel, result);

    // validate
    EasyMock.verify(mockChannelDbDao, mockChannelTypeDbDao, mockCreateChannelQueue, mockContactDetailsDbDao);
  }

  @Test
  public void createWithContactDetails() {

    final Channel channel = new Channel();
    channel.setTitle(CHANNEL_TITLE);

    final String channelTypeName = "premium";
    final ChannelType channelType = new ChannelType();
    channelType.setName(channelTypeName);
    channel.setCreatedType(channelType);

    final ContactDetails contactDetails = new ContactDetails();

    final Long createdChannelId = 1L;
    final Channel createdChannel = new Channel(createdChannelId);

    EasyMock.expect(mockChannelDbDao.create(channel)).andReturn(createdChannel);
    EasyMock.expect(mockChannelDbDao.titleInUse(CHANNEL_TITLE)).andReturn(false);

    EasyMock.expect(mockChannelTypeDbDao.findDefault()).andReturn(new ChannelType());
    EasyMock.expect(mockChannelTypeDbDao.find(channelTypeName)).andReturn(new ChannelType());

    mockCreateChannelQueue.createChannel(createdChannelId);
    EasyMock.expectLastCall().once();

    mockContactDetailsDbDao.create(createdChannelId, contactDetails);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockChannelDbDao, mockChannelTypeDbDao, mockCreateChannelQueue, mockContactDetailsDbDao);

    // do test
    Channel result = uut.create(new User(), channel, contactDetails);

    assertEquals(createdChannel, result);

    // validate
    EasyMock.verify(mockChannelDbDao, mockChannelTypeDbDao, mockCreateChannelQueue, mockContactDetailsDbDao);
  }

  @Test
  public void updateWhenUserIsChannelOwner() {
    // Set up
    final User user = new User(1L);
    final AuditRecordDto auditRecordDto = new AuditRecordDto();

    final Channel updateChannel = new Channel(CHANNEL_ID);
    updateChannel.setTitle(CHANNEL_TITLE);

    final Channel existingChannel = new Channel(CHANNEL_ID);
    existingChannel.setTitle(CHANNEL_TITLE);
    existingChannel.setOwnerUserId(user.getId());

    // Expectations
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(existingChannel);
    EasyMock.expect(mockChannelDbDao.update(existingChannel)).andReturn(existingChannel);
    mockChannelUpdateValidator.validate(existingChannel);
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelAuditRecordDtoBuilder.update(user.getId(), existingChannel.getId(), null, null)).andReturn(
        auditRecordDto);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelFinder, mockChannelUpdateValidator, mockChannelDbDao, mockChannelAuditRecordDtoBuilder,
        mockAuditQueue);

    // Do test
    Channel result = uut.update(user, updateChannel);

    // Assertions
    assertNotNull(result);
    assertEquals(updateChannel, result);
  }

  @Test
  public void updateWhenUserIsBrightTalkManager() {

    // Set up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);

    final User user = new User(1L);
    user.setRoles(roles);

    final Channel updateChannel = new Channel(CHANNEL_ID);
    updateChannel.setTitle(CHANNEL_TITLE);
    updateChannel.setOwnerUserId(user.getId());

    final Channel existingChannel = new Channel(CHANNEL_ID);
    existingChannel.setTitle(CHANNEL_TITLE);

    final AuditRecordDto auditRecordDto = new AuditRecordDto();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(existingChannel);
    EasyMock.expect(mockChannelDbDao.update(existingChannel)).andReturn(existingChannel);
    mockChannelUpdateValidator.validate(existingChannel);
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelAuditRecordDtoBuilder.update(user.getId(), existingChannel.getId(), null, null)).andReturn(
        auditRecordDto);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelFinder, mockChannelUpdateValidator, mockChannelDbDao, mockChannelAuditRecordDtoBuilder,
        mockAuditQueue);

    // Do test
    Channel result = uut.update(user, updateChannel);

    // Assertions
    assertNotNull(result);
    assertEquals(updateChannel, result);
  }

  @Test
  public void updateWhenUserIsChannelManager() {

    // Set up
    final Long ownerUserId = 2L;
    final User user = new User(1L);
    final User channelManagerUser = new User();
    channelManagerUser.setId(1L);

    ChannelManager channelManager = new ChannelManager();
    channelManager.setUser(channelManagerUser);
    final List<ChannelManager> channelManagers = new ArrayList<>();
    channelManagers.add(channelManager);

    final Feature channelManagersFeature = new Feature(Type.CHANNEL_MANAGERS);
    channelManagersFeature.setIsEnabled(Boolean.TRUE);
    channelManagersFeature.setValue("5");
    final List<Feature> features = new ArrayList<>();
    features.add(channelManagersFeature);

    final Channel updateChannel = new Channel(CHANNEL_ID);
    updateChannel.setTitle(CHANNEL_TITLE);

    final Channel existingChannel = new Channel(CHANNEL_ID);
    existingChannel.setTitle(CHANNEL_TITLE);
    existingChannel.setOwnerUserId(ownerUserId);
    existingChannel.setFeatures(features);
    existingChannel.setChannelManagers(channelManagers);

    final AuditRecordDto auditRecordDto = new AuditRecordDto();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(existingChannel);
    EasyMock.expect(mockChannelDbDao.update(existingChannel)).andReturn(existingChannel);
    mockChannelUpdateValidator.validate(existingChannel);
    EasyMock.expectLastCall();
    EasyMock.expect(mockChannelAuditRecordDtoBuilder.update(user.getId(), existingChannel.getId(), null, null)).andReturn(
        auditRecordDto);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelFinder, mockChannelUpdateValidator, mockChannelDbDao, mockChannelAuditRecordDtoBuilder,
        mockAuditQueue);

    // Do test
    Channel result = uut.update(user, updateChannel);

    // Assertions
    assertNotNull(result);
    assertEquals(updateChannel, result);
  }

  @Test(expected = ChannelUpdateAuthorisationException.class)
  public void updateWhenUserIsNotAuthorisedToUpdateOwnerId() {

    // Set up
    final User user = new User(1L);

    final Channel updateChannel = new Channel(CHANNEL_ID);
    updateChannel.setTitle(CHANNEL_TITLE);
    updateChannel.setOwnerUserId(user.getId());

    final Channel existingChannel = new Channel(CHANNEL_ID);
    existingChannel.setTitle(CHANNEL_TITLE);

    // Expectations
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(existingChannel);
    EasyMock.expect(mockChannelDbDao.update(existingChannel)).andReturn(existingChannel);
    mockChannelUpdateValidator.validate(existingChannel);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelFinder, mockChannelUpdateValidator, mockChannelDbDao);

    // Do test
    Channel result = uut.update(user, updateChannel);

    // Assertions
    assertNotNull(result);
    assertEquals(updateChannel, result);
  }

  @Test(expected = UserAuthorisationException.class)
  public void updateWhenUserIsNotAuthorised() {

    // Set up
    final User user = new User(1L);
    final Long ownerUserId = 2L;

    final List<ChannelManager> channelManagers = new ArrayList<>();

    final Feature channelManagersFeature = new Feature(Type.CHANNEL_MANAGERS);
    channelManagersFeature.setIsEnabled(Boolean.TRUE);
    channelManagersFeature.setValue("5");
    final List<Feature> features = new ArrayList<>();
    features.add(channelManagersFeature);

    final Channel updateChannel = new Channel(CHANNEL_ID);
    updateChannel.setTitle(CHANNEL_TITLE);

    final Channel existingChannel = new Channel(CHANNEL_ID);
    existingChannel.setTitle(CHANNEL_TITLE);
    existingChannel.setOwnerUserId(ownerUserId);
    existingChannel.setFeatures(features);
    existingChannel.setChannelManagers(channelManagers);

    // Expectations
    EasyMock.expect(mockChannelFinder.find(CHANNEL_ID)).andReturn(existingChannel);
    EasyMock.expect(mockChannelDbDao.update(existingChannel)).andReturn(existingChannel);
    mockChannelUpdateValidator.validate(existingChannel);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelFinder, mockChannelUpdateValidator, mockChannelDbDao);

    // Do test
    Channel result = uut.update(user, updateChannel);

    // Assertions
    assertNotNull(result);
    assertEquals(updateChannel, result);
  }

  @Test
  public void unsubscribeUserFromChannel() {

    final User user = new User(USER_ID);

    mockSubscriptionDbDao.unsubscribeUser(CHANNEL_ID, USER_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockSubscriptionDbDao);

    // do test
    uut.unsubscribeUserFromChannel(user, CHANNEL_ID);

    EasyMock.verify(mockSubscriptionDbDao);
  }

  @Test
  public void unsubscribeUserFromChannels() {
    final User user = new User(USER_ID);

    mockSubscriptionDbDao.unsubscribeUser(USER_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockSubscriptionDbDao);

    // do test
    uut.unsubscribeUserFromChannels(user);

    EasyMock.verify(mockSubscriptionDbDao);
  }

  /**
   * Tests {@link DefaultChannelService#getContactDetails(User, Long)} when the specified channel does not exist. This
   * will result in a {@link ChannelNotFoundException} being thrown.
   */
  @Test
  public void getContactDetailsWhenChannelDoesNotExist() {

    // Set up
    User user = new User(USER_ID);

    Long channelId = 1L;

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andThrow(new ChannelNotFoundException(channelId));
    EasyMock.replay(mockChannelFinder, mockChannelDbDao);

    expectedException.expect(ChannelNotFoundException.class);

    // Do test
    uut.getContactDetails(user, channelId);

    // Assertions
  }

  /**
   * Tests {@link DefaultChannelService#getContactDetails(User, Long)} when the specified user is not authorised to
   * access contact details for the channel. This will result in a {@link UserAuthorisationException} being thrown.
   */
  @Test
  public void getContactDetailsWhenUserNotAuthorised() {

    // Set up
    User user = new User(USER_ID);
    // Owner user ID is different to the requesting user's ID
    Long ownerUserId = USER_ID + 1;

    // Set up the channel managers feature in the channel
    Feature channelManagersFeature = new Feature(Type.CHANNEL_MANAGERS);
    channelManagersFeature.setIsEnabled(Boolean.FALSE);
    channelManagersFeature.setValue("5");
    List<Feature> features = Arrays.asList(channelManagersFeature);

    Long channelId = 1L;
    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(ownerUserId);
    channel.setFeatures(features);

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao);

    expectedException.expect(UserAuthorisationException.class);

    // Do test
    uut.getContactDetails(user, channelId);

    // Assertions
  }

  /**
   * Tests {@link DefaultChannelService#getContactDetails(User, Long)} when the specified user is a BrightTalk manager
   * and is authorised to access contact details for the channel.
   */
  @Test
  public void getContactDetailsWhenUserIsBrightTalkManager() {

    // Set up
    User user = new User(USER_ID);
    user.setRoles(Arrays.asList(Role.MANAGER));

    Long channelId = 1L;
    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(user.getId());

    ContactDetails contactDetails = new ContactDetails();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.expect(mockContactDetailsDbDao.get(channelId)).andReturn(contactDetails);
    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao);

    // Do test
    ContactDetails result = uut.getContactDetails(user, channelId);

    // Assertions
    EasyMock.verify(mockChannelFinder, mockContactDetailsDbDao);
    assertContactDetailsAreEqual(contactDetails, result);
  }

  /**
   * Tests {@link DefaultChannelService#getContactDetails(User, Long)} when the specified user is a channel manager and
   * is authorised to access contact details for the channel.
   */
  @Test
  public void getContactDetailsWhenUserIsChannelManager() {

    // Set up
    User channelOwner = new User(USER_ID);

    // Set up the channel managers for the channel
    User channelManagerUser = new User(USER_ID + 1);
    ChannelManager channelManager = new ChannelManager();
    channelManager.setUser(channelManagerUser);
    List<ChannelManager> channelManagers = Arrays.asList(channelManager);

    // Set up the channel managers feature in the channel
    Feature channelManagersFeature = new Feature(Type.CHANNEL_MANAGERS);
    channelManagersFeature.setIsEnabled(Boolean.TRUE);
    channelManagersFeature.setValue("5");
    List<Feature> features = Arrays.asList(channelManagersFeature);

    Long channelId = 1L;
    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(channelOwner.getId());
    channel.setFeatures(features);
    channel.setChannelManagers(channelManagers);

    ContactDetails contactDetails = new ContactDetails();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.expect(mockContactDetailsDbDao.get(channelId)).andReturn(contactDetails);
    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao);

    // Do test
    ContactDetails result = uut.getContactDetails(channelManagerUser, channelId);

    // Assertions
    EasyMock.verify(mockChannelFinder, mockContactDetailsDbDao);
    assertContactDetailsAreEqual(contactDetails, result);
  }

  /**
   * Tests {@link DefaultChannelService#getContactDetails(Long)} when the user is the channel owner, and contact details
   * are found in the DAO.
   */
  @Test
  public void getContactDetailsWhenUserIsChannelOwner() {

    // Set up
    User user = new User(USER_ID);

    Long channelId = 1L;
    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(user.getId());

    ContactDetails contactDetails = new ContactDetails();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.expect(mockContactDetailsDbDao.get(channelId)).andReturn(contactDetails);
    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao);

    // Do test
    ContactDetails result = uut.getContactDetails(user, channelId);

    // Assertions
    EasyMock.verify(mockChannelFinder, mockContactDetailsDbDao);
    assertContactDetailsAreEqual(contactDetails, result);
  }

  /**
   * Tests {@link DefaultChannelService#getContactDetails(User, Long)} when no contact details are returned from the
   * DAO.
   * 
   * Empty ContactDetails will be returned.
   */
  @Test
  public void getContactDetailsWhenNoneExist() {

    // Set up
    User user = new User(USER_ID);
    user.setRoles(Arrays.asList(Role.MANAGER));

    Long channelId = 1L;
    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(user.getId());

    // Expected contact details are blank
    ContactDetails expectedContactDetails = new ContactDetails();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.expect(mockContactDetailsDbDao.get(channelId)).andReturn(expectedContactDetails);
    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao);

    // Do test
    ContactDetails result = uut.getContactDetails(user, channelId);

    // Assertions
    EasyMock.verify(mockChannelFinder, mockContactDetailsDbDao);
    assertContactDetailsAreEqual(expectedContactDetails, result);
  }

  /**
   * Tests {@link DefaultChannelService#updateContactDetails(User, Long, ContactDetails)} when the specified channel
   * does not exist. This will result in a {@link ChannelNotFoundException} being thrown.
   */
  @Test
  public void updateContactDetailsWhenChannelDoesNotExist() {

    // Set up
    User user = new User(USER_ID);
    ContactDetails newContactDetails = DomainFactoryUtils.createContactDetails();

    Long channelId = 1L;

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andThrow(new ChannelNotFoundException(channelId));
    EasyMock.replay(mockChannelFinder, mockChannelDbDao);

    expectedException.expect(ChannelNotFoundException.class);

    // Do test
    uut.updateContactDetails(user, channelId, newContactDetails);

    // Assertions
  }

  /**
   * Tests {@link DefaultChannelService#updateContactDetails(User, Long, ContactDetails)} when the specified user is not
   * authorised to update contact details for the channel. This will result in a {@link UserAuthorisationException}
   * being thrown.
   */
  @Test
  public void updateContactDetailsWhenUserNotAuthorised() {

    // Set up
    User user = new User(USER_ID);
    // Owner user ID is different to the requesting user's ID
    Long ownerUserId = USER_ID + 1;

    // Set up the channel managers feature in the channel
    Feature channelManagersFeature = new Feature(Type.CHANNEL_MANAGERS);
    channelManagersFeature.setIsEnabled(Boolean.FALSE);
    channelManagersFeature.setValue("5");
    List<Feature> features = Arrays.asList(channelManagersFeature);

    Long channelId = 1L;
    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(ownerUserId);
    channel.setFeatures(features);

    ContactDetails newContactDetails = DomainFactoryUtils.createContactDetails();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao);

    expectedException.expect(UserAuthorisationException.class);

    // Do test
    uut.updateContactDetails(user, channelId, newContactDetails);

    // Assertions
  }

  /**
   * Tests {@link DefaultChannelService#updateContactDetails(User, Long, ContactDetails)} when the specified user is a
   * BrightTalk manager and is authorised to update contact details for the channel. In addition to an update, an audit
   * record of the event will be generated.
   */
  @Test
  public void updateContactDetailsWhenUserIsBrightTalkManager() {

    // Set up
    User user = new User(USER_ID);
    user.setRoles(Arrays.asList(Role.MANAGER));

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(user.getId());

    ContactDetails newContactDetails = DomainFactoryUtils.createContactDetails();

    AuditRecordDto auditRecordDto = new AuditRecordDto();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channel.getId())).andReturn(channel);
    EasyMock.expect(mockContactDetailsDbDao.update(channel.getId(), newContactDetails)).andReturn(newContactDetails);

    EasyMock.expect(mockContactDetailsAuditRecordDtoBuilder.update(user.getId(), channel.getId(), null, null)).andReturn(
        auditRecordDto);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();

    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao, mockContactDetailsAuditRecordDtoBuilder, mockAuditQueue);

    // Do test
    ContactDetails result = uut.updateContactDetails(user, channel.getId(), newContactDetails);

    // Assertions
    EasyMock.verify(mockChannelFinder, mockContactDetailsDbDao, mockContactDetailsAuditRecordDtoBuilder, mockAuditQueue);
    assertContactDetailsAreEqual(newContactDetails, result);
  }

  /**
   * Tests {@link DefaultChannelService#updateContactDetails(User, Long, ContactDetails)} when the specified user is a
   * channel manager and is authorised to update contact details for the channel. In addition to an update, an audit
   * record of the event will be generated.
   */
  @Test
  public void updateContactDetailsWhenUserIsChannelManager() {

    // Set up
    User channelOwner = new User(USER_ID);

    // Set up the channel managers for the channel
    User channelManagerUser = new User(USER_ID + 1);
    ChannelManager channelManager = new ChannelManager();
    channelManager.setUser(channelManagerUser);
    List<ChannelManager> channelManagers = Arrays.asList(channelManager);

    // Set up the channel managers feature in the channel
    Feature channelManagersFeature = new Feature(Type.CHANNEL_MANAGERS);
    channelManagersFeature.setIsEnabled(Boolean.TRUE);
    channelManagersFeature.setValue("5");
    List<Feature> features = Arrays.asList(channelManagersFeature);

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(channelOwner.getId());
    channel.setFeatures(features);
    channel.setChannelManagers(channelManagers);

    ContactDetails newContactDetails = DomainFactoryUtils.createContactDetails();

    AuditRecordDto auditRecordDto = new AuditRecordDto();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channel.getId())).andReturn(channel);
    EasyMock.expect(mockContactDetailsDbDao.update(channel.getId(), newContactDetails)).andReturn(newContactDetails);

    EasyMock.expect(
        mockContactDetailsAuditRecordDtoBuilder.update(channelManagerUser.getId(), channel.getId(), null, null)).andReturn(
        auditRecordDto);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();

    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao, mockContactDetailsAuditRecordDtoBuilder, mockAuditQueue);

    // Do test
    ContactDetails result = uut.updateContactDetails(channelManagerUser, channel.getId(), newContactDetails);

    // Assertions
    EasyMock.verify(mockChannelFinder, mockContactDetailsDbDao, mockContactDetailsAuditRecordDtoBuilder, mockAuditQueue);
    assertContactDetailsAreEqual(newContactDetails, result);
  }

  /**
   * Tests {@link DefaultChannelService#updateContactDetails(User, Long, ContactDetails)} when the user is the channel
   * owner and is attempting to update the contact details for the channel. In addition to an update, an audit record of
   * the event will be generated.
   */
  @Test
  public void updateContactDetailsWhenUserIsChannelOwner() {

    // Set up
    User user = new User(USER_ID);

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(user.getId());

    ContactDetails newContactDetails = DomainFactoryUtils.createContactDetails();

    AuditRecordDto auditRecordDto = new AuditRecordDto();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channel.getId())).andReturn(channel);
    EasyMock.expect(mockContactDetailsDbDao.update(channel.getId(), newContactDetails)).andReturn(newContactDetails);

    EasyMock.expect(mockContactDetailsAuditRecordDtoBuilder.update(user.getId(), channel.getId(), null, null)).andReturn(
        auditRecordDto);
    mockAuditQueue.create(auditRecordDto);
    EasyMock.expectLastCall();

    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao, mockContactDetailsAuditRecordDtoBuilder, mockAuditQueue);

    // Do test
    ContactDetails result = uut.updateContactDetails(user, channel.getId(), newContactDetails);

    // Assertions
    EasyMock.verify(mockChannelFinder, mockContactDetailsDbDao, mockContactDetailsAuditRecordDtoBuilder, mockAuditQueue);
    assertContactDetailsAreEqual(newContactDetails, result);
  }

  /**
   * Tests {@link DefaultChannelService#updateContactDetails(User, Long, ContactDetails)} when an
   * {@link ApplicationException} is thrown by the DAO.
   */
  @Test
  public void updateContactDetailsWhenUnexpectedErrorOccursInDao() {

    // Set up
    User user = new User(USER_ID);

    Long channelId = 1L;
    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(user.getId());

    ContactDetails newContactDetails = DomainFactoryUtils.createContactDetails();

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.expect(mockContactDetailsDbDao.update(channelId, newContactDetails)).andThrow(
        new ApplicationException("Could not update contact details for channel [" + channelId + "]"));
    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao);

    expectedException.expect(ApplicationException.class);

    // Do test
    uut.updateContactDetails(user, channelId, newContactDetails);

    // Assertions
  }

  /**
   * Tests {@link DefaultChannelService#updateContactDetails(User, Long, ContactDetails)} when no contact details are
   * returned from the DAO.
   * 
   * An Application
   */
  @Test
  public void updateContactDetailsWhenNoneExist() {

    // Set up
    User user = new User(USER_ID);
    user.setRoles(Arrays.asList(Role.MANAGER));

    Long channelId = 1L;
    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(user.getId());

    // Expectations
    EasyMock.expect(mockChannelFinder.find(channelId)).andReturn(channel);
    EasyMock.expect(mockContactDetailsDbDao.get(channelId)).andReturn(null); // DAO returns no contact details
    EasyMock.replay(mockChannelFinder, mockContactDetailsDbDao);

    // Do test
    ContactDetails result = uut.getContactDetails(user, channelId);

    // Assertions
    EasyMock.verify(mockChannelFinder, mockContactDetailsDbDao);
    assertNull(result);
  }

  private void assertContactDetailsAreEqual(ContactDetails expected, ContactDetails actual) {

    assertEquals(expected.getFirstName(), actual.getFirstName());
    assertEquals(expected.getLastName(), actual.getLastName());
    assertEquals(expected.getEmail(), actual.getEmail());
    assertEquals(expected.getTelephone(), actual.getTelephone());
    assertEquals(expected.getJobTitle(), actual.getJobTitle());
    assertEquals(expected.getCompany(), actual.getCompany());
    assertEquals(expected.getAddress1(), actual.getAddress1());
    assertEquals(expected.getAddress2(), actual.getAddress2());
    assertEquals(expected.getCity(), actual.getCity());
    assertEquals(expected.getState(), actual.getState());
    assertEquals(expected.getPostcode(), actual.getPostcode());
    assertEquals(expected.getCountry(), actual.getCountry());
  }
}
