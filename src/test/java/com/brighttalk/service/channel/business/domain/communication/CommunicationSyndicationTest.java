/**
 * *****************************************************************************
 * * Copyright BrightTALK Ltd, 2009-2010.
 * * All Rights Reserved.
 * * Id: CommunicationSyndicationTest.java
 * *****************************************************************************
 * @package com.brighttalk.service.channel.business.domain
 */
package com.brighttalk.service.channel.business.domain.communication;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationAuthorisationStatus;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationType;

/**
 * Unit tests for {@link CommunicationSyndication}.
 */
public class CommunicationSyndicationTest {

  private CommunicationSyndication uut;

  @Before
  public void setUp() {
    uut = new CommunicationSyndication();
  }

  @After
  public void tearDown() {

  }

  @Test
  public void testIsSyndicatedReturnsTrueForSyndicatedTypeIn() {
    uut.setSyndicationType(CommunicationSyndication.SyndicationType.IN);
    assertTrue("incorrect syndication value", uut.isSyndicated());
  }

  @Test
  public void testIsSyndicatedReturnsTrueForSyndicatedTypeOut() {
    uut.setSyndicationType(CommunicationSyndication.SyndicationType.IN);
    assertTrue("incorrect syndication value", uut.isSyndicated());
  }

  @Test
  public void testIsSyndicatedReturnsFalseForSyndicatedTypeNone() {
    SyndicationType testType = null;
    uut.setSyndicationType(testType);
    assertFalse("incorrect syndication value", uut.isSyndicated());
  }

  public void testIsSyndicatedInReturnsTrueForSyndicatedTypeIn() {
    uut.setSyndicationType(CommunicationSyndication.SyndicationType.IN);
    assertTrue("incorrect syndication value", uut.isSyndicatedIn());
  }

  public void testIsSyndicatedInReturnsFalseForSyndicatedTypeOut() {
    uut.setSyndicationType(CommunicationSyndication.SyndicationType.OUT);
    assertFalse("incorrect syndication value", uut.isSyndicatedIn());
  }

  public void testIsSyndicatedInReturnsFalseForSyndicatedTypeNone() {
    SyndicationType testType = null;
    uut.setSyndicationType(testType);
    assertFalse("incorrect syndication value", uut.isSyndicatedIn());
  }

  public void testIsSyndicatedOutReturnsTrueForSyndicatedTypeOut() {
    uut.setSyndicationType(CommunicationSyndication.SyndicationType.OUT);
    assertTrue("incorrect syndication value", uut.isSyndicatedOut());
  }

  public void testIsSyndicatedOutReturnsFalseForSyndicatedTypeIn() {
    uut.setSyndicationType(CommunicationSyndication.SyndicationType.IN);
    assertFalse("incorrect syndication value", uut.isSyndicatedOut());
  }

  public void testIsSyndicatedOutReturnsFalseForSyndicatedTypeNone() {
    SyndicationType testType = null;
    uut.setSyndicationType(testType);
    assertFalse("incorrect syndication value", uut.isSyndicatedOut());
  }

  public void testIsFullyAuthorisedReturnsTrueForFullyAuthorisedCommunication() {
    uut.setMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    uut.setConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    assertTrue("incorrect syndication authorization", uut.isFullyAuthorisedForSyndication());
  }

  public void testIsFullyAuthorisedReturnsFalseForNotAuthorisedCommunication() {
    SyndicationAuthorisationStatus authorisationStatus = null;
    uut.setMasterChannelSyndicationAuthorisationStatus(authorisationStatus);
    uut.setConsumerChannelSyndicationAuthorisationStatus(authorisationStatus);
    assertFalse("incorrect syndication authorization", uut.isFullyAuthorisedForSyndication());
  }

  public void testIsFullyAuthorisedReturnsFalseForPartiallyAuthorisedCommunicationPendingMaster() {
    uut.setMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    uut.setConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    assertFalse("incorrect syndication authorization", uut.isFullyAuthorisedForSyndication());
  }

  public void testIsFullyAuthorisedReturnsFalseForPartiallyAuthorisedCommunicationPendingConsumer() {
    uut.setMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    uut.setConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    assertFalse("incorrect syndication authorization", uut.isFullyAuthorisedForSyndication());
  }

  public void testIsFullyAuthorisedReturnsFalseForPartiallyAuthorisedCommunicationDeclinedMaster() {
    uut.setMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    uut.setConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    assertFalse("incorrect syndication authorization", uut.isFullyAuthorisedForSyndication());
  }

  public void testIsFullyAuthorisedReturnsFalseForPartiallyAuthorisedCommunicationDeclinedConsumer() {
    uut.setMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    uut.setConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    assertFalse("incorrect syndication authorization", uut.isFullyAuthorisedForSyndication());
  }

  public void testIsFullyAuthorisedReturnsFalseForPartiallyAuthorisedCommunicationPendingMasterConsumer() {
    uut.setMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    uut.setConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    assertFalse("incorrect syndication authorization", uut.isFullyAuthorisedForSyndication());
  }

  public void testIsFullyAuthorisedReturnsFalseForPartiallyAuthorisedCommunicationDeclinedMasterConsumer() {
    uut.setMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    uut.setConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    assertFalse("incorrect syndication authorization", uut.isFullyAuthorisedForSyndication());
  }
}
