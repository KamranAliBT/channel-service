/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodDeleteValidatorTest.java 101321 2015-10-09 16:19:44Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.InvalidContractPeriodException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests {@link ContractPeriodDeleteValidator} for correct validation for contract periods about to be deleted
 */
public class ContractPeriodDeleteValidatorTest {

  //Unit under test
  private ContractPeriodDeleteValidator uut;

  //Test objects
  private ContractPeriod contractPeriod;

  @Before
  public void setUp() throws Exception {
    uut = new ContractPeriodDeleteValidator();
    contractPeriod = new ContractPeriod();
  }

  /**
   * Tests {@link ContractPeriodDeleteValidator#validate(ContractPeriod)} with a {@link ContractPeriod} that has start
   * and end dates in the future. This should pass validation
   * @throws Exception
   */
  @Test
  public void testContractPeriodInTheFutureCanBeDeleted() throws Exception {
    //Setup
    contractPeriod.setStart(DateTime.now().plusWeeks(1).toDate());
    contractPeriod.setEnd(DateTime.now().plusMonths(1).toDate());

    //Test
    uut.validate(contractPeriod);
  }

  /**
   * Tests {@link ContractPeriodDeleteValidator#validate(ContractPeriod)} with a {@link ContractPeriod} that has start
   * and end dates in the past. A InvalidContractPeriodException should be thrown.
   * @throws Exception
   */
  @Test
  public void testContractPeriodCompletelyInThePastCannotBeDeleted() throws Exception {
    //Setup
    contractPeriod.setStart(DateTime.now().minusMonths(1).toDate());
    contractPeriod.setEnd(DateTime.now().minusWeeks(1).toDate());

    //Test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.CONTRACT_PERIOD_INELIGIBLE_FOR_DELETION.getName(),e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link ContractPeriodDeleteValidator#validate(ContractPeriod)}with a {@link ContractPeriod} that has start
   * date in the past and an end date in the future. A InvalidContractPeriodException should be thrown.
   * @throws Exception
   */
  @Test
  public void testContractPeriodWhichIsCurrentCannotDeleted() throws Exception {
    //Setup
    contractPeriod.setStart(DateTime.now().minusMonths(1).toDate());
    contractPeriod.setEnd(DateTime.now().plusMonths(1).toDate());

    //Test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.CONTRACT_PERIOD_INELIGIBLE_FOR_DELETION.getName(),e.getUserErrorCode());
    }
  }
}