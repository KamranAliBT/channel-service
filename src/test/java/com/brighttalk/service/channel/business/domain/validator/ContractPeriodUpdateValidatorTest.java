/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodUpdateValidatorTest.java 101520 2015-10-15 16:29:24Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.ContractPeriodNotFoundException;
import com.brighttalk.service.channel.business.error.InvalidContractPeriodException;
import com.brighttalk.service.channel.integration.database.ContractPeriodDbDao;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

/**
 * Tests {@link ContractPeriodUpdateValidator} for the validation of a contract period prior to getting updated
 */
public class ContractPeriodUpdateValidatorTest {

  //Unit under test
  private ContractPeriodUpdateValidator uut;

  // Mocks
  private ContractPeriodDbDao contractPeriodDaoMock;

  //Test objects
  private ContractPeriod contractPeriod;
  private ContractPeriod oldContractPeriod;

  @Before
  public void setUp() throws Exception {
    contractPeriodDaoMock = createMock(ContractPeriodDbDao.class);

    //init unit under test
    uut = new ContractPeriodUpdateValidator(contractPeriodDaoMock);

    contractPeriod = new ContractPeriod(123l);
    contractPeriod.setChannelId(456l);
    contractPeriod.setStart(DateTime.now().minusDays(10).toDate());
    oldContractPeriod = new ContractPeriod(contractPeriod.getId());
    oldContractPeriod.setChannelId(contractPeriod.getChannelId());
  }

  /**
   * Tests {@link ContractPeriodUpdateValidator#validate(ContractPeriod)} to verify if an
   * {@link InvalidContractPeriodException} is thrown for contract periods that do not exist for their channels (or if
   * the channel itself is deactivated)
   *
   * @throws Exception
   * @see ContractPeriodDbDao#exists(Long, Long)
   */
  @Test
  public void testValidateUpdateContractPeriodFailsIfContractPeriodDoesNotExistForChannel() throws Exception {
    //setup
    contractPeriod.setStart(DateTime.now().minusDays(1).toDate());
    contractPeriod.setEnd(DateTime.now().plusDays(1).toDate());
    expect(contractPeriodDaoMock.find(contractPeriod.getId())).andThrow(
        new ContractPeriodNotFoundException(contractPeriod.getId()));
    replay(contractPeriodDaoMock);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected a ContractPeriodNotFoundException");
    } catch (ContractPeriodNotFoundException e) {
      assertEquals(ErrorCode.NOT_FOUND.getName(), e.getUserErrorCode());
      assertEquals(contractPeriod.getId(), e.getContractPeriodId());
    }

    //verify
    verify(contractPeriodDaoMock);
  }

  /**
   * Tests {@link ContractPeriodUpdateValidator#validate(ContractPeriod)} to verify if an
   * {@link InvalidContractPeriodException} is thrown for contract periods have expired
   *
   * @throws Exception
   */
  @Test
  public void testValidateUpdateContractPeriodFailsIfContractPeriodHasExpired() throws Exception {
    //setup
    contractPeriod.setEnd(DateTime.now().minusDays(9).toDate());
    replay(contractPeriodDaoMock);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected a InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_END.getName(), e.getUserErrorCode());
    }

    //verify
    verify(contractPeriodDaoMock);
  }

  /**
   * Tests {@link ContractPeriodUpdateValidator#validate(ContractPeriod)} to verify if an
   * {@link InvalidContractPeriodException} is thrown because the channel ID has changed between the old contract period
   * and the new updated one
   *
   * @throws Exception
   */
  @Test
  public void testValidateUpdateContractPeriodFailsIfChannelIdHasChanged() throws Exception {
    //setup
    contractPeriod.setEnd(DateTime.now().plusDays(9).toDate());

    ContractPeriod oldContractPeriod = new ContractPeriod(contractPeriod.getId());
    oldContractPeriod.setChannelId(789l);
    oldContractPeriod.setStart(contractPeriod.getStart());
    oldContractPeriod.setEnd(contractPeriod.getEnd());
    expect(contractPeriodDaoMock.find(contractPeriod.getId())).andReturn(oldContractPeriod);
    replay(contractPeriodDaoMock);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected a InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }

    //verify
    verify(contractPeriodDaoMock);
  }

  /**
   * Tests {@link ContractPeriodUpdateValidator#validate(ContractPeriod)} to verify if an
   * {@link InvalidContractPeriodException} is thrown because the existing contract period has already expired
   *
   * @throws Exception
   */
  @Test
  public void testValidateUpdateContractPeriodFailsIfExistingContractPeriodHasExpired() throws Exception {
    //setup
    contractPeriod.setEnd(DateTime.now().plusDays(9).toDate());

    ContractPeriod oldContractPeriod = new ContractPeriod(contractPeriod.getId());
    oldContractPeriod.setChannelId(contractPeriod.getChannelId());
    oldContractPeriod.setStart(new DateTime().minusDays(10).toDate());
    oldContractPeriod.setEnd(new DateTime().minusDays(1).toDate());
    expect(contractPeriodDaoMock.find(contractPeriod.getId())).andReturn(oldContractPeriod);
    replay(contractPeriodDaoMock);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected a InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }

    //verify
    verify(contractPeriodDaoMock);
  }

  /**
   * Tests {@link ContractPeriodUpdateValidator#validate(ContractPeriod)} to verify if an
   * {@link InvalidContractPeriodException} is thrown because the new start date is after the new end date
   *
   * @throws Exception
   */
  @Test
  public void testValidateUpdateContractPeriodFailsIfEndDateIsBeforeStartDate() throws Exception {
    //setup
    contractPeriod.setStart(DateTime.now().plusDays(10).toDate());
    contractPeriod.setEnd(DateTime.now().plusDays(1).toDate());

    oldContractPeriod.setStart(contractPeriod.getStart());
    oldContractPeriod.setEnd(contractPeriod.getEnd());
    expect(contractPeriodDaoMock.find(contractPeriod.getId())).andReturn(oldContractPeriod);
    replay(contractPeriodDaoMock);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected a InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD.getName(), e.getUserErrorCode());
    }

    //verify
    verify(contractPeriodDaoMock);
  }

  /**
   * Tests {@link ContractPeriodUpdateValidator#validate(ContractPeriod)} with a valid contract period that is in the
   * future and exists for its channel
   *
   * @throws Exception
   */
  @Test
  public void testValidateUpdateContractPeriodPassesForValidContractPeriod() throws Exception {
    //setup
    contractPeriod.setStart(DateTime.now().plusDays(10).toDate());
    contractPeriod.setEnd(DateTime.now().plusDays(20).toDate());
    expect(contractPeriodDaoMock.find(contractPeriod.getId())).andReturn(contractPeriod);
    expect(
        contractPeriodDaoMock.contractPeriodOverlapsExisting(contractPeriod)).andReturn(
        false);
    replay(contractPeriodDaoMock);

    //test
    uut.validate(contractPeriod);

    //verify
    verify(contractPeriodDaoMock);
  }

  /**
   * Tests {@link ContractPeriodUpdateValidator#validate(ContractPeriod)} in the case where the contract period is due
   * to end today and we want to extend it
   *
   * @throws Exception
   */
  @Test
  public void testValidateUpdateContractPeriodExtensionForContractPeriodEndingToday() throws Exception {
    //setup
    contractPeriod.setEnd(DateTime.now().plusDays(1).withTimeAtStartOfDay().toDate());//ending tonight midnight
    oldContractPeriod.setStart(contractPeriod.getStart());
    oldContractPeriod.setEnd(new DateTime().plusDays(10).toDate());

    expect(contractPeriodDaoMock.find(contractPeriod.getId())).andReturn(oldContractPeriod);
    expect(
        contractPeriodDaoMock.contractPeriodOverlapsExisting(contractPeriod)).andReturn(
        false);
    replay(contractPeriodDaoMock);

    //test
    uut.validate(contractPeriod);

    //verify
    verify(contractPeriodDaoMock);
  }

  /**
   * Tests {@link ContractPeriodUpdateValidator#validate(ContractPeriod)} in the case where the contract period is due
   * to end today and we want to extend it but we also move the start date forwards. This should fail.
   *
   * @throws Exception
   */
  @Test
  public void testValidateUpdateContractPeriodExtensionForContractPeriodEndingTodayWithModifiedStartDate()
      throws Exception {
    //setup
    contractPeriod.setEnd(DateTime.now().plusDays(1).withTimeAtStartOfDay().toDate());//ending tonight midnight

    oldContractPeriod.setStart(new DateTime().plusDays(1).toDate());
    oldContractPeriod.setEnd(new DateTime().plusDays(10).toDate());

    expect(contractPeriodDaoMock.find(contractPeriod.getId())).andReturn(oldContractPeriod);
    replay(contractPeriodDaoMock);

    //test
    try {
      uut.validate(contractPeriod);
      fail("Expected InvalidContractPeriodException");
    } catch (InvalidContractPeriodException e) {
      assertEquals(ErrorCode.INVALID_CONTRACT_PERIOD_START.getName(), e.getUserErrorCode());
    }

    //verify
    verify(contractPeriodDaoMock);
  }
}