/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationServiceTest.java 72595 2014-01-15 13:37:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.easymock.EasyMock.createMock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.User.Role;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationStatisticsBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationSyndicationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationAuthorisationStatus;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndications;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.database.CommunicationConfigurationDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationOverrideDbDao;
import com.brighttalk.service.channel.integration.database.statistics.CommunicationStatisticsDbDao;
import com.brighttalk.service.channel.integration.database.syndication.SyndicationDbDao;
import com.brighttalk.service.channel.presentation.error.InvalidWebcastException;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;

/**
 * Unit tests for {@link CommunicationSyndicationService}.
 */
public class DefaultCommunicationSyndicationServiceTest {
  private static final Long USER_ID = 3L;
  private static final long MASTER_CHANNEL_ID = 86723l;
  private static final long CONSUMER_CHANNEL_ID1 = 43545l;
  private static final long CONSUMER_CHANNEL_ID2 = 87878l;
  private static final long COMM_ID = 12457l;
  private static final String TEST_MASTER_COMMUNICATION_TITLE = "Test Master Comm Title";
  private static final String TEST_CONSUMER_COMMUNICATION_TITLE = "Test Consumer Comm Title";

  private CommunicationDbDao mockCommunicationDao;
  private CommunicationConfigurationDbDao mockCommunicationConfigurationDbDao;
  private CommunicationOverrideDbDao mockCommunicationOverrideDbDao;
  private ChannelFinder mockChannelFinder;
  private CommunicationStatisticsDbDao mockCommunicationStatisticsDbDao;
  private SyndicationDbDao mockSyndicationDbDao;
  private CommunicationSyndicationBuilder communicationSyndicationBuilder;
  private CommunicationStatisticsBuilder communicationStatisticsBuilder;

  private DefaultCommunicationSyndicationService uut;

  @Before
  public void setUp() {

    uut = new DefaultCommunicationSyndicationService();

    mockCommunicationDao = createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDao);

    mockCommunicationConfigurationDbDao = createMock(CommunicationConfigurationDbDao.class);
    ReflectionTestUtils.setField(uut, "configurationDbDao", mockCommunicationConfigurationDbDao);

    mockCommunicationOverrideDbDao = createMock(CommunicationOverrideDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationOverrideDbDao", mockCommunicationOverrideDbDao);

    mockChannelFinder = createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockCommunicationStatisticsDbDao = createMock(CommunicationStatisticsDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationStatisticsDbDao", mockCommunicationStatisticsDbDao);

    mockSyndicationDbDao = createMock(SyndicationDbDao.class);
    ReflectionTestUtils.setField(uut, "syndicationDbDao", mockSyndicationDbDao);

    communicationSyndicationBuilder = new CommunicationSyndicationBuilder();
    communicationStatisticsBuilder = new CommunicationStatisticsBuilder();
  }

  /**
   * Tests {@link DefaultCommunicationSyndicationService#get} in the success case of retrieving communication
   * syndication for master channel.
   */
  @Test
  public void getSyndicationForMasterChannel() {
    // Setup -
    Channel masterChannel = new Channel(MASTER_CHANNEL_ID);
    Communication communication = new Communication(COMM_ID);
    communication.setTitle(TEST_MASTER_COMMUNICATION_TITLE);
    communication.setChannelId(MASTER_CHANNEL_ID);
    communication.setMasterChannelId(MASTER_CHANNEL_ID);
    // 1st consumer channel syndication details
    Channel consumerChannel1 = new Channel(CONSUMER_CHANNEL_ID1);
    CommunicationSyndication communicationSyndication1 = communicationSyndicationBuilder.withDefaultValues().build();
    communicationSyndication1.setChannel(consumerChannel1);
    CommunicationStatistics communicationStatistics1 = communicationStatisticsBuilder.withDefaultValues().build();
    communicationStatistics1.setChannelId(CONSUMER_CHANNEL_ID1);
    communicationSyndication1.setCommunicationStatistics(communicationStatistics1);

    // 2nd consumer channel syndication details
    Channel consumerChannel2 = new Channel(CONSUMER_CHANNEL_ID2);
    CommunicationSyndication communicationSyndication2 = communicationSyndicationBuilder.withDefaultValues().build();
    communicationSyndication2.setChannel(consumerChannel2);
    CommunicationStatistics communicationStatistics2 = communicationStatisticsBuilder.withDefaultValues().build();
    communicationStatistics2.setChannelId(CONSUMER_CHANNEL_ID2);
    communicationSyndication2.setCommunicationStatistics(communicationStatistics2);
    List<CommunicationSyndication> communicationSyndications = Arrays.asList(communicationSyndication1,
        communicationSyndication2);
    List<CommunicationStatistics> communicationsStatistics = Arrays.asList(communicationStatistics1,
        communicationStatistics2);
    List<Long> channelIds = Arrays.asList(CONSUMER_CHANNEL_ID1, CONSUMER_CHANNEL_ID2);

    // Expectation
    EasyMock.expect(mockSyndicationDbDao.findConsumerChannelsSyndication(COMM_ID)).andReturn(communicationSyndications);
    EasyMock.expect(mockCommunicationStatisticsDbDao.findCommunicationStatisticsByChannelIds(COMM_ID, channelIds)).
        andReturn(communicationsStatistics);
    EasyMock.replay(mockSyndicationDbDao, mockCommunicationStatisticsDbDao);

    // Do Test.
    CommunicationSyndications actualCommunicationSyndications = this.uut.get(masterChannel, communication);
    EasyMock.verify(mockSyndicationDbDao, mockCommunicationStatisticsDbDao);

    // Assertion
    assertNotNull(actualCommunicationSyndications);
    Channel actualMasterChannel = actualCommunicationSyndications.getChannel();
    assertNotNull(actualMasterChannel);
    assertEquals(masterChannel.getId(), actualMasterChannel.getId());
    Communication actualMasterCommunication = actualCommunicationSyndications.getCommunication();
    assertNotNull(actualMasterCommunication);
    assertEquals(communication.getTitle(), actualMasterCommunication.getTitle());

    List<CommunicationSyndication> actualCommunicationSyndicationList =
        actualCommunicationSyndications.getCommunicationSyndications();
    assertNotNull(actualCommunicationSyndicationList);
    assertEquals(2, actualCommunicationSyndicationList.size());
    // Assert the reported communications syndication list has Syndication Authorisation Status and communication
    // statistics
    for (CommunicationSyndication communicationSyndication : actualCommunicationSyndicationList) {
      assertNotNull(communicationSyndication);
      assertEquals(SyndicationAuthorisationStatus.APPROVED,
          communicationSyndication.getMasterChannelSyndicationAuthorisationStatus());
      assertEquals(SyndicationAuthorisationStatus.APPROVED,
          communicationSyndication.getConsumerChannelSyndicationAuthorisationStatus());
      assertNotNull(communicationSyndication.getCommunicationStatistics());
    }
  }

  /**
   * Tests {@link DefaultCommunicationSyndicationService#get} in the success case of retrieving communication
   * syndication for consumer channel.
   */
  @Test
  public void getSyndicationForConsumerChannel() {
    // Setup -
    Channel masterChannel = new Channel(MASTER_CHANNEL_ID);
    // Master channel communication.
    Communication masterCommunication = new Communication(COMM_ID);
    masterCommunication.setTitle(TEST_MASTER_COMMUNICATION_TITLE);
    masterCommunication.setChannelId(MASTER_CHANNEL_ID);
    masterCommunication.setMasterChannelId(MASTER_CHANNEL_ID);

    // Consumer channel communication.
    Communication consumerCommunication = new Communication(COMM_ID);
    consumerCommunication.setTitle(TEST_CONSUMER_COMMUNICATION_TITLE);
    consumerCommunication.setChannelId(CONSUMER_CHANNEL_ID1);
    consumerCommunication.setMasterChannelId(MASTER_CHANNEL_ID);

    // 1st consumer channel syndication details
    Channel consumerChannel = new Channel(CONSUMER_CHANNEL_ID1);
    CommunicationSyndication communicationSyndication = communicationSyndicationBuilder.withDefaultValues().build();
    communicationSyndication.setChannel(consumerChannel);
    List<CommunicationSyndication> communicationSyndications = Arrays.asList(communicationSyndication);

    // Expectation
    EasyMock.expect(mockChannelFinder.find(EasyMock.isA(Long.class))).andReturn(masterChannel);
    EasyMock.expect(mockCommunicationDao.find(EasyMock.isA(Long.class), EasyMock.isA(Long.class))).andReturn(
        masterCommunication);
    EasyMock.expect(mockSyndicationDbDao.findConsumerChannelsSyndication(COMM_ID)).andReturn(communicationSyndications);
    EasyMock.replay(mockChannelFinder, mockCommunicationDao, mockSyndicationDbDao);

    // Do Test.
    CommunicationSyndications actualCommunicationSyndications = this.uut.get(consumerChannel, consumerCommunication);
    EasyMock.verify(mockChannelFinder, mockCommunicationDao, mockSyndicationDbDao);

    // Assertion
    assertNotNull(actualCommunicationSyndications);
    Channel actualMasterChannel = actualCommunicationSyndications.getChannel();
    assertNotNull(actualMasterChannel);
    assertEquals(masterChannel.getId(), actualMasterChannel.getId());
    Communication actualMasterCommunication = actualCommunicationSyndications.getCommunication();
    assertNotNull(actualMasterCommunication);
    assertEquals(masterCommunication.getTitle(), actualMasterCommunication.getTitle());

    List<CommunicationSyndication> actualCommunicationSyndicationList =
        actualCommunicationSyndications.getCommunicationSyndications();
    assertNotNull(actualCommunicationSyndicationList);
    assertEquals(1, actualCommunicationSyndicationList.size());
    // Assert the reported communications syndication has Syndication Authorisation Status and no communication
    // statistics
    CommunicationSyndication actualCommunicationSyndication = actualCommunicationSyndicationList.get(0);
    assertNotNull(actualCommunicationSyndication);
    assertEquals(SyndicationAuthorisationStatus.APPROVED,
        actualCommunicationSyndication.getMasterChannelSyndicationAuthorisationStatus());
    assertEquals(SyndicationAuthorisationStatus.APPROVED,
        actualCommunicationSyndication.getConsumerChannelSyndicationAuthorisationStatus());
    assertNull(actualCommunicationSyndication.getCommunicationStatistics());
  }

  @Test(expected = InvalidWebcastException.class)
  public void updateFailsSyndicatedCommunicationDataValidation() {

    // set up
    User user = new User();
    Channel channel = new ChannelBuilder().withDefaultValues().build();
    Communication communication = getCommunication(PublishStatus.UNPUBLISHED);
    communication.setPublishStatusHasChanged(true);

    // do test
    uut.update(user, channel, communication);
  }

  @Test(expected = NotAuthorisedException.class)
  public void updateFailsWhenUserIsNotChannelOwnerNorManagerNorChannelManager() {

    // set up
    User user = new User();

    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Channel channel = new ChannelBuilder().withDefaultValues().build();
    channel.setFeatures(features);

    Communication communication = getCommunication(PublishStatus.PUBLISHED);

    // do test
    uut.update(user, channel, communication);
  }

  @Test(expected = FeatureNotEnabledException.class)
  public void updateFailsWhenSyndicationOverridesFeatureIsNotEnabled() {

    // set up
    User user = new User(USER_ID);

    List<Feature> features = getChannelFeaturesForSyndicationOverride(false);

    Channel channel = new ChannelBuilder().withDefaultValues().withFeatures(features).withOwnerUserId(USER_ID).build();

    Communication communication = getCommunication(PublishStatus.PUBLISHED);

    // do test
    uut.update(user, channel, communication);
  }

  @Test
  public void updateWhenUserIsChannelOwnerSuccess() {

    // set up
    User user = new User(USER_ID);

    List<Feature> features = getChannelFeaturesForSyndicationOverride(true);

    Channel channel = new ChannelBuilder().withDefaultValues().withFeatures(features).withOwnerUserId(USER_ID).build();

    Communication communication = getCommunication(PublishStatus.PUBLISHED);

    // Expectations
    mockCommunicationConfigurationDbDao.update(communication.getConfiguration());
    EasyMock.expectLastCall();

    EasyMock.expect(mockCommunicationDao.find(communication.getChannelId(), communication.getId())).andReturn(
        communication);

    mockCommunicationOverrideDbDao.updateCommunicationDetailsOverride(communication);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationConfigurationDbDao, mockCommunicationDao, mockCommunicationOverrideDbDao);

    // do test
    uut.update(user, channel, communication);

    // Assertions
    EasyMock.verify(mockCommunicationConfigurationDbDao, mockCommunicationDao, mockCommunicationOverrideDbDao);
  }

  @Test
  public void updateWhenUserIsChannelManager() {

    // set up
    Long channelManagerUserId = 123L;
    User user = new User(channelManagerUserId);

    List<Feature> features = getChannelFeaturesForSyndicationOverride(true);
    features.add(getChannelManagerFeature(true));

    Channel channel =
        new ChannelBuilder().withDefaultValues().withFeatures(features).withOwnerUserId(USER_ID).withChannelManagerUserId(
            channelManagerUserId).build();

    Communication communication = getCommunication(PublishStatus.PUBLISHED);

    // Expectations
    mockCommunicationConfigurationDbDao.update(communication.getConfiguration());
    EasyMock.expectLastCall();

    EasyMock.expect(mockCommunicationDao.find(communication.getChannelId(), communication.getId())).andReturn(
        communication);

    mockCommunicationOverrideDbDao.updateCommunicationDetailsOverride(communication);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationConfigurationDbDao, mockCommunicationDao, mockCommunicationOverrideDbDao);

    // do test
    uut.update(user, channel, communication);

    // Assertions
    EasyMock.verify(mockCommunicationConfigurationDbDao, mockCommunicationDao, mockCommunicationOverrideDbDao);
  }

  @Test
  public void updateWhenUserIsManagerAndFeatureIsDisabledSuccess() {

    // set up
    User user = new User(USER_ID);
    user.addRole(Role.MANAGER);

    List<Feature> features = getChannelFeaturesForSyndicationOverride(false);

    Channel channel = new ChannelBuilder().withDefaultValues().withFeatures(features).withOwnerUserId(USER_ID).build();

    Communication communication = getCommunication(PublishStatus.PUBLISHED);

    // Expectations
    mockCommunicationConfigurationDbDao.update(communication.getConfiguration());
    EasyMock.expectLastCall();

    EasyMock.expect(mockCommunicationDao.find(communication.getChannelId(), communication.getId())).andReturn(
        communication);

    mockCommunicationOverrideDbDao.updateCommunicationDetailsOverride(communication);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationConfigurationDbDao, mockCommunicationDao, mockCommunicationOverrideDbDao);

    // do test
    uut.update(user, channel, communication);

    // Assertions
    EasyMock.verify(mockCommunicationConfigurationDbDao, mockCommunicationDao, mockCommunicationOverrideDbDao);
  }

  private Communication getCommunication(final PublishStatus publishStatus) {

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.RECORDED).withPublishStatus(
            publishStatus).build();

    return communication;
  }

  private List<Feature> getChannelFeaturesForSyndicationOverride(final Boolean isEnabled) {
    Feature syndicationOverrideFeature = new Feature(Type.SYNDICATION_OVERRIDES);
    syndicationOverrideFeature.setIsEnabled(isEnabled);

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationOverrideFeature);
    return features;
  }

  private Feature getChannelManagerFeature(final Boolean isEnabled) {
    Feature channelManagerFeature = new Feature(Type.CHANNEL_MANAGERS);
    channelManagerFeature.setIsEnabled(isEnabled);
    channelManagerFeature.setValue("5");
    return channelManagerFeature;
  }

}