/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ImageConverterSettingsBuilder.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.builder;

import com.brighttalk.service.channel.business.domain.ImageConverterSettings;
import com.brighttalk.service.channel.business.domain.ImageConverterSettings.Status;

public class ImageConverterSettingsBuilder {

  public static final String DEFAULT_SOURCE = "DefaultImageConverterSettingsBuilderSource";
  public static final String DEFAULT_DESTINATION = "DefaultImageConverterSettingsBuilderDestination";
  public static final String DEFAULT_NOTIFY = "DefaultImageConverterSettingsBuilderNotify";
  public static final String DEFAULT_FORMAT = "image/png";
  public static final Integer DEFAULT_WIDTH = 640;
  public static final Integer DEFAULT_HEIGHT = 360;
  public static final Status DEFAULT_STATUS = Status.PENDING;
  public static final String DEFAULT_SCALE_MODE = "DefaultImageConverterSettingsBuilderScaleMode";

  /** source of the image (san, aws...) */
  private String source;

  /** destination of the file once has been completed. */
  private String destination;

  /** url to notify back once the process has been completed. */
  private String notify;

  /** format of the image. */
  private String format;

  /** width of the image. */
  private Integer width;

  /** height of the image. */
  private Integer height;

  /** The status of this task. E.g. Pending, Complete etc. */
  private ImageConverterSettings.Status status;

  /** Scale mode of the image. */
  private String scaleMode;

  // this may still miss few properties. Current is a minimum needed to insert communication into db
  public ImageConverterSettingsBuilder withDefaultValues() {

    source = DEFAULT_SOURCE;
    destination = DEFAULT_DESTINATION;
    notify = DEFAULT_NOTIFY;
    format = DEFAULT_FORMAT;
    width = DEFAULT_WIDTH;
    height = DEFAULT_HEIGHT;
    status = DEFAULT_STATUS;
    scaleMode = DEFAULT_SCALE_MODE;

    return this;
  }

  public ImageConverterSettingsBuilder withSource(final String value) {

    source = value;
    return this;
  }

  public ImageConverterSettingsBuilder withDestination(final String value) {

    destination = value;
    return this;
  }

  public ImageConverterSettingsBuilder withNotify(final String value) {

    notify = value;
    return this;
  }

  public ImageConverterSettingsBuilder withFormat(final String value) {

    format = value;
    return this;
  }

  public ImageConverterSettingsBuilder withWidth(final Integer value) {

    width = value;
    return this;
  }

  public ImageConverterSettingsBuilder withHeight(final Integer value) {

    height = value;
    return this;
  }

  public ImageConverterSettingsBuilder withStatus(final Status value) {

    status = value;
    return this;
  }

  public ImageConverterSettingsBuilder withScaleMode(final String value) {

    scaleMode = value;
    return this;
  }

  public ImageConverterSettings build() {

    ImageConverterSettings imageConverterSettings = new ImageConverterSettings();
    imageConverterSettings.setSource(source);
    imageConverterSettings.setDestination(destination);
    imageConverterSettings.setNotify(notify);
    imageConverterSettings.setFormat(format);
    imageConverterSettings.setWidth(width);
    imageConverterSettings.setHeight(height);
    imageConverterSettings.setStatus(status);
    imageConverterSettings.setScaleMode(scaleMode);

    return imageConverterSettings;
  }
}