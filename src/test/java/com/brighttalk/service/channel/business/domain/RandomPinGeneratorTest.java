/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationFinderTest.java 70061 2013-10-21 16:34:03Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertNotNull;
import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.domain.RandomNumberPinGenerator;
import com.brighttalk.service.channel.integration.database.PinGeneratorDbDao;

/**
 * Unit tests for {@link RandomNumberPinGenerator}.
 */
public class RandomPinGeneratorTest {

  private static final int EXPECTED_LENGTH = 8;

  private RandomNumberPinGenerator uut;

  private PinGeneratorDbDao mockPinManagerDbDao;

  @Before
  public void setUp() {
    uut = new RandomNumberPinGenerator();

    mockPinManagerDbDao = EasyMock.createMock(PinGeneratorDbDao.class);
    ReflectionTestUtils.setField(uut, "pinManagerDbDao", mockPinManagerDbDao);
  }

  @Test
  public void generatePinNumber_NotInUse() {
    // setup
    boolean inUse = false;

    // expectations
    EasyMock.expect(mockPinManagerDbDao.isPinUsed(isA(String.class))).andReturn(inUse);
    EasyMock.replay(mockPinManagerDbDao);
    // expectations

    // do test
    String pinNumber = uut.generatePinNumber();

    // assertions
    assertNotNull(pinNumber);
    Assert.assertEquals(EXPECTED_LENGTH, pinNumber.length());
    EasyMock.verify(mockPinManagerDbDao);
  }

  @Test
  public void generatePinNumber_InUse() {
    // setup
    boolean inUse = true;

    // expectations
    EasyMock.expect(mockPinManagerDbDao.isPinUsed(isA(String.class))).andReturn(inUse).once();
    EasyMock.expect(mockPinManagerDbDao.isPinUsed(isA(String.class))).andReturn(false).once();
    EasyMock.replay(mockPinManagerDbDao);

    // do test
    String pinNumber = uut.generatePinNumber();

    // assertions
    assertNotNull(pinNumber);
    Assert.assertEquals(EXPECTED_LENGTH, pinNumber.length());
    EasyMock.verify(mockPinManagerDbDao);
  }

  @Test
  public void generatePinNumber_IsRandom() {
    // setup
    boolean inUse = false;

    // expectations
    EasyMock.expect(mockPinManagerDbDao.isPinUsed(isA(String.class))).andReturn(inUse).times(2);
    EasyMock.replay(mockPinManagerDbDao);

    // do test
    String pinNumber1 = uut.generatePinNumber();
    String pinNumber2 = uut.generatePinNumber();

    // assertions
    assertNotNull(pinNumber1);
    Assert.assertEquals(EXPECTED_LENGTH, pinNumber1.length());
    assertNotNull(pinNumber2);
    Assert.assertEquals(EXPECTED_LENGTH, pinNumber2.length());
    Assert.assertNotSame(pinNumber1, pinNumber2);
    EasyMock.verify(mockPinManagerDbDao);
  }

  @Test
  public void generatePinNumber_IsNumericOnlyString() {
    // setup
    boolean inUse = false;

    // expectations
    EasyMock.expect(mockPinManagerDbDao.isPinUsed(isA(String.class))).andReturn(inUse);
    EasyMock.replay(mockPinManagerDbDao);

    // do test
    String pinNumber = uut.generatePinNumber();

    // assertions
    assertNotNull(pinNumber);
    Assert.assertEquals(EXPECTED_LENGTH, pinNumber.length());
    Assert.assertTrue(pinNumber.matches("\\d{8}"));
    EasyMock.verify(mockPinManagerDbDao);
  }
}
