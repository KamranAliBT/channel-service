package com.brighttalk.service.channel.business.domain.channel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria.SortBy;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;

public class ChannelManagersSearchCriteriaTest {

  private static final Integer PAGE_NUMBER = 5;

  private static final Integer PAGE_SIZE = 10;

  private static final SortBy SORT_BY_ENUM = SortBy.EMAIL_ADDRESS;

  private static final String SORT_BY_STRING = "email";

  private static final SortOrder SORT_ORDER_ENUM = SortOrder.DESC;

  private static final String SORT_ORDER_STRING = "desc";

  private ChannelManagersSearchCriteria uut;

  @Test
  public void testPageNumber() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setPageNumber(PAGE_NUMBER);

    // Assertions
    assertEquals(uut.getPageNumber(), PAGE_NUMBER);
  }

  @Test
  public void testPageSize() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setPageSize(PAGE_SIZE);

    // Assertions
    assertEquals(uut.getPageSize(), PAGE_SIZE);
  }

  @Test
  public void testSortByWhenString() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setSortBy(SORT_BY_STRING);

    // Assertions
    assertEquals(uut.getSortBy(), SORT_BY_ENUM);
  }

  @Test
  public void testSortByWhenStringWhenMissing() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setSortBy("");

    // Expectations
    SortBy sortBy = SortBy.LAST_NAME;

    // Assertions
    assertEquals(uut.getSortBy(), sortBy);
  }

  @Test(expected = SearchCriteriaException.class)
  public void testSortByWhenInvalidString() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setSortBy("invalid");

    // Assertions
  }

  @Test
  public void testSortByWhenEnum() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setSortBy(SORT_BY_ENUM);

    // Assertions
    assertEquals(uut.getSortBy(), SORT_BY_ENUM);
  }

  @Test
  public void testSortOrderWhenString() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setSortOrder(SORT_ORDER_STRING);

    // Assertions
    assertEquals(uut.getSortOrder(), SORT_ORDER_ENUM);
  }

  @Test
  public void testSortOrderWhenMissing() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setSortOrder("");

    // Expectations
    SortOrder sortOrder = SortOrder.ASC;

    // Assertions
    assertEquals(uut.getSortOrder(), sortOrder);
  }

  @Test(expected = SearchCriteriaException.class)
  public void testSortOrderWhenInvalidString() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setSortOrder("invalid");

    // Assertions
  }

  @Test
  public void testSortOrderWhenEnum() {

    // Set up
    uut = new ChannelManagersSearchCriteria();
    uut.setSortOrder(SORT_ORDER_ENUM);

    // Assertions
    assertEquals(uut.getSortOrder(), SORT_ORDER_ENUM);
  }

}