/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: CommunicationSyndicationBuilder.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.builder;

import java.util.Date;

import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationAuthorisationStatus;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationType;

/**
 * Utility test class to build a {@link CommunicationSyndication} domain object.
 */
public class CommunicationSyndicationBuilder {

  public final static SyndicationType DEFAULT_SYNDICATION_TYPE = SyndicationType.IN;
  public final static SyndicationAuthorisationStatus DEFAULT_MASTER_AUTHORISATION_STATUS =
      SyndicationAuthorisationStatus.APPROVED;
  public final static SyndicationAuthorisationStatus DEFAULT_CONSUMER_AUTHORISATION_STATUS =
      SyndicationAuthorisationStatus.APPROVED;
  public final static Date DEFAULT_MASTER_AUTHORISATION_TIMESTAMP = new Date();
  public final static Date DEFAULT_CONSUMER_AUTHORISATION_TIMESTAMP = new Date();

  private SyndicationType syndicationType;
  private SyndicationAuthorisationStatus masterSyndicationAuthorisationStatus;
  private SyndicationAuthorisationStatus consumerSyndicationAuthorisationStatus;
  private Date masterSyndicationAuthorisationTimeStamp;
  private Date consumerSyndicationAuthorisationTimeStamp;

  public CommunicationSyndicationBuilder withDefaultValues() {
    syndicationType = DEFAULT_SYNDICATION_TYPE;
    masterSyndicationAuthorisationStatus = DEFAULT_MASTER_AUTHORISATION_STATUS;
    consumerSyndicationAuthorisationStatus = DEFAULT_CONSUMER_AUTHORISATION_STATUS;
    masterSyndicationAuthorisationTimeStamp = DEFAULT_MASTER_AUTHORISATION_TIMESTAMP;
    consumerSyndicationAuthorisationTimeStamp = DEFAULT_CONSUMER_AUTHORISATION_TIMESTAMP;
    return this;
  }

  public CommunicationSyndicationBuilder withSyndicationType(SyndicationType value) {
    syndicationType = value;
    return this;
  }

  public CommunicationSyndicationBuilder withMasterSyndicationAuthorisationStatus(SyndicationAuthorisationStatus value) {
    masterSyndicationAuthorisationStatus = value;
    return this;
  }

  public CommunicationSyndicationBuilder withMasterSyndicationAuthorisationTimeStamp(Date value) {
    masterSyndicationAuthorisationTimeStamp = value;
    return this;
  }

  public CommunicationSyndicationBuilder withConsumerSyndicationAuthorisationStatus(SyndicationAuthorisationStatus value) {
    consumerSyndicationAuthorisationStatus = value;
    return this;
  }

  public CommunicationSyndicationBuilder withConsumerSyndicationAuthorisationTimeStamp(Date value) {
    consumerSyndicationAuthorisationTimeStamp = value;
    return this;
  }

  public CommunicationSyndication build() {
    CommunicationSyndication communicationSyndication = new CommunicationSyndication();
    communicationSyndication.setSyndicationType(syndicationType);
    communicationSyndication.setMasterChannelSyndicationAuthorisationStatus(masterSyndicationAuthorisationStatus);
    communicationSyndication.setMasterChannelSyndicationAuthorisationTimestamp(masterSyndicationAuthorisationTimeStamp);
    communicationSyndication.setConsumerChannelSyndicationAuthorisationStatus(consumerSyndicationAuthorisationStatus);
    communicationSyndication.setConsumerChannelSyndicationAuthorisationTimestamp(consumerSyndicationAuthorisationTimeStamp);
    return communicationSyndication;
  }
}
