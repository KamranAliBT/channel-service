/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: FeaturesPostProcessorTest.java 97229 2015-06-29 09:06:08Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import static org.easymock.EasyMock.isA;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;
import com.brighttalk.service.channel.integration.database.SubscriptionDbDao;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.util.UserSearchCriteriaBuilder;

/**
 * Unit test for {@link FeaturesPostProcessor}.
 */
public class FeaturesPostProcessorTest {

  private static final Long CHANNEL_ID = 33L;

  private static final Long USER_ID = 123L;

  private UserServiceDao mockUserServiceDao;

  private UserSearchCriteriaBuilder mockUserSearchCriteriaBuilder;

  private SubscriptionDbDao mockSubscriptionDbDao;

  private FeaturesPostProcessor uut;

  @Before
  public void setup() {
    uut = new FeaturesPostProcessor();

    mockUserSearchCriteriaBuilder = EasyMock.createMock(UserSearchCriteriaBuilder.class);
    ReflectionTestUtils.setField(uut, "userSearchCriteriaBuilder", mockUserSearchCriteriaBuilder);

    mockUserServiceDao = EasyMock.createMock(UserServiceDao.class);
    ReflectionTestUtils.setField(uut, "userServiceDao", mockUserServiceDao);

    mockSubscriptionDbDao = EasyMock.createMock(SubscriptionDbDao.class);
    ReflectionTestUtils.setField(uut, "subscriptionDbDao", mockSubscriptionDbDao);
  }

  @Test
  public void processWithNullUpdatedFeaturesList() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);
    List<Feature> updatedFeatures = null;

    // Expectations

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
  }

  @Test
  public void processWithEmptyUpdatedFeaturesList() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);
    List<Feature> updatedFeatures = new ArrayList<>();

    // Expectations

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
  }

  @Test
  public void processWithoutBlockedUsersFeatures() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    List<Feature> updatedFeatures = new ArrayList<>();
    updatedFeatures.add(new Feature(Type.ADS_DISPLAYED));

    // Expectations

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
  }

  @Test
  public void processWithBlockedUsersFeaturesDisabled() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    Feature blockedUsersFeature = new Feature(Type.BLOCKED_USER);
    blockedUsersFeature.setIsEnabled(false);

    List<Feature> updatedFeatures = new ArrayList<>();
    updatedFeatures.add(blockedUsersFeature);

    // Expectations

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
  }

  @Test
  public void processWithBlockedUsersFeaturesEnabledWithoutValue() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    Feature blockedUsersFeature = new Feature(Type.BLOCKED_USER);
    blockedUsersFeature.setIsEnabled(true);

    List<Feature> updatedFeatures = new ArrayList<>();
    updatedFeatures.add(blockedUsersFeature);

    // Expectations

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
  }

  @Test
  public void processWithBlockedUsersFeaturesEnabledWithNullValue() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    Feature blockedUsersFeature = new Feature(Type.BLOCKED_USER);
    blockedUsersFeature.setIsEnabled(true);
    blockedUsersFeature.setValue(null);

    List<Feature> updatedFeatures = new ArrayList<>();
    updatedFeatures.add(blockedUsersFeature);

    // Expectations

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
  }

  @Test
  public void processWithBlockedUsersFeaturesEnabledWithEmptyValue() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    Feature blockedUsersFeature = new Feature(Type.BLOCKED_USER);
    blockedUsersFeature.setIsEnabled(true);
    blockedUsersFeature.setValue("");

    List<Feature> updatedFeatures = new ArrayList<>();
    updatedFeatures.add(blockedUsersFeature);

    // Expectations

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
  }

  @Test
  public void processWithBlockedUsersFeaturesEnabledWithNotFoundUsers() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    Feature blockedUsersFeature = new Feature(Type.BLOCKED_USER);
    blockedUsersFeature.setIsEnabled(true);
    blockedUsersFeature.setValue("abc@abc.com");

    List<Feature> updatedFeatures = new ArrayList<>();
    updatedFeatures.add(blockedUsersFeature);

    // Expectations
    PaginatedList<User> paginatedList = new PaginatedList<User>();

    EasyMock.expect(mockUserSearchCriteriaBuilder.buildWithDefaultValues()).andReturn(new UsersSearchCriteria());
    EasyMock.expect(mockUserServiceDao.findByEmailAddress(isA(String.class), isA(UsersSearchCriteria.class))).andReturn(
        paginatedList);

    EasyMock.replay(mockUserSearchCriteriaBuilder, mockUserServiceDao);

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
    EasyMock.verify(mockUserSearchCriteriaBuilder, mockUserServiceDao);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void processWithBlockedUsersFeaturesEnabledWithSingleUser() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    Feature blockedUsersFeature = new Feature(Type.BLOCKED_USER);
    blockedUsersFeature.setIsEnabled(true);
    blockedUsersFeature.setValue("abc@abc.com");

    List<Feature> updatedFeatures = new ArrayList<>();
    updatedFeatures.add(blockedUsersFeature);

    // Expectations
    PaginatedList<User> paginatedList = new PaginatedList<User>();
    paginatedList.add(new User(USER_ID));
    paginatedList.setIsFinalPage(true);

    EasyMock.expect(mockUserSearchCriteriaBuilder.buildWithDefaultValues()).andReturn(new UsersSearchCriteria());
    EasyMock.expect(mockUserServiceDao.findByEmailAddress(isA(String.class), isA(UsersSearchCriteria.class))).andReturn(
        paginatedList);
    mockSubscriptionDbDao.unsubscribeUsers(isA(Long.class), isA(List.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockUserSearchCriteriaBuilder, mockUserServiceDao, mockSubscriptionDbDao);

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
    EasyMock.verify(mockUserSearchCriteriaBuilder, mockUserServiceDao, mockSubscriptionDbDao);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void processWithBlockedUsersFeaturesEnabledWithMultipleUsers() {
    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    Feature blockedUsersFeature = new Feature(Type.BLOCKED_USER);
    blockedUsersFeature.setIsEnabled(true);
    blockedUsersFeature.setValue("abc@abc.com,abc@abc.com,abc@abc.com");

    List<Feature> updatedFeatures = new ArrayList<>();
    updatedFeatures.add(blockedUsersFeature);

    // Expectations
    PaginatedList<User> paginatedList1 = new PaginatedList<User>();
    paginatedList1.add(new User(USER_ID));
    paginatedList1.setIsFinalPage(false);

    PaginatedList<User> paginatedList2 = new PaginatedList<User>();
    paginatedList2.add(new User(USER_ID));
    paginatedList2.setIsFinalPage(true);

    EasyMock.expect(mockUserSearchCriteriaBuilder.buildWithDefaultValues()).andReturn(new UsersSearchCriteria());
    EasyMock.expect(mockUserServiceDao.findByEmailAddress(isA(String.class), isA(UsersSearchCriteria.class))).andReturn(
        paginatedList1);
    EasyMock.expect(mockUserServiceDao.findByEmailAddress(isA(String.class), isA(UsersSearchCriteria.class))).andReturn(
        paginatedList2);
    mockSubscriptionDbDao.unsubscribeUsers(isA(Long.class), isA(List.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockUserSearchCriteriaBuilder, mockUserServiceDao, mockSubscriptionDbDao);

    // Do test
    uut.process(channel, updatedFeatures);

    // Assertions
    EasyMock.verify(mockUserSearchCriteriaBuilder, mockUserServiceDao, mockSubscriptionDbDao);
  }

}
