package com.brighttalk.service.channel.business.service.channel;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.integration.database.ChannelManagerDbDao;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.util.PaginatedListBuilder;
import com.brighttalk.service.channel.presentation.util.UserSearchCriteriaBuilder;

public class DefaultChannelManagerFinderTest {

  private static final Long USER_ID = 999L;

  private static final Long CHANNEL_MANAGER_ID = 888L;

  private static final Long CHANNEL_OWNER_ID = 777L;

  private static final Long CHANNEL_ID = 5L;

  private static final String EMAIL_ADDRESS = "email@brighttalk.com";

  private static final boolean EMAIL_ALERTS_ENABLED = true;

  private static final Integer PAGE_NUMBER = 0;

  private static final Integer PAGE_SIZE = 10;

  private static final UsersSearchCriteria.SortBy USER_SORT_BY = UsersSearchCriteria.SortBy.CREATED;

  private static final UsersSearchCriteria.SortOrder USER_SORT_ORDER = UsersSearchCriteria.SortOrder.ASC;

  private static final ChannelManagersSearchCriteria.SortBy CHANNEL_MANAGER_SORT_BY = ChannelManagersSearchCriteria.SortBy.EMAIL_ADDRESS;

  private static final ChannelManagersSearchCriteria.SortOrder CHANNEL_MANAGER_SORT_ORDER = ChannelManagersSearchCriteria.SortOrder.ASC;

  private static final String ERROR_MESSAGE = "ERROR";

  private ChannelFinder mockChannelFinder;

  private ChannelManagerDbDao mockChannelManagerDbDao;

  private UserServiceDao mockUserServiceDao;

  private PaginatedListBuilder mockPaginatedListBuilder;

  private UserSearchCriteriaBuilder mockUserSearchCriteriaBuilder;

  private ChannelManagerValidator mockValidator;

  private DefaultChannelManagerFinder uut;

  private Set<Object> mocks;

  @Before
  public void setUp() {
    uut = new DefaultChannelManagerFinder();

    mockChannelManagerDbDao = EasyMock.createMock(ChannelManagerDbDao.class);
    ReflectionTestUtils.setField(uut, "channelManagerDbDao", mockChannelManagerDbDao);

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockUserServiceDao = EasyMock.createMock(UserServiceDao.class);
    ReflectionTestUtils.setField(uut, "userServiceDao", mockUserServiceDao);

    mockPaginatedListBuilder = EasyMock.createMock(PaginatedListBuilder.class);
    ReflectionTestUtils.setField(uut, "paginatedListBuilder", mockPaginatedListBuilder);

    mockUserSearchCriteriaBuilder = EasyMock.createMock(UserSearchCriteriaBuilder.class);
    ReflectionTestUtils.setField(uut, "userSearchCriteriaBuilder", mockUserSearchCriteriaBuilder);

    mockValidator = EasyMock.createMock(ChannelManagerValidator.class);
    ReflectionTestUtils.setField(uut, "validator", mockValidator);

    mocks = new HashSet<Object>();
  }

  @Test
  public void testSearch() {

    // Set up
    Channel channel = buildChannel();

    List<User> users = buildUsers();

    List<ChannelManager> channelManagers = buildChannelManagers();

    UsersSearchCriteria userSearchCriteria = buildUserSearchCriteria();

    ChannelManagersSearchCriteria channelManagersSearchCriteria = buildChannelManagersSearchCriteria();

    PaginatedList<ChannelManager> paginatedChannelManagers = buildPaginatedChannelManagers();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorWhenSearching();
    expectChannelManagerDbDaoWhenFiding(channelManagers);
    expectSearchCriteriaBuilderToReturnCriteria(userSearchCriteria);
    expectUserServiceDaoWhenFinding(users);
    expectListBuilderToReturnPaginatedChannelManagers(channelManagers, paginatedChannelManagers);
    expect();

    // Do test
    PaginatedList<ChannelManager> result = uut.find(buildSessionUser(), CHANNEL_ID, channelManagersSearchCriteria);

    // Assertions
    assertNotNull(result);
    assertEquals(result, channelManagers);

    verify();
  }

  @Test(expected = UserAuthorisationException.class)
  public void testWhenUserNotAuthorisedToSearch() {

    // Set up
    Channel channel = buildChannel();

    ChannelManagersSearchCriteria channelManagersSearchCriteria = buildChannelManagersSearchCriteria();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorToThorwExcptionWhenSearching(new UserAuthorisationException(ERROR_MESSAGE));
    expect();

    // Do test
    uut.find(buildSessionUser(), CHANNEL_ID, channelManagersSearchCriteria);

    // Assertions
    verify();
  }

  @Test(expected = ChannelNotFoundException.class)
  public void testSearchWhenChannelDoesNotExist() {

    // Set up
    ChannelManagersSearchCriteria channelManagersSearchCriteria = buildChannelManagersSearchCriteria();

    // Expectations
    expectChannelFinderToThrowExcptionWhenFiding(new ChannelNotFoundException(CHANNEL_ID));
    expect();

    // Do test
    uut.find(buildSessionUser(), CHANNEL_ID, channelManagersSearchCriteria);

    // Assertions
    verify();
  }

  @Test(expected = FeatureNotEnabledException.class)
  public void testSearchWhenChannelManagerFeatureDidabled() {

    // Set up
    Channel channel = buildChannel();

    ChannelManagersSearchCriteria channelManagersSearchCriteria = buildChannelManagersSearchCriteria();

    // Expectations
    expectChannelFinderWhenFinding(channel);
    expectChannelManagerValidatorToThorwExcptionWhenSearching(new FeatureNotEnabledException(""));
    expect();

    // Do test
    uut.find(buildSessionUser(), CHANNEL_ID, channelManagersSearchCriteria);

    // Assertions
    verify();
  }

  private void expectListBuilderToReturnPaginatedChannelManagers(final List<ChannelManager> channelManagers,
      final PaginatedList<ChannelManager> paginatedChannelManagers) {
    EasyMock.expect(mockPaginatedListBuilder.build(channelManagers, PAGE_NUMBER, PAGE_SIZE)).andReturn(
        paginatedChannelManagers);
    expect(mockPaginatedListBuilder);
  }

  @SuppressWarnings("unchecked")
  private void expectUserServiceDaoWhenFinding(final List<User> users) {
    EasyMock.expect(mockUserServiceDao.find(isA(List.class), isA(UsersSearchCriteria.class))).andReturn(users);
    expect(mockUserServiceDao);
  }

  private void expectSearchCriteriaBuilderToReturnCriteria(final UsersSearchCriteria userSearchCriteria) {
    EasyMock.expect(
        mockUserSearchCriteriaBuilder.build(isA(Integer.class), isA(Integer.class), isA(String.class),
            isA(String.class))).andReturn(userSearchCriteria);
    expect(mockUserSearchCriteriaBuilder);
  }

  private void expectChannelManagerDbDaoWhenFiding(final List<ChannelManager> channelManagers) {
    EasyMock.expect(mockChannelManagerDbDao.find(isA(Channel.class))).andReturn(channelManagers);
    expect(mockChannelManagerDbDao);
  }

  private void expectChannelManagerValidatorWhenSearching() {
    mockValidator.assertSearch(isA(User.class), isA(Channel.class));
    EasyMock.expectLastCall();
    expect(mockValidator);
  }

  private void expectChannelManagerValidatorToThorwExcptionWhenSearching(final Throwable cause) {
    mockValidator.assertSearch(isA(User.class), isA(Channel.class));
    EasyMock.expectLastCall().andThrow(cause);
    expect(mockValidator);
  }

  private void expectChannelFinderWhenFinding(final Channel channel) {
    EasyMock.expect(mockChannelFinder.find(isA(Long.class))).andReturn(channel);
    expect(mockChannelFinder);
  }

  private void expectChannelFinderToThrowExcptionWhenFiding(final Throwable cause) {
    EasyMock.expect(mockChannelFinder.find(isA(Long.class))).andThrow(cause);
    expect(mockChannelFinder);
  }

  private void expect(final Object obj) {
    mocks.add(obj);
  }

  private void expect() {
    EasyMock.replay(mocks.toArray());
  }

  private void verify() {
    EasyMock.verify(mocks.toArray());
  }

  private User buildSessionUser() {
    return new User(CHANNEL_OWNER_ID);
  }

  private List<User> buildUsers() {
    User user = new User();
    user.setId(USER_ID);

    List<User> users = new ArrayList<User>();
    users.add(user);

    return users;
  }

  private ChannelManagersSearchCriteria buildChannelManagersSearchCriteria() {
    ChannelManagersSearchCriteria criteria = new ChannelManagersSearchCriteria();
    criteria.setPageNumber(PAGE_NUMBER);
    criteria.setPageSize(PAGE_SIZE);
    criteria.setSortBy(CHANNEL_MANAGER_SORT_BY);
    criteria.setSortOrder(CHANNEL_MANAGER_SORT_ORDER);
    return criteria;
  }

  private UsersSearchCriteria buildUserSearchCriteria() {
    UsersSearchCriteria criteria = new UsersSearchCriteria();
    criteria.setPageNumber(PAGE_NUMBER);
    criteria.setPageSize(PAGE_SIZE);
    criteria.setSortBy(USER_SORT_BY);
    criteria.setSortOrder(USER_SORT_ORDER);
    return criteria;
  }

  private Channel buildChannel() {
    return buildChannel(CHANNEL_ID, USER_ID);
  }

  private Channel buildChannel(final Long channelId, final Long ownerUserId) {

    Channel channel = new Channel(channelId);
    channel.setOwnerUserId(ownerUserId);

    return channel;
  }

  private List<ChannelManager> buildChannelManagers() {

    Channel channel = new Channel(CHANNEL_ID);

    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(buildChannelManager(channel));

    return channelManagers;
  }

  private PaginatedList<ChannelManager> buildPaginatedChannelManagers() {

    PaginatedList<ChannelManager> channelManagers = new PaginatedList<ChannelManager>();
    channelManagers.add(buildChannelManager());

    return channelManagers;
  }

  private ChannelManager buildChannelManager() {

    Channel channel = new Channel(CHANNEL_ID);

    return buildChannelManager(channel);
  }

  private ChannelManager buildChannelManager(final Channel channel) {

    User user = new User();
    user.setId(USER_ID);
    user.setEmail(EMAIL_ADDRESS);

    return buildChannelManager(channel, user, CHANNEL_MANAGER_ID, EMAIL_ALERTS_ENABLED);
  }

  private ChannelManager buildChannelManager(final Channel channel, final User user, final Long channelManagerId,
      final boolean emailAlerts) {

    ChannelManager channelManager = new ChannelManager();
    channelManager.setId(channelManagerId);
    channelManager.setUser(user);
    channelManager.setChannel(channel);
    channelManager.setEmailAlerts(emailAlerts);

    return channelManager;
  }

}
