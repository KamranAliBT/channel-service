/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: DefaultSubscriptionsServiceTest.java 92672 2015-04-01 08:27:25Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.Subscription.Referral;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError.SubscriptionErrorCode;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionsSummary;
import com.brighttalk.service.channel.business.domain.extractor.SubscriptionsElementExtractor;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.SubscriptionNotFoundException;
import com.brighttalk.service.channel.business.error.SubscriptionProcessException;
import com.brighttalk.service.channel.business.error.UserNotFoundException;
import com.brighttalk.service.channel.business.filter.UserFilter;
import com.brighttalk.service.channel.integration.communication.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.database.SubscriptionContextDbDao;
import com.brighttalk.service.channel.integration.database.SubscriptionDbDao;
import com.brighttalk.service.channel.integration.summit.dto.SummitDto;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.utils.DomainFactoryUtils;

public class DefaultSubscriptionsServiceTest {

  private static final Long USER_ID = 33L;

  private static final String EMAIL = "test@brighttalk.com";

  private static final Long SUMMIT_ID = 1111L;

  private static final Long COMM_ID = 2222L;

  private static final Long SUBSCRIPTION_ID = 5555L;

  private static final Boolean DRY_RUN_FALSE = false;

  private static final Boolean DRY_RUN_TRUE = true;

  private static final String EXPECT_EXCEPTION_MSG = "Expected an exception to be thrown.";

  private static final String ENGAGEMENT_SCORE = "33";

  private UserFilter mockUserFilter;

  private SubscriptionDbDao mockSubscriptionDbDao;

  private SubscriptionContextDbDao mockSubscriptionContextDbDao;

  private ChannelFinder mockChannelFinder;

  private SubscriptionValidator mockValidator;

  private SubscriptionsElementExtractor mockExtractor;

  private UserServiceDao mockUserServiceDao;

  private Channel testChannel;

  private DefaultSubscriptionsService uut;

  @Before
  @SuppressWarnings("unchecked")
  public void setUp() {
    uut = new DefaultSubscriptionsService();

    mockValidator = EasyMock.createMock(SubscriptionValidator.class);
    ReflectionTestUtils.setField(uut, "validator", mockValidator);

    mockExtractor = EasyMock.createMock(SubscriptionsElementExtractor.class);
    ReflectionTestUtils.setField(uut, "extractor", mockExtractor);
    EasyMock.expect(mockExtractor.extractLeadContextSummits(isA(List.class))).andReturn(new ArrayList<SummitDto>());
    EasyMock.expect(mockExtractor.extractLeadContextCommunications(isA(List.class))).andReturn(
        new ArrayList<CommunicationDto>());

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockSubscriptionDbDao = EasyMock.createMock(SubscriptionDbDao.class);
    ReflectionTestUtils.setField(uut, "subscriptionDbDao", mockSubscriptionDbDao);

    mockUserServiceDao = EasyMock.createMock(UserServiceDao.class);
    ReflectionTestUtils.setField(uut, "userServiceDao", mockUserServiceDao);

    mockUserFilter = EasyMock.createMock(UserFilter.class);
    ReflectionTestUtils.setField(uut, "userFilter", mockUserFilter);

    testChannel = buildChannel();
    EasyMock.expect(mockChannelFinder.find(testChannel.getId())).andReturn(testChannel).anyTimes();

    mockSubscriptionContextDbDao = EasyMock.createMock(SubscriptionContextDbDao.class);
    ReflectionTestUtils.setField(uut, "subscriptionContextDbDao", mockSubscriptionContextDbDao);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWithNoSubscriptions() {

    // Set up
    List<Subscription> subscriptions = new ArrayList<>();

    // Expectations
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.replay(mockUserServiceDao, mockChannelFinder, mockExtractor);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    // Assertions
    assertNull(result.getSubscriptions());
    assertNull(result.getErrors());

    EasyMock.verify(mockUserServiceDao, mockChannelFinder, mockExtractor);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWhileUserNotFound() {

    // Set up
    User user = buildUser(USER_ID);

    Subscription subscription = buildSubscription(user, Referral.paid);
    List<Subscription> subscriptions = Collections.singletonList(subscription);

    // Expectations
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andThrow(
        new UserNotFoundException(user.getId()));
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.replay(mockExtractor, mockChannelFinder, mockUserFilter);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    // Assertions
    assertNull(result.getSubscriptions());

    SubscriptionError error = result.getErrors().iterator().next();
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.USER_NOT_FOUND, errorCodes.get(0));
    EasyMock.verify(mockExtractor, mockChannelFinder, mockUserFilter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWhileUserAlreadySubscribed() {

    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription = buildSubscription(user, Referral.paid);
    List<Subscription> subscriptions = Collections.singletonList(subscription);

    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, false)));

    // Expectations
    List<User> expectedUsers = new ArrayList<User>();
    expectedUsers.add(buildUser(USER_ID, EMAIL));

    Subscription expectedSubscription = buildSubscription(buildUser(USER_ID, EMAIL), Referral.paid);
    expectedSubscription.setActive(true);

    List<Subscription> expectedSubscriptions = new ArrayList<>();
    expectedSubscriptions.add(expectedSubscription);

    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(expectedSubscriptions).anyTimes();
    mockValidator.assertBlockedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertSubscribedUser(isA(Subscription.class), isA(Channel.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(subscription, SubscriptionErrorCode.ALREADY_SUBSCRIBED));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.replay(mockUserServiceDao, mockExtractor, mockChannelFinder, mockUserFilter,
        mockValidator);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    // Assertions
    assertNull(result.getSubscriptions());

    SubscriptionError error = result.getErrors().iterator().next();
    assertNotNull(error.getSubscription());
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.ALREADY_SUBSCRIBED, errorCodes.get(0));
    EasyMock.verify(mockUserServiceDao, mockExtractor, mockChannelFinder, mockUserFilter,
        mockValidator);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWhileUserUnsubscribed() {

    // Set up
    User user = buildUser(USER_ID);
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid));

    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, false)));

    // Expectations
    List<User> expectedUsers = new ArrayList<User>();
    expectedUsers.add(buildUser(USER_ID, EMAIL));

    Subscription expectedSubscription = buildSubscription(buildUser(USER_ID, EMAIL), Referral.paid);
    expectedSubscription.setActive(false);

    List<Subscription> expectedSubscriptions = new ArrayList<>();
    expectedSubscriptions.add(expectedSubscription);

    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(expectedSubscriptions).anyTimes();
    mockValidator.assertBlockedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertSubscribedUser(isA(Subscription.class), isA(Channel.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.UNSUBSCRIBED));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.replay(mockExtractor, mockChannelFinder, mockValidator, mockUserFilter);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    // Assertions
    assertNull(result.getSubscriptions());

    SubscriptionError error = result.getErrors().iterator().next();
    assertNotNull(error.getSubscription());
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.UNSUBSCRIBED, errorCodes.get(0));
    EasyMock.verify(mockExtractor, mockChannelFinder, mockValidator, mockUserFilter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWhileBlockedUserFeatureEnabledAndUserBlocked() {

    // Set up
    User user = buildUser(USER_ID);
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid));

    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, true)));

    // Expectations
    List<User> expectedUsers = new ArrayList<User>();
    expectedUsers.add(buildUser(USER_ID, EMAIL));

    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    mockValidator.assertBlockedUser(isA(Subscription.class), isA(Channel.class));
    EasyMock.expectLastCall().andThrow(new SubscriptionProcessException(user, SubscriptionErrorCode.USER_BLOCKED));
    mockValidator.assertSubscribedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.replay(mockExtractor, mockChannelFinder, mockValidator, mockUserFilter);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    // Assertions
    assertNull(result.getSubscriptions());

    SubscriptionError error = result.getErrors().iterator().next();
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.USER_BLOCKED, errorCodes.get(0));
    EasyMock.verify(mockExtractor, mockChannelFinder, mockValidator, mockUserFilter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWhileBlockedUserFeatureEnabledAndUserNotBlocked() {

    // Set up
    User user = buildUser(USER_ID);
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid));

    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature("", true)));

    // Expectations
    List<User> expectedUsers = new ArrayList<User>();
    expectedUsers.add(buildUser(USER_ID, EMAIL));

    List<Subscription> expectedSubscriptions = new ArrayList<>();
    Subscription expectedSubscription = new Subscription();

    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(expectedSubscriptions).anyTimes();
    EasyMock.expect(mockSubscriptionDbDao.create(isA(Subscription.class), isA(Channel.class))).andReturn(
        expectedSubscription);
    EasyMock.replay(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    // Assertions
    assertFalse(result.getSubscriptions().isEmpty());
    assertNull(result.getErrors());
    EasyMock.verify(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void dryRunCreateSubscriptionsWhileBlockedUserFeatureEnabledAndUserNotBlocked() {

    // Set up
    User user = buildUser(USER_ID);
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid));

    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature("", true)));

    // Expectations
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(new ArrayList<Subscription>()).anyTimes();
    EasyMock.replay(mockUserServiceDao, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockExtractor);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_TRUE);

    // Assertions
    assertFalse(result.getSubscriptions().isEmpty());
    assertNull(result.getErrors());
    EasyMock.verify(mockUserServiceDao, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockExtractor);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWhileBlockedUserFeatureDisabledAndUserBlocked() {

    // Set up
    User user = buildUser(EMAIL);
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid));

    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, false)));

    // Expectations
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(new ArrayList<Subscription>()).anyTimes();
    EasyMock.expect(mockSubscriptionDbDao.create(isA(Subscription.class), isA(Channel.class))).andReturn(
        new Subscription());
    EasyMock.replay(mockExtractor, mockSubscriptionDbDao, mockChannelFinder);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    // Assertions
    assertFalse(result.getSubscriptions().isEmpty());
    assertNull(result.getErrors());
    EasyMock.verify(mockExtractor, mockSubscriptionDbDao, mockChannelFinder);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void dryRunCreateSubscriptionsWhileBlockedUserFeatureDisabledAndUserBlocked() {

    // Set up
    User user = buildUser(USER_ID);
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid));

    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, false)));

    // Expectations
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(new ArrayList<Subscription>()).anyTimes();
    EasyMock.replay(mockExtractor, mockSubscriptionDbDao, mockChannelFinder);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_TRUE);

    // Assertions
    assertFalse(result.getSubscriptions().isEmpty());
    assertNull(result.getErrors());

    EasyMock.verify(mockExtractor, mockSubscriptionDbDao, mockChannelFinder);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#createSubscriptions} in the error case of creating subscriptions with a
   * supplied channel that does not identify an existing channel.
   */
  @Test
  public void createSubscriptionsWhileChannelNotFound() {

    // Set up
    List<Subscription> subscriptions = new ArrayList<>();

    // Expectations - Reset the Channel Finder mock object to return channel not found exception.
    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);
    EasyMock.expect(mockChannelFinder.find(testChannel.getId())).andThrow(new ChannelNotFoundException(
        testChannel.getId())).anyTimes();
    EasyMock.replay(mockUserServiceDao, mockSubscriptionDbDao, mockChannelFinder);

    // Do test
    try {
      uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);
      fail(EXPECT_EXCEPTION_MSG);
    } catch (ChannelNotFoundException exception) {
      assertEquals(ErrorCode.NOT_FOUND.getName(), exception.getUserErrorCode());
      assertEquals("Channel [" + testChannel.getId() + "] not found.", exception.getMessage());
    }
  }

  /**
   * Tests {@link DefaultSubscriptionsService#createSubscriptions} in the error case of creating subscriptions with
   * subscription context of lead Type "summit" and a lead context containing summit id that does not identify an
   * existing summit.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWithSubscriptionContextWhileSummitLeadTypeAndSummitNotFound() {
    // Set up
    User user = buildUser(USER_ID);
    SubscriptionContext context = new SubscriptionContext(SubscriptionLeadType.SUMMIT, SUMMIT_ID.toString(), "0");
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid, context));
    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, false)));

    // Expectations
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(new ArrayList<Subscription>()).anyTimes();
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    mockValidator.assertBlockedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertSubscribedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    EasyMock.expectLastCall().andThrow(new SubscriptionProcessException(user, SubscriptionErrorCode.SUMMIT_NOT_FOUND));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.replay(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    assertNotNull(result);
    SubscriptionError error = result.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.SUMMIT_NOT_FOUND, errorCodes.get(0));
    EasyMock.verify(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#createSubscriptions} in the error case of creating subscriptions with
   * subscription context of lead Type "content" and a lead context containing communication id that does not identify
   * an existing communication.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionsWithSubscriptionContextWhileWebinarLeadTypeAndCommunicationNotFound() {
    // Set up
    User user = buildUser(USER_ID);
    SubscriptionContext context = new SubscriptionContext(SubscriptionLeadType.CONTENT, COMM_ID.toString(), "0");
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid, context));
    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, false)));

    // Expectations
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(new ArrayList<Subscription>()).anyTimes();
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    mockValidator.assertBlockedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertSubscribedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.COMMUNICATION_NOT_FOUND));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.replay(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    assertNotNull(result);
    SubscriptionError error = result.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.COMMUNICATION_NOT_FOUND, errorCodes.get(0));
    EasyMock.verify(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#createSubscriptions} in the error case of creating subscriptions with
   * subscription context with invalid engagement score - value that are not between 0 and 100.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionWithSubscriptionContextWithInvalidEngagementScore() {
    // Set up
    User user = buildUser(USER_ID);
    SubscriptionContext context = new SubscriptionContext(SubscriptionLeadType.CONTENT, COMM_ID.toString(), "900");
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid, context));
    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, false)));

    // Expectations
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(new ArrayList<Subscription>()).anyTimes();
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    mockValidator.assertBlockedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertSubscribedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE));
    EasyMock.replay(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    assertNotNull(result);
    SubscriptionError error = result.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE, errorCodes.get(0));
    EasyMock.verify(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#createSubscriptions} in the error case of creating subscriptions with
   * subscription context with invalid engagement score - value that are not a valid integer.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionWithSubscriptionContextWithInvalidIntegerEngagementScore() {
    // Set up
    User user = buildUser(USER_ID);
    SubscriptionContext context = new SubscriptionContext(SubscriptionLeadType.CONTENT, COMM_ID.toString(), "test");
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid, context));
    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, false)));

    // Expectations
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(new ArrayList<Subscription>()).anyTimes();
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    mockValidator.assertBlockedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertSubscribedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE));
    EasyMock.replay(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    assertNotNull(result);
    SubscriptionError error = result.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE, errorCodes.get(0));
    EasyMock.verify(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#createSubscriptions} in the error case of creating subscriptions with
   * subscription context with multiple errors.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void createSubscriptionWithSubscriptionContextWithMultipleErrors() {
    // Set up
    User user = buildUser(USER_ID);
    SubscriptionContext context = new SubscriptionContext(SubscriptionLeadType.CONTENT, COMM_ID.toString(), "test");
    List<Subscription> subscriptions = Collections.singletonList(buildSubscription(user, Referral.paid, context));
    testChannel.setFeatures(Collections.singletonList(buildBlockeUserFeature(EMAIL, false)));

    // Expectations
    EasyMock.expect(mockExtractor.extractUsers(isA(List.class))).andReturn(new ArrayList<User>());
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(new ArrayList<Subscription>()).anyTimes();
    EasyMock.expect(mockUserFilter.filterByUserIdOrEmail(isA(List.class), isA(User.class))).andReturn(user);
    mockValidator.assertBlockedUser(isA(Subscription.class), isA(Channel.class));
    mockValidator.assertSubscribedUser(isA(Subscription.class), isA(Channel.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.ALREADY_SUBSCRIBED));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.INVALID_LEAD_CONTEXT));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE));
    EasyMock.replay(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);

    // Do test
    SubscriptionsSummary result = uut.create(testChannel.getId(), subscriptions, DRY_RUN_FALSE);

    assertNotNull(result);
    SubscriptionError error = result.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(3, errorCodes.size());
    assertEquals(SubscriptionErrorCode.ALREADY_SUBSCRIBED, errorCodes.get(0));
    assertEquals(SubscriptionErrorCode.INVALID_LEAD_CONTEXT, errorCodes.get(1));
    assertEquals(SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE, errorCodes.get(2));
    EasyMock.verify(mockExtractor, mockSubscriptionDbDao, mockChannelFinder, mockUserFilter, mockValidator);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the success case of calling update method with no
   * subscriptions.
   */
  @Test
  public void updateSubscriptionWithNoSubscriptions() {
    // Set up
    List<Subscription> subscriptions = new ArrayList<>();

    // Expectations -
    EasyMock.replay(mockChannelFinder, mockExtractor);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    // Assertions -
    assertNull(summary.getSubscriptions());
    assertNull(summary.getErrors());
    EasyMock.verify(mockChannelFinder, mockExtractor);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the error case of update subscriptions while the updated
   * subscription does not identify an existing subscription.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionWhileSubscriptionNotFound() {
    // Set up
    User user = buildUser(USER_ID);
    user.setEmail(EMAIL);
    Subscription subscription = buildSubscription(user, Referral.paid, DomainFactoryUtils.createSubscriptionContext());
    List<Subscription> subscriptions = Collections.singletonList(subscription);

    // Expectations -
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andThrow(
        new SubscriptionNotFoundException());
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.replay(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    assertNotNull(summary);
    SubscriptionError error = summary.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    assertEquals(EMAIL, error.getSubscription().getUser().getEmail());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.SUBSCRIPTION_NOT_FOUND, errorCodes.get(0));
    EasyMock.verify(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the error case of update subscriptions while the updated
   * subscription is inactive.
   */
  @Test
  public void updateSubscriptionWhileSubscriptionInactive() {
    // Set up
    User user = buildUser(USER_ID);
    user.setEmail(EMAIL);
    Subscription subscription = buildSubscription(user, Referral.paid);
    subscription.setContext(null);
    subscription.setActive(false);
    List<Subscription> subscriptions = Collections.singletonList(subscription);

    // Expectations -
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(subscription);
    mockValidator.assertSubscriptionActive(subscription);
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(subscription, SubscriptionErrorCode.SUBSCRIPTION_INACTIVE));
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    EasyMock.replay(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    assertNotNull(summary);
    SubscriptionError error = summary.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    assertEquals(EMAIL, error.getSubscription().getUser().getEmail());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.SUBSCRIPTION_INACTIVE, errorCodes.get(0));
    EasyMock.verify(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the error case of update subscriptions while the updated
   * subscription context exists.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionWhileSubscriptionContextExists() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription =
        buildSubscription(user, Referral.dotcom, DomainFactoryUtils.createSubscriptionContext());
    List<Subscription> subscriptions = Collections.singletonList(subscription);
    SubscriptionContext foundSubscriptionContext =
        new SubscriptionContext(SubscriptionLeadType.KEYWORD, "test keyword", "99");
    Subscription foundSubscription = buildSubscription(user, Referral.dotcom, foundSubscriptionContext);

    // Expectations -
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(foundSubscription);
    mockValidator.assertSubscriptionActive(isA(Subscription.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    EasyMock.replay(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    assertNotNull(summary);
    SubscriptionError error = summary.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.CONTEXT_ALREADY_EXIST, errorCodes.get(0));
    SubscriptionContext actualContext = error.getSubscription().getContext();
    assertNotNull(actualContext);
    assertEquals(foundSubscription.getContext().getLeadType(), actualContext.getLeadType());
    assertEquals(foundSubscription.getContext().getLeadContext(), actualContext.getLeadContext());
    assertEquals(foundSubscription.getContext().getEngagementScore(), actualContext.getEngagementScore());
    EasyMock.verify(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the error case of update subscriptions while the updated
   * subscription Lead context value doesn't identify an existing communication.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionWithSubscriptionContextWhileCommunicationNotFound() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription =
        buildSubscription(user, Referral.notdotcom, DomainFactoryUtils.createSubscriptionContext());
    List<Subscription> subscriptions = Collections.singletonList(subscription);
    Subscription foundSubscription = buildSubscription(user, Referral.notdotcom, null);

    // Expectations -
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(foundSubscription);
    mockValidator.assertSubscriptionActive(isA(Subscription.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.COMMUNICATION_NOT_FOUND));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.replay(mockUserServiceDao, mockExtractor, mockValidator, mockSubscriptionDbDao);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    assertNotNull(summary);
    SubscriptionError error = summary.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.COMMUNICATION_NOT_FOUND, errorCodes.get(0));
    EasyMock.verify(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the error case of update subscriptions while the updated
   * subscription Lead context value doesn't identify an existing summit.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionWithSubscriptionContextWhileSummitNotFound() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription = buildSubscription(user, Referral.paid, DomainFactoryUtils.createSubscriptionContext());
    List<Subscription> subscriptions = Collections.singletonList(subscription);
    Subscription foundSubscription = buildSubscription(user, Referral.notdotcom, null);

    // Expectations -
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(foundSubscription);
    mockValidator.assertSubscriptionActive(isA(Subscription.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.SUMMIT_NOT_FOUND));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.replay(mockUserServiceDao, mockExtractor, mockValidator, mockSubscriptionDbDao);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    assertNotNull(summary);
    SubscriptionError error = summary.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.SUMMIT_NOT_FOUND, errorCodes.get(0));
    EasyMock.verify(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the error case of update subscriptions while the updated
   * subscription context has invalid engagement score value.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionWithSubscriptionContextWhileEngagementScoreInvalid() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription =
        buildSubscription(user, Referral.dotcom, DomainFactoryUtils.createSubscriptionContext());
    List<Subscription> subscriptions = Collections.singletonList(subscription);
    Subscription foundSubscription = buildSubscription(user, Referral.notdotcom, null);

    // Expectations -
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(foundSubscription);
    mockValidator.assertSubscriptionActive(isA(Subscription.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE));
    EasyMock.replay(mockUserServiceDao, mockExtractor, mockValidator, mockSubscriptionDbDao);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    assertNotNull(summary);
    SubscriptionError error = summary.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(1, errorCodes.size());
    assertEquals(SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE, errorCodes.get(0));
    EasyMock.verify(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the error case of update subscriptions while the updated
   * subscription context has invalid engagement score value and invalid lead context.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionWithSubscriptionContextWhileEngagementScoreAndLeadContextInvalid() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription =
        buildSubscription(user, Referral.dotcom, DomainFactoryUtils.createSubscriptionContext());
    List<Subscription> subscriptions = Collections.singletonList(subscription);
    Subscription foundSubscription = buildSubscription(user, Referral.notdotcom, null);

    // Expectations -
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(foundSubscription);
    mockValidator.assertSubscriptionActive(isA(Subscription.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.SUMMIT_NOT_FOUND));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE));
    EasyMock.replay(mockUserServiceDao, mockExtractor, mockValidator, mockSubscriptionDbDao);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    assertNotNull(summary);
    SubscriptionError error = summary.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(2, errorCodes.size());
    assertEquals(SubscriptionErrorCode.SUMMIT_NOT_FOUND, errorCodes.get(0));
    assertEquals(SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE, errorCodes.get(1));
    EasyMock.verify(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the error case of update subscriptions while the updated
   * subscription inactive and it has a context and the supplied subscription to update has a context with invalid
   * engagement score and lead context.
   * <p>
   * Expected result - The reported subscription has 4 errors - SubscriptionInactive, ContextAlreadyExist,
   * SummitNotFound and InvalidEngagementScore.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionWithContextWhileInactiveContextExistsAndEngagementScoreAndLeadContextInvalid() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription =
        buildSubscription(user, Referral.dotcom, DomainFactoryUtils.createSubscriptionContext());
    List<Subscription> subscriptions = Collections.singletonList(subscription);
    Subscription foundSubscription = buildSubscription(user, Referral.notdotcom,
        DomainFactoryUtils.createSubscriptionContext());

    // Expectations -
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(foundSubscription);
    mockValidator.assertSubscriptionActive(isA(Subscription.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(subscription, SubscriptionErrorCode.SUBSCRIPTION_INACTIVE));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.SUMMIT_NOT_FOUND));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(user, SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE));
    EasyMock.replay(mockUserServiceDao, mockExtractor, mockValidator, mockSubscriptionDbDao);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    assertNotNull(summary);
    SubscriptionError error = summary.getErrors().iterator().next();
    assertNotNull(error);
    assertEquals(USER_ID, error.getSubscription().getUser().getId());
    List<SubscriptionErrorCode> errorCodes = error.getErrorCodes();
    assertNotNull(errorCodes);
    assertEquals(4, errorCodes.size());
    assertEquals(SubscriptionErrorCode.SUBSCRIPTION_INACTIVE, errorCodes.get(0));
    assertEquals(SubscriptionErrorCode.CONTEXT_ALREADY_EXIST, errorCodes.get(1));
    assertEquals(SubscriptionErrorCode.SUMMIT_NOT_FOUND, errorCodes.get(2));
    assertEquals(SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE, errorCodes.get(3));
    EasyMock.verify(mockUserServiceDao, mockValidator, mockSubscriptionDbDao, mockExtractor);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the success case of update subscriptions with dry run set
   * to false - validate the subscription and update the subscription details.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionSuccessWithDryRunFalse() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription =
        buildSubscription(user, Referral.dotcom, DomainFactoryUtils.createSubscriptionContext());
    List<Subscription> subscriptions = Collections.singletonList(subscription);
    Subscription foundSubscription = buildSubscription(user, Referral.notdotcom, null);

    // Expectations -
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(foundSubscription);
    mockValidator.assertSubscriptionActive(isA(Subscription.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    mockSubscriptionDbDao.updateReferral(isA(Referral.class), isA(Long.class));
    mockSubscriptionContextDbDao.create(isA(SubscriptionContext.class));
    EasyMock.replay(mockUserServiceDao, mockExtractor, mockValidator, mockSubscriptionDbDao,
        mockSubscriptionContextDbDao);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_FALSE);

    assertNotNull(summary);
    assertNull(summary.getErrors());
    List<Subscription> updateSubscriptions = summary.getSubscriptions();
    assertEquals(1, updateSubscriptions.size());
    Subscription updateSubscription = updateSubscriptions.iterator().next();
    assertNotNull(updateSubscription);
    assertEquals(subscription, updateSubscription);
    EasyMock.verify(mockUserServiceDao, mockExtractor, mockValidator, mockSubscriptionDbDao,
        mockSubscriptionContextDbDao);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#update(List)} in the success case of update subscriptions with dry run set
   * to true - validate the subscription details only.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateSubscriptionSuccessWithDryRunTrue() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription =
        buildSubscription(user, Referral.dotcom, DomainFactoryUtils.createSubscriptionContext());
    List<Subscription> subscriptions = Collections.singletonList(subscription);
    Subscription foundSubscription = buildSubscription(user, Referral.notdotcom, null);

    // Expectations -
    EasyMock.expect(mockUserServiceDao.find(isA(Long.class))).andReturn(user);
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(foundSubscription);
    mockValidator.assertSubscriptionActive(isA(Subscription.class));
    mockValidator.assertLeadContextValue(isA(Subscription.class), isA(List.class), isA(List.class));
    mockValidator.assertContextEngagementScore(isA(Subscription.class));
    EasyMock.replay(mockUserServiceDao, mockExtractor, mockValidator, mockSubscriptionDbDao,
        mockSubscriptionContextDbDao);

    // Do Test -
    SubscriptionsSummary summary = uut.update(subscriptions, DRY_RUN_TRUE);

    assertNotNull(summary);
    assertNull(summary.getErrors());
    List<Subscription> updateSubscriptions = summary.getSubscriptions();
    assertEquals(1, updateSubscriptions.size());
    Subscription updateSubscription = updateSubscriptions.iterator().next();
    assertNotNull(updateSubscription);
    assertEquals(subscription, updateSubscription);
    EasyMock.verify(mockUserServiceDao, mockExtractor, mockValidator, mockSubscriptionDbDao,
        mockSubscriptionContextDbDao);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#find(Long)} in the success case of finding subscription by id.
   */
  @Test
  public void findByIdSuccess() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription =
        buildSubscription(user, Referral.dotcom, DomainFactoryUtils.createSubscriptionContext());

    // Expectations
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(subscription);
    EasyMock.replay(mockSubscriptionDbDao);

    // Do Test -
    SubscriptionsSummary summary = uut.findById(subscription.getId());

    // Assertions: -
    assertNotNull(summary);
    assertNull(summary.getErrors());
    assertNotNull(summary.getSubscriptions());
    assertEquals(1, summary.getSubscriptions().size());
    Subscription foundSubscription = summary.getSubscriptions().iterator().next();
    assertEquals(subscription, foundSubscription);
    EasyMock.verify(mockSubscriptionDbDao);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#find(Long)} in the error case of finding subscription by id and reporting
   * subscription inactive.
   */
  @Test
  public void findByIdSubscriptionInactive() {
    // Set up
    User user = buildUser(USER_ID);
    Subscription subscription =
        buildSubscription(user, Referral.dotcom, DomainFactoryUtils.createSubscriptionContext());
    subscription.setActive(false);

    // Expectations
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andReturn(subscription);
    mockValidator.assertSubscriptionActive(isA(Subscription.class));
    EasyMock.expectLastCall().andThrow(
        new SubscriptionProcessException(subscription, SubscriptionErrorCode.SUBSCRIPTION_INACTIVE));
    EasyMock.replay(mockSubscriptionDbDao, mockValidator);

    // Do Test -
    SubscriptionsSummary summary = uut.findById(subscription.getId());

    // Assertions: -
    assertNotNull(summary);
    assertNotNull(summary.getSubscriptions());
    assertNotNull(summary.getErrors());
    assertEquals(1, summary.getErrors().size());
    SubscriptionError errorSubscription = summary.getErrors().get(0);
    SubscriptionErrorCode errorCode = errorSubscription.getErrorCodes().get(0);
    assertNotNull(errorCode);
    assertEquals(SubscriptionErrorCode.SUBSCRIPTION_INACTIVE, errorCode);
    EasyMock.verify(mockSubscriptionDbDao, mockValidator);
  }

  /**
   * Tests {@link DefaultSubscriptionsService#find(Long)} in the error case of finding subscription by id and reporting
   * subscription not found.
   */
  @Test
  public void findByIdSubscriptionNotFound() {
    // Expectations
    EasyMock.expect(mockSubscriptionDbDao.findById(isA(Long.class))).andThrow(new SubscriptionNotFoundException());
    EasyMock.replay(mockSubscriptionDbDao);

    // Do Test -
    SubscriptionsSummary summary = uut.findById(SUBSCRIPTION_ID);

    // Assertions: -
    assertNotNull(summary);
    assertNull(summary.getSubscriptions());
    assertNotNull(summary.getErrors());
    assertEquals(1, summary.getErrors().size());
    SubscriptionError errorSubscription = summary.getErrors().get(0);
    SubscriptionErrorCode errorCode = errorSubscription.getErrorCodes().get(0);
    assertNotNull(errorCode);
    assertEquals(SubscriptionErrorCode.SUBSCRIPTION_NOT_FOUND, errorCode);
    EasyMock.verify(mockSubscriptionDbDao);
  }

  private User buildUser(final Long userId) {
    User user = new User();
    user.setId(userId);
    return user;
  }

  private User buildUser(final String email) {
    User user = new User();
    user.setEmail(email);
    return user;
  }

  private User buildUser(final Long userId, final String email) {
    User user = new User();
    user.setId(userId);
    user.setEmail(email);
    return user;
  }

  private Subscription buildSubscription(final User user, final Referral referral) {
    Subscription subscription = new Subscription();
    subscription.setId(SUBSCRIPTION_ID);
    subscription.setUser(user);
    subscription.setReferral(referral);
    subscription.setActive(true);
    subscription.setContext(new SubscriptionContext(SubscriptionLeadType.SUMMIT, SUMMIT_ID.toString(), ENGAGEMENT_SCORE));
    return subscription;
  }

  private Subscription buildSubscription(final User user, final Referral referral, final SubscriptionContext context) {
    Subscription subscription = buildSubscription(user, referral);
    subscription.setId(SUBSCRIPTION_ID);
    subscription.setContext(context);
    return subscription;
  }

  private Channel buildChannel() {
    Channel channel = new Channel();
    channel.setId(31L);
    return channel;
  }

  private Feature buildBlockeUserFeature(final String email, final Boolean isActive) {
    Feature blockeUserFeature = new Feature(Type.BLOCKED_USER);
    blockeUserFeature.setValue(email);
    blockeUserFeature.setIsEnabled(isActive);
    return blockeUserFeature;
  }

}
