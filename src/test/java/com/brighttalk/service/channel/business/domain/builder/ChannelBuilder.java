/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelBuilder.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Channel.Searchable;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.channel.ChannelStatistics;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;

public class ChannelBuilder {

  private static final String DEFAULT_CHANNEL_TITLE = "defaultChannelTitle";

  private static final String DEFAULT_CHANNEL_DESCRIPTION = "defaultChannelDescription";

  private static final String DEFAULT_CHANNEL_KEYWORDS = "defaultChannelKeywords,AndSomeMore";

  private static final String DEFAULT_CHANNEL_STRAPLINE = "defaultChannelStrapline";

  private static final String DEFAULT_CHANNEL_ORGANISATION = "defaultChannelOrganisation";

  private Long id;

  private String title;

  private String keywords;

  private String strapline;

  private String organisation;

  private String description;

  private Long ownerUserId;

  private float rating;

  private long viewedFor;

  private boolean active;

  private boolean promotedOnHomepage;

  private Searchable searchable;

  private ChannelType type;

  private ChannelType createdType;

  private ChannelType pendingType;

  private Date created;

  private List<Feature> features;

  private List<ChannelManager> channelManagers;

  // this is incomplete. only populated values needed to insert channel into db
  public ChannelBuilder withDefaultValues() {

    title = DEFAULT_CHANNEL_TITLE;
    description = DEFAULT_CHANNEL_DESCRIPTION;
    keywords = DEFAULT_CHANNEL_KEYWORDS;
    strapline = DEFAULT_CHANNEL_STRAPLINE;
    organisation = DEFAULT_CHANNEL_ORGANISATION;
    type = new ChannelType();
    createdType = new ChannelType();
    pendingType = new ChannelType();
    searchable = Searchable.INCLUDED;
    promotedOnHomepage = false;
    ownerUserId = 1l;
    viewedFor = 0;
    rating = 0f;
    active = true;
    created = new Date();
    features = new ArrayList<Feature>();
    channelManagers = new ArrayList<ChannelManager>();

    return this;
  }

  public ChannelBuilder withId(final Long value) {

    id = value;
    return this;
  }

  public ChannelBuilder withTitle(final String value) {

    title = value;
    return this;
  }

  public ChannelBuilder withOrganisation(final String value) {

    organisation = value;
    return this;
  }

  public ChannelBuilder withOwnerUserId(final long value) {

    ownerUserId = value;
    return this;
  }

  public ChannelBuilder withChannelManagerUserId(final long value) {

    User user = new User();
    user.setId(value);

    ChannelManager channelManager = new ChannelManager();
    channelManager.setUser(user);

    channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(channelManager);

    return this;
  }

  public ChannelBuilder withFeatures(final List<Feature> value) {

    features = value;
    return this;
  }

  public ChannelBuilder withCreatedDate(final Date value) {
    created = value;
    return this;
  }

  public Channel build() {

    Channel channel = new Channel();
    channel.setId(id);
    channel.setTitle(title);
    channel.setDescription(description);
    channel.setKeywords(keywords != null ? Arrays.asList(keywords) : null);
    channel.setStrapline(strapline);
    channel.setOrganisation(organisation);
    channel.setType(type);
    channel.setCreatedType(createdType);
    channel.setPendingType(pendingType);
    channel.setSearchable(searchable);
    channel.setPromotedOnHomepage(promotedOnHomepage);
    channel.setOwnerUserId(ownerUserId);
    channel.setCreated(created);

    ChannelStatistics statistics = new ChannelStatistics();
    statistics.setViewedFor(viewedFor);
    statistics.setRating(rating);

    channel.setStatistics(statistics);

    channel.setActive(active);
    channel.setFeatures(features);

    channel.setChannelManagers(channelManagers);

    return channel;
  }
}