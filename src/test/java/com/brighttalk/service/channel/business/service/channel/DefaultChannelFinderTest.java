/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelFinderTest.java 75635 2014-03-24 16:09:48Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.User.Role;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.channel.ChannelUrlGenerator;
import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.database.ChannelManagerDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.FeatureDbDao;
import com.brighttalk.service.utils.DomainFactoryUtils;

/**
 * Unit tests for {@link DefaultChannelFinder}.
 */
public class DefaultChannelFinderTest {

  private static final Long USER_ID = 999L;
  private static final Long CHANNEL_ID = 5L;
  private static final int DEFAULT_PAGE_SIZE = 10;
  private static final int DEFAULT_MAX_PAGE_SIZE = 50;
  private static final int DEFAULT_PAGE_NUMBER = 2;
  private static final Long CHANNEL_ID2 = 6L;
  private static final String CHANNEL_KEYWORDS = "Channel Test Keywords";
  private static final String CHANNEL_ORGANISATION = "Channel Test Organisation";
  private static final String CHANNEL_STRAPLINE = "Channel Test Strapline";
  private static final String CHANNEL_TITLE = "Channel Test Title";

  private DefaultChannelFinder uut;

  private ChannelDbDao mockChannelDbDao;
  private ChannelManagerDbDao mockChannelManagerDbDao;
  private FeatureDbDao mockFeatureDbDao;
  private CategoryDbDao mockCategoryDbDao;
  private CommunicationDbDao mockCommunicationDbDao;

  private ChannelUrlGenerator mockChannelUrlGenerator;
  private ChannelUrlGenerator mockChannelAtomFeedUrlGenerator;
  private ChannelUrlGenerator mockChannelRssFeedUrlGenerator;

  @Before
  public void setUp() {
    uut = new DefaultChannelFinder();

    mockChannelDbDao = EasyMock.createMock(ChannelDbDao.class);
    ReflectionTestUtils.setField(uut, "channelDbDao", mockChannelDbDao);

    mockChannelManagerDbDao = EasyMock.createMock(ChannelManagerDbDao.class);
    ReflectionTestUtils.setField(uut, "channelManagerDbDao", mockChannelManagerDbDao);

    mockFeatureDbDao = EasyMock.createMock(FeatureDbDao.class);
    ReflectionTestUtils.setField(uut, "featureDbDao", mockFeatureDbDao);

    mockCategoryDbDao = EasyMock.createMock(CategoryDbDao.class);
    ReflectionTestUtils.setField(uut, "categoryDbDao", mockCategoryDbDao);

    mockCommunicationDbDao = EasyMock.createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDAO", mockCommunicationDbDao);

    mockChannelUrlGenerator = EasyMock.createMock(ChannelUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "channelUrlGenerator", mockChannelUrlGenerator);

    mockChannelAtomFeedUrlGenerator = EasyMock.createMock(ChannelUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "channelAtomFeedUrlGenerator", mockChannelAtomFeedUrlGenerator);

    mockChannelRssFeedUrlGenerator = EasyMock.createMock(ChannelUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "channelRssFeedUrlGenerator", mockChannelRssFeedUrlGenerator);

    ReflectionTestUtils.setField(uut, "defaultPageSize", DEFAULT_PAGE_SIZE);
    ReflectionTestUtils.setField(uut, "maxPageSize", DEFAULT_MAX_PAGE_SIZE);
    ReflectionTestUtils.setField(uut, "defaultPageNumber", DEFAULT_PAGE_NUMBER);
  }

  @SuppressWarnings("deprecation")
  @Test
  public void testFindSubscribedChannelsForAuthorizedUserWithTwoChannel() {
    // setup
    User user = createUser();
    Channel channel = new Channel(CHANNEL_ID);

    Channel channel2 = new Channel(CHANNEL_ID2);
    List<Channel> channels = new ArrayList<Channel>();
    channels.add(channel);
    channels.add(channel2);

    // expectations
    EasyMock.expect(mockChannelDbDao.findSubscribedChannels(USER_ID)).andReturn(channels);
    EasyMock.replay(mockChannelDbDao);

    // do test
    List<Channel> result = uut.findSubscribedChannels(user);

    // assertions
    assertEquals(channels.size(), result.size());
  }

  @SuppressWarnings("deprecation")
  @Test
  public void testFindSubscribedChannelsForAuthorizedUserWithNoChannel() {
    // setup

    User user = createUser();
    List<Channel> channels = new ArrayList<Channel>();

    // expectations
    EasyMock.expect(mockChannelDbDao.findSubscribedChannels(USER_ID)).andReturn(channels);
    EasyMock.replay(mockChannelDbDao);

    // do test
    List<Channel> result = uut.findSubscribedChannels(user);

    // assertions
    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWithDefaultPagingSettings() {

    // setup
    Long channelId = CHANNEL_ID;

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(CHANNEL_ID.toString());

    Channel channel = new Channel(channelId);
    channel.setOwnerUserId(USER_ID);

    PaginatedList<Channel> channels = new PaginatedList<Channel>();
    channels.add(channel);

    // expectations
    EasyMock.expect(mockChannelDbDao.find(searchCriteria)).andReturn(channels);

    mockChannelDbDao.loadStatistics(channels);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channels);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channels);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channels);
    EasyMock.expectLastCall();

    EasyMock.replay(mockChannelDbDao);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    // assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.get(0).getId());

    EasyMock.verify(mockChannelDbDao);

    assertEquals(DEFAULT_PAGE_SIZE, (int) searchCriteria.getPageSize());
    assertEquals(DEFAULT_PAGE_NUMBER, (int) searchCriteria.getPageNumber());
  }

  @Test
  public void findChannelsWhenSingleChannelIsReturnedAndShouldLoadFeaturedCommunicationIsFalse() {

    // setup
    Long channelId = CHANNEL_ID;

    final int expectedPageSize = 6;
    final int expectedPageNumber = 4;

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(CHANNEL_ID.toString());
    searchCriteria.setPageNumber(expectedPageNumber);
    searchCriteria.setPageSize(expectedPageSize);

    Channel channel = new Channel(channelId);
    channel.setOwnerUserId(USER_ID);

    PaginatedList<Channel> channels = new PaginatedList<Channel>();
    channels.add(channel);

    // expectations
    EasyMock.expect(mockChannelDbDao.find(searchCriteria)).andReturn(channels);
    mockChannelDbDao.loadStatistics(channels);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channels);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channels);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channels);
    EasyMock.expectLastCall();

    EasyMock.replay(mockChannelDbDao);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    // assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.get(0).getId());

    EasyMock.verify(mockChannelDbDao);

  }

  @Test
  public void findChannelsWhenSingleChannelIsReturnedAndShouldLoadFeaturedCommunicationIsTrue() {

    // setup
    Long channelId = CHANNEL_ID;

    final int expectedPageSize = 6;
    final int expectedPageNumber = 4;

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(CHANNEL_ID.toString());
    searchCriteria.setPageNumber(expectedPageNumber);
    searchCriteria.setPageSize(expectedPageSize);
    searchCriteria.setPrivateWebcastsIncluded(false);

    Channel channel = new Channel(channelId);
    channel.setOwnerUserId(USER_ID);

    PaginatedList<Channel> channels = new PaginatedList<Channel>();
    channels.add(channel);

    // expectations
    EasyMock.expect(mockChannelDbDao.find(searchCriteria)).andReturn(channels);
    mockChannelDbDao.loadStatistics(channels);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channels);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channels);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channels);
    EasyMock.expectLastCall();

    EasyMock.replay(mockChannelDbDao);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    // assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.get(0).getId());

    EasyMock.verify(mockChannelDbDao);

  }

  @Test
  public void findChannelsWhenNoChannelsAreReturnedFromChannelDboFind() {

    // setup
    final int expectedPageSize = 6;
    final int expectedPageNumber = 4;

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(CHANNEL_ID.toString());
    searchCriteria.setPageNumber(expectedPageNumber);
    searchCriteria.setPageSize(expectedPageSize);

    PaginatedList<Channel> channels = new PaginatedList<Channel>();

    // expectations
    EasyMock.expect(mockChannelDbDao.find(searchCriteria)).andReturn(channels);
    EasyMock.replay(mockChannelDbDao);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    // assertions
    assertEquals(0, result.size());
    EasyMock.verify(mockChannelDbDao);

  }

  @Test
  public void testFindUserIsManager() {
    // Set up
    Long channelOwnerId = 1234L;

    Feature channelManagersFeature = buildChannelManagersFeature("5", Boolean.TRUE);

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(channelOwnerId);
    channel.setChannelManagers(new ArrayList<ChannelManager>());
    channel.addFeature(channelManagersFeature);

    User user = new User(USER_ID);
    List<Role> roles = new ArrayList<Role>();
    roles.add(Role.MANAGER);
    user.setRoles(roles);

    // Expectations
    EasyMock.expect(mockChannelDbDao.find(CHANNEL_ID)).andReturn(channel);
    mockChannelDbDao.loadStatistics(channel);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channel);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channel);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channel);
    EasyMock.expectLastCall();

    mockChannelManagerDbDao.load(channel);
    EasyMock.expectLastCall();

    EasyMock.expect(mockChannelDbDao.isPresenter(user, channel)).andReturn(false);

    EasyMock.replay(mockChannelDbDao);

    // Do Test
    Channel result = uut.find(user, CHANNEL_ID);

    // Assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.getId());
    EasyMock.verify(mockChannelDbDao);
  }

  @Test
  public void testFindUserIsChannelOwner() {
    // Set up
    Long channelOwnerId = USER_ID;

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(channelOwnerId);

    User user = new User(USER_ID);
    List<Role> roles = new ArrayList<Role>();
    roles.add(Role.USER);
    user.setRoles(roles);

    // Expectations
    EasyMock.expect(mockChannelDbDao.find(CHANNEL_ID)).andReturn(channel);
    mockChannelDbDao.loadStatistics(channel);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channel);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channel);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channel);
    EasyMock.expectLastCall();

    mockChannelManagerDbDao.load(channel);
    EasyMock.expectLastCall();

    EasyMock.expect(mockChannelDbDao.isPresenter(user, channel)).andReturn(false);

    EasyMock.replay(mockChannelDbDao);

    // Do Test
    Channel result = uut.find(user, CHANNEL_ID);

    // Assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.getId());
  }

  @Test
  public void testFindUserIsPresenter() {
    // Set up
    Long channelOwnerId = 1234L;
    Boolean isPresenter = true;

    Feature channelManagersFeature = buildChannelManagersFeature("5", Boolean.TRUE);

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(channelOwnerId);
    channel.setChannelManagers(new ArrayList<ChannelManager>());
    channel.addFeature(channelManagersFeature);

    User user = new User(USER_ID);
    List<Role> roles = new ArrayList<Role>();
    roles.add(Role.USER);
    user.setRoles(roles);

    // Expectations
    EasyMock.expect(mockChannelDbDao.find(CHANNEL_ID)).andReturn(channel);
    mockChannelDbDao.loadStatistics(channel);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channel);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channel);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channel);
    EasyMock.expectLastCall();

    mockChannelManagerDbDao.load(channel);
    EasyMock.expectLastCall();

    EasyMock.expect(mockChannelDbDao.isPresenter(user, channel)).andReturn(isPresenter);
    EasyMock.replay(mockChannelDbDao);

    // Do Test
    Channel result = uut.find(user, CHANNEL_ID);

    // Assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.getId());
  }

  @Test
  public void testFindWhenUserIsChannelManager() {
    // Set up
    Long channelOwnerId = 1234L;
    Boolean isPresenter = false;

    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(buildChannelManager(USER_ID));

    Feature channelManagersFeature = buildChannelManagersFeature("5", Boolean.TRUE);

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(channelOwnerId);
    channel.setChannelManagers(channelManagers);
    channel.addFeature(channelManagersFeature);

    User user = new User(USER_ID);
    List<Role> roles = new ArrayList<Role>();
    roles.add(Role.USER);
    user.setRoles(roles);

    // Expectations
    EasyMock.expect(mockChannelDbDao.find(CHANNEL_ID)).andReturn(channel);
    mockChannelDbDao.loadStatistics(channel);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channel);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channel);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channel);
    EasyMock.expectLastCall();

    mockChannelManagerDbDao.load(channel);
    EasyMock.expectLastCall();

    EasyMock.expect(mockChannelDbDao.isPresenter(user, channel)).andReturn(isPresenter);
    EasyMock.replay(mockChannelDbDao);

    // Do Test
    Channel result = uut.find(user, CHANNEL_ID);

    // Assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.getId());
  }

  @Test(expected = UserAuthorisationException.class)
  public void testFindWhenUserIsChannelManagerAndFeatureDisabled() {
    // Set up
    Long channelOwnerId = 1234L;
    Boolean isPresenter = false;

    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(buildChannelManager(USER_ID));

    Feature channelManagersFeature = buildChannelManagersFeature("5", Boolean.FALSE);

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(channelOwnerId);
    channel.setChannelManagers(channelManagers);
    channel.addFeature(channelManagersFeature);

    User user = new User(USER_ID);
    List<Role> roles = new ArrayList<Role>();
    roles.add(Role.USER);
    user.setRoles(roles);

    // Expectations
    EasyMock.expect(mockChannelDbDao.find(CHANNEL_ID)).andReturn(channel);
    mockChannelDbDao.loadStatistics(channel);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channel);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channel);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channel);
    EasyMock.expectLastCall();

    mockChannelManagerDbDao.load(channel);
    EasyMock.expectLastCall();

    EasyMock.expect(mockChannelDbDao.isPresenter(user, channel)).andReturn(isPresenter);
    EasyMock.replay(mockChannelDbDao);

    // Do Test
    uut.find(user, CHANNEL_ID);

    // Assertions
  }

  private Feature buildChannelManagersFeature(final String value, final Boolean isEnabled) {
    Feature channelManagersFeature = new Feature(Feature.Type.CHANNEL_MANAGERS);
    channelManagersFeature.setIsEnabled(isEnabled);
    channelManagersFeature.setValue(value);
    return channelManagersFeature;
  }

  private ChannelManager buildChannelManager(final Long userId) {
    Long channelManagerId = 4321L;

    User user = new User();
    user.setId(userId);

    ChannelManager channelManager = new ChannelManager();
    channelManager.setId(channelManagerId);
    channelManager.setUser(user);

    return channelManager;
  }

  @Test(expected = UserAuthorisationException.class)
  public void testFindUserNotPresenterAndNotChannelManager() {
    // Set up
    Long channelOwnerId = 1234L;
    Boolean isPresenter = false;

    Feature channelManagersFeature = buildChannelManagersFeature("5", Boolean.TRUE);

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(channelOwnerId);
    channel.setChannelManagers(new ArrayList<ChannelManager>());
    channel.addFeature(channelManagersFeature);

    User user = new User(USER_ID);
    List<Role> roles = new ArrayList<Role>();
    roles.add(Role.USER);
    user.setRoles(roles);

    // Expectations
    EasyMock.expect(mockChannelDbDao.find(CHANNEL_ID)).andReturn(channel);
    mockChannelDbDao.loadStatistics(channel);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channel);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channel);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channel);
    EasyMock.expectLastCall();

    mockChannelManagerDbDao.load(channel);
    EasyMock.expectLastCall();

    EasyMock.expect(mockChannelDbDao.isPresenter(user, channel)).andReturn(isPresenter);
    EasyMock.replay(mockChannelDbDao);

    // Do Test
    Channel result = uut.find(user, CHANNEL_ID);

    // Assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.getId());
  }

  @SuppressWarnings("deprecation")
  @Test
  public void testFindSubscribedChannelsForAuthorizedUserWithOneChannel() {
    // setup

    User user = createUser();
    Channel channel = DomainFactoryUtils.createChannel();

    List<Channel> channels = new ArrayList<Channel>();
    channels.add(channel);

    // expectations
    EasyMock.expect(mockChannelDbDao.findSubscribedChannels(USER_ID)).andReturn(channels);
    EasyMock.replay(mockChannelDbDao);

    // do test
    List<Channel> result = uut.findSubscribedChannels(user);

    // assertions
    assertEquals(1, result.size());
    assertEquals(CHANNEL_ID, result.get(0).getId());
    assertEquals(CHANNEL_TITLE, result.get(0).getTitle());
    assertEquals(CHANNEL_ORGANISATION, result.get(0).getOrganisation());
    assertEquals(Arrays.asList(CHANNEL_KEYWORDS), result.get(0).getKeywords());
    assertEquals(CHANNEL_STRAPLINE, result.get(0).getStrapline());
  }

  @Test
  public void findPaginatedOwnedChannelsWithFeaturedCommunications() {

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();
    User user = new User(USER_ID);

    ArrayList<Channel> foundChannels = new ArrayList<Channel>();
    final Channel expectedChannel = new Channel(CHANNEL_ID);
    foundChannels.add(expectedChannel);

    final Long expectedCommunicationId1 = 666l;
    final Long expectedCommunicationId2 = 667l;

    List<Long> featuredCommunicationIds = new ArrayList<Long>();
    featuredCommunicationIds.add(expectedCommunicationId1);

    Communication communication = new Communication(expectedCommunicationId1);
    communication.setChannelId(CHANNEL_ID);

    Communication communication2 = new Communication(expectedCommunicationId2);
    communication2.setChannelId(CHANNEL_ID);

    List<Communication> featuredCommunications = new ArrayList<Communication>();
    featuredCommunications.add(communication);
    featuredCommunications.add(communication2);

    expectedChannel.setFeaturedCommunications(featuredCommunications);

    PaginatedList<Channel> searchResult = new PaginatedList<Channel>();
    searchResult.addAll(foundChannels);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsOwned(USER_ID, searchCriteria)).andReturn(searchResult);
    mockChannelDbDao.loadStatistics(searchResult);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(searchResult);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(searchResult);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(searchResult);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelDbDao);

    // do test
    PaginatedList<Channel> result = uut.findOwnedChannels(user, searchCriteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(CHANNEL_ID, result.get(0).getId());
    assertNotNull(result.get(0).getFeaturedCommunications());
    assertEquals(2, result.get(0).getFeaturedCommunications().size());
    assertEquals(expectedCommunicationId1, result.get(0).getFeaturedCommunications().get(0).getId());
    assertEquals(expectedCommunicationId2, result.get(0).getFeaturedCommunications().get(1).getId());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void findPaginatedOwnedChannelsWithSearchCriteriaAndTooBigPageSize() {

    final int expectedPageSize = DEFAULT_MAX_PAGE_SIZE + 1;
    final int expectedPageNumber = 4;

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();
    searchCriteria.setPageNumber(expectedPageNumber);
    searchCriteria.setPageSize(expectedPageSize);

    User user = new User(USER_ID);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsOwned(USER_ID, searchCriteria)).andReturn(
        new PaginatedList<Channel>());
    EasyMock.expect(mockCommunicationDbDao.findFeatured(EasyMock.anyObject(List.class), EasyMock.anyBoolean())).andReturn(
        null);
    EasyMock.replay(mockChannelDbDao);

    // do test
    uut.findOwnedChannels(user, searchCriteria);

    assertEquals(DEFAULT_MAX_PAGE_SIZE, (int) searchCriteria.getPageSize());
    assertEquals(expectedPageNumber, (int) searchCriteria.getPageNumber());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void findPaginatedOwnedChannelsWithSearchCriteria() {

    final int expectedPageSize = 6;
    final int expectedPageNumber = 4;

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();
    searchCriteria.setPageNumber(expectedPageNumber);
    searchCriteria.setPageSize(expectedPageSize);

    User user = new User(USER_ID);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsOwned(USER_ID, searchCriteria)).andReturn(
        new PaginatedList<Channel>());
    EasyMock.expect(mockCommunicationDbDao.findFeatured(EasyMock.anyObject(List.class), EasyMock.anyBoolean())).andReturn(
        null);
    EasyMock.replay(mockChannelDbDao);

    // do test
    uut.findOwnedChannels(user, searchCriteria);

    assertEquals(expectedPageSize, (int) searchCriteria.getPageSize());
    assertEquals(expectedPageNumber, (int) searchCriteria.getPageNumber());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void findPaginatedOwnedChannelsWithDefaultPagingSettings() {

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();
    User user = new User(USER_ID);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsOwned(USER_ID, searchCriteria)).andReturn(
        new PaginatedList<Channel>());
    EasyMock.expect(mockCommunicationDbDao.findFeatured(EasyMock.anyObject(List.class), EasyMock.anyBoolean())).andReturn(
        null);
    EasyMock.replay(mockChannelDbDao);

    // do test
    uut.findOwnedChannels(user, searchCriteria);

    assertEquals(DEFAULT_PAGE_SIZE, (int) searchCriteria.getPageSize());
    assertEquals(DEFAULT_PAGE_NUMBER, (int) searchCriteria.getPageNumber());
  }

  @Test
  public void findPaginatedOwnedChannelsWithSortBySubscribers() {

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();
    User user = new User(USER_ID);

    ArrayList<Channel> foundChannels = new ArrayList<Channel>();
    final Channel expectedChannel = new Channel(CHANNEL_ID);
    foundChannels.add(expectedChannel);

    PaginatedList<Channel> searchResult = new PaginatedList<Channel>();
    searchResult.addAll(foundChannels);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsOwned(USER_ID, searchCriteria)).andReturn(searchResult);
    mockChannelDbDao.loadStatistics(searchResult);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(searchResult);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(searchResult);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(searchResult);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelDbDao);

    // do test
    PaginatedList<Channel> result = uut.findOwnedChannels(user, searchCriteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(CHANNEL_ID, result.get(0).getId());
  }

  @Test
  public void findPaginatedOwnedChannels() {

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();
    User user = new User(USER_ID);

    ArrayList<Channel> foundChannels = new ArrayList<Channel>();
    final Channel expectedChannel = new Channel(CHANNEL_ID);
    foundChannels.add(expectedChannel);

    PaginatedList<Channel> searchResult = new PaginatedList<Channel>();
    searchResult.addAll(foundChannels);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsOwned(USER_ID, searchCriteria)).andReturn(searchResult);
    mockChannelDbDao.loadStatistics(searchResult);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(searchResult);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(searchResult);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(searchResult);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelDbDao);

    // do test
    PaginatedList<Channel> result = uut.findOwnedChannels(user, searchCriteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(CHANNEL_ID, result.get(0).getId());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void findPaginatedSubscribedChannelsWithSearchCriteriaAndTooBigPageSize() {

    final int expectedPageSize = DEFAULT_MAX_PAGE_SIZE + 1;
    final int expectedPageNumber = 4;

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();
    searchCriteria.setPageNumber(expectedPageNumber);
    searchCriteria.setPageSize(expectedPageSize);

    User user = new User(USER_ID);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsSubscribed(USER_ID, searchCriteria)).andReturn(
        new PaginatedList<Channel>());
    EasyMock.expect(mockCommunicationDbDao.findFeatured(EasyMock.anyObject(List.class), EasyMock.anyBoolean())).andReturn(
        null);
    EasyMock.replay(mockChannelDbDao);

    // do test
    uut.findSubscribedChannels(user, searchCriteria);

    assertEquals(DEFAULT_MAX_PAGE_SIZE, (int) searchCriteria.getPageSize());
    assertEquals(expectedPageNumber, (int) searchCriteria.getPageNumber());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void findPaginatedSubscribedChannelsWithSearchCriteriaKeepsValuesInSearchCriteria() {

    final int expectedPageSize = 6;
    final int expectedPageNumber = 4;

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();
    searchCriteria.setPageNumber(expectedPageNumber);
    searchCriteria.setPageSize(expectedPageSize);

    User user = new User(USER_ID);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsSubscribed(USER_ID, searchCriteria)).andReturn(
        new PaginatedList<Channel>());
    EasyMock.expect(mockCommunicationDbDao.findFeatured(EasyMock.anyObject(List.class), EasyMock.anyBoolean())).andReturn(
        null);
    EasyMock.replay(mockChannelDbDao);

    // do test
    uut.findSubscribedChannels(user, searchCriteria);

    assertEquals(expectedPageSize, (int) searchCriteria.getPageSize());
    assertEquals(expectedPageNumber, (int) searchCriteria.getPageNumber());
  }

  @Test
  public void findPaginatedSubscribedChannels() {

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();
    User user = new User(USER_ID);

    ArrayList<Channel> foundChannels = new ArrayList<Channel>();
    final Channel expectedChannel = new Channel(CHANNEL_ID);
    foundChannels.add(expectedChannel);

    PaginatedList<Channel> searchResult = new PaginatedList<Channel>();
    searchResult.addAll(foundChannels);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsSubscribed(USER_ID, searchCriteria)).andReturn(searchResult);
    mockChannelDbDao.loadStatistics(searchResult);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(searchResult);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(searchResult);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(searchResult);
    EasyMock.expectLastCall();
    EasyMock.replay(mockChannelDbDao);

    // do test
    PaginatedList<Channel> result = uut.findSubscribedChannels(user, searchCriteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(CHANNEL_ID, result.get(0).getId());
  }

  @SuppressWarnings("unchecked")
  @Test
  public void findPaginatedSubscribedChannelsWithDefaultPagingSettingsDefaultValuesOnSearchCriteria() {

    MyChannelsSearchCriteria searchCriteria = new MyChannelsSearchCriteria();

    User user = new User(USER_ID);

    EasyMock.expect(mockChannelDbDao.findPaginatedChannelsSubscribed(USER_ID, searchCriteria)).andReturn(
        new PaginatedList<Channel>());
    EasyMock.expect(mockCommunicationDbDao.findFeatured(EasyMock.anyObject(List.class), EasyMock.anyBoolean())).andReturn(
        null);
    EasyMock.replay(mockChannelDbDao);

    // do test
    uut.findSubscribedChannels(user, searchCriteria);

    assertEquals(DEFAULT_PAGE_SIZE, (int) searchCriteria.getPageSize());
    assertEquals(DEFAULT_PAGE_NUMBER, (int) searchCriteria.getPageNumber());

  }

  @Test
  public void findWhenChannelDbDaoReturnsSingleChannel() {
    // setup
    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(USER_ID);
    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channel.getId());

    // expectations
    EasyMock.expect(mockChannelDbDao.find(CHANNEL_ID)).andReturn(channel);

    mockChannelDbDao.loadStatistics(channel);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channel);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channel);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channel);
    EasyMock.expectLastCall();

    mockChannelManagerDbDao.load(channel);
    EasyMock.expectLastCall();

    EasyMock.replay(mockChannelDbDao);

    // do test
    Channel result = uut.find(CHANNEL_ID);

    // assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.getId());
    EasyMock.verify(mockChannelDbDao);
  }

  /**
   * Test find channel for for not existing channel.
   */
  @Test(expected = NotFoundException.class)
  public void findWhenChannelDbDaoThrowsNotFoundException() {
    // setup

    Channel channel = new Channel(CHANNEL_ID);
    channel.setOwnerUserId(USER_ID);

    // expectations
    EasyMock.expect(mockChannelDbDao.find(CHANNEL_ID)).andThrow(new NotFoundException("Channel doesn't exist."));
    EasyMock.replay(mockChannelDbDao);

    // do test
    uut.find(CHANNEL_ID);

    // assertions
  }

  @Test
  public void findMultipleWhenChannelDbDaoReturnsSingleChannel() {

    // setup
    Long channelId = CHANNEL_ID;

    List<Long> channelIds = Arrays.asList(channelId);

    Channel channel = new Channel(channelId);
    channel.setOwnerUserId(USER_ID);

    List<Channel> channels = Arrays.asList(channel);

    // expectations

    EasyMock.expect(mockChannelDbDao.find(channelIds)).andReturn(channels);

    mockChannelDbDao.loadStatistics(channels);
    EasyMock.expectLastCall().once();

    mockFeatureDbDao.loadFeatures(channels);
    EasyMock.expectLastCall().once();

    mockCategoryDbDao.loadCategories(channels);
    EasyMock.expectLastCall().once();

    mockChannelDbDao.loadSurvey(channels);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockChannelDbDao);

    // do test
    List<Channel> result = uut.find(channelIds);

    // assertions
    assertNotNull(result);
    assertEquals(CHANNEL_ID, result.get(0).getId());

    EasyMock.verify(mockChannelDbDao);
  }

  @Test
  public void findMultipleWhenChannelDbDaoReturnsEmptChannelList() {

    // setup
    List<Long> channelIds = Arrays.asList(CHANNEL_ID);

    List<Channel> channels = Arrays.asList();

    // expectations
    EasyMock.expect(mockChannelDbDao.find(channelIds)).andReturn(channels);

    EasyMock.replay(mockChannelDbDao);

    // do test
    List<Channel> result = uut.find(channelIds);

    // assertions
    assertEquals(0, result.size());
    EasyMock.verify(mockChannelDbDao);

  }

  @Test
  public void findWhenChannelDbDaoReturnsSingleChannelAndUrlsAreLoaded() {

    Channel channel = new Channel();

    final String expectedChannelUrl = "http://local.brighttalk.net/channelUrl";
    final String expectedFeedAtomUrl = "http://local.brighttalk.net/channelAtomFeedUrl";
    final String expectedFeedRssUrl = "http://local.brighttalk.net/channelRssFeedUrl";

    EasyMock.expect(mockChannelUrlGenerator.generate(channel)).andReturn(expectedChannelUrl);
    EasyMock.expect(mockChannelAtomFeedUrlGenerator.generate(channel)).andReturn(expectedFeedAtomUrl);
    EasyMock.expect(mockChannelRssFeedUrlGenerator.generate(channel)).andReturn(expectedFeedRssUrl);

    EasyMock.replay(mockChannelUrlGenerator, mockChannelAtomFeedUrlGenerator, mockChannelRssFeedUrlGenerator);

    EasyMock.expect(mockChannelDbDao.find(CHANNEL_ID)).andReturn(channel);

    mockChannelDbDao.loadStatistics(channel);
    EasyMock.expectLastCall();

    mockFeatureDbDao.loadFeatures(channel);
    EasyMock.expectLastCall();

    mockCategoryDbDao.loadCategories(channel);
    EasyMock.expectLastCall();

    mockChannelDbDao.loadSurvey(channel);
    EasyMock.expectLastCall();

    mockChannelManagerDbDao.load(channel);
    EasyMock.expectLastCall();

    EasyMock.replay(mockChannelDbDao);

    // do test
    Channel result = uut.find(CHANNEL_ID);

    // assertions
    assertEquals(expectedChannelUrl, result.getUrl());
    assertEquals(expectedFeedAtomUrl, result.getFeedUrlAtom());
    assertEquals(expectedFeedRssUrl, result.getFeedUrlRss());

    // verification
    EasyMock.verify(mockChannelUrlGenerator, mockChannelAtomFeedUrlGenerator, mockChannelRssFeedUrlGenerator);
  }

  @Test
  public void findMultipleWhenChannelDbDaoReturnsSingleChannelAndUrlsAreLoaded() {

    List<Long> channelIds = Arrays.asList(CHANNEL_ID);

    Channel channel = new Channel();
    List<Channel> channels = Arrays.asList(channel);

    final String expectedChannelUrl = "http://local.brighttalk.net/channelUrl";
    final String expectedFeedAtomUrl = "http://local.brighttalk.net/channelAtomFeedUrl";
    final String expectedFeedRssUrl = "http://local.brighttalk.net/channelRssFeedUrl";

    EasyMock.expect(mockChannelUrlGenerator.generate(channel)).andReturn(expectedChannelUrl);
    EasyMock.expect(mockChannelAtomFeedUrlGenerator.generate(channel)).andReturn(expectedFeedAtomUrl);
    EasyMock.expect(mockChannelRssFeedUrlGenerator.generate(channel)).andReturn(expectedFeedRssUrl);

    EasyMock.replay(mockChannelUrlGenerator, mockChannelAtomFeedUrlGenerator, mockChannelRssFeedUrlGenerator);

    EasyMock.expect(mockChannelDbDao.find(channelIds)).andReturn(channels);

    mockChannelDbDao.loadStatistics(channels);
    EasyMock.expectLastCall().once();

    mockFeatureDbDao.loadFeatures(channels);
    EasyMock.expectLastCall().once();

    mockCategoryDbDao.loadCategories(channels);
    EasyMock.expectLastCall().once();

    mockChannelDbDao.loadSurvey(channels);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockChannelDbDao);

    // do test
    List<Channel> results = uut.find(channelIds);
    Channel result = results.get(0);
    // assertions
    assertEquals(expectedChannelUrl, result.getUrl());
    assertEquals(expectedFeedAtomUrl, result.getFeedUrlAtom());
    assertEquals(expectedFeedRssUrl, result.getFeedUrlRss());

    // verification
    EasyMock.verify(mockChannelUrlGenerator, mockChannelAtomFeedUrlGenerator, mockChannelRssFeedUrlGenerator);
  }

  private User createUser() {

    User user = new User();
    user.setId(USER_ID);

    return user;
  }

}
