/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationResourceAccessValidatorTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Unit tests for {@link CommunicationResourceAccessValidator}.
 */
public class CommunicationResourceAccessValidatorTest {

  private CommunicationResourceAccessValidator uut;

  @Before
  public void setUp() {
    uut = new CommunicationResourceAccessValidator();
  }

  /**
   * Test validation communication provided by MediaZone.
   */
  @Test
  public void testValidateMediaZoneCommunication() {

    // setup
    Communication communication = new Communication();
    communication.setProvider(new Provider(Provider.MEDIAZONE));

    // do test
    try {
      uut.validate(communication);
      // assert
      Assert.fail();
    } catch (ApplicationException ae) {
      Assert.assertEquals("NotAllowedToAccessMediaZoneCommunication", ae.getUserErrorCode());
    }
  }

  /**
   * Test validation communication provided by BrightTALK.
   */
  @Test
  public void testValidateBrighttalkCommunication() {

    // setup
    Communication communication = new Communication();
    communication.setProvider(new Provider(Provider.BRIGHTTALK));

    // do test
    try {
      uut.validate(communication);
      // assert
    } catch (ApplicationException ae) {
      Assert.fail();
    }
  }
}
