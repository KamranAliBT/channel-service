/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultFeatureServiceTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.User.Role;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.FeatureAuthorisation;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.queue.AuditQueue;
import com.brighttalk.service.channel.business.service.channel.DefaultFeatureService;
import com.brighttalk.service.channel.business.service.channel.FeaturesPostProcessor;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.FeatureAuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.database.FeatureDbDao;
import com.brighttalk.service.utils.DomainFactoryUtils;

/**
 * Unit tests for {@link DefaultFeatureService}.
 */
public class DefaultFeatureServiceTest {

  private static final Long USER_ID = 123L;

  private FeatureDbDao mockFeatureDbDao;

  private FeatureAuthorisation mockFeatureAuthorisation;

  private AuditQueue mockAuditQueue;

  private AuditRecordDtoBuilder mockFeatureAuditRecordDtoBuilder;

  private FeaturesPostProcessor mockFeaturesPostProcessor;

  private DefaultFeatureService uut;

  private Channel channel;

  @Before
  public void setUp() {
    uut = new DefaultFeatureService();

    mockFeatureDbDao = EasyMock.createMock(FeatureDbDao.class);
    ReflectionTestUtils.setField(uut, "featureDbDao", mockFeatureDbDao);

    mockFeatureAuthorisation = EasyMock.createMock(FeatureAuthorisation.class);
    ReflectionTestUtils.setField(uut, "featureAuthorisation", mockFeatureAuthorisation);

    mockAuditQueue = EasyMock.createMock(AuditQueue.class);
    ReflectionTestUtils.setField(uut, "auditQueue", mockAuditQueue);

    mockFeatureAuditRecordDtoBuilder = EasyMock.createMock(FeatureAuditRecordDtoBuilder.class);
    ReflectionTestUtils.setField(uut, "featureAuditRecordDtoBuilder", mockFeatureAuditRecordDtoBuilder);

    mockFeaturesPostProcessor = EasyMock.createMock(FeaturesPostProcessor.class);
    ReflectionTestUtils.setField(uut, "featuresPostProcessor", mockFeaturesPostProcessor);

    channel = DomainFactoryUtils.createChannel();
  }

  @Test
  @SuppressWarnings("unchecked")
  public void updateFeaturesAsBrightTalkAdministrator() {
    // Set up
    User user = buildUser(Role.MANAGER);

    List<Feature> featuresToUpdate = new ArrayList<>();

    // Expectations
    ArrayList<Feature> updateFeatures = new ArrayList<>();
    updateFeatures.add(buildFeature(Type.ADS_DISPLAYED));
    updateFeatures.add(buildFeature(Type.EMAIL_CHANNEL_SUBSCRIPTION));
    updateFeatures.add(buildFeature(Type.BLOCKED_USER));

    Integer expectedFeaturesSize = updateFeatures.size();

    mockFeatureAuthorisation.authoriseForUpdate(isA(User.class), isA(Channel.class), isA(List.class));
    EasyMock.expectLastCall();

    EasyMock.expect(mockFeatureDbDao.findDefaultFeatureByChannelTypeId(isA(Long.class))).andReturn(updateFeatures);
    EasyMock.expect(mockFeatureDbDao.findByChannelId(isA(Long.class))).andReturn(updateFeatures);

    EasyMock.expect(mockFeatureDbDao.findByChannelId(isA(Long.class))).andReturn(updateFeatures);
    mockFeaturesPostProcessor.process(isA(Channel.class), isA(List.class));
    EasyMock.expectLastCall();
    EasyMock.expect(mockFeatureAuditRecordDtoBuilder.update(isA(Long.class), isA(Long.class),
        isA(String.class))).andReturn(new AuditRecordDto());
    mockAuditQueue.create(isA(AuditRecordDto.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockFeatureAuthorisation, mockFeatureDbDao, mockFeaturesPostProcessor,
        mockFeatureAuditRecordDtoBuilder, mockAuditQueue);

    // Do Test
    List<Feature> result = uut.update(user, channel, featuresToUpdate);

    // Assertions
    assertTrue(expectedFeaturesSize.equals(result.size()));
    EasyMock.verify(this.mockFeatureAuthorisation, mockFeatureDbDao, mockFeaturesPostProcessor,
        mockFeatureAuditRecordDtoBuilder, mockAuditQueue);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void updateFeaturesAsChannelOwnerManager() {
    // Set up
    User user = buildUser();

    List<Feature> featuresToUpdate = new ArrayList<>();

    // Expectations
    ArrayList<Feature> updatedFeatures = new ArrayList<>();
    updatedFeatures.add(buildFeature(Type.ADS_DISPLAYED));
    updatedFeatures.add(buildFeature(Type.EMAIL_CHANNEL_SUBSCRIPTION));
    updatedFeatures.add(buildFeature(Type.BLOCKED_USER));

    Integer expectedFeaturesSize = 1;

    mockFeatureAuthorisation.authoriseForUpdate(isA(User.class), isA(Channel.class), isA(List.class));
    EasyMock.expectLastCall();

    EasyMock.expect(mockFeatureDbDao.findDefaultFeatureByChannelTypeId(isA(Long.class))).andReturn(updatedFeatures);
    EasyMock.expect(mockFeatureDbDao.findByChannelId(isA(Long.class))).andReturn(updatedFeatures);

    EasyMock.expect(mockFeatureDbDao.findByChannelId(isA(Long.class))).andReturn(updatedFeatures);

    mockFeaturesPostProcessor.process(isA(Channel.class), isA(List.class));
    EasyMock.expectLastCall();

    EasyMock.expect(mockFeatureAuditRecordDtoBuilder.update(isA(Long.class), isA(Long.class),
        isA(String.class))).andReturn(new AuditRecordDto());

    mockAuditQueue.create(isA(AuditRecordDto.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockFeatureAuthorisation, mockFeatureDbDao, mockFeaturesPostProcessor,
        mockFeatureAuditRecordDtoBuilder, mockAuditQueue);

    // Do Test
    List<Feature> result = uut.update(user, channel, featuresToUpdate);

    // Assertions
    assertTrue(expectedFeaturesSize.equals(result.size()));
    assertEquals(Type.EMAIL_CHANNEL_SUBSCRIPTION, result.iterator().next().getName());

    EasyMock.verify(this.mockFeatureAuthorisation, mockFeatureDbDao, mockFeaturesPostProcessor,
        mockFeatureAuditRecordDtoBuilder, mockAuditQueue);
  }

  /**
   * Tests {@link DefaultFeatureService#update(User, Channel, List)} in success case of updating channel features with a
   * list of new features where some features are different to the default features and the exsiting channel features.
   * <p>
   * Setup - This test update the given channel that has 2 features with following values for default and exsiting:<br>
   * 1) Default (emailCommRegistrationConfirmation - enabled and emailCommMissedYou - enabled).<br>
   * 2) Existing (emailCommRegistrationConfirmation - disabled and emailCommMissedYou - enabled) - Feature override
   * values.
   * <p>
   * And update the channel with the following new features values:<br>
   * 1) New (emailCommRegistrationConfirmation - enabled and emailCommMissedYou - disabled)
   * <p>
   * Expected Result: The 1st feature exsiting override value will be removed and default to enabled (since the new
   * feature different to the existing and the same as the default) and the 2nd feature will be updated to disabled and
   * override the exsiting feature value (since the new feature different to the default and exsiting value) - value has
   * been overridden from enabled to disabled.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void updateWithFeaturesDifferentToExistingAndDefaultFeatures() {
    // Set up
    User user = buildUser();

    ArrayList<Feature> newFeatures = new ArrayList<>();
    newFeatures.add(buildFeature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION, true));
    Feature updatedFeature = buildFeature(Type.EMAIL_COMM_MISSED_YOU, false);
    newFeatures.add(updatedFeature);

    List<Feature> defaultFeatures = new ArrayList<>();
    defaultFeatures.add(buildFeature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION, true));
    defaultFeatures.add(buildFeature(Type.EMAIL_COMM_MISSED_YOU, true));

    List<Feature> exsitingFeatures = new ArrayList<>();
    exsitingFeatures.add(buildFeature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION, false));
    exsitingFeatures.add(buildFeature(Type.EMAIL_COMM_MISSED_YOU, true));

    // Expectations
    ArrayList<Feature> expectedUpdatedFeatures = new ArrayList<>();
    expectedUpdatedFeatures.add(buildFeature(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION, true));
    expectedUpdatedFeatures.add(buildFeature(Type.EMAIL_COMM_MISSED_YOU, false));

    mockFeatureAuthorisation.authoriseForUpdate(isA(User.class), isA(Channel.class), isA(List.class));
    EasyMock.expectLastCall();
    EasyMock.expect(mockFeatureDbDao.findDefaultFeatureByChannelTypeId(isA(Long.class))).andReturn(defaultFeatures);
    EasyMock.expect(mockFeatureDbDao.findByChannelId(isA(Long.class))).andReturn(exsitingFeatures);
    mockFeatureDbDao.deleteFeatureOverridesByNames(channel.getId(),
        Arrays.asList(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION));
    EasyMock.expectLastCall();
    mockFeatureDbDao.update(channel, Arrays.asList(updatedFeature));
    EasyMock.expectLastCall();
    EasyMock.expect(mockFeatureAuditRecordDtoBuilder.update(isA(Long.class), isA(Long.class),
        isA(String.class))).andReturn(new AuditRecordDto());
    mockAuditQueue.create(isA(AuditRecordDto.class));
    EasyMock.expectLastCall();
    mockFeaturesPostProcessor.process(isA(Channel.class), isA(List.class));
    EasyMock.expectLastCall();

    EasyMock.expect(mockFeatureDbDao.findByChannelId(isA(Long.class))).andReturn(expectedUpdatedFeatures);

    EasyMock.replay(mockFeatureAuthorisation, mockFeatureDbDao, mockFeaturesPostProcessor,
        mockFeatureAuditRecordDtoBuilder, mockAuditQueue);

    // Do Test
    List<Feature> result = uut.update(user, channel, newFeatures);

    EasyMock.verify(this.mockFeatureAuthorisation, mockFeatureDbDao, mockFeaturesPostProcessor,
        mockFeatureAuditRecordDtoBuilder, mockAuditQueue);

    assertEquals(2, result.size());
  }

  private Feature buildFeature(final Type type) {
    return buildFeature(type, true);
  }

  private Feature buildFeature(final Type type, final boolean isEnabled) {
    Feature feature = new Feature(type);
    feature.setIsEnabled(isEnabled);
    return feature;
  }

  private User buildUser(final Role... roles) {
    return buildUser(USER_ID, roles);
  }

  private User buildUser(final Long userId, final Role... roles) {
    User user = new User(userId);
    user.setRoles(Arrays.asList(roles));
    return user;
  }

}
