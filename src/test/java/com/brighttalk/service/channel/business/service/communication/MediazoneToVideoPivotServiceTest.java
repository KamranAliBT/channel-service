/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationProviderServiceTest.java 85193 2014-10-24 15:54:02Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.configuration.Messages;
import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Format;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.error.DataCleanUpFailedException;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.RenditionDbDao;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;
import com.brighttalk.service.utils.UserErrorMessageMatcher;

public class MediazoneToVideoPivotServiceTest {

  private static final Long COMMUNICATION_ID = 666L;

  private MediazoneToVideoPivotService uut;

  private CommunicationDbDao mockCommunicationDao;

  private RenditionDbDao mockRenditionDbDao;

  private Messages mockMessages;

  private ResourceDbDao mockResourceDao;

  private ChannelFinder mockChannelFinder;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Before
  public void setUp() {

    uut = new MediazoneToVideoPivotService();

    mockResourceDao = EasyMock.createMock(ResourceDbDao.class);
    ReflectionTestUtils.setField(uut, "resourceDbDao", mockResourceDao);

    mockCommunicationDao = EasyMock.createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDao);

    mockRenditionDbDao = EasyMock.createMock(RenditionDbDao.class);
    ReflectionTestUtils.setField(uut, "renditionDbDao", mockRenditionDbDao);

    mockMessages = EasyMock.createMock(Messages.class);
    ReflectionTestUtils.setField(uut, "messages", mockMessages);

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    // need to do this, unfortunately, because there is no way to mock the validator
    addLoadFeaturesMocks();
  }

  private void addLoadFeaturesMocks() {
    Channel mockChannel = EasyMock.createMock(Channel.class);
    Feature feature1 = new Feature(Type.MEDIA_ZONES);
    feature1.setIsEnabled(true);
    EasyMock.expect(mockChannel.getFeature(Type.MEDIA_ZONES)).andReturn(feature1);

    Feature feature2 = new Feature(Type.ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS);
    feature2.setIsEnabled(true);
    EasyMock.expect(mockChannel.getFeature(Type.ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS)).andReturn(feature2);

    EasyMock.expect(mockChannelFinder.find(CommunicationBuilder.DEFAULT_MASTER_CHANNEL_ID)).andReturn(mockChannel);

    EasyMock.replay(mockChannel, mockChannelFinder);
  }

  @Test
  public void updateProviderToVideoWhenCommunicationIsMediazone() {

    Provider providerMediazone = new Provider(Provider.MEDIAZONE);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.PROCESSING).withProvider(
            providerMediazone).withFormat(Format.AUDIO).build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.VIDEO);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockCommunicationDao.removeMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao);

    // do test
    Provider result = uut.pivot(communication, provider);

    assertNotNull(result);
    assertEquals(Provider.VIDEO, result.getName());

    assertEquals(Provider.VIDEO, communication.getProvider().getName());
    assertEquals(Format.VIDEO, communication.getFormat());
    assertNull(communication.getPreviewUrl());
    assertNull(communication.getThumbnailUrl());
  }

  @Test
  public void deleteMediazoneAssetsOnProviderUpdateWhenCommunicationIsMediazone() {
    Provider providerMediazone = new Provider(Provider.MEDIAZONE);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.PROCESSING).withProvider(
            providerMediazone).withFormat(Format.AUDIO).build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.VIDEO);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockCommunicationDao.removeMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao);

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockCommunicationDao);
  }

  @Test
  public void deleteMediazoneAssetsOnProviderUpdateWhenCommunicationIsMediazoneAndCallFails() {

    Provider providerMediazone = new Provider(Provider.MEDIAZONE);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.PROCESSING).withProvider(
            providerMediazone).withFormat(Format.AUDIO).build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.VIDEO);

    String expectedErrorMessage = "expectedErrorMessage";
    EasyMock.expect(mockMessages.getValue(MediazoneToVideoPivotService.DELETE_MEDIAZONE_ASSETS_ERROR_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockCommunicationDao.removeMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test exception"));

    EasyMock.replay(mockCommunicationDao, mockMessages);

    thrown.expect(DataCleanUpFailedException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockCommunicationDao, mockMessages);
  }

  @Test
  public void updateProviderToMediazoneWhenCommunicationIsVideo() {

    Provider providerVideo = new Provider(Provider.VIDEO);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.RECORDED).withProvider(
            providerVideo).withFormat(Format.VIDEO).build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockRenditionDbDao.removeRenditionAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao);

    // do test
    Provider result = uut.pivot(communication, provider);

    assertNotNull(result);
    assertEquals(Provider.MEDIAZONE, result.getName());

    assertEquals(Provider.MEDIAZONE, communication.getProvider().getName());
    assertEquals(Format.VIDEO, communication.getFormat());
    assertEquals(Status.PROCESSING, communication.getStatus());
    assertNull(communication.getPreviewUrl());
    assertNull(communication.getThumbnailUrl());
  }

  @Test
  public void deleteRenditionAssetsOnUpdateProviderToMediazoneWhenCommunicationIsVideo() {

    Provider providerVideo = new Provider(Provider.VIDEO);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.RECORDED).withProvider(
            providerVideo).withFormat(Format.VIDEO).build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockRenditionDbDao.removeRenditionAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao);

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockCommunicationDao);
  }

  @Test
  public void deleteRenditionAssetsOnUpdateProviderToMediazoneWhenCommunicationIsVideoAndCallFails() {

    Provider providerVideo = new Provider(Provider.VIDEO);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.RECORDED).withProvider(
            providerVideo).withFormat(Format.VIDEO).build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockRenditionDbDao.removeRenditionAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test exception"));

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    final String expectedErrorMessage = "expectedErrorMessage";
    EasyMock.expect(mockMessages.getValue(MediazoneToVideoPivotService.DELETE_RENDITIONS_ERROR_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);

    EasyMock.expect(mockMessages.getValue(isA(String.class))).andReturn("someError");

    EasyMock.replay(mockCommunicationDao, mockRenditionDbDao, mockMessages);

    thrown.expect(DataCleanUpFailedException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.pivot(communication, provider);
  }

  @Test
  public void deleteCommunicationResourcesOnProviderUpdateToMediazoneWhenCommunicationIsVideo() {

    Provider providerVideo = new Provider(Provider.VIDEO);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.RECORDED).withProvider(
            providerVideo).withFormat(Format.VIDEO).build();
    communication.setId(COMMUNICATION_ID);

    final List<Resource> resources = new ArrayList<Resource>();

    final Long resourceId1 = 45l;
    Resource resource1 = new Resource(resourceId1);
    resources.add(resource1);

    final Long resourceId2 = 56l;
    Resource resource2 = new Resource(resourceId2);
    resources.add(resource2);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockRenditionDbDao.removeRenditionAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockResourceDao.findAll(COMMUNICATION_ID)).andReturn(resources);

    mockResourceDao.delete(COMMUNICATION_ID, resourceId1);
    EasyMock.expectLastCall().once();

    mockResourceDao.delete(COMMUNICATION_ID, resourceId2);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao, mockResourceDao);

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockResourceDao);
  }

  @Test
  public void deleteCommunicationResourcesOnProviderUpdateToMediazoneWhenCommunicationIsVideoAndCallFails() {

    Provider providerVideo = new Provider(Provider.VIDEO);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.RECORDED).withProvider(
            providerVideo).withFormat(Format.VIDEO).build();
    communication.setId(COMMUNICATION_ID);

    final List<Resource> resources = new ArrayList<Resource>();

    final Long resourceId1 = 45l;
    Resource resource1 = new Resource(resourceId1);
    resources.add(resource1);

    final Long resourceId2 = 56l;
    Resource resource2 = new Resource(resourceId2);
    resources.add(resource2);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockRenditionDbDao.removeRenditionAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockResourceDao.findAll(COMMUNICATION_ID)).andReturn(resources);

    mockResourceDao.delete(COMMUNICATION_ID, resourceId1);
    EasyMock.expectLastCall().once();

    mockResourceDao.delete(COMMUNICATION_ID, resourceId2);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test exception"));

    final String expectedErrorMessage = "expectedErrorMessage";
    EasyMock.expect(mockMessages.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);

    EasyMock.expect(mockMessages.getValue(isA(String.class))).andReturn("someError");

    EasyMock.replay(mockCommunicationDao, mockResourceDao, mockMessages);

    thrown.expect(DataCleanUpFailedException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockResourceDao);
  }

  @Test
  public void restoreMediazoneRelatedAssetsOnupdateProviderToMediazoneWhenCommunicationIsVideo() {

    Provider providerVideo = new Provider(Provider.VIDEO);

    Communication communication =
        new CommunicationBuilder().withDefaultValues().withStatus(Status.RECORDED).withProvider(
            providerVideo).withFormat(Format.VIDEO).build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockRenditionDbDao.removeRenditionAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao);

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockCommunicationDao);
  }

  private Provider getProvider(final String value) {
    return new Provider(value);
  }
}