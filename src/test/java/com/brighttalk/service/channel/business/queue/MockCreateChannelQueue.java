/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MockCreateChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Implementation of a Delete Channel Queue that does not actually use a queue.
 * 
 * The required processing starts straight away.
 * 
 * This is used for unit testing.
 */
public class MockCreateChannelQueue implements CreateChannelQueue {

  @Override
  public void createChannel(Long channelId) {
    // TODO Auto-generated method stub
  }
}