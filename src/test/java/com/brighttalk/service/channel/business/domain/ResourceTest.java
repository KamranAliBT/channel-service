package com.brighttalk.service.channel.business.domain;

import static junit.framework.Assert.*;

import org.junit.Test;

/**
 * Unit tests for {@link Resource}.
 */
public class ResourceTest {

  private Resource uut;

  @Test
  public void testMergeIdNotChanged() {
    // Set up
    Long existingValue = 1234L;
    Long updatedValue = 5678L;

    uut = new Resource();
    uut.setId(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setId(updatedValue);

    // Expectations
    Long expectedResult = existingValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getId());
  }

  @Test
  public void testMergeTypeNotChanged() {
    // Set up
    Resource.Type existingValue = Resource.Type.FILE;
    Resource.Type updatedValue = Resource.Type.LINK;

    uut = new Resource();
    uut.setType(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setType(updatedValue);

    // Expectations
    Resource.Type expectedResult = existingValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getType());
  }

  @Test
  public void testMergeHostedNotChanged() {
    // Set up
    Resource.Hosted existingValue = Resource.Hosted.EXTERNAL;
    Resource.Hosted updatedValue = Resource.Hosted.INTERNAL;

    uut = new Resource();
    uut.setType(Resource.Type.FILE);
    uut.setHosted(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setType(Resource.Type.FILE);
    updatedResource.setHosted(updatedValue);

    // Expectations
    Resource.Hosted expectedResult = existingValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getHosted());
  }

  @Test
  public void testMergeTitleChanged() {
    // Set up
    String existingValue = "1234";
    String updatedValue = "abcdef";

    uut = new Resource();
    uut.setTitle(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setTitle(updatedValue);

    // Expectations
    String expectedResult = updatedValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getTitle());
  }

  @Test
  public void testMergeTitleNull() {
    // Set up
    String existingValue = "1234";
    String updatedValue = null;

    uut = new Resource();
    uut.setTitle(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setTitle(updatedValue);

    // Expectations
    String expectedResult = existingValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getTitle());
  }

  @Test
  public void testMergeDescriptionChanged() {
    // Set up
    String existingValue = "1234";
    String updatedValue = "abcdef";

    uut = new Resource();
    uut.setDescription(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setDescription(updatedValue);

    // Expectations
    String expectedResult = updatedValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getDescription());
  }

  @Test
  public void testMergeDescriptionNull() {
    // Set up
    String existingValue = "1234";
    String updatedValue = null;

    uut = new Resource();
    uut.setDescription(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setDescription(updatedValue);

    // Expectations
    String expectedResult = existingValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getDescription());
  }

  @Test
  public void testMergeUrlChanged() {
    // Set up
    String existingValue = "1234";
    String updatedValue = "abcdef";

    uut = new Resource();
    uut.setUrl(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setUrl(updatedValue);

    // Expectations
    String expectedResult = updatedValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getUrl());
  }

  @Test
  public void testMergeUrlNull() {
    // Set up
    String existingValue = "1234";
    String updatedValue = null;

    uut = new Resource();
    uut.setUrl(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setUrl(updatedValue);

    // Expectations
    String expectedResult = existingValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getUrl());
  }

  @Test
  public void testMergeFileSizeChanged() {
    // Set up
    Long existingValue = 1234L;
    Long updatedValue = 5678L;

    uut = new Resource();
    uut.setType(Resource.Type.FILE);
    uut.setSize(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setType(Resource.Type.FILE);
    updatedResource.setSize(updatedValue);

    // Expectations
    Long expectedResult = updatedValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getSize());
  }

  @Test
  public void testMergeFileSizeNull() {
    // Set up
    Long existingValue = 1234L;
    Long updatedValue = null;

    uut = new Resource();
    uut.setType(Resource.Type.FILE);
    uut.setSize(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setType(Resource.Type.FILE);
    updatedResource.setSize(updatedValue);

    // Expectations
    Long expectedResult = existingValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getSize());
  }

  @Test
  public void testMergeLinkSizeIgnored() {
    // Set up
    Long updatedValue = 5678L;

    uut = new Resource();
    uut.setType(Resource.Type.LINK);

    Resource updatedResource = new Resource();
    updatedResource.setSize(updatedValue);

    // Expectations
    Long expectedResult = null;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getSize());
  }

  @Test
  public void testMergeFileMimeTypeChanged() {
    // Set up
    String existingValue = "pqrstu";
    String updatedValue = "abcdef";

    uut = new Resource();
    uut.setType(Resource.Type.FILE);
    uut.setMimeType(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setType(Resource.Type.FILE);
    updatedResource.setMimeType(updatedValue);

    // Expectations
    String expectedResult = updatedValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getMimeType());
  }

  @Test
  public void testMergeFileMimeTypeNull() {
    // Set up
    String existingValue = "pqrstu";
    String updatedValue = null;

    uut = new Resource();
    uut.setType(Resource.Type.FILE);
    uut.setMimeType(existingValue);

    Resource updatedResource = new Resource();
    updatedResource.setType(Resource.Type.FILE);
    updatedResource.setMimeType(updatedValue);

    // Expectations
    String expectedResult = existingValue;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getMimeType());
  }

  @Test
  public void testMergeLinkMimeTypeIgnored() {
    // Set up
    String updatedValue = "abcdef";

    uut = new Resource();
    uut.setType(Resource.Type.LINK);

    Resource updatedResource = new Resource();
    updatedResource.setMimeType(updatedValue);

    // Expectations
    Long expectedResult = null;

    // Do Test
    uut.merge(updatedResource);

    // Assertions
    assertEquals(expectedResult, uut.getMimeType());
  }
}