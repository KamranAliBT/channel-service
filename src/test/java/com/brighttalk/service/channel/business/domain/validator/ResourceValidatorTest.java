/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceValidatorTest.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import org.junit.Test;

import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.error.InvalidResourceException;

/**
 * Unit tests for {@link ResourceValidator}.
 */
public class ResourceValidatorTest extends AbstractResourceTest {

  private ResourceValidator uut;

  @Test
  public void testValidateNotChangedType() {
    // Set up
    Resource existingResource = new Resource();
    existingResource.setType(Resource.Type.LINK);

    Resource newResource = new Resource();
    newResource.setType(Resource.Type.LINK);

    uut = new ResourceValidator(existingResource);

    // Expectations

    // Do Test
    uut.validate(newResource);

    // Assertions
  }

  @Test(expected = InvalidResourceException.class)
  public void testValidateChangedType() {
    // Set up
    Resource existingResource = new Resource();
    existingResource.setType(Resource.Type.LINK);

    Resource newResource = new Resource();
    newResource.setType(Resource.Type.FILE);

    uut = new ResourceValidator(existingResource);

    // Expectations

    // Do Test
    uut.validate(newResource);

    // Assertions
  }

  @Test
  public void testValidateNotChangedHosted() {
    // Set up
    Resource existingResource = new Resource();
    existingResource.setType(Resource.Type.FILE);
    existingResource.setHosted(Resource.Hosted.INTERNAL);

    Resource newResource = new Resource();
    newResource.setType(Resource.Type.FILE);
    newResource.setHosted(Resource.Hosted.INTERNAL);

    uut = new ResourceValidator(existingResource);

    // Expectations

    // Do Test
    uut.validate(newResource);

    // Assertions
  }

  @Test(expected = InvalidResourceException.class)
  public void testValidateChangedHosted() {
    // Set up
    Resource existingResource = new Resource();
    existingResource.setType(Resource.Type.FILE);
    existingResource.setHosted(Resource.Hosted.INTERNAL);

    Resource newResource = new Resource();
    newResource.setType(Resource.Type.FILE);
    newResource.setHosted(Resource.Hosted.EXTERNAL);

    uut = new ResourceValidator(existingResource);

    // Expectations

    // Do Test
    uut.validate(newResource);

    // Assertions
  }
}
