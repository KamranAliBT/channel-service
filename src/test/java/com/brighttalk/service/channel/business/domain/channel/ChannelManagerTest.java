package com.brighttalk.service.channel.business.domain.channel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.Date;

import org.junit.Test;

import com.brighttalk.common.user.User;

public class ChannelManagerTest {

  private static final Long CHANNEL_MANAGER_ID = 999L;

  private static final String USER_EMAIL_ADDRESS = "email@brighttalk.com";

  private static final boolean EMAIL_ALERTS = true;

  private static final Long USER_ID = 888L;

  private static final String USER_FIRST_NAME = "FirstName";

  private static final String USER_LAST_NAME = "LastName";

  private static final String USER_JOB_TITLE = "JobTitle";

  private static final String USER_COMPANY_NAME = "CompanyName";

  private static final String USER_EXTERNAL_ID = "ExternalId";

  private static final boolean USER_IS_COMPLETE = true;

  private static final boolean USER_IS_ACTIVE = true;

  private static final String USER_REALM_USER_ID = "RealmId";

  private static final String USER_TIME_ZONE = "TimeZone";

  private static final String USER_API_KEY = "ApiKey";

  private static final Date USER_CREATED = new Date();

  private static final Date USER_LAST_LOGIN = new Date();

  private static final String USER_TELEPHONE = "123456789";

  private static final String USER_LEVEL = "Level";

  private static final String USER_INDUSTRY = "Industry";

  private static final String USER_COMPANY_SIZE = "1-10";

  private static final String USER_STATE_REGION = "StateRegion";

  private static final String USER_COUNTRY = "Country";

  private ChannelManager uut;

  @Test
  public void testFields() {

    // Set up
    uut = buildChannelManager();

    // Assertions
    User user = uut.getUser();

    assertEquals(uut.getId(), CHANNEL_MANAGER_ID);
    assertEquals(uut.getEmailAlerts(), EMAIL_ALERTS);

    assertEquals(user.getId(), USER_ID);
    assertEquals(user.getExternalId(), USER_EXTERNAL_ID);
    assertEquals(user.getIsComplete(), USER_IS_COMPLETE);
    assertEquals(user.getIsActive(), USER_IS_ACTIVE);
    assertEquals(user.getRealmUserId(), USER_REALM_USER_ID);
    assertEquals(user.getFirstName(), USER_FIRST_NAME);
    assertEquals(user.getLastName(), USER_LAST_NAME);
    assertEquals(user.getEmail(), USER_EMAIL_ADDRESS);
    assertEquals(user.getTimeZone(), USER_TIME_ZONE);
    assertEquals(user.getApiKey(), USER_API_KEY);
    assertEquals(user.getCreated(), USER_CREATED);
    assertEquals(user.getLastLogin(), USER_LAST_LOGIN);
    assertEquals(user.getTelephone(), USER_TELEPHONE);
    assertEquals(user.getJobTitle(), USER_JOB_TITLE);
    assertEquals(user.getLevel(), USER_LEVEL);
    assertEquals(user.getCompanyName(), USER_COMPANY_NAME);
    assertEquals(user.getIndustry(), USER_INDUSTRY);
    assertEquals(user.getCompanySize(), USER_COMPANY_SIZE);
    assertEquals(user.getStateRegion(), USER_STATE_REGION);
    assertEquals(user.getCountry(), USER_COUNTRY);
  }

  @Test
  public void testEguals() {

    // Set up
    uut = buildChannelManager();
    ChannelManager channelManager = buildChannelManager();

    // Assertions
    assertEquals(uut, channelManager);
  }

  @Test
  public void testEqualsWhenSameInstances() {

    // Set up
    uut = buildChannelManager();
    ChannelManager channelManager = uut;

    // Assertions
    assertEquals(uut, channelManager);
  }

  @Test
  public void testEgualsWhenDiferentInstances() {

    // Set up
    uut = buildChannelManager();
    ChannelManager channelManager = buildChannelManager();

    // Assertions
    assertEquals(uut, channelManager);
  }

  @Test
  public void testEgualsWhenDiferent() {

    // Set up
    Long channelManagerId = 123L;

    uut = buildChannelManager();
    ChannelManager channelManager = buildChannelManager();
    channelManager.setId(channelManagerId);

    // Assertions
    assertNotSame(uut, channelManager);
  }

  private ChannelManager buildChannelManager() {

    User user = buildUser();

    ChannelManager channelManager = new ChannelManager();
    channelManager.setId(CHANNEL_MANAGER_ID);
    channelManager.setUser(user);
    channelManager.setEmailAlerts(EMAIL_ALERTS);

    return channelManager;
  }

  private User buildUser() {

    User user = new User();
    user.setId(USER_ID);
    user.setExternalId(USER_EXTERNAL_ID);
    user.setIsComplete(USER_IS_COMPLETE);
    user.setIsActive(USER_IS_ACTIVE);
    user.setRealmUserId(USER_REALM_USER_ID);
    user.setFirstName(USER_FIRST_NAME);
    user.setLastName(USER_LAST_NAME);
    user.setEmail(USER_EMAIL_ADDRESS);
    user.setTimeZone(USER_TIME_ZONE);
    user.setApiKey(USER_API_KEY);
    user.setCreated(USER_CREATED);
    user.setLastLogin(USER_LAST_LOGIN);
    user.setTelephone(USER_TELEPHONE);
    user.setJobTitle(USER_JOB_TITLE);
    user.setLevel(USER_LEVEL);
    user.setCompanyName(USER_COMPANY_NAME);
    user.setIndustry(USER_INDUSTRY);
    user.setCompanySize(USER_COMPANY_SIZE);
    user.setStateRegion(USER_STATE_REGION);
    user.setCountry(USER_COUNTRY);

    return user;
  }
}