/**
 * *****************************************************************************
 * * Copyright BrightTALK Ltd, 2012.
 * * All Rights Reserved.
 * * Id: PaginatedChannelFeedTest.java
 * *****************************************************************************
 * @package com.brighttalk.service.channel.business.domain
 */
package com.brighttalk.service.channel.business.domain.channel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationType;
import com.brighttalk.service.channel.presentation.util.ChannelFeedSearchCriteriaBuilder;

/**
 * Unit tests for {@link PaginatedChannelFeed}.
 */
public class PaginatedChannelFeedTest {

  private final static Long CHANNEL_ID = new Long(1);

  private PaginatedChannelFeed uut;

  private final List<Communication> communications = new ArrayList<Communication>();

  @Before
  public void setUp() {

    CommunicationConfiguration comm1Config = new CommunicationConfiguration(1L);

    CommunicationConfiguration comm2Config = new CommunicationConfiguration(2L);

    CommunicationConfiguration comm3Config = new CommunicationConfiguration(3L);

    CommunicationSyndication communicationSyndication4 = new CommunicationSyndication();
    communicationSyndication4.setSyndicationType(SyndicationType.OUT);
    CommunicationConfiguration comm4Config = new CommunicationConfiguration(4L);
    comm4Config.setCommunicationSyndication(communicationSyndication4);

    CommunicationSyndication communicationSyndication5 = new CommunicationSyndication();
    communicationSyndication5.setSyndicationType(SyndicationType.OUT);
    CommunicationConfiguration comm5Config = new CommunicationConfiguration(5L);
    comm5Config.setCommunicationSyndication(communicationSyndication5);

    Communication communication1 = new Communication(1L);
    Communication communication2 = new Communication(2L);
    Communication communication3 = new Communication(3L);
    Communication communication4 = new Communication(4L);
    Communication communication5 = new Communication(5L);

    communication1.setConfiguration(comm1Config);
    communication2.setConfiguration(comm2Config);
    communication3.setConfiguration(comm3Config);
    communication4.setConfiguration(comm4Config);
    communication5.setConfiguration(comm5Config);

    communications.add(communication1);
    communications.add(communication2);
    communications.add(communication3);
    communications.add(communication4);
    communications.add(communication5);

    int pageNumber = 2;
    int pageSize = 5;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    uut = new PaginatedChannelFeed(new Channel(CHANNEL_ID), communications, 100, searchCriteria);

  }

  @After
  public void tearDown() {

  }

  @Test
  public void testConstruction() {
    assertEquals("incorrect construction - channel id wrong", new Long(CHANNEL_ID), uut.getChannel().getId());
    assertEquals("incorrect construction - communications wrong", 5, uut.getAllCommunications().size());
    assertEquals("incorrect construction - total no of comms wrong", 100, uut.getTotalNumberOfCommunications());
    assertEquals("incorrect construction - current page no wrong", 2, uut.getCurrentPageNumber());
    assertEquals("incorrect construction - page size wrong", 5, uut.getPageSize());
  }

  @Test
  public void testGetCommunications() {
    assertEquals("incorrect number of communications", 5, uut.getAllCommunications().size());
  }

  @Test
  public void testGetNonSyndicatedCommunications() {
    assertEquals("incorrect number of non synd communications", 3, uut.getNonSyndicatedCommunications().size());
  }

  @Test
  public void testGetSyndicatedOutCommunications() {
    assertEquals("incorrect number of synd out communications", 2, uut.getSyndicatedOutCommunications().size());
  }

  @Test
  public void testLastPageNumberIsAsExpected() {
    assertEquals("incorrect last page number", 20, uut.getLastPageNumber());
  }

  @Test
  public void testFirstPageNumberIsAsExpected() {
    assertEquals("incorrect first page number", 1, uut.getFirstPageNumber());
  }

  @Test
  public void testCurrentPageNumberIsAsExpected() {
    assertEquals("incorrect current page number", 2, uut.getCurrentPageNumber());
  }

  @Test
  public void testIsFirstPageIsAsExpected() {
    assertFalse("incorrect  is first page flag", uut.isFirstPage());
  }

  @Test
  public void testIsLastPageIsAsExpected() {
    assertFalse("incorrect  is last page flag", uut.isLastPage());
  }

  @Test
  public void testIsEmptyIsAsExpected() {
    assertFalse("expected a non empty feed", uut.isEmpty());
  }

  // Feed set up for last page - check behaviour around page number generation
  @Test
  public void testLastPageReturnsTrueWhenLastPage() {

    int pageNumber = 10;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertTrue("expected last page to be true", feed.isLastPage());
  }

  @Test
  public void testFirstPageReturnsFalseWhenLastPage() {

    int pageNumber = 10;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertFalse("expected first page to be true", feed.isFirstPage());
  }

  @Test
  public void testCurrentPageNumberReturnsLastPageValueWhenLastPage() {

    int pageNumber = 10;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect current page", 10, feed.getCurrentPageNumber());
  }

  @Test
  public void testFirstPageNumberReturnsFirstPageValueWhenLastPage() {

    int pageNumber = 10;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect first page", 1, feed.getFirstPageNumber());
  }

  @Test
  public void testLastPageNumberReturnsLastPageValueWhenLastPage() {

    int pageNumber = 10;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect last page", 10, feed.getLastPageNumber());
  }

  @Test
  public void testNextPageNumberReturnsLastPageValueWhenLastPage() {

    int pageNumber = 10;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect next page", 10, feed.getNextPageNumber());
  }

  @Test
  public void testPreviusPageNumberReturnsLastPageMinus1ValueWhenLastPage() {

    int pageNumber = 10;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect previous page", 9, feed.getPreviousPageNumber());
  }

  // Feed set up for first page - - check behaviour around page number generation
  @Test
  public void testLastPageReturnsFalseWhenFirstPage() {

    int pageNumber = 1;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertFalse("expected last page to be false", feed.isLastPage());
  }

  @Test
  public void testFirstPageReturnsTrueWhenFirstPage() {

    int pageNumber = 1;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertTrue("expected first page to be true", feed.isFirstPage());
  }

  @Test
  public void testCurrentPageNumberReturnsFirstPageValueWhenLastPage() {

    int pageNumber = 1;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect current page", 1, feed.getCurrentPageNumber());
  }

  @Test
  public void testFirstPageNumberReturnsFirstPageValueWhenFirstPage() {

    int pageNumber = 1;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect first page", 1, feed.getFirstPageNumber());
  }

  @Test
  public void testLastPageNumberReturnsLastPageValueWhenFirstPage() {

    int pageNumber = 1;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect last page", 10, feed.getLastPageNumber());
  }

  @Test
  public void testNextPageNumberReturnsFirstPageValuePlusOneWhenFirstPage() {

    int pageNumber = 1;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect next page", 2, feed.getNextPageNumber());
  }

  @Test
  public void testPreviusPageNumberReturnsFirsPageValueWhenFirstPage() {

    int pageNumber = 1;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect previous page", 1, feed.getPreviousPageNumber());
  }

  // Feed set up somewhere in the middle - check behaviour around page number generation

  @Test
  public void testLastPageReturnsFalseWhenMiddlePage() {

    int pageNumber = 5;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertFalse("expected last page to be false", feed.isLastPage());
  }

  @Test
  public void testFirstPageReturnsFalseWhenMiddlePage() {

    int pageNumber = 5;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertFalse("expected first page to be false", feed.isFirstPage());
  }

  @Test
  public void testCurrentPageNumberReturnsCorrectValueWhenMiddlePage() {

    int pageNumber = 5;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect current page", 5, feed.getCurrentPageNumber());
  }

  @Test
  public void testFirstPageNumberReturnsFirstPageValueWhenMiddlePage() {

    int pageNumber = 5;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect first page", 1, feed.getFirstPageNumber());
  }

  @Test
  public void testLastPageNumberReturnsLastPageValueWhenMiddlePage() {

    int pageNumber = 5;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect last page", 10, feed.getLastPageNumber());
  }

  @Test
  public void testNextPageNumberReturnsCorrectValuePlusOneWhenMiddlePage() {

    int pageNumber = 5;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect next page", 6, feed.getNextPageNumber());
  }

  @Test
  public void testPreviusPageNumberReturnsCorrectValueWhenMiddlePage() {

    int pageNumber = 5;
    int pageSize = 100;
    ChannelFeedSearchCriteria searchCriteria = newChannelFeedSearchCriteria(pageNumber, pageSize);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID), getCommunications(100), 1000,
        searchCriteria);
    assertEquals("incorrect previous page", 4, feed.getPreviousPageNumber());
  }

  // test empty feed operation

  @Test
  public void testFeedIndicatesEmptyWhenEmpty() {
    PaginatedChannelFeed feed = new PaginatedChannelFeed();
    assertTrue("expected empty feed", feed.isEmpty());
  }

  @Test
  public void testToStringOnEmptyFeedDoesNotThrowNullPointerExceptions() {
    PaginatedChannelFeed feed = new PaginatedChannelFeed();
    assertFalse("expected to string to be ok", feed.toString().isEmpty());
  }

  @Test
  public void testFeedIndicatesNotEmptyWhenFeedOnlyHasAChannel() {
    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID));
    assertFalse("did not expect empty feed", feed.isEmpty());
  }

  @Test
  public void testFeedIndicatesNotEmptyWhenFeedHasAChannelAndCommunications() {
    assertFalse("did not expect empty feed", uut.isEmpty());
  }

  @Test
  public void testFeedHasEmptyListOfCommunications() {
    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID));

    assertNotNull("expected a not null list of communications", feed.getAllCommunications());
    assertEquals("expected zero communications in list", 0, feed.getAllCommunications().size());

    assertNotNull("expected a not null list of non synd communications", feed.getNonSyndicatedCommunications());
    assertEquals("expected zero communications in list of non synd comms", 0,
        feed.getNonSyndicatedCommunications().size());

    assertNotNull("expected a not null list of synd communications", feed.getSyndicatedOutCommunications());
    assertEquals("expected zero communications in list of synd comms", 0, feed.getSyndicatedOutCommunications().size());
  }

  /**
   * Tests {@link PaginatedChannelFeed#getCommunicationStatusFilter()} when the instance of
   * {@link ChannelFeedSearchCriteria} has not been set. This should return an empty list.
   */
  @Test
  public void getCommunicationStatusFilterWhenSearchCriteriaIsNull() {

    // Set up
    List<CommunicationStatusFilter> expectedCommunicationStatusFilter = Collections.<CommunicationStatusFilter>emptyList();

    // Expectations

    // Do test
    PaginatedChannelFeed feed = new PaginatedChannelFeed(new Channel(CHANNEL_ID));
    List<CommunicationStatusFilter> actualCommunicationStatusFilter = feed.getCommunicationStatusFilter();

    // Assertions
    assertEquals(expectedCommunicationStatusFilter, actualCommunicationStatusFilter);
  }

  /**
   * Tests {@link PaginatedChannelFeed#getCommunicationStatusFilter()} when the instance of
   * {@link ChannelFeedSearchCriteria} has not been set. This should return the list contained in the search criteria.
   */
  @Test
  public void getCommunicationStatusFilterWhenSearchCriteriaIsNotNull() {

    // Set up
    List<CommunicationStatusFilter> communicationStatusFilter = Arrays.asList(CommunicationStatusFilter.UPCOMING);

    Channel channel = new Channel(CHANNEL_ID);
    List<Communication> communications = Collections.<Communication>emptyList();
    int totalNumberOfCommunications = 0;

    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteriaBuilder().buildWithDefaults();
    searchCriteria.setCommunicationStatusFilter(communicationStatusFilter);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(channel, communications, totalNumberOfCommunications,
        searchCriteria);

    // Expectations

    // Do test
    List<CommunicationStatusFilter> actualCommunicationStatusFilter = feed.getCommunicationStatusFilter();

    // Assertions
    assertEquals(communicationStatusFilter, actualCommunicationStatusFilter);
  }

  /**
   * Build a list of communications to add to the feed
   * 
   * @param numberOfCommunications the number of communications to build.
   * @return the communications
   */
  private List<Communication> getCommunications(final int numberOfCommunications) {
    List<Communication> communications = new ArrayList<Communication>();
    for (int i = 0; i < numberOfCommunications; i++) {
      this.communications.add(new Communication(new Long(i)));
    }
    return communications;
  }

  private ChannelFeedSearchCriteria newChannelFeedSearchCriteria(int pageNumber, int pageSize) {

    ChannelFeedSearchCriteria searchCriteria = new ChannelFeedSearchCriteria();
    searchCriteria.setPageNumber(pageNumber);
    searchCriteria.setPageSize(pageSize);

    return searchCriteria;
  }

}
