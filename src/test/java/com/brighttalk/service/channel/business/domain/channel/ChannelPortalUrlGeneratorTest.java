/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelPortalUrlGeneratorTest.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelPortalUrlGenerator;

public class ChannelPortalUrlGeneratorTest {

  private static final String DEFAULT_URL_TEMPLATE = "http://www.local.brighttalk.net/channel/{channelId}/url";
  
  private static final String CHANNEL_TAG = "{channelId}";
  
  private static final Long CHANNEL_ID = 666l;
  
  private ChannelPortalUrlGenerator uut;
  
  
  @Before
  public void setUp() {
    
    uut = new ChannelPortalUrlGenerator();
    uut.setChannelUriTemplate( DEFAULT_URL_TEMPLATE );
  }
  
  @Test
  public void generateDefault() {
    
    Channel channel = new Channel();
    channel.setId( CHANNEL_ID );
    List<Feature> features = new ArrayList<Feature>();
    features.add( new Feature( Type.CUSTOM_URL ) );
    channel.setFeatures( features );
    
    //do test
    String result = uut.generate( channel );
    
    final String expectedResult = DEFAULT_URL_TEMPLATE.replace( CHANNEL_TAG, CHANNEL_ID.toString() );
    assertEquals( expectedResult, result );
  }
  
  @Test
  public void generateDefaultWhenNoCustomValueSet() {
    
    Channel channel = new Channel();
    channel.setId( CHANNEL_ID );
    
    List<Feature> features = new ArrayList<Feature>();
    Feature customUrlFeature = new Feature( Type.CUSTOM_URL );
    customUrlFeature.setIsEnabled( Boolean.TRUE );
    features.add( customUrlFeature );
    
    channel.setFeatures( features );
    
    //do test
    String result = uut.generate( channel );
    
    final String expectedResult = DEFAULT_URL_TEMPLATE.replace( CHANNEL_TAG, CHANNEL_ID.toString() );
    assertEquals( expectedResult, result );
  }
  

  @Test
  public void generateEmptyCustom() {
    
    Channel channel = new Channel();
    channel.setId( CHANNEL_ID );
    
    List<Feature> features = new ArrayList<Feature>();
    Feature customUrlFeature = new Feature( Type.CUSTOM_URL );
    customUrlFeature.setIsEnabled( Boolean.TRUE );
    customUrlFeature.setValue( ChannelPortalUrlGenerator.CHANNEL_URL_EMPTY_VALUE );
    features.add( customUrlFeature );
    
    channel.setFeatures( features );
    
    //do test
    String result = uut.generate( channel );
    
    final String expectedResult = "";
    assertEquals( expectedResult, result );
  }

  @Test
  public void generateCustom() {
    
    Channel channel = new Channel();
    channel.setId( CHANNEL_ID );
    
    final String expectedCustomUrl = "http://www.local.brighttalk.net/customUrl";
    
    List<Feature> features = new ArrayList<Feature>();
    Feature customUrlFeature = new Feature( Type.CUSTOM_URL );
    customUrlFeature.setIsEnabled( Boolean.TRUE );
    customUrlFeature.setValue( expectedCustomUrl );
    features.add( customUrlFeature );
    
    channel.setFeatures( features );
    
    //do test
    String result = uut.generate( channel );
    
    assertEquals( expectedCustomUrl, result );
  }
}