/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CalendarUrlGeneratorTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;

public class CalendarUrlGeneratorTest {

  private static final long COMMUNICATION_ID = 666l;
  
  private static final long CHANNEL_ID = 123l;
  
  
  private CalendarUrlGenerator uut;
  
  private Communication communication;
  
  private Channel channel;
  
  @Before
  public void setUp() {
    
    uut = new CalendarUrlGenerator();
    communication = new Communication( COMMUNICATION_ID );
    channel = new Channel( CHANNEL_ID );
  }
  
  @Test
  public void generate() {
  
    final String fullTemplate = "http://local.brighttalk.net/channel/{channelId}/comunication/{communicationId}";
    uut.setCalendarUriTemplate( fullTemplate );
    
    //do test
    String result = uut.generate( channel, communication );
    
    final String expectedUrl = "http://local.brighttalk.net/channel/" + CHANNEL_ID + "/comunication/" + COMMUNICATION_ID;
    assertEquals( expectedUrl, result );
  }

  @Test
  public void generateCommunicationIdTagOnly() {
    
    final String communicationOnlyTemplate = "http://local.brighttalk.net/comunication/{communicationId}";
    uut.setCalendarUriTemplate( communicationOnlyTemplate );
    
    //do test
    String result = uut.generate( channel, communication );
    
    final String expectedUrl = "http://local.brighttalk.net/comunication/" + COMMUNICATION_ID;
    assertEquals( expectedUrl, result );
  }
  
  @Test
  public void generateChannelIdTagOnly() {
  
    final String channelIdOnlyTemplate = "http://local.brighttalk.net/channel/{channelId}/calendar";
    uut.setCalendarUriTemplate( channelIdOnlyTemplate );
    
    //do test
    String result = uut.generate( channel, communication );
    
    final String expectedUrl = "http://local.brighttalk.net/channel/" + CHANNEL_ID + "/calendar";
    assertEquals( expectedUrl, result );
  }
  
  @Test
  public void generateNoTags() {
  
    final String noTagsTemplate = "http://local.brighttalk.net/channel/calendar";
    uut.setCalendarUriTemplate( noTagsTemplate );
    
    //do test
    String result = uut.generate( channel, communication );
    
    final String expectedUrl = noTagsTemplate;
    assertEquals( expectedUrl, result );
  }
}