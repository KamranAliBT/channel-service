/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationServiceTest.java 72595 2014-01-15 13:37:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.easymock.EasyMock.createMock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.easymock.EasyMock;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.common.utils.JobIdGenerator;
import com.brighttalk.service.channel.business.domain.CalendarUrlGenerator;
import com.brighttalk.service.channel.business.domain.ContentPlan;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.Keywords;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationPortalUrlGenerator;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.error.InvalidCommunicationException;
import com.brighttalk.service.channel.business.error.ProviderNotSupportedException;
import com.brighttalk.service.channel.business.queue.EmailQueuer;
import com.brighttalk.service.channel.business.queue.ImageConverterQueuer;
import com.brighttalk.service.channel.business.queue.LesQueuer;
import com.brighttalk.service.channel.business.queue.NotificationQueuer;
import com.brighttalk.service.channel.business.queue.SummitQueuer;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.booking.HDBookingServiceDao;
import com.brighttalk.service.channel.integration.database.CommunicationCategoryDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationConfigurationDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.RenditionDbDao;
import com.brighttalk.service.channel.integration.database.ScheduledPublicationDbDao;
import com.brighttalk.service.channel.integration.database.syndication.SyndicationDbDao;
import com.brighttalk.service.channel.integration.email.EmailServiceDao;
import com.brighttalk.service.channel.integration.les.LiveEventServiceDao;
import com.brighttalk.service.channel.integration.summit.SummitServiceDao;

/**
 * Unit tests for {@link CommunicationUpdateService}.
 */
public class HDCommunicationUpdateServiceTest extends AbstractCommunicationServiceTest {

  private HDCommunicationUpdateService uut;

  private static final Long TIME_TO_RESCHEDULE = 1800L;
  private static final Long RESCHEDULE_WINDOW = 1800L;

  @Before
  public void setUp() {

    uut = new HDCommunicationUpdateService();
    uut.timeToResourceAllocation = TIME_TO_RESCHEDULE;
    uut.rescheduleWindow = RESCHEDULE_WINDOW;

    mockCommunicationSyndicationService = EasyMock.createMock(CommunicationSyndicationService.class);
    ReflectionTestUtils.setField(uut, "communicationSyndicationService", mockCommunicationSyndicationService);

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    mockCommunicationFinder = EasyMock.createMock(CommunicationFinder.class);
    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);

    mockContentPlan = EasyMock.createMock(ContentPlan.class);
    ReflectionTestUtils.setField(uut, "contentPlan", mockContentPlan);

    mockCommunicationDao = createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDao);

    mockCommunicationConfigurationDbDao = createMock(CommunicationConfigurationDbDao.class);
    ReflectionTestUtils.setField(uut, "configurationDbDao", mockCommunicationConfigurationDbDao);

    mockCommunicationCategoryDbDao = createMock(CommunicationCategoryDbDao.class);
    ReflectionTestUtils.setField(uut, "categoryDbDao", mockCommunicationCategoryDbDao);

    mockSyndicationDbDao = createMock(SyndicationDbDao.class);
    ReflectionTestUtils.setField(uut, "syndicationDbDao", mockSyndicationDbDao);

    mockRenditionDbDao = createMock(RenditionDbDao.class);
    ReflectionTestUtils.setField(uut, "renditionDbDao", mockRenditionDbDao);

    mockEmailServiceDao = EasyMock.createMock(EmailServiceDao.class);
    ReflectionTestUtils.setField(uut, "emailServiceDao", mockEmailServiceDao);

    mockHDBookingServiceDao = EasyMock.createMock(HDBookingServiceDao.class);
    ReflectionTestUtils.setField(uut, "hDBookingServiceDao", mockHDBookingServiceDao);

    mockLiveEventServiceDao = EasyMock.createMock(LiveEventServiceDao.class);
    ReflectionTestUtils.setField(uut, "liveEventServiceDao", mockLiveEventServiceDao);

    mockSummitServiceDao = EasyMock.createMock(SummitServiceDao.class);
    ReflectionTestUtils.setField(uut, "summitServiceDao", mockSummitServiceDao);

    mockEmailQueuer = EasyMock.createMock(EmailQueuer.class);
    ReflectionTestUtils.setField(uut, "emailQueuer", mockEmailQueuer);

    mockLesQueuer = EasyMock.createMock(LesQueuer.class);
    ReflectionTestUtils.setField(uut, "lesQueuer", mockLesQueuer);

    mockImageConverterQueuer = EasyMock.createMock(ImageConverterQueuer.class);
    ReflectionTestUtils.setField(uut, "imageConverterQueuer", mockImageConverterQueuer);

    mockNotificationQueuer = EasyMock.createMock(NotificationQueuer.class);
    ReflectionTestUtils.setField(uut, "notificationQueuer", mockNotificationQueuer);

    mockSummitQueuer = EasyMock.createMock(SummitQueuer.class);
    ReflectionTestUtils.setField(uut, "summitQueuer", mockSummitQueuer);

    mockScheduledPublicationDbDao = EasyMock.createMock(ScheduledPublicationDbDao.class);
    ReflectionTestUtils.setField(uut, "scheduledPublicationDbDao", mockScheduledPublicationDbDao);

    mockIdGenerator = EasyMock.createMock(JobIdGenerator.class);
    ReflectionTestUtils.setField(uut, "jobIdGenerator", mockIdGenerator);

    mockKeywords = EasyMock.createMock(Keywords.class);
    ReflectionTestUtils.setField(uut, "keywords", mockKeywords);

    mockCalendarUrlGenerator = EasyMock.createMock(CalendarUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "calendarUrlGenerator", mockCalendarUrlGenerator);

    mockCommunicationPortalUrlGenerator = EasyMock.createMock(CommunicationPortalUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "communicationPortalUrlGenerator", mockCommunicationPortalUrlGenerator);

  }

  @Test
  public void lesRescheduleCallback() {
    // set up
    Communication communication = new CommunicationBuilder().withDefaultValues().build();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(COMMUNICATION_ID)).andReturn(communication);

    mockLiveEventServiceDao.reschedule(communication);
    EasyMock.expectLastCall();

    // exercise
    uut.lesRescheduleCallback(COMMUNICATION_ID);
  }

  @Test
  public void summitRescheduleCallback() {
    // set up
    Communication communication = new CommunicationBuilder().withDefaultValues().build();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(COMMUNICATION_ID)).andReturn(communication);

    mockSummitServiceDao.inform(communication);
    EasyMock.expectLastCall();

    // exercise
    uut.summitRescheduleCallback(COMMUNICATION_ID);
  }

  @Test
  public void emailRescheduleCallback() {
    // set up
    Long syndicatedChannelId = 456L;
    Channel channel = new ChannelBuilder().withDefaultValues().build();

    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().build();
    communicationConfiguration.setId(COMMUNICATION_ID);
    communicationConfiguration.setChannelId(syndicatedChannelId);

    Provider provider = new Provider(Provider.BRIGHTTALK_HD);

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).build();
    communication.setCalendarUrlGenerator(mockCalendarUrlGenerator);
    communication.setCommunicationPortalUrlGenerator(mockCommunicationPortalUrlGenerator);
    communication.setConfiguration(communicationConfiguration);

    Communication syndicatedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        syndicatedChannelId).withProvider(provider).build();

    List<Communication> communications = new ArrayList<Communication>();
    communications.add(communication);
    communications.add(syndicatedCommunication);

    CommunicationSyndication communicationSyndication = new CommunicationSyndication();
    communicationSyndication.setChannel(new Channel(syndicatedChannelId));
    List<CommunicationSyndication> consumerSyndications = new ArrayList<CommunicationSyndication>();
    consumerSyndications.add(communicationSyndication);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(COMMUNICATION_ID)).andReturn(communication);

    EasyMock.expect(mockSyndicationDbDao.findConsumerChannelsSyndication(communication.getId())).andReturn(
        consumerSyndications);

    EasyMock.expect(mockCommunicationConfigurationDbDao.get(syndicatedChannelId, communication.getId())).andReturn(
        communicationConfiguration);

    EasyMock.expect(mockChannelFinder.find(syndicatedChannelId)).andReturn(channel);

    EasyMock.expect(mockCalendarUrlGenerator.generate(channel, communication)).andReturn("calendar url");
    EasyMock.expect(mockCommunicationPortalUrlGenerator.generate(channel, communication)).andReturn(
        "communication portal url");
    EasyMock.expect(mockCommunicationPortalUrlGenerator.getDefaultPortalChannelUrl(channel, syndicatedCommunication)).andReturn(
        "default channel url");

    mockEmailServiceDao.sendCommunicationRescheduleConfirmation(communications);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockSyndicationDbDao, mockCommunicationConfigurationDbDao,
        mockChannelFinder, mockEmailServiceDao, mockCalendarUrlGenerator, mockCommunicationPortalUrlGenerator);

    // exercise
    uut.emailRescheduleCallback(COMMUNICATION_ID);

    EasyMock.verify(mockCommunicationFinder, mockSyndicationDbDao, mockCommunicationConfigurationDbDao,
        mockChannelFinder, mockEmailServiceDao, mockCalendarUrlGenerator, mockCommunicationPortalUrlGenerator);
  }

  @Test
  public void updateMasterCommunicationSuccess() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.UPCOMING;
    String maxCommunicationLength = "300";
    Integer duration = 120;
    Boolean isOverlapping = false;
    String dialInPhoneNumber = "+44 424 878 1234";

    Date existingScheduledDateTime = getDate(10L);
    Date newScheduledDateTime = getDate(60L);

    User user = new User(USER_ID);

    Feature maxCommunicationLengthFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxCommunicationLengthFeature.setIsEnabled(true);
    maxCommunicationLengthFeature.setValue(maxCommunicationLength);

    List<Feature> features = new ArrayList<Feature>();
    features.add(maxCommunicationLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).withFeatures(
        features).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).withScheduledDateTime(
        newScheduledDateTime).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).withScheduledDateTime(existingScheduledDateTime).build();

    HashMap<Long, CommunicationCategory> categories = new HashMap<Long, CommunicationCategory>();
    List<CommunicationCategory> categoriesToUpdate = new ArrayList<CommunicationCategory>();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);
    EasyMock.expect(mockCommunicationDao.update(loadedCommunication)).andReturn(loadedCommunication);
    mockCommunicationConfigurationDbDao.update(loadedCommunication.getConfiguration());
    EasyMock.expectLastCall();
    mockContentPlan.assertForUpdate(user, channel, loadedCommunication);
    EasyMock.expectLastCall();
    EasyMock.expect(mockCommunicationDao.isOverlapping(loadedCommunication)).andReturn(isOverlapping);

    EasyMock.expect(mockCommunicationCategoryDbDao.find(CHANNEL_ID, COMMUNICATION_ID)).andReturn(categories);
    mockCommunicationCategoryDbDao.update(categoriesToUpdate);
    EasyMock.expectLastCall();

    EasyMock.expect(mockHDBookingServiceDao.updateBooking(communication)).andReturn(dialInPhoneNumber);

    mockCommunicationDao.updateLivePhoneNumber(communication);
    EasyMock.expectLastCall();

    mockLesQueuer.reschedule(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockEmailQueuer.reschedule(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockSummitQueuer.reschedule(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockImageConverterQueuer.inform(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao, mockCommunicationConfigurationDbDao,
        mockCommunicationCategoryDbDao, mockLesQueuer, mockEmailQueuer, mockSummitQueuer, mockContentPlan,
        mockImageConverterQueuer, mockHDBookingServiceDao);

    // EXERCISE
    uut.update(user, channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao, mockCommunicationConfigurationDbDao,
        mockCommunicationCategoryDbDao, mockLesQueuer, mockEmailQueuer, mockSummitQueuer, mockContentPlan,
        mockImageConverterQueuer, mockHDBookingServiceDao);
  }

  @Test
  public void updateMasterCommunicationSuccessDoesNotCallBookingService() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.UPCOMING;
    String maxCommunicationLength = "300";
    Date startDate = new Date();
    Integer duration = 120;
    String timeZone = "Europe/London";

    User user = new User(USER_ID);

    Feature maxCommunicationLengthFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxCommunicationLengthFeature.setIsEnabled(true);
    maxCommunicationLengthFeature.setValue(maxCommunicationLength);

    List<Feature> features = new ArrayList<Feature>();
    features.add(maxCommunicationLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).withFeatures(
        features).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withScheduledDateTime(startDate).withDuration(duration).withTimeZone(timeZone).withProvider(
        provider).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withScheduledDateTime(startDate).withDuration(duration).withTimeZone(timeZone).withProvider(
        provider).build();

    HashMap<Long, CommunicationCategory> categories = new HashMap<Long, CommunicationCategory>();
    List<CommunicationCategory> categoriesToUpdate = new ArrayList<CommunicationCategory>();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);
    EasyMock.expect(mockCommunicationDao.update(loadedCommunication)).andReturn(loadedCommunication);
    mockCommunicationConfigurationDbDao.update(loadedCommunication.getConfiguration());
    EasyMock.expectLastCall();
    mockContentPlan.assertForUpdate(user, channel, loadedCommunication);
    EasyMock.expectLastCall();
    // EasyMock.expect(mockCommunicationDao.isOverlapping(loadedCommunication)).andReturn(isOverlapping);

    EasyMock.expect(mockCommunicationCategoryDbDao.find(CHANNEL_ID, COMMUNICATION_ID)).andReturn(categories);
    mockCommunicationCategoryDbDao.update(categoriesToUpdate);
    EasyMock.expectLastCall();

    mockImageConverterQueuer.inform(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao, mockCommunicationConfigurationDbDao,
        mockCommunicationCategoryDbDao, mockContentPlan, mockImageConverterQueuer);

    // EXERCISE
    uut.update(user, channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao, mockCommunicationConfigurationDbDao,
        mockCommunicationCategoryDbDao, mockContentPlan, mockImageConverterQueuer);
  }

  @Test(expected = ProviderNotSupportedException.class)
  public void updateMasterCommunicationFailsProviderIsNotHD() {
    // SET UP
    Boolean isActiveAsset = false;
    Provider provider = new Provider(Provider.BRIGHTTALK);
    Status status = Status.UPCOMING;
    Integer duration = 120;

    User user = new User(USER_ID);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao);

    // EXERCISE
    uut.update(user, channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao);
  }

  @Test(expected = InvalidCommunicationException.class)
  public void updateMasterCommunicationFailsCommunicationIsNotActiveAsset() {
    // SET UP
    Boolean isActiveAsset = false;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.UPCOMING;
    Integer duration = 120;

    User user = new User(USER_ID);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao);

    // EXERCISE
    uut.update(user, channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao);
  }

  @Test(expected = InvalidCommunicationException.class)
  public void updateMasterCommunicationFailsCommunicationStatusIsCancelled() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.CANCELLED;
    Integer duration = 120;

    User user = new User(USER_ID);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao);

    // EXERCISE
    uut.update(user, channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao);
  }

  @Test(expected = ProviderNotSupportedException.class)
  public void updateMasterCommunicationFailsCommunicationProviderHasChanged() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.UPCOMING;
    Integer duration = 120;

    User user = new User(USER_ID);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(new Provider(Provider.BRIGHTTALK)).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao);

    // EXERCISE
    uut.update(user, channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao);
  }

  @Test
  public void updateStatusAndInformNotificationService() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.PROCESSING;
    Integer duration = 120;
    boolean notificationServiceEnabled = true;

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);
    EasyMock.expect(mockCommunicationDao.update(loadedCommunication)).andReturn(communication);
    mockNotificationQueuer.inform(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao, mockNotificationQueuer);
    uut.setNotificationServiceEnabled(notificationServiceEnabled);

    // EXERCISE
    uut.updateStatus(channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao, mockNotificationQueuer);
  }

  @Test
  public void updateStatusAndDontInformNotificationService() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.PROCESSING;
    Integer duration = 120;
    boolean notificationServiceEnabled = false;

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);
    EasyMock.expect(mockCommunicationDao.update(loadedCommunication)).andReturn(communication);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao);
    uut.setNotificationServiceEnabled(notificationServiceEnabled);

    // EXERCISE
    uut.updateStatus(channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao);
  }

  @Test(expected = ProviderNotSupportedException.class)
  public void updateStatusFailsWhenProviderIsNotBrightTALKHD() {
    // SET UP
    Provider provider = new Provider(Provider.BRIGHTTALK);
    Status status = Status.PROCESSING;
    Integer duration = 120;

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).build();

    // EXPECTATIONS

    // EXERCISE
    uut.updateStatus(channel, communication);
  }

  @Test(expected = InvalidCommunicationException.class)
  public void updateStatusFailsWhenCommunicationIsNotActiveAsset() {
    // SET UP
    Boolean isActiveAsset = false;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.PROCESSING;
    Integer duration = 120;

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao);

    // EXERCISE
    uut.updateStatus(channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao);
  }

  @Test(expected = InvalidCommunicationException.class)
  public void updateStatusFailsWhenCommunicationCannotBeUpdated() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.CANCELLED;
    Integer duration = 120;

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao);

    // EXERCISE
    uut.updateStatus(channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao);
  }

  @Test(expected = ProviderNotSupportedException.class)
  public void updateStatusFailsWhenProviderHasChanged() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.CANCELLED;
    Integer duration = 120;

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(new Provider(Provider.BRIGHTTALK)).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao);

    // EXERCISE
    uut.updateStatus(channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao);
  }

  @Test
  public void updateFeatureImage() {
    // SET UP
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).build();

    Communication updatedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationDao.update(updatedCommunication)).andReturn(updatedCommunication);

    EasyMock.replay(mockCommunicationDao);

    // EXERCISE
    uut.updateFeatureImage(communication);

    EasyMock.verify(mockCommunicationDao);
  }

  @Test
  public void updateInternalSuccess() {
    // SET UP
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();
    Status status = Status.UPCOMING;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).withStatus(status).build();

    Communication updatedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationDao.isActiveAsset(communication)).andReturn(true);
    EasyMock.expect(mockCommunicationDao.update(communication)).andReturn(updatedCommunication);

    EasyMock.replay(mockCommunicationDao);

    // EXERCISE
    uut.update(channel, communication);

    EasyMock.verify(mockCommunicationDao);
  }

  @Test
  public void updateInternalSuccessLiveToRecorded() {
    // SET UP
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();
    Status status = Status.RECORDED;
    Long renditionId = 99L;

    Rendition rendition = new Rendition();
    rendition.setBitrate(10L);
    rendition.setCodecs("codecs");
    rendition.setContainer("container");
    rendition.setType("type");
    rendition.setUrl("url");
    rendition.setWidth(10L);
    rendition.setId(renditionId);

    List<Rendition> renditions = new ArrayList<>();
    renditions.add(rendition);

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).withStatus(status).withRenditions(renditions).build();

    Communication updatedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).withStatus(status).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationDao.isActiveAsset(communication)).andReturn(true);
    EasyMock.expect(mockCommunicationDao.update(communication)).andReturn(updatedCommunication);

    mockCommunicationDao.updateEncodingCount(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockRenditionDbDao.removeRenditionAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    EasyMock.expect(mockRenditionDbDao.findByCommunicationId(COMMUNICATION_ID)).andReturn(renditions);

    mockRenditionDbDao.delete(renditionId);
    EasyMock.expectLastCall();
    EasyMock.expect(mockRenditionDbDao.create(rendition)).andReturn(rendition);
    mockRenditionDbDao.addRenditionAsset(COMMUNICATION_ID, renditionId);
    EasyMock.expectLastCall();

    mockLesQueuer.videoUploadComplete(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockEmailQueuer.recordingPublished(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockEmailQueuer.missedYou(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationDao, mockRenditionDbDao, mockEmailQueuer, mockLesQueuer);

    // EXERCISE
    uut.update(channel, communication);

    EasyMock.verify(mockCommunicationDao, mockRenditionDbDao, mockEmailQueuer, mockLesQueuer);
  }

  @Test
  public void updateInternalProcessingFailed() {
    // SET UP
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();
    Status status = Status.PROCESSINGFAILED;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).withStatus(status).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationDao.isActiveAsset(communication)).andReturn(true);
    mockLesQueuer.videoUploadError(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationDao, mockLesQueuer);

    // EXERCISE
    uut.update(channel, communication);

    EasyMock.verify(mockCommunicationDao, mockLesQueuer);
  }

  @Test
  public void internalUpdateEmailsAreSentOnlyOnce() {
    // SET UP
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).build();
    Status status = Status.RECORDED;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).withStatus(status).withEncodingCount(2).build();

    Communication updatedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(provider).withStatus(status).build();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationDao.isActiveAsset(communication)).andReturn(true);
    EasyMock.expect(mockCommunicationDao.update(communication)).andReturn(updatedCommunication);

    mockCommunicationDao.updateEncodingCount(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    mockLesQueuer.videoUploadComplete(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationDao, mockRenditionDbDao, mockEmailQueuer, mockLesQueuer);

    // EXERCISE
    uut.update(channel, communication);

    EasyMock.verify(mockCommunicationDao, mockRenditionDbDao, mockEmailQueuer, mockLesQueuer);
  }

  @Test
  public void updateMasterCommunicationRescheduleValidationSuccess() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.UPCOMING;
    String maxCommunicationLength = "300";
    Integer duration = 120;
    Boolean isOverlapping = false;
    String dialInPhoneNumber = "+44 424 878 1234";

    Date existingScheduledDateTime = getDate(10L);
    Date newScheduledDateTime = getDate(45L);

    User user = new User(USER_ID);

    Feature maxCommunicationLengthFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxCommunicationLengthFeature.setIsEnabled(true);
    maxCommunicationLengthFeature.setValue(maxCommunicationLength);

    List<Feature> features = new ArrayList<Feature>();
    features.add(maxCommunicationLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).withFeatures(
        features).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).withScheduledDateTime(
        newScheduledDateTime).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).withScheduledDateTime(existingScheduledDateTime).build();

    HashMap<Long, CommunicationCategory> categories = new HashMap<Long, CommunicationCategory>();
    List<CommunicationCategory> categoriesToUpdate = new ArrayList<CommunicationCategory>();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);
    EasyMock.expect(mockCommunicationDao.update(loadedCommunication)).andReturn(loadedCommunication);
    mockCommunicationConfigurationDbDao.update(loadedCommunication.getConfiguration());
    EasyMock.expectLastCall();
    mockContentPlan.assertForUpdate(user, channel, loadedCommunication);
    EasyMock.expectLastCall();
    EasyMock.expect(mockCommunicationDao.isOverlapping(loadedCommunication)).andReturn(isOverlapping);

    EasyMock.expect(mockCommunicationCategoryDbDao.find(CHANNEL_ID, COMMUNICATION_ID)).andReturn(categories);
    mockCommunicationCategoryDbDao.update(categoriesToUpdate);
    EasyMock.expectLastCall();

    EasyMock.expect(mockHDBookingServiceDao.updateBooking(communication)).andReturn(dialInPhoneNumber);

    mockCommunicationDao.updateLivePhoneNumber(communication);
    EasyMock.expectLastCall();

    mockLesQueuer.reschedule(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockEmailQueuer.reschedule(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockSummitQueuer.reschedule(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockImageConverterQueuer.inform(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao, mockCommunicationConfigurationDbDao,
        mockCommunicationCategoryDbDao, mockLesQueuer, mockEmailQueuer, mockSummitQueuer, mockContentPlan,
        mockImageConverterQueuer, mockHDBookingServiceDao);

    // EXERCISE
    uut.update(user, channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao, mockCommunicationConfigurationDbDao,
        mockCommunicationCategoryDbDao, mockLesQueuer, mockEmailQueuer, mockSummitQueuer, mockContentPlan,
        mockImageConverterQueuer, mockHDBookingServiceDao);
  }

  @Test(expected = InvalidCommunicationException.class)
  public void updateMasterCommunicationRescheduleValidationFail() {
    // SET UP
    Boolean isActiveAsset = true;
    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Status status = Status.UPCOMING;
    String maxCommunicationLength = "300";
    Integer duration = 120;
    Boolean isOverlapping = false;
    String dialInPhoneNumber = "+44 424 878 1234";

    // now + 10 mins
    Date existingScheduledDateTime = getDate(10L);
    Date newScheduledDateTime = getDate(15L);

    User user = new User(USER_ID);

    Feature maxCommunicationLengthFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxCommunicationLengthFeature.setIsEnabled(true);
    maxCommunicationLengthFeature.setValue(maxCommunicationLength);

    List<Feature> features = new ArrayList<Feature>();
    features.add(maxCommunicationLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(USER_ID).withFeatures(
        features).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withDuration(duration).withProvider(provider).withScheduledDateTime(
        newScheduledDateTime).build();

    Communication loadedCommunication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withStatus(status).withProvider(provider).withScheduledDateTime(existingScheduledDateTime).build();

    HashMap<Long, CommunicationCategory> categories = new HashMap<Long, CommunicationCategory>();
    List<CommunicationCategory> categoriesToUpdate = new ArrayList<CommunicationCategory>();

    // EXPECTATIONS
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andReturn(loadedCommunication);
    EasyMock.expect(mockCommunicationDao.isActiveAsset(loadedCommunication)).andReturn(isActiveAsset);
    EasyMock.expect(mockCommunicationDao.update(loadedCommunication)).andReturn(loadedCommunication);
    mockCommunicationConfigurationDbDao.update(loadedCommunication.getConfiguration());
    EasyMock.expectLastCall();
    mockContentPlan.assertForUpdate(user, channel, loadedCommunication);
    EasyMock.expectLastCall();
    EasyMock.expect(mockCommunicationDao.isOverlapping(loadedCommunication)).andReturn(isOverlapping);

    EasyMock.expect(mockCommunicationCategoryDbDao.find(CHANNEL_ID, COMMUNICATION_ID)).andReturn(categories);
    mockCommunicationCategoryDbDao.update(categoriesToUpdate);
    EasyMock.expectLastCall();

    EasyMock.expect(mockHDBookingServiceDao.updateBooking(communication)).andReturn(dialInPhoneNumber);

    mockCommunicationDao.updateLivePhoneNumber(communication);
    EasyMock.expectLastCall();

    mockLesQueuer.reschedule(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockEmailQueuer.reschedule(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockSummitQueuer.reschedule(COMMUNICATION_ID);
    EasyMock.expectLastCall();
    mockImageConverterQueuer.inform(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationFinder, mockCommunicationDao, mockCommunicationConfigurationDbDao,
        mockCommunicationCategoryDbDao, mockLesQueuer, mockEmailQueuer, mockSummitQueuer, mockContentPlan,
        mockImageConverterQueuer, mockHDBookingServiceDao);

    // EXERCISE
    uut.update(user, channel, communication);

    EasyMock.verify(mockCommunicationFinder, mockCommunicationDao, mockCommunicationConfigurationDbDao,
        mockCommunicationCategoryDbDao, mockLesQueuer, mockEmailQueuer, mockSummitQueuer, mockContentPlan,
        mockImageConverterQueuer, mockHDBookingServiceDao);
  }

  private Date getDate(final Long timeInMinutes) {
    // Get time now in seconds
    Long nowInSecs = new DateTime().getMillis() / 1000;

    Long dateInSeconds = nowInSecs + (timeInMinutes * 60);

    return new DateTime(dateInSeconds * 1000).toDate();
  }

}