/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationBuilder.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Format;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationScheduledPublication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;

public class CommunicationBuilder {

  public static final String DEFAULT_TITLE = "DefaultCommunicationBuilderTitle";
  public static final String DEFAULT_PRESENTER = "DefaultCommunicationBuilderPresenter";
  public static final String DEFAULT_DESCRIPTION = "DefaultCommunicationBuilderDescription";
  public static final String DEFAULT_KEYWORDS = "DefaultCommunicationBuilderKeywords,Two";
  public static final Integer DEFAULT_DURATION = 300;
  public static final String DEFAULT_TIMEZONE = "Europe/London";
  public static final Status DEFAULT_STATUS = Status.UPCOMING;
  public static final PublishStatus DEFAULT_PUBLISH_STATUS = PublishStatus.PUBLISHED;
  public static final Format DEFAULT_FORMAT = Format.AUDIO;
  public static final String DEFAULT_PHONE_NUMBER = "defaultPhoneNumber";
  public static final String DEFAULT_LIVE_URL = "defaultLiveUrl";
  public static final String DEFAULT_PIN_NUMBER = "01234567";
  public static final String DEFAULT_THUMBNAIL_URL = "defaultThumbnailUrl";
  public static final String DEFAULT_PREVIEW_URL = "defaultPreviewUrl";
  public static final Long DEFAULT_MASTER_CHANNEL_ID = 789l;
  public static final Integer DEFAULT_ENCODING_COUNT = 0;

  private Long masterChannelId;

  private Long id;

  private Long channelId;

  private String title;

  private String presenter;

  private String description;

  private String keywords;

  private Integer duration;

  private String timeZone;

  private Status status;

  private PublishStatus publishStatus;

  private Date scheduledDateTime;

  private Date created;

  private Date lastUpdated;

  private Provider provider;

  private Format format;

  private CommunicationConfiguration configuration;

  private CommunicationStatistics viewingStatistics;

  private String liveUrl;

  private String pinNumber;

  private String livePhoneNumber;

  private String thumbnailUrl;

  private String previewUrl;

  private List<CommunicationCategory> categories;

  private List<Rendition> renditions;

  private Integer encodingCount;

  private CommunicationScheduledPublication scheduledPublication;

  // this may still miss few properties. Current is a minimum needed to insert communication into db
  public CommunicationBuilder withDefaultValues() {

    title = DEFAULT_TITLE;
    presenter = DEFAULT_PRESENTER;
    description = DEFAULT_DESCRIPTION;
    keywords = DEFAULT_KEYWORDS;
    duration = DEFAULT_DURATION;
    timeZone = DEFAULT_TIMEZONE;
    status = DEFAULT_STATUS;
    publishStatus = DEFAULT_PUBLISH_STATUS;
    // scheduled date puts communication as upcoming in authoring period.
    scheduledDateTime = DateUtils.addMinutes(new Date(), 16);
    format = DEFAULT_FORMAT;
    provider = new Provider(Provider.BRIGHTTALK);
    configuration = new CommunicationConfigurationBuilder().withDefaultValues().build();
    viewingStatistics = new CommunicationStatisticsBuilder().withDefaultValues().build();
    livePhoneNumber = DEFAULT_PHONE_NUMBER;
    liveUrl = DEFAULT_LIVE_URL;
    pinNumber = DEFAULT_PIN_NUMBER;
    thumbnailUrl = DEFAULT_THUMBNAIL_URL;
    previewUrl = DEFAULT_PREVIEW_URL;
    masterChannelId = DEFAULT_MASTER_CHANNEL_ID;
    // scheduled date puts communication as upcoming in authoring period.
    created = DateUtils.addMinutes(new Date(), -60);
    lastUpdated = DateUtils.addMinutes(new Date(), -30);
    categories = new ArrayList<CommunicationCategory>();
    renditions = new ArrayList<Rendition>();
    encodingCount = DEFAULT_ENCODING_COUNT;
    scheduledPublication = new CommunicationScheduledPublication();
    return this;
  }

  public CommunicationBuilder withId(final Long value) {

    id = value;
    return this;
  }

  public CommunicationBuilder withChannelId(final Long value) {

    channelId = value;
    return this;
  }

  public CommunicationBuilder withMasterChannelId(final Long value) {

    masterChannelId = value;
    return this;
  }

  public CommunicationBuilder withTitle(final String value) {

    title = value;
    return this;
  }

  public CommunicationBuilder withDescription(final String value) {

    description = value;
    return this;
  }

  public CommunicationBuilder withKeywords(final String value) {

    keywords = value;
    return this;
  }

  public CommunicationBuilder withPresenter(final String value) {

    presenter = value;
    return this;
  }

  public CommunicationBuilder withStatus(final Status value) {

    status = value;
    return this;
  }

  public CommunicationBuilder withScheduledDateTime(final Date value) {

    scheduledDateTime = value;
    return this;
  }

  public CommunicationBuilder withDuration(final Integer value) {

    duration = value;
    return this;
  }

  public CommunicationBuilder withTimeZone(final String value) {

    timeZone = value;
    return this;
  }

  public CommunicationBuilder withPublishStatus(final PublishStatus value) {

    publishStatus = value;
    return this;
  }

  public CommunicationBuilder withProvider(final Provider value) {

    provider = value;
    return this;
  }

  public CommunicationBuilder withFormat(final Format value) {

    format = value;
    return this;
  }

  public CommunicationBuilder withConfiguration(final CommunicationConfiguration value) {

    configuration = value;
    return this;
  }

  public CommunicationBuilder withViewingStatistics(final CommunicationStatistics value) {

    viewingStatistics = value;
    return this;
  }

  public CommunicationBuilder withCategories(final List<CommunicationCategory> value) {

    categories = value;
    return this;
  }

  public CommunicationBuilder withPinNumber(final String value) {

    pinNumber = value;
    return this;
  }

  public CommunicationBuilder withRenditions(final List<Rendition> value) {

    renditions = value;
    return this;
  }

  public CommunicationBuilder withEncodingCount(final Integer value) {

    encodingCount = value;
    return this;
  }

  public CommunicationBuilder withThumbnailUrl(final String value) {
    thumbnailUrl = value;
    return this;
  }

  public CommunicationBuilder withPreviewUrl(final String value) {
    previewUrl = value;
    return this;
  }

  public CommunicationBuilder withScheduledPublication(final CommunicationScheduledPublication value) {
    scheduledPublication = value;
    return this;
  }

  public Communication build() {

    Communication communication = new Communication();
    communication.setId(id);
    communication.setChannelId(channelId);
    communication.setMasterChannelId(masterChannelId);
    communication.setTitle(title);
    communication.setDescription(description);
    communication.setKeywords(keywords);
    communication.setAuthors(presenter);
    communication.setTimeZone(timeZone);
    communication.setStatus(status);
    communication.setPublishStatus(publishStatus);
    communication.setFormat(format);
    communication.setProvider(provider);
    communication.setScheduledDateTime(scheduledDateTime);
    communication.setDuration(duration);
    communication.setConfiguration(configuration);
    communication.setCommunicationStatistics(viewingStatistics);
    communication.setLiveUrl(liveUrl);
    communication.setPinNumber(pinNumber);
    communication.setLivePhoneNumber(livePhoneNumber);
    communication.setThumbnailUrl(thumbnailUrl);
    communication.setPreviewUrl(previewUrl);
    communication.setCreated(created);
    communication.setLastUpdated(lastUpdated);
    communication.setCategories(categories);
    communication.setRenditions(renditions);
    communication.setEncodingCount(encodingCount);
    communication.setScheduledPublication(scheduledPublication);

    return communication;
  }
}