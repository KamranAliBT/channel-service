/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationServiceTest.java 72595 2014-01-15 13:37:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.easymock.EasyMock.createMock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.ContentPlan;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.Keywords;
import com.brighttalk.service.channel.business.domain.PinGenerator;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.error.BookingCapacityExceededException;
import com.brighttalk.service.channel.business.error.ContentPlanException;
import com.brighttalk.service.channel.business.error.ProviderNotSupportedException;
import com.brighttalk.service.channel.business.queue.EmailQueuer;
import com.brighttalk.service.channel.business.queue.ImageConverterQueuer;
import com.brighttalk.service.channel.integration.booking.HDBookingServiceDao;
import com.brighttalk.service.channel.integration.database.CommunicationCategoryDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationConfigurationDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.email.EmailServiceDao;

/**
 * Unit tests for {@link CommunicationCreateService}.
 */
public class HDCommunicationCreateServiceTest extends AbstractCommunicationServiceTest {

  private HDCommunicationCreateService uut;

  @Before
  public void setUp() {

    uut = new HDCommunicationCreateService();

    mockHDBookingServiceDao = EasyMock.createMock(HDBookingServiceDao.class);
    ReflectionTestUtils.setField(uut, "hDBookingServiceDao", mockHDBookingServiceDao);

    mockContentPlan = EasyMock.createMock(ContentPlan.class);
    ReflectionTestUtils.setField(uut, "contentPlan", mockContentPlan);

    mockCommunicationDao = createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDao);

    mockCommunicationConfigurationDbDao = createMock(CommunicationConfigurationDbDao.class);
    ReflectionTestUtils.setField(uut, "configurationDbDao", mockCommunicationConfigurationDbDao);

    mockCommunicationCategoryDbDao = createMock(CommunicationCategoryDbDao.class);
    ReflectionTestUtils.setField(uut, "categoryDbDao", mockCommunicationCategoryDbDao);

    mockEmailServiceDao = EasyMock.createMock(EmailServiceDao.class);
    ReflectionTestUtils.setField(uut, "emailServiceDao", mockEmailServiceDao);

    mockEmailQueuer = EasyMock.createMock(EmailQueuer.class);
    ReflectionTestUtils.setField(uut, "emailQueuer", mockEmailQueuer);

    mockImageConverterQueuer = EasyMock.createMock(ImageConverterQueuer.class);
    ReflectionTestUtils.setField(uut, "imageConverterQueuer", mockImageConverterQueuer);

    mockPinGenerator = EasyMock.createMock(PinGenerator.class);
    ReflectionTestUtils.setField(uut, "pinGenerator", mockPinGenerator);

    mockKeywords = EasyMock.createMock(Keywords.class);
    ReflectionTestUtils.setField(uut, "keywords", mockKeywords);
  }

  @Test(expected = ProviderNotSupportedException.class)
  public void createCommunicationThrowsInvalidProviderException() {
    // Set up
    User user = createUser();
    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(user.getId()).build();
    Provider provider = new Provider(Provider.BRIGHTTALK);
    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).withProvider(provider).build();

    // expectations

    // exercise
    uut.create(user, channel, communication);

    // assertions
  }

  @Test(expected = UserAuthorisationException.class)
  public void createThrowsUserNotAuthorisedExceptionWhenUserNotChannelOwnerNorBrightTALKManager() {
    // Set up
    User user = createUser();
    Long channelOwnerUserId = 123456L;

    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);
    List<Feature> features = Collections.singletonList(feature);

    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(channelOwnerUserId).withFeatures(
        features).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).withProvider(provider).build();

    // expectations

    // exercise
    uut.create(user, channel, communication);

    // assertions
  }

  @Test(expected = ContentPlanException.class)
  public void createThrowsContentPlanException() {
    // Set up
    User user = createUser();

    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(user.getId()).build();

    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().build();

    CommunicationCategory category = new CommunicationCategory(123L, CHANNEL_ID, "description", true);
    List<CommunicationCategory> categories = new ArrayList<CommunicationCategory>();
    categories.add(category);

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).withProvider(provider).withConfiguration(communicationConfiguration).withCategories(
        categories).build();

    // expectations
    mockContentPlan.assertForCreate(user, channel, communication);
    EasyMock.expectLastCall().andThrow(new ContentPlanException("Content Plan Reached"));

    EasyMock.replay(mockContentPlan);

    // exercise
    uut.create(user, channel, communication);

    // assertions
    EasyMock.verify(mockContentPlan);
  }

  @Test(expected = BookingCapacityExceededException.class)
  public void createThrowsBookingCapacityExceededException() {
    // Set up
    User user = createUser();

    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Feature maxCommunicationDurationFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxCommunicationDurationFeature.isEnabled();
    maxCommunicationDurationFeature.setValue("0");

    List<Feature> features = new ArrayList<Feature>();
    features.add(maxCommunicationDurationFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(user.getId()).withFeatures(
        features).build();

    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().build();

    CommunicationCategory category = new CommunicationCategory(123L, CHANNEL_ID, "description", true);
    List<CommunicationCategory> categories = new ArrayList<CommunicationCategory>();
    categories.add(category);

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).withProvider(provider).withConfiguration(communicationConfiguration).withCategories(
        categories).build();

    Communication createdCommunication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).withProvider(provider).withConfiguration(communicationConfiguration).withCategories(
        categories).build();

    String pinNumber = "12345678";
    boolean isOverlapping = false;
    String keywords = "tag 1, tag 2";
    communication.setKeywords(keywords);

    // expectations
    mockContentPlan.assertForCreate(user, channel, communication);
    EasyMock.expectLastCall();

    EasyMock.expect(mockCommunicationDao.isOverlapping(communication)).andReturn(isOverlapping);

    EasyMock.expect(mockPinGenerator.generatePinNumber()).andReturn(pinNumber);

    EasyMock.expect(mockKeywords.addQuotes(communication.getKeywords())).andReturn(keywords);

    EasyMock.expect(mockCommunicationDao.create(communication)).andReturn(createdCommunication);

    EasyMock.expect(mockHDBookingServiceDao.createBooking(createdCommunication)).andThrow(
        new BookingCapacityExceededException("CapacityExceeded"));

    EasyMock.replay(mockContentPlan, mockCommunicationDao, mockPinGenerator, mockKeywords, mockHDBookingServiceDao);

    // exercise
    uut.create(user, channel, communication);

    // assertions
    EasyMock.verify(mockContentPlan, mockCommunicationDao, mockPinGenerator, mockKeywords, mockHDBookingServiceDao);
  }

  @Test
  public void createCommunicationAndReturnCommunicationId() {
    // Set up
    User user = createUser();

    Provider provider = new Provider(Provider.BRIGHTTALK_HD);
    Feature maxCommunicationDurationFeature = new Feature(Type.MAX_COMM_LENGTH);
    maxCommunicationDurationFeature.isEnabled();
    maxCommunicationDurationFeature.setValue("0");

    List<Feature> features = new ArrayList<Feature>();
    features.add(maxCommunicationDurationFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withOwnerUserId(user.getId()).withFeatures(
        features).build();

    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().build();

    CommunicationCategory category = new CommunicationCategory(123L, CHANNEL_ID, "description", true);
    List<CommunicationCategory> categories = new ArrayList<CommunicationCategory>();
    categories.add(category);

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withProvider(
        provider).withConfiguration(communicationConfiguration).withCategories(categories).build();

    Communication createdCommunication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).withProvider(provider).withConfiguration(communicationConfiguration).withCategories(
        categories).build();

    String pinNumber = "12345678";
    String dialInPhoneNumber = "+44 424 878 1234";
    boolean isOverlapping = false;
    String keywords = "tag 1, tag 2";
    communication.setKeywords(keywords);

    // expectations
    mockContentPlan.assertForCreate(user, channel, communication);
    EasyMock.expectLastCall();

    EasyMock.expect(mockCommunicationDao.isOverlapping(communication)).andReturn(isOverlapping);

    EasyMock.expect(mockPinGenerator.generatePinNumber()).andReturn(pinNumber);

    EasyMock.expect(mockKeywords.addQuotes(communication.getKeywords())).andReturn(keywords);

    EasyMock.expect(mockCommunicationDao.create(communication)).andReturn(createdCommunication);

    EasyMock.expect(mockHDBookingServiceDao.createBooking(createdCommunication)).andReturn(dialInPhoneNumber);

    mockCommunicationDao.updateLivePhoneNumber(createdCommunication);
    EasyMock.expectLastCall();

    mockCommunicationDao.createChannelAsset(createdCommunication);
    EasyMock.expectLastCall();

    mockCommunicationConfigurationDbDao.create(communicationConfiguration);
    EasyMock.expectLastCall();

    mockCommunicationCategoryDbDao.create(category);
    EasyMock.expectLastCall();

    mockEmailQueuer.create(communication.getId());
    EasyMock.expectLastCall();

    mockImageConverterQueuer.inform(communication.getId());
    EasyMock.expectLastCall();

    EasyMock.replay(mockContentPlan, mockPinGenerator, mockHDBookingServiceDao, mockCommunicationDao,
        mockCommunicationConfigurationDbDao, mockCommunicationCategoryDbDao, mockEmailQueuer, mockKeywords,
        mockImageConverterQueuer);

    // exercise
    Long communicationId = uut.create(user, channel, communication);

    // assertions
    Assert.assertNotNull(communicationId);
    Assert.assertEquals(COMMUNICATION_ID, communicationId);
    EasyMock.verify(mockContentPlan, mockPinGenerator, mockHDBookingServiceDao, mockCommunicationDao,
        mockCommunicationConfigurationDbDao, mockCommunicationCategoryDbDao, mockEmailQueuer, mockKeywords,
        mockImageConverterQueuer);
  }

}