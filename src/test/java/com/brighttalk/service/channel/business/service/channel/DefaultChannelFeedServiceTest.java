/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: DefaultChannelFeedServiceTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.FilterType;
import com.brighttalk.service.channel.business.domain.channel.PaginatedChannelFeed;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationUrlGenerator;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.ChannelFeedDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationOverrideDbDao;
import com.brighttalk.service.channel.integration.database.statistics.CommunicationStatisticsDbDao;

/**
 * Unit tests for {@link com.brighttalk.service.channel.business.service.channel.DefaultChannelFeedService
 * DefaultChannelFeedService}.
 */
public class DefaultChannelFeedServiceTest {

  private static final Long COMMUNICATION_ID = 1L;
  private static final Long CHANNEL_ID = 2L;
  private static final int DEFAULT_MAX_PAGE_SIZE = 50;

  private DefaultChannelFeedService uut;

  private ChannelFeedDbDao mockChannelFeedDao;
  private CommunicationOverrideDbDao mockCommunicationOverrideDao;
  private CategoryDbDao mockCategoryDao;
  private CommunicationStatisticsDbDao mockCommunicationStatisticsDao;
  private CommunicationUrlGenerator mockCalendarUrlGenerator;
  private CommunicationUrlGenerator mockCommunicationPortalUrlGenerator;

  @Before
  public void setup() {

    uut = new DefaultChannelFeedService();

    mockChannelFeedDao = EasyMock.createMock(ChannelFeedDbDao.class);
    ReflectionTestUtils.setField(uut, "channelFeedDao", mockChannelFeedDao);

    mockCommunicationOverrideDao = EasyMock.createMock(CommunicationOverrideDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationOverrideDao", mockCommunicationOverrideDao);

    mockCategoryDao = EasyMock.createMock(CategoryDbDao.class);
    ReflectionTestUtils.setField(uut, "categoryDao", mockCategoryDao);

    mockCommunicationStatisticsDao = EasyMock.createMock(CommunicationStatisticsDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationStatisticsDao", mockCommunicationStatisticsDao);

    mockCalendarUrlGenerator = EasyMock.createMock(CommunicationUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "calendarUrlGenerator", mockCalendarUrlGenerator);

    mockCommunicationPortalUrlGenerator = EasyMock.createMock(CommunicationUrlGenerator.class);
    ReflectionTestUtils.setField(uut, "communicationPortalUrlGenerator", mockCommunicationPortalUrlGenerator);
  }

  /**
   * Tests the {@link DefaultChannelFeedService#getFeed(Channel, ChannelFeedSearchCriteria) where there is one
   * communications in the channel with no status filter for webcasts.
   */
  @Test
  public void getFeedWithOneCommunicationInChannelWithNoStatusFilter() {

    // Set up
    Long channelId = CHANNEL_ID;
    Channel channel = new Channel(channelId);

    Long communicationId = COMMUNICATION_ID;
    Integer totalNumberOfCommunicationsInFeed = 1;
    Integer numberOfCommunicationsAfterScheduledDate = 1;

    Integer pageNumber = 1;
    Integer pageSize = DEFAULT_MAX_PAGE_SIZE;
    FilterType filterType = FilterType.CLOSESTTONOW;
    List<CommunicationStatusFilter> communicationStatusFilter = null;

    ChannelFeedSearchCriteria searchCriteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, filterType,
        communicationStatusFilter);

    Date currentDate = new Date();

    Date scheduledDateForCommunication = DateUtils.addDays(currentDate, 1);
    Communication communication = buildCommunication(communicationId, scheduledDateForCommunication);
    List<Communication> communications = Arrays.asList(communication);

    // Expectations
    mockFindTotalCommunicationsInFeed(channelId, searchCriteria, totalNumberOfCommunicationsInFeed);

    mockFindFeaturedDateTime(channelId, searchCriteria, currentDate);

    mockFindTotalCommunicationsScheduledAfterDate(channelId, currentDate, searchCriteria,
        numberOfCommunicationsAfterScheduledDate);

    mockFindFeed(channelId, searchCriteria, communications);

    EasyMock.replay(mockChannelFeedDao);

    // Do test
    uut.getFeed(channel, searchCriteria);

    // Assertions
    EasyMock.verify(mockChannelFeedDao);
  }

  /**
   * Tests the {@link DefaultChannelFeedService#getFeed(Channel, ChannelFeedSearchCriteria) where there is one
   * communications in the channel with a filter for live and upcoming webcasts.
   */
  @Test
  public void getFeedWithOneCommunicationInChannelUsingLiveAndUpcomingStatusFilter() {

    // Set up
    Long channelId = CHANNEL_ID;
    Channel channel = new Channel(channelId);

    Long communicationId = COMMUNICATION_ID;
    Integer totalNumberOfCommunicationsInFeed = 1;
    Integer numberOfCommunicationsAfterScheduledDate = 1;

    Integer pageNumber = 1;
    Integer pageSize = DEFAULT_MAX_PAGE_SIZE;
    FilterType filterType = FilterType.CLOSESTTONOW;
    List<CommunicationStatusFilter> communicationStatusFilter = Arrays.asList(CommunicationStatusFilter.LIVE,
        CommunicationStatusFilter.UPCOMING);

    ChannelFeedSearchCriteria searchCriteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, filterType,
        communicationStatusFilter);

    Date currentDate = new Date();

    Date scheduledDateForCommunication = DateUtils.addDays(currentDate, 1);
    Communication communication = buildCommunication(communicationId, scheduledDateForCommunication);
    List<Communication> communications = Arrays.asList(communication);

    // Expectations
    mockFindTotalCommunicationsInFeed(channelId, searchCriteria, totalNumberOfCommunicationsInFeed);

    mockFindFeaturedDateTime(channelId, searchCriteria, currentDate);

    mockFindTotalCommunicationsScheduledAfterDate(channelId, currentDate, searchCriteria,
        numberOfCommunicationsAfterScheduledDate);

    mockFindFeed(channelId, searchCriteria, communications);

    EasyMock.replay(mockChannelFeedDao);

    // Do test
    uut.getFeed(channel, searchCriteria);

    // Assertions
    EasyMock.verify(mockChannelFeedDao);
  }

  /**
   * Tests the {@link DefaultChannelFeedService#getFeed(Channel, ChannelFeedSearchCriteria) where there are no
   * communications in the channel with a filter for live and upcoming webcasts.
   */
  @Test
  public void getFeedFromChannelWithNoCommunicationsUsingLiveAndUpcomingStatusFilter() {

    // Set up
    Long channelId = CHANNEL_ID;
    Channel channel = new Channel(channelId);

    Integer totalNumberOfCommunicationsInFeed = 0;

    Integer pageNumber = 1;
    Integer pageSize = DEFAULT_MAX_PAGE_SIZE;
    FilterType filterType = FilterType.CLOSESTTONOW;
    List<CommunicationStatusFilter> communicationStatusFilter = Arrays.asList(CommunicationStatusFilter.LIVE,
        CommunicationStatusFilter.UPCOMING);

    ChannelFeedSearchCriteria searchCriteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, filterType,
        communicationStatusFilter);

    // Expectations
    mockFindTotalCommunicationsInFeed(channelId, searchCriteria, totalNumberOfCommunicationsInFeed);

    EasyMock.replay(mockChannelFeedDao);

    // Do test
    PaginatedChannelFeed result = uut.getFeed(channel, searchCriteria);

    // Assertions
    EasyMock.verify(mockChannelFeedDao);

    assertTrue(result.getTotalNumberOfCommunications() == 0);
    assertTrue(result.getAllCommunications().isEmpty());
    assertEquals(communicationStatusFilter, result.getCommunicationStatusFilter());
  }

  /**
   * Builds an instance of the {@link ChannelFeedSearchCriteria} with the specified arguments.
   * 
   * @param pageNumber Page number for the request.
   * @param pageSize Size of the page to be returned.
   * @param filterType Type of filter.
   * @param communicationStatusFilter Communication status filter.
   * 
   * @return Instance of {@link ChannelFeedSearchCriteria}.
   */
  private ChannelFeedSearchCriteria buildChannelFeedSearchCriteria(Integer pageNumber, Integer pageSize,
      FilterType filterType, List<CommunicationStatusFilter> communicationStatusFilter) {

    ChannelFeedSearchCriteria searchCriteria = new ComparableChannelFeedSearchCriteria();
    searchCriteria.setPageNumber(pageNumber);
    searchCriteria.setPageSize(pageSize);
    searchCriteria.setFilterType(filterType);
    searchCriteria.setCommunicationStatusFilter(communicationStatusFilter);
    return searchCriteria;

  }

  private Communication buildCommunication(Long communicationId, Date scheduledDate) {

    Communication communication = new Communication(communicationId);
    communication.setScheduledDateTime(scheduledDate);

    CommunicationConfiguration configuration = new CommunicationConfiguration();
    communication.setConfiguration(configuration);

    return communication;
  }

  /**
   * Set up the mock object {@link #mockChannelFeedDao}, to return the communication count in the feed when
   * {@link ChannelFeedDbDao#findTotalCommunicationsInFeed(Long, ChannelFeedSearchCriteria)} is called.
   * 
   * @param expectedChannelId Expected channel ID that the mocked method will be called with.
   * @param expectedChannelFeedSearchCriteria Expected search criteria that the mocked method will be called with.
   * @param totalNumberOfCommunicationsInFeed Total number of communications in feed to return when method is called.
   */
  private void mockFindTotalCommunicationsInFeed(Long expectedChannelId,
      ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria, Integer totalNumberOfCommunicationsInFeed) {

    EasyMock.expect(
        mockChannelFeedDao.findTotalCommunicationsInFeed(EasyMock.leq(expectedChannelId),
            EasyMock.eq(expectedChannelFeedSearchCriteria))).andReturn(totalNumberOfCommunicationsInFeed);
  }

  /**
   * Set up the mock object {@link #mockChannelFeedDao}, to return the featured date time in the feed when
   * {@link ChannelFeedDbDao#findFeaturedDateTime(Long, ChannelFeedSearchCriteria)} is called.
   * 
   * @param expectedChannelId Expected channel ID that the mocked method will be called with.
   * @param expectedChannelFeedSearchCriteria Expected search criteria that the mocked method will be called with.
   * @param featuredDateTime Featured date time.
   */
  private void mockFindFeaturedDateTime(Long expectedChannelId,
      ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria, Date featuredDateTime) {

    EasyMock.expect(
        mockChannelFeedDao.findFeaturedDateTime(EasyMock.leq(expectedChannelId),
            EasyMock.eq(expectedChannelFeedSearchCriteria))).andReturn(featuredDateTime);
  }

  /**
   * Set up the mock object {@link #mockChannelFeedDao}, to return the number of communications in the feed after the
   * specified date when
   * {@link ChannelFeedDbDao#findTotalCommunicationsScheduledAfterDate(Long, Date, ChannelFeedSearchCriteria)} is
   * called.
   * 
   * @param expectedChannelId Expected channel ID that the mocked method will be called with.
   * @param date Expected date to include communications after.
   * @param expectedChannelFeedSearchCriteria Expected search criteria that the mocked method will be called with.
   */
  private void mockFindTotalCommunicationsScheduledAfterDate(Long expectedChannelId, Date date,
      ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria, Integer numberOfCommunicationsAfterDate) {

    EasyMock.expect(
        mockChannelFeedDao.findTotalCommunicationsScheduledAfterDate(EasyMock.leq(expectedChannelId),
            EasyMock.eq(date), EasyMock.eq(expectedChannelFeedSearchCriteria))).andReturn(
        numberOfCommunicationsAfterDate);
  }

  /**
   * Set up the mock object {@link #mockChannelFeedDao}, to return a collection of communications when
   * {@link ChannelFeedDbDao#findFeed(Long, ChannelFeedSearchCriteria)} is called.
   * 
   * @param expectedChannelId Expected channel ID that the mocked method will be called with.
   * @param expectedChannelFeedSearchCriteria Expected search criteria that the mocked method will be called with.
   * @param communications Communications to return when method is called.
   */
  private void mockFindFeed(Long expectedChannelId, ChannelFeedSearchCriteria expectedChannelFeedSearchCriteria,
      List<Communication> communications) {

    EasyMock.expect(
        mockChannelFeedDao.findFeed(EasyMock.leq(expectedChannelId), EasyMock.eq(expectedChannelFeedSearchCriteria))).andReturn(
        communications);
  }

  /**
   * Not testing the unit under test, but the private inner class {@link ComparableChannelFeedSearchCriteria} to be
   * certain the {@link ComparableChannelFeedSearchCriteria#equals(Object)} functions as expected.
   */
  @Test
  public void compareComparableChannelFeedSearchCriteria() {

    ChannelFeedSearchCriteria crit1 = buildChannelFeedSearchCriteria(1, 2, FilterType.CLOSESTTONOW,
        Arrays.asList(CommunicationStatusFilter.UPCOMING));
    ChannelFeedSearchCriteria crit2 = buildChannelFeedSearchCriteria(1, 2, FilterType.CLOSESTTONOW,
        Arrays.asList(CommunicationStatusFilter.UPCOMING));

    assertEquals(crit1, crit2);
  }

  /**
   * Extension of {@link ChannelFeedSearchCriteria} that enables them to be compared when the mocking framework expects
   * a call.
   */
  private class ComparableChannelFeedSearchCriteria extends ChannelFeedSearchCriteria {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {

      if (!(obj instanceof ChannelFeedSearchCriteria))
        return false;
      if (obj == this)
        return true;

      ChannelFeedSearchCriteria rhs = (ChannelFeedSearchCriteria) obj;

      EqualsBuilder builder = new EqualsBuilder();
      builder.append(getPageNumber(), rhs.getPageNumber());
      builder.append(getPageSize(), rhs.getPageSize());
      builder.append(getFilterType(), rhs.getFilterType());
      builder.append(getCommunicationStatusFilter(), rhs.getCommunicationStatusFilter());

      return builder.isEquals();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

      HashCodeBuilder builder = new HashCodeBuilder();
      builder.append(getPageNumber());
      builder.append(getPageSize());
      builder.append(getFilterType());
      builder.append(getCommunicationStatusFilter());

      return super.hashCode();
    }
  }
}
