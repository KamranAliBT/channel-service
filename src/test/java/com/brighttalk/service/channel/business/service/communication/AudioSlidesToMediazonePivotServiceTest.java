/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationProviderServiceTest.java 85193 2014-10-24 15:54:02Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.configuration.Messages;
import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.DataCleanUpFailedException;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.audience.AudienceServiceDao;
import com.brighttalk.service.channel.integration.booking.BookingServiceDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.RenditionDbDao;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;
import com.brighttalk.service.channel.integration.livestate.LiveStateServiceDao;
import com.brighttalk.service.channel.integration.slide.SlideServiceDao;
import com.brighttalk.service.utils.UserErrorMessageMatcher;

public class AudioSlidesToMediazonePivotServiceTest {

  private static final Long COMMUNICATION_ID = 666L;

  private AudioSlidesToMediazonePivotService uut;

  private CommunicationDbDao mockCommunicationDao;

  private RenditionDbDao mockRenditionDbDao;

  private Messages mockMessages;

  private BookingServiceDao mockBookingServiceDao;

  private AudienceServiceDao mockAudienceServiceDao;

  private LiveStateServiceDao mockLiveStateServiceDao;

  private SlideServiceDao mockSlideServiceDao;

  private ResourceDbDao mockResourceDao;

  private ChannelFinder mockChannelFinder;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Before
  public void setUp() {

    uut = new AudioSlidesToMediazonePivotService();

    mockResourceDao = EasyMock.createMock(ResourceDbDao.class);
    ReflectionTestUtils.setField(uut, "resourceDbDao", mockResourceDao);

    mockCommunicationDao = EasyMock.createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDao);

    mockRenditionDbDao = EasyMock.createMock(RenditionDbDao.class);
    ReflectionTestUtils.setField(uut, "renditionDbDao", mockRenditionDbDao);

    mockBookingServiceDao = EasyMock.createMock(BookingServiceDao.class);
    ReflectionTestUtils.setField(uut, "bookingServiceDao", mockBookingServiceDao);

    mockAudienceServiceDao = EasyMock.createMock(AudienceServiceDao.class);
    ReflectionTestUtils.setField(uut, "audienceServiceDao", mockAudienceServiceDao);

    mockLiveStateServiceDao = EasyMock.createMock(LiveStateServiceDao.class);
    ReflectionTestUtils.setField(uut, "liveStateServiceDao", mockLiveStateServiceDao);

    mockSlideServiceDao = EasyMock.createMock(SlideServiceDao.class);
    ReflectionTestUtils.setField(uut, "slideServiceDao", mockSlideServiceDao);

    mockMessages = EasyMock.createMock(Messages.class);
    ReflectionTestUtils.setField(uut, "messages", mockMessages);

    mockChannelFinder = EasyMock.createMock(ChannelFinder.class);
    ReflectionTestUtils.setField(uut, "channelFinder", mockChannelFinder);

    // need to do this, unfortunately, because there is no way to mock the validator
    addLoadFeaturesMocks();
  }

  private void addLoadFeaturesMocks() {
    Channel mockChannel = EasyMock.createMock(Channel.class);
    Feature feature1 = new Feature(Type.MEDIA_ZONES);
    feature1.setIsEnabled(true);
    EasyMock.expect(mockChannel.getFeature(Type.MEDIA_ZONES)).andReturn(feature1);

    Feature feature2 = new Feature(Type.ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS);
    feature2.setIsEnabled(true);
    EasyMock.expect(mockChannel.getFeature(Type.ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS)).andReturn(feature2);

    EasyMock.expect(mockChannelFinder.find(CommunicationBuilder.DEFAULT_MASTER_CHANNEL_ID)).andReturn(mockChannel);

    EasyMock.replay(mockChannel, mockChannelFinder);
  }

  @Test
  public void updateProviderToMediazoneWhenCommunicationIsBrightttalk() {

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);
    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao);

    // do test
    Provider result = uut.pivot(communication, provider);

    assertNotNull(result);
    assertEquals(Provider.MEDIAZONE, result.getName());

    assertEquals(Provider.MEDIAZONE, communication.getProvider().getName());
    assertNull(communication.getPinNumber());
    assertNull(communication.getLiveUrl());
    assertNull(communication.getLivePhoneNumber());
    assertNull(communication.getPreviewUrl());
    assertNull(communication.getThumbnailUrl());
  }

  @Test
  public void deleteBookingOnProviderUpdateWhenCommunicationIsBrightttalk() {

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockBookingServiceDao.deleteBookings(communicationIds);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao, mockBookingServiceDao);

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockBookingServiceDao);
  }

  @Test
  public void deleteBookingOnProviderUpdateWhenCallFailsWhenCommunicationIsBrightttalk() {

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    String expectedErrorMessage = "expectedErrorMessage";

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockBookingServiceDao.deleteBookings(communicationIds);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test exception"));

    EasyMock.expect(mockMessages.getValue(AudioSlidesToMediazonePivotService.DELETE_BOOKING_ERROR_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);

    EasyMock.replay(mockCommunicationDao, mockBookingServiceDao, mockMessages);

    thrown.expect(DataCleanUpFailedException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.pivot(communication, provider);
  }

  @Test
  public void deleteCommunicationFromLiveStateOnProviderUpdateWhenCommunicationIsBrightttalk() {

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockLiveStateServiceDao.deleteCommunication(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao, mockLiveStateServiceDao);

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockLiveStateServiceDao);
  }

  @Test
  public void deleteCommunicationFromLiveStateOnProviderUpdateWhenCallFailsWhenCommunicationIsBrightttalk() {

    String expectedErrorMessage = "expectedErrorMessage";

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockMessages.getValue(AudioSlidesToMediazonePivotService.DELETE_LIVESTATE_ERROR_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);

    mockLiveStateServiceDao.deleteCommunication(COMMUNICATION_ID);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test exception"));

    EasyMock.replay(mockCommunicationDao, mockLiveStateServiceDao, mockMessages);

    thrown.expect(DataCleanUpFailedException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.pivot(communication, provider);
  }

  @Test
  public void deleteCommunicationSlidesOnProviderUpdateWhenCommunicationIsBrightttalk() {

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockSlideServiceDao.deleteSlides(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao, mockSlideServiceDao);

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockSlideServiceDao);
  }

  @Test
  public void deleteCommunicationSlidesOnProviderUpdateWhenCallFailsWhenCommunicationIsBrightttalk() {

    String expectedErrorMessage = "expectedErrorMessage";

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockMessages.getValue(AudioSlidesToMediazonePivotService.DELETE_SLIDES_ERROR_MESSAGE_KEY)).andReturn(
        expectedErrorMessage);

    mockSlideServiceDao.deleteSlides(COMMUNICATION_ID);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test exception"));

    EasyMock.replay(mockCommunicationDao, mockSlideServiceDao, mockMessages);

    thrown.expect(DataCleanUpFailedException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.pivot(communication, provider);
  }

  @Test
  public void deleteCommunicationVotesOnProviderUpdateWhenCommunicationIsBrightttalk() {

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    final Long voteId1 = 45l;
    final Long voteId2 = 56l;

    List<Long> voteIds = new ArrayList<Long>();
    voteIds.add(voteId1);
    voteIds.add(voteId2);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockAudienceServiceDao.findVoteIds(COMMUNICATION_ID)).andReturn(voteIds);

    mockAudienceServiceDao.deleteVote(voteId1);
    EasyMock.expectLastCall().once();

    mockAudienceServiceDao.deleteVote(voteId2);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao, mockAudienceServiceDao);

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockAudienceServiceDao);
  }

  @Test
  public void deleteCommunicationVotesOnProviderUpdateWhenCallFailsWhenCommunicationIsBrightttalk() {

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    final Long voteId1 = 45l;
    final Long voteId2 = 56l;

    List<Long> voteIds = new ArrayList<Long>();
    voteIds.add(voteId1);
    voteIds.add(voteId2);

    Provider provider = getProvider(Provider.MEDIAZONE);

    final String expectedErrorMessage = "expectedErrorMessage";

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockMessages.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);

    EasyMock.expect(mockAudienceServiceDao.findVoteIds(COMMUNICATION_ID)).andReturn(voteIds);

    mockAudienceServiceDao.deleteVote(voteId1);
    EasyMock.expectLastCall().once();

    mockAudienceServiceDao.deleteVote(voteId2);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test exception"));

    EasyMock.replay(mockCommunicationDao, mockAudienceServiceDao, mockMessages);

    thrown.expect(DataCleanUpFailedException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.pivot(communication, provider);
  }

  @Test
  public void deleteCommunicationResourcesOnProviderUpdateWhenCommunicationIsBrightttalk() {

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    final List<Resource> resources = new ArrayList<Resource>();

    final Long resourceId1 = 45l;
    Resource resource1 = new Resource(resourceId1);
    resources.add(resource1);

    final Long resourceId2 = 56l;
    Resource resource2 = new Resource(resourceId2);
    resources.add(resource2);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.restoreMediazoneAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockResourceDao.findAll(COMMUNICATION_ID)).andReturn(resources);

    mockResourceDao.delete(COMMUNICATION_ID, resourceId1);
    EasyMock.expectLastCall().once();

    mockResourceDao.delete(COMMUNICATION_ID, resourceId2);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockCommunicationDao, mockResourceDao);

    // do test
    uut.pivot(communication, provider);

    EasyMock.verify(mockResourceDao);
  }

  @Test
  public void deleteCommunicationResourcesOnProviderUpdateWhenCallFailsWhenCommunicationIsBrightttalk() {

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    final List<Resource> resources = new ArrayList<Resource>();

    final Long resourceId1 = 45l;
    Resource resource1 = new Resource(resourceId1);
    resources.add(resource1);

    final Long resourceId2 = 56l;
    Resource resource2 = new Resource(resourceId2);
    resources.add(resource2);

    Provider provider = getProvider(Provider.MEDIAZONE);

    final String expectedErrorMessage = "expectedErrorMessage";

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockResourceDao.findAll(COMMUNICATION_ID)).andReturn(resources);

    EasyMock.expect(mockMessages.getValue(isA(String.class), isA(Object[].class))).andReturn(expectedErrorMessage);

    mockResourceDao.delete(COMMUNICATION_ID, resourceId1);
    EasyMock.expectLastCall().once();

    mockResourceDao.delete(COMMUNICATION_ID, resourceId2);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test exception"));

    EasyMock.replay(mockCommunicationDao, mockResourceDao, mockMessages);

    thrown.expect(DataCleanUpFailedException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.pivot(communication, provider);
  }

  @Test
  public void updateProviderWhenMultipleCallsFailWhenCommunicationIsBrightttalk() {

    final String expectedBookingErrorMessage = "bookingErrorMessage";
    final String expectedLiveStateErrorMessage = "liveStateErrorMessage";
    final String expectedErrorMessage = expectedBookingErrorMessage + "; " + expectedLiveStateErrorMessage;

    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communication.setId(COMMUNICATION_ID);

    Provider provider = getProvider(Provider.MEDIAZONE);

    EasyMock.expect(mockCommunicationDao.pivot(communication)).andReturn(communication);

    mockCommunicationDao.removeVotes(COMMUNICATION_ID);
    EasyMock.expectLastCall().once();

    EasyMock.expect(mockMessages.getValue(AudioSlidesToMediazonePivotService.DELETE_BOOKING_ERROR_MESSAGE_KEY)).andReturn(
        expectedBookingErrorMessage);

    EasyMock.expect(mockMessages.getValue(AudioSlidesToMediazonePivotService.DELETE_LIVESTATE_ERROR_MESSAGE_KEY)).andReturn(
        expectedLiveStateErrorMessage);

    List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add(COMMUNICATION_ID);

    mockBookingServiceDao.deleteBookings(communicationIds);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test exception"));

    mockLiveStateServiceDao.deleteCommunication(COMMUNICATION_ID);
    EasyMock.expectLastCall().andThrow(new ApplicationException("test message 2"));

    EasyMock.replay(mockCommunicationDao, mockBookingServiceDao, mockLiveStateServiceDao, mockMessages);

    thrown.expect(DataCleanUpFailedException.class);
    thrown.expect(UserErrorMessageMatcher.hasMessage(expectedErrorMessage));

    // do test
    uut.pivot(communication, provider);
  }

  private Provider getProvider(final String value) {
    return new Provider(value);
  }
}