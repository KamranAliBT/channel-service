/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationServiceTest.java 72595 2014-01-15 13:37:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.easymock.EasyMock.createMock;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.queue.EmailQueuer;
import com.brighttalk.service.channel.business.queue.LesQueuer;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.RenditionDbDao;
import com.brighttalk.service.channel.integration.email.EmailServiceDao;
import com.brighttalk.service.channel.integration.les.LiveEventServiceDao;

/**
 * Unit tests for {@link CommunicationUpdateService}.
 */
public class VideoCommunicationUpdateServiceTest extends AbstractCommunicationServiceTest {

  private VideoCommunicationUpdateService uut;

  @Before
  public void setUp() {

    uut = new VideoCommunicationUpdateService();

    mockCommunicationDao = createMock(CommunicationDbDao.class);
    ReflectionTestUtils.setField(uut, "communicationDbDao", mockCommunicationDao);

    mockRenditionDbDao = createMock(RenditionDbDao.class);
    ReflectionTestUtils.setField(uut, "renditionDbDao", mockRenditionDbDao);

    mockEmailServiceDao = EasyMock.createMock(EmailServiceDao.class);
    ReflectionTestUtils.setField(uut, "emailServiceDao", mockEmailServiceDao);

    mockLiveEventServiceDao = EasyMock.createMock(LiveEventServiceDao.class);
    ReflectionTestUtils.setField(uut, "liveEventServiceDao", mockLiveEventServiceDao);

    mockEmailQueuer = EasyMock.createMock(EmailQueuer.class);
    ReflectionTestUtils.setField(uut, "emailQueuer", mockEmailQueuer);

    mockLesQueuer = EasyMock.createMock(LesQueuer.class);
    ReflectionTestUtils.setField(uut, "lesQueuer", mockLesQueuer);
  }

  @Test
  public void updateSuccessfullWithRenditions() {
    // set up
    Boolean isActiveAsset = true;
    String provider = Provider.VIDEO;
    Status status = Status.RECORDED;
    Long renditionId = 999L;
    Rendition rendition = new Rendition();
    rendition.setId(renditionId);
    rendition.setUrl("url");
    rendition.setBitrate(123L);
    rendition.setCodecs("codecs");
    rendition.setWidth(555L);
    List<Rendition> renditions = new ArrayList<>();
    renditions.add(rendition);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).build();
    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(new Provider(provider)).withStatus(status).withRenditions(renditions).build();

    // expectations
    EasyMock.expect(mockCommunicationDao.isActiveAsset(communication)).andReturn(isActiveAsset);

    EasyMock.expect(mockCommunicationDao.update(communication)).andReturn(communication);

    mockRenditionDbDao.removeRenditionAssets(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    EasyMock.expect(mockRenditionDbDao.findByCommunicationId(COMMUNICATION_ID)).andReturn(renditions);

    mockRenditionDbDao.delete(renditionId);
    EasyMock.expectLastCall();

    EasyMock.expect(mockRenditionDbDao.create(rendition)).andReturn(rendition);

    mockRenditionDbDao.addRenditionAsset(COMMUNICATION_ID, renditionId);
    EasyMock.expectLastCall();

    mockCommunicationDao.updateEncodingCount(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    mockEmailQueuer.videoUploadComplete(COMMUNICATION_ID, provider);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationDao, mockRenditionDbDao, mockEmailQueuer);

    // exercise
    uut.update(channel, communication);

    // assertions
    EasyMock.verify(mockCommunicationDao, mockRenditionDbDao, mockEmailQueuer);
  }

  @Test
  public void updateSuccessfullWithoutRenditions() {
    // set up
    Boolean isActiveAsset = true;
    String provider = Provider.VIDEO;
    Status status = Status.RECORDED;

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).build();
    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(new Provider(provider)).withStatus(status).build();

    // expectations
    EasyMock.expect(mockCommunicationDao.isActiveAsset(communication)).andReturn(isActiveAsset);

    EasyMock.expect(mockCommunicationDao.update(communication)).andReturn(communication);

    mockCommunicationDao.updateEncodingCount(COMMUNICATION_ID);
    EasyMock.expectLastCall();

    mockEmailQueuer.videoUploadComplete(COMMUNICATION_ID, provider);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationDao, mockEmailQueuer);

    // exercise
    uut.update(channel, communication);

    // assertions
    EasyMock.verify(mockCommunicationDao, mockEmailQueuer);
  }

  @Test
  public void updateFailsWithStatusProcessingFail() {
    // set up
    Boolean isActiveAsset = true;
    String provider = Provider.VIDEO;
    Status status = Status.PROCESSINGFAILED;
    Long renditionId = 999L;
    Rendition rendition = new Rendition();
    rendition.setId(renditionId);
    rendition.setUrl("url");
    rendition.setBitrate(123L);
    rendition.setCodecs("codecs");
    rendition.setWidth(555L);
    List<Rendition> renditions = new ArrayList<>();
    renditions.add(rendition);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).build();
    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withProvider(new Provider(provider)).withStatus(status).withRenditions(renditions).build();

    // expectations
    EasyMock.expect(mockCommunicationDao.isActiveAsset(communication)).andReturn(isActiveAsset);

    mockEmailQueuer.videoUploadError(COMMUNICATION_ID, provider);
    EasyMock.expectLastCall();

    EasyMock.replay(mockCommunicationDao, mockEmailQueuer);

    // exercise
    uut.update(channel, communication);

    // assertions
    EasyMock.verify(mockCommunicationDao, mockEmailQueuer);
  }

}