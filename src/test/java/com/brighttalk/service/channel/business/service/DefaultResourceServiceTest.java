/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultResourceServiceTest.java 87022 2014-12-03 13:00:40Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import static org.easymock.EasyMock.createMock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.Session;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.error.InvalidResourceException;
import com.brighttalk.service.channel.business.error.MustBeInMasterChannelException;
import com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException;
import com.brighttalk.service.channel.business.error.NotAllowedToAccessMediaZoneCommunicationException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;

/**
 * Unit tests for {@link AbstractCommunicationService}.
 */
public class DefaultResourceServiceTest {

  private static final Long CHANNEL_ID = 1L;

  private static final Long COMMUNICATION_ID = 2L;

  private static final Long RESOURCE_ID = 5L;

  private static final Long RESOURCE_ID_2 = 6L;

  private static final String RESOURCE_URL = "http://test";

  private static final String RESOURCE_TITLE = "Resource Test Title";

  private static final String RESOURCE_DESCRIPTION = "Resource Test Description";

  private static final String RESOURCE_MIME_TYPE = "application/xml";

  private static final Long RESOURCE_FILE_SIZE = 50l;

  private static final Long USER_ID = 999L;

  private static final String SESSION_ID = "sessionId";

  private DefaultResourceService uut;

  private ResourceDbDao mockResourceDao;

  private CommunicationFinder mockCommunicationFinder;

  @Before
  public void setUp() {
    uut = new DefaultResourceService();

    mockCommunicationFinder = EasyMock.createMock(CommunicationFinder.class);
    ReflectionTestUtils.setField(uut, "communicationFinder", mockCommunicationFinder);

    mockResourceDao = createMock(ResourceDbDao.class);
    ReflectionTestUtils.setField(uut, "resourceDbDao", mockResourceDao);

  }

  @Test
  public void findResourceWhenUserIsManager() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();
    communication.setResources(resources);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.findResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
  }

  @Test
  public void findResourceWhenUserIsPresenter() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();
    communication.setResources(resources);
    User user = createPresenter();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.findResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
  }

  @Test
  public void findResourceWhenUserIsChannelOwner() {
    // setup
    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);

    List<Resource> resources = createResources();

    Communication communication = createCommmunicationInMasterChannel();
    communication.setResources(resources);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.findResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
  }

  @Test
  public void findResourceWhenUserIsChannelManager() {
    // setup
    Long userId = 123L;
    User user = createUser(userId);

    Long channelManagerUserId = 321L;
    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(createChannelManager(channelManagerUserId));

    Channel channel = createChannel();
    channel.setOwnerUserId(user.getId());
    channel.setChannelManagers(channelManagers);

    List<Resource> resources = createResources();

    Communication communication = createCommmunicationInMasterChannel();
    communication.setResources(resources);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.findResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
  }

  @Test(expected = UserAuthorisationException.class)
  public void findResourceWhenUserIsNotAuthorisedUser() {
    // setup
    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Channel channel = createChannel();
    channel.setOwnerUserId(7777L);
    channel.setFeatures(features);

    List<Resource> resources = createResources();

    Communication communication = createCommunicationInConsumerChannel();
    communication.setResources(resources);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.findResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

  }

  @Test(expected = NotAllowedToAccessMediaZoneCommunicationException.class)
  public void findResourceWhenCommunicationIsMediaZoneCommunication() {
    // setup
    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);

    Communication communication = createMediaZoneCommunication();
    List<Resource> resources = createResources();
    communication.setResources(resources);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.findResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

  }

  @Test(expected = NotFoundException.class)
  public void findResourceWhenCommunicationDoesNotExists() {
    // setup
    Channel channel = createChannel();
    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andThrow(new NotFoundException("test"));
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.findResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);
  }

  @Test(expected = NotFoundException.class)
  public void findResourceWhenResourceNotPartOfCommunication() {
    // setup
    Channel channel = createChannel();
    List<Resource> resources = new ArrayList<Resource>();

    Communication communication = createCommmunicationInMasterChannel();
    communication.setResources(resources);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.findResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);
  }

  @Test(expected = InvalidResourceException.class)
  public void findResourceWhenCommunicationFinderThrowsInvalidResourceException() {
    // setup
    Channel channel = createChannel();
    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, COMMUNICATION_ID)).andThrow(
        new InvalidResourceException("Test Invalid"));
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.findResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);
  }

  @Test
  public void findResourcesWhenUserIsManager() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();

    communication.setResources(resources);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    List<Resource> result = uut.findResources(user, channel, COMMUNICATION_ID);

    // assertions
    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(RESOURCE_ID, result.get(0).getId());
  }

  @Test
  public void findResourcesWhenUserIsPresenter() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();

    communication.setResources(resources);

    User user = createPresenter();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    List<Resource> result = uut.findResources(user, channel, COMMUNICATION_ID);

    // assertions
    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(RESOURCE_ID, result.get(0).getId());
  }

  @Test
  public void findResourcesWhenUserIsChannelOwner() {
    // setup
    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();
    communication.setResources(resources);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    List<Resource> result = uut.findResources(user, channel, COMMUNICATION_ID);

    // assertions
    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(RESOURCE_ID, result.get(0).getId());
  }

  @Test
  public void findResourcesWhenUserIsChannelManager() {
    // setup
    Long userId = 123L;
    User user = createUser(userId);

    Long channelManagerUserId = 321L;
    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(createChannelManager(channelManagerUserId));

    Channel channel = createChannel();
    channel.setOwnerUserId(user.getId());
    channel.setChannelManagers(channelManagers);

    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();
    communication.setResources(resources);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    List<Resource> result = uut.findResources(user, channel, COMMUNICATION_ID);

    // assertions
    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(RESOURCE_ID, result.get(0).getId());
  }

  @Test(expected = NotAllowedToAccessMediaZoneCommunicationException.class)
  public void findResourcesWhenCommunicationIsMediaZone() {
    // setup
    Channel channel = createChannel();
    Communication communication = createMediaZoneCommunication();

    List<Resource> resources = createResources();
    communication.setResources(resources);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.findResources(user, channel, COMMUNICATION_ID);

  }

  @Test(expected = UserAuthorisationException.class)
  public void findResourcesWhenUserIsNotAuthorised() {
    // setup
    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Channel channel = createChannel();
    channel.setOwnerUserId(7777L);
    channel.setFeatures(features);

    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();

    communication.setResources(resources);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.findResources(user, channel, COMMUNICATION_ID);

  }

  @Test
  public void findResourcesWhenNoResources() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    List<Resource> resources = new ArrayList<Resource>();

    communication.setResources(resources);
    User user = createManager();
    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(resources);
    EasyMock.replay(mockResourceDao);

    // do test
    List<Resource> result = uut.findResources(user, channel, COMMUNICATION_ID);

    // assertions
    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void createResourceFileAsManager() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    communication.setResources(new ArrayList<Resource>());

    Resource resource = createExternalFileResource();
    Resource resourceToReturn = createExternalFileResource();

    resourceToReturn.setId(RESOURCE_ID);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(new ArrayList<Resource>());
    EasyMock.expect(mockResourceDao.create(COMMUNICATION_ID, resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.createResource(user, channel, COMMUNICATION_ID, resource);

    // assertions
    EasyMock.verify(mockCommunicationFinder);
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
  }

  @Test
  public void createResourceFileWhenUserIsPresenter() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    communication.setResources(new ArrayList<Resource>());

    Resource resource = createExternalFileResource();

    Resource resourceToReturn = createExternalFileResource();
    resourceToReturn.setId(RESOURCE_ID);

    User user = createPresenter();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(new ArrayList<Resource>());
    EasyMock.expect(mockResourceDao.create(COMMUNICATION_ID, resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.createResource(user, channel, COMMUNICATION_ID, resource);

    // assertions
    EasyMock.verify(mockCommunicationFinder);
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());

  }

  @Test
  public void createResourceFileWhenUserIsChannelOwner() {

    // setup
    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);

    Communication communication = createCommmunicationInMasterChannel();
    communication.setResources(new ArrayList<Resource>());

    Resource resource = createExternalFileResource();

    Resource resourceToReturn = createExternalFileResource();
    resourceToReturn.setId(RESOURCE_ID);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(new ArrayList<Resource>());
    EasyMock.expect(mockResourceDao.create(COMMUNICATION_ID, resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.createResource(user, channel, COMMUNICATION_ID, resource);

    // assertions
    EasyMock.verify(mockCommunicationFinder);
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
  }

  @Test
  public void createResourceFileWhenUserIsChannelManager() {

    // setup
    Long userId = 123L;
    User user = createUser(userId);

    Long channelManagerUserId = 321L;
    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(createChannelManager(channelManagerUserId));

    Channel channel = createChannel();
    channel.setOwnerUserId(user.getId());
    channel.setChannelManagers(channelManagers);

    Communication communication = createCommmunicationInMasterChannel();
    communication.setResources(new ArrayList<Resource>());

    Resource resource = createExternalFileResource();

    Resource resourceToReturn = createExternalFileResource();
    resourceToReturn.setId(RESOURCE_ID);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(new ArrayList<Resource>());
    EasyMock.expect(mockResourceDao.create(COMMUNICATION_ID, resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.createResource(user, channel, COMMUNICATION_ID, resource);

    // assertions
    EasyMock.verify(mockCommunicationFinder);
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
  }

  @Test(expected = UserAuthorisationException.class)
  public void createResourceFileWhenUserIsNotAuthorised() {

    // setup
    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Channel channel = createChannel();
    channel.setOwnerUserId(77777L);
    channel.setFeatures(features);

    Communication communication = createCommmunicationInMasterChannel();
    communication.setResources(new ArrayList<Resource>());

    Resource resource = createExternalFileResource();

    Resource resourceToReturn = createExternalFileResource();
    resourceToReturn.setId(RESOURCE_ID);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(new ArrayList<Resource>());
    EasyMock.expect(mockResourceDao.create(COMMUNICATION_ID, resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.createResource(user, channel, COMMUNICATION_ID, resource);

  }

  @Test(expected = NotAllowedToAccessMediaZoneCommunicationException.class)
  public void createResourceFileForMediaZoneCommunication() {
    // setup
    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);

    Communication communication = createMediaZoneCommunication();
    communication.setResources(new ArrayList<Resource>());

    Resource resource = createExternalFileResource();

    Resource resourceToReturn = createExternalFileResource();
    resourceToReturn.setId(RESOURCE_ID);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(communication.getId())).andReturn(new ArrayList<Resource>());
    EasyMock.expect(mockResourceDao.create(COMMUNICATION_ID, resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.createResource(user, channel, COMMUNICATION_ID, resource);
  }

  @Test(expected = MustBeInMasterChannelException.class)
  public void createResourceWhenCommunicationIsNotInMasterChannel() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(false);
    communication.setConfiguration(configuration);
    Resource resource = createLinkResource();

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.createResource(user, channel, COMMUNICATION_ID, resource);
  }

  @Test(expected = NotAllowedAtThisTimeException.class)
  public void createResourceWhenCommunicationIsInLiveState() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    communication.setStatus(Communication.Status.LIVE);
    Resource resource = new Resource();
    resource.setType(Resource.Type.FILE);
    resource.setUrl(RESOURCE_URL);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.createResource(user, channel, COMMUNICATION_ID, resource);

  }

  @Test(expected = NotAllowedAtThisTimeException.class)
  public void createResourceWhenCommunicationIsIn15MinuteWindow() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    Date date = new Date();
    date.setTime(date.getTime() + 10 * 60000);
    communication.setScheduledDateTime(date);
    Resource resource = new Resource();
    resource.setType(Resource.Type.FILE);
    resource.setUrl(RESOURCE_URL);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.createResource(user, channel, COMMUNICATION_ID, resource);
    // assertions
  }

  @Test
  public void updateResourceLinkWhenUserIsManager() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    Resource resource = createLinkResource();
    resource.setId(RESOURCE_ID);

    List<Resource> resources = new ArrayList<Resource>();
    resources.add(resource);
    communication.setResources(resources);

    Resource resourceToReturn = createLinkResource();
    resourceToReturn.setUrl(RESOURCE_URL);
    resourceToReturn.setId(RESOURCE_ID);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.update(resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.updateResource(user, channel, COMMUNICATION_ID, resource);

    // assertions
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
  }

  @Test
  public void updateResourceLinkWhenUserIsPresenter() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    Resource resource = createLinkResource();
    resource.setId(RESOURCE_ID);

    List<Resource> resources = new ArrayList<Resource>();
    resources.add(resource);
    communication.setResources(resources);

    Resource resourceToReturn = createLinkResource();
    resourceToReturn.setUrl(RESOURCE_URL);
    resourceToReturn.setId(RESOURCE_ID);

    User user = createPresenter();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.update(resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.updateResource(user, channel, COMMUNICATION_ID, resource);

    // assertions
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
  }

  @Test
  public void updateResourceLinkWhenUserIsChannelOwner() {
    // setup
    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);
    Communication communication = createCommmunicationInMasterChannel();

    Resource resource = createLinkResource();
    resource.setId(RESOURCE_ID);

    List<Resource> resources = new ArrayList<Resource>();
    resources.add(resource);
    communication.setResources(resources);

    Resource resourceToReturn = createLinkResource();
    resourceToReturn.setUrl(RESOURCE_URL);
    resourceToReturn.setId(RESOURCE_ID);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.update(resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.updateResource(user, channel, COMMUNICATION_ID, resource);

    // assertions
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
  }

  @Test
  public void updateResourceLinkWhenUserIsChannelManager() {
    // setup
    Long userId = 123L;
    User user = createUser(userId);

    Long channelManagerUserId = 321L;
    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(createChannelManager(channelManagerUserId));

    Channel channel = createChannel();
    channel.setOwnerUserId(user.getId());
    channel.setChannelManagers(channelManagers);

    Communication communication = createCommmunicationInMasterChannel();

    Resource resource = createLinkResource();
    resource.setId(RESOURCE_ID);

    List<Resource> resources = new ArrayList<Resource>();
    resources.add(resource);
    communication.setResources(resources);

    Resource resourceToReturn = createLinkResource();
    resourceToReturn.setUrl(RESOURCE_URL);
    resourceToReturn.setId(RESOURCE_ID);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.update(resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.updateResource(user, channel, COMMUNICATION_ID, resource);

    // assertions
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
  }

  @Test(expected = UserAuthorisationException.class)
  public void updateResourceLinkWhenUserIsNotAuthorized() {
    // setup
    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Channel channel = createChannel();
    channel.setOwnerUserId(7777L);
    channel.setFeatures(features);

    Communication communication = createCommmunicationInMasterChannel();

    Resource resource = createLinkResource();
    resource.setId(RESOURCE_ID);

    List<Resource> resources = new ArrayList<Resource>();
    resources.add(resource);
    communication.setResources(resources);

    Resource resourceToReturn = createLinkResource();
    resourceToReturn.setUrl(RESOURCE_URL);
    resourceToReturn.setId(RESOURCE_ID);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    EasyMock.expect(mockResourceDao.findAll(EasyMock.anyLong())).andReturn(resources);
    EasyMock.expect(mockResourceDao.update(resource)).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.updateResource(user, channel, COMMUNICATION_ID, resource);
  }

  @Test(expected = MustBeInMasterChannelException.class)
  public void updateResourceWhenCommunicationIsNotInMasterChannel() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(false);
    communication.setConfiguration(configuration);

    Resource resource = new Resource(RESOURCE_ID);
    resource.setType(Resource.Type.FILE);
    resource.setUrl(RESOURCE_URL);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.updateResource(user, channel, COMMUNICATION_ID, resource);

    // assertions
  }

  @Test(expected = NotAllowedAtThisTimeException.class)
  public void updateResourceWhenCommunicationIsInLiveState() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    communication.setStatus(Communication.Status.LIVE);
    Resource resource = new Resource(RESOURCE_ID);
    resource.setType(Resource.Type.FILE);
    resource.setUrl(RESOURCE_URL);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.updateResource(user, channel, COMMUNICATION_ID, resource);
    // assertions
  }

  @Test(expected = NotAllowedAtThisTimeException.class)
  public void updateResourceWhenCommunicationIsIn15MinuteWindow() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    Date date = new Date();
    date.setTime(date.getTime() + 10 * 60000);
    communication.setScheduledDateTime(date);
    Resource resource = new Resource(RESOURCE_ID);
    resource.setType(Resource.Type.FILE);
    resource.setUrl(RESOURCE_URL);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.updateResource(user, channel, COMMUNICATION_ID, resource);
    // assertions
  }

  @Test
  public void updateResourceInternal() {
    // setup
    Resource resource = new Resource(RESOURCE_ID);
    resource.setType(Resource.Type.FILE);
    resource.setHosted(Resource.Hosted.INTERNAL);
    resource.setUrl(RESOURCE_URL);
    resource.setMimeType(RESOURCE_MIME_TYPE);
    resource.setSize(RESOURCE_FILE_SIZE);

    Resource existingResource = new Resource(RESOURCE_ID);
    existingResource.setType(Resource.Type.FILE);
    existingResource.setTitle(RESOURCE_TITLE);
    existingResource.setDescription(RESOURCE_DESCRIPTION);
    existingResource.setHosted(Resource.Hosted.INTERNAL);

    Resource resourceToReturn = new Resource(RESOURCE_ID);
    resourceToReturn.setType(Resource.Type.FILE);
    resourceToReturn.setUrl(RESOURCE_URL);

    // expectations
    EasyMock.expect(mockResourceDao.find(EasyMock.anyLong())).andReturn(existingResource);
    EasyMock.expect(mockResourceDao.update(EasyMock.anyObject(Resource.class))).andReturn(resourceToReturn);
    EasyMock.replay(mockResourceDao);

    // do test
    Resource result = uut.updateResource(resource);

    // assertions
    EasyMock.verify(mockResourceDao);
    assertNotNull(result);
    assertEquals(RESOURCE_ID, result.getId());
    assertEquals(Resource.Type.FILE, result.getType());
  }

  @Test
  public void updateResourcesOrderWhenUserIsManager() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources(RESOURCE_ID, RESOURCE_ID_2);
    communication.setResources(resources);

    Communication communication2 = createCommmunicationInMasterChannel();
    List<Resource> resourcesNewOrder = createResources(RESOURCE_ID_2, RESOURCE_ID);
    communication2.setResources(resourcesNewOrder);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication2);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.updateOrder(resourcesNewOrder);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    List<Resource> result = uut.updateResourcesOrder(user, channel, COMMUNICATION_ID, resourcesNewOrder);

    // assertions
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(RESOURCE_ID, result.get(1).getId());
    assertEquals(Integer.valueOf(1), result.get(1).getPrecedence());
    assertEquals(RESOURCE_ID_2, result.get(0).getId());
    assertEquals(Integer.valueOf(0), result.get(0).getPrecedence());
  }

  @Test
  public void updateResourcesOrderWhenUserIsPresenter() {

    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources(RESOURCE_ID, RESOURCE_ID_2);
    communication.setResources(resources);

    Communication communication2 = createCommmunicationInMasterChannel();
    List<Resource> resourcesNewOrder = createResources(RESOURCE_ID_2, RESOURCE_ID);
    communication2.setResources(resourcesNewOrder);

    User user = createPresenter();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication2);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.updateOrder(resourcesNewOrder);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    List<Resource> result = uut.updateResourcesOrder(user, channel, COMMUNICATION_ID, resourcesNewOrder);

    // assertions
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(RESOURCE_ID, result.get(1).getId());
    assertEquals(Integer.valueOf(1), result.get(1).getPrecedence());
    assertEquals(RESOURCE_ID_2, result.get(0).getId());
    assertEquals(Integer.valueOf(0), result.get(0).getPrecedence());
  }

  @Test
  public void updateResourcesOrderWhenUserIsChannelOwner() {
    // setup
    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources(RESOURCE_ID, RESOURCE_ID_2);
    communication.setResources(resources);

    Communication communication2 = createCommmunicationInMasterChannel();
    List<Resource> resourcesNewOrder = createResources(RESOURCE_ID_2, RESOURCE_ID);
    communication2.setResources(resourcesNewOrder);

    User user = createPresenter();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication2);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.updateOrder(resourcesNewOrder);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    List<Resource> result = uut.updateResourcesOrder(user, channel, COMMUNICATION_ID, resourcesNewOrder);

    // assertions
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(RESOURCE_ID, result.get(1).getId());
    assertEquals(Integer.valueOf(1), result.get(1).getPrecedence());
    assertEquals(RESOURCE_ID_2, result.get(0).getId());
    assertEquals(Integer.valueOf(0), result.get(0).getPrecedence());
  }

  @Test(expected = UserAuthorisationException.class)
  public void updateResourcesOrderAsNotAuthorizedUser() {
    // setup
    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Channel channel = createChannel();
    channel.setOwnerUserId(777L);
    channel.setFeatures(features);

    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources(RESOURCE_ID, RESOURCE_ID_2);
    communication.setResources(resources);

    Communication communication2 = createCommmunicationInMasterChannel();
    List<Resource> resourcesNewOrder = createResources(RESOURCE_ID_2, RESOURCE_ID);
    communication2.setResources(resourcesNewOrder);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.expect(mockCommunicationFinder.find(channel, communication2.getId())).andReturn(communication2);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.updateOrder(resourcesNewOrder);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.updateResourcesOrder(user, channel, COMMUNICATION_ID, resourcesNewOrder);

  }

  @Test(expected = ApplicationException.class)
  public void updateResourcesOrderWhenNewOrderProvidedHasInvalidElement() {

    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources(RESOURCE_ID, RESOURCE_ID_2);
    communication.setResources(resources);

    Communication communication2 = createCommmunicationInMasterChannel();
    List<Resource> resourcesNewOrder = new ArrayList<Resource>();
    resourcesNewOrder.add(new Resource(99999L));
    communication2.setResources(resourcesNewOrder);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication2);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.updateOrder(resourcesNewOrder);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.updateResourcesOrder(user, channel, COMMUNICATION_ID, resourcesNewOrder);

  }

  @Test(expected = ApplicationException.class)
  public void updateResourcesOrderDuplicateElement() {

    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources(RESOURCE_ID, RESOURCE_ID_2);
    communication.setResources(resources);

    Communication communication2 = createCommmunicationInMasterChannel();
    List<Resource> resourcesNewOrder = createResources(RESOURCE_ID, RESOURCE_ID);
    communication2.setResources(resourcesNewOrder);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication2);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.updateOrder(resourcesNewOrder);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.updateResourcesOrder(user, channel, COMMUNICATION_ID, resourcesNewOrder);
  }

  @Test(expected = ApplicationException.class)
  public void updateResourcesOrderNotAllElementProvided() {

    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources(RESOURCE_ID, RESOURCE_ID_2);
    communication.setResources(resources);

    Communication communication2 = createCommmunicationInMasterChannel();
    List<Resource> resourcesNewOrder = new ArrayList<Resource>();
    resourcesNewOrder.add(new Resource(RESOURCE_ID));
    communication2.setResources(resourcesNewOrder);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication2);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.updateOrder(resourcesNewOrder);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.updateResourcesOrder(user, channel, COMMUNICATION_ID, resourcesNewOrder);
  }

  @Test
  public void deleteResourceFileWhenUserIsManager() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();
    communication.setResources(resources);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.delete(communication.getId(), RESOURCE_ID);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.deleteResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
  }

  @Test
  public void deleteResourceFileWhenUserIsPresenter() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();
    communication.setResources(resources);

    User user = createPresenter();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.delete(communication.getId(), RESOURCE_ID);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.deleteResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
  }

  @Test
  public void deleteResourceFileWhenUserIsChannelOwner() {
    // setup
    Channel channel = createChannel();
    channel.setOwnerUserId(USER_ID);
    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();
    communication.setResources(resources);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.delete(communication.getId(), RESOURCE_ID);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.deleteResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
  }

  @Test
  public void deleteResourceFileWhenUserIsChannelManager() {
    // setup
    Long userId = 123L;
    User user = createUser(userId);

    Long channelManagerUserId = 321L;
    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();
    channelManagers.add(createChannelManager(channelManagerUserId));

    Channel channel = createChannel();
    channel.setOwnerUserId(user.getId());
    channel.setChannelManagers(channelManagers);

    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();
    communication.setResources(resources);

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    mockResourceDao.delete(communication.getId(), RESOURCE_ID);
    EasyMock.expectLastCall().times(1);
    EasyMock.replay(mockResourceDao);

    // do test
    uut.deleteResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);

    // assertions
    EasyMock.verify(mockResourceDao, mockCommunicationFinder);
  }

  @Test(expected = UserAuthorisationException.class)
  public void deleteResourceFileWhenUserIsNotAuthorized() {
    // setup
    Feature feature = new Feature(Feature.Type.CHANNEL_MANAGERS);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Channel channel = createChannel();
    channel.setOwnerUserId(777L);
    channel.setFeatures(features);

    Communication communication = createCommmunicationInMasterChannel();

    List<Resource> resources = createResources();
    communication.setResources(resources);

    User user = createUser();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.deleteResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);
  }

  @Test(expected = MustBeInMasterChannelException.class)
  public void deleteResourceFileWhenCommunicationIsNotInMasterChannel() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(false);
    communication.setConfiguration(configuration);
    Resource resource = new Resource(RESOURCE_ID);
    resource.setType(Resource.Type.FILE);
    resource.setUrl(RESOURCE_URL);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.deleteResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);
    // assertions
  }

  @Test(expected = NotAllowedAtThisTimeException.class)
  public void deleteResourceFileWhenCommunicationIsInLiveState() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    communication.setStatus(Communication.Status.LIVE);

    Resource resource = new Resource(RESOURCE_ID);
    resource.setType(Resource.Type.FILE);
    resource.setUrl(RESOURCE_URL);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.deleteResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);
    // assertions
  }

  @Test(expected = NotAllowedAtThisTimeException.class)
  public void deleteResourceWhenCommunicationIsIn15MinuteWindow() {
    // setup
    Channel channel = createChannel();
    Communication communication = createCommmunicationInMasterChannel();
    Date date = new Date();
    date.setTime(date.getTime() + 10 * 60000);
    communication.setScheduledDateTime(date);
    Resource resource = new Resource(RESOURCE_ID);
    resource.setType(Resource.Type.FILE);
    resource.setUrl(RESOURCE_URL);

    User user = createManager();

    // expectations
    EasyMock.expect(mockCommunicationFinder.find(channel, communication.getId())).andReturn(communication);
    EasyMock.replay(mockCommunicationFinder);

    // do test
    uut.deleteResource(user, channel, COMMUNICATION_ID, RESOURCE_ID);
    // assertions
  }

  private Channel createChannel() {
    Channel channel = new Channel(CHANNEL_ID);
    return channel;
  }

  private Communication createCommmunicationInMasterChannel() {
    return createCommunication(true);
  }

  private Communication createCommunicationInConsumerChannel() {
    return createCommunication(false);
  }

  private Communication createCommunication(final boolean isMasterChannel) {

    Communication communication = new Communication(COMMUNICATION_ID);

    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(isMasterChannel);

    communication.setConfiguration(configuration);
    communication.setStatus(Communication.Status.UPCOMING);
    communication.setProvider(new Provider(Provider.BRIGHTTALK));

    Date date = new Date();
    date.setTime(date.getTime() + 60 * 60000);
    communication.setScheduledDateTime(date);
    communication.getPresenters().add(new Session(SESSION_ID));

    return communication;
  }

  private Communication createMediaZoneCommunication() {
    Communication communication = createCommunication(true);
    communication.setProvider(new Provider(Provider.MEDIAZONE));
    return communication;
  }

  private User createManager() {

    User user = new User();
    user.setId(USER_ID);
    List<User.Role> roles = new ArrayList<User.Role>();
    roles.add(User.Role.MANAGER);
    user.setRoles(roles);
    return user;
  }

  private User createUser() {

    return createUser(USER_ID);
  }

  private User createUser(final Long userId) {

    User user = new User();
    user.setId(userId);
    List<User.Role> roles = new ArrayList<User.Role>();
    roles.add(User.Role.USER);
    user.setRoles(roles);
    return user;
  }

  private User createPresenter() {

    User user = new User();
    user.setId(USER_ID);
    List<User.Role> roles = new ArrayList<User.Role>();
    roles.add(User.Role.USER);
    user.setRoles(roles);
    user.setSession(new Session(SESSION_ID));
    return user;
  }

  private List<Resource> createResources() {
    List<Resource> resources = new ArrayList<Resource>();
    Resource resource = new Resource();
    resource.setId(RESOURCE_ID);
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL);
    resources.add(resource);
    return resources;
  }

  private List<Resource> createResources(final Long resourceId1, final Long resourceId2) {
    List<Resource> resources = new ArrayList<Resource>();
    Resource resource = new Resource(resourceId1);
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL);
    resource.setPrecedence(0);

    Resource resource2 = new Resource(resourceId2);
    resource2.setType(Resource.Type.FILE);
    resource2.setHosted(Resource.Hosted.EXTERNAL);
    resource2.setUrl(RESOURCE_URL);
    resource2.setPrecedence(1);

    resources.add(resource);
    resources.add(resource2);

    return resources;
  }

  private Resource createExternalFileResource() {
    Resource resource = new Resource();
    resource.setTitle(RESOURCE_TITLE);
    resource.setType(Resource.Type.FILE);
    resource.setHosted(Resource.Hosted.EXTERNAL);
    resource.setSize(1024L);
    resource.setMimeType("application/pdf");
    resource.setUrl(RESOURCE_URL);
    return resource;
  }

  private Resource createLinkResource() {
    Resource resource = new Resource();
    resource.setTitle(RESOURCE_TITLE);
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL);
    return resource;
  }

  private ChannelManager createChannelManager(final Long userId) {

    User user = new User();
    user.setId(userId);

    ChannelManager channelManager = new ChannelManager();
    channelManager.setUser(user);

    return channelManager;
  }

}
