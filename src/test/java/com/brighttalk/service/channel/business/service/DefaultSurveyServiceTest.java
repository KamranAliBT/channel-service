/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultSurveyServiceTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import junit.framework.Assert;

import java.util.Random;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.form.domain.Form;
import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.error.SurveyNotFoundException;
import com.brighttalk.service.channel.integration.database.SurveyDbDao;
import com.brighttalk.service.channel.integration.survey.SurveyServiceDao;

/**
 * Unit tests for {@link DefaultSurveyService}.
 */
public class DefaultSurveyServiceTest {

  private DefaultSurveyService uut;

  private SurveyDbDao mockSurveyDbDao;

  private SurveyServiceDao mockSurveyServiceDao;

  @Before
  public void setUp() {
    uut = new DefaultSurveyService();

    mockSurveyDbDao = EasyMock.createMock(SurveyDbDao.class);
    mockSurveyServiceDao = EasyMock.createMock(SurveyServiceDao.class);
    uut.setSurveyDbDao(mockSurveyDbDao);
    uut.setSurveyServiceDao(mockSurveyServiceDao);
  }

  @Test
  public void findChannelIdChannelSurvey() {
    // Set up
    Random r = new Random();
    long surveyId = (long) r.nextInt(999999999);
    long channelId = (long) r.nextInt(999999999);
    Survey survey = new Survey(surveyId, channelId, true);

    // Expectations
    Survey expectedResult = new Survey(surveyId, channelId, true);

    EasyMock.expect(this.mockSurveyDbDao.findChannelSurvey(surveyId)).andReturn(survey);

    EasyMock.replay(this.mockSurveyDbDao);

    // Do Test
    Survey result = uut.findSurvey(surveyId);

    // Assertions
    Assert.assertEquals(expectedResult, result);
    EasyMock.verify(this.mockSurveyDbDao);
  }

  @Test
  public void findChannelIdCommunicationSurvey() {
    // Set up
    Random r = new Random();
    long surveyId = (long) r.nextInt(999999999);
    long channelId = (long) r.nextInt(999999999);
    Survey survey = new Survey(surveyId, channelId, true);

    // Expectations
    Survey expectedResult = new Survey(surveyId, channelId, true);

    EasyMock.expect(this.mockSurveyDbDao.findChannelSurvey(surveyId)).andReturn(null);
    EasyMock.expect(this.mockSurveyDbDao.findCommunicationSurvey(surveyId)).andReturn(survey);

    EasyMock.replay(this.mockSurveyDbDao);

    // Do Test
    Survey result = uut.findSurvey(surveyId);

    // Assertions
    Assert.assertEquals(expectedResult, result);
    EasyMock.verify(this.mockSurveyDbDao);
  }

  @Test(expected = SurveyNotFoundException.class)
  public void findChannelIdSurveyNotFound() {
    // Set up
    Random r = new Random();
    long surveyId = (long) r.nextInt(999999999);

    // Expectations
    Long expectedResult = null;

    EasyMock.expect(this.mockSurveyDbDao.findChannelSurvey(surveyId)).andReturn(null);
    EasyMock.expect(this.mockSurveyDbDao.findCommunicationSurvey(surveyId)).andReturn(null);

    EasyMock.replay(this.mockSurveyDbDao, this.mockSurveyServiceDao);

    // Do Test
    Survey result = uut.findSurvey(surveyId);

    // Assertions
    Assert.assertEquals(expectedResult, result);
    EasyMock.verify(this.mockSurveyDbDao, this.mockSurveyServiceDao);
  }

  @Test
  public void loadSurvey() {
    // Set up
    Random r = new Random();
    long surveyId = (long) r.nextInt(999999999);

    Form form = new Form();

    Survey survey = new Survey(surveyId);

    // Expectations
    Survey expectedResult = new Survey(surveyId, form);

    EasyMock.expect(this.mockSurveyServiceDao.getSurveyForm(surveyId)).andReturn(form);

    EasyMock.replay(this.mockSurveyDbDao, this.mockSurveyServiceDao);

    // Do Test
    Survey result = uut.loadSurvey(survey);

    // Assertions

    Assert.assertEquals(expectedResult, result);
    EasyMock.verify(this.mockSurveyDbDao, this.mockSurveyServiceDao);
  }

  @Test(expected = SurveyNotFoundException.class)
  public void loadSurveyWhenReturnedSuveryIsNull() {
    // Set up
    Random r = new Random();
    long surveyId = (long) r.nextInt(999999999);

    Survey survey = new Survey(surveyId);

    // Expectations
    EasyMock.expect(this.mockSurveyServiceDao.getSurveyForm(surveyId)).andReturn(null);

    EasyMock.replay(this.mockSurveyDbDao, this.mockSurveyServiceDao);

    // Do Test
    uut.loadSurvey(survey);
  }
}
