/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationResourceModificationValidatorTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.error.MustBeInMasterChannelException;
import com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException;

/**
 * Unit tests for {@link CommunicationResourceModificationValidator}.
 */
public class CommunicationResourceModificationValidatorTest extends AbstractResourceTest {

  private CommunicationResourceModificationValidator uut;

  @Before
  public void setUp() {
    uut = new CommunicationResourceModificationValidator();
  }

  /**
   * Test validation communication provided by MediaZone.
   */
  @Test
  public void testValidateMediaZoneCommunication() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    communication.setProvider(new Provider(Provider.MEDIAZONE));

    // do test
    try {
      uut.validate(communication);
      // assert
      Assert.fail();
    } catch (ApplicationException ae) {
      Assert.assertEquals("NotAllowedToAccessMediaZoneCommunication", ae.getUserErrorCode());
    }
  }

  /**
   * Test validation communication provided by BrightTALK which is in 15 minute window.
   */
  @Test
  public void testValidateCommunicationIn15MinuteWindow() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    Date date = new Date();
    date.setTime(date.getTime() + 10 * 60000);
    communication.setScheduledDateTime(date);

    // do test
    try {
      uut.validate(communication);
      // assert
      Assert.fail();
    } catch (NotAllowedAtThisTimeException e) {
      Assert.assertEquals("NotAllowedAtThisTime", e.getUserErrorCode());
    }
  }

  /**
   * Test validation communication provided should be in Live state but nobody dialed in.
   */
  @Test
  public void testValidateCommunicationWhichIsPending() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    Date date = new Date();
    date.setTime(date.getTime() - 10 * 60000);
    communication.setStatus(Status.UPCOMING);
    communication.setScheduledDateTime(date);

    // do test
    try {
      uut.validate(communication);
      // assert
      Assert.fail();
    } catch (NotAllowedAtThisTimeException e) {
      Assert.assertEquals("NotAllowedAtThisTime", e.getUserErrorCode());
    }
  }

  /**
   * Test validation communication provided by BrightTALK which is outside 15 minute window.
   */
  @Test
  public void testValidateCommunicationIsLive() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    communication.setStatus(Status.LIVE);

    // do test
    try {
      uut.validate(communication);
      // assert
      Assert.fail();
    } catch (NotAllowedAtThisTimeException e) {
      Assert.assertEquals("NotAllowedAtThisTime", e.getUserErrorCode());
    }
  }

  /**
   * Test validation communication provided by BrightTALK which is canceled.
   */
  @Test
  public void testValidateCommunicationIsCanceled() {

    // setup
    Communication communication = createCommunicationMasterChannel();
    communication.setStatus(Status.CANCELLED);

    // do test
    try {
      uut.validate(communication);
      // assert

    } catch (NotAllowedAtThisTimeException e) {
      Assert.fail();
    }
  }

  /**
   * Test validation communication provided by BrightTALK which is not in master channel.
   */
  @Test
  public void testValidateCommunicationIsNotInMasterChannel() {

    // setup
    Communication communication = createCommunicationConsumerChannel();

    // do test
    try {
      uut.validate(communication);
      // assert
      Assert.fail();
    } catch (MustBeInMasterChannelException e) {
      Assert.assertEquals("MustBeInMasterChannel", e.getUserErrorCode());
    }
  }

  /**
   * Test validation master channel communication.
   */
  @Test
  public void testValidateCommunicationInMasterChannel() {

    // setup
    Communication communication = createCommunicationMasterChannel();

    // do test
    try {
      uut.validate(communication);
      // assert
    } catch (ApplicationException e) {
      Assert.fail();
    }
  }
}
