/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationTest.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.user.Session;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.error.DialledInTooEarlyException;
import com.brighttalk.service.channel.business.error.DialledInTooLateException;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * Unit tests for {@link Communication}.
 */
public class CommunicationTest {

  private static final Long COMMUNICATION_ID = 2L;

  private static final Long RESOURCE_ID = 5L;

  private static final String SESSION_ID = "sessionId";

  private Communication uut;

  @Before
  public void setUp() {
    uut = new CommunicationBuilder().withDefaultValues().build();
    uut.setId(COMMUNICATION_ID);

    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(true);

    Provider provider = new Provider(Provider.BRIGHTTALK);

    uut.setConfiguration(configuration);
    uut.setProvider(provider);
    uut.setStatus(Communication.Status.UPCOMING);
    Date date = new Date();
    date.setTime(date.getTime() + 60 * 60000);
    uut.setScheduledDateTime(date);
  }

  @Test(expected = NotFoundException.class)
  public void testResourceNotFound() {
    // do test
    uut.setResources(new ArrayList<Resource>());

    // do test
    uut.getResource(RESOURCE_ID);
  }

  @Test
  public void testGetResourceById() {
    // setup
    List<Resource> resources = new ArrayList<Resource>();
    Resource resource = new Resource();
    resource.setId(RESOURCE_ID);
    resources.add(resource);
    uut.setResources(resources);
    // do test
    Resource result = uut.getResource(RESOURCE_ID);

    // assertions
    assertEquals(resource, result);
  }

  @Test(expected = NotFoundException.class)
  public void testGetResourceByIdNotFound() {
    // setup
    List<Resource> resources = new ArrayList<Resource>();
    uut.setResources(resources);
    // do test
    uut.getResource(RESOURCE_ID);
  }

  @Test
  public void testHasResourceIdNotExistingId() {
    // setup
    List<Resource> resources = new ArrayList<Resource>();
    Resource resource = new Resource();
    resource.setId(RESOURCE_ID);
    resources.add(resource);
    uut.setResources(resources);
    // do test
    boolean result = uut.hasResource(9999L);
    assertFalse(result);
  }

  @Test
  public void testHasResourceIdExistingId() {
    // setup
    List<Resource> resources = new ArrayList<Resource>();
    Resource resource = new Resource();
    resource.setId(RESOURCE_ID);
    resources.add(resource);
    uut.setResources(resources);
    // do test
    boolean result = uut.hasResource(RESOURCE_ID);
    assertTrue(result);
  }

  @Test
  public void testCurrentUserIsAPresenter() {
    // setup
    List<Session> presentersSessions = new ArrayList<Session>();
    presentersSessions.add(new Session(SESSION_ID));

    User user = new User();
    user.setId(9999L);
    user.setSession(new Session(SESSION_ID));

    uut.setPresenters(presentersSessions);
    // do test
    boolean result = uut.isPresenter(user);
    // assertions
    assertTrue(result);
  }

  @Test
  public void testCurrentUserIsNotAPresenter() {
    // setup
    List<Session> presentersSessions = new ArrayList<Session>();
    presentersSessions.add(new Session(SESSION_ID));

    User user = new User();
    user.setId(9999L);
    user.setSession(new Session("testSession"));

    uut.setPresenters(presentersSessions);
    // do test
    boolean result = uut.isPresenter(user);
    // assertions
    assertFalse(result);
  }

  @Test
  public void testCurrentUserAndNoPresenterListInCommunication() {
    // setup
    List<Session> presentersSessions = new ArrayList<Session>();

    User user = new User();
    user.setId(9999L);
    user.setSession(new Session(SESSION_ID));

    uut.setPresenters(presentersSessions);
    // do test
    boolean result = uut.isPresenter(user);
    // assertions
    assertFalse(result);
  }

  @Test
  public void testIsPendingFalse() {
    // setup
    // do test
    boolean result = uut.isPending();
    // assertions
    assertFalse(result);
  }

  @Test
  public void testIsPending10After() {
    // setup
    Date date = new Date();
    date.setTime(date.getTime() - 10 * 60000);
    uut.setScheduledDateTime(date);

    // do test
    boolean result = uut.isPending();
    // assertions
    assertTrue(result);
  }

  @Test
  public void testIsPending4minutesBefore() {
    // setup
    Date date = new Date();
    date.setTime(date.getTime() + 4 * 60000);
    uut.setScheduledDateTime(date);

    // do test
    boolean result = uut.isPending();
    // assertions
    assertTrue(result);
  }

  @Test
  public void testIsPending5minutesBefore() {
    // setup
    Date date = new Date();
    date.setTime(date.getTime() + 5 * 60000);
    uut.setScheduledDateTime(date);

    // do test
    boolean result = uut.isPending();
    // assertions
    assertTrue(result);
  }

  @Test
  public void testIsPending6minutesBefore() {
    // setup
    Date date = new Date();
    date.setTime(date.getTime() + 6 * 60000);
    uut.setScheduledDateTime(date);

    // do test
    boolean result = uut.isPending();
    // assertions
    assertFalse(result);
  }

  @Test
  public void testIsIn15MinuteWindow20MinutesBeforeScheduledTimeReturnsFalse() {
    // setup
    Date date = new Date();
    date.setTime(date.getTime() + 20 * 60000);
    uut.setScheduledDateTime(date);
    // do test
    boolean result = uut.isIn15MinuteWindow();
    // assertions
    assertFalse(result);
  }

  @Test
  public void testIsIn15MinuteWindow16MinutesBeforeScheduledTimeReturnsFalse() {
    // setup
    Date date = new Date();
    date.setTime(date.getTime() + 16 * 60000);
    uut.setScheduledDateTime(date);
    // do test
    boolean result = uut.isIn15MinuteWindow();
    // assertions
    assertFalse(result);
  }

  @Test
  public void testIsIn15MinuteWindow14MinutesBeforeScheduledTimeReturnsTrue() {
    // setup
    Date date = new Date();
    date.setTime(date.getTime() + 14 * 60000);
    uut.setScheduledDateTime(date);
    // do test
    boolean result = uut.isIn15MinuteWindow();
    // assertions
    assertTrue(result);
  }

  @Test
  public void testIsIn15MinuteWindow5MinutesBeforeScheduledTimeReturnsTrue() {
    // setup
    Date date = new Date();
    date.setTime(date.getTime() + 5 * 60000);
    uut.setScheduledDateTime(date);
    // do test
    boolean result = uut.isIn15MinuteWindow();
    // assertions
    assertTrue(result);
  }

  @Test
  public void testIsIn15MinuteWindowAtScheduledTimeReturnsFalse() {
    // setup
    Date date = new Date();
    date.setTime(date.getTime());
    uut.setScheduledDateTime(date);
    // do test
    boolean result = uut.isIn15MinuteWindow();
    // assertions
    assertFalse(result);
  }

  @Test
  public void testIsInMasterChannelTrue() {
    // setup
    // do test
    boolean result = uut.getConfiguration().isInMasterChannel();
    // assertions
    assertTrue(result);
  }

  @Test
  public void testIsInMasterChannelFalse() {
    // setup
    uut.getConfiguration().setInMasterChannel(false);
    // do test
    boolean result = uut.getConfiguration().isInMasterChannel();
    // assertions
    assertFalse(result);
  }

  @Test
  public void mergeTitle() {

    final String expectedTitle = "expectedTitle";

    final Communication communication = new CommunicationBuilder().withDefaultValues().withTitle(expectedTitle).build();

    // do test
    uut.merge(communication);

    assertEquals(expectedTitle, uut.getTitle());
    assertEquals(CommunicationBuilder.DEFAULT_DESCRIPTION, uut.getDescription());
    assertEquals(CommunicationBuilder.DEFAULT_KEYWORDS, uut.getKeywords());
    assertEquals(CommunicationBuilder.DEFAULT_PRESENTER, uut.getAuthors());
    assertEquals(Provider.BRIGHTTALK, uut.getProvider().getName());
  }

  @Test
  public void mergeDescription() {

    final String expectedDescription = "expectedDescription";

    final Communication communication = new CommunicationBuilder().withDefaultValues().withDescription(
        expectedDescription).build();

    // do test
    uut.merge(communication);
    assertEquals(CommunicationBuilder.DEFAULT_TITLE, uut.getTitle());
    assertEquals(expectedDescription, uut.getDescription());
    assertEquals(CommunicationBuilder.DEFAULT_KEYWORDS, uut.getKeywords());
    assertEquals(CommunicationBuilder.DEFAULT_PRESENTER, uut.getAuthors());
    assertEquals(Provider.BRIGHTTALK, uut.getProvider().getName());
  }

  @Test
  public void mergeKeywords() {

    final String expectedKeywords = "expectedKeywords";

    final Communication communication = new CommunicationBuilder().withDefaultValues().withKeywords(expectedKeywords).build();

    // do test
    uut.merge(communication);
    assertEquals(CommunicationBuilder.DEFAULT_TITLE, uut.getTitle());
    assertEquals(CommunicationBuilder.DEFAULT_DESCRIPTION, uut.getDescription());
    assertEquals(expectedKeywords, uut.getKeywords());
    assertEquals(CommunicationBuilder.DEFAULT_PRESENTER, uut.getAuthors());
    assertEquals(Provider.BRIGHTTALK, uut.getProvider().getName());
  }

  @Test
  public void mergePresenter() {

    final String expectedPresenter = "expectedPresenter";

    final Communication communication = new CommunicationBuilder().withDefaultValues().withPresenter(expectedPresenter).build();

    // do test
    uut.merge(communication);
    assertEquals(CommunicationBuilder.DEFAULT_TITLE, uut.getTitle());
    assertEquals(CommunicationBuilder.DEFAULT_DESCRIPTION, uut.getDescription());
    assertEquals(CommunicationBuilder.DEFAULT_KEYWORDS, uut.getKeywords());
    assertEquals(expectedPresenter, uut.getAuthors());
    assertEquals(Provider.BRIGHTTALK, uut.getProvider().getName());
  }

  @Test
  public void testEntryTime() {
    // setup
    Date date = new Date();
    long scheduledDateTime = date.getTime() + 6 * 60 * 1000;
    date.setTime(scheduledDateTime);
    uut.setScheduledDateTime(date);

    Date expectedEntryTime = new Date();
    expectedEntryTime.setTime(scheduledDateTime - (5 * 60 * 1000));

    // do test
    Date result = uut.getEntryTime();

    // assertions
    assertEquals(expectedEntryTime, result);
  }

  @Test
  public void testCloseTimeWhenUpcoming() {
    // setup
    Integer duration = 900;
    Date date = new Date();
    long scheduledDateTime = date.getTime() + 6 * 60 * 1000;
    date.setTime(scheduledDateTime);

    uut.setScheduledDateTime(date);
    uut.setDuration(duration);
    uut.setStatus(Status.UPCOMING);

    Date expectedCloseTime = new Date();
    expectedCloseTime.setTime(scheduledDateTime + (duration / 60) * 60 * 1000 + (5 * 60 * 1000));

    // do test
    Date result = uut.getCloseTime();

    // assertions
    assertEquals(expectedCloseTime, result);
  }

  @Test
  public void testCloseTimeWhenRecorded() {
    // setup
    Integer duration = 848;
    Date date = new Date();
    long scheduledDateTime = date.getTime();
    date.setTime(scheduledDateTime);

    uut.setScheduledDateTime(date);
    uut.setDuration(duration);
    uut.setStatus(Status.RECORDED);

    Date expectedCloseTime = new Date();
    expectedCloseTime.setTime(scheduledDateTime + duration * 1000);

    // do test
    Date result = uut.getCloseTime();

    // assertions
    assertEquals(expectedCloseTime, result);
  }

  @Test(expected = DialledInTooEarlyException.class)
  public void testCanDialInThrowsTooEarlyException() {
    // set up
    Integer duration = 60;
    Date scheduledDateTime = getScheduledTime(100);

    uut.setScheduledDateTime(scheduledDateTime);
    uut.setDuration(duration);

    // do test
    uut.canDialIn();
  }

  @Test(expected = DialledInTooLateException.class)
  public void testCanDialInThrowsTooLateException() {
    // set up
    Integer duration = 60;
    Date scheduledDateTime = getScheduledTime(-100);

    uut.setScheduledDateTime(scheduledDateTime);
    uut.setDuration(duration);

    // do test
    uut.canDialIn();
  }

  @Test
  public void testCopyConstructor() {
    // SET UP
    Long id = 123456L;
    Long channelId = 654321L;
    String title = "Copy Constructor test";
    Status status = Status.UPCOMING;
    Date scheduledDateTime = new Date();
    String timeZone = "Europe/Paris";
    String authors = "test author";

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(id).withChannelId(channelId).withTitle(
        title).withStatus(status).withScheduledDateTime(scheduledDateTime).withTimeZone(timeZone).withPresenter(authors).build();

    // Exercise
    Communication copiedCommunication = new Communication(communication);

    // assertions
    assertEquals(id, copiedCommunication.getId());
    assertEquals(channelId, copiedCommunication.getChannelId());
    assertEquals(title, copiedCommunication.getTitle());
    assertEquals(status, copiedCommunication.getStatus());
    assertEquals(scheduledDateTime, copiedCommunication.getScheduledDateTime());
    assertEquals(timeZone, copiedCommunication.getTimeZone());
    assertEquals(authors, copiedCommunication.getAuthors());
  }

  private Date getScheduledTime(final Integer minutes) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.MINUTE, minutes);
    return calendar.getTime();

  }

}