/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultContentPlanCheckerTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import static org.easymock.EasyMock.createMock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.easymock.EasyMock;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.User.Role;
import com.brighttalk.service.channel.business.domain.DefaultTimestampToolbox;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.TimestampToolbox;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelContentPlan;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.error.ContentPlanException;
import com.brighttalk.service.channel.integration.database.ContentPlanDbDao;

/**
 * Unit tests for {@link ChannelContentPlan}.
 */
public class ChannelContentPlanTest {

  private static Long CHANNEL_ID = 123L;
  private static Long COMMUNICATION_ID = 456L;

  private ChannelContentPlan uut;

  private ContentPlanDbDao mockContentPlanDbDao;

  private TimestampToolbox mockTimestampToolbox;

  @Before
  public void setUp() {
    uut = new ChannelContentPlan();

    mockContentPlanDbDao = createMock(ContentPlanDbDao.class);
    ReflectionTestUtils.setField(uut, "contentPlanDbDao", mockContentPlanDbDao);

    mockTimestampToolbox = createMock(TimestampToolbox.class);
    ReflectionTestUtils.setField(uut, "timestampToolBox", mockTimestampToolbox);
  }

  @Test
  public void assertContentPlanToCreate_Syndicated_SyndicationsNotInContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    Feature feature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    feature.setIsEnabled(false);

    List<Feature> features = new ArrayList<Feature>();
    features.add(feature);

    Channel channel = new ChannelBuilder().withDefaultValues().withFeatures(features).build();

    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().withSyndicationType(
        CommunicationSyndication.SyndicationType.IN).withMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED).withConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(COMMUNICATION_ID).withChannelId(
        CHANNEL_ID).withConfiguration(communicationConfiguration).build();

    // Expectations

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions

  }

  @Test
  public void assertContentPlanToCreate_InFirstMonth_UnderContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    boolean includeSyndicatedContent = true;

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-06-06T13:00:00Z");
    Long contentPlanLimitFirstMonth = 2L;
    Long maxCommPeriodLength = 12L;
    Long communicationCount = 1L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);
    Date startPeriod = period.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = period.get(TimestampToolbox.END_PERIOD);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_FIRST_MONTH);
    maxCommFirstMonthFeature.setIsEnabled(true);
    maxCommFirstMonthFeature.setValue(contentPlanLimitFirstMonth.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(true);
    EasyMock.expect(mockTimestampToolbox.getFirstMonthPeriod(created)).andReturn(period);

    EasyMock.expect(
        mockContentPlanDbDao.countCommunicationsInPeriod(CHANNEL_ID, startPeriod, endPeriod, includeSyndicatedContent,
            null)).andReturn(communicationCount);
    EasyMock.replay(mockContentPlanDbDao, mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockContentPlanDbDao, mockTimestampToolbox);
  }

  @Test(expected = ContentPlanException.class)
  public void assertContentPlanToCreate_InFirstMonth_ReachingContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    boolean includeSyndicatedContent = true;

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-06-06T13:00:00Z");
    Long contentPlanLimitFirstMonth = 2L;
    Long maxCommPeriodLength = 12L;
    Long communicationCount = 2L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);
    Date startPeriod = period.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = period.get(TimestampToolbox.END_PERIOD);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_FIRST_MONTH);
    maxCommFirstMonthFeature.setIsEnabled(true);
    maxCommFirstMonthFeature.setValue(contentPlanLimitFirstMonth.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(true);
    EasyMock.expect(mockTimestampToolbox.getFirstMonthPeriod(created)).andReturn(period);

    EasyMock.expect(
        mockContentPlanDbDao.countCommunicationsInPeriod(CHANNEL_ID, startPeriod, endPeriod, includeSyndicatedContent,
            null)).andReturn(communicationCount);

    EasyMock.replay(mockContentPlanDbDao, mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockContentPlanDbDao, mockTimestampToolbox);
  }

  @Test(expected = ContentPlanException.class)
  public void assertContentPlanToCreate_InFirstMonth_OverContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    boolean includeSyndicatedContent = true;

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-06-06T13:00:00Z");
    Long contentPlanLimitFirstMonth = 2L;
    Long maxCommPeriodLength = 12L;
    Long communicationCount = 3L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);
    Date startPeriod = period.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = period.get(TimestampToolbox.END_PERIOD);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_FIRST_MONTH);
    maxCommFirstMonthFeature.setIsEnabled(true);
    maxCommFirstMonthFeature.setValue(contentPlanLimitFirstMonth.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(true);
    EasyMock.expect(mockTimestampToolbox.getFirstMonthPeriod(created)).andReturn(period);

    EasyMock.expect(
        mockContentPlanDbDao.countCommunicationsInPeriod(CHANNEL_ID, startPeriod, endPeriod, includeSyndicatedContent,
            null)).andReturn(communicationCount);

    EasyMock.replay(mockContentPlanDbDao, mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockContentPlanDbDao, mockTimestampToolbox);
  }

  @Test
  public void assertContentPlanToCreate_InFirstMonth_UnlimitedContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-06-06T13:00:00Z");
    Long contentPlanLimitFirstMonth = 2L;
    Long maxCommPeriodLength = 12L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_FIRST_MONTH);
    maxCommFirstMonthFeature.setIsEnabled(false);
    maxCommFirstMonthFeature.setValue(contentPlanLimitFirstMonth.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(true);
    EasyMock.expect(mockTimestampToolbox.getFirstMonthPeriod(created)).andReturn(period);

    EasyMock.replay(mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockTimestampToolbox);
  }

  @Test
  public void assertContentPlanToCreate_InSecondMonth_UnderContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    boolean includeSyndicatedContent = true;

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-07-06T13:00:00Z");
    Long maxCommPerPeriod = 2L;
    Long maxCommPeriodLength = 12L;
    Long communicationCount = 1L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);
    Date startPeriod = period.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = period.get(TimestampToolbox.END_PERIOD);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_PER_PERIOD);
    maxCommFirstMonthFeature.setIsEnabled(true);
    maxCommFirstMonthFeature.setValue(maxCommPerPeriod.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(false);
    EasyMock.expect(mockTimestampToolbox.getPeriod(created, scheduled, maxCommPeriodLength.intValue())).andReturn(
        period);

    EasyMock.expect(
        mockContentPlanDbDao.countCommunicationsInPeriod(CHANNEL_ID, startPeriod, endPeriod, includeSyndicatedContent,
            null)).andReturn(communicationCount);

    EasyMock.replay(mockContentPlanDbDao, mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockContentPlanDbDao, mockTimestampToolbox);
  }

  @Test(expected = ContentPlanException.class)
  public void assertContentPlanToCreate_InSecondMonth_ReachingContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    boolean includeSyndicatedContent = true;

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-07-06T13:00:00Z");
    Long maxCommPerPeriod = 2L;
    Long maxCommPeriodLength = 12L;
    Long communicationCount = 2L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);
    Date startPeriod = period.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = period.get(TimestampToolbox.END_PERIOD);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_PER_PERIOD);
    maxCommFirstMonthFeature.setIsEnabled(true);
    maxCommFirstMonthFeature.setValue(maxCommPerPeriod.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(false);
    EasyMock.expect(mockTimestampToolbox.getPeriod(created, scheduled, maxCommPeriodLength.intValue())).andReturn(
        period);

    EasyMock.expect(
        mockContentPlanDbDao.countCommunicationsInPeriod(CHANNEL_ID, startPeriod, endPeriod, includeSyndicatedContent,
            null)).andReturn(communicationCount);

    EasyMock.replay(mockContentPlanDbDao, mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockContentPlanDbDao, mockTimestampToolbox);
  }

  @Test(expected = ContentPlanException.class)
  public void assertContentPlanToCreate_InSecondMonth_OverContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    boolean includeSyndicatedContent = true;

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-07-06T13:00:00Z");
    Long maxCommPerPeriod = 2L;
    Long maxCommPeriodLength = 12L;
    Long communicationCount = 3L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);
    Date startPeriod = period.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = period.get(TimestampToolbox.END_PERIOD);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_PER_PERIOD);
    maxCommFirstMonthFeature.setIsEnabled(true);
    maxCommFirstMonthFeature.setValue(maxCommPerPeriod.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(false);
    EasyMock.expect(mockTimestampToolbox.getPeriod(created, scheduled, maxCommPeriodLength.intValue())).andReturn(
        period);

    EasyMock.expect(
        mockContentPlanDbDao.countCommunicationsInPeriod(CHANNEL_ID, startPeriod, endPeriod, includeSyndicatedContent,
            null)).andReturn(communicationCount);

    EasyMock.replay(mockContentPlanDbDao, mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockContentPlanDbDao, mockTimestampToolbox);
  }

  @Test
  public void assertContentPlanToCreate_InSecondMonth_UnlimitedContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-07-06T13:00:00Z");
    Long maxCommPerPeriod = 2L;
    Long maxCommPeriodLength = 12L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_PER_PERIOD);
    maxCommFirstMonthFeature.setIsEnabled(false);
    maxCommFirstMonthFeature.setValue(maxCommPerPeriod.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(false);
    EasyMock.expect(mockTimestampToolbox.getPeriod(created, scheduled, maxCommPeriodLength.intValue())).andReturn(
        period);

    EasyMock.replay(mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockTimestampToolbox);
  }

  @Test(expected = ContentPlanException.class)
  public void assertContentPlanToCreate_InPast_UnderContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    boolean includeSyndicatedContent = true;

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-06-05T13:00:00Z");
    Long maxCommPerPeriod = 2L;
    Long maxCommPeriodLength = 12L;
    Long communicationCount = 2L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);
    Date startPeriod = period.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = period.get(TimestampToolbox.END_PERIOD);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_PER_PERIOD);
    maxCommFirstMonthFeature.setIsEnabled(true);
    maxCommFirstMonthFeature.setValue(maxCommPerPeriod.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(false);
    EasyMock.expect(mockTimestampToolbox.getPeriod(created, scheduled, maxCommPeriodLength.intValue())).andReturn(
        period);

    EasyMock.expect(
        mockContentPlanDbDao.countCommunicationsInPeriod(CHANNEL_ID, startPeriod, endPeriod, includeSyndicatedContent,
            null)).andReturn(communicationCount);

    EasyMock.replay(mockContentPlanDbDao, mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockContentPlanDbDao, mockTimestampToolbox);
  }

  @Test(expected = ContentPlanException.class)
  public void assertContentPlanToCreate_InPast_OverContentPlan() {

    // Set Up
    List<Role> roles = new ArrayList<User.Role>();
    roles.add(Role.MANAGER);
    User user = new User();
    user.setRoles(roles);

    boolean includeSyndicatedContent = true;

    Date created = parseStringToDate("2011-06-06T12:00:00Z");
    Date scheduled = parseStringToDate("2011-06-05T13:00:00Z");
    Long maxCommPerPeriod = 2L;
    Long maxCommPeriodLength = 12L;
    Long communicationCount = 3L;
    HashMap<String, Date> period = getPeriod(created, scheduled, maxCommPeriodLength);
    Date startPeriod = period.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = period.get(TimestampToolbox.END_PERIOD);

    Feature syndicationIncludedInContentPlanFeature = new Feature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    syndicationIncludedInContentPlanFeature.setIsEnabled(true);

    Feature maxCommFirstMonthFeature = new Feature(Type.MAX_COMM_PER_PERIOD);
    maxCommFirstMonthFeature.setIsEnabled(true);
    maxCommFirstMonthFeature.setValue(maxCommPerPeriod.toString());

    Feature maxCommPeriodLengthFeature = new Feature(Type.MAX_COMM_PERIOD_LENGTH);
    maxCommPeriodLengthFeature.setIsEnabled(true);
    maxCommPeriodLengthFeature.setValue(maxCommPeriodLength.toString());

    List<Feature> features = new ArrayList<Feature>();
    features.add(syndicationIncludedInContentPlanFeature);
    features.add(maxCommFirstMonthFeature);
    features.add(maxCommPeriodLengthFeature);

    Channel channel = new ChannelBuilder().withDefaultValues().withId(CHANNEL_ID).withFeatures(features).withCreatedDate(
        created).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(scheduled).withId(
        COMMUNICATION_ID).withChannelId(CHANNEL_ID).build();

    // Expectations
    EasyMock.expect(mockTimestampToolbox.inFirstMonth(created, scheduled)).andReturn(false);
    EasyMock.expect(mockTimestampToolbox.getPeriod(created, scheduled, maxCommPeriodLength.intValue())).andReturn(
        period);

    EasyMock.expect(
        mockContentPlanDbDao.countCommunicationsInPeriod(CHANNEL_ID, startPeriod, endPeriod, includeSyndicatedContent,
            null)).andReturn(communicationCount);

    EasyMock.replay(mockContentPlanDbDao, mockTimestampToolbox);

    // Excercise
    uut.assertForCreate(user, channel, communication);

    // Assertions
    EasyMock.verify(mockContentPlanDbDao, mockTimestampToolbox);
  }

  private Date parseStringToDate(final String date) {
    return DateTime.parse(date).toDateTime(DateTimeZone.UTC).toDate();
  }

  private HashMap<String, Date> getPeriod(final Date created, final Date scheduled, final Long periodInMonths) {
    DefaultTimestampToolbox timestampToolbox = new DefaultTimestampToolbox();
    return timestampToolbox.getPeriod(created, scheduled, periodInMonths.intValue());
  }
}
