package com.brighttalk.service.channel.business.domain.user;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria.SortBy;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;

public class UsersSearchCriteriaTest {

  private static final Integer PAGE_NUMBER = 5;

  private static final Integer PAGE_SIZE = 10;

  private static final SortBy SORT_BY_ENUM = SortBy.LAST_LOGIN;

  private static final String SORT_BY_STRING = "lastLogin";

  private static final SortOrder SORT_ORDER_ENUM = SortOrder.DESC;

  private static final String SORT_ORDER_STRING = "desc";

  private UsersSearchCriteria uut;

  @Test
  public void testPageNumber() {

    // Set up
    uut = new UsersSearchCriteria();
    uut.setPageNumber(PAGE_NUMBER);

    // Assertions
    assertEquals(uut.getPageNumber(), PAGE_NUMBER);
  }

  @Test
  public void testPageSize() {

    // Set up
    uut = new UsersSearchCriteria();
    uut.setPageSize(PAGE_SIZE);

    // Assertions
    assertEquals(uut.getPageSize(), PAGE_SIZE);
  }

  @Test
  public void testSortByWhenString() {

    // Set up
    uut = new UsersSearchCriteria();
    uut.setSortBy(SORT_BY_STRING);

    // Assertions
    assertEquals(uut.getSortBy(), SORT_BY_ENUM);
  }

  @Test(expected = SearchCriteriaException.class)
  public void testSortByWhenInvalidString() {

    // Set up
    uut = new UsersSearchCriteria();
    uut.setSortBy("invalid");

    // Assertions
  }

  @Test
  public void testSortByWhenEnum() {

    // Set up
    uut = new UsersSearchCriteria();
    uut.setSortBy(SORT_BY_ENUM);

    // Assertions
    assertEquals(uut.getSortBy(), SORT_BY_ENUM);
  }

  @Test
  public void testSortOrderWhenString() {

    // Set up
    uut = new UsersSearchCriteria();
    uut.setSortOrder(SORT_ORDER_STRING);

    // Assertions
    assertEquals(uut.getSortOrder(), SORT_ORDER_ENUM);
  }

  @Test(expected = SearchCriteriaException.class)
  public void testSortOrderWhenInvalidString() {

    // Set up
    uut = new UsersSearchCriteria();
    uut.setSortOrder("invalid");

    // Assertions
  }

  @Test
  public void testSortOrderWhenEnum() {

    // Set up
    uut = new UsersSearchCriteria();
    uut.setSortOrder(SORT_ORDER_ENUM);

    // Assertions
    assertEquals(uut.getSortOrder(), SORT_ORDER_ENUM);
  }

}