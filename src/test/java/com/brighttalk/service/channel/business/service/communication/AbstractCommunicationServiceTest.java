/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationServiceTest.java 72595 2014-01-15 13:37:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.ArrayList;
import java.util.List;

import com.brighttalk.common.user.User;
import com.brighttalk.common.utils.JobIdGenerator;
import com.brighttalk.service.channel.business.domain.CalendarUrlGenerator;
import com.brighttalk.service.channel.business.domain.ContentPlan;
import com.brighttalk.service.channel.business.domain.Keywords;
import com.brighttalk.service.channel.business.domain.PinGenerator;
import com.brighttalk.service.channel.business.domain.communication.CommunicationPortalUrlGenerator;
import com.brighttalk.service.channel.business.queue.CommunityQueuer;
import com.brighttalk.service.channel.business.queue.EmailQueuer;
import com.brighttalk.service.channel.business.queue.ImageConverterQueuer;
import com.brighttalk.service.channel.business.queue.LesQueuer;
import com.brighttalk.service.channel.business.queue.NotificationQueuer;
import com.brighttalk.service.channel.business.queue.SummitQueuer;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.booking.HDBookingServiceDao;
import com.brighttalk.service.channel.integration.community.CommunityServiceDao;
import com.brighttalk.service.channel.integration.database.CommunicationCategoryDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationConfigurationDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.RenditionDbDao;
import com.brighttalk.service.channel.integration.database.ScheduledPublicationDbDao;
import com.brighttalk.service.channel.integration.database.syndication.SyndicationDbDao;
import com.brighttalk.service.channel.integration.email.EmailServiceDao;
import com.brighttalk.service.channel.integration.les.LiveEventServiceDao;
import com.brighttalk.service.channel.integration.summit.SummitServiceDao;

/**
 * Unit tests for {@link CommunicationCreateService}.
 */
public abstract class AbstractCommunicationServiceTest {

  protected static final Long CHANNEL_ID = 1L;

  protected static final Long COMMUNICATION_ID = 2L;

  protected static final Long USER_ID = 999L;

  protected ChannelFinder mockChannelFinder;

  protected CommunicationFinder mockCommunicationFinder;

  protected CommunicationSyndicationService mockCommunicationSyndicationService;

  protected ContentPlan mockContentPlan;

  protected CommunicationDbDao mockCommunicationDao;

  protected CommunicationConfigurationDbDao mockCommunicationConfigurationDbDao;

  protected CommunicationCategoryDbDao mockCommunicationCategoryDbDao;

  protected SyndicationDbDao mockSyndicationDbDao;

  protected RenditionDbDao mockRenditionDbDao;

  protected EmailServiceDao mockEmailServiceDao;

  protected HDBookingServiceDao mockHDBookingServiceDao;

  protected EmailQueuer mockEmailQueuer;

  protected LesQueuer mockLesQueuer;

  protected ImageConverterQueuer mockImageConverterQueuer;

  protected NotificationQueuer mockNotificationQueuer;

  protected SummitQueuer mockSummitQueuer;

  protected ScheduledPublicationDbDao mockScheduledPublicationDbDao;

  protected LiveEventServiceDao mockLiveEventServiceDao;

  protected SummitServiceDao mockSummitServiceDao;

  protected JobIdGenerator mockIdGenerator;

  protected Keywords mockKeywords;

  protected PinGenerator mockPinGenerator;

  protected CalendarUrlGenerator mockCalendarUrlGenerator;

  protected CommunicationPortalUrlGenerator mockCommunicationPortalUrlGenerator;

  protected CommunityServiceDao mockCommunityServiceDao;

  protected CommunityQueuer mockCommunityQueuer;

  protected User createUser() {

    User user = new User();
    user.setId(USER_ID);
    List<User.Role> roles = new ArrayList<User.Role>();
    roles.add(User.Role.USER);
    user.setRoles(roles);
    return user;
  }

}