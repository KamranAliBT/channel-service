/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionValidatorTest.java 91908 2015-03-18 16:46:30Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError.SubscriptionErrorCode;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.error.SubscriptionProcessException;
import com.brighttalk.service.channel.business.error.SummitNotFoundException;
import com.brighttalk.service.channel.business.filter.CommunicationFilter;
import com.brighttalk.service.channel.business.filter.SubscriptionFilter;
import com.brighttalk.service.channel.business.filter.SummitFilter;
import com.brighttalk.service.channel.integration.communication.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.database.SubscriptionContextDbDao;
import com.brighttalk.service.channel.integration.database.SubscriptionDbDao;
import com.brighttalk.service.channel.integration.summit.dto.SummitDto;
import com.brighttalk.service.utils.DomainFactoryUtils;

/**
 * Unit test for {@link SubscriptionValidator}.
 */
public class SubscriptionValidatorTest {

  private SubscriptionValidator uut;

  private Subscription subscription;
  private Channel channel;
  private SubscriptionContext context;
  private SubscriptionDbDao mockSubscriptionDbDao;
  private SubscriptionFilter mockSubscriptionFilter;
  private SubscriptionContextDbDao mockSubscriptionContextDbDao;
  private SummitFilter mockSummitFilter;
  private CommunicationFilter mockCommFilter;

  /**
   * Test setup.
   */
  @Before
  public void setup() {
    uut = new SubscriptionValidator();
    subscription = DomainFactoryUtils.createSubscription();
    channel = DomainFactoryUtils.createChannel();
    context = DomainFactoryUtils.createSubscriptionContext();

    mockSubscriptionDbDao = EasyMock.createMock(SubscriptionDbDao.class);
    ReflectionTestUtils.setField(uut, "subscriptionDbDao", mockSubscriptionDbDao);

    mockSubscriptionFilter = EasyMock.createMock(SubscriptionFilter.class);
    ReflectionTestUtils.setField(uut, "subscriptionFilter", mockSubscriptionFilter);

    mockSubscriptionContextDbDao = EasyMock.createMock(SubscriptionContextDbDao.class);
    ReflectionTestUtils.setField(uut, "subscriptionContextDbDao", mockSubscriptionContextDbDao);

    mockSummitFilter = EasyMock.createMock(SummitFilter.class);
    ReflectionTestUtils.setField(uut, "summitFilter", mockSummitFilter);

    mockCommFilter = EasyMock.createMock(CommunicationFilter.class);
    ReflectionTestUtils.setField(uut, "commFilter", mockCommFilter);
  }

  /**
   * Tests {@link SubscriptionValidator#assertBlockedUser(Subscription, Channel)} in the error case of asserting blocked
   * user.
   */
  @Test
  public void testAssertBlockedUser() {
    // Setup -
    User user = DomainFactoryUtils.createUser();
    Channel channel = DomainFactoryUtils.createChannel();
    Feature feature = new Feature(Type.BLOCKED_USER);
    feature.setIsEnabled(true);
    feature.setValue(user.getEmail());
    channel.addFeature(feature);

    try {
      uut.assertBlockedUser(subscription, channel);
    } catch (SubscriptionProcessException e) {
      assertEquals(SubscriptionErrorCode.USER_BLOCKED, e.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionValidator#assertSubscribedUser(Subscription, Channel)} in the error case of asserting user
   * already subscribed.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void testAssertSubscribedUserAlreadySubscribed() {
    // Setup -
    subscription.setActive(true);
    List<Subscription> subscriptions = Collections.singletonList(subscription);

    // Expectation -
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(subscriptions).anyTimes();
    EasyMock.expect(mockSubscriptionFilter.filterByUserId(isA(List.class), isA(Long.class))).andReturn(subscription);
    EasyMock.replay(mockSubscriptionDbDao, mockSubscriptionFilter);

    try {
      uut.assertSubscribedUser(subscription, channel);
    } catch (SubscriptionProcessException e) {
      assertEquals(SubscriptionErrorCode.ALREADY_SUBSCRIBED, e.getErrorCode());
    }
    EasyMock.verify(mockSubscriptionDbDao, mockSubscriptionFilter);
  }

  /**
   * Tests {@link SubscriptionValidator#assertSubscribedUser(Subscription, Channel)} in the error case of asserting user
   * already unsubscribed.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void testAssertSubscribedUserUnSubscribed() {
    // Setup -
    subscription.setActive(false);
    List<Subscription> subscriptions = Collections.singletonList(subscription);

    // Expectation -
    EasyMock.expect(mockSubscriptionDbDao.findByChannelId(isA(Long.class))).andReturn(subscriptions).anyTimes();
    EasyMock.expect(mockSubscriptionFilter.filterByUserId(isA(List.class), isA(Long.class))).andReturn(subscription);
    EasyMock.replay(mockSubscriptionDbDao, mockSubscriptionFilter);

    try {
      uut.assertSubscribedUser(subscription, channel);
    } catch (SubscriptionProcessException e) {
      assertEquals(SubscriptionErrorCode.UNSUBSCRIBED, e.getErrorCode());
    }
    EasyMock.verify(mockSubscriptionDbDao, mockSubscriptionFilter);
  }

  /**
   * Tests {@link SubscriptionValidator#assertSubscriptionActive} in the error case of asserting subscription is
   * inactive.
   */
  @Test
  public void testAssertSubscriptionInactive() {
    // Setup -
    subscription.setActive(false);

    try {
      uut.assertSubscriptionActive(subscription);
    } catch (SubscriptionProcessException e) {
      assertEquals(SubscriptionErrorCode.SUBSCRIPTION_INACTIVE, e.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionValidator#assertContextDoNotExist} in the error case of asserting subscription already
   * exists.
   */
  @Test
  public void testAssertContextDoNotExist() {
    // Do Test -
    try {
      uut.assertContextDoNotExist(subscription);
    } catch (SubscriptionProcessException e) {
      assertEquals(SubscriptionErrorCode.CONTEXT_ALREADY_EXIST, e.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionValidator#assertContextEngagementScore} in the error case of asserting subscription
   * context engagement score value containing invalid integer.
   */
  @Test
  public void testAssertContextEngagementScoreInvalidInteger() {
    // Setup -
    context.setEngagementScore("test");
    subscription.setContext(context);

    try {
      uut.assertContextEngagementScore(subscription);
    } catch (SubscriptionProcessException e) {
      assertEquals(SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE, e.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionValidator#assertContextEngagementScore} in the error case of asserting subscription
   * context engagement score containing a value outside expected range of 0-100.
   */
  @Test
  public void testAssertContextEngagementScoreOutOfRange() {
    // Setup -
    context.setEngagementScore("101");
    subscription.setContext(context);

    try {
      uut.assertContextEngagementScore(subscription);
    } catch (SubscriptionProcessException e) {
      assertEquals(SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE, e.getErrorCode());
    }
  }

  /**
   * Tests {@link SubscriptionValidator#assertLeadContextValue} in the error case of asserting subscription context lead
   * context value containing id of a summit that doesn't identify an existing summit.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void testAssertLeadContextValueSummitNotFound() {
    // Setup -
    context.setLeadType(SubscriptionLeadType.SUMMIT);
    subscription.setContext(context);

    // Expectation -
    EasyMock.expect(mockSummitFilter.filterBySummitId(isA(List.class), isA(SummitDto.class))).andThrow(
        new SummitNotFoundException(""));
    EasyMock.replay(mockSummitFilter);

    try {
      uut.assertLeadContextValue(subscription, new ArrayList<SummitDto>(), new ArrayList<CommunicationDto>());
    } catch (SubscriptionProcessException e) {
      assertEquals(SubscriptionErrorCode.SUMMIT_NOT_FOUND, e.getErrorCode());
    }
    EasyMock.verify(mockSummitFilter);
  }

  /**
   * Tests {@link SubscriptionValidator#assertLeadContextValue} in the error case of asserting subscription context lead
   * context value containing id of a communication that doesn't identify an existing communication.
   */
  @Test
  @SuppressWarnings("unchecked")
  public void testAssertLeadContextValueCommunicationNotFound() {
    // Setup -
    context.setLeadType(SubscriptionLeadType.CONTENT);
    subscription.setContext(context);

    // Expectation -
    EasyMock.expect(mockCommFilter.filterByCommunicationId(isA(List.class), isA(CommunicationDto.class))).andThrow(
        new CommunicationNotFoundException(""));
    EasyMock.replay(mockCommFilter);

    try {
      uut.assertLeadContextValue(subscription, new ArrayList<SummitDto>(), new ArrayList<CommunicationDto>());
    } catch (SubscriptionProcessException e) {
      assertEquals(SubscriptionErrorCode.COMMUNICATION_NOT_FOUND, e.getErrorCode());
    }
    EasyMock.verify(mockCommFilter);
  }
}
