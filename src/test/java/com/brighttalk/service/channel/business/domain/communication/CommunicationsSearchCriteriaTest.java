/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: CommunicationsSearchCriteriaTest.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;

/**
 * Juni test for {@link CommunicationsSearchCriteria}.
 */
public class CommunicationsSearchCriteriaTest {

  @Test
  public void isExpandedWhenExpandIsNull() {

    // setup
    String ids = null;
    String expand = null;

    CommunicationsSearchCriteria uut = new CommunicationsSearchCriteria();
    uut.setExpand(expand);
    uut.setIds(ids);

    // setup
    boolean result = uut.includeCategories();

    // do assert
    assertFalse(result);

  }

  @Test
  public void isExpandedWhenExpandIsEmptyString() {

    // setup
    String ids = null;
    String expand = "";

    CommunicationsSearchCriteria uut = new CommunicationsSearchCriteria();
    uut.setExpand(expand);
    uut.setIds(ids);

    // setup
    boolean result = uut.includeCategories();

    // do assert
    assertFalse(result);

  }

  @Test
  public void isExpandedWhenExpandIsRandomString() {

    // setup
    String ids = null;
    String expand = "testststsst";

    CommunicationsSearchCriteria uut = new CommunicationsSearchCriteria();
    uut.setExpand(expand);
    uut.setIds(ids);

    // setup
    boolean result = uut.includeCategories();

    // do assert
    assertFalse(result);

  }

  @Test
  public void isExpandedWhenExpandIsValid() {

    // setup
    String ids = null;
    String expand = "categories";

    CommunicationsSearchCriteria uut = new CommunicationsSearchCriteria();
    uut.setExpand(expand);
    uut.setIds(ids);

    // setup
    boolean result = uut.includeCategories();

    // do assert
    assertTrue(result);

  }

  @Test
  public void getChanelAndCommunicationsIdsWhenIdsContainsEmptyString() {

    // setup
    String ids = "";

    CommunicationsSearchCriteria uut = new CommunicationsSearchCriteria();
    uut.setIds(ids);

    // setup
    List<String> result = uut.getChanelAndCommunicationsIds();

    // do assert
    assertEquals(0, result.size());

  }

  @Test
  public void getChanelAndCommunicationsIdsWhenIdsContainsSingleNumber() {

    // setup
    String ids = "10-11";

    CommunicationsSearchCriteria uut = new CommunicationsSearchCriteria();
    uut.setIds(ids);

    // setup
    List<String> result = uut.getChanelAndCommunicationsIds();

    // do assert
    assertEquals(1, result.size());
    assertEquals(ids, result.get(0));

  }

  @Test
  public void getChanelAndCommunicationsIdsWhenIdsContainsTwoNumbers() {

    // setup
    String ids = "10-1,20-2";
    String expand = "expand";

    CommunicationsSearchCriteria uut = new CommunicationsSearchCriteria();
    uut.setExpand(expand);
    uut.setIds(ids);

    // setup
    List<String> result = uut.getChanelAndCommunicationsIds();

    // do assert
    assertEquals(2, result.size());
    assertEquals("10-1", result.get(0));
    assertEquals("20-2", result.get(1));

  }

  @Test
  public void getChanelAndCommunicationsIdsWhenIdsContainsTwoExactSameNumbers() {

    // setup
    String ids = "10-1,10-1";
    String expand = "expand";

    CommunicationsSearchCriteria uut = new CommunicationsSearchCriteria();
    uut.setExpand(expand);
    uut.setIds(ids);

    // setup
    List<String> result = uut.getChanelAndCommunicationsIds();

    // do assert
    assertEquals(1, result.size());
    assertEquals("10-1", result.get(0));

  }

  @Test
  public void getChanelAndCommunicationsIdsWhenIdsContainsTenNumbers() {

    // setup
    String ids = "10-1,20-2,31-3,32-4,33-5,34-6,35-7,44-8,55-9,66-10";
    String expand = "expand";

    CommunicationsSearchCriteria uut = new CommunicationsSearchCriteria();
    uut.setExpand(expand);
    uut.setIds(ids);

    // setup
    List<String> result = uut.getChanelAndCommunicationsIds();

    // do assert
    assertEquals(10, result.size());

  }

}
