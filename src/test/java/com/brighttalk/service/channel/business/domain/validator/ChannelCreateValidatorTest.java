/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelCreateValidatorTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.error.InvalidChannelException;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;

public class ChannelCreateValidatorTest {

  private ChannelCreateValidator uut;

  private ChannelDbDao mockChannelDbDao;

  @Before
  public void setUp() {

    mockChannelDbDao = EasyMock.createMock(ChannelDbDao.class);
    uut = new ChannelCreateValidator(mockChannelDbDao);
  }

  @Test
  public void success() {

    Channel channel = new Channel();
    
    // do test
    try {

      uut.validate(channel);

    } catch (InvalidChannelException invalidChannelException) {

      fail();
    }
  }

  @Test
  public void assertNoChannelId() {

    Channel channel = new Channel();
    channel.setId(666L);

    // do test
    try {

      uut.validate(channel);
      fail();

    } catch (InvalidChannelException invalidChannelException) {

      final String expectedErrorMessage = "[" + ChannelCreateValidator.ERRORCODE_REDUNDANT_CHANNEL_ID_PROVIDED + "]";
      assertEquals(expectedErrorMessage, invalidChannelException.getUserErrorMessage());

      assertEquals(InvalidChannelException.ERROR_CODE_INVALID_CHANNEL, invalidChannelException.getUserErrorCode());
    }
  }

  @Test
  public void assertNoDuplicateTitle() {

    Channel channel = new Channel();
    final String channelTitle = "channelTitle";
    channel.setTitle(channelTitle);

    EasyMock.expect(mockChannelDbDao.titleInUse(channelTitle)).andReturn(false);
    EasyMock.replay(mockChannelDbDao);

    // do test
    try {

      uut.validate(channel);

    } catch (InvalidChannelException invalidChannelException) {

      fail();
    }
  }

  @Test
  public void assertDuplicateTitle() {

    Channel channel = new Channel();
    final String channelTitle = "channelTitle";
    channel.setTitle(channelTitle);

    EasyMock.expect(mockChannelDbDao.titleInUse(channelTitle)).andReturn(true);
    EasyMock.replay(mockChannelDbDao);

    // do test
    try {

      uut.validate(channel);
      fail();

    } catch (InvalidChannelException invalidChannelException) {

      final String expectedErrorMessage = "[" + ChannelCreateValidator.ERRORCODE_DUPLICATE_CHANNEL_TITLE + "]";
      assertEquals(expectedErrorMessage, invalidChannelException.getUserErrorMessage());

      assertEquals(InvalidChannelException.ERROR_CODE_INVALID_CHANNEL, invalidChannelException.getUserErrorCode());
    }
  }
}