/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationRegistrationServiceTest.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedListWithTotalPages;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria;
import com.brighttalk.service.channel.business.service.communication.DefaultCommunicationRegistrationService;
import com.brighttalk.service.channel.integration.database.CommunicationRegistrationsDbDao;

public class DefaultCommunicationRegistrationServiceTest {

  private DefaultCommunicationRegistrationService uut;

  private CommunicationRegistrationsDbDao mockCommunicationRegistrationsDbDao;

  @Before
  public void setUp() {

    uut = new DefaultCommunicationRegistrationService();

    mockCommunicationRegistrationsDbDao = EasyMock.createMock(CommunicationRegistrationsDbDao.class);
    uut.setCommunicationRegistrationsDbDao(mockCommunicationRegistrationsDbDao);
  }

  @Test
  public void getWhenNoRegistrationsFound() {

    final Long userId = 13L;
    User user = new User();
    user.setId(userId);

    CommunicationRegistrationSearchCriteria searchCriteria = new CommunicationRegistrationSearchCriteria();

    EasyMock.expect(mockCommunicationRegistrationsDbDao.findTotalRegistrations(userId, searchCriteria)).andReturn(0);
    EasyMock.replay(mockCommunicationRegistrationsDbDao);

    // do test
    PaginatedListWithTotalPages<CommunicationRegistration> result = uut.findRegistrations(user, searchCriteria);

    assertNotNull(result);
  }

  @Test
  public void getWithRegistrations() {

    final Long userId = 13L;
    User user = new User();
    user.setId(userId);

    final int totalNumberOfRegistrations = 20;
    final int pageSize = 5;
    final int pageNumber = 3;
    final CommunicationRegistrationSearchCriteria searchCriteria = new CommunicationRegistrationSearchCriteria();
    final List<CommunicationRegistration> registrations = new ArrayList<CommunicationRegistration>();

    uut.setDefaultPageNumber(pageNumber);
    uut.setDefaultPageSize(pageSize);
    uut.setMaxPageSize(100);

    EasyMock.expect(mockCommunicationRegistrationsDbDao.findTotalRegistrations(userId, searchCriteria)).andReturn(
        totalNumberOfRegistrations);
    EasyMock.expect(mockCommunicationRegistrationsDbDao.findRegistrations(userId, searchCriteria)).andReturn(
        registrations);
    EasyMock.replay(mockCommunicationRegistrationsDbDao);

    // do test
    PaginatedListWithTotalPages<CommunicationRegistration> result = uut.findRegistrations(user, searchCriteria);

    assertNotNull(result);

    assertEquals(registrations.size(), result.size());
    assertEquals(pageSize, result.getPageSize());
    assertEquals(pageNumber, result.getCurrentPageNumber());
    assertEquals(totalNumberOfRegistrations, result.getUnpaginatedTotal());
  }

  @Test(expected = ApplicationException.class)
  public void getTooBigPageNumber() {

    final Long userId = 13L;
    User user = new User();
    user.setId(userId);

    final int totalNumberOfRegistrations = 20;
    final int pageSize = 5;
    final int pageNumber = 5;
    final CommunicationRegistrationSearchCriteria searchCriteria = new CommunicationRegistrationSearchCriteria();

    uut.setDefaultPageNumber(pageNumber);
    uut.setDefaultPageSize(pageSize);
    uut.setMaxPageSize(100);

    EasyMock.expect(mockCommunicationRegistrationsDbDao.findTotalRegistrations(userId, searchCriteria)).andReturn(
        totalNumberOfRegistrations);
    EasyMock.replay(mockCommunicationRegistrationsDbDao);

    // do test
    uut.findRegistrations(user, searchCriteria);
  }

  @Test(expected = ApplicationException.class)
  public void getTooSmallPageNumber() {
    
    final Long userId = 13L;
    User user = new User();
    user.setId(userId);
    
    final int totalNumberOfRegistrations = 20;
    final int pageSize = 5;
    final int pageNumber = 0;
    final CommunicationRegistrationSearchCriteria searchCriteria = new CommunicationRegistrationSearchCriteria();
    
    uut.setDefaultPageNumber(pageNumber);
    uut.setDefaultPageSize(pageSize);
    uut.setMaxPageSize(100);
    
    EasyMock.expect(mockCommunicationRegistrationsDbDao.findTotalRegistrations(userId, searchCriteria)).andReturn(
        totalNumberOfRegistrations);
    EasyMock.replay(mockCommunicationRegistrationsDbDao);
    
    // do test
    uut.findRegistrations(user, searchCriteria);
  }
}