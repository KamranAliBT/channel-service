/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelUpdateValidatorTest.java 91285 2015-03-09 13:05:18Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.error.InvalidChannelException;
import com.brighttalk.service.channel.business.error.UserNotFoundException;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.user.UserServiceDao;

public class ChannelUpdateValidatorTest {

  private ChannelUpdateValidator uut;

  private ChannelDbDao mockChannelDbDao;

  private UserServiceDao mockUserServiceDao;

  @Before
  public void setUp() {
    uut = new ChannelUpdateValidator();

    mockChannelDbDao = EasyMock.createMock(ChannelDbDao.class);
    ReflectionTestUtils.setField(uut, "channelDbDao", mockChannelDbDao);

    mockUserServiceDao = EasyMock.createMock(UserServiceDao.class);
    ReflectionTestUtils.setField(uut, "userServiceDao", mockUserServiceDao);
  }

  @Test(expected = InvalidChannelException.class)
  public void validateWhenDuplicatedTitle() {
    // Set up
    String channelTitle = "ChannelTitle";
    Long channelId = 5001L;

    Channel channel = new Channel();
    channel.setId(channelId);
    channel.setTitle(channelTitle);
    channel.setTitleChanged(true);

    // Expectations
    EasyMock.expect(mockChannelDbDao.titleInUseExcludingChannel(channelTitle, channelId)).andReturn(true);
    EasyMock.replay(mockChannelDbDao);

    // Do test
    uut.validate(channel);
  }

  @Test
  public void validateWithUnchangedTitle() {
    // Set up
    String channelTitle = "ChannelTitle";
    Long channelId = 5001L;

    Channel channel = new Channel();
    channel.setId(channelId);
    channel.setTitle(channelTitle);
    channel.setTitleChanged(false);

    // Expectations
    EasyMock.expect(mockChannelDbDao.titleInUseExcludingChannel(channelTitle, channelId)).andReturn(true);
    EasyMock.replay(mockChannelDbDao);

    // Do test
    uut.validate(channel);
  }

  @Test(expected = UserNotFoundException.class)
  public void validateWhenUserNotFound() {
    // Set up
    String channelTitle = "ChannelTitle";
    Long channelId = 5001L;
    Long userId = 1001L;

    Channel channel = new Channel();
    channel.setId(channelId);
    channel.setTitle(channelTitle);
    channel.setOwnerUserId(userId);

    // Expectations
    EasyMock.expect(mockChannelDbDao.titleInUseExcludingChannel(channelTitle, channelId)).andReturn(false);
    EasyMock.expect(mockUserServiceDao.find(userId)).andThrow(new UserNotFoundException(userId));
    EasyMock.replay(mockUserServiceDao, mockChannelDbDao);

    // Do test
    uut.validate(channel);
  }

  @Test
  public void validateWhenSuccess() {
    // Set up
    String channelTitle = "ChannelTitle";
    Long channelId = 5001L;
    Long userId = 1001L;

    Channel channel = new Channel();
    channel.setId(channelId);
    channel.setTitle(channelTitle);
    channel.setOwnerUserId(userId);

    // Expectations
    EasyMock.expect(mockChannelDbDao.titleInUseExcludingChannel(channelTitle, channelId)).andReturn(false);
    EasyMock.expect(mockUserServiceDao.find(userId)).andReturn(new User());
    EasyMock.replay(mockUserServiceDao, mockChannelDbDao);

    // Do test
    uut.validate(channel);
  }

}