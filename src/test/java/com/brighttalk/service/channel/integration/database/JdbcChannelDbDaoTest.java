/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcChannelDbDaoTest.java 96319 2015-06-09 12:25:26Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.common.user.Session;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * Tests {link JdbcChannelDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcChannelDbDaoTest extends AbstractJdbcDbDaoTest {

  private static final String KEYWORDS_SEPARATOR = ",";

  @Autowired
  private JdbcChannelDbDao uut;

  @Test
  public void testFindSubscribedChannelsForUserWithOneChannel() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    addChannelSubscription(channelId, CHANNEL_USER_ID, true);
    // expectations

    // do test
    List<Channel> result = uut.findSubscribedChannels(CHANNEL_USER_ID);

    // assertions
    assertEquals(1, result.size());
    assertEquals(channelId, result.get(0).getId());
    assertEquals(CHANNEL_TITLE, result.get(0).getTitle());
    assertEquals(CHANNEL_ORGANISATION, result.get(0).getOrganisation());
    assertEquals(CHANNEL_KEYWORDS_LIST, result.get(0).getKeywords());
    assertEquals(CHANNEL_STRAPLINE, result.get(0).getStrapline());
  }

  @Test
  public void testFindSubscribedChannelsForUserWithInactiveChannel() {

    // setup
    Long channelId = addTemporaryInactiveChannelWithTypes();
    addChannelSubscription(channelId, CHANNEL_USER_ID, true);
    // expectations

    // do test
    List<Channel> result = uut.findSubscribedChannels(CHANNEL_USER_ID);

    // assertions
    assertEquals(0, result.size());
  }

  @Test
  public void testFindSubscribedChannelsForUserWithInactiveSubscribtion() {

    // setup
    Long channelId = addTemporaryChannelWithTypes();
    addChannelSubscription(channelId, CHANNEL_USER_ID, false);
    // expectations

    // do test
    List<Channel> result = uut.findSubscribedChannels(CHANNEL_USER_ID);

    // assertions
    assertEquals(0, result.size());
  }

  @Test
  public void testFindSubscribedChannelsForNotExistingUser() {

    // do test
    List<Channel> result = uut.findSubscribedChannels(0L);

    // assertions
    assertEquals(0, result.size());
  }

  /**
   * Test find channel with all basic fields. Successful scenario.
   */
  @Test
  public void testFindChannelAssertBasicFields() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();

    // do test
    Channel result = uut.find(channelId);

    // assertions
    assertNotNull(result);
    assertEquals(channelId, result.getId());
    assertEquals(CHANNEL_DESCRIPTION, result.getDescription());
    assertEquals(CHANNEL_TITLE, result.getTitle());
    assertEquals(CHANNEL_ORGANISATION, result.getOrganisation());
    assertEquals(CHANNEL_KEYWORDS_LIST, result.getKeywords());
    assertEquals(CHANNEL_STRAPLINE, result.getStrapline());
    assertEquals(CHANNEL_VIEWED_FOR, result.getStatistics().getViewedFor());
    assertEquals(2.0F, result.getStatistics().getRating(), 0);

    assertTrue(result.isActive());
    assertFalse(result.isPromotedOnHomepage());
    assertTrue(result.isIncluded());

  }

  /**
   * Test find channel with channel type, created channel type and pending channel type. Successful scenario.
   */
  @Test
  public void testFindChannelAssertChannelsTypeFields() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();

    // do test
    Channel result = uut.find(channelId);

    // assertions
    assertNotNull(result);
    assertEquals(channelId, result.getId());
    assertEquals(CUSTOM_CHANNEL_TYPE_TEST, result.getType().getName());
    assertEquals(CUSTOM_CHANNEL_TYPE_TEST, result.getType().getEmailGroup());
    assertTrue(result.getType().isEnabled());
    assertFalse(result.getType().isDefault());

    assertEquals(DEFAULT_CHANNEL_TYPE, result.getCreatedType().getName());
    assertNull(result.getCreatedType().getEmailGroup());
    assertTrue(result.getCreatedType().isEnabled());
    assertTrue(result.getCreatedType().isDefault());

    assertEquals(ENTERPRISE_CHANNEL_TYPE_TEST, result.getPendingType().getName());
    assertEquals(ENTERPRISE_CHANNEL_TYPE_TEST, result.getPendingType().getEmailGroup());
    assertTrue(result.getPendingType().isEnabled());
    assertFalse(result.getPendingType().isDefault());

  }

  /**
   * Test find channel with pending channel type with not existing channel type.
   */
  @Test
  public void testFindChannelWithNotExistingPendingChannelType() {
    // setup
    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    // do test
    Channel result = uut.find(channelId);

    // assertions

    assertNull(result.getPendingType());

  }

  /**
   * Test find channel with empty pending channel type. Bad case scenario.
   */
  @Test
  public void testFindChannelWithEmptyPendingChannelType() {
    // setup
    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, null, true);

    // do test
    Channel result = uut.find(channelId);

    // assertions

    assertNull(result.getPendingType());

  }

  /**
   * Test find channel with statistics values. Successful scenario.
   */
  @Test
  public void testLoadStatistics() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    final long upcoming = 14;
    final long recorded = 35;
    final long live = 1;
    final long subscribers = 130;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);
    Channel channel = new Channel(channelId);

    // do test
    uut.loadStatistics(channel);

    // assertions
    assertEquals(upcoming, channel.getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(live, channel.getStatistics().getNumberOfLiveCommunications());
    assertEquals(recorded, channel.getStatistics().getNumberOfRecordedCommunications());
    assertEquals(subscribers, channel.getStatistics().getNumberOfSubscribers());
  }

  /**
   * Test find channel with statistics values not found. Bad case scenario.
   */
  @Test
  public void testLoadStatisticsWhenNoStatisticsAreFound() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    Channel channel = new Channel(channelId);

    // do test
    uut.loadStatistics(channel);

    // assertions
    assertFalse(channel.hasStatistics());
  }

  /**
   * Test find channel with statistics values. Successful scenario.
   */
  @Test
  public void testLoadStatisticsMultipleChannels() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    final long upcoming = 14;
    final long recorded = 35;
    final long live = 1;
    final long subscribers = 130;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);
    Channel channel = new Channel(channelId);

    // do test
    uut.loadStatistics(Arrays.asList(channel));

    // assertions
    assertEquals(upcoming, channel.getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(live, channel.getStatistics().getNumberOfLiveCommunications());
    assertEquals(recorded, channel.getStatistics().getNumberOfRecordedCommunications());
    assertEquals(subscribers, channel.getStatistics().getNumberOfSubscribers());
  }

  /**
   * Test find channel with statistics values not found. Bad case scenario.
   */
  @Test
  public void testLoadStatisticsMultipleChannelsWhenNoStatisticsAreFound() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    Channel channel = new Channel(channelId);

    // do test
    uut.loadStatistics(Arrays.asList(channel));

    // assertions
    assertFalse(channel.hasStatistics());
  }

  /**
   * Test load channel survey with not existing survey.
   */
  @Test
  public void testLoadSurveyWithNotExistingSurvey() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    Channel channel = new Channel(channelId);

    // do test
    uut.loadSurvey(channel);

    // assertions
    assertNull(channel.getSurvey());
  }

  /**
   * Test load channel survey with existing survey.
   */
  @Test
  public void testLoadSurveyWithExistingActiveSurvey() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    addTemporarySurveyAsset(channelId, SURVEY_ID, true);

    Channel channel = (new Channel(channelId));
    // do test
    uut.loadSurvey(channel);

    // assertions
    assertNotNull(channel.getSurvey());
    assertEquals(channelId, channel.getSurvey().getChannelId());
    assertEquals(SURVEY_ID, channel.getSurvey().getId());
    assertTrue(channel.getSurvey().isActive());
  }

  /**
   * Test load channel survey with existing not active survey.
   */
  @Test
  public void testLoadSurveyWithExistingNotActiveSurvey() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    addTemporarySurveyAsset(channelId, SURVEY_ID, false);

    Channel channel = (new Channel(channelId));
    // do test
    uut.loadSurvey(channel);

    // assertions
    assertNotNull(channel.getSurvey());
    assertEquals(channelId, channel.getSurvey().getChannelId());
    assertEquals(SURVEY_ID, channel.getSurvey().getId());
    assertFalse(channel.getSurvey().isActive());
  }

  /**
   * Test load channel survey with not existing survey.
   */
  @Test
  public void testLoadSurveyMultipleChannelsWithNotExistingSurvey() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    Channel channel = new Channel(channelId);
    List<Channel> channels = Arrays.asList(channel);

    // do test
    uut.loadSurvey(channels);

    // assertions
    assertNull(channel.getSurvey());
  }

  /**
   * Test load channel survey with existing survey.
   */
  @Test
  public void testLoadSurveyMultipleChannelsWithExistingActiveSurvey() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    addTemporarySurveyAsset(channelId, SURVEY_ID, true);

    Channel channel = (new Channel(channelId));
    List<Channel> channels = Arrays.asList(channel);

    // do test
    uut.loadSurvey(channels);

    // assertions
    assertNotNull(channel.getSurvey());
    assertEquals(channelId, channel.getSurvey().getChannelId());
    assertEquals(SURVEY_ID, channel.getSurvey().getId());
    assertTrue(channel.getSurvey().isActive());
  }

  /**
   * Test load channel survey with existing not active survey.
   */
  @Test
  public void testLoadSurveyMultipleChannelsWithExistingNotActiveSurvey() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    addTemporarySurveyAsset(channelId, SURVEY_ID, false);

    Channel channel = (new Channel(channelId));
    List<Channel> channels = Arrays.asList(channel);

    // do test
    uut.loadSurvey(channels);

    // assertions
    assertNotNull(channel.getSurvey());
    assertEquals(channelId, channel.getSurvey().getChannelId());
    assertEquals(SURVEY_ID, channel.getSurvey().getId());
    assertFalse(channel.getSurvey().isActive());
  }

  @Test(expected = NotFoundException.class)
  public void testFindChannelNotExistingChannel() {

    // do test
    uut.find(0L);

    // assertions
  }

  @Test
  public void findChannelsNotExistingChannel() {

    // setup
    Long channelId = 0L;
    final int expectedPageSize = 1;
    final int expectedPageNumber = 1;

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(channelId.toString());
    searchCriteria.setPageNumber(expectedPageNumber);
    searchCriteria.setPageSize(expectedPageSize);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    // assertions
    assertTrue(result.isEmpty());

  }

  @Test
  public void findChannelsAssertBasicFields() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();

    final int expectedPageSize = 1;
    final int expectedPageNumber = 1;

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(channelId.toString());
    searchCriteria.setPageNumber(expectedPageNumber);
    searchCriteria.setPageSize(expectedPageSize);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    // assertions
    assertNotNull(result);

    Channel resultChannel = result.get(0);
    assertEquals(channelId, resultChannel.getId());
    assertEquals(CHANNEL_DESCRIPTION, resultChannel.getDescription());
    assertEquals(CHANNEL_TITLE, resultChannel.getTitle());
    assertEquals(CHANNEL_ORGANISATION, resultChannel.getOrganisation());
    assertEquals(CHANNEL_KEYWORDS_LIST, resultChannel.getKeywords());
    assertEquals(CHANNEL_STRAPLINE, resultChannel.getStrapline());
    assertEquals(CHANNEL_VIEWED_FOR, resultChannel.getStatistics().getViewedFor());
    assertEquals(2.0F, resultChannel.getStatistics().getRating(), 0);

    assertTrue(resultChannel.isActive());
    assertFalse(resultChannel.isPromotedOnHomepage());
    assertTrue(resultChannel.isIncluded());

  }

  @Test
  public void findChannelsWithPageOneSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(channelId1.toString() + "," + channelId2.toString());
    searchCriteria.setPageNumber(1);
    searchCriteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    assertEquals(1, result.size());
    assertEquals(channelId1, result.get(0).getId());
  }

  @Test
  public void findChannelsWithPageOneSizeOneHasMore() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(channelId1.toString() + "," + channelId2.toString());
    searchCriteria.setPageNumber(1);
    searchCriteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    assertEquals(1, result.size());
    assertTrue(result.hasNextPage());
    assertFalse(result.hasPreviousPage());
  }

  @Test
  public void findChannelsWithPageTwoSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(channelId1.toString() + "," + channelId2.toString());
    searchCriteria.setPageNumber(2);
    searchCriteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    assertEquals(1, result.size());
    assertEquals(channelId2, result.get(0).getId());
  }

  @Test
  public void findChannelsWithPageTwoSizeOneAndHasPreviousPage() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    ChannelsSearchCriteria searchCriteria = new ChannelsSearchCriteria();
    searchCriteria.setIds(channelId1.toString() + "," + channelId2.toString());
    searchCriteria.setPageNumber(2);
    searchCriteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.find(searchCriteria);

    assertEquals(1, result.size());
    assertTrue(result.hasPreviousPage());
  }

  @Test
  public void findChannelsByIdsNotExistingChannel() {

    // setup
    Long channelId = 0L;
    List<Long> channelIds = Arrays.asList(channelId);

    // do test
    List<Channel> result = uut.find(channelIds);

    // assertions
    assertTrue(result.isEmpty());

  }

  @Test
  public void findChannelsByIdsAssertBasicFields() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();

    List<Long> channelIds = Arrays.asList(channelId);
    // do test
    List<Channel> result = uut.find(channelIds);

    // assertions
    assertNotNull(result);

    Channel resultChannel = result.get(0);
    assertEquals(channelId, resultChannel.getId());
    assertEquals(CHANNEL_DESCRIPTION, resultChannel.getDescription());
    assertEquals(CHANNEL_TITLE, resultChannel.getTitle());
    assertEquals(CHANNEL_ORGANISATION, resultChannel.getOrganisation());
    assertEquals(CHANNEL_KEYWORDS_LIST, resultChannel.getKeywords());
    assertEquals(CHANNEL_STRAPLINE, resultChannel.getStrapline());
    assertEquals(CHANNEL_VIEWED_FOR, resultChannel.getStatistics().getViewedFor());
    assertEquals(2.0F, resultChannel.getStatistics().getRating(), 0);

    assertTrue(resultChannel.isActive());
    assertFalse(resultChannel.isPromotedOnHomepage());
    assertTrue(resultChannel.isIncluded());

  }

  @Test
  public void findChannelsbyIdsWithPageTwoSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    List<Long> channelIds = Arrays.asList(channelId1, channelId2);

    // do test
    List<Channel> result = uut.find(channelIds);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(channelId2, result.get(1).getId());
  }

  @Test
  public void testIsPresenterNoSessions() {
    // Set up
    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    String sessionId = "abcde";

    Channel channel = new Channel(channelId);

    User user = new User();
    user.setSession(new Session(sessionId));

    // Expectations
    Boolean expectedResult = false;

    // Do Test
    Boolean result = uut.isPresenter(user, channel);

    // Assertions
    assertEquals(expectedResult, result);
  }

  @Test
  public void testIsPresenterOneSession() {
    // Set up
    String sessionId = "abcde";

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);
    addPresenterSession(communicationId, sessionId);

    Channel channel = new Channel(channelId);

    User user = new User();
    user.setSession(new Session(sessionId));

    // Expectations
    Boolean expectedResult = true;

    // Do Test
    Boolean result = uut.isPresenter(user, channel);

    // Assertions
    assertEquals(expectedResult, result);
  }

  @Test
  public void testIsPresenterTwoSessions() {
    // Set up
    String sessionId = "abcde";

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId1 = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);
    addPresenterSession(communicationId1, sessionId);

    Long communicationId2 = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);
    addPresenterSession(communicationId2, sessionId);

    Channel channel = new Channel(channelId);

    User user = new User();
    user.setSession(new Session(sessionId));

    // Expectations
    Boolean expectedResult = true;

    // Do Test
    Boolean result = uut.isPresenter(user, channel);

    // Assertions
    assertEquals(expectedResult, result);
  }

  @Test
  public void create() {

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long customChannelTypeId = addTemporaryChannelType(CUSTOM_CHANNEL_TYPE_TEST, false, CUSTOM_CHANNEL_TYPE_TEST);
    Long enterpriseChannelTypeId = addTemporaryChannelType(ENTERPRISE_CHANNEL_TYPE_TEST, false,
        ENTERPRISE_CHANNEL_TYPE_TEST);

    Channel channel = new Channel();
    channel.setTitle(CHANNEL_TITLE);
    channel.setDescription(CHANNEL_DESCRIPTION);
    channel.setKeywords(CHANNEL_KEYWORDS_LIST);
    channel.setOrganisation(CHANNEL_ORGANISATION);
    channel.setStrapline(CHANNEL_STRAPLINE);

    ChannelType type = new ChannelType(defaultChannelTypeId);
    channel.setType(type);

    ChannelType createdType = new ChannelType(customChannelTypeId);
    channel.setCreatedType(createdType);

    ChannelType pendingType = new ChannelType(enterpriseChannelTypeId);
    channel.setPendingType(pendingType);

    Long channelOwnerId = 13l;
    channel.setOwnerUserId(channelOwnerId);

    // do test
    Channel result = uut.create(channel);

    assertNotNull(result);
    assertNotNull(result.getId());
    temporaryChannelIds.add(result.getId());

    assertEquals(CHANNEL_DESCRIPTION, result.getDescription());
    assertEquals(CHANNEL_TITLE, result.getTitle());
    assertEquals(CHANNEL_ORGANISATION, result.getOrganisation());
    assertEquals(CHANNEL_KEYWORDS_LIST, result.getKeywords());
    assertEquals(CHANNEL_STRAPLINE, result.getStrapline());
    assertEquals(0, result.getStatistics().getViewedFor());
    assertEquals(0.0F, result.getStatistics().getRating(), 0);
    assertEquals(channelOwnerId, result.getOwnerUserId());

    assertTrue(result.isActive());
    assertFalse(result.isPromotedOnHomepage());
    assertTrue(result.isRuleBased());

    assertEquals(DEFAULT_CHANNEL_TYPE, result.getType().getName());
    assertNull(result.getType().getEmailGroup());
    assertTrue(result.getType().isEnabled());
    assertTrue(result.getType().isDefault());

    assertEquals(CUSTOM_CHANNEL_TYPE_TEST, result.getCreatedType().getName());
    assertEquals(CUSTOM_CHANNEL_TYPE_TEST, result.getCreatedType().getEmailGroup());
    assertTrue(result.getCreatedType().isEnabled());
    assertFalse(result.getCreatedType().isDefault());

    assertEquals(ENTERPRISE_CHANNEL_TYPE_TEST, result.getPendingType().getName());
    assertEquals(ENTERPRISE_CHANNEL_TYPE_TEST, result.getPendingType().getEmailGroup());
    assertTrue(result.getPendingType().isEnabled());
    assertFalse(result.getPendingType().isDefault());
  }

  @Test
  public void update() {

    // Set up
    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    final String expectedTitle = "expectedTitle";
    final String expectedDescription = "expectedDescription";
    final String expectedKeywords = "expectedKeywords";
    final String expectedOrganisation = "expectedOrganisation";
    final String expectedStrapline = "expectedStrapline";
    final Long expectedOwnerUserId = 1001L;

    Channel updateChannel = new Channel(channelId);
    updateChannel.setTitle(expectedTitle);
    updateChannel.setDescription(expectedDescription);
    updateChannel.setKeywords(Arrays.asList(expectedKeywords));
    updateChannel.setOrganisation(expectedOrganisation);
    updateChannel.setStrapline(expectedStrapline);
    updateChannel.setOwnerUserId(expectedOwnerUserId);

    // Do test
    uut.update(updateChannel);

    // Assertions
    Map<String, Object> updatedChannelData = jdbcTemplate.queryForMap("SELECT * FROM channel WHERE id = ?", channelId);

    assertEquals(expectedTitle, updatedChannelData.get("title"));
    assertEquals(expectedDescription, updatedChannelData.get("description"));
    assertEquals(Arrays.asList(expectedKeywords), Arrays.asList(updatedChannelData.get("keywords")));
    assertEquals(expectedOrganisation, updatedChannelData.get("organisation"));
    assertEquals(expectedStrapline, updatedChannelData.get("strapline"));
    assertEquals(expectedOwnerUserId, updatedChannelData.get("owner_user_id"));

    final Date lastUpdatedDate = (Date) updatedChannelData.get("last_updated");
    assertNotNull(lastUpdatedDate);

    final Date now = new Date();
    final long expectedNow = now.getTime();
    final long resultLastUpdate = lastUpdatedDate.getTime();
    // the difference between and last update might be about a second if the test took long to run
    assertEquals(expectedNow, resultLastUpdate, 1000);
  }

  /**
   * Tests {@link JdbcChannelDbDao#update} in the case of updating channel details with keywords containing spaces.
   * <p>
   * Expected Result: The updated keywords will be inserted in the database with quotation, however the return channel
   * from the update() method will not contain quotation, it will be same as what it was supplied to the method.
   */
  @Test
  public void updateWithChannelKeywordsContainingSpaces() {

    // Set up
    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    final String expectedTitle = "expectedTitle";
    final String expectedDescription = "expectedDescription";
    final String expectedOrganisation = "expectedOrganisation";
    final String expectedStrapline = "expectedStrapline";
    final Long expectedOwnerUserId = 1001L;
    final String expectedKeywords = "expectedKeywords,keyword 1,keyword 2,keyword 3";
    final String expectedKeywordsWithQuotation = "expectedKeywords,keyword 1,keyword 2,keyword 3";
    final List<String> expectedKeywordsList = Arrays.asList(expectedKeywords.split(KEYWORDS_SEPARATOR));

    Channel updateChannel = new Channel(channelId);
    updateChannel.setTitle(expectedTitle);
    updateChannel.setDescription(expectedDescription);
    updateChannel.setKeywords(expectedKeywordsList);
    updateChannel.setOrganisation(expectedOrganisation);
    updateChannel.setStrapline(expectedStrapline);
    updateChannel.setOwnerUserId(expectedOwnerUserId);

    // Do test
    Channel channel = uut.update(updateChannel);

    // Assertions
    // Assert existing channel keywords without quotation (the quotation will only be inserted in the database).
    assertEquals(expectedKeywordsList, channel.getKeywords());

    Map<String, Object> updatedChannelData = jdbcTemplate.queryForMap("SELECT * FROM channel WHERE id = ?", channelId);
    assertEquals(expectedTitle, updatedChannelData.get("title"));
    assertEquals(expectedDescription, updatedChannelData.get("description"));
    // Assert the updated channel has keywords wrapped with quotation.
    assertEquals(expectedKeywordsWithQuotation, updatedChannelData.get("keywords"));
    assertEquals(expectedOrganisation, updatedChannelData.get("organisation"));
    assertEquals(expectedStrapline, updatedChannelData.get("strapline"));
    assertEquals(expectedOwnerUserId, updatedChannelData.get("owner_user_id"));

    final Date lastUpdatedDate = (Date) updatedChannelData.get("last_updated");
    assertNotNull(lastUpdatedDate);

    final Date now = new Date();
    final long expectedNow = now.getTime();
    final long resultLastUpdate = lastUpdatedDate.getTime();
    // the difference between and last update might be about a second if the test took long to run
    assertEquals(expectedNow, resultLastUpdate, 1000);
  }

  @Test
  public void delete() {

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    // do test
    uut.delete(channelId);

    // Check the results
    Map<String, Object> updatedChannelData = jdbcTemplate.queryForMap("SELECT * FROM channel WHERE id = ?", channelId);

    // assertions
    Boolean channelIsActive = (Boolean) updatedChannelData.get("is_active");
    assertFalse(channelIsActive);
  }

  @Test
  public void existsById() {

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    // do test
    boolean result = uut.exists(channelId);

    assertTrue(result);
  }

  @Test
  public void doesNotExistById() {

    Long nonExistingChannelId = 99999l;

    // do test
    boolean result = uut.exists(nonExistingChannelId);

    assertFalse(result);
  }

  @Test
  public void doesNotExistBecauseIsNotActiveById() {

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);
    jdbcTemplate.update("UPDATE channel SET is_active = 0 WHERE id = ? ", channelId);

    // do test
    boolean result = uut.exists(channelId);

    assertFalse(result);
  }

  @Test
  public void titleInUse() {

    final String expectedTitle = "expectedTitle";

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("title", expectedTitle);
    params.addValue("id", channelId);

    namedParameterJdbcTemplate.update("UPDATE channel SET title = :title WHERE id = :id ", params);

    // do test
    boolean result = uut.titleInUse(expectedTitle);

    assertTrue(result);
  }

  @Test
  public void titleNotInUse() {

    final String expectedTitle = "expectedTitle";

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("title", expectedTitle);
    params.addValue("id", channelId);

    namedParameterJdbcTemplate.update("UPDATE channel SET title = :title WHERE id = :id ", params);

    final String someOtherTitle = "someOtherTitleNotExisting";

    // do test
    boolean result = uut.titleInUse(someOtherTitle);

    assertFalse(result);
  }

  @Test
  public void titleInUseButNotActive() {

    final String expectedTitle = "expectedTitle";

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("title", expectedTitle);
    params.addValue("id", channelId);

    namedParameterJdbcTemplate.update("UPDATE channel SET title = :title, is_active = 0 WHERE id = :id ", params);

    // do test
    boolean result = uut.titleInUse(expectedTitle);

    assertFalse(result);
  }

  @Test
  public void titleInUseExcludingChannel() {

    final String expectedTitle = "expectedTitle";

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("title", expectedTitle);
    params.addValue("id", channelId);

    namedParameterJdbcTemplate.update("UPDATE channel SET title = :title WHERE id = :id ", params);

    // do test
    boolean result = uut.titleInUseExcludingChannel(expectedTitle, channelId + 1);

    assertTrue(result);
  }

  @Test
  public void titleInUseExcludingChannelNotActive() {

    final String expectedTitle = "expectedTitle";

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("title", expectedTitle);
    params.addValue("id", channelId);

    namedParameterJdbcTemplate.update("UPDATE channel SET title = :title, is_active = 0 WHERE id = :id ", params);

    // do test
    boolean result = uut.titleInUseExcludingChannel(expectedTitle, channelId + 1);

    assertFalse(result);
  }

  @Test
  public void titleNotInUseExcludingChannel() {

    final String expectedTitle = "expectedTitle";

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, null, true);

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("title", expectedTitle);
    params.addValue("id", channelId);

    namedParameterJdbcTemplate.update("UPDATE channel SET title = :title WHERE id = :id ", params);

    // do test
    boolean result = uut.titleInUseExcludingChannel(expectedTitle, channelId);

    assertFalse(result);
  }

}