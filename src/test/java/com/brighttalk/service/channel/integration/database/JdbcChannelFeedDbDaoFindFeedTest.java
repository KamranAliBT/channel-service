/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: JdbcChannelFeedDbDaoFindFeedTest.java 96745 2015-06-17 15:58:25Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;

/**
 * Tests {link JdbcCommunicationDbDao} related to findFeed.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcChannelFeedDbDaoFindFeedTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcChannelFeedDbDao uut;

  @Test
  public void findFeaturedWithNoCommunications() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  @Test
  public void findFeaturedWithInvalidChannel() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;
    long channelId = 99999999;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  @Test
  public void findFeaturedWithNullChannelId() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;
    Long channelId = null;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Standard Select
   */
  @Test
  public void getFeedWithOneCommunication() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Select more than one communication at a time
   */
  @Test
  public void getFeedWithTwoCommunications() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication1 = buildCommunication(channelId);
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    int expectedSize = 2;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Pagination
   */
  @Test
  public void getFeedWithTwoCommunicationsPageSizeOfOne() {
    // Set up
    int pageNumber = 1;
    int pageSize = 1;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication1 = buildCommunication(channelId);
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Pagination
   */
  @Test
  public void getFeedWithTwoCommunicationsPageNumberTwo() {
    // Set up
    int pageNumber = 2;
    int pageSize = 1;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication1 = buildCommunication(channelId);
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Pagination
   */
  @Test
  public void getFeedWithTwoCommunicationsPageNumberThree() {
    // Set up
    int pageNumber = 3;
    int pageSize = 1;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication1 = buildCommunication(channelId);
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Ordering
   */
  @Test
  public void getFeedWithTwoCommunicationsFutureFirst() {
    // Set up
    int pageNumber = 1;
    int pageSize = 1;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, 2));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, 1));
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    int expectedSize = 1;
    long expectedCommunicationId = communication1.getId();

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
    assertEquals("incorrect Communication returned", expectedCommunicationId, result.get(0).getId(), 0.1);
  }

  /**
   * Testing Ordering
   */
  @Test
  public void getFeedWithTwoCommunicationsFutureFirstInsertedInDifferentOrder() {
    // Set up
    int pageNumber = 1;
    int pageSize = 1;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, 1));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, 2));
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    int expectedSize = 1;
    long expectedCommunicationId = communication2.getId();

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
    assertEquals("incorrect Communication returned", expectedCommunicationId, result.get(0).getId(), 0.1);
  }

  /**
   * Testing Ordering
   */
  @Test
  public void getFeedWithTwoCommunicationsFutureFirstPageTwo() {
    // Set up
    int pageNumber = 2;
    int pageSize = 1;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, 2));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, 1));
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    int expectedSize = 1;
    long expectedCommunicationId = communication2.getId();

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
    assertEquals("incorrect Communication returned", expectedCommunicationId, result.get(0).getId(), 0.1);
  }

  /**
   * Testing Filtering
   */
  @Test
  public void getFeedWithOneCommunicationInactiveChannel() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = false;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Filtering
   */
  @Test
  public void getFeedWithOneCommunicationDeletedCommunication() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.setStatus(Communication.Status.DELETED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeed(Long, ChannelFeedSearchCriteria)} will include all communications if
   * the status filter is not set in the criteria, except if they are cancelled or deleted.
   */
  @Test
  public void totalAfterDateWithoutSpecifyingCommunicationStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication cancelledCommunication = buildCommunication(channelId);
    cancelledCommunication.setStatus(Communication.Status.CANCELLED);

    Communication deletedCommunication = buildCommunication(channelId);
    deletedCommunication.setStatus(Communication.Status.DELETED);

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);

    addTemporaryCommunication(channelId, upcomingCommunication);
    addTemporaryCommunication(channelId, liveCommunication);
    addTemporaryCommunication(channelId, recordedCommunication);
    addTemporaryCommunication(channelId, cancelledCommunication);
    addTemporaryCommunication(channelId, deletedCommunication);

    int pageNumber = 1;
    int pageSize = 10;
    List<CommunicationStatusFilter> statusFilter = Collections.<CommunicationStatusFilter>emptyList();

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, statusFilter);

    // Expectations
    int expectedSize = 3;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeed(Long, ChannelFeedSearchCriteria)} with a status filter to only
   * include communications that are upcoming.
   */
  @Test
  public void getFeedWithUpcomingStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);

    addTemporaryCommunication(channelId, upcomingCommunication);

    int pageNumber = 1;
    int pageSize = 10;
    List<CommunicationStatusFilter> statusFilter =
        Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.UPCOMING);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, statusFilter);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeed(Long, ChannelFeedSearchCriteria)} with a status filter to only
   * include communications that are live.
   */
  @Test
  public void getFeedWithLiveStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);

    addTemporaryCommunication(channelId, liveCommunication);

    int pageNumber = 1;
    int pageSize = 10;
    List<CommunicationStatusFilter> statusFilter =
        Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.LIVE);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, statusFilter);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeed(Long, ChannelFeedSearchCriteria)} with a status filter to only
   * include communications that are recorded.
   */
  @Test
  public void getFeedWithRecordedStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);

    addTemporaryCommunication(channelId, recordedCommunication);

    int pageNumber = 1;
    int pageSize = 10;
    List<CommunicationStatusFilter> statusFilter =
        Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.RECORDED);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, statusFilter);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeed(Long, ChannelFeedSearchCriteria)} with a status filter to only
   * include communications that are upcoming or live.
   */
  @Test
  public void getFeedWithMultipleStatusesFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);

    addTemporaryCommunication(channelId, upcomingCommunication);
    addTemporaryCommunication(channelId, liveCommunication);
    addTemporaryCommunication(channelId, recordedCommunication);

    int pageNumber = 1;
    int pageSize = 10;
    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(
        CommunicationStatusFilter.UPCOMING, CommunicationStatusFilter.LIVE);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, statusFilter);

    // Expectations
    int expectedSize = 2;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Tests {@link JdbcChannelFeedDbDao#findFeed(Long, ChannelFeedSearchCriteria)} will correctly page if the status
   * filter is set to only include upcoming communications.
   * 
   * In this case we create numerous communications with varying statuses, and two upcoming communications. The filter
   * is set to include only upcoming communications. With a page size of 1, page 2 should return the second upcoming
   * webcast.
   */
  @Test
  public void getFeedWithOnlyUpcomingCommunicationsWithPagination() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Calendar calendar = Calendar.getInstance();
    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);
    upcomingCommunication.setScheduledDateTime(calendar.getTime());
    addTemporaryCommunication(channelId, upcomingCommunication);

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);
    addTemporaryCommunication(channelId, liveCommunication);

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);
    addTemporaryCommunication(channelId, recordedCommunication);

    calendar.add(Calendar.DAY_OF_YEAR, 1);
    Communication upcomingCommunication2 = buildCommunication(channelId);
    upcomingCommunication2.setStatus(Communication.Status.UPCOMING);
    upcomingCommunication2.setScheduledDateTime(calendar.getTime());
    addTemporaryCommunication(channelId, upcomingCommunication2);

    int pageNumber = 2;
    int pageSize = 1;
    List<CommunicationStatusFilter> statusFilter =
        Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.UPCOMING);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, statusFilter);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
    assertEquals(upcomingCommunication, result.get(0));
  }

  /**
   * Testing Filtering
   */
  @Test
  public void getFeedWithOneCommunicationCancelledCommunication() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.setStatus(Communication.Status.CANCELLED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Filtering
   */
  @Test
  public void getFeedWithOneCommunicationUnpublishedCommunication() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.setPublishStatus(Communication.PublishStatus.UNPUBLISHED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Filtering
   */
  @Test
  public void getFeedWithOneCommunicationPublicCommunication() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.PUBLIC);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Filtering
   */
  @Test
  public void getFeedWithOneCommunicationPrivateCommunication() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.PRIVATE);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Filtering
   */
  @Test
  public void getFeedWithOneCommunicationFeedOnlyCommunication() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.FEEDONLY);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void getFeedWithOneCommunicationSyndicatedOut() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.OUT);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void getFeedWithOneCommunicationSyndicatedInApprovedByBoth() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 1;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void getFeedWithOneCommunicationSyndicatedInNotApprovedByMaster() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);

    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void getFeedWithOneCommunicationSyndicatedInNotApprovedByConsumer() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void getFeedWithOneCommunicationSyndicatedInNotApprovedByBoth() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void getFeedWithOneCommunicationSyndicatedInPendingBoth() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void getFeedWithOneCommunicationSyndicatedInPendingMaster() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void getFeedWithOneCommunicationSyndicatedInPendingConsumer() {
    // Set up
    int pageNumber = 1;
    int pageSize = 10;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteria(pageNumber, pageSize, null);

    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedSize = 0;

    // Do Test
    List<Communication> result = uut.findFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed size", expectedSize, result.size());
  }

  private ChannelFeedSearchCriteria buildChannelFeedSearchCriteria(final Integer pageNumber, final Integer pageSize,
      final List<CommunicationStatusFilter> statusFilter) {

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria(pageNumber, pageSize, null);
    criteria.setCommunicationStatusFilter(statusFilter);

    return criteria;
  }
}
