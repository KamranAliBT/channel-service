package com.brighttalk.service.channel.integration.database.data;

import com.brighttalk.service.channel.business.domain.communication.Communication;

import java.util.Date;

public class ScheduledPublicationData {

	private Long communicationId;
	private Date scheduledDate;
	private String jobId;
	private boolean completed;
	private Communication.PublishStatus publishStatus;
	private boolean active;

	public Long getCommunicationId()
	{
		return communicationId;
	}

	public void setCommunicationId(final Long communicationId)
	{
		this.communicationId = communicationId;
	}

	public Date getScheduledDate()
	{
		return scheduledDate;
	}

	public void setScheduledDate(final Date scheduledDate)
	{
		this.scheduledDate = scheduledDate;
	}

	public String getJobId()
	{
		return jobId;
	}

	public void setJobId(final String jobId)
	{
		this.jobId = jobId;
	}

	public boolean isCompleted()
	{
		return completed;
	}

	public void setCompleted(final boolean completed)
	{
		this.completed = completed;
	}

	public Communication.PublishStatus getPublishStatus()
	{
		return publishStatus;
	}

	public void setPublishStatus(final Communication.PublishStatus publishStatus)
	{
		this.publishStatus = publishStatus;
	}

	public void setActive(final boolean active)
	{
		this.active = active;
	}

	public boolean isActive()
	{
		return active;
	}
}