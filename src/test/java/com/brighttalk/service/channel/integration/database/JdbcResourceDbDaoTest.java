/**
 * ****************************************************************************
 \ * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcResourceDbDaoTest.java 69149 2013-10-02 09:17:11Z build $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.Entity;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * Tests {link JdbcResourceDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcResourceDbDaoTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcResourceDbDao uut;

  @Test
  public void testFindResourceById() {
    // setup
    Long resourceId = addTemporaryResource(true);

    // do test
    Resource result = uut.find(resourceId);

    // assertions
    Assert.assertEquals(resourceId, result.getId());
    Assert.assertEquals(RESOURCE_TITLE, result.getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.getDescription());
    Assert.assertEquals(RESOURCE_URL, result.getUrl());
    Assert.assertEquals(RESOURCE_MIME_TYPE, result.getMimeType());
    Assert.assertEquals(RESOURCE_SIZE, result.getSize());
    Assert.assertEquals(Resource.Type.FILE, result.getType());
  }

  @Test(expected = NotFoundException.class)
  public void testFindResourceByNotExistingId() {

    // do test
    uut.find(0L);
  }

  @Test
  public void testCreateResourceLink() {
    Resource resource = new Resource();
    resource.setTitle(RESOURCE_TITLE);
    resource.setDescription(RESOURCE_DESCRIPTION);
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL);
    resource.setPrecedence(1);

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // do test
    Resource result = uut.create(communicationId, resource);

    // assertions
    Assert.assertNotNull(result.getId());
    temporaryResourceIds.add(result.getId());

    Map<String, Object> createdResource = jdbcTemplate.queryForMap("SELECT * FROM resource r WHERE r.id = ?",
        result.getId());

    Assert.assertEquals(RESOURCE_TITLE, createdResource.get("title").toString());
    Assert.assertEquals(RESOURCE_URL, createdResource.get("url").toString());

    Map<String, Object> createdCommunicationAsset = jdbcTemplate.queryForMap(
        "SELECT * FROM communication_asset WHERE communication_id=? AND  target_id = ? ", communicationId,
        result.getId());

    Assert.assertNotNull(createdCommunicationAsset);
    Assert.assertEquals("resource", createdCommunicationAsset.get("type"));
    Assert.assertEquals(Boolean.TRUE, createdCommunicationAsset.get("is_active"));

  }

  @Test
  public void testCreateResourceLinkWithDescriptionIsNull() {
    Resource resource = new Resource();
    resource.setTitle(RESOURCE_TITLE);
    resource.setDescription(null);
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL);
    resource.setPrecedence(1);

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // do test
    Resource result = uut.create(communicationId, resource);

    // assertions
    Assert.assertNotNull(result.getId());
    temporaryResourceIds.add(result.getId());
    try {
      jdbcTemplate.queryForMap("SELECT d.* FROM " + "resource_description d WHERE d.id = ?", result.getId());
      Assert.fail();
    } catch (EmptyResultDataAccessException e) {
    }

  }

  @Test
  public void testCreateResourceLinkTestJapaneseUTF8() {
    Resource resource = new Resource();
    resource.setTitle("どこからですか");
    resource.setDescription("どこからですか");
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL);
    resource.setPrecedence(1);

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // do test
    Resource result = uut.create(communicationId, resource);

    // assertions
    Assert.assertNotNull(result.getId());
    temporaryResourceIds.add(result.getId());

    Map<String, Object> createdResource = jdbcTemplate.queryForMap(
        "SELECT * FROM resource r, resource_description d  WHERE r.id = ? AND d.id=r.id ", result.getId());

    Assert.assertEquals("どこからですか", createdResource.get("title").toString());
    Assert.assertEquals("どこからですか", createdResource.get("description").toString());

  }

  @Test
  public void testCreateResourceLinkTestJKoreanUTF8() {
    Resource resource = new Resource();
    resource.setTitle("영어 할줄압니까");
    resource.setDescription("영어 할줄압니까");
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL);
    resource.setPrecedence(1);

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // do test
    Resource result = uut.create(communicationId, resource);

    // assertions
    Assert.assertNotNull(result.getId());
    temporaryResourceIds.add(result.getId());

    Map<String, Object> createdResource = jdbcTemplate.queryForMap(
        "SELECT * FROM resource r, resource_description d  WHERE r.id = ? AND d.id=r.id ", result.getId());

    Assert.assertEquals("영어 할줄압니까", createdResource.get("title").toString());
    Assert.assertEquals("영어 할줄압니까", createdResource.get("description").toString());

  }

  @Test
  public void testCreateResourceLinkTestGermanUTF8() {
    Resource resource = new Resource();
    resource.setTitle("äÄöÖüÜß");
    resource.setDescription("äÄöÖüÜß");
    resource.setType(Resource.Type.LINK);
    resource.setUrl(RESOURCE_URL);
    resource.setPrecedence(1);

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // do test
    Resource result = uut.create(communicationId, resource);

    // assertions
    Assert.assertNotNull(result.getId());
    temporaryResourceIds.add(result.getId());

    Map<String, Object> createdResource = jdbcTemplate.queryForMap(
        "SELECT * FROM resource r, resource_description d  WHERE r.id = ? AND d.id=r.id ", result.getId());

    Assert.assertEquals("äÄöÖüÜß", createdResource.get("title").toString());
    Assert.assertEquals("äÄöÖüÜß", createdResource.get("description").toString());

  }

  @Test
  public void testCreateResourceFileInternal() {
    // setup
    Resource resource = new Resource();
    resource.setTitle(RESOURCE_TITLE);
    resource.setDescription(RESOURCE_DESCRIPTION);
    resource.setType(Resource.Type.FILE);
    resource.setHosted(Resource.Hosted.INTERNAL);
    resource.setUrl(RESOURCE_URL);
    resource.setSize(RESOURCE_SIZE);
    resource.setMimeType(RESOURCE_MIME_TYPE);
    resource.setPrecedence(1);

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // do test
    Resource result = uut.create(communicationId, resource);

    // assertions
    Assert.assertNotNull(result.getId());
    temporaryResourceIds.add(result.getId());

    Map<String, Object> createdResource = jdbcTemplate.queryForMap("SELECT * FROM resource r WHERE r.id = ?",
        result.getId());

    Assert.assertEquals(RESOURCE_TITLE, createdResource.get("title").toString());
    Assert.assertEquals(RESOURCE_URL, createdResource.get("url").toString());
    // Long size = ( (java.math.BigInteger)createdResource.get("size"));
    // Assert.assertEquals(RESOURCE_SIZE, size);
    Assert.assertEquals(RESOURCE_MIME_TYPE, createdResource.get("mime_type").toString());
    Assert.assertEquals(RESOURCE_HOSTED_INTERNAL, createdResource.get("hosted").toString());
    Assert.assertEquals(Resource.Type.FILE.name().toLowerCase(), createdResource.get("type").toString());

    Map<String, Object> createdCommunicationAsset = jdbcTemplate.queryForMap(
        "SELECT * FROM communication_asset WHERE communication_id=? AND  target_id = ? ", communicationId,
        result.getId());

    Assert.assertNotNull(createdCommunicationAsset);
    Assert.assertEquals("resource", createdCommunicationAsset.get("type"));
    Assert.assertEquals(Boolean.TRUE, createdCommunicationAsset.get("is_active"));
  }

  @Test
  public void testCreateResourceFileExternal() {
    // setup
    Resource resource = new Resource();
    resource.setTitle(RESOURCE_TITLE);
    resource.setDescription(RESOURCE_DESCRIPTION);
    resource.setType(Resource.Type.FILE);
    resource.setHosted(Resource.Hosted.EXTERNAL);
    resource.setUrl(RESOURCE_URL);
    resource.setSize(RESOURCE_SIZE);
    resource.setMimeType(RESOURCE_MIME_TYPE);
    resource.setPrecedence(1);

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // do test
    Resource result = uut.create(communicationId, resource);

    // assertions
    Assert.assertNotNull(result.getId());
    temporaryResourceIds.add(result.getId());

    Map<String, Object> createdResource = jdbcTemplate.queryForMap("SELECT * FROM resource r WHERE r.id = ?",
        result.getId());

    Assert.assertEquals(RESOURCE_TITLE, createdResource.get("title").toString());
    Assert.assertEquals(RESOURCE_URL, createdResource.get("url").toString());
    // Long size = ((Long) createdResource.get("size")).longValue();
    // Assert.assertEquals(RESOURCE_SIZE, size);
    Assert.assertEquals(RESOURCE_MIME_TYPE, createdResource.get("mime_type").toString());
    Assert.assertEquals(RESOURCE_HOSTED_EXTERNAL, createdResource.get("hosted").toString());
    Assert.assertEquals(Resource.Type.FILE.name().toLowerCase(), createdResource.get("type").toString());

    Map<String, Object> createdCommunicationAsset = jdbcTemplate.queryForMap(
        "SELECT * FROM communication_asset WHERE communication_id=? AND  target_id = ? ", communicationId,
        result.getId());

    Assert.assertNotNull(createdCommunicationAsset);
    Assert.assertEquals("resource", createdCommunicationAsset.get("type"));
    Assert.assertEquals(Boolean.TRUE, createdCommunicationAsset.get("is_active"));
  }

  @Test
  public void testUpdateResourceFileUrl() {
    // setup
    Long resourecId = addTemporaryResource(true);
    Resource resource = new Resource(resourecId);
    resource.setUrl(RESOURCE_URL_2);
    resource.setSize(RESOURCE_SIZE_2);
    resource.setMimeType(RESOURCE_MIME_TYPE_2);
    resource.setType(Resource.Type.FILE);
    resource.setTitle(RESOURCE_TITLE_2);
    resource.setDescription(RESOURCE_DESCRIPTION_2);

    // do test
    uut.update(resource);

    // Check the results
    Map<String, Object> updatedResource = jdbcTemplate.queryForMap(
        "SELECT * FROM resource r, resource_description d  WHERE r.id = ? AND d.id=r.id ", resourecId);

    Assert.assertEquals(RESOURCE_URL_2, updatedResource.get("url").toString());
    // Long size = ((Long) updatedResource.get("size")).longValue();
    // Assert.assertEquals(RESOURCE_SIZE_2, size);
    Assert.assertEquals(RESOURCE_MIME_TYPE_2, updatedResource.get("mime_type").toString());
    Assert.assertEquals(RESOURCE_TITLE_2, updatedResource.get("title").toString());
    Assert.assertEquals(RESOURCE_DESCRIPTION_2, updatedResource.get("description").toString());
  }

  @Test
  public void testUpdateResourceLinkUrl() {
    // setup
    Long resourecId = addTemporaryResource(false);
    Resource resource = new Resource(resourecId);
    resource.setUrl(RESOURCE_URL_2);
    resource.setType(Resource.Type.LINK);
    resource.setPrecedence(1);
    resource.setTitle(RESOURCE_TITLE_2);

    // do test
    uut.update(resource);

    // Check the results
    Map<String, Object> updatedResource = jdbcTemplate.queryForMap("SELECT * FROM resource WHERE id = ?", resourecId);

    // assertions
    Assert.assertEquals(RESOURCE_URL_2, updatedResource.get("url").toString());
  }

  @Test
  public void testUpdateResourceLinkUrlWithOutDescription() {
    // setup
    Long resourecId = addTemporaryResource(false, false);
    Resource resource = new Resource(resourecId);
    resource.setUrl(RESOURCE_URL_2);
    resource.setDescription(RESOURCE_DESCRIPTION_2);
    resource.setType(Resource.Type.LINK);
    resource.setPrecedence(1);
    resource.setTitle(RESOURCE_TITLE_2);

    // do test
    uut.update(resource);

    // Check the results
    Map<String, Object> updatedResource = jdbcTemplate.queryForMap(
        "SELECT * FROM resource r,resource_description d WHERE r.id=d.id AND r.id = ?", resourecId);

    // assertions
    Assert.assertEquals(RESOURCE_DESCRIPTION_2, updatedResource.get("description").toString());
  }

  @Test
  public void testUpdateResourceLinkUrlWithDescriptionIsEmpty() {
    // setup
    Long resourecId = addTemporaryResource(false, false);
    Resource resource = new Resource(resourecId);
    resource.setUrl(RESOURCE_URL_2);
    resource.setDescription(Entity.DELETE_INDICATOR);
    resource.setType(Resource.Type.LINK);
    resource.setPrecedence(1);
    resource.setTitle(RESOURCE_TITLE_2);

    // do test
    uut.update(resource);

    // Check the results
    Map<String, Object> updatedResource = jdbcTemplate.queryForMap(
        "SELECT * FROM resource r,resource_description d WHERE r.id=d.id AND r.id = ?", resourecId);

    // assertions
    Assert.assertNull(updatedResource.get("description"));
  }

  @Test
  public void testUpdateResourceFileUrlWithEmptyMimeType() {
    // setup
    Long resourecId = addTemporaryResource(false, false);
    Resource resource = new Resource(resourecId);
    resource.setUrl(RESOURCE_URL_2);
    resource.setMimeType(Entity.DELETE_INDICATOR);
    resource.setType(Resource.Type.LINK);
    resource.setPrecedence(1);
    resource.setTitle(RESOURCE_TITLE_2);

    // do test
    uut.update(resource);

    // Check the results
    Map<String, Object> updatedResource = jdbcTemplate.queryForMap(
        "SELECT * FROM resource r,resource_description d WHERE r.id=d.id AND r.id = ?", resourecId);

    // assertions
    Assert.assertNull(updatedResource.get("mimeType"));
  }

  @Test
  public void testDeleteResourceLink() {
    // setup
    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    Long resourceId = addTemporaryResource(true);
    addCommunicationResourceAsset(resourceId, communicationId);

    // do test
    uut.delete(communicationId, resourceId);

    // Check the results
    Map<String, Object> deletedResource = jdbcTemplate.queryForMap("SELECT * FROM  resource  WHERE id = ?", resourceId);

    // assertions
    Assert.assertEquals(Boolean.FALSE, (deletedResource.get("is_active")));

    Map<String, Object> deletedCommunicationAsset = jdbcTemplate.queryForMap(
        "SELECT * FROM communication_asset WHERE communication_id=? AND  target_id = ? ", communicationId, resourceId);

    Assert.assertNotNull(deletedCommunicationAsset);
    Assert.assertEquals("resource", deletedCommunicationAsset.get("type"));
    Assert.assertEquals(Boolean.FALSE, deletedCommunicationAsset.get("is_active"));
  }

  @Test
  public void testFindAllResourceByCommunicationIdWithOneResource() {

    // setup
    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    Long resourceId = addTemporaryResource(true);
    addCommunicationResourceAsset(resourceId, communicationId);

    // do test
    List<Resource> result = uut.findAll(communicationId);

    // assertions
    Assert.assertEquals(1, result.size());
    Assert.assertEquals(resourceId, result.get(0).getId());
    Assert.assertEquals(RESOURCE_TITLE, result.get(0).getTitle());
    Assert.assertEquals(RESOURCE_DESCRIPTION, result.get(0).getDescription());
    Assert.assertEquals(RESOURCE_URL, result.get(0).getUrl());
    Assert.assertEquals(RESOURCE_MIME_TYPE, result.get(0).getMimeType());
    Assert.assertEquals(RESOURCE_SIZE, result.get(0).getSize());
    Assert.assertEquals(Resource.Type.FILE, result.get(0).getType());
  }

  /**
   * Test to check that only resources are selected byt the find all query - @see PRODISSUES-73.
   */
  @Test
  public void testFindAllResourceByCommunicationIdWithConfilitingIds() {

    // setup
    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    Long resourceId = addTemporaryResource(true);

    // add the resource - but label it as a video
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communication_id", communicationId);
    params.addValue("target_id", resourceId);
    params.addValue("type", "vide");
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    params.addValue("is_active", 1);
    simpleJdbcInsertCommunicationAsset.execute(params);

    // do test
    List<Resource> result = uut.findAll(communicationId);

    // assertions
    Assert.assertEquals(0, result.size());
  }

  @Test
  public void testFindAllResourceByCommunicationIdWithTwoResources() {

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // setup
    Long resourceId = addTemporaryResource(true);
    addCommunicationResourceAsset(resourceId, communicationId);
    Long resourceId2 = addTemporaryResource(false);
    addCommunicationResourceAsset(resourceId2, communicationId);

    // expectations

    // do test
    List<Resource> result = uut.findAll(communicationId);

    // assertions
    Assert.assertEquals(2, result.size());
    Assert.assertEquals(resourceId, result.get(0).getId());
    Assert.assertEquals(resourceId2, result.get(1).getId());

  }

  @Test
  public void testFindAllResourceByCommunicationIdInvalidCommunicationId() {

    // setup

    // expectations

    // do test
    List<Resource> result = uut.findAll(0L);

    // assertions
    Assert.assertEquals(0, result.size());
  }

  @Test
  public void testUpdateResourceOrderTwo() {

    // setup
    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_TYPE_ID, null, true);
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    Long resourceId = addTemporaryResource(true);
    addCommunicationResourceAsset(resourceId, communicationId);
    Long resourceId2 = addTemporaryResource(true);
    addCommunicationResourceAsset(resourceId2, communicationId);
    List<Resource> resourcesNewOrder = new ArrayList<Resource>();
    resourcesNewOrder.add(new Resource(resourceId2));
    resourcesNewOrder.add(new Resource(resourceId));

    // do test
    uut.updateOrder(resourcesNewOrder);

    List<Resource> result = jdbcTemplate.query(
        "SELECT * FROM resource r, communication_asset a  WHERE a.communication_id = ? AND a.target_id=r.id AND r.is_active=1",
        new ResourceRowMapper(), communicationId);
    // assertions
    Assert.assertEquals(2, result.size());
    Assert.assertEquals(resourceId, result.get(0).getId());
    Assert.assertEquals(Integer.valueOf(1), result.get(0).getPrecedence());
    Assert.assertEquals(resourceId2, result.get(1).getId());
    Assert.assertEquals(Integer.valueOf(0), result.get(1).getPrecedence());

  }

  @Test(expected = NotFoundException.class)
  public void testFindChannelAndCommunicationIdNoResource() {
    // Set up
    Long resourceId = 99999999L;

    // Expectations

    // Do Test
    uut.findChannelIdAndCommunicationId(resourceId);

    // Assertions
  }

  @Test
  public void testFindChannelAndCommunicationIdOneResource() {
    // Set up
    Long channelId = addTemporaryChannelWithTypes();
    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);
    Long resourceId = addTemporaryResource(false);
    addCommunicationResourceAsset(resourceId, communicationId);

    // Expectations
    Map<String, Long> expectedResult = new HashMap<String, Long>();
    expectedResult.put("channelId", channelId);
    expectedResult.put("communicationId", communicationId);

    // Do Test
    Map<String, Long> result = uut.findChannelIdAndCommunicationId(resourceId);

    // Assertions
    Assert.assertEquals(expectedResult, result);
  }

  @Test
  public void testConvertDeleteIndicatorWithNullValue() {
    Assert.assertNull(uut.convertDeleteIndicator(null));
  }

  @Test
  public void testConvertDeleteIndicatorWithDeleteIndicatorValue() {
    Assert.assertNull(uut.convertDeleteIndicator(Entity.DELETE_INDICATOR));
  }

  @Test
  public void testConvertDeleteIndicatorWithProperValue() {
    Assert.assertEquals("real value", uut.convertDeleteIndicator("real value"));
  }

  @Test
  public void testConvertDeleteIndicatorWithEmptyValue() {
    Assert.assertEquals("", uut.convertDeleteIndicator(""));
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Resource}.
   */
  private static class ResourceRowMapper implements RowMapper<Resource> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Resource mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Resource resource = new Resource(resultSet.getLong("id"));
      resource.setPrecedence(resultSet.getInt("precedence"));
      return resource;
    }
  }
}
