/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationRegistrationsDbDaoUserRegistrationsTest.java 76805 2014-04-23 11:07:05Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang.time.DateUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria.CommunicationStatusFilterType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCommunicationRegistrationsDbDaoUserRegistrationsTest extends AbstractJdbcDbDaoTest {

  private static final Long USER_ID = 999999L;

  @Autowired
  private JdbcCommunicationRegistrationsDbDao uut;

  private SimpleJdbcInsert simpleJdbcInsertCommunicationRegistration;

  private List<Long> temporaryRegistrationIds = new ArrayList<Long>();

  @Autowired
  @Override
  public void setDataSource(final DataSource dataSource) {

    super.setDataSource(dataSource);
    simpleJdbcInsertCommunicationRegistration = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertCommunicationRegistration.withTableName("communication_registration").usingGeneratedKeyColumns("id");
  }

  @After
  @Override
  public void tearDown() {

    super.tearDown();

    for (Long registrationId : temporaryRegistrationIds) {

      jdbcTemplate.update("DELETE FROM communication_registration WHERE id = ?", registrationId);
    }

    temporaryRegistrationIds = new ArrayList<Long>();
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenNoneCreated() {

    // do test
    int result = uut.findTotalRegistrations(USER_ID, new CommunicationRegistrationSearchCriteria());

    final int expectedResult = 0;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsNoSearchCriteria() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);
    final Communication communication = buildCommunication(channelId);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, new CommunicationRegistrationSearchCriteria());

    final int expectedResult = 1;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenFilterUpcoming() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.UPCOMING);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.UPCOMING);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, criteria);

    final int expectedResult = 1;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenFilterUpcomingWhenNoneCreated() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.LIVE);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.UPCOMING);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, criteria);

    final int expectedResult = 0;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenFilterLive() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.LIVE);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.LIVE);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, criteria);

    final int expectedResult = 1;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenFilterLiveWhenNoneCreated() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.UPCOMING);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.LIVE);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, criteria);

    final int expectedResult = 0;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenFilterRecorded() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.RECORDED);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.RECORDED);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, criteria);

    final int expectedResult = 1;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenFilterRecordedWhenNoneCreated() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.LIVE);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.RECORDED);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, criteria);

    final int expectedResult = 0;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenFilterForValidChannel() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);
    criteria.setChannelIds(channelIds);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, criteria);

    final int expectedResult = 1;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenFilterForInvalidChannel() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId + 1);
    criteria.setChannelIds(channelIds);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, criteria);

    final int expectedResult = 0;
    assertEquals(expectedResult, result);
  }

  @Test
  public void getTotalNumberOfRegistrationsWhenFilterForStatusAndChannel() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);
    criteria.setChannelIds(channelIds);

    // do test
    int result = uut.findTotalRegistrations(USER_ID, criteria);

    final int expectedResult = 1;
    assertEquals(expectedResult, result);
  }

  @Test
  public void findWhenNoRegistrations() {

    final CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    final int expectedResultSize = 0;
    assertEquals(expectedResultSize, result.size());
  }

  @Test
  public void findNoSearchCriteria() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    final Long registrationId = insertCommunicationRegistration(channelId, communicationId, USER_ID);

    final CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);

    final int expectedResultSize = 1;
    assertEquals(expectedResultSize, result.size());

    final CommunicationRegistration resultCommRegistration = result.get(0);
    assertNotNull(resultCommRegistration);
    assertEquals(registrationId, resultCommRegistration.getId());
    assertEquals(channelId, resultCommRegistration.getChannelId());
    assertEquals(communicationId, resultCommRegistration.getCommunicationId());

    final Date expectedRegisteredDate = new Date();
    final Long expectedRegisteredInMiliseconds = expectedRegisteredDate.getTime();
    // comparison of the time leaves space for the test duration and the fact that we do not store dates in miliseconds
    // in database.
    // 2 seconds difference should be sufficient to have confidence that the date is correct
    assertEquals(expectedRegisteredInMiliseconds, resultCommRegistration.getRegistered().getTime(), 2000);
  }

  @Test
  public void findWhenFilterUpcoming() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.UPCOMING);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    final Long registrationId = insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.UPCOMING);
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertNotNull(result.get(0));
    assertEquals(registrationId, result.get(0).getId());
  }

  @Test
  public void findWhenFilterUpcomingWhenNoneCreated() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.RECORDED);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.UPCOMING);
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void findWhenFilterLive() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.LIVE);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    final Long registrationId = insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.LIVE);
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertNotNull(result.get(0));
    assertEquals(registrationId, result.get(0).getId());
  }

  @Test
  public void findWhenFilterLiveWhenNoneCreated() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.UPCOMING);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.LIVE);
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void findWhenFilterRecorded() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.RECORDED);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    final Long registrationId = insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.RECORDED);
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertNotNull(result.get(0));
    assertEquals(registrationId, result.get(0).getId());
  }

  @Test
  public void findWhenFilterRecordedWhenNoneCreated() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.UPCOMING);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.RECORDED);
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void findWhenFilterForValidChannel() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    final Long registrationId = insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);
    criteria.setChannelIds(channelIds);
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertNotNull(result.get(0));
    assertEquals(registrationId, result.get(0).getId());
  }

  @Test
  public void findWhenFilterForInvalidChannel() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId + 1);
    criteria.setChannelIds(channelIds);
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void findWhenFilterForStatusAndChannel() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication = buildCommunication(channelId);
    communication.setStatus(Status.RECORDED);
    final Long communicationId = addTemporaryCommunication(channelId, communication);

    final Long registrationId = insertCommunicationRegistration(channelId, communicationId, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.RECORDED);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);
    criteria.setChannelIds(channelIds);
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertNotNull(result.get(0));
    assertEquals(registrationId, result.get(0).getId());
  }

  @Test
  public void findInCorrectOrder() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication1 = buildCommunication(channelId);
    final Long communicationId1 = addTemporaryCommunication(channelId, communication1);

    final Communication communication2 = buildCommunication(channelId);
    final Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    final Long registrationId1 = insertCommunicationRegistration(channelId, communicationId1, USER_ID);
    final Long registrationId2 = insertCommunicationRegistration(channelId, communicationId2, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(5);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertNotNull(result.get(0));
    assertNotNull(result.get(1));
    assertEquals(registrationId2, result.get(0).getId());
    assertEquals(registrationId1, result.get(1).getId());
  }

  @Test
  public void findWithTwoRegistrationsPageSizeOfOne() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication1 = buildCommunication(channelId);
    final Long communicationId1 = addTemporaryCommunication(channelId, communication1);

    final Communication communication2 = buildCommunication(channelId);
    final Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    insertCommunicationRegistration(channelId, communicationId1, USER_ID);
    final Long registrationId2 = insertCommunicationRegistration(channelId, communicationId2, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(1);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertNotNull(result.get(0));
    assertEquals(registrationId2, result.get(0).getId());
  }

  @Test
  public void findWithTwoRegistrationsPageNumberTwo() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication1 = buildCommunication(channelId);
    final Long communicationId1 = addTemporaryCommunication(channelId, communication1);

    final Communication communication2 = buildCommunication(channelId);
    final Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    final Long registrationId1 = insertCommunicationRegistration(channelId, communicationId1, USER_ID);
    insertCommunicationRegistration(channelId, communicationId2, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(2);
    criteria.setPageSize(1);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertNotNull(result.get(0));
    assertEquals(registrationId1, result.get(0).getId());
  }

  @Test
  public void findWithTwoRegistrationsPageNumberThree() {

    final Long channelTypeId = addTemporaryChannelType("defaultChannelType", true, null);
    final Long channelId = addTemporaryChannel(channelTypeId, channelTypeId, 99L, true);

    final Communication communication1 = buildCommunication(channelId);
    final Long communicationId1 = addTemporaryCommunication(channelId, communication1);

    final Communication communication2 = buildCommunication(channelId);
    final Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    insertCommunicationRegistration(channelId, communicationId1, USER_ID);
    insertCommunicationRegistration(channelId, communicationId2, USER_ID);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(3);
    criteria.setPageSize(1);

    // do test
    List<CommunicationRegistration> result = uut.findRegistrations(USER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  private Long insertCommunicationRegistration(final Long channelId, final Long communicationId, final Long userId) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("communication_id", communicationId);
    params.addValue("user_id", userId);
    params.addValue("is_active", true);
    params.addValue("embed_url", "embedUrl");
    params.addValue("embed_track", "embedTrack");
    params.addValue("notification_status", "not-sent");

    Date now = new Date();
    // each registration is created on different time (one minute difference between subsequent registrations)
    Date created = DateUtils.addMinutes(now, temporaryRegistrationIds.size());
    params.addValue("created", created);
    params.addValue("last_updated", new Date());

    Number primaryKey = simpleJdbcInsertCommunicationRegistration.executeAndReturnKey(params);
    Long registrationId = primaryKey.longValue();
    temporaryRegistrationIds.add(registrationId);

    return registrationId;
  }
}