/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: AbstractJdbcDbDaoTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.builder.ChannelBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationStatisticsBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationAuthorisationStatus;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationType;
import com.brighttalk.service.channel.integration.database.data.ScheduledPublicationData;

public class AbstractJdbcDbDaoTest {

  protected static final String CUSTOM_CHANNEL_TYPE_TEST = "customType";

  protected static final String ENTERPRISE_CHANNEL_TYPE_TEST = "enterpriseType";

  protected static final String DEFAULT_CHANNEL_TYPE = "defaultType";

  protected static final String RESOURCE_TITLE = "Resource Test Title";

  protected static final String RESOURCE_TITLE_2 = "Resource Test Title 2";

  protected static final String RESOURCE_DESCRIPTION = "Resource Test Description";

  protected static final String RESOURCE_DESCRIPTION_2 = "Resource Test Description 2";

  protected static final String RESOURCE_URL = "http://test.brighttalk.net/resource_005.pdf";

  protected static final String RESOURCE_URL_2 = "http://test.brighttalk.net/resource_006.pdf";

  protected static final Long RESOURCE_SIZE = 1000L;

  protected static final String RESOURCE_MIME_TYPE = "application/pdf";

  protected static final String RESOURCE_HOSTED_INTERNAL = "internal";

  protected static final String RESOURCE_HOSTED_EXTERNAL = "external";

  protected static final Long RESOURCE_SIZE_2 = 2000L;

  protected static final String RESOURCE_MIME_TYPE_2 = "application/xml";

  protected static final String CHANNEL_TITLE = "Channel Test Title";

  protected static final String CHANNEL_KEYWORDS = "Channel, Test, Keywords";

  protected static final List<String> CHANNEL_KEYWORDS_LIST = Arrays.asList(CHANNEL_KEYWORDS.split(","));

  protected static final String CHANNEL_ORGANISATION = "Channel Test Organisation";

  protected static final String CHANNEL_STRAPLINE = "Channel Test Strapline";

  protected static final String CHANNEL_DESCRIPTION = "Channel Test description";

  protected static final Long CHANNEL_USER_ID = 999998L;

  protected static final Long USER_ID = 50221L;

  protected static final long CHANNEL_VIEWED_FOR = 999998;

  protected static final String CHANNEL_REFERAL = "dotcom";

  protected static final Long CHANNEL_TYPE_ID = 2L;

  protected static final Long CHANNEL_CREATED_TYPE_ID = 2L;

  protected static final String CHANNEL_IS_SEARCHABLE = "included";

  protected static final String FEATURE_NAME = "allowed_realms";

  protected static final String FEATURE_VALUE = "feature value";

  protected static final String CATEGORY_NAME = "Category name";

  protected static final Long SURVEY_ID = 5021L;

  protected static final Long MASTER_CHANNEL_ID = 501L;

  protected static final String SESSION_ID = "sessionId";

  protected List<Long> temporaryCommunicationIds = new ArrayList<Long>();

  protected List<Long> temporaryResourceIds = new ArrayList<Long>();

  protected List<Long> temporaryChannelIds = new ArrayList<Long>();

  protected List<Long> temporaryScheduledPublications = new ArrayList<Long>();

  protected List<Long> temporaryChannelTypeIds = new ArrayList<Long>();

  protected List<Long> temporaryFeatureTypeIds = new ArrayList<Long>();

  protected List<Long> temporaryRenditionIds = new ArrayList<Long>();

  protected JdbcTemplate jdbcTemplate;

  protected SimpleJdbcInsert simpleJdbcInsertResource;

  protected SimpleJdbcInsert simpleJdbcInsertCommunication;

  protected SimpleJdbcInsert simpleJdbcInsertCommunicationAsset;

  protected SimpleJdbcInsert simpleJdbcInsertCommunicationRenditionAsset;

  protected SimpleJdbcInsert simpleJdbcInsertAsset;

  protected SimpleJdbcInsert simpleJdbcInsertDescription;

  protected SimpleJdbcInsert simpleJdbcInsertConfiguration;

  protected SimpleJdbcInsert simpleJdbcInsertPresenter;

  protected SimpleJdbcInsert simpleJdbcInsertCommunicationStatistics;

  protected SimpleJdbcInsert simpleJdbcInsertCommunicationDetailsOverride;

  protected SimpleJdbcInsert simpleJdbcInsertCommunicationRegistration;

  protected SimpleJdbcInsert simpleJdbcInsertSubscribed;

  protected SimpleJdbcInsert simpleJdbcInsertSubscriptionContext;

  protected SimpleJdbcInsert simpleJdbcInsertScheduled;

  protected SimpleJdbcInsert simpleJdbcInsertChannelStatistics;

  protected SimpleJdbcInsert simpleJdbcInsertChannel;

  protected SimpleJdbcInsert simpleJdbcInsertChannelType;

  protected SimpleJdbcInsert simpleJdbcInsertFeatureType;

  protected SimpleJdbcInsert simpleJdbcInsertChannelFeature;

  protected SimpleJdbcInsert simpleJdbcInsertChannelCategory;

  protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  public AbstractJdbcDbDaoTest() {
  }

  @Autowired
  public void setDataSource(final DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);
    namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    simpleJdbcInsertResource = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertResource.withTableName("resource");
    simpleJdbcInsertResource.usingGeneratedKeyColumns("id");
    simpleJdbcInsertResource.usingColumns("title", "url", "type", "size", "mime_type", "hosted", "precedence",
        "created", "last_updated", "is_active");

    simpleJdbcInsertDescription = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertDescription.withTableName("resource_description");
    simpleJdbcInsertDescription.usingColumns("id", "description");

    simpleJdbcInsertCommunication = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertCommunication.withTableName("communication");
    simpleJdbcInsertCommunication.usingGeneratedKeyColumns("id");
    simpleJdbcInsertCommunication.usingColumns("master_channel_id", "title", "description", "keywords", "presenter",
        "timezone", "status", "publish_status", "scheduled", "created", "last_updated", "rerun_count", "provider",
        "format", "duration", "pin_number");

    simpleJdbcInsertConfiguration = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertConfiguration.withTableName("communication_configuration");
    simpleJdbcInsertConfiguration.usingColumns("channel_id", "communication_id", "visibility", "allow_anon_viewings",
        "is_master", "survey_id", "survey_active", "show_channel_survey", "custom_url",
        "exclude_from_channel_capacity", "client_booking_reference", "syndication_type", "syndication_auth_master",
        "syndication_auth_master_timestamp", "syndication_auth_consumer", "syndication_auth_consumer_timestamp",
        "created", "last_updated");

    simpleJdbcInsertPresenter = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertPresenter.withTableName("can_present");
    simpleJdbcInsertPresenter.usingGeneratedKeyColumns("id");
    simpleJdbcInsertPresenter.usingColumns("session_id", "communication_id", "is_active", "created", "last_updated");

    simpleJdbcInsertCommunicationStatistics = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertCommunicationStatistics.withTableName("communication_statistics");

    simpleJdbcInsertCommunicationDetailsOverride = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertCommunicationDetailsOverride.withTableName("communication_details_override");

    simpleJdbcInsertCommunicationRegistration = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertCommunicationRegistration.withTableName("communication_registration");

    simpleJdbcInsertCommunicationAsset = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertCommunicationAsset.withTableName("communication_asset");
    simpleJdbcInsertCommunicationAsset.usingColumns("communication_id", "target_id", "type", "created", "last_updated",
        "is_active");

    simpleJdbcInsertChannel = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertChannel.withTableName("channel");
    simpleJdbcInsertChannel.usingGeneratedKeyColumns("id");
    simpleJdbcInsertChannel.usingColumns("title", "description", "keywords", "organisation", "strapline",
        "channel_type_id", "created_channel_type_id", "pending_channel_type_id", "is_searchable",
        "promote_on_homepage", "owner_user_id", "viewed_for", "rating", "created", "last_updated", "is_active");

    simpleJdbcInsertChannelType = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertChannelType.withTableName("channel_type");
    simpleJdbcInsertChannelType.usingGeneratedKeyColumns("id");
    simpleJdbcInsertChannelType.usingColumns("name", "is_enabled", "required_role", "is_default", "email_group",
        "created", "last_modified", "is_active");

    simpleJdbcInsertSubscribed = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertSubscribed.withTableName("subscription");
    simpleJdbcInsertSubscribed.usingGeneratedKeyColumns("id");
    simpleJdbcInsertSubscribed.usingColumns("user_id", "channel_id", "referal", "is_active", "last_subscribed",
        "created", "last_updated");

    simpleJdbcInsertSubscriptionContext = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertSubscriptionContext.withTableName("subscription_context");
    simpleJdbcInsertSubscriptionContext.usingColumns("subscription_id", "lead_type", "lead_context",
        "engagement_score", "created", "last_updated");

    simpleJdbcInsertScheduled = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertScheduled.withTableName("scheduled_publication");
    simpleJdbcInsertScheduled.usingColumns("communication_id", "scheduled", "job_id", "completed", "last_updated",
        "created", "publish_status", "is_active");

    simpleJdbcInsertChannelStatistics = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertChannelStatistics.withTableName("channel_statistics");
    simpleJdbcInsertChannelStatistics.usingColumns("channel_id", "upcoming_count", "recorded_count", "live_count",
        "subscriber_count");

    simpleJdbcInsertAsset = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertAsset.withTableName("asset");
    simpleJdbcInsertAsset.usingColumns("channel_id", "target_id", "type", "created", "last_updated", "is_active");

    simpleJdbcInsertFeatureType = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertFeatureType.withTableName("feature_type");
    simpleJdbcInsertFeatureType.usingColumns("name", "channel_type_id", "default_value", "default_enabled", "created",
        "last_updated");

    simpleJdbcInsertChannelFeature = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertChannelFeature.withTableName("feature_override");
    simpleJdbcInsertChannelFeature.usingColumns("channel_id", "name", "override_enabled", "override_value", "is_active");

    simpleJdbcInsertChannelCategory = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertChannelCategory.withTableName("category");
    simpleJdbcInsertChannelCategory.usingGeneratedKeyColumns("id");
    simpleJdbcInsertChannelCategory.usingColumns("category", "channel_id", "is_active", "created", "last_updated");

    simpleJdbcInsertCommunicationRenditionAsset = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertCommunicationRenditionAsset.withTableName("communication_rendition_asset");
    simpleJdbcInsertCommunicationRenditionAsset.usingGeneratedKeyColumns("id");
    simpleJdbcInsertCommunicationRenditionAsset.usingColumns("is_active", "url", "container", "codecs", "width",
        "bitrate", "created", "last_updated");

  }

  @After
  public void tearDown() {

    for (Long channelId : temporaryChannelIds) {

      jdbcTemplate.update("DELETE FROM channel WHERE id = ?", channelId);
      jdbcTemplate.update("DELETE FROM channel_statistics WHERE channel_id = ?", channelId);
      jdbcTemplate.update("DELETE FROM subscription_context WHERE subscription_id IN ("
          + "SELECT id FROM subscription WHERE channel_id = ?)", channelId);
      jdbcTemplate.update("DELETE FROM subscription WHERE channel_id = ?", channelId);
      jdbcTemplate.update("DELETE FROM asset WHERE channel_id = ?", channelId);
      jdbcTemplate.update("DELETE FROM feature_override WHERE channel_id = ?", channelId);
      jdbcTemplate.update("DELETE FROM category WHERE channel_id = ?", channelId);

      for (Long communicationId : temporaryCommunicationIds) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("channelId", channelId);
        params.addValue("communicationId", communicationId);

        String sql = "DELETE FROM asset WHERE channel_id = :channelId AND target_id = :communicationId";
        namedParameterJdbcTemplate.update(sql, params);

        String sql2 =
            "DELETE FROM communication_statistics WHERE channel_id = :channelId AND communication_id = :communicationId";
        namedParameterJdbcTemplate.update(sql2, params);

        String sql3 =
            "DELETE FROM communication_details_override WHERE channel_id = :channelId AND communication_id = :communicationId";
        namedParameterJdbcTemplate.update(sql3, params);

        String sql4 =
            "DELETE FROM communication_registration WHERE channel_id = :channelId AND communication_id = :communicationId";
        namedParameterJdbcTemplate.update(sql4, params);
      }
    }

    for (Long channelTypeId : temporaryChannelTypeIds) {

      jdbcTemplate.update("DELETE FROM channel_type WHERE id = ?", channelTypeId);
      jdbcTemplate.update("DELETE FROM feature_type WHERE channel_type_id = ?", channelTypeId);
    }

    for (Long scheduledCommId : temporaryScheduledPublications) {
      jdbcTemplate.update("DELETE FROM scheduled_publication WHERE communication_id = ?", scheduledCommId);
    }

    for (Long communicationId : temporaryCommunicationIds) {
      jdbcTemplate.update("DELETE FROM communication WHERE id = ?", communicationId);
      jdbcTemplate.update("DELETE FROM communication_configuration WHERE communication_id = ?", communicationId);
      jdbcTemplate.update("DELETE FROM can_present WHERE communication_id = ?", communicationId);
      jdbcTemplate.update("DELETE FROM communication_asset WHERE communication_id = ?", communicationId);
      jdbcTemplate.update("DELETE FROM communication_category WHERE communication_id = ?", communicationId);
    }

    for (Long resourceId : temporaryResourceIds) {
      jdbcTemplate.update("DELETE FROM resource WHERE id = ?", resourceId);
      jdbcTemplate.update("DELETE FROM resource_description WHERE id = ?", resourceId);
    }

    for (Long renditionId : temporaryRenditionIds) {
      jdbcTemplate.update("DELETE FROM communication_rendition_asset WHERE id = ?", renditionId);
    }

  }

  protected Long addTemporaryChannel(final Long channelTypeId, final boolean isActive) {
    return addTemporaryChannel(channelTypeId, channelTypeId, channelTypeId, isActive);
  }

  protected Long addTemporaryChannel(final Long channelTypeId, final Long createdChannelTypeId,
      final Long pendingChannelTypeId, final boolean isActive) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("title", CHANNEL_TITLE);
    params.addValue("description", CHANNEL_DESCRIPTION);
    params.addValue("keywords", CHANNEL_KEYWORDS);
    params.addValue("organisation", CHANNEL_ORGANISATION);
    params.addValue("strapline", CHANNEL_STRAPLINE);
    params.addValue("channel_type_id", channelTypeId);
    params.addValue("created_channel_type_id", createdChannelTypeId);
    params.addValue("pending_channel_type_id", pendingChannelTypeId);
    params.addValue("is_searchable", CHANNEL_IS_SEARCHABLE);
    params.addValue("promote_on_homepage", false);
    params.addValue("owner_user_id", CHANNEL_USER_ID);
    params.addValue("viewed_for", CHANNEL_VIEWED_FOR);
    params.addValue("rating", 2.0f);

    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    params.addValue("is_active", isActive);

    Number primaryKey = simpleJdbcInsertChannel.executeAndReturnKey(params);

    Long channelId = primaryKey.longValue();
    temporaryChannelIds.add(channelId);
    return channelId;
  }

  protected Long addTemporaryChannel(final Channel channel) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("title", channel.getTitle());
    params.addValue("description", channel.getDescription());
    params.addValue("keywords", channel.getKeywords());
    params.addValue("organisation", channel.getOrganisation());
    params.addValue("strapline", channel.getStrapline());
    params.addValue("channel_type_id", channel.getType().getId());
    params.addValue("created_channel_type_id", channel.getCreatedType().getId());
    params.addValue("pending_channel_type_id", channel.getPendingType().getId());
    params.addValue("is_searchable", CHANNEL_IS_SEARCHABLE);
    params.addValue("promote_on_homepage", channel.isPromotedOnHomepage());
    params.addValue("owner_user_id", channel.getOwnerUserId());
    if (channel.hasStatistics()) {
      params.addValue("viewed_for", channel.getStatistics().getViewedFor());
      params.addValue("rating", channel.getStatistics().getRating());
    }

    // start date with 1970-01-01 01:00:00
    // and increment by 1 second with each new channel to make the times unique
    Integer dateBase = 1000 * temporaryChannelIds.size();
    params.addValue("created", new Date(dateBase));
    params.addValue("last_updated", new Date(dateBase));
    params.addValue("is_active", channel.isActive());

    Number primaryKey = simpleJdbcInsertChannel.executeAndReturnKey(params);

    Long channelId = primaryKey.longValue();
    temporaryChannelIds.add(channelId);
    return channelId;
  }

  protected Long addTemporaryChannelType(final ChannelType channelType, final boolean isActive) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("name", channelType.getName());
    params.addValue("is_enabled", channelType.isEnabled());
    params.addValue("is_default", channelType.isDefault());
    params.addValue("email_group", channelType.getEmailGroup());
    params.addValue("is_active", isActive);
    params.addValue("created", new Date());
    params.addValue("last_modified", new Date());

    Number primaryKey = simpleJdbcInsertChannelType.executeAndReturnKey(params);

    Long channelTypeId = primaryKey.longValue();
    temporaryChannelTypeIds.add(channelTypeId);

    return channelTypeId;
  }

  protected Long addTemporaryChannelType(final String name, final boolean isDefault) {
    return addTemporaryChannelType(name, isDefault, null);
  }

  protected Long addTemporaryChannelType(final String name, final boolean isDefault, final String emailGroup) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("name", name);
    params.addValue("is_enabled", 1);
    params.addValue("required_role", User.Role.USER.name().toLowerCase());

    params.addValue("is_default", isDefault);
    params.addValue("email_group", emailGroup);

    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_modified", new Date());
    params.addValue("is_active", 1);

    Number primaryKey = simpleJdbcInsertChannelType.executeAndReturnKey(params);

    Long channelTypeId = primaryKey.longValue();
    temporaryChannelTypeIds.add(channelTypeId);

    return channelTypeId;
  }

  protected Long addTemporaryChannelWithTypes() {

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long customChannelTypeId = addTemporaryChannelType(CUSTOM_CHANNEL_TYPE_TEST, false, CUSTOM_CHANNEL_TYPE_TEST);
    Long enterpriseChannelTypeId = addTemporaryChannelType(ENTERPRISE_CHANNEL_TYPE_TEST, false,
        ENTERPRISE_CHANNEL_TYPE_TEST);
    return addTemporaryChannel(customChannelTypeId, defaultChannelTypeId, enterpriseChannelTypeId, true);
  }

  protected Long addTemporaryInactiveChannelWithTypes() {

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long customChannelTypeId = addTemporaryChannelType(CUSTOM_CHANNEL_TYPE_TEST, false, CUSTOM_CHANNEL_TYPE_TEST);
    Long enterpriseChannelTypeId = addTemporaryChannelType(ENTERPRISE_CHANNEL_TYPE_TEST, false,
        ENTERPRISE_CHANNEL_TYPE_TEST);
    return addTemporaryChannel(customChannelTypeId, defaultChannelTypeId, enterpriseChannelTypeId, false);

  }

  protected Long addChannelSubscription(final Long channelId, final Long userId, final boolean isActive) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("user_id", userId);
    params.addValue("channel_id", channelId);
    params.addValue("referal", CHANNEL_REFERAL);
    params.addValue("is_active", isActive);

    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    params.addValue("last_subscribed", new Date());
    Long subscriptionId = simpleJdbcInsertSubscribed.executeAndReturnKey(params).longValue();

    temporaryChannelIds.add(channelId);

    return subscriptionId;
  }

  protected void addSubscriptionContext(final Long subscriptionId, final SubscriptionLeadType leadType,
      final String leadContext, final Integer engagementScore) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("subscription_id", subscriptionId);
    params.addValue("lead_type", leadType.getType());
    params.addValue("lead_context", leadContext);
    params.addValue("engagement_score", engagementScore);
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    simpleJdbcInsertSubscriptionContext.execute(params);
  }

  protected void addTemporaryScheduledPublication(final ScheduledPublicationData data) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communication_id", data.getCommunicationId());
    params.addValue("scheduled", data.getScheduledDate());
    params.addValue("job_id", data.getJobId());
    params.addValue("publish_status", data.getPublishStatus().toString().toLowerCase());
    params.addValue("completed", data.isCompleted());
    params.addValue("is_active", data.isActive());
    params.addValue("last_updated", new Date());
    params.addValue("created", new Date());
    simpleJdbcInsertScheduled.execute(params);

    temporaryScheduledPublications.add(data.getCommunicationId());
  }

  protected void addChannelStatistics(final Long channelId, final Long upcoming, final Long recorded, final Long live,
      final Long subscribers) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("upcoming_count", upcoming == null ? 0 : upcoming);
    params.addValue("recorded_count", recorded == null ? 0 : recorded);
    params.addValue("live_count", live == null ? 0 : live);
    params.addValue("subscriber_count", subscribers == null ? 0 : subscribers);

    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    // params.addValue("last_subscribed", new Date());
    simpleJdbcInsertChannelStatistics.execute(params);

    temporaryChannelIds.add(channelId);
  }

  protected void addChannelFeatures(final Long channelId, final String name, final boolean isOverridedEnabled) {
    addChannelFeatures(channelId, name, isOverridedEnabled, null);
  }

  protected void addChannelFeatures(final Long channelId, final String name, final boolean isOverridedEnabled,
      final String overrideValue) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("name", name);
    params.addValue("override_enabled", isOverridedEnabled);
    params.addValue("override_value", overrideValue);

    params.addValue("is_active", 1);

    simpleJdbcInsertChannelFeature.execute(params);
  }

  protected void addFeatures(final Long channelTypeId, final String name) {
    addFeatures(channelTypeId, name, null);
  }

  protected void addFeatures(final Long channelTypeId, final String name, final String defaultValue) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("name", name);
    params.addValue("channel_type_id", channelTypeId);
    params.addValue("default_value", defaultValue);
    params.addValue("default_enabled", 1);

    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    simpleJdbcInsertFeatureType.execute(params);
  }

  protected void addCategories(final Long channelId, final String name) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("category", name);
    params.addValue("channel_id", channelId);
    params.addValue("is_active", 1);

    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    simpleJdbcInsertChannelCategory.execute(params);
  }

  protected void addTemporarySurveyAsset(final Long channelId, final Long surveyId, final boolean isActive) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("target_id", surveyId);
    params.addValue("type", "survey");
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    params.addValue("is_active", isActive);
    simpleJdbcInsertAsset.execute(params);

    temporaryChannelIds.add(channelId);
  }

  protected void addCommunicationConfiguration(final Communication communication) {
    addCommunicationConfiguration(communication.getChannelId(), communication);
  }

  protected void addCommunicationConfiguration(final long channelId, final Communication communication) {
    CommunicationConfiguration configuration = communication.getConfiguration();

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("communication_id", communication.getId());
    params.addValue("visibility", configuration.getVisibility().name().toLowerCase());
    params.addValue("allow_anon_viewings", configuration.allowAnonymousViewings());
    params.addValue("is_master", configuration.isInMasterChannel());
    params.addValue("survey_id", configuration.getSurveyId());
    params.addValue("survey_active", configuration.isSurveyActive());
    params.addValue("show_channel_survey", configuration.showChannelSurvey());
    params.addValue("custom_url", configuration.getCustomUrl());
    params.addValue("exclude_from_channel_capacity", configuration.excludeFromChannelContentPlan());
    params.addValue("client_booking_reference", configuration.getClientBookingReference());

    CommunicationSyndication communicationSyndication = configuration.getCommunicationSyndication();

    if (communicationSyndication.isSyndicated()) {
      params.addValue("syndication_type", communicationSyndication.getSyndicationType().name().toLowerCase());

      if (communicationSyndication.isSyndicatedIn()) {
        params.addValue("syndication_auth_master",
            communicationSyndication.getMasterChannelSyndicationAuthorisationStatus().name().toLowerCase());
        params.addValue("syndication_auth_master_timestamp", new Date());
        params.addValue("syndication_auth_consumer",
            communicationSyndication.getConsumerChannelSyndicationAuthorisationStatus().name().toLowerCase());
        params.addValue("syndication_auth_consumer_timestamp", new Date());
      }
    }

    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    simpleJdbcInsertConfiguration.execute(params);

  }

  /**
   * 
   * @param communicationId
   * @param channelId
   * @param syndicationType
   * @return
   * 
   * @deprecated
   */
  @Deprecated
  protected Long addCommunicationConfiguration(final Long communicationId, final Long channelId,
      final CommunicationSyndication.SyndicationType syndicationType) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("communication_id", communicationId);
    params.addValue("visibility", "public");
    params.addValue("is_master", 1);
    params.addValue("allow_anon_viewings", 1);
    params.addValue("survey_active", 1);
    params.addValue("show_channel_survey", 1);
    params.addValue("exclude_from_channel_capacity", 0);
    params.addValue("scheduled", new Date());
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    if (syndicationType != null) {

      params.addValue("syndication_type", syndicationType.toString().toLowerCase());
    }

    simpleJdbcInsertConfiguration.execute(params);
    return communicationId;
  }

  protected Long addPresenterSession(final Long communicationId, final String sessionId) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("session_id", sessionId);
    params.addValue("communication_id", communicationId);
    params.addValue("is_active", 1);
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    simpleJdbcInsertPresenter.execute(params);
    return communicationId;
  }

  protected Communication buildCommunication(final Long channelId) {

    Provider provider = new Provider(Provider.BRIGHTTALK);

    Communication communication = new Communication();
    communication.setChannelId(channelId);
    communication.setMasterChannelId(channelId);
    communication.setTitle("title");
    communication.setDescription("desc");
    communication.setKeywords("one,two,three");
    communication.setAuthors("mr. junit");
    communication.setDuration(1234);
    communication.setTimeZone("Europe/London");
    communication.setStatus(Communication.Status.UPCOMING);
    communication.setPublishStatus(Communication.PublishStatus.PUBLISHED);
    communication.setFormat(Communication.Format.AUDIO);
    communication.setProvider(provider);
    communication.setScheduledDateTime(new Date());
    communication.setBookingDuration(300);
    communication.setPinNumber("01234567");

    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(true);
    configuration.setVisibility(CommunicationConfiguration.Visibility.PUBLIC);

    CommunicationSyndication communicationSyndication = new CommunicationSyndication();
    configuration.setCommunicationSyndication(communicationSyndication);

    communication.setConfiguration(configuration);
    return communication;
  }

  protected Long addTemporaryCommunication(final Long channelId, final Communication communication) {

    return addTemporaryCommunication(channelId, communication, "communication", true);
  }

  protected Long addTemporaryCommunication(final Long channelId, final Communication communication,
      final String assetType, final boolean assetActive) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("master_channel_id", communication.getMasterChannelId());
    params.addValue("title", communication.getTitle());
    params.addValue("description", communication.getDescription());
    params.addValue("keywords", communication.getKeywords());
    params.addValue("presenter", communication.getAuthors());
    params.addValue("timezone", communication.getTimeZone());
    params.addValue("status", communication.getStatus().name().toLowerCase());
    params.addValue("publish_status", communication.getPublishStatus().name().toLowerCase());
    params.addValue("scheduled", communication.getScheduledDateTime());
    params.addValue("created", communication.getCreated() == null ? new Date() : communication.getCreated());
    params.addValue("last_updated", communication.getLastUpdated() != null ? communication.getLastUpdated()
        : new Date());
    params.addValue("is_active", 1);
    params.addValue("rerun_count", communication.getRerunCount());
    params.addValue("provider", communication.getProvider().getName());
    params.addValue("format", communication.getFormat().name().toLowerCase());
    params.addValue("duration", communication.getDuration() != null ? communication.getDuration() : 0);
    params.addValue("live_phone_number", communication.getLivePhoneNumber());
    params.addValue("live_url", communication.getLiveUrl());

    if (communication.getPinNumber() != null) {
      params.addValue("pin_number", communication.getPinNumber());
    }
    Number primaryKey = simpleJdbcInsertCommunication.executeAndReturnKey(params);

    Long communicationId = primaryKey.longValue();
    communication.setId(communicationId);

    addCommunicationConfiguration(communication);
    addChannelAsset(channelId, communicationId, assetType, assetActive);
    temporaryCommunicationIds.add(communicationId);

    return communicationId;
  }

  protected Long addTemporaryCommunication(final Long channelId, final String provider) {

    Long communicationId = addTemporaryCommunication(channelId, provider, null);
    return communicationId;
  }

  protected Long addTemporaryCommunication(final Long channelId, final String provider,
      final CommunicationSyndication.SyndicationType syndicationType) {

    MapSqlParameterSource params = this.getCommunicationMappedSqlFields(channelId, provider);

    Number primaryKey = simpleJdbcInsertCommunication.executeAndReturnKey(params);
    Long communicationId = primaryKey.longValue();

    addCommunicationConfiguration(communicationId, channelId, syndicationType);

    addChannelAsset(channelId, communicationId, "communication");

    temporaryCommunicationIds.add(communicationId);

    return communicationId;
  }

  protected Long addTemporaryCommunicationWithoutConfiguration(final Long channelId, final String provider) {

    MapSqlParameterSource params = this.getCommunicationMappedSqlFields(channelId, provider);

    Number primaryKey = simpleJdbcInsertCommunication.executeAndReturnKey(params);
    Long communicationId = primaryKey.longValue();

    addChannelAsset(channelId, communicationId, "communication");

    temporaryCommunicationIds.add(communicationId);

    return communicationId;
  }

  protected void addChannelAsset(final Long channelId, final Long communicationId, final String assetType) {
    addChannelAsset(channelId, communicationId, assetType, true);
  }

  protected void addCommunicationChannelAsset(final Long channelId, final Long communicationId) {
    addChannelAsset(channelId, communicationId, "communication");
  }

  protected void addChannelAsset(final Long channelId, final Long communicationId, final String assetType,
      final boolean assetActive) {
    MapSqlParameterSource assetParams = new MapSqlParameterSource();
    assetParams.addValue("channel_id", channelId);
    assetParams.addValue("target_id", communicationId);
    assetParams.addValue("type", assetType);
    assetParams.addValue("is_active", assetActive);
    assetParams.addValue("created", new Date());
    assetParams.addValue("last_updated", new Date());

    if (!temporaryChannelIds.contains(channelId)) {
      temporaryChannelIds.add(channelId);
    }
    simpleJdbcInsertAsset.execute(assetParams);
  }

  protected void addCommunicationResourceAsset(final Long resourceId, final Long communicationId) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communication_id", communicationId);
    params.addValue("target_id", resourceId);
    params.addValue("type", "resource");
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    params.addValue("is_active", 1);
    simpleJdbcInsertCommunicationAsset.execute(params);
  }

  protected void addMediazoneCommunicationAsset(final String url, final String mediazoneId, final Long communicationId,
      final boolean isActive) {

    Date now = new Date();

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communication_id", communicationId);
    params.addValue("target_id", mediazoneId);
    params.addValue("type", "mediazone_webcast");
    params.addValue("created", now);
    params.addValue("last_updated", now);
    params.addValue("is_active", isActive);
    simpleJdbcInsertCommunicationAsset.execute(params);

    params = new MapSqlParameterSource();
    params.addValue("communication_id", communicationId);
    params.addValue("target_id", url);
    params.addValue("type", "mediazone_config");
    params.addValue("created", now);
    params.addValue("last_updated", now);
    params.addValue("is_active", isActive);
    simpleJdbcInsertCommunicationAsset.execute(params);
  }

  protected Long addCommunicationRendition() {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("is_active", 1);
    params.addValue("url", "http://test...");
    params.addValue("container", "container");
    params.addValue("codecs", "codecs");
    params.addValue("width", "45");
    params.addValue("bitrate", "314");
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    Number primaryKey = simpleJdbcInsertCommunicationRenditionAsset.executeAndReturnKey(params);
    Long renditionId = primaryKey.longValue();

    temporaryRenditionIds.add(renditionId);

    return renditionId;

  }

  protected void addCommunicationRenditionAsset(final Long renditionId, final Long communicationId) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communication_id", communicationId);
    params.addValue("target_id", renditionId);
    params.addValue("type", "video");
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    params.addValue("is_active", 1);
    simpleJdbcInsertCommunicationAsset.execute(params);
  }

  protected void addCommunicationVoteAsset(final Long voteId, final Long communicationId) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communication_id", communicationId);
    params.addValue("target_id", voteId);
    params.addValue("type", "vote");
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    params.addValue("is_active", 1);
    simpleJdbcInsertCommunicationAsset.execute(params);
  }

  protected void addResourceDescription(final Long resourceId) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("id", resourceId);
    params.addValue("description", RESOURCE_DESCRIPTION);
    simpleJdbcInsertDescription.execute(params);
  }

  protected Long addTemporaryResource(final boolean isFile) {
    return addTemporaryResource(isFile, true);
  }

  protected Long addTemporaryResource(final boolean isFile, final boolean withDescription) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("title", RESOURCE_TITLE);
    params.addValue("url", RESOURCE_URL);
    if (isFile) {
      params.addValue("type", Resource.Type.FILE.name().toLowerCase());
      params.addValue("size", RESOURCE_SIZE);
      params.addValue("mime_type", RESOURCE_MIME_TYPE);
      params.addValue("hosted", RESOURCE_HOSTED_EXTERNAL);
    } else {
      params.addValue("type", Resource.Type.LINK.name().toLowerCase());
    }

    params.addValue("precedence", 0);
    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    params.addValue("is_active", 1);

    Number primaryKey = simpleJdbcInsertResource.executeAndReturnKey(params);
    Long resourceId = primaryKey.longValue();

    addResourceDescription(resourceId);

    temporaryResourceIds.add(resourceId);
    return resourceId;
  }

  protected List<Channel> createMultipleTemporaryChannelsWithTypes(final int quantity) {

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long customChannelTypeId = addTemporaryChannelType(CUSTOM_CHANNEL_TYPE_TEST, false, CUSTOM_CHANNEL_TYPE_TEST);
    Long enterpriseChannelTypeId = addTemporaryChannelType(ENTERPRISE_CHANNEL_TYPE_TEST, false,
        ENTERPRISE_CHANNEL_TYPE_TEST);

    List<Channel> channels = new ArrayList<Channel>();

    for (int i = 0; i < quantity; i += 1) {

      final ChannelBuilder channelBuilder = new ChannelBuilder().withDefaultValues();
      channelBuilder.withTitle(CHANNEL_TITLE + i);
      channelBuilder.withOwnerUserId(CHANNEL_USER_ID);

      Channel channel = channelBuilder.build();
      channel.getType().setId(customChannelTypeId);
      channel.getCreatedType().setId(defaultChannelTypeId);
      channel.getPendingType().setId(enterpriseChannelTypeId);

      channels.add(channel);
    }

    return channels;
  }

  protected void addCommunicationStatistics(final Communication communication) {
    addCommunicationStatistics(communication.getChannelId(), communication);
  }

  protected void addCommunicationStatistics(final long channelId, final Communication communication) {

    CommunicationStatistics viewingStatistics = communication.getCommunicationStatistics();

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communication_id", communication.getId());
    params.addValue("channel_id", channelId);
    params.addValue("rating", viewingStatistics.getAverageRating());
    params.addValue("num_ratings", viewingStatistics.getNumberOfRatings());
    params.addValue("num_views", viewingStatistics.getNumberOfViewings());
    params.addValue("viewed_for", viewingStatistics.getTotalViewingDuration());

    simpleJdbcInsertCommunicationStatistics.execute(params);

    this.temporaryChannelIds.add(channelId);
  }

  protected void addCommunicationDetailsOverride(final Communication communication) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communication_id", communication.getId());
    params.addValue("channel_id", communication.getChannelId());
    params.addValue("title", communication.getTitle());
    params.addValue("description", communication.getDescription());
    params.addValue("presenter", communication.getAuthors());
    params.addValue("keywords", communication.getKeywords());
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    simpleJdbcInsertCommunicationDetailsOverride.execute(params);
  }

  protected void addCommunicationRegistration(final Long userId, final Communication communication,
      final boolean isActive) {
    addCommunicationRegistration(userId, communication.getChannelId(), communication, isActive);
  }

  protected void addCommunicationRegistration(final Long userId, final long channelId,
      final Communication communication,
      final boolean isActive) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communication_id", communication.getId());
    params.addValue("channel_id", channelId);
    params.addValue("user_id", userId);
    params.addValue("notification_status", "not-sent");
    params.addValue("is_active", isActive);
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    simpleJdbcInsertCommunicationRegistration.execute(params);

    this.temporaryCommunicationIds.add(communication.getId());
    this.temporaryChannelIds.add(channelId);
  }

  protected class CommunicationDetailsOverride {

    public String title;
    public String description;
    public String keywords;
    public String authors;
  }

  protected CommunicationConfiguration buildCommunicationConfiguration(final Long channelId,
      final SyndicationType type,
      boolean masterChannel) {
    CommunicationConfiguration configuration = new CommunicationConfiguration();
    configuration.setInMasterChannel(masterChannel);
    configuration.setVisibility(CommunicationConfiguration.Visibility.PUBLIC);

    CommunicationSyndication communicationSyndication = buildCommunicationSyndication(channelId, type);
    configuration.setCommunicationSyndication(communicationSyndication);

    return configuration;
  }

  protected CommunicationSyndication buildCommunicationSyndication(final Long channelId, SyndicationType type) {
    CommunicationSyndication communicationSyndication = new CommunicationSyndication();
    Channel channel = new Channel(channelId);
    channel.setTitle(CHANNEL_TITLE);
    channel.setOrganisation(CHANNEL_ORGANISATION);
    communicationSyndication.setChannel(channel);
    communicationSyndication.setChannel(new Channel(channelId));
    communicationSyndication.setSyndicationType(type);
    communicationSyndication.setMasterChannelSyndicationAuthorisationStatus(SyndicationAuthorisationStatus.APPROVED);
    communicationSyndication.setMasterChannelSyndicationAuthorisationTimestamp(new Date());
    communicationSyndication.setConsumerChannelSyndicationAuthorisationStatus(SyndicationAuthorisationStatus.APPROVED);
    communicationSyndication.setConsumerChannelSyndicationAuthorisationTimestamp(new Date());
    communicationSyndication.setCommunicationStatistics(new CommunicationStatisticsBuilder().withDefaultValues().
        build());
    return communicationSyndication;
  }

  protected void syndicateCommunication(final long channelId, final Communication communication) {
    CommunicationConfiguration configuration = buildCommunicationConfiguration(channelId, SyndicationType.IN, false);
    communication.setChannelId(channelId);
    communication.setConfiguration(configuration);
    addChannelAsset(channelId, communication.getId(), "communication");
    addCommunicationConfiguration(communication);
  }

  /**
   * Assert the two communication to have the same fields.
   * 
   * @param expectedCommunication The expected communication to assert.
   * @param actualWebcastDto The actual Communication.
   */
  protected void assertAllCommunicationFields(final Communication expectedCommunication,
      final Communication actualCommunication) {
    assertNotNull(expectedCommunication);
    assertNotNull(actualCommunication);
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    assertEquals(expectedCommunication.getId(), actualCommunication.getId());
    assertEquals(expectedCommunication.getChannelId(), actualCommunication.getChannelId());
    assertEquals(expectedCommunication.getTitle(), actualCommunication.getTitle());
    assertEquals(expectedCommunication.getDescription(), actualCommunication.getDescription());
    assertEquals(expectedCommunication.getKeywords(), actualCommunication.getKeywords());
    assertEquals(expectedCommunication.getAuthors(), actualCommunication.getAuthors());
    assertEquals(expectedCommunication.getDuration(), actualCommunication.getDuration());
    assertEquals(expectedCommunication.getTimeZone(), actualCommunication.getTimeZone());
    assertEquals(dateFormat.format(expectedCommunication.getScheduledDateTime()),
        dateFormat.format(actualCommunication.getScheduledDateTime()));
    assertEquals(expectedCommunication.getFormat().name(), actualCommunication.getFormat().name());
  }

  protected void assertAllCommunicationConfigurationDetails(final CommunicationConfiguration expectedConfiguration,
      final CommunicationConfiguration actualConfiguration) {
    assertNotNull(expectedConfiguration);
    assertNotNull(actualConfiguration);

    assertEquals(expectedConfiguration.getChannelId(), actualConfiguration.getChannelId());
    assertEquals(expectedConfiguration.getVisibility().name(), actualConfiguration.getVisibility().name());
    assertEquals(expectedConfiguration.allowAnonymousViewings(), actualConfiguration.allowAnonymousViewings());
    assertEquals(expectedConfiguration.excludeFromChannelContentPlan(),
        actualConfiguration.excludeFromChannelContentPlan());
    assertEquals(expectedConfiguration.showChannelSurvey(), actualConfiguration.showChannelSurvey());
    assertEquals(expectedConfiguration.getClientBookingReference(), actualConfiguration.getClientBookingReference());
  }

  protected void assertAllCommunicationCategoryDetails(final List<CommunicationCategory> expectedCategories,
      final List<CommunicationCategory> actualCategories) {
    assertEquals(expectedCategories.size(), actualCategories.size());
    for (int i = 0; i < expectedCategories.size(); i++) {
      assertEquals(expectedCategories.get(i).getId(), actualCategories.get(i).getId());
    }
  }

  private MapSqlParameterSource getCommunicationMappedSqlFields(final Long channelId, final String provider) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("master_channel_id", channelId);
    params.addValue("title", "test");
    params.addValue("description", "test description");
    params.addValue("keywords", "one,two,three");
    params.addValue("presenter", "test");
    params.addValue("timezone", "Europe/London");
    params.addValue("status", Communication.Status.UPCOMING.name().toLowerCase());
    params.addValue("publish_status", "published");
    params.addValue("scheduled", new Date());
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());
    params.addValue("rerun_count", 0);
    params.addValue("provider", provider);
    params.addValue("pin_number", "01234567");
    params.addValue("format", "video");

    return params;
  }

  protected Date getDateInUTC() {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    return cal.getTime();
  }
}