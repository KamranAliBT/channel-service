/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationDbDaoChannelsWebcastsTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationStatisticsBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration.Visibility;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCommunicationDbDaoChannelsWebcastsTest extends AbstractJdbcDbDaoTest {

  private static final long SUBSCRIBER_ID = 999932l;

  private static final int DEFAULT_PAGE_SIZE = 10;

  private static final int DEFAULT_PAGE_NUMBER = 1;

  private CommunicationsSearchCriteria criteria;

  @Autowired
  private JdbcCommunicationDbDao uut;

  @Before
  public void setup() {
    criteria = new CommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
  }

  @Test
  public void findChannelsWebcasts() {

    Long channelId = 99999L;
    Long communicationId = 999998L;

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
    assertEquals(DEFAULT_PAGE_NUMBER, result.getCurrentPageNumber());
    assertEquals(DEFAULT_PAGE_SIZE, result.getPageSize());
  }

  @Test
  public void findChannelsWebcastsCheckCommunicationDetails() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(channelId, result.get(0).getMasterChannelId());
    assertEquals(CommunicationBuilder.DEFAULT_TITLE, result.get(0).getTitle());
    assertEquals(CommunicationBuilder.DEFAULT_DESCRIPTION, result.get(0).getDescription());
    assertEquals(CommunicationBuilder.DEFAULT_PRESENTER, result.get(0).getAuthors());
    assertEquals(CommunicationBuilder.DEFAULT_KEYWORDS, result.get(0).getKeywords());
    assertEquals(CommunicationBuilder.DEFAULT_DURATION, result.get(0).getDuration());
    assertEquals(CommunicationBuilder.DEFAULT_TIMEZONE, result.get(0).getTimeZone());
    assertEquals(CommunicationBuilder.DEFAULT_STATUS, result.get(0).getStatus());
    assertEquals(CommunicationBuilder.DEFAULT_PUBLISH_STATUS, result.get(0).getPublishStatus());
    assertEquals(Provider.BRIGHTTALK, result.get(0).getProvider().getName());
    assertEquals(CommunicationBuilder.DEFAULT_FORMAT, result.get(0).getFormat());
    assertNotNull(result.get(0).getConfiguration());
    assertEquals(CommunicationConfigurationBuilder.DEFAULT_VISIBILITY, result.get(0).getConfiguration().getVisibility());
    assertTrue(result.get(0).getConfiguration().isInMasterChannel());
  }

  @Test
  public void findChannelsWebcastsCheckOverrides() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    final String expectedTitle = "overridenTitle";
    communication.setTitle(expectedTitle);

    final String expectedDescription = "overridenDescription";
    communication.setDescription(expectedDescription);

    final String expectedPresenter = "overridenPresenter";
    communication.setAuthors(expectedPresenter);

    final String expectedKeywords = "overridenKeywords";
    communication.setKeywords(expectedKeywords);

    addCommunicationDetailsOverride(communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(channelId, result.get(0).getMasterChannelId());
    assertEquals(expectedTitle, result.get(0).getTitle());
    assertEquals(expectedDescription, result.get(0).getDescription());
    assertEquals(expectedPresenter, result.get(0).getAuthors());
    assertEquals(expectedKeywords, result.get(0).getKeywords());
  }

  @Test
  public void findChannelsWebcastsCheckCommunicationStatsWhenNoCreated() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(1, result.size());
    assertNotNull(result.get(0).getCommunicationStatistics());
    assertEquals(0, result.get(0).getCommunicationStatistics().getAverageRating(), 0);
    assertEquals(0, result.get(0).getCommunicationStatistics().getNumberOfViewings(), 0);
  }

  @Test
  public void findChannelsWebcastsCheckCommunicationStats() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    final float expectedRating = 4.3f;
    final int expectedNumberOfViewings = 100;

    CommunicationStatisticsBuilder statsBuilder = new CommunicationStatisticsBuilder().withDefaultValues();
    statsBuilder.withCommunicationId(communicationId).withNumberOfViewings(expectedNumberOfViewings).withAverageRating(
        expectedRating);
    communication.setCommunicationStatistics(statsBuilder.build());

    addCommunicationStatistics(communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(1, result.size());
    assertNotNull(result.get(0).getCommunicationStatistics());
    assertEquals(expectedRating, result.get(0).getCommunicationStatistics().getAverageRating(), 0);
    assertEquals(expectedNumberOfViewings, result.get(0).getCommunicationStatistics().getNumberOfViewings(), 0);
  }

  @Test
  public void findChannelsWebcastsNonPublicVisibility() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();
    communication.getConfiguration().setVisibility(Visibility.PRIVATE);

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsUnpublished() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId).withPublishStatus(PublishStatus.UNPUBLISHED);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsCancelled() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.CANCELLED);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsUpcoming() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.UPCOMING);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(1, result.size());
  }

  @Test
  public void findChannelsWebcastsLive() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.LIVE);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(1, result.size());
  }

  @Test
  public void findChannelsWebcastsRecorded() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.RECORDED);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(1, result.size());
  }

  @Test
  public void findChannelsWebcastsDeleted() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.DELETED);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsSyndicatedOut() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.OUT);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findChannelsWebcastsSyndicatedInApprovedByBoth() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findChannelsWebcastsSyndicatedInApprovedByMasterOnly() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsSyndicatedInApprovedByConsumerOnly() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsSyndicatedInNotApprovedByBoth() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsSyndicatedInPendingBoth() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsSyndicatedInPendingMaster() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsSyndicatedInPendingConsumer() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    criteria.setIds(channelId + "-" + communicationId);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findChannelsWebcastsMultipleCommunicationsSameChannel() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(2000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(1000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    criteria.setIds(channelId + "-" + communicationId1 + "," + channelId + "-" + communicationId2);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(communicationId1, result.get(0).getId());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(communicationId2, result.get(1).getId());
    assertEquals(channelId, result.get(1).getChannelId());
  }

  @Test
  public void findChannelsWebcastsMultipleCommunicationsDifferentChannels() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(2000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    builder.withChannelId(channelId2).withMasterChannelId(channelId2).withScheduledDateTime(new Date(1000));
    Communication communication2 = builder.build();
    Long communicationId2 = addTemporaryCommunication(channelId2, communication2);

    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    criteria.setIds(channelId + "-" + communicationId1 + "," + channelId2 + "-" + communicationId2);
    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(communicationId1, result.get(0).getId());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(communicationId2, result.get(1).getId());
    assertEquals(channelId2, result.get(1).getChannelId());
  }

  @Test
  public void findChannelsWebcastsPageOneSizeOne() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    criteria.setIds(channelId + "-" + communicationId1 + "," + channelId + "-" + communicationId2);
    criteria.setPageNumber(1);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(communicationId1, result.get(0).getId());
  }

  @Test
  public void findChannelsWebcastsPageOneSizeOneHasMore() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    criteria.setIds(channelId + "-" + communicationId1 + "," + channelId + "-" + communicationId2);
    criteria.setPageNumber(1);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertTrue(result.hasNextPage());
  }

  @Test
  public void findChannelsWebcastsPageTwoSizeOne() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    criteria.setIds(channelId + "-" + communicationId1 + "," + channelId + "-" + communicationId2);
    criteria.setPageNumber(2);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(communicationId2, result.get(0).getId());
  }

  @Test
  public void findChannelsWebcastsPageTwoSizeOneHasPreviousPage() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    criteria.setIds(channelId + "-" + communicationId1 + "," + channelId + "-" + communicationId2);
    criteria.setPageNumber(2);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Communication> result = uut.find(criteria);

    assertTrue(result.hasPreviousPage());
  }

}