/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpLiveStateServiceDaoTest.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.notification;

import static org.easymock.EasyMock.isA;

import java.net.URI;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.notification.dto.EventDto;
import com.brighttalk.service.channel.integration.notification.dto.converter.NotificationRequestConverter;

public class XmlHttpNotificationServiceDaoTest {

  private static final String SERVICE_BASE_URL = "http://test.notification.brighttalk.net";

  private XmlHttpNotificationServiceDao uut;

  private RestTemplate mockRestTemplate;

  private NotificationRequestConverter mockConverter;

  @Before
  public void setUp() {

    uut = new XmlHttpNotificationServiceDao();
    uut.setServiceBaseUrl(SERVICE_BASE_URL);

    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    uut.setRestTemplate(mockRestTemplate);

    mockConverter = EasyMock.createMock(NotificationRequestConverter.class);
    uut.setConverter(mockConverter);
  }

  @Test
  public void inform() throws Exception {
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    EventDto eventDto = new EventDto();

    EasyMock.expect(mockConverter.convert(communication)).andReturn(eventDto);

    EasyMock.expect(mockRestTemplate.postForLocation(isA(String.class), isA(EventDto.class))).andReturn(new URI(""));

    EasyMock.replay(mockRestTemplate, mockConverter);

    // do test
    uut.inform(communication);

    EasyMock.verify(mockRestTemplate, mockConverter);
  }

}