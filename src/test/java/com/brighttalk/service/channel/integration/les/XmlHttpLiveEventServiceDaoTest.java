/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpLiveStateServiceDaoTest.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.les;

import static org.easymock.EasyMock.isA;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.les.dto.CancelCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.RescheduleCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.converter.LiveEventRequestConverter;

public class XmlHttpLiveEventServiceDaoTest {

  private static final String SERVICE_BASE_URL = "http://test.les.brighttalk.net";

  private XmlHttpLiveEventServiceDao uut;

  private RestTemplate mockRestTemplate;

  private LiveEventRequestConverter mockConverter;

  private MappingJacksonHttpMessageConverter mockMessageConverter;

  @Before
  public void setUp() {

    uut = new XmlHttpLiveEventServiceDao();
    uut.setServiceBaseUrl(SERVICE_BASE_URL);

    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    uut.setRestTemplate(mockRestTemplate);

    mockConverter = EasyMock.createMock(LiveEventRequestConverter.class);
    uut.setConverter(mockConverter);

    mockMessageConverter = EasyMock.createMock(MappingJacksonHttpMessageConverter.class);
    uut.setMessageConverter(mockMessageConverter);
  }

  @Test
  public void reschedule() throws Exception {
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    RescheduleCommunicationDto rescheduleCommunicationDto = new RescheduleCommunicationDto();
    List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
    messageConverters.add(mockMessageConverter);

    EasyMock.expect(mockConverter.convertForReschedule(communication)).andReturn(rescheduleCommunicationDto);

    mockRestTemplate.setMessageConverters(messageConverters);
    EasyMock.expectLastCall();

    EasyMock.expect(mockRestTemplate.postForLocation(isA(String.class), isA(RescheduleCommunicationDto.class))).andReturn(
        new URI(""));

    EasyMock.replay(mockRestTemplate, mockConverter);

    // do test
    uut.reschedule(communication);

    EasyMock.verify(mockRestTemplate, mockConverter);
  }

  @Test
  public void cancel() throws Exception {
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    CancelCommunicationDto rescheduleCommunicationDto = new CancelCommunicationDto();
    List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
    messageConverters.add(mockMessageConverter);

    EasyMock.expect(mockConverter.convertForCancel(communication)).andReturn(rescheduleCommunicationDto);

    mockRestTemplate.setMessageConverters(messageConverters);
    EasyMock.expectLastCall();

    EasyMock.expect(mockRestTemplate.postForLocation(isA(String.class), isA(CancelCommunicationDto.class))).andReturn(
        new URI(""));

    EasyMock.replay(mockRestTemplate, mockConverter);

    // do test
    uut.cancel(communication);

    EasyMock.verify(mockRestTemplate, mockConverter);
  }

}