/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: WebcastAuditRecordDtoBuilderTest.java 97400 2015-07-01 08:36:42Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit.dto.builder;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.WebcastAuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder.AuditAction;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder.AuditEntity;

/**
 * Tests for {@link WebcastAuditRecordDtoBuilder}.
 */
public class WebcastAuditRecordDtoBuilderTest {

  private static final Long USER_ID = 1L;
  private static final Long CHANNEL_ID = 2L;
  private static final Long COMMUNICATION_ID = 3L;
  private static final String ACTION_DESCRIPTION = "Test audit description 123...";

  private AuditRecordDtoBuilder uut;

  /**
   * Test setup.
   */
  @Before
  public void setup() {
    uut = new WebcastAuditRecordDtoBuilder();
  }

  /**
   * Tests {@link WebcastAuditRecordDtoBuilder#create(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Webcast creation event.
   */
  @Test
  public void createWebcastAuditRecordDto() {
    // Do test
    AuditRecordDto auditRecord = uut.create(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);

    // Assert audit record is as expected
    assertEquals(USER_ID, auditRecord.getUserId());
    assertEquals(CHANNEL_ID, auditRecord.getChannelId());
    assertEquals(COMMUNICATION_ID, auditRecord.getCommunicationId());
    assertEquals(AuditAction.CREATE.getAction(), auditRecord.getAction());
    assertEquals(AuditEntity.WEBCAST.getEntity(), auditRecord.getEntity());
    assertEquals(ACTION_DESCRIPTION, auditRecord.getActionDescription());
  }

  /**
   * Tests {@link WebcastAuditRecordDtoBuilder#cancel(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Webcast cancellation event.
   */
  @Test
  public void cancelWebcastAuditRecordDto() {
    // Do test
    AuditRecordDto auditRecord = uut.cancel(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);

    // Assert audit record is as expected
    assertEquals(USER_ID, auditRecord.getUserId());
    assertEquals(CHANNEL_ID, auditRecord.getChannelId());
    assertEquals(COMMUNICATION_ID, auditRecord.getCommunicationId());
    assertEquals(AuditAction.CANCEL.getAction(), auditRecord.getAction());
    assertEquals(AuditEntity.WEBCAST.getEntity(), auditRecord.getEntity());
    assertEquals(ACTION_DESCRIPTION, auditRecord.getActionDescription());
  }

  /**
   * Tests {@link WebcastAuditRecordDtoBuilder#update(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Webcast update event.
   * <p>
   * Expected Result: {@link UnsupportedOperationException} as auditing 'update webcast event' is not supported yet.
   */
  @Test(expected = UnsupportedOperationException.class)
  public void updateWebcastAuditRecordDto() {
    // Do test
    uut.update(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);
  }

  /**
   * Tests {@link WebcastAuditRecordDtoBuilder#delete(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Webcast deletion event.
   * <p>
   * Expected Result: {@link UnsupportedOperationException} as auditing 'delete webcast event' is not supported yet.
   */
  @Test(expected = UnsupportedOperationException.class)
  public void deleteWebcastAuditRecordDto() {
    // Do test
    uut.delete(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);

  }
}
