/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpCommunityServiceDaoTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.community;

import static org.easymock.EasyMock.isA;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.community.dto.CommunicationsDto;
import com.brighttalk.service.channel.integration.community.dto.converter.CommunicationsConverter;

public class XmlHttpCommunityServiceDaoTest {

  private static final String SERVICE_BASE_URL = "http://test.community.brighttalk.net/";
  
  private XmlHttpCommunityServiceDao uut;
  
  private RestTemplate mockRestTemplate;
  
  private CommunicationsConverter mockConverter;
  
  
  @Before
  public void setUp() {
    
    uut = new XmlHttpCommunityServiceDao();
    
    mockConverter = EasyMock.createMock( CommunicationsConverter.class );
    uut.setConverter( mockConverter );
    
    mockRestTemplate = EasyMock.createMock( RestTemplate.class );
    uut.setRestTemplate( mockRestTemplate );
    
    uut.setServiceBaseUrl( SERVICE_BASE_URL );
  }
  
  @Ignore("Tests not ready - need to find a way to easy mock the null parameter in the exchange call.")
  @SuppressWarnings("unchecked")
  @Test
  public void deleteChannel() {
    
    final Long channelId = 666l;
    
    EasyMock.expect( mockRestTemplate.exchange( isA( String.class), isA( HttpMethod.class ), isA( HttpEntity.class ), isA( Class.class ), isA( Map.class ) ) ).
          andReturn( new ResponseEntity<Object>(HttpStatus.ACCEPTED));
    
    EasyMock.replay( mockRestTemplate );
    
    //do test
    uut.deleteChannel( channelId );
    
    EasyMock.verify( mockRestTemplate );
  }
  
  @Test
  public void deleteCommunications() throws Exception {
    
    final List<Long> communicationIds = new ArrayList<Long>();
    final Long channelId = 666l;
    
    final CommunicationsDto communicationsDto = new CommunicationsDto();
    
    EasyMock.expect( mockConverter.convert( channelId, communicationIds ) ).andReturn( communicationsDto );
    EasyMock.expect( mockRestTemplate.postForLocation( isA( String.class ), isA( HttpEntity.class) ) ).andReturn( new URI( "" ) );
    
    EasyMock.replay( mockConverter, mockRestTemplate );
    
    //do test
    uut.deleteCommunications( channelId, communicationIds );

    EasyMock.verify( mockConverter, mockRestTemplate );
  }
  
}