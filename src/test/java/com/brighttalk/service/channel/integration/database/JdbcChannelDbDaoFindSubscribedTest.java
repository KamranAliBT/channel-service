/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcChannelDbDaoFindSubscribedTest.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria.SortBy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcChannelDbDaoFindSubscribedTest extends AbstractJdbcDbDaoTest {

  private static final long SUBSCRIBER_ID = 143l;

  @Autowired
  private JdbcChannelDbDao uut;

  @Test
  public void findSubscribedChannels() {

    int pageSize = 10;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(pageNumber);
    criteria.setPageSize(pageSize);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
    assertEquals(pageNumber, result.getCurrentPageNumber());
    assertEquals(pageSize, result.getPageSize());
  }

  @Test
  public void findSubscribedChannelsWithSummary() {

    Long channelId = addTemporaryChannelWithTypes();
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    final Channel resultChannel = result.get(0);

    assertEquals(CHANNEL_TITLE, resultChannel.getTitle());
    assertEquals(CHANNEL_DESCRIPTION, resultChannel.getDescription());
    assertEquals(CHANNEL_STRAPLINE, resultChannel.getStrapline());
    assertEquals(CHANNEL_KEYWORDS_LIST, resultChannel.getKeywords());
  }

  @Test
  public void findSubscribedChannelsWhenUnsubscribed() {

    Long channelId = addTemporaryChannelWithTypes();
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    jdbcTemplate.update("UPDATE subscription SET is_active = 0 WHERE channel_id = ?", channelId);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWhenUnsubscribedAndSortBySubscribers() {

    Long channelId = addTemporaryChannelWithTypes();
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    jdbcTemplate.update("UPDATE subscription SET is_active = 0 WHERE channel_id = ?", channelId);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);
    criteria.setSortBy(SortBy.SUBSCRIBERS);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWithStatisticsViewedFor() {

    Long channelId = addTemporaryChannelWithTypes();
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(CHANNEL_VIEWED_FOR, result.get(0).getStatistics().getViewedFor());
  }

  @Test
  public void findSubscribedChannelsWithStatisticsNumberOfUpcomingCommunicationsForUpcomingStatus() {

    Long channelId = addTemporaryChannelWithTypes();

    final Long upcoming = 1L;
    final Long recorded = 0L;
    final Long live = 0L;
    final Long subscribers = 0L;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfLiveCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfRecordedCommunications());

  }

  @Test
  public void findSubscribedChannelsWithStatisticsNumberOfLiveCommunicationsForLiveStatus() {

    Long channelId = addTemporaryChannelWithTypes();

    final Long upcoming = 0L;
    final Long recorded = 0L;
    final Long live = 1L;
    final Long subscribers = 0L;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.get(0).getStatistics().getNumberOfLiveCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfRecordedCommunications());
  }

  @Test
  public void findSubscribedChannelsWithStatisticsNumberOfRecordedCommunicationsForRecordedStatus() {

    Long channelId = addTemporaryChannelWithTypes();

    final Long upcoming = 0L;
    final Long recorded = 1L;
    final Long live = 0L;
    final Long subscribers = 0L;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.get(0).getStatistics().getNumberOfLiveCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(1, result.get(0).getStatistics().getNumberOfRecordedCommunications());
  }

  @Test
  public void findSubscribedChannelsWithStatisticsRatings() {

    Long channelId = addTemporaryChannelWithTypes();
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2.0f, result.get(0).getStatistics().getRating(), 0);
  }

  @Test
  public void findSubscribedChannelsWithSubscribers() {

    Long channelId = addTemporaryChannelWithTypes();

    final Long upcoming = 0L;
    final Long recorded = 0L;
    final Long live = 0L;
    final Long subscribers = 3L;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    final long expectedSubsribersCount = 3l;
    assertEquals(expectedSubsribersCount, result.get(0).getStatistics().getNumberOfSubscribers());
  }

  @Test
  public void findSubscribedChannelsMultipleEntriesNoSortBy() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(channelId2, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortByCreatedDateDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.CREATEDDATE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(channelId1, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortByCreatedDateAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.CREATEDDATE);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(channelId2, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortByRatingDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Channel channel1 = channels.get(0);
    channel1.getStatistics().setRating(5);
    Long channelId1 = addTemporaryChannel(channel1);

    Channel channel2 = channels.get(1);
    channel2.getStatistics().setRating(1);
    Long channelId2 = addTemporaryChannel(channel2);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.AVERAGERATING);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(channelId2, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortByRatingDescPageOneSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Channel channel1 = channels.get(0);
    channel1.getStatistics().setRating(5);
    Long channelId1 = addTemporaryChannel(channel1);

    Channel channel2 = channels.get(1);
    channel2.getStatistics().setRating(1);
    Long channelId2 = addTemporaryChannel(channel2);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.AVERAGERATING);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(1);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(channelId1, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortByRatingDescPageOneSizeOneHasMore() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Channel channel1 = channels.get(0);
    channel1.getStatistics().setRating(5);
    Long channelId1 = addTemporaryChannel(channel1);

    Channel channel2 = channels.get(1);
    channel2.getStatistics().setRating(1);
    Long channelId2 = addTemporaryChannel(channel2);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.AVERAGERATING);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(1);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertTrue(result.hasNextPage());
  }

  @Test
  public void findSubscribedChannelsWithSortByRatingDescPageTwoSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Channel channel1 = channels.get(0);
    channel1.getStatistics().setRating(5);
    Long channelId1 = addTemporaryChannel(channel1);

    Channel channel2 = channels.get(1);
    channel2.getStatistics().setRating(1);
    Long channelId2 = addTemporaryChannel(channel2);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.AVERAGERATING);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(2);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(channelId2, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortByRatingDescPageTwoSizeOneHasMore() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Channel channel1 = channels.get(0);
    channel1.getStatistics().setRating(5);
    Long channelId1 = addTemporaryChannel(channel1);

    Channel channel2 = channels.get(1);
    channel2.getStatistics().setRating(1);
    Long channelId2 = addTemporaryChannel(channel2);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.AVERAGERATING);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(2);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertFalse(result.hasNextPage());
  }

  @Test
  public void findSubscribedChannelsWithSortByRatingAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Channel channel1 = channels.get(0);
    channel1.getStatistics().setRating(5);
    Long channelId1 = addTemporaryChannel(channel1);

    Channel channel2 = channels.get(1);
    channel2.getStatistics().setRating(1);
    Long channelId2 = addTemporaryChannel(channel2);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.AVERAGERATING);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(channelId1, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortByRecordedCommsAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long upcoming = 0L;
    final Long recorded = 2L;
    final Long live = 0L;
    final Long subscribers = 0L;
    addChannelStatistics(channelId1, upcoming, recorded, live, subscribers);

    addChannelStatistics(channelId2, upcoming, 1L, live, subscribers);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.RECORDEDWEBCASTS);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(1, result.get(0).getStatistics().getNumberOfRecordedCommunications());
    assertEquals(channelId1, result.get(1).getId());
    assertEquals(2, result.get(1).getStatistics().getNumberOfRecordedCommunications());

  }

  @Test
  public void findSubscribedChannelsWithSortByRecordedCommsDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long upcoming = 0L;
    final Long live = 0L;
    final Long subscribers = 0L;
    addChannelStatistics(channelId1, upcoming, 2L, live, subscribers);

    addChannelStatistics(channelId2, upcoming, 1L, live, subscribers);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.RECORDEDWEBCASTS);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(2, result.get(0).getStatistics().getNumberOfRecordedCommunications());
    assertEquals(channelId2, result.get(1).getId());
    assertEquals(1, result.get(1).getStatistics().getNumberOfRecordedCommunications());
  }

  @Test
  public void findSubscribedChannelsWithSortByUpcomingCommsAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long recorded = 0L;
    final Long live = 0L;
    final Long subscribers = 0L;
    addChannelStatistics(channelId1, 2L, recorded, live, subscribers);

    addChannelStatistics(channelId2, 1L, recorded, live, subscribers);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.UPCOMINGWEBCASTS);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(1L, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(channelId1, result.get(1).getId());
    assertEquals(2L, result.get(1).getStatistics().getNumberOfUpcomingCommunications());
  }

  @Test
  public void findSubscribedChannelsWithSortByUpcomingCommsDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long recorded = 0L;
    final Long live = 0L;
    final Long subscribers = 0L;
    addChannelStatistics(channelId1, 2L, recorded, live, subscribers);

    addChannelStatistics(channelId2, 1L, recorded, live, subscribers);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.UPCOMINGWEBCASTS);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(2L, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(channelId2, result.get(1).getId());
    assertEquals(1L, result.get(1).getStatistics().getNumberOfUpcomingCommunications());

  }

  @Test
  public void findSubscribedChannelsWithSortBySubscribers() {

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.SUBSCRIBERS);
    criteria.setSortOrder(SortOrder.DESC);

    criteria.setPageNumber(1);
    criteria.setPageSize(10);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWithSortBySubscribersDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long recorded = 0L;
    final Long live = 0L;
    final Long upcoming = 0L;

    addChannelStatistics(channelId1, upcoming, recorded, live, 2L);

    addChannelStatistics(channelId2, upcoming, recorded, live, 1L);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.SUBSCRIBERS);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(channelId2, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortBySubscribersWithSummary() {

    Long channelId = addTemporaryChannelWithTypes();

    final Long recorded = 0L;
    final Long live = 0L;
    final Long upcoming = 0L;
    final Long subscribers = 0L;

    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.SUBSCRIBERS);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    final Channel resultChannel = result.get(0);

    assertEquals(CHANNEL_TITLE, resultChannel.getTitle());
    assertEquals(CHANNEL_DESCRIPTION, resultChannel.getDescription());
    assertEquals(CHANNEL_STRAPLINE, resultChannel.getStrapline());
    assertEquals(CHANNEL_KEYWORDS_LIST, resultChannel.getKeywords());
  }

  @Test
  public void findSubscribedChannelsWithSortBySubscribersAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long recorded = 0L;
    final Long live = 0L;
    final Long upcoming = 0L;

    addChannelStatistics(channelId1, upcoming, recorded, live, 2L);
    addChannelStatistics(channelId2, upcoming, recorded, live, 1L);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);

    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.SUBSCRIBERS);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setPageNumber(1);
    criteria.setPageSize(50);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(1, result.get(0).getStatistics().getNumberOfSubscribers());
    assertEquals(channelId1, result.get(1).getId());
    assertEquals(2, result.get(1).getStatistics().getNumberOfSubscribers());
  }

  @Test
  public void findSubscribedChannelsWithSortBySubscribersAscPageOneSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long recorded = 0L;
    final Long live = 0L;
    final Long upcoming = 0L;

    addChannelStatistics(channelId1, upcoming, recorded, live, 1L);
    addChannelStatistics(channelId2, upcoming, recorded, live, 2L);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);

    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.SUBSCRIBERS);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setPageNumber(1);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(channelId1, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortBySubscribersAscPageOneSizeOneHasMore() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long recorded = 0L;
    final Long live = 0L;
    final Long upcoming = 0L;

    addChannelStatistics(channelId1, upcoming, recorded, live, 1L);
    addChannelStatistics(channelId2, upcoming, recorded, live, 2L);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);

    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.SUBSCRIBERS);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setPageNumber(1);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertTrue(result.hasNextPage());
  }

  @Test
  public void findSubscribedChannelsWithSortBySubscribersAscPageTwoSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long recorded = 0L;
    final Long live = 0L;
    final Long upcoming = 0L;

    addChannelStatistics(channelId1, upcoming, recorded, live, 1L);
    addChannelStatistics(channelId2, upcoming, recorded, live, 2L);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);

    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.SUBSCRIBERS);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setPageNumber(2);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(channelId2, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWithSortBySubscribersAscPageTwoSizeOneHasMore() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long recorded = 0L;
    final Long live = 0L;
    final Long upcoming = 0L;

    addChannelStatistics(channelId1, upcoming, recorded, live, 1L);
    addChannelStatistics(channelId2, upcoming, recorded, live, 2L);

    addChannelSubscription(channelId1, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setSortBy(SortBy.SUBSCRIBERS);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setPageNumber(2);
    criteria.setPageSize(1);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsSubscribed(SUBSCRIBER_ID, criteria);

    assertFalse(result.hasNextPage());
  }
}