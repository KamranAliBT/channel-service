/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationConfigurationDbDaoTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;

/**
 * Tests {@link JdbcCommunicationConfigurationDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCommunicationConfigurationDbDaoTest extends AbstractJdbcDbDaoTest {
  private static final long TEST_CHANNEL_ID = 9999;
  
  @Autowired
  private JdbcCommunicationConfigurationDbDao uut;
  
  @Autowired
  private JdbcCommunicationDbDao communicationDbDao;
  
  /**
   * Tests {@link JdbcCommunicationConfigurationDbDao#create()} in case of successfully create communication 
   * configuration.
   */
  @Test
  public final void testCreateCommunicationConfiguration() {
    // setup.
    Long commId = this.addTemporaryCommunicationWithoutConfiguration(TEST_CHANNEL_ID, "brighttalkhd");
    CommunicationConfiguration configuration = new CommunicationConfiguration(commId, TEST_CHANNEL_ID);
    configuration.setVisibility(CommunicationConfiguration.Visibility.PRIVATE);
    configuration.setAllowAnonymousViewings(true);
    configuration.setShowChannelSurvey(true);
    configuration.setExcludeFromChannelContentPlan(true);
    configuration.setClientBookingReference("Test 1234567");
    
    // do test
    this.uut.create(configuration);
    
    Communication communication = this.communicationDbDao.find(commId);
    communication.getConfiguration();
    
    this.assertAllCommunicationConfigurationDetails(communication.getConfiguration(), configuration);
    this.temporaryCommunicationIds.add(commId);
  }

}
