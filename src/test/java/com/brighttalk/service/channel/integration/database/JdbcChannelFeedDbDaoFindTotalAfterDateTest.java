/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: JdbcChannelFeedDbDaoFindTotalAfterDateTest.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;

/**
 * Tests {link JdbcCommunicationDbDao} related to findFeed.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcChannelFeedDbDaoFindTotalAfterDateTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcChannelFeedDbDao uut;

  @Test
  public void totalAfterDateNoCommunications() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateNoChannel() {
    // Set up
    long channelId = 999999999;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateNullDate() {
    // Set up
    long channelId = 999999999;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date currentDate = null;

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationInactiveChannel() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = false;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationDeletedCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.setStatus(Communication.Status.DELETED);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationCancelledCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.setStatus(Communication.Status.CANCELLED);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsScheduledAfterDate(Long, ChannelFeedSearchCriteria)}
   * total will include all communications after the specified date if the status filter is not set in the criteria,
   * except if they are cancelled or deleted.
   */
  @Test
  public void totalAfterDateWithoutSpecifyingCommunicationStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date currentDate = new Date();

    Communication cancelledCommunication = buildCommunication(channelId);
    cancelledCommunication.setStatus(Communication.Status.CANCELLED);
    cancelledCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 1));

    Communication deletedCommunication = buildCommunication(channelId);
    deletedCommunication.setStatus(Communication.Status.DELETED);
    deletedCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 2));

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);
    upcomingCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 3));

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);
    liveCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 4));

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);
    recordedCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 5));

    addTemporaryCommunication(channelId, upcomingCommunication);
    addTemporaryCommunication(channelId, liveCommunication);
    addTemporaryCommunication(channelId, recordedCommunication);
    addTemporaryCommunication(channelId, cancelledCommunication);
    addTemporaryCommunication(channelId, deletedCommunication);

    List<CommunicationStatusFilter> statusFilter = Collections.<CommunicationStatusFilter>emptyList();
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 3;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsScheduledAfterDate(Long, ChannelFeedSearchCriteria)}
   * will only include upcoming communications after the specified date if the status filter in the criteria is set to
   * only include upcoming communications.
   */
  @Test
  public void totalAfterDateWithUpcomingStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date currentDate = new Date();

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);
    upcomingCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 1));

    addTemporaryCommunication(channelId, upcomingCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.UPCOMING);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsScheduledAfterDate(Long, ChannelFeedSearchCriteria)}
   * will only include live communications after the specified date if the status filter in the criteria is set to only
   * include live communications.
   */
  @Test
  public void totalAfterDateWithLiveStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date currentDate = new Date();

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);
    liveCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 1));

    addTemporaryCommunication(channelId, liveCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.LIVE);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsScheduledAfterDate(Long, ChannelFeedSearchCriteria)}
   * will only include recorded communications after the specified date if the status filter in the criteria is set to
   * only include recorded communications.
   */
  @Test
  public void totalAfterDateWithRecordedStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date currentDate = new Date();

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);
    recordedCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 1));

    addTemporaryCommunication(channelId, recordedCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.RECORDED);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsScheduledAfterDate(Long, ChannelFeedSearchCriteria)}
   * will only include upcoming or live communications after the specified date if the status filter in the criteria is
   * set to only include upcoming or live communications.
   */
  @Test
  public void totalAfterDateWithMultipleStatusesFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date currentDate = new Date();

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);
    upcomingCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 3));

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);
    liveCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 2));

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);
    recordedCommunication.setScheduledDateTime(DateUtils.addDays(currentDate, 1));

    addTemporaryCommunication(channelId, upcomingCommunication);
    addTemporaryCommunication(channelId, liveCommunication);
    addTemporaryCommunication(channelId, recordedCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(
        CommunicationStatusFilter.UPCOMING, CommunicationStatusFilter.LIVE);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 2;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationUnpublishedCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.setPublishStatus(Communication.PublishStatus.UNPUBLISHED);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationFeedOnlyCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.FEEDONLY);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationPrivateCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.PRIVATE);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationSyndicatedOut() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.OUT);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationSyndicatedInApprovedByBoth() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationSyndicatedInNotApprovedByMaster() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationSyndicatedInNotApprovedByConsumer() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationSyndicatedInNotApprovedByBoth() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationSyndicatedInPendingBoth() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationSyndicatedInPendingMaster() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void totalAfterDateOneCommunicationSyndicatedInPendingConsumer() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setScheduledDateTime(DateUtils.addDays(new Date(), 1));

    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Testing Feature Rule - Closest to Now
   */
  @Test
  public void totalAfterDateTwoCommunicationsAllFuture() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, 2));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, 1));
    addTemporaryCommunication(channelId, communication2);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 2;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Testing Feature Rule - Closest to Now
   */
  @Test
  public void totalAfterDateTwoCommunicationsAllPast() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, -2));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, -1));
    addTemporaryCommunication(channelId, communication2);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Testing Feature Rule - Closest to Now
   */
  @Test
  public void totalAfterDateTwoCommunicationsAllPastAndFuture() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, 2));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, -1));
    addTemporaryCommunication(channelId, communication2);

    Date currentDate = new Date();

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsScheduledAfterDate(channelId, currentDate, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  private ChannelFeedSearchCriteria buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(
      List<CommunicationStatusFilter> statusFilter) {

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(statusFilter);

    return criteria;
  }
}
