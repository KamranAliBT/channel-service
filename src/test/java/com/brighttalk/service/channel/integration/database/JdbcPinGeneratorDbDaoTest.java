/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcPinGeneratorDbDaoTest.java 70061 2013-10-21 16:34:03Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;

/**
 * Tests {link JdbcPinGeneratorDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcPinGeneratorDbDaoTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcPinGeneratorDbDao uut;

  @Test
  public void testIsPinUsedReturnsFalsePinIsUsed() {
    // Set up
    String pinNumber = "01234567";
    Long channelId = 132l;
    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().build();
    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(channelId).withPinNumber(
        pinNumber).withConfiguration(communicationConfiguration).build();
    addTemporaryCommunication(channelId, communication);

    // expectations
    boolean expectedIsPinUsed = true;

    // do test
    boolean isPinUsed = uut.isPinUsed(pinNumber);

    // assertions
    assertNotNull(isPinUsed);
    assertEquals(expectedIsPinUsed, isPinUsed);
  }

  @Test
  public void testIsPinUsedReturnsFalsePinIsNotUsed() {
    // Set up
    String pinNumber = "09887456";

    // expectations
    boolean expectedIsPinUsed = false;

    // do test
    boolean isPinUsed = uut.isPinUsed(pinNumber);

    // assertions
    assertNotNull(isPinUsed);
    assertEquals(expectedIsPinUsed, isPinUsed);
  }
}