/**
 * *****************************************************************************
 * * Copyright BrightTALK Ltd, 2009-2010.
 * * All Rights Reserved.
 * * Id: JdbcCommunicationOverrideDbDaoTest.java
 * *****************************************************************************
 * @package com.brighttalk.service.channel.integration.database
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Test Case for {@link JdbcCommunicationOverrideDbDao}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCommunicationOverrideDbDaoTest extends AbstractJdbcDbDaoTest {

  private final static Long CHANNEL1_ID = new Long(1);

  private final static Long CHANNEL2_ID = new Long(2);

  private final static Long COMMUNICATION1_ID = new Long(1);

  private final static Long COMMUNICATION2_ID = new Long(2);

  private final static Long COMMUNICATION3_ID = new Long(3);

  private SimpleJdbcInsert simpleJdbcInsertCommunicationOverride;

  private List<CommunicationOverridePrimaryKey> idsToDelete = new ArrayList<CommunicationOverridePrimaryKey>();

  private List<Communication> communications = new ArrayList<Communication>();

  @Autowired
  private JdbcCommunicationOverrideDbDao uut;

  @Autowired
  @Override
  public void setDataSource(DataSource dataSource) {
    super.setDataSource(dataSource);

    this.simpleJdbcInsertCommunicationOverride = new SimpleJdbcInsert(dataSource);
    this.simpleJdbcInsertCommunicationOverride.withTableName("communication_details_override");
    this.simpleJdbcInsertCommunicationOverride.usingColumns(
            "channel_id",
            "communication_id",
            "title",
            "description",
            "presenter",
            "keywords",
            "created",
            "last_updated");
  }

  /**
   * Set up this test case. 
   * Create some categories for two communications in two channels. 
   */
  @Before
  public void setUp() {

    Communication communication1 = new Communication(COMMUNICATION1_ID);
    communication1.setTitle("com1 title - no override");
    communication1.setDescription("com1 description - no override");
    communication1.setAuthors("com1 author - no override");
    communication1.setKeywords("com1 keywords - no override");

    Communication communication2 = new Communication(COMMUNICATION2_ID);
    communication2.setTitle("com2 title - no override");
    communication2.setDescription("com2 description - no override");
    communication2.setAuthors("com2 author - no override");
    communication2.setKeywords("com2 keywords - no override");

    Communication communication3 = new Communication(COMMUNICATION3_ID);
    communication3.setTitle("com3 title - no override");
    communication3.setDescription("com3 description - no override");
    communication3.setAuthors("com3 author - no override");
    communication3.setKeywords("com3 keywords - no override");

    communications.add(communication1);
    communications.add(communication2);
    communications.add(communication3);
  }

  /**
   * Tear down this test case. 
   * Delete categories we created in the DB.
   */
  @After
  @Override
  public void tearDown() {
    super.tearDown();

    for (CommunicationOverridePrimaryKey id : this.idsToDelete) {
      Long channelId = id.getChannelId();
      Long communicationId = id.getCommunicationId();
      this.jdbcTemplate.update("DELETE FROM communication_details_override WHERE channel_id = ? AND communication_id = ?", new Object[]{channelId, communicationId});
    }
  }

  /**
   * Just sanity check we have no errors in the SQL in the DAO. 
   */
  @Test
  public void testOverridesSanityCheck() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID,
            "override title 1 1",
            "override description 1 1",
            "override author 1 1",
            "override 1 1, keyword1,keyword2");

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "override title 1 1", "override description 1 1", "override author 1 1", "override 1 1, keyword1,keyword2");

    Communication communication2 = communications.get(1);
    this.assertCommunication(communication2, "com2 title - no override", "com2 description - no override", "com2 author - no override", "com2 keywords - no override");

    Communication communication3 = communications.get(2);
    this.assertCommunication(communication3, "com3 title - no override", "com3 description - no override", "com3 author - no override", "com3 keywords - no override");
  }

  /**
   * Test that overrides are working correctly for a couple of communications that 
   * have their title, desc etc overridden.
   */
  @Test
  public void testOverridesSetForTwoCommunications() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID,
            "override title 1 1",
            "override description 1 1",
            "override author 1 1",
            "override 1 1, keyword1,keyword2");

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION2_ID,
            "override title 1 2",
            "override description 1 2",
            "override author 1 2",
            "override 1 2, keyword1,keyword2");

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "override title 1 1", "override description 1 1", "override author 1 1", "override 1 1, keyword1,keyword2");

    Communication communication2 = communications.get(1);
    this.assertCommunication(communication2, "override title 1 2", "override description 1 2", "override author 1 2", "override 1 2, keyword1,keyword2");
  }

  /**
   * Test we only get the overrides for the communications in channel 1 but not for the communication in channel2
   */
  @Test
  public void testOverridesSetForTwoCommunicationsButNotThirdOnDifferentChannel() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID,
            "override title 1 1",
            "override description 1 1",
            "override author 1 1",
            "override 1 1, keyword1,keyword2");

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION2_ID,
            "override title 1 2",
            "override description 1 2",
            "override author 1 2",
            "override 1 2, keyword1,keyword2");

    this.insertCommunicationOverride(CHANNEL2_ID, COMMUNICATION3_ID,
            "override title 2 3",
            "override description 2 3",
            "override author 2 3",
            "override 2 3, keyword1,keyword2");

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "override title 1 1", "override description 1 1", "override author 1 1", "override 1 1, keyword1,keyword2");

    Communication communication2 = communications.get(1);
    this.assertCommunication(communication2, "override title 1 2", "override description 1 2", "override author 1 2", "override 1 2, keyword1,keyword2");

    Communication communication3 = communications.get(2);
    this.assertCommunication(communication3, "com3 title - no override", "com3 description - no override", "com3 author - no override", "com3 keywords - no override");
  }

  /**
   * Just test a mixture of some communications having some overrides set and some not set
   */
  @Test
  public void testMixtureOfOverridesSetAndNotSet() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID,
            null,
            null,
            "override author 1 1",
            "override 1 1, keyword1,keyword2");

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION2_ID,
            "override title 1 2",
            "override description 1 2",
            null,
            null);

    this.insertCommunicationOverride(CHANNEL2_ID, COMMUNICATION3_ID,
            null,
            "override description 2 3",
            "override author 2 3",
            "override 2 3, keyword1,keyword2");

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "com1 title - no override", "com1 description - no override", "override author 1 1", "override 1 1, keyword1,keyword2");

    Communication communication2 = communications.get(1);
    this.assertCommunication(communication2, "override title 1 2", "override description 1 2", "com2 author - no override", "com2 keywords - no override");

    Communication communication3 = communications.get(2);
    this.assertCommunication(communication3, "com3 title - no override", "com3 description - no override", "com3 author - no override", "com3 keywords - no override");
  }

  /**
   * Check that the title override only set and all other fields are not overridden. 
   */
  @Test
  public void testOverridesTitleOnlySet() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID, "override title 1 1", null, null, null);

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "override title 1 1", "com1 description - no override", "com1 author - no override", "com1 keywords - no override");

  }

  /**
   * Check that the description override only set and all other fields are not overridden. 
   */
  @Test
  public void testOverridesDescriptionOnlySet() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID, null, "override description 1 1", null, null);

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "com1 title - no override", "override description 1 1", "com1 author - no override", "com1 keywords - no override");
  }

  /**
   * Check that the authors override only set and all other fields are not overridden. 
   */
  @Test
  public void testOverridesAuthorsOnlySet() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID, null, null, "override author 1 1", null);

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "com1 title - no override", "com1 description - no override", "override author 1 1", "com1 keywords - no override");
  }

  /**
   * Check that the keywords override only set and all other fields are not overridden. 
   */
  @Test
  public void testOverridesKeywordsOnlySet() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID, null, null, null, "override 1 1, keyword1,keyword2");

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "com1 title - no override", "com1 description - no override", "com1 author - no override", "override 1 1, keyword1,keyword2");
  }

  /**
   * Final check, couple of fields being overridden. 
   */
  @Test
  public void testOverridesTitleAndDescriptionOverriddenOnly() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID, "override title 1 1", "override description 1 1", null, null);

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "override title 1 1", "override description 1 1", "com1 author - no override", "com1 keywords - no override");

  }

  /**
   * Test we don't get any overrides set on communications if none in the DB. 
   */
  @Test
  public void testOverridesNotSetIfNone() {

    uut.loadOverrides(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "com1 title - no override", "com1 description - no override", "com1 author - no override", "com1 keywords - no override");

    Communication communication2 = communications.get(1);
    this.assertCommunication(communication2, "com2 title - no override", "com2 description - no override", "com2 author - no override", "com2 keywords - no override");

  }

  /**
   * Test if we pass in a null channel id, nothing breaks. No exceptions or anything. 
   */
  @Test
  public void testOverridesNullChannelId() {

    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION1_ID, "override title 1 1", "override description 1 1", null, null);
    this.insertCommunicationOverride(CHANNEL1_ID, COMMUNICATION2_ID, "override title 1 2", "override description 1 2", null, null);

    uut.loadOverrides(null, communications);

    Communication communication1 = communications.get(0);
    this.assertCommunication(communication1, "com1 title - no override", "com1 description - no override", "com1 author - no override", "com1 keywords - no override");

    Communication communication2 = communications.get(1);
    this.assertCommunication(communication2, "com2 title - no override", "com2 description - no override", "com2 author - no override", "com2 keywords - no override");
  }

  /**
   * Just test if we pass in null communications we don't get any exceptions or other errors. 
   */
  @Test
  public void testOverridesNullCommunications() {

    uut.loadOverrides(CHANNEL1_ID, null);
  }

  /**
   * Just test if we pass in empty communications we don't get any exceptions or other errors. 
   */
  @Test
  public void testOverridesEmptyCommunications() {

    uut.loadOverrides(CHANNEL1_ID, new ArrayList<Communication>());
  }

  /**
   * Helper to check a communication is correct. 
   */
  private void assertCommunication(Communication communication, String title, String description, String authors, String keywords) {
    assertEquals("incorrect title", title, communication.getTitle());
    assertEquals("incorrect description", description, communication.getDescription());
    assertEquals("incorrect authors", authors, communication.getAuthors());
    assertEquals("incorrect keywords", keywords, communication.getKeywords());
  }

  /**
   * Helper to create an override in the DB. 
   * @param channelId
   * @param communicationId
   * @param title
   * @param description
   * @param presenter
   * @param keywords
   */
  private void insertCommunicationOverride(Long channelId, Long communicationId, String title, String description, String presenter, String keywords) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("communication_id", communicationId);
    params.addValue("title", title);
    params.addValue("description", description);
    params.addValue("presenter", presenter);
    params.addValue("keywords", keywords);

    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    this.simpleJdbcInsertCommunicationOverride.execute(params);
    this.idsToDelete.add(new CommunicationOverridePrimaryKey(channelId, communicationId));
  }

  /**
   * The primary key for each entry in the communication_override_table. 
   * Primary key is channel id and communication id. 
   * Used so we can delete all entries we set up in this test case. 
   */
  private static class CommunicationOverridePrimaryKey {

    private Long channelId;

    private Long communicationId;

    public CommunicationOverridePrimaryKey(Long channelId, Long communicationId) {
      super();
      this.channelId = channelId;
      this.communicationId = communicationId;
    }

    public Long getChannelId() {
      return channelId;
    }

    public Long getCommunicationId() {
      return communicationId;
    }
  }//communicationoverrideprimarykey
}
