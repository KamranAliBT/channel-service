/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationDbDaoTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Tests {@link JdbcRenditionDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcRenditionDbDaoTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcRenditionDbDao uut;

  @Test
  public void findRenditionById() {
    // set up
    Long renditionId = addCommunicationRendition();

    // expectations

    // exercise
    Rendition retrievedRendition = uut.find(renditionId);

    // assertions
    assertNotNull(retrievedRendition.getId());
    assertNotNull(retrievedRendition.getUrl());
    assertNotNull(retrievedRendition.getContainer());
    assertNotNull(retrievedRendition.getBitrate());
    assertNotNull(retrievedRendition.getWidth());
    assertNotNull(retrievedRendition.getCodecs());
  }

  @Test
  public void findRenditionByCommunicationId() {
    // set up
    Long communicationId = 112L;
    Long renditionId = addCommunicationRendition();
    addCommunicationRenditionAsset(renditionId, communicationId);

    // expectations

    // exercise
    List<Rendition> retrievedRenditions = uut.findByCommunicationId(communicationId);

    // assertions
    Rendition retrievedRendition = retrievedRenditions.get(0);
    assertNotNull(retrievedRendition.getId());
    assertNotNull(retrievedRendition.getUrl());
    assertNotNull(retrievedRendition.getContainer());
    assertNotNull(retrievedRendition.getBitrate());
    assertNotNull(retrievedRendition.getWidth());
    assertNotNull(retrievedRendition.getCodecs());
    temporaryCommunicationIds.add(communicationId);
  }

  @Test
  public void findRenditionByCommunicationIds() {
    // set up
    Long communicationId = 111L;
    List<Long> communicationIds = new ArrayList<>();
    communicationIds.add(communicationId);

    Long renditionId = addCommunicationRendition();
    addCommunicationRenditionAsset(renditionId, communicationId);

    // expectations

    // exercise
    List<Rendition> retrievedRenditions = uut.findByCommunicationIds(communicationIds);

    // assertions
    Rendition retrievedRendition = retrievedRenditions.get(0);
    assertNotNull(retrievedRendition.getId());
    assertNotNull(retrievedRendition.getUrl());
    assertNotNull(retrievedRendition.getContainer());
    assertNotNull(retrievedRendition.getBitrate());
    assertNotNull(retrievedRendition.getWidth());
    assertNotNull(retrievedRendition.getCodecs());

    temporaryCommunicationIds.add(communicationId);
  }

  @Test
  public void create() {
    // set up
    Rendition rendition = new Rendition();
    rendition.setBitrate(122L);
    rendition.setCodecs("codecs");
    rendition.setContainer("container");
    rendition.setIsActive(true);
    rendition.setUrl("url");
    rendition.setWidth(456L);

    // expectations

    // exercise
    Rendition retrievedRendition = uut.create(rendition);

    // assertions
    assertNotNull(retrievedRendition.getId());
    assertNotNull(retrievedRendition.getUrl());
    assertNotNull(retrievedRendition.getContainer());
    assertNotNull(retrievedRendition.getBitrate());
    assertNotNull(retrievedRendition.getWidth());
    assertNotNull(retrievedRendition.getCodecs());

    temporaryRenditionIds.add(retrievedRendition.getId());
  }

  @Test
  public void delete() {
    // set up

    Long renditionId = addCommunicationRendition();

    // expectations

    // exercise
    uut.delete(renditionId);

    // assertions
    Rendition rendition = uut.find(renditionId);

    assertTrue(!rendition.getIsActive());
  }

  @Test(expected = EmptyResultDataAccessException.class)
  public void removeRenditionAssets() {

    final Long channelId = 666L;
    Communication communication = buildCommunication(channelId);
    Long communicationId = addTemporaryCommunication(channelId, communication);

    final Long renditionId = 6666l;
    addCommunicationRenditionAsset(renditionId, communicationId);

    uut.removeRenditionAssets(communicationId);

    jdbcTemplate.queryForMap("SELECT * FROM communication_asset WHERE is_active = 1 AND communication_id = ?",
        communicationId);

    // fail if we get pass that query
    assertTrue(false);
  }

}