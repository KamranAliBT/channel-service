/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: LiveEventRequestConverterTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.les.dto.converter;

import java.util.Date;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.time.Iso8601DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.les.dto.CancelCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.RescheduleCommunicationDto;

public class LiveEventRequestConverterTest {

  private LiveEventRequestConverter uut;

  @Before
  public void setUp() {

    uut = new LiveEventRequestConverter();
  }

  @Test
  public void convertForReschedule() {
    // Set up
    Long communicationId = 123L;
    Date scheduledDateTime = new Date();
    Integer duration = 500;
    String timeZone = "France/Paris";

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(communicationId).withScheduledDateTime(
        scheduledDateTime).withDuration(duration).withTimeZone(timeZone).build();

    // Expectations

    // Excercise
    RescheduleCommunicationDto rescheduleCommunicationDto = uut.convertForReschedule(communication);

    // Assertions
    Assert.assertEquals(rescheduleCommunicationDto.getId(), communicationId);
    Assert.assertEquals(rescheduleCommunicationDto.getScheduled(), format(scheduledDateTime));
    Assert.assertEquals(rescheduleCommunicationDto.getDuration(), duration);
    Assert.assertEquals(rescheduleCommunicationDto.getTimezone(), timeZone);
  }

  @Test
  public void convertForCancel() {
    // Set up
    Long communicationId = 123L;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(communicationId).build();

    // Expectations

    // Excercise
    CancelCommunicationDto cancelCommunicationDto = uut.convertForCancel(communication);

    // Assertions
    Assert.assertEquals(cancelCommunicationDto.getId(), communicationId);
  }

  /**
   * Format the scheduled date to be Iso860.
   * 
   * @param date to be formatted
   * @return the formatted date as string.
   */
  private String format(final Date date) {
    Iso8601DateTimeFormatter iso8601FormattedDate = new Iso8601DateTimeFormatter();
    return iso8601FormattedDate.convert(date);
  }
}