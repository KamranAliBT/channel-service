/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ContactDetailsAuditRecordDtoBuilderTest.java 97400 2015-07-01 08:36:42Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit.dto.builder;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.ContactDetailsAuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder.AuditAction;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder.AuditEntity;

/**
 * Tests for {@link ContactDetailsAuditRecordDtoBuilder}.
 */
public class ContactDetailsAuditRecordDtoBuilderTest {

  private static final Long USER_ID = 1L;
  private static final Long CHANNEL_ID = 2L;
  private static final Long COMMUNICATION_ID = 3L;
  private static final String ACTION_DESCRIPTION = "Test audit description 123...";

  private AuditRecordDtoBuilder uut;

  /**
   * Test setup.
   */
  @Before
  public void setup() {
    uut = new ContactDetailsAuditRecordDtoBuilder();
  }

  /**
   * Tests {@link ContactDetailsAuditRecordDtoBuilder#create(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Contact Details creation event.
   */
  @Test
  public void createContactDetailsAuditRecordDto() {
    // Do test
    AuditRecordDto auditRecord = uut.create(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);

    // Assert audit record is as expected
    assertEquals(USER_ID, auditRecord.getUserId());
    assertEquals(CHANNEL_ID, auditRecord.getChannelId());
    assertEquals(COMMUNICATION_ID, auditRecord.getCommunicationId());
    assertEquals(AuditAction.CREATE.getAction(), auditRecord.getAction());
    assertEquals(AuditEntity.CONTACT_DETAILS.getEntity(), auditRecord.getEntity());
    assertEquals(ACTION_DESCRIPTION, auditRecord.getActionDescription());
  }

  /**
   * Tests {@link ContactDetailsAuditRecordDtoBuilder#update(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Contact Details update event.
   */
  @Test
  public void updateContactDetailsAuditRecordDto() {
    // Do test
    AuditRecordDto auditRecord = uut.update(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);

    // Assert audit record is as expected
    assertEquals(USER_ID, auditRecord.getUserId());
    assertEquals(CHANNEL_ID, auditRecord.getChannelId());
    assertEquals(COMMUNICATION_ID, auditRecord.getCommunicationId());
    assertEquals(AuditAction.UPDATE.getAction(), auditRecord.getAction());
    assertEquals(AuditEntity.CONTACT_DETAILS.getEntity(), auditRecord.getEntity());
    assertEquals(ACTION_DESCRIPTION, auditRecord.getActionDescription());
  }

  /**
   * Tests {@link ContactDetailsAuditRecordDtoBuilder#cancel(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Contact Details cancellation event.
   * <p>
   * Expected Result: {@link UnsupportedOperationException} as auditing 'cancel contact details event' is not supported
   * yet.
   */
  @Test(expected = UnsupportedOperationException.class)
  public void cancelContactDetailsAuditRecordDto() {
    // Do test
    uut.cancel(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);
  }

  /**
   * Tests {@link ContactDetailsAuditRecordDtoBuilder#delete(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Contact Details deletion event.
   * <p>
   * Expected Result: {@link UnsupportedOperationException} as auditing 'delete contact details event' is not supported
   * yet.
   */
  @Test(expected = UnsupportedOperationException.class)
  public void deleteContactDetailsAuditRecordDto() {
    // Do test
    uut.delete(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);

  }
}
