/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpSurveyServiceDaoTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.survey;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.form.converter.FormConverter;
import com.brighttalk.common.form.domain.Form;
import com.brighttalk.common.form.dto.FormDto;
import com.brighttalk.service.channel.business.error.SurveyNotFoundException;
import com.brighttalk.service.channel.integration.survey.dto.SurveyResponseDto;
import com.brighttalk.service.channel.integration.survey.dto.SurveyResponseDto.State;
import com.brighttalk.service.channel.integration.survey.validator.FormCreateValidator;

/**
 * Unit test {@link XmlHttpSurveyServiceDao}
 */
public class XmlHttpSurveyServiceDaoTest {

  private XmlHttpSurveyServiceDao uut;

  private RestTemplate mockRestTemplate;

  private FormConverter mockFormConverter;
  private FormDto mockFormDto;

  @Before
  public void setUp() {

    uut = new XmlHttpSurveyServiceDao();

    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    mockFormConverter = EasyMock.createMock(FormConverter.class);
    mockFormDto = EasyMock.createMock(FormDto.class);
    uut.setRestTemplate(mockRestTemplate);
    uut.setServiceBaseUrl("http://suvey.brighttalk.net");
    uut.setFormConverter(mockFormConverter);

  }

  @SuppressWarnings("unchecked")
  @Test
  public void getSurvey() throws Exception {

    // setup
    Form form = new Form();
    SurveyResponseDto responseDto = new SurveyResponseDto();
    responseDto.setState(State.processed);
    responseDto.setForm(this.mockFormDto);

    ResponseEntity<SurveyResponseDto> response = new ResponseEntity<SurveyResponseDto>(responseDto, HttpStatus.OK);
    Long surveyId = 500L;

    // Expectations
    Form expectedResult = form;

    EasyMock.expect(this.mockFormConverter.convert(this.mockFormDto)).andReturn(form);
    EasyMock.replay(this.mockFormConverter);

    EasyMock.expect(
        this.mockRestTemplate.exchange(EasyMock.anyObject(String.class), EasyMock.anyObject(HttpMethod.class),
            EasyMock.anyObject(HttpEntity.class), EasyMock.anyObject(Class.class))).andReturn(response);
    EasyMock.replay(this.mockRestTemplate);

    this.mockFormDto.validate(EasyMock.anyObject(FormCreateValidator.class));
    EasyMock.expectLastCall();
    EasyMock.replay(this.mockFormDto);

    // do test
    Form result = uut.getSurveyForm(surveyId);

    // Assertions
    EasyMock.verify(this.mockRestTemplate, this.mockFormConverter, this.mockFormDto);
    Assert.assertEquals(expectedResult, result);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void getSurveyValidatorThrowsError() throws Exception {

    // setup
    SurveyResponseDto responseDto = new SurveyResponseDto();
    responseDto.setState(State.processed);
    responseDto.setForm(this.mockFormDto);

    ResponseEntity<SurveyResponseDto> response = new ResponseEntity<SurveyResponseDto>(responseDto, HttpStatus.OK);
    Long surveyId = 500L;

    EasyMock.replay(this.mockFormConverter);

    EasyMock.expect(
        this.mockRestTemplate.exchange(EasyMock.anyObject(String.class), EasyMock.anyObject(HttpMethod.class),
            EasyMock.anyObject(HttpEntity.class), EasyMock.anyObject(Class.class))).andReturn(response);
    EasyMock.replay(this.mockRestTemplate);

    this.mockFormDto.validate(EasyMock.anyObject(FormCreateValidator.class));
    EasyMock.expectLastCall().andThrow(new SurveyNotFoundException("not found"));
    EasyMock.replay(this.mockFormDto);

    // do test
    try {
      uut.getSurveyForm(surveyId);
      Assert.fail();
    } catch (Exception e) {
      Assert.assertTrue(e instanceof SurveyNotFoundException);
    }
    // Assertions
    EasyMock.verify(this.mockRestTemplate, this.mockFormConverter, this.mockFormDto);

  }

  @SuppressWarnings("unchecked")
  @Test
  public void getSurveyForErrorResponse() throws Exception {

    // setup
    SurveyResponseDto responseDto = new SurveyResponseDto();
    responseDto.setState(State.failed);
    responseDto.setErrorCode("ClientError");

    ResponseEntity<SurveyResponseDto> response = new ResponseEntity<SurveyResponseDto>(responseDto, HttpStatus.OK);
    Long surveyId = 500L;

    EasyMock.expect(
        this.mockRestTemplate.exchange(EasyMock.anyObject(String.class), EasyMock.anyObject(HttpMethod.class),
            EasyMock.anyObject(HttpEntity.class), EasyMock.anyObject(Class.class))).andReturn(response);
    EasyMock.replay(this.mockRestTemplate);
    
    EasyMock.replay(this.mockFormConverter);
    EasyMock.replay(this.mockFormDto);

    // do test
    try {
      uut.getSurveyForm(surveyId);
    } catch (ApplicationException applicationException) {
      Assert.assertEquals("SystemError", applicationException.getUserErrorCode());
    }
    EasyMock.verify(mockRestTemplate, mockFormConverter);
    EasyMock.verify(this.mockRestTemplate, this.mockFormConverter, this.mockFormDto);

  }
}