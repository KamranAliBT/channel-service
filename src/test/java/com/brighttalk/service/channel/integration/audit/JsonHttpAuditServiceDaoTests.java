/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JsonHttpAuditServiceDaoTests.java 97420 2015-07-01 10:37:21Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit;

import java.net.URI;
import java.net.URISyntaxException;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.WebcastAuditRecordDtoBuilder;

/**
 * Tests for {@link com.brighttalk.service.channel.integration.audit.JsonHttpAuditServiceDao JsonHttpAuditServiceDao}.
 */
public class JsonHttpAuditServiceDaoTests {

  private static final String BASE_SERVICE_URL = "http://audit.local.com";

  private static final Long USER_ID = 1L;

  private static final Long CHANNEL_ID = 2L;

  private static final Long COMMUNICATION_ID = 3L;

  private static final String ACTION_DESCRIPTION = "a description";

  private JsonHttpAuditServiceDao uut;

  private AuditRecordDtoBuilder mockAuditRecordDtoBuilder;

  private RestTemplate mockRestTemplate;

  /**
   * Set up for test.
   */
  @Before
  public void setUp() {

    uut = new JsonHttpAuditServiceDao();

    mockAuditRecordDtoBuilder = new WebcastAuditRecordDtoBuilder();

    // Mock the REST template
    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    ReflectionTestUtils.setField(uut, "auditServiceRestTemplate", mockRestTemplate);

    // Set the base service URL to be used
    ReflectionTestUtils.setField(uut, "auditServiceBaseUrl", BASE_SERVICE_URL);
  }

  /**
   * Tests calling the service to create an audit record from the supplied audit record.
   * 
   * @throws java.net.URISyntaxException
   * @throws org.springframework.web.client.RestClientException
   */
  @Test
  public void createAuditRecord() throws RestClientException, URISyntaxException {

    // Set up
    AuditRecordDto auditRecordDto = mockAuditRecordDtoBuilder.create(USER_ID, CHANNEL_ID,
        COMMUNICATION_ID, ACTION_DESCRIPTION);

    String expectedRequestUrl = BASE_SERVICE_URL + JsonHttpAuditServiceDao.CREATE_REQUEST_PATH;

    // Expectations
    EasyMock.expect(mockRestTemplate.postForLocation(expectedRequestUrl, auditRecordDto)).andReturn(new URI(""));
    EasyMock.replay(mockRestTemplate);

    // Do test
    uut.create(auditRecordDto);

    // Verify
    EasyMock.verify(mockRestTemplate);
  }

  /**
   * Tests calling the service to create an audit record when the supplied audit record is null.from the supplied audit
   * record.
   * 
   * @throws java.net.URISyntaxException
   * @throws org.springframework.web.client.RestClientException
   */
  @Test
  public void createAuditRecordWhenNull() throws RestClientException, URISyntaxException {

    // Set up

    // Expectations
    EasyMock.replay(mockRestTemplate);

    // Do test
    uut.create(null);

    // Verify
    EasyMock.verify(mockRestTemplate);
  }
}
