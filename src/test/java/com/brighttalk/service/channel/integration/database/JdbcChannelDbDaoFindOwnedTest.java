/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcChannelDbDaoFindOwnedTest.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria.SortBy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcChannelDbDaoFindOwnedTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcChannelDbDao uut;

  @Test
  public void findOwnedChannels() {

    int pageSize = 10;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
    assertEquals(pageNumber, result.getCurrentPageNumber());
    assertEquals(pageSize, result.getPageSize());
  }

  @Test
  public void findOwnedChannelsWithSummary() {

    addTemporaryChannelWithTypes();

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(1, result.size());
    final Channel resultChannel = result.get(0);

    assertEquals(CHANNEL_TITLE, resultChannel.getTitle());
    assertEquals(CHANNEL_DESCRIPTION, resultChannel.getDescription());
    assertEquals(CHANNEL_STRAPLINE, resultChannel.getStrapline());
    assertEquals(CHANNEL_KEYWORDS_LIST, resultChannel.getKeywords());
  }

  @Test
  public void findOwnedChannelsWithStatisticsViewedFor() {

    addTemporaryChannelWithTypes();

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(CHANNEL_VIEWED_FOR, result.get(0).getStatistics().getViewedFor());
  }

  @Test
  public void findOwnedChannelsWithStatisticsNumberOfUpcomingCommunicationsForUpcomingStatus() {

    Long channelId = addTemporaryChannelWithTypes();

    final long upcoming = 1L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers = 0L;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(1, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfRecordedCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfLiveCommunications());
  }

  @Test
  public void findOwnedChannelsWithStatisticsNumberOfLiveCommunicationsForLiveStatus() {

    Long channelId = addTemporaryChannelWithTypes();

    final long upcoming = 0L;
    final long recorded = 0L;
    final long live = 1L;
    final long subscribers = 0L;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(1, result.get(0).getStatistics().getNumberOfLiveCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfRecordedCommunications());
  }

  @Test
  public void findOwnedChannelsWithStatisticsNumberOfRecordedCommunicationsForRecordedStatus() {

    Long channelId = addTemporaryChannelWithTypes();

    final long upcoming = 0L;
    final long recorded = 1L;
    final long live = 0L;
    final long subscribers = 0L;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(0, result.get(0).getStatistics().getNumberOfLiveCommunications());
    assertEquals(0, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(1, result.get(0).getStatistics().getNumberOfRecordedCommunications());
  }

  @Test
  public void findOwnedChannelsWithStatisticsRatings() {

    addTemporaryChannelWithTypes();

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2.0f, result.get(0).getStatistics().getRating(), 0);
  }

  @Test
  public void findOwnedChannelsWithSubscribers() {

    Long channelId = addTemporaryChannelWithTypes();

    final long upcoming = 0L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers = 3L;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    final long expectedSubsribersCount = 3l;
    assertEquals(expectedSubsribersCount, result.get(0).getStatistics().getNumberOfSubscribers());
  }

  @Test
  public void findOwnedChannelsMultipleEntriesNoSortBy() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(channelId1, result.get(1).getId());
  }

  @Test
  public void findOwnedChannelsWithSortByCreatedDateDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.CREATEDDATE, SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(channelId1, result.get(1).getId());
  }

  @Test
  public void findOwnedChannelsWithSortByCreatedDateDescWithPageOneSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    int pageSize = 1;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.CREATEDDATE, SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(channelId2, result.get(0).getId());
  }

  @Test
  public void findOwnedChannelsWithSortByCreatedDateDescWithPageOneSizeOneHasMore() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    addTemporaryChannel(channels.get(0));
    addTemporaryChannel(channels.get(1));

    int pageSize = 1;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.CREATEDDATE, SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertTrue(result.hasNextPage());
  }

  @Test
  public void findOwnedChannelsWithSortByCreatedDateDescWithPageTwoSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    addTemporaryChannel(channels.get(1));

    int pageSize = 1;
    int pageNumber = 2;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.CREATEDDATE, SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(channelId1, result.get(0).getId());
  }

  @Test
  public void findOwnedChannelsWithSortByCreatedDateDescWithPageTwoSizeOneHasMore() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    addTemporaryChannel(channels.get(0));
    addTemporaryChannel(channels.get(1));

    int pageSize = 2;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.CREATEDDATE, SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertFalse(result.hasNextPage());
  }

  @Test
  public void findOwnedChannelsWithSortByCreatedDateAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.CREATEDDATE, SortOrder.ASC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(channelId2, result.get(1).getId());
  }

  @Test
  public void findOwnedChannelsWithSortByRatingDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Channel channel1 = channels.get(0);
    channel1.getStatistics().setRating(5);
    Long channelId1 = addTemporaryChannel(channel1);

    Channel channel2 = channels.get(1);
    channel2.getStatistics().setRating(1);
    Long channelId2 = addTemporaryChannel(channel2);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.AVERAGERATING, SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(channelId2, result.get(1).getId());
  }

  @Test
  public void findOwnedChannelsWithSortByRatingAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Channel channel1 = channels.get(0);
    channel1.getStatistics().setRating(5);
    Long channelId1 = addTemporaryChannel(channel1);

    Channel channel2 = channels.get(1);
    channel2.getStatistics().setRating(1);
    Long channelId2 = addTemporaryChannel(channel2);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.AVERAGERATING, SortOrder.ASC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(channelId1, result.get(1).getId());
  }

  @Test
  public void findOwnedChannelsWithSortByRecordedCommsAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final long upcoming = 0L;
    final Long recorded1 = 2L;
    final Long recorded2 = 1L;
    final long live = 0L;
    final long subscribers = 0L;
    addChannelStatistics(channelId1, upcoming, recorded1, live, subscribers);
    addChannelStatistics(channelId2, upcoming, recorded2, live, subscribers);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.RECORDEDWEBCASTS,
        SortOrder.ASC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(1, result.get(0).getStatistics().getNumberOfRecordedCommunications());
    assertEquals(channelId1, result.get(1).getId());
    assertEquals(2, result.get(1).getStatistics().getNumberOfRecordedCommunications());

  }

  @Test
  public void findOwnedChannelsWithSortByRecordedCommsDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final long upcoming = 0L;
    final Long recorded1 = 2L;
    final Long recorded2 = 1L;
    final long live = 0L;
    final long subscribers = 0L;
    addChannelStatistics(channelId1, upcoming, recorded1, live, subscribers);
    addChannelStatistics(channelId2, upcoming, recorded2, live, subscribers);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.RECORDEDWEBCASTS,
        SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(2, result.get(0).getStatistics().getNumberOfRecordedCommunications());
    assertEquals(channelId2, result.get(1).getId());
    assertEquals(1, result.get(1).getStatistics().getNumberOfRecordedCommunications());

  }

  @Test
  public void findOwnedChannelsWithSortByUpcomingCommsAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long upcoming1 = 2L;
    final Long upcoming2 = 1L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers = 0L;
    addChannelStatistics(channelId1, upcoming1, recorded, live, subscribers);
    addChannelStatistics(channelId2, upcoming2, recorded, live, subscribers);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.UPCOMINGWEBCASTS,
        SortOrder.ASC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId2, result.get(0).getId());
    assertEquals(1, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(channelId1, result.get(1).getId());
    assertEquals(2, result.get(1).getStatistics().getNumberOfUpcomingCommunications());

  }

  @Test
  public void findOwnedChannelsWithSortByUpcomingCommsDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final Long upcoming1 = 2L;
    final Long upcoming2 = 1L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers = 0L;
    addChannelStatistics(channelId1, upcoming1, recorded, live, subscribers);
    addChannelStatistics(channelId2, upcoming2, recorded, live, subscribers);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.UPCOMINGWEBCASTS,
        SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(2, result.get(0).getStatistics().getNumberOfUpcomingCommunications());
    assertEquals(channelId2, result.get(1).getId());
    assertEquals(1, result.get(1).getStatistics().getNumberOfUpcomingCommunications());

  }

  @Test
  public void findOwnedChannelsWithSortBySubscribers() {

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.SUBSCRIBERS, SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void findOwnedChannelsWithSortBySubscribersDesc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final long upcoming = 0L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers1 = 100L;
    final long subscribers2 = 2L;
    addChannelStatistics(channelId1, upcoming, recorded, live, subscribers1);
    addChannelStatistics(channelId2, upcoming, recorded, live, subscribers2);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.SUBSCRIBERS, SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(channelId2, result.get(1).getId());

    assertEquals(subscribers1, result.get(0).getStatistics().getNumberOfSubscribers());
    assertEquals(subscribers2, result.get(1).getStatistics().getNumberOfSubscribers());

    assertEquals(CHANNEL_TITLE + "0", result.get(0).getTitle());
    assertEquals(CHANNEL_TITLE + "1", result.get(1).getTitle());
  }

  @Test
  public void findOwnedChannelsWithSortBySubscribersWithSummary() {

    addTemporaryChannelWithTypes();

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.SUBSCRIBERS, SortOrder.DESC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(1, result.size());
    final Channel resultChannel = result.get(0);

    assertEquals(CHANNEL_TITLE, resultChannel.getTitle());
    assertEquals(CHANNEL_DESCRIPTION, resultChannel.getDescription());
    assertEquals(CHANNEL_STRAPLINE, resultChannel.getStrapline());
    assertEquals(CHANNEL_KEYWORDS_LIST, resultChannel.getKeywords());
  }

  @Test
  public void findOwnedChannelsWithSortBySubscribersAsc() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final long upcoming = 0L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers1 = 1L;
    final long subscribers2 = 2L;
    addChannelStatistics(channelId1, upcoming, recorded, live, subscribers1);
    addChannelStatistics(channelId2, upcoming, recorded, live, subscribers2);

    int pageSize = 50;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.SUBSCRIBERS, SortOrder.ASC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(2, result.size());
    assertEquals(channelId1, result.get(0).getId());
    assertEquals(channelId2, result.get(1).getId());

    assertEquals(1, result.get(0).getStatistics().getNumberOfSubscribers());
    assertEquals(2, result.get(1).getStatistics().getNumberOfSubscribers());
  }

  @Test
  public void findOwnedChannelsWithSortBySubscribersAscPageOneSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final long upcoming = 0L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers1 = 1L;
    final long subscribers2 = 2L;
    addChannelStatistics(channelId1, upcoming, recorded, live, subscribers1);
    addChannelStatistics(channelId2, upcoming, recorded, live, subscribers2);

    int pageSize = 1;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.SUBSCRIBERS, SortOrder.ASC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(channelId1, result.get(0).getId());
  }

  @Test
  public void findOwnedChannelsWithSortBySubscribersAscPageOneSizeOneHasMore() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final long upcoming = 0L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers1 = 1L;
    final long subscribers2 = 2L;
    addChannelStatistics(channelId1, upcoming, recorded, live, subscribers1);
    addChannelStatistics(channelId2, upcoming, recorded, live, subscribers2);

    int pageSize = 1;
    int pageNumber = 1;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.SUBSCRIBERS, SortOrder.ASC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertTrue(result.hasNextPage());
  }

  @Test
  public void findOwnedChannelsWithSortBySubscribersAscPageTwoSizeOne() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final long upcoming = 0L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers1 = 1L;
    final long subscribers2 = 2L;
    addChannelStatistics(channelId1, upcoming, recorded, live, subscribers1);
    addChannelStatistics(channelId2, upcoming, recorded, live, subscribers2);

    int pageSize = 1;
    int pageNumber = 2;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.SUBSCRIBERS, SortOrder.ASC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(channelId2, result.get(0).getId());
  }

  @Test
  public void findOwnedChannelsWithSortBySubscribersAscPageTwoSizeOneHasMore() {

    List<Channel> channels = createMultipleTemporaryChannelsWithTypes(2);

    Long channelId1 = addTemporaryChannel(channels.get(0));
    Long channelId2 = addTemporaryChannel(channels.get(1));

    final long upcoming = 0L;
    final long recorded = 0L;
    final long live = 0L;
    final long subscribers1 = 1L;
    final long subscribers2 = 2L;
    addChannelStatistics(channelId1, upcoming, recorded, live, subscribers1);
    addChannelStatistics(channelId2, upcoming, recorded, live, subscribers2);

    int pageSize = 1;
    int pageNumber = 2;

    MyChannelsSearchCriteria criteria = buildSearchCriteria(pageSize, pageNumber, SortBy.SUBSCRIBERS, SortOrder.ASC);

    // do test
    PaginatedList<Channel> result = uut.findPaginatedChannelsOwned(CHANNEL_USER_ID, criteria);

    assertFalse(result.hasNextPage());
  }

  private MyChannelsSearchCriteria buildSearchCriteria(final int pageSize, final int pageNumber) {

    SortBy sortBy = SortBy.CREATEDDATE;
    SortOrder sortOrder = SortOrder.DESC;

    return buildSearchCriteria(pageSize, pageNumber, sortBy, sortOrder);
  }

  private MyChannelsSearchCriteria buildSearchCriteria(final int pageSize, final int pageNumber, final SortBy sortBy,
      final SortOrder sortOrder) {

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(pageNumber);
    criteria.setPageSize(pageSize);
    criteria.setSortBy(sortBy);
    criteria.setSortOrder(sortOrder);

    return criteria;
  }

}