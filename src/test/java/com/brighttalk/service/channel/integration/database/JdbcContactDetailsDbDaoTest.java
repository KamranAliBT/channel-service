/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcContactDetailsDbDaoTest.java 98771 2015-08-04 16:31:59Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.business.error.ContactDetailsNotFoundException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcContactDetailsDbDaoTest extends JdbcDao {

  private static final String FIRST_NAME = "firstNameTest";

  private static final String LAST_NAME = "lastNameTest";

  private static final String EMAIL = "email@test.net";

  private static final String TELEPHONE = "078333333";

  private static final String JOB_TITLE = "dev";

  private static final String COMPANY_NAME = "BrightTALK";

  private static final String ADDRESS1 = "address1";

  private static final String ADDRESS2 = "address2";

  private static final String CITY = "RandomCity";

  private static final String STATE_REGION = "Rainforest";

  private static final String POSTCODE = "EC1A 4NA";

  private static final String COUNTRY = "Neverland";

  @Autowired
  private JdbcContactDetailsDbDao uut;

  /** DAO used to set-up test data and assert the state of the database. */
  private JdbcTemplate jdbcTemplate;

  private final List<Long> channelIdsWithCreatedContactDetails = new ArrayList<>();

  @Autowired
  public void setDataSource(final DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);
  }

  @Before
  public void setUp() {

  }

  @After
  public void tearDown() {

    jdbcTemplate.update("DELETE FROM contact_details WHERE channel_id in (?)",
        StringUtils.join(channelIdsWithCreatedContactDetails, ','));
  }

  @Test
  public void create() {

    ContactDetails contactDetails = createContactDetails();

    // do test
    long channelId = getChannelIdPrimaryKeyForContactDetailsTable();
    uut.create(channelId, contactDetails);

    // Check the results
    Map<String, Object> createdContactData = jdbcTemplate.queryForMap(
        "SELECT * FROM contact_details WHERE channel_id = ?", channelId);

    assertNotNull(createdContactData);
    assertEquals(FIRST_NAME, createdContactData.get("first_name"));
    assertEquals(LAST_NAME, createdContactData.get("last_name"));
    assertEquals(EMAIL, createdContactData.get("email"));
    assertEquals(TELEPHONE, createdContactData.get("telephone"));
    assertEquals(JOB_TITLE, createdContactData.get("job_title"));
    assertEquals(COMPANY_NAME, createdContactData.get("company_name"));
    assertEquals(ADDRESS1, createdContactData.get("address1"));
    assertEquals(ADDRESS2, createdContactData.get("address2"));
    assertEquals(CITY, createdContactData.get("city"));
    assertEquals(STATE_REGION, createdContactData.get("state_region"));
    assertEquals(POSTCODE, createdContactData.get("postcode"));
    assertEquals(COUNTRY, createdContactData.get("country"));
    assertNotNull(createdContactData.get("last_updated"));
    assertNotNull(createdContactData.get("created"));
  }

  @Test
  public void getContactDetails() {

    // Set up
    long channelId = getChannelIdPrimaryKeyForContactDetailsTable();
    ContactDetails contactDetails = createContactDetails();
    uut.create(channelId, contactDetails);

    // Expectations

    // Do test
    ContactDetails retrievedContactDetails = uut.get(channelId);

    // Assertions
    assertContactDetailsAreEqual(contactDetails, retrievedContactDetails);
  }

  @Test(expected = ContactDetailsNotFoundException.class)
  public void getContactDetailsWhenNoneExist() {

    // Set up
    long channelId = getChannelIdPrimaryKeyForContactDetailsTable();

    // Expectations

    // Do test
    uut.get(channelId);

    // Assertions
  }

  @Test
  public void updateContactDetails() {

    // Set up
    long channelId = getChannelIdPrimaryKeyForContactDetailsTable();
    ContactDetails contactDetails = createContactDetails();
    uut.create(channelId, contactDetails);

    // Contact details to update to
    ContactDetails newContactDetails = new ContactDetails();
    newContactDetails.setFirstName("Lego");
    newContactDetails.setLastName("Las");
    newContactDetails.setEmail("someone@somewhere.com");
    newContactDetails.setTelephone("12345");
    newContactDetails.setJobTitle("My job");
    newContactDetails.setCompany("My company");
    newContactDetails.setAddress1("First line address");
    newContactDetails.setAddress2("Second line address");
    newContactDetails.setCity("My city");
    newContactDetails.setState("My state");
    newContactDetails.setPostcode("PO5TC0D3");
    newContactDetails.setCountry("UK");

    // Expectations

    // Do test
    ContactDetails updatedContactDetails = uut.update(channelId, newContactDetails);

    // Assertions
    assertContactDetailsAreEqual(newContactDetails, updatedContactDetails);

    // Retrieve record from DB to be sure it has been updated
    ContactDetails contactDetailsFromDb = uut.get(channelId);
    assertContactDetailsAreEqual(newContactDetails, contactDetailsFromDb);
  }

  @Test
  public void updateContactDetailsWhenNoneExist() {

    // Set up
    long channelId = getChannelIdPrimaryKeyForContactDetailsTable();

    // Contact details to update to
    ContactDetails newContactDetails = new ContactDetails();
    newContactDetails.setFirstName("Lego");
    newContactDetails.setLastName("Las");
    newContactDetails.setEmail("someone@somewhere.com");
    newContactDetails.setTelephone("12345");
    newContactDetails.setJobTitle("My job");
    newContactDetails.setCompany("My company");
    newContactDetails.setAddress1("First line address");
    newContactDetails.setAddress2("Second line address");
    newContactDetails.setCity("My city");
    newContactDetails.setState("My state");
    newContactDetails.setPostcode("PO5TC0D3");
    newContactDetails.setCountry("UK");

    // Expectations

    // Do test
    ContactDetails updatedContactDetails = uut.update(channelId, newContactDetails);

    // Assertions
    assertContactDetailsAreEqual(newContactDetails, updatedContactDetails);

    // Retrieve record from DB to be sure it has been updated
    ContactDetails contactDetailsFromDb = uut.get(channelId);
    assertContactDetailsAreEqual(newContactDetails, contactDetailsFromDb);
  }

  private ContactDetails createContactDetails() {

    ContactDetails contactDetails = new ContactDetails();
    contactDetails.setFirstName(FIRST_NAME);
    contactDetails.setLastName(LAST_NAME);
    contactDetails.setEmail(EMAIL);
    contactDetails.setTelephone(TELEPHONE);
    contactDetails.setJobTitle(JOB_TITLE);
    contactDetails.setCompany(COMPANY_NAME);
    contactDetails.setAddress1(ADDRESS1);
    contactDetails.setAddress2(ADDRESS2);
    contactDetails.setCity(CITY);
    contactDetails.setState(STATE_REGION);
    contactDetails.setPostcode(POSTCODE);
    contactDetails.setCountry(COUNTRY);

    return contactDetails;
  }

  /**
   * Gets next suitable PK for the contact details table. This is the current maximum channel ID in the table + 1.
   * 
   * @return A channel ID suitable for use as a PK.
   */
  private long getChannelIdPrimaryKeyForContactDetailsTable() {

    long channelId = 0;
    String sql = "SELECT MAX(channel_id) FROM contact_details";
    Long maxChannelId = queryForLong(jdbcTemplate, sql);

    channelId = maxChannelId + 1;
    channelIdsWithCreatedContactDetails.add(channelId);
    return channelId;
  }

  private void assertContactDetailsAreEqual(final ContactDetails expected, final ContactDetails actual) {

    assertEquals(expected.getFirstName(), actual.getFirstName());
    assertEquals(expected.getLastName(), actual.getLastName());
    assertEquals(expected.getEmail(), actual.getEmail());
    assertEquals(expected.getTelephone(), actual.getTelephone());
    assertEquals(expected.getJobTitle(), actual.getJobTitle());
    assertEquals(expected.getCompany(), actual.getCompany());
    assertEquals(expected.getAddress1(), actual.getAddress1());
    assertEquals(expected.getAddress2(), actual.getAddress2());
    assertEquals(expected.getCity(), actual.getCity());
    assertEquals(expected.getState(), actual.getState());
    assertEquals(expected.getPostcode(), actual.getPostcode());
    assertEquals(expected.getCountry(), actual.getCountry());
  }
}