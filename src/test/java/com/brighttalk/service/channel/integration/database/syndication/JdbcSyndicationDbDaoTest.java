/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: JdbcSyndicationDbDaoTest.java 98711 2015-08-03 10:24:04Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database.syndication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationAuthorisationStatus;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationType;
import com.brighttalk.service.channel.integration.database.AbstractJdbcDbDaoTest;

/**
 * Integration Tests for {@link JdbcSyndicationDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcSyndicationDbDaoTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcSyndicationDbDao uut;
  private Long channel1Id;
  private Long channel2Id;
  private Long channel3Id;
  private Communication testCommunication;
  private long defaultChannelTypeId;

  /**
   * Test setup.
   */
  @Before
  public void setup() {
    defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    channel1Id = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    channel2Id = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    channel3Id = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    testCommunication = buildCommunication(channel1Id);
    addTemporaryCommunication(channel1Id, testCommunication);
  }

  /**
   * Tests {@link JdbcSyndicationDbDao#findConsumerChannelsSyndication} in the success case of finding consumer channels
   * syndication.
   * <p>
   * Setup - This tests creates a test communication in one channel then syndicate to 2 other channels.
   * <p>
   * Expected Results - List of 2 communication syndication containing the details of the distributed channels
   * syndication.
   */
  @Test
  public void findConsumerChannelsSyndicationSuccess() {
    // Setup - Syndicated the test communication to 2 distributed channels.
    syndicateCommunication(channel2Id, testCommunication);
    syndicateCommunication(channel3Id, testCommunication);

    // Do Test -
    List<CommunicationSyndication> communicationSyndications =
        uut.findConsumerChannelsSyndication(testCommunication.getId());

    // Assertion - 1st communication syndication
    assertNotNull(communicationSyndications);
    assertEquals(2, communicationSyndications.size());
    CommunicationSyndication actualCommunicationSyndication1 = communicationSyndications.get(0);
    Channel actualChannel1 = actualCommunicationSyndication1.getChannel();
    assertNotNull(actualChannel1);
    assertEquals(channel2Id, actualChannel1.getId());
    assertEquals(CHANNEL_TITLE, actualChannel1.getTitle());
    assertEquals(CHANNEL_ORGANISATION, actualChannel1.getOrganisation());
    assertEquals(SyndicationType.IN, actualCommunicationSyndication1.getSyndicationType());
    assertEquals(SyndicationAuthorisationStatus.APPROVED,
        actualCommunicationSyndication1.getMasterChannelSyndicationAuthorisationStatus());
    assertEquals(SyndicationAuthorisationStatus.APPROVED,
        actualCommunicationSyndication1.getConsumerChannelSyndicationAuthorisationStatus());
    assertNotNull(actualCommunicationSyndication1.getMasterChannelSyndicationAuthorisationTimestamp());
    assertNotNull(actualCommunicationSyndication1.getConsumerChannelSyndicationAuthorisationTimestamp());

    // 2nd communication syndication
    CommunicationSyndication actualCommunicationSyndication2 = communicationSyndications.get(1);
    Channel actualChannel2 = actualCommunicationSyndication2.getChannel();
    assertNotNull(actualChannel2);
    assertEquals(channel3Id, actualChannel2.getId());
    assertEquals(CHANNEL_TITLE, actualChannel2.getTitle());
    assertEquals(CHANNEL_ORGANISATION, actualChannel2.getOrganisation());
    assertEquals(SyndicationType.IN, actualCommunicationSyndication2.getSyndicationType());
    assertEquals(SyndicationAuthorisationStatus.APPROVED,
        actualCommunicationSyndication2.getMasterChannelSyndicationAuthorisationStatus());
    assertEquals(SyndicationAuthorisationStatus.APPROVED,
        actualCommunicationSyndication2.getConsumerChannelSyndicationAuthorisationStatus());
    assertNotNull(actualCommunicationSyndication2.getMasterChannelSyndicationAuthorisationTimestamp());
    assertNotNull(actualCommunicationSyndication2.getConsumerChannelSyndicationAuthorisationTimestamp());
  }

  /**
   * Tests {@link JdbcSyndicationDbDao#findConsumerChannelsSyndication} in the success case of finding consumer channels
   * syndication where the identified communication is not syndicated.
   * <p>
   * Expected Results - Empty list of syndications.
   */
  @Test
  public void findConsumerChannelsSyndicationNoSyndication() {
    // Do Test -
    List<CommunicationSyndication> communicationSyndications =
        uut.findConsumerChannelsSyndication(testCommunication.getId());

    // Assertion -
    assertNotNull(communicationSyndications);
    assertEquals(0, communicationSyndications.size());
  }

  /**
   * Tests {@link JdbcSyndicationDbDao#findConsumerChannelsSyndication} in the success case of finding consumer channels
   * syndication where the identified communication is deleted.
   * <p>
   * Expected Results - Empty list of syndications.
   */
  @Test
  public void findConsumerChannelsSyndicationWithDeletedCommunication() {
    // Setup - Syndicated the test communication to a distributed channel.
    syndicateCommunication(channel2Id, testCommunication);
    // Delete the test communication from the syndicated channel.
    this.jdbcTemplate.update("UPDATE asset SET is_active = 0 WHERE channel_id = " + channel2Id + " AND target_id =" +
        testCommunication.getId());

    // Do Test -
    List<CommunicationSyndication> communicationSyndications =
        uut.findConsumerChannelsSyndication(testCommunication.getId());

    // Assertion -
    assertNotNull(communicationSyndications);
    assertEquals(0, communicationSyndications.size());
  }

  /**
   * Tests {@link JdbcSyndicationDbDao#findConsumerChannelsSyndication} in the success case of finding consumer channels
   * syndication where the identified channel deleted after the communication syndication.
   * <p>
   * Expected Results - Empty list of syndications.
   */
  @Test
  public void findConsumerChannelsSyndicationWithDeletedChannel() {
    // Setup - Syndicated the test communication to a distributed channel.
    syndicateCommunication(channel2Id, testCommunication);
    // Delete the distributed channel.
    this.jdbcTemplate.update("UPDATE channel SET is_active = 0 WHERE id = " + channel2Id);

    // Do Test -
    List<CommunicationSyndication> communicationSyndications =
        uut.findConsumerChannelsSyndication(testCommunication.getId());

    // Assertion -
    assertNotNull(communicationSyndications);
    assertEquals(0, communicationSyndications.size());
  }

  /**
   * Tests {@link JdbcSyndicationDbDao#findConsumerChannelsSyndication} in the success case of finding consumer channels
   * syndication where one of the identified channels get deleted after the communication syndication and the syndicated
   * communication get deleted form one of the syndicated channels.
   * <p>
   * Setup - Test creates 3 distributed channel and syndicate a communication to all three, then delete the 1st channel
   * and delete the communication from the 3rd syndicated channel.
   * <p>
   * Expected Results - List of 1 consumer channel syndications, since the 1st channel got deleted and 3rd channel
   * syndicated communication got deleted.
   */
  @Test
  public void findConsumerChannelsSyndicationWithDeletedChannelAndCommunication() {
    // Setup -
    long channel4Id = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    syndicateCommunication(channel2Id, testCommunication);
    syndicateCommunication(channel3Id, testCommunication);
    syndicateCommunication(channel4Id, testCommunication);
    // Delete the 1st consumer channel.
    this.jdbcTemplate.update("UPDATE channel SET is_active = 0 WHERE id = " + channel2Id);
    // Delete the 3rd syndicated channel communication.
    this.jdbcTemplate.update("UPDATE asset SET is_active = 0 WHERE channel_id = " + channel4Id + " AND target_id =" +
        testCommunication.getId());

    // Do Test -
    List<CommunicationSyndication> communicationSyndications =
        uut.findConsumerChannelsSyndication(testCommunication.getId());

    // Assertion -
    assertNotNull(communicationSyndications);
    assertEquals(1, communicationSyndications.size());
    CommunicationSyndication actualCommunicationSyndication = communicationSyndications.get(0);
    assertEquals(channel3Id, actualCommunicationSyndication.getChannel().getId());
  }

}
