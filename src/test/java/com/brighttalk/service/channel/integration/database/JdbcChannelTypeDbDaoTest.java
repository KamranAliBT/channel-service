/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcChannelTypeDbDaoTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.business.error.NotFoundException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcChannelTypeDbDaoTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcChannelTypeDbDao uut;

  @Test
  public void findDefault() {

    final String expectedDefaultName = "default";

    // do test
    ChannelType result = uut.findDefault();

    assertNotNull(result);
    assertNotNull(result.getId());
    assertEquals(expectedDefaultName, result.getName());
    assertNull(result.getEmailGroup());
  }

  @Test
  public void findByName() {

    final String expectedName = "expectedName";
    final String expectedEmailGroup = "premium";

    ChannelType channelType = new ChannelType();
    channelType.setName(expectedName);
    channelType.setIsEnabled(true);
    channelType.setEmailGroup(expectedEmailGroup);

    Long expectedId = addTemporaryChannelType(channelType, true);

    // do test
    ChannelType result = uut.find(expectedName);

    assertNotNull(result);
    assertEquals(expectedId, result.getId());
    assertEquals(expectedName, result.getName());
    assertEquals(expectedEmailGroup, result.getEmailGroup());
  }

  @Test(expected = NotFoundException.class)
  public void findByNameWhenNoSuchName() {

    final String expectedName = "expectedName";

    ChannelType channelType = new ChannelType();
    channelType.setName(expectedName);
    channelType.setIsEnabled(true);

    addTemporaryChannelType(channelType, true);

    // do test
    uut.find("SomeOtherName");
  }

  @Test(expected = NotFoundException.class)
  public void findByNameWhenNotActive() {

    final String expectedName = "expectedName";

    ChannelType channelType = new ChannelType();
    channelType.setName(expectedName);
    channelType.setIsEnabled(true);
    channelType.setEmailGroup("premium");

    addTemporaryChannelType(channelType, false);

    // do test
    uut.find(expectedName);
  }

  @Test(expected = NotFoundException.class)
  public void findByNameWhenNotEnabled() {

    final String expectedName = "expectedName";

    ChannelType channelType = new ChannelType();
    channelType.setName(expectedName);
    channelType.setIsEnabled(false);

    addTemporaryChannelType(channelType, true);

    // do test
    uut.find(expectedName);
  }
}