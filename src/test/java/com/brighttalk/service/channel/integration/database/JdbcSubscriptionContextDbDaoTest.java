/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: JdbcSubscriptionContextDbDaoTest.java 95321 2015-05-19 14:59:11Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.channel.Subscription.Referral;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.utils.DomainFactoryUtils;

/**
 * Integration test for {@link JdbcSubscriptionContextDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcSubscriptionContextDbDaoTest {

  private static final Long SUBSCRIPTION_ID = 123878L;
  private static final Long CHANNEL_ID = 99999L;
  private static final Long USER_ID = 45678L;
  private static final String LEAD_CONTEXT = "1111";
  private static final String ENGAGEMENT_SCORE = "88";
  private static final String EXPECT_EXCEPTION_MSG = "Expected an exception to be thrown.";

  /** DAO used to set-up test data and assert the state of the database. */
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private JdbcSubscriptionContextDbDao uut;

  private SubscriptionContext context;

  private SimpleJdbcInsert insertSubscription;

  private Long testSubscriptionId;

  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
    insertSubscription = new SimpleJdbcInsert(dataSource).withTableName("subscription");
  }

  /**
   * Test setup.
   */
  @Before
  public void setup() {
    context = DomainFactoryUtils.createSubscriptionContext();
    context.setSubscriptionId(SUBSCRIPTION_ID);
  }

  /**
   * Test TearDown.
   */
  @After
  public void teardown() {
    this.jdbcTemplate.update("DELETE FROM subscription_context WHERE subscription_id = ?", SUBSCRIPTION_ID);
    if (testSubscriptionId != null) {
      this.jdbcTemplate.update("DELETE FROM subscription WHERE id = ?", testSubscriptionId);
    }
  }

  /**
   * Tests {@link JdbcSubscriptionContextDbDao#create(SubscriptionContext)} in the success case of creating subscription
   * context and checking subscription last updated date updated to current date.
   * <p>
   * This tests check when creating new context we are also updating the context's subscription last updated date.
   * 
   * @throws Exception in case of any errors.
   */
  @Test
  public void createSubscriptionContextSuccess() throws Exception {
    // Setup- Create a subscription with a specific date/time;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date testCreatedDate = dateFormat.parse("2010-05-14 16:09:09");
    testSubscriptionId = createSubscription(testCreatedDate);
    // Set the test context with the created subscription Id.
    context.setSubscriptionId(testSubscriptionId);

    // Do Test -
    uut.create(context);

    // Expectation -
    Map<String, Object> createdContextData =
        this.jdbcTemplate.queryForMap("SELECT * FROM subscription_context WHERE subscription_id = ?",
            testSubscriptionId);
    assertEquals(testSubscriptionId.longValue(), ((Integer) createdContextData.get("subscription_id")).longValue());
    assertEquals(SubscriptionLeadType.SUMMIT.getType(), createdContextData.get("lead_type"));
    assertEquals(LEAD_CONTEXT, createdContextData.get("lead_context"));
    assertEquals(Integer.parseInt(ENGAGEMENT_SCORE), createdContextData.get("engagement_score"));

    // Assertion - Assert when creating context, we are also updating the subscription last updated date. i.e. the
    // subscription last updated date/time will be after the created date used when creating the existing subscription.
    Map<String, Object> updatedSubscriptionData =
        this.jdbcTemplate.queryForMap("SELECT * FROM subscription WHERE id = ?", testSubscriptionId);
    Timestamp ts = ((Timestamp) updatedSubscriptionData.get("last_updated"));
    Date subscriptionLastUpdated = new Date(ts.getTime());
    assertTrue("The actual subscription last updated date [" + subscriptionLastUpdated +
        "] is not after the expected subscription last updated date [" + testCreatedDate + "].",
        subscriptionLastUpdated.after(testCreatedDate));
  }

  /**
   * Tests {@link JdbcSubscriptionContextDbDao#create(SubscriptionContext)} in the success case of creating subscription
   * context no engagement Score. Engagement Score is optional and only required for summit and keyword subscription
   * context.
   */
  @Test
  public void createSubscriptionContextWithMissingEngagementScore() {
    // Setup -
    context.setLeadType(SubscriptionLeadType.CONTENT);
    context.setEngagementScore(null);

    // Do Test -
    uut.create(context);

    // Expectation -
    Map<String, Object> createdContextData =
        this.jdbcTemplate.queryForMap("SELECT * FROM subscription_context WHERE subscription_id = ?", SUBSCRIPTION_ID);
    assertEquals(SUBSCRIPTION_ID.longValue(), ((Integer) createdContextData.get("subscription_id")).longValue());
    assertEquals(SubscriptionLeadType.CONTENT.getType(), createdContextData.get("lead_type"));
    assertEquals(LEAD_CONTEXT, createdContextData.get("lead_context"));
    assertNull(createdContextData.get("engagement_score"));
  }

  /**
   * Tests {@link JdbcSubscriptionContextDbDao#create(SubscriptionContext)} in the error case of creating subscription
   * context without subscription Id.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void createSubscriptionContextWithMissingSubscriptionId() {
    // Setup -
    context.setSubscriptionId(null);

    // Do Test -
    uut.create(context);
    fail(EXPECT_EXCEPTION_MSG);
  }

  /**
   * Tests {@link JdbcSubscriptionContextDbDao#create(SubscriptionContext)} in the error case of creating subscription
   * context without lead type.
   */
  @Test(expected = NullPointerException.class)
  public void createSubscriptionContextWithMissingLeadType() {
    // Setup -
    context.setLeadType(null);

    // Do Test -
    uut.create(context);
    fail(EXPECT_EXCEPTION_MSG);
  }

  /**
   * Tests {@link JdbcSubscriptionContextDbDao#create(SubscriptionContext)} in the error case of creating subscription
   * context without lead context.
   */
  @Test(expected = DataIntegrityViolationException.class)
  public void createSubscriptionContextWithMissingLeadContext() {
    // Setup -
    context.setLeadContext(null);
    // Do Test -
    uut.create(context);
    fail(EXPECT_EXCEPTION_MSG);
  }

  /**
   * Tests {@link JdbcSubscriptionContextDbDao#findBySubscriptionId(Long)} in the success case of finding subscription
   * context by subscription Id.
   */
  @Test
  public void findBySubscriptionIdSuccess() {
    // Setup -
    uut.create(context);

    // Do Test
    SubscriptionContext foundContext = uut.findBySubscriptionId(context.getSubscriptionId());

    // Expectation -
    assertNotNull(foundContext);
    assertEquals(context.getSubscriptionId(), foundContext.getSubscriptionId());
    assertEquals(context.getLeadType(), foundContext.getLeadType());
    assertEquals(context.getLeadContext(), foundContext.getLeadContext());
    assertEquals(context.getEngagementScore(), foundContext.getEngagementScore());
  }

  /**
   * Tests {@link JdbcSubscriptionContextDbDao#findBySubscriptionId(Long)} in the case of not finding subscription
   * context by subscription Id.
   */
  @Test
  public void findBySubscriptionIdContextNotFound() {
    // Do Test
    SubscriptionContext foundContext = uut.findBySubscriptionId(context.getSubscriptionId());

    // Expectation -
    assertNull(foundContext);
  }

  /**
   * Creates a test subscription with supplied created date.
   * 
   * @param createdDate The created date used when created subscription
   * @return The subscription Id.
   */
  private long createSubscription(Date createdDate) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", CHANNEL_ID);
    params.addValue("user_id", USER_ID);
    params.addValue("is_active", 1);
    params.addValue("last_subscribed", createdDate);
    params.addValue("referal", Referral.paid);
    params.addValue("created", createdDate);
    params.addValue("last_updated", createdDate);
    insertSubscription.setGeneratedKeyName("id");
    return insertSubscription.executeAndReturnKey(params).longValue();
  }
}
