/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EmailRequestConverterTest.java 88525 2015-01-20 12:15:30Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.email.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.integration.email.dto.EmailRequestDto;

public class EmailRequestConverterTest {

  private static final Long CHANNEL_ID = 666l;

  private static final Long COMMUNICATION_ID = 999l;

  private static final String EMAIL_GROUP = "premium";

  private static final String CHANNEL_TITLE = "channelType";

  private static final Long CHANNEL_TYPE_ID = 13L;

  private static final Long OWNER_USER_ID = 7L;

  private static final String CUSTOM_URL = "http://custom.url";

  private EmailRequestConverter uut;

  @Before
  public void setUp() {

    uut = new EmailRequestConverter();
  }

  @Test
  public void convert() {

    final Channel channel = createChannel();
    final String expectedEmailType = EmailRequestConverter.EMAIL_TYPE_PATTERN.replace("{emailGroup}", EMAIL_GROUP);

    // do test
    EmailRequestDto result = uut.convert(channel);

    assertNotNull(result);

    assertNotNull(result.getEmail());
    assertEquals(expectedEmailType, result.getEmail().getType());

    assertNotNull(result.getEmail().getChannel());
    assertEquals(CHANNEL_ID, result.getEmail().getChannel().getId());
    assertEquals(CHANNEL_TITLE, result.getEmail().getChannel().getTitle());
    assertEquals(OWNER_USER_ID, result.getEmail().getChannel().getOwnerUserID());

    final String expectedChannelTypeId = CHANNEL_TYPE_ID.toString();
    assertEquals(expectedChannelTypeId, result.getEmail().getChannel().getType());
  }

  @Test
  public void convertForCreate() {
    convertTest(EmailRequestConverter.COMMUNICATION_SCHEDULED_EMAIL_TYPE);
  }

  @Test
  public void convertForUpdate() {
    convertTest(EmailRequestConverter.COMMUNICATION_RESCHEDULED_EMAIL_TYPE);
  }

  @Test
  public void convertForCancel() {
    convertTest(EmailRequestConverter.COMMUNICATION_CANCELLED_EMAIL_TYPE);
  }

  @Test
  public void convertForMissedYou() {
    convertTest(EmailRequestConverter.COMMUNICATION_MISSED_YOU_EMAIL_TYPE);
  }

  @Test
  public void convertForVideoUploadComplete() {
    convertTest(EmailRequestConverter.VIDEO_UPLOAD_COMPLETE);
  }

  @Test
  public void convertForVideoUploadError() {
    convertTest(EmailRequestConverter.VIDEO_UPLOAD_ERROR);
  }

  private void convertTest(final String emailType) {
    // set up
    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().withCustomUrl(
        CUSTOM_URL).build();

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).withConfiguration(communicationConfiguration).build();
    Long communcationDateAndTime = communication.getScheduledDateTime().getTime() / 1000;
    String expectedEmailType = emailType;

    // expectations

    // excerise
    EmailRequestDto emailRequestDto = null;
    switch (emailType) {
      case EmailRequestConverter.COMMUNICATION_SCHEDULED_EMAIL_TYPE:
        emailRequestDto = uut.convertForCreate(communication);
        break;
      case EmailRequestConverter.COMMUNICATION_RESCHEDULED_EMAIL_TYPE:
        emailRequestDto = uut.convertForUpdate(communication);
        break;
      case EmailRequestConverter.COMMUNICATION_CANCELLED_EMAIL_TYPE:
        emailRequestDto = uut.convertForCancel(communication);
        break;
      case EmailRequestConverter.COMMUNICATION_MISSED_YOU_EMAIL_TYPE:
        emailRequestDto = uut.convertForMissedYou(communication);
        break;
      case EmailRequestConverter.VIDEO_UPLOAD_COMPLETE:
        emailRequestDto = uut.convertForVideoUploadComplete(communication);
        break;
      case EmailRequestConverter.VIDEO_UPLOAD_ERROR:
        emailRequestDto = uut.convertForVideoUploadError(communication);
        break;
    }

    // assertions
    assertNotNull(emailRequestDto);
    assertNotNull(emailRequestDto.getEmail());
    assertEquals(expectedEmailType, emailRequestDto.getEmail().getType());

    assertNotNull(emailRequestDto.getEmail().getCommunication());
    assertEquals(communication.getChannelId(), emailRequestDto.getEmail().getCommunication().getChannel().getId());
    assertEquals(communication.getId(), emailRequestDto.getEmail().getCommunication().getId());

    if (!emailType.equals(EmailRequestConverter.COMMUNICATION_MISSED_YOU_EMAIL_TYPE)) {
      assertEquals(communication.getTitle(), emailRequestDto.getEmail().getCommunication().getTitle());
      assertEquals(communication.getStatus().toString().toLowerCase(),
          emailRequestDto.getEmail().getCommunication().getStatus());
      assertEquals(communcationDateAndTime, emailRequestDto.getEmail().getCommunication().getScheduled());
      assertEquals(communication.getAuthors(), emailRequestDto.getEmail().getCommunication().getPresenter());
      assertEquals(communication.getConfiguration().getCustomUrl(),
          emailRequestDto.getEmail().getCommunication().getCustomUrl());

    }

  }

  private Channel createChannel() {

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);
    channel.setTitle(CHANNEL_TITLE);

    ChannelType channelType = new ChannelType();
    channelType.setId(CHANNEL_TYPE_ID);
    channel.setType(channelType);

    channel.setOwnerUserId(OWNER_USER_ID);

    ChannelType pendingChannelType = new ChannelType();
    pendingChannelType.setEmailGroup(EMAIL_GROUP);
    channel.setPendingType(pendingChannelType);

    return channel;
  }

}