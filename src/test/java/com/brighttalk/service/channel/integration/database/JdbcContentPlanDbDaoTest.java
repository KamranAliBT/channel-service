/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcContentPlanDbDaoTest.java 70061 2013-10-21 16:34:03Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.DefaultTimestampToolbox;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.TimestampToolbox;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;

/**
 * Tests {link JdbcContentPlanDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcContentPlanDbDaoTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcContentPlanDbDao uut;

  @Autowired
  private TimestampToolbox timestampToolBox;

  @Test
  public void countCommunicationsInPeriodReturnsNoCommunications() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    boolean includeSyndicatedCommunications = true;

    Long channelId = addChannel();

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 0L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriodReturnsOneCommunication() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 1L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_IgnoreDeleted() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).withStatus(Status.DELETED).build();

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 0L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_IgnoreCancelled() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).withStatus(Status.CANCELLED).build();

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 0L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_IgnorePrivate() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();
    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().withVisibility(
        CommunicationConfiguration.Visibility.PRIVATE).build();

    communication.setConfiguration(communicationConfiguration);

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 0L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_BeforePeriod() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, -1, "seconds");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 0L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_LastSecondOfPeriod() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = endPeriod;

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 1L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_AfterPeriod() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(endPeriod, 1, "seconds");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 0L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_ExcludedCommunication() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(endPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    // expectations
    Long expectedCommunicationCount = 0L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_SyndicatedIn() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();
    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().withSyndicationType(
        CommunicationSyndication.SyndicationType.IN).withMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED).withConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED).build();

    communication.setConfiguration(communicationConfiguration);

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 1L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_SyndicatedOut() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();
    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().withSyndicationType(
        CommunicationSyndication.SyndicationType.OUT).build();

    communication.setConfiguration(communicationConfiguration);

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = false;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 1L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_SyndicatedIn_Ignored() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();
    CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().withSyndicationType(
        CommunicationSyndication.SyndicationType.IN).withMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED).withConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED).build();

    communication.setConfiguration(communicationConfiguration);

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = false;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 0L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_MediaZone() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).withProvider(
        new Provider(Provider.MEDIAZONE)).build();

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = true;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 1L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_SyndicationIgnored_NotSyndicatedCommunicationStillIncluded() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Date scheduledDateAndTime = getPeriod(startPeriod, 1, "months");

    Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

    addTemporaryCommunication(channelId, communication);

    boolean includeSyndicatedCommunications = false;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = 1L;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_MultipleCommunications() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Long numberOfCommunications = 10l;

    for (int i = 1; i <= numberOfCommunications; i++) {
      Date scheduledDateAndTime = getPeriod(startPeriod, i * 24 * 60 * 60, "seconds");
      Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
          scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

      addTemporaryCommunication(channelId, communication);
    }

    boolean includeSyndicatedCommunications = false;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = numberOfCommunications;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_MultipleCommunications_OneExcluded() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();

    Long numberOfCommunications = 10l;

    Date excludedScheduledDateAndTime = getPeriod(startPeriod, 3600, "seconds");
    Communication excludedCommunication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
        excludedScheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

    Long excludedCommunicationId = addTemporaryCommunication(channelId, excludedCommunication);

    for (int i = 1; i <= numberOfCommunications; i++) {
      Date scheduledDateAndTime = getPeriod(startPeriod, i * 24 * 60 * 60, "seconds");
      Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
          scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

      addTemporaryCommunication(channelId, communication);
    }

    boolean includeSyndicatedCommunications = false;

    // expectations
    Long expectedCommunicationCount = numberOfCommunications;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, excludedCommunicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_SyndicatedCommunicationInactive() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();
    Long numberOfCommunications = 10l;

    for (int i = 1; i <= numberOfCommunications; i++) {
      boolean isActive = (i == 6) ? false : true;
      Date scheduledDateAndTime = getPeriod(startPeriod, i * 24 * 60 * 60, "seconds");
      Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
          scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

      addTemporaryCommunication(channelId, communication, "communication", isActive);
    }

    boolean includeSyndicatedCommunications = false;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = numberOfCommunications - 1;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  @Test
  public void countCommunicationsInPeriod_SyndicatedCommunication_ExcludedFromChannelCapacity() {
    // Set up
    Date startPeriod = getStartAndEndPeriod().get(TimestampToolbox.START_PERIOD);
    Date endPeriod = getStartAndEndPeriod().get(TimestampToolbox.END_PERIOD);

    Long channelId = addChannel();
    Long numberOfCommunications = 10l;

    for (int i = 1; i <= numberOfCommunications; i++) {
      boolean excludeFromContentPlan = (i == 6) ? true : false;
      Date scheduledDateAndTime = getPeriod(startPeriod, i * 24 * 60 * 60, "seconds");

      Communication communication = new CommunicationBuilder().withDefaultValues().withScheduledDateTime(
          scheduledDateAndTime).withChannelId(channelId).withMasterChannelId(channelId).build();

      CommunicationConfiguration communicationConfiguration = new CommunicationConfigurationBuilder().withDefaultValues().withExcludeFromContentPlan(
          excludeFromContentPlan).build();

      communication.setConfiguration(communicationConfiguration);

      addTemporaryCommunication(channelId, communication);

    }

    boolean includeSyndicatedCommunications = false;

    Long communicationId = null;

    // expectations
    Long expectedCommunicationCount = numberOfCommunications - 1;

    // do test
    doCommunicationCount(channelId, startPeriod, endPeriod, includeSyndicatedCommunications, communicationId,
        expectedCommunicationCount);
  }

  private Long addChannel() {
    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    return channelId;
  }

  private void doCommunicationCount(final Long channelId, final Date startPeriod, final Date endPeriod,
    final boolean includeSyndicatedCommunications, final Long communicationId, final Long expectedCommunicationCount) {
    // do test
    Long communicationCount = uut.countCommunicationsInPeriod(channelId, startPeriod, endPeriod,
        includeSyndicatedCommunications, communicationId);

    // assertions
    assertNotNull(communicationCount);
    assertEquals(expectedCommunicationCount, communicationCount);
  }

  private HashMap<String, Date> getStartAndEndPeriod() {
    HashMap<String, Date> period = new HashMap<String, Date>();

    // Set up
    int periodLength = 12;

    Date startPeriod = getDateInUTC();
    Date endPeriod = getPeriod(startPeriod, periodLength, "months");

    period.put(TimestampToolbox.START_PERIOD, startPeriod);
    period.put(TimestampToolbox.END_PERIOD, endPeriod);

    return period;
  }

  private Date getPeriod(final Date startPeriod, final int period, final String periodType) {
    DefaultTimestampToolbox timestampToolbox = new DefaultTimestampToolbox();
    if (periodType == "months") {
      return timestampToolbox.addMonths(startPeriod, period);
    } else {
      return timestampToolbox.addSeconds(startPeriod, period);
    }
  }
}