/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationDbDaoTest.java 85869 2014-11-11 11:17:11Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.MediazoneConfig;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Tests {@link JdbcCommunicationDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcMediazoneCommunicationDbDaoTest extends AbstractJdbcDbDaoTest {

  private static final long TEST_CHANNEL_ID = 9999;
  private static final long TEST_MEDIAZONE_ID = 8888;

  @Autowired
  private JdbcMediazoneCommunicationDbDao uut;

  /**
   * Find the mediazone config for the given communication id.
   */
  @Test
  public void testFindMediazoneConfig() {
    String providerName = Provider.MEDIAZONE;
    Provider mediazoneProvider = new Provider(providerName);
    String configUrl = "http://mediazones.brighttalk.com/sitedata/7647966b7343c29048673252e490f736/communication/25852/flashplayer.xml";
    String mediazoneId = String.valueOf(TEST_MEDIAZONE_ID);

    Communication testCommunication = new CommunicationBuilder().withDefaultValues().withChannelId(TEST_CHANNEL_ID).withProvider(
        mediazoneProvider).build();
    Long temporaryCommunicationId = addTemporaryCommunication(TEST_CHANNEL_ID, testCommunication);

    addMediazoneCommunicationAsset(configUrl, mediazoneId, temporaryCommunicationId, true);

    // do test
    MediazoneConfig result = uut.findMediazoneConfig(temporaryCommunicationId);

    assertEquals(result.getId(), temporaryCommunicationId);
    assertEquals(result.getTargetMediazoneId(), mediazoneId);
    assertEquals(result.getTargetMediazoneConfigUrl(), configUrl);
    assertNotNull(result.getLastUpdated());

    temporaryCommunicationIds.add(temporaryCommunicationId);
  }

  /**
   * No mediazone config found for the given communication id.
   */
  @Test
  public void testMediazoneConfigNotFound() {
    String providerName = Provider.MEDIAZONE;
    Provider mediazoneProvider = new Provider(providerName);

    Communication testCommunication = new CommunicationBuilder().withDefaultValues().withChannelId(TEST_CHANNEL_ID).withProvider(
        mediazoneProvider).build();
    Long temporaryCommunicationId = addTemporaryCommunication(TEST_CHANNEL_ID, testCommunication);

    // do test
    MediazoneConfig result = uut.findMediazoneConfig(temporaryCommunicationId);

    assertNull(result);

    temporaryCommunicationIds.add(temporaryCommunicationId);
  }

}