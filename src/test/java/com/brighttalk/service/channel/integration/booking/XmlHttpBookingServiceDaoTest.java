/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpBookingServiceDaoTest.java 83838 2014-09-24 11:10:48Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.booking.dto.BookingRequestDto;
import com.brighttalk.service.channel.integration.booking.dto.converter.BookingRequestConverter;

public class XmlHttpBookingServiceDaoTest {

  private static final String SERVICE_BASE_URL = "http://test.booking.brighttalk.net";

  private XmlHttpBookingServiceDao uut;

  private RestTemplate mockRestTemplate;

  private BookingRequestConverter mockConverter;

  @Before
  public void setUp() {

    uut = new XmlHttpBookingServiceDao();

    uut.setServiceBaseUrl(SERVICE_BASE_URL);

    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    uut.setRestTemplate(mockRestTemplate);

    mockConverter = EasyMock.createMock(BookingRequestConverter.class);
    uut.setConverter(mockConverter);

  }

  @Test
  public void deleteBookings() throws Exception {

    final List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add(1L);
    final BookingRequestDto requestDto = new BookingRequestDto();
    final String expectedRequestUrl = SERVICE_BASE_URL + XmlHttpBookingServiceDao.DELETE_REQUEST_PATH;

    EasyMock.expect(mockConverter.convert(communicationIds)).andReturn(requestDto);
    EasyMock.expect(mockRestTemplate.postForLocation(expectedRequestUrl, requestDto)).andReturn(new URI(""));

    EasyMock.replay(mockConverter, mockRestTemplate);

    // do test
    uut.deleteBookings(communicationIds);

    // verification
    EasyMock.verify(mockConverter, mockRestTemplate);
  }

}