/**
 * *****************************************************************************
 * * Copyright BrightTALK Ltd, 2012.
 * * All Rights Reserved.
 * * Id: JdbcCommunicationStatisticsDbDaoTest.java
 * *****************************************************************************
 * @package com.brighttalk.service.channel.integration.database
 */
package com.brighttalk.service.channel.integration.database.statistics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.builder.CommunicationStatisticsBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.integration.database.AbstractJdbcDbDaoTest;

/**
 * Test Case for {@link JdbcCommunicationStatisticsDbDao}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCommunicationStatisticsDbDaoTest extends AbstractJdbcDbDaoTest {

  private static long TEST_USER_ID = 3378;
  private static final int NO_OF_VIEWINGS = 100;
  private static final long TOTAL_VIEWINGS_DURATION = 1000;
  private static final int NO_OF_RATINGS = 10;
  private static final float AVERAGE_RATINGS = 1.0f;

  @Autowired
  private JdbcCommunicationStatisticsDbDao uut;

  private SimpleJdbcInsert simpleJdbcInsertCommunicationStatistics;

  private final List<CommunicationStatisticsPrimaryKey> communicationStatisticsToDelete =
      new ArrayList<CommunicationStatisticsPrimaryKey>();

  private final List<Communication> communicationsChannel1 = new ArrayList<Communication>();

  private final List<Communication> communicationsChannel2 = new ArrayList<Communication>();

  private Long channel1Id;

  private Long channel2Id;

  private Long channel3Id;

  private long communicationId1;

  private CommunicationStatistics expectedCommStatistics1;

  @Autowired
  @Override
  public void setDataSource(final DataSource dataSource) {
    super.setDataSource(dataSource);

    simpleJdbcInsertCommunicationStatistics = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertCommunicationStatistics.withTableName("communication_statistics");
    simpleJdbcInsertCommunicationStatistics.usingColumns("channel_id", "communication_id", "rating", "num_ratings",
        "num_views", "viewed_for");
  }

  /**
   * Set up this test case.
   * 
   * Create some categories for two communicationsChannel1 in two channels.
   */
  @Before
  public void setUp() {

    // set up a couple of temp channel
    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    channel1Id = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    channel2Id = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    channel3Id = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    // add some communicationsChannel1 to those channels
    communicationId1 = super.addTemporaryCommunication(channel1Id, Provider.BRIGHTTALK);
    Long communicationId2 = super.addTemporaryCommunication(channel1Id, Provider.BRIGHTTALK);
    Long communicationId3 = super.addTemporaryCommunication(channel1Id, Provider.BRIGHTTALK);

    Long communicationId4 = super.addTemporaryCommunication(channel2Id, Provider.BRIGHTTALK);
    Long communicationId5 = super.addTemporaryCommunication(channel2Id, Provider.BRIGHTTALK);
    Long communicationId6 = super.addTemporaryCommunication(channel2Id, Provider.BRIGHTTALK);

    communicationsChannel1.add(new Communication(communicationId1));
    communicationsChannel1.add(new Communication(communicationId2));
    communicationsChannel1.add(new Communication(communicationId3));

    communicationsChannel2.add(new Communication(communicationId4));
    communicationsChannel2.add(new Communication(communicationId5));
    communicationsChannel2.add(new Communication(communicationId6));

    // add some data in the Db using the channel/comm ids we have just created
    expectedCommStatistics1 =
        new CommunicationStatistics(communicationId1, NO_OF_VIEWINGS, TOTAL_VIEWINGS_DURATION, NO_OF_RATINGS,
            AVERAGE_RATINGS);
    this.insertCommunicationStatistics(channel1Id, expectedCommStatistics1);
    this.insertCommunicationStatistics(channel1Id, new CommunicationStatistics(communicationId2,
        NO_OF_VIEWINGS + 100, TOTAL_VIEWINGS_DURATION + 1000, NO_OF_RATINGS + 10, AVERAGE_RATINGS + 1.0f));
    this.insertCommunicationStatistics(channel1Id, new CommunicationStatistics(communicationId3,
        NO_OF_VIEWINGS + 200, TOTAL_VIEWINGS_DURATION + 2000, NO_OF_RATINGS + 20, AVERAGE_RATINGS + 2.0f));

    this.insertCommunicationStatistics(channel2Id, new CommunicationStatistics(communicationId4,
        NO_OF_VIEWINGS + 300, TOTAL_VIEWINGS_DURATION + 3000, NO_OF_RATINGS + 30, AVERAGE_RATINGS + 3.0f));
    this.insertCommunicationStatistics(channel2Id, new CommunicationStatistics(communicationId5,
        NO_OF_VIEWINGS + 400, TOTAL_VIEWINGS_DURATION + 4000, NO_OF_RATINGS + 40, AVERAGE_RATINGS + 4.0f));
    this.insertCommunicationStatistics(channel2Id, new CommunicationStatistics(communicationId6,
        NO_OF_VIEWINGS + 500, TOTAL_VIEWINGS_DURATION + 5000, NO_OF_RATINGS + 50, AVERAGE_RATINGS + 5.0f));
  }

  /**
   * Tear down this test case. Delete categories we created in the DB.
   */
  @After
  @Override
  public void tearDown() {
    super.tearDown();

    for (CommunicationStatisticsPrimaryKey id : communicationStatisticsToDelete) {
      Long channelId = id.getChannelId();
      Long communicationId = id.getCommunicationId();
      jdbcTemplate.update("DELETE FROM communication_statistics WHERE channel_id = ? AND communication_id = ?",
          new Object[] { channelId, communicationId });
    }
  }

  /**
   * Tests {@link JdbcCommunicationStatisticsDbDao#findCommunicationStatisticsByChannelIds(Long, List)} in the success
   * case of finding all the communication statistics for the given channel Ids.
   */
  @Test
  public void findCommunicationStatisticsByChannelIdsSuccess() {
    // Set Up
    // Register 10 user to each communication on each channel
    Communication communication = new Communication(communicationId1);
    for (int i = 0; i < CommunicationStatisticsBuilder.DEFAULT_TOTAL_REGISTRATIONS; i++) {
      long userId = TEST_USER_ID++;
      addCommunicationRegistration(userId, channel1Id, communication, true);
      addCommunicationRegistration(userId, channel2Id, communication, true);
      addCommunicationRegistration(userId, channel3Id, communication, true);
    }
    expectedCommStatistics1.setTotalRegistration(10);

    // Create Communication statistics record, this doesn't include communication statistics.
    CommunicationStatistics expectedCommStatistics2 =
        new CommunicationStatistics(communicationId1, NO_OF_VIEWINGS + 700,
            TOTAL_VIEWINGS_DURATION + 7000, NO_OF_RATINGS + 70, AVERAGE_RATINGS + 7.0f);
    expectedCommStatistics2.setTotalRegistration(10);
    this.insertCommunicationStatistics(channel2Id, expectedCommStatistics2);
    CommunicationStatistics expectedCommStatistics3 =
        new CommunicationStatistics(communicationId1, NO_OF_VIEWINGS + 800,
            TOTAL_VIEWINGS_DURATION + 8000, NO_OF_RATINGS + 80, AVERAGE_RATINGS + 8.0f);
    expectedCommStatistics3.setTotalRegistration(10);
    this.insertCommunicationStatistics(channel3Id, expectedCommStatistics3);

    // Do Test
    List<CommunicationStatistics> commStatistics = uut.findCommunicationStatisticsByChannelIds(communicationId1,
        Arrays.asList(channel1Id, channel2Id, channel3Id));

    // Assertions
    assertNotNull(commStatistics);
    assertEquals(3, commStatistics.size());
    CommunicationStatistics actualCommStat1 = commStatistics.get(0);
    assertCommunicationStatistics(expectedCommStatistics1, actualCommStat1);
    CommunicationStatistics actualCommStat2 = commStatistics.get(1);
    assertCommunicationStatistics(expectedCommStatistics2, actualCommStat2);
    CommunicationStatistics actualCommStat3 = commStatistics.get(2);
    assertCommunicationStatistics(expectedCommStatistics3, actualCommStat3);
  }

  /**
   * Tests {@link JdbcCommunicationStatisticsDbDao#findCommunicationStatisticsByChannelIds(Long, List)} in the success
   * case of finding all the communication statistics for the given channel Ids and when some of the communication had
   * no viewings.
   */
  @Test
  public void findCommunicationStatisticsByChannelIdsSuccessNoViewings() {
    // Set Up
    // Register 10 user to each communication on each channel
    Communication communication = new Communication(communicationId1);
    for (int i = 0; i < CommunicationStatisticsBuilder.DEFAULT_TOTAL_REGISTRATIONS; i++) {
      long userId = TEST_USER_ID++;
      addCommunicationRegistration(userId, channel1Id, communication, true);
      addCommunicationRegistration(userId, channel2Id, communication, true);
      addCommunicationRegistration(userId, channel3Id, communication, true);
    }
    expectedCommStatistics1.setTotalRegistration(10);

    // Create Communication statistics record, where the communication had no viewings.
    CommunicationStatistics expectedCommStatistics2 =
        new CommunicationStatistics(communicationId1, 0, 0, 0, 0.0f);
    expectedCommStatistics2.setTotalRegistration(10);
    this.insertCommunicationStatistics(channel2Id, expectedCommStatistics2);

    // Do Test
    List<CommunicationStatistics> commStatistics = uut.findCommunicationStatisticsByChannelIds(communicationId1,
        Arrays.asList(channel1Id, channel2Id, channel3Id));

    // Assertions
    assertNotNull(commStatistics);
    assertEquals(2, commStatistics.size());
    CommunicationStatistics actualCommStat1 = commStatistics.get(0);
    assertCommunicationStatistics(expectedCommStatistics1, actualCommStat1);
    CommunicationStatistics actualCommStat2 = commStatistics.get(1);
    assertCommunicationStatistics(expectedCommStatistics2, actualCommStat2);
  }

  /**
   * Tests {@link JdbcCommunicationStatisticsDbDao#findCommunicationStatisticsByChannelIds(Long, List)} in the success
   * case of finding all the communication statistics for the given channel Ids and when none of the communication had
   * any pre-registration.
   */
  @Test
  public void findCommunicationStatisticsByChannelIdsSuccessNoRegsitration() {
    // Set Up
    // 1st communication statistics expects zero registration.
    expectedCommStatistics1.setTotalRegistration(0);

    // Create Communication statistics record, this doesn't include communication statistics.
    CommunicationStatistics expectedCommStatistics2 =
        new CommunicationStatistics(communicationId1, NO_OF_VIEWINGS + 700,
            TOTAL_VIEWINGS_DURATION + 7000, NO_OF_RATINGS + 70, AVERAGE_RATINGS + 7.0f);
    // 2nd communication statistics expects zero registration.
    expectedCommStatistics2.setTotalRegistration(0);
    this.insertCommunicationStatistics(channel2Id, expectedCommStatistics2);
    CommunicationStatistics expectedCommStatistics3 =
        new CommunicationStatistics(communicationId1, NO_OF_VIEWINGS + 800,
            TOTAL_VIEWINGS_DURATION + 8000, NO_OF_RATINGS + 80, AVERAGE_RATINGS + 8.0f);
    // 3rd communication statistics expects zero registration.
    expectedCommStatistics3.setTotalRegistration(0);
    this.insertCommunicationStatistics(channel3Id, expectedCommStatistics3);

    // Do Test
    List<CommunicationStatistics> commStatistics = uut.findCommunicationStatisticsByChannelIds(communicationId1,
        Arrays.asList(channel1Id, channel2Id, channel3Id));

    // Assertions
    assertNotNull(commStatistics);
    assertEquals(3, commStatistics.size());
    CommunicationStatistics actualCommStat1 = commStatistics.get(0);
    assertCommunicationStatistics(expectedCommStatistics1, actualCommStat1);
    CommunicationStatistics actualCommStat2 = commStatistics.get(1);
    assertCommunicationStatistics(expectedCommStatistics2, actualCommStat2);
    CommunicationStatistics actualCommStat3 = commStatistics.get(2);
    assertCommunicationStatistics(expectedCommStatistics3, actualCommStat3);
  }

  /**
   * Just sanity check we have no errors in the SQL in the DAO.
   */
  @Test
  public void testAddCommunicationStatisticsSanityCheck() {

    Communication communication1 = communicationsChannel1.get(0);
    Communication communication2 = communicationsChannel1.get(1);
    Communication communication3 = communicationsChannel1.get(2);

    uut.loadStatistics(channel1Id, communicationsChannel1);

    assertViewingStatistics(communication1, 1.0f, 100, 1000);
    assertViewingStatistics(communication2, 2.0f, 200, 2000);
    assertViewingStatistics(communication3, 3.0f, 300, 3000);
  }

  @Test
  public void testAddCommunicationStatisticsAnotherChannel() {

    Communication communication4 = communicationsChannel2.get(0);
    Communication communication5 = communicationsChannel2.get(1);
    Communication communication6 = communicationsChannel2.get(2);

    uut.loadStatistics(channel2Id, communicationsChannel2);

    assertViewingStatistics(communication4, 4.0f, 400, 4000);
    assertViewingStatistics(communication5, 5.0f, 500, 5000);
    assertViewingStatistics(communication6, 6.0f, 600, 6000);
  }

  @Test
  public void testAddCommunicationStatisticsForChannelThatDoesNotExist() {

    Communication communication4 = communicationsChannel2.get(0);
    Communication communication5 = communicationsChannel2.get(1);
    Communication communication6 = communicationsChannel2.get(2);

    Long nonExistentChannelId = new Long(100000L);
    uut.loadStatistics(nonExistentChannelId, communicationsChannel2);

    assertViewingStatistics(communication4, 0.0f, 0, 0);
    assertViewingStatistics(communication5, 0.0f, 0, 0);
    assertViewingStatistics(communication6, 0.0f, 0, 0);
  }

  /**
   * Just test DAO does not blow up if it gets null channel id
   */
  @Test
  public void testAddCommunicationStatisticsForNullChannelId() {

    Communication communication4 = communicationsChannel2.get(0);
    Communication communication5 = communicationsChannel2.get(1);
    Communication communication6 = communicationsChannel2.get(2);

    uut.loadStatistics(null, communicationsChannel2);

    assertViewingStatistics(communication4, 0.0f, 0, 0);
    assertViewingStatistics(communication5, 0.0f, 0, 0);
    assertViewingStatistics(communication6, 0.0f, 0, 0);
  }

  /**
   * Just test DAO does not blow up if it gets null communications.
   */
  @Test
  public void testAddCommunicationStatisticsForNullCommunications() {
    uut.loadStatistics(channel2Id, null);
  }

  /**
   * Just test DAO does not blow up if it gets empty communications.
   */
  @Test
  public void testAddCommunicationStatisticsForEmptyCommunications() {
    uut.loadStatistics(channel2Id, new ArrayList<Communication>());
  }

  @Test
  public void testLoadStatisticsFromAllChannelsValidateSyndicatedCommunicationTotals() {
    // Set up
    Random r = new Random();
    long myChannelId1 = r.nextInt(999999999);
    long myChannelId2 = r.nextInt(999999999);

    // Add a communiction and add it to a 2nd channel too
    Long communicationId = addTemporaryCommunication(myChannelId1, Provider.BRIGHTTALK);
    Communication communication = new Communication(communicationId);

    addChannelAsset(myChannelId2, communicationId, "communication");

    // Set some stats in both channels for this communication
    float channel1Rating = 1.0f;
    float channel2Rating = 5.0f;

    int channel1NumRating = 2;
    int channel2NumRating = 2;

    int channel1Viewings = 100;
    int channel2Viewings = 200;

    int channel1ViewingsDuration = 300;
    int channel2ViewingsDuration = 500;

    insertCommunicationStatistics(myChannelId1, new CommunicationStatistics(communicationId, channel1Viewings,
        channel1ViewingsDuration, channel1NumRating, channel1Rating));

    insertCommunicationStatistics(myChannelId2, new CommunicationStatistics(communicationId, channel2Viewings,
        channel2ViewingsDuration, channel2NumRating, channel2Rating));

    // Create a list of communications to get stas for
    List<Communication> communications = new ArrayList<Communication>();
    communications.add(communication);

    // Expectations

    // expected rating is the average of all ratings - {(rating * number) per channel} / total number
    double expectedRating = ((channel1Rating * channel1NumRating) + (channel2Rating * channel2NumRating))
        / (channel1NumRating + channel2NumRating);
    int expectedViewings = channel1Viewings + channel2Viewings;
    int expectedViewingsDuration = channel1ViewingsDuration + channel2ViewingsDuration;

    // Do Test
    uut.loadStatisticsFromAllChannels(communications);

    // Assertions
    CommunicationStatistics result = communication.getCommunicationStatistics();
    assertEquals("incorrect average rating ", expectedRating, result.getAverageRating(), 0.1);
    assertEquals("incorrect total number of viewings ", expectedViewings, result.getNumberOfViewings());
    assertEquals("incorrect total viewing duration ", expectedViewingsDuration, result.getTotalViewingDuration(), 0.1);
  }

  @Test
  public void testLoadStatisticsFromAllChannelsValidateNotSyndicatedCommunicationTotals() {
    // Set up
    Random r = new Random();
    long myChannelId = r.nextInt(999999999);

    // Add a communiction and add it to a 2nd channel too
    Long communicationId = addTemporaryCommunication(myChannelId, Provider.BRIGHTTALK);
    Communication communication = new Communication(communicationId);

    // Set some stats in both channels for this communication
    float channel1Rating = 1.0f;
    int channel1NumRating = 2;
    int channel1Viewings = 100;
    int channel1ViewingsDuration = 300;

    insertCommunicationStatistics(myChannelId, new CommunicationStatistics(communicationId, channel1Viewings,
        channel1ViewingsDuration, channel1NumRating, channel1Rating));

    // Create a list of communications to get stas for
    List<Communication> communications = new ArrayList<Communication>();
    communications.add(communication);

    // Expectations

    // expected rating is the average of all ratings - {(rating * number) per channel} / total number
    double expectedRating = channel1Rating;
    int expectedViewings = channel1Viewings;
    int expectedViewingsDuration = channel1ViewingsDuration;

    // Do Test
    uut.loadStatisticsFromAllChannels(communications);

    // Assertions
    CommunicationStatistics result = communication.getCommunicationStatistics();
    assertEquals("incorrect average rating ", expectedRating, result.getAverageRating(), 0.1);
    assertEquals("incorrect total number of viewings ", expectedViewings, result.getNumberOfViewings());
    assertEquals("incorrect total viewing duration ", expectedViewingsDuration, result.getTotalViewingDuration(), 0.1);
  }

  /**
   * Just test DAO does not blow up if it gets null communications.
   */
  @Test
  public void testAddCommunicationStatisticsForNullSyndicatedOutCommunications() {
    uut.loadStatisticsFromAllChannels(null);
  }

  /**
   * Just test DAO does not blow up if it gets empty communications.
   */
  @Test
  public void testAddCommunicationStatisticsForEmptySyndicatedOutCommunications() {
    uut.loadStatisticsFromAllChannels(new ArrayList<Communication>());
  }

  private void assertViewingStatistics(final Communication communication, final float averageRating,
      final int totalNumberViewings, final int totalViewingDuration) {
    CommunicationStatistics viewingStatistics = communication.getCommunicationStatistics();
    assertEquals("incorrect total number of viewings ", totalNumberViewings, viewingStatistics.getNumberOfViewings());
    assertEquals("incorrect total viewing duration ", totalViewingDuration,
        viewingStatistics.getTotalViewingDuration(), 0.1);
    assertEquals("incorrect average rating ", averageRating, viewingStatistics.getAverageRating(), 0.1);
  }

  private void assertCommunicationStatistics(CommunicationStatistics expectedStat, CommunicationStatistics actualStat) {
    assertNotNull(expectedStat);
    assertNotNull(actualStat);
    assertEquals(expectedStat.getId(), actualStat.getId());
    assertEquals(expectedStat.getChannelId(), actualStat.getChannelId());
    assertEquals(expectedStat.getNumberOfViewings(), actualStat.getNumberOfViewings());
    assertEquals(new Float(expectedStat.getTotalViewingDuration()), new Float(actualStat.getTotalViewingDuration()));
    assertEquals(expectedStat.getTotalRegistration(), actualStat.getTotalRegistration());
    assertEquals(new Float(expectedStat.getAverageRating()), new Float(actualStat.getAverageRating()));
    assertEquals(expectedStat.getNumberOfRatings(), actualStat.getNumberOfRatings());
  }

  /**
   * Helper to create communication statistics in the DB
   * 
   * @param channel1Id
   * @param communicationId
   * @param title
   * @param description
   * @param presenter
   * @param keywords
   */
  private void insertCommunicationStatistics(final Long channelId, final CommunicationStatistics statistics) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    statistics.setChannelId(channelId);
    params.addValue("communication_id", statistics.getId());
    params.addValue("rating", statistics.getAverageRating());
    params.addValue("num_ratings", statistics.getNumberOfRatings());
    params.addValue("num_views", statistics.getNumberOfViewings());
    params.addValue("viewed_for", statistics.getTotalViewingDuration());

    simpleJdbcInsertCommunicationStatistics.execute(params);
    communicationStatisticsToDelete.add(new CommunicationStatisticsPrimaryKey(channelId, statistics.getId()));
  }

  /**
   * The primary key for each entry in the communication_statistics table and communication_registration table. Primary
   * key is channel id and communication id. Used so we can delete all entries we set up in this test case.
   */
  private static class CommunicationStatisticsPrimaryKey {

    private final Long channelId;

    private final Long communicationId;

    public CommunicationStatisticsPrimaryKey(final Long channelId, final Long communicationId) {
      super();
      this.channelId = channelId;
      this.communicationId = communicationId;
    }

    public Long getChannelId() {
      return channelId;
    }

    public Long getCommunicationId() {
      return communicationId;
    }
  }// ChannelIdCommunicationIdPrimaryKey
}
