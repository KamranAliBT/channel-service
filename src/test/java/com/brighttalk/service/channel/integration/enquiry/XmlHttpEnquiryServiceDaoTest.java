/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpEnquiryServiceDaoTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.enquiry;

import java.net.URI;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.integration.enquiry.dto.EnquiryRequestDto;
import com.brighttalk.service.channel.integration.enquiry.dto.converter.EnquiryRequestConverter;

public class XmlHttpEnquiryServiceDaoTest {

  private XmlHttpEnquiryServiceDao uut;

  private RestTemplate mockRestTemplate;

  private EnquiryRequestConverter mockConverter;

  @Before
  public void setUp() {

    uut = new XmlHttpEnquiryServiceDao();

    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    uut.setRestTemplate(mockRestTemplate);

    mockConverter = EasyMock.createMock(EnquiryRequestConverter.class);
    uut.setConverter(mockConverter);
  }

  @Test
  public void sendRequest() throws Exception {
    
    final Channel channel = new Channel();
    final EnquiryRequestDto requestDto = new EnquiryRequestDto();

    final String serviceBaseUrl = "serviceBaseUrl";
    uut.setServiceBaseUrl(serviceBaseUrl);

    final String expectedRequestUrl = serviceBaseUrl + XmlHttpEnquiryServiceDao.REQUEST_PATH;

    EasyMock.expect(mockConverter.convert(channel)).andReturn(requestDto);
    EasyMock.expect(mockRestTemplate.postForLocation(expectedRequestUrl, requestDto)).andReturn(new URI(""));

    EasyMock.replay(mockConverter, mockRestTemplate);

    // do test
    uut.sendCreateChannelRequest(channel);

    EasyMock.verify(mockConverter, mockRestTemplate);
  }
}