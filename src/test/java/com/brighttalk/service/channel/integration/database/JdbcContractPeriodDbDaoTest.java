/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: JdbcContractPeriodDbDaoTest.java 101536 2015-10-16 10:42:52Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.ContractPeriodNotFoundException;
import com.brighttalk.service.channel.integration.error.ContractPeriodAlreadyExistsForChannel;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcContractPeriodDbDaoTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcContractPeriodDbDao uut;

  //contract periods created for test purposes
  private List<Long> testContractPeriods;

  private long channelId;

  //test datetime for parsing test datetimes
  private static DateTime utcDateTime;

  /**
   * Tests {@link JdbcContractPeriodDbDao#create(ContractPeriod)} to test that a ContractPeriod is correctly stored
   *
   * @throws Exception
   */
  @Test
  public void testInsertValidContractPeriod() throws Exception {
    //setup
    ContractPeriod contractPeriod = new ContractPeriod();
    contractPeriod.setChannelId(channelId);
    contractPeriod.setStart(utcDateTime.parse("2015-01-01T00:00:00Z").toDate());
    contractPeriod.setEnd(utcDateTime.parse("2015-10-10T10:10:10Z").toDate());
    contractPeriod.setCreated(utcDateTime.parse("2015-01-01T00:00:00Z").toDate());

    //test
    ContractPeriod created = uut.create(contractPeriod);

    //assert
    testContractPeriods.add(created.getId());
    assertTrue(created.isActive());
    assertEquals(contractPeriod.getChannelId(), created.getChannelId());
    assertEquals(contractPeriod.getStart().getTime(), created.getStart().getTime());
    assertEquals(contractPeriod.getEnd().getTime(), created.getEnd().getTime());
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#create(ContractPeriod)} to test that a
   * {@link ContractPeriodAlreadyExistsForChannel} exception is thrown if the contract period is a duplicate
   *
   * @throws Exception
   */
  @Test
  public void testInsertDuplicateContractPeriodThrowsException() throws Exception {
    //setup
    ContractPeriod contractPeriod = new ContractPeriod();
    contractPeriod.setChannelId(channelId);
    contractPeriod.setStart(utcDateTime.parse("2015-01-01T00:00:00Z").toDate());
    contractPeriod.setEnd(utcDateTime.parse("2015-10-10T10:10:10Z").toDate());
    contractPeriod.setCreated(utcDateTime.parse("2015-01-01T00:00:00Z").toDate());
    ContractPeriod created = uut.create(contractPeriod);
    testContractPeriods.add(created.getId());

    //test
    try {
      uut.create(contractPeriod);
      fail("Expected ContractPeriodAlreadyExistsForChannel");
    } catch (ContractPeriodAlreadyExistsForChannel e) {
      assertEquals("InvalidContractPeriod", e.getUserErrorCode());
    }
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#find(Long)} to test that a ContractPeriod is correctly retrieved
   *
   * @throws Exception
   */
  @Test
  public void testGetValidContractPeriod() throws Exception {
    //setup
    ContractPeriod contractPeriod = createContractPeriodInDB();

    //test
    ContractPeriod created = uut.find(contractPeriod.getId());

    //assert
    assertTrue(created.isActive());
    assertEquals(contractPeriod.getChannelId(), created.getChannelId());
    assertEquals(contractPeriod.getStart(), created.getStart());
    assertEquals(contractPeriod.getEnd(), created.getEnd());
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#findAllByChannelId(Long)} to test that ContractPeriods are correctly retrieved
   * only for the channel requested
   *
   * @throws Exception
   */
  @Test
  public void testGetValidContractPeriodsForChannelAmongOthers() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-05T00:00:00Z", "2015-02-05T00:00:00Z", channelId);
    createContractPeriodInDB("2015-03-05T00:00:00Z", "2015-04-05T00:00:00Z", channelId);
    createContractPeriodInDB("2015-05-05T00:00:00Z", "2015-06-05T00:00:00Z", addTemporaryChannel(456l, true));

    //test
    List<ContractPeriod> retrievedContractPeriods = uut.findAllByChannelId(channelId);

    //assert
    for (ContractPeriod contractPeriod : retrievedContractPeriods) {
      assertTrue(contractPeriod.isActive());
      assertEquals(contractPeriod.getChannelId().longValue(), channelId);
    }
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#findAllByChannelId(Long)} to test that it returns an empty {@link List} if
   * there are no matches.
   *
   * @throws Exception
   */
  @Test
  public void testGetValidContractPeriodsForChannelReturnsEmptyListWhenNoMatches() throws Exception {
    //setup
    createContractPeriodInDB("2015-05-01T00:00:00Z", "2015-05-02T00:00:00Z", channelId);
    createContractPeriodInDB("2015-05-03T00:00:00Z", "2015-05-04T00:00:00Z", channelId);

    //test
    List<ContractPeriod> retrievedContractPeriods = uut.findAllByChannelId(addTemporaryChannel(456l, true));

    //assert
    assertTrue(retrievedContractPeriods.isEmpty());
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#findAllByChannelId(Long)} to test that it returns an empty {@link List} if
   * there are matches however the channel has been deleted
   *
   * @throws Exception
   */
  @Test
  public void testGetValidContractPeriodsForChannelReturnsEmptyListWhenMatchesExistButChannelDeleted()
      throws Exception {
    //setup
    Long deletedChannelId = addTemporaryChannel(456l, false);
    createContractPeriodInDB("2015-05-01T00:00:00Z", "2015-05-02T00:00:00Z", deletedChannelId);
    createContractPeriodInDB("2015-05-03T00:00:00Z", "2015-05-04T00:00:00Z", deletedChannelId);

    //test
    List<ContractPeriod> retrievedContractPeriods = uut.findAllByChannelId(deletedChannelId);

    //assert
    assertTrue(retrievedContractPeriods.isEmpty());
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#findAllByChannelId(Long)} to test that it returns an empty {@link List} if
   * there are no active matches
   *
   * @throws Exception
   */
  @Test
  public void testGetValidContractPeriodsForChannelReturnsEmptyListWhenNoMatchesExceptInactive() throws Exception {
    //setup
    ContractPeriod contractPeriod1 =
        createContractPeriodInDB("2015-01-05T00:00:00Z", "2015-02-05T00:00:00Z", channelId);
    ContractPeriod contractPeriod2 =
        createContractPeriodInDB("2015-03-05T00:00:00Z", "2015-04-05T00:00:00Z", channelId);
    uut.delete(contractPeriod1.getId());
    uut.delete(contractPeriod2.getId());

    //test
    List<ContractPeriod> retrievedContractPeriods = uut.findAllByChannelId(channelId);

    //assert
    assertTrue(retrievedContractPeriods.isEmpty());
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#update(ContractPeriod)} for changes being applied
   *
   * @throws Exception
   */
  @Test
  public void testUpdateContractPeriod() throws Exception {
    //setup
    ContractPeriod created = createContractPeriodInDB();

    //test
    created.setStart(utcDateTime.parse("2015-05-05T00:00:00Z").toDate());
    created.setEnd(utcDateTime.parse("2016-10-10T10:10:10Z").toDate());
    created.setChannelId(created.getChannelId());
    ContractPeriod updated = uut.update(created);

    //assert
    assertTrue(created.isActive());
    assertEquals(created.getChannelId(), updated.getChannelId());
    assertEquals(created.getStart(), updated.getStart());
    assertEquals(created.getEnd(), updated.getEnd());
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#update(ContractPeriod)} for changes being applied where the updated contract
   * period is the same as the current. This tests for idempotence of the update operation.
   *
   * @throws Exception
   */
  @Test
  public void testUpdateContractPeriodWithDuplicate() throws Exception {
    //setup
    ContractPeriod created = createContractPeriodInDB();
    created.setStart(utcDateTime.parse("2015-05-05T00:00:00Z").toDate());
    created.setEnd(utcDateTime.parse("2016-10-10T10:10:10Z").toDate());
    created.setChannelId(321l);
    ContractPeriod updated = uut.update(created);

    //test
    ContractPeriod updatedAgain = uut.update(created);

    //assert
    assertTrue(created.isActive());
    assertEquals(created.getChannelId(), updated.getChannelId());
    assertEquals(created.getStart(), updated.getStart());
    assertEquals(created.getEnd(), updated.getEnd());
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#delete(Long)} to ensure that a ContractPeriod is deleted correctly
   *
   * @throws Exception
   */
  @Test(expected = ContractPeriodNotFoundException.class)
  public void testDeleteContractPeriod() throws Exception {
    //Setup
    ContractPeriod contractPeriod = createContractPeriodInDB();

    //test
    uut.delete(contractPeriod.getId());

    //assert
    uut.find(contractPeriod.getId());
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns false if the
   * only overlap is the contract period being tested
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodOverlapDoesNotConsiderSelfAsOverlap() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-02T00:00:00Z", "2015-01-03T00:00:00Z", channelId);
    ContractPeriod one =
        createContractPeriodInDB("2015-01-05T00:00:00Z", "2015-01-07T00:00:00Z", channelId);
    createContractPeriodInDB("2015-01-09T00:00:00Z", "2015-01-16T00:00:00Z", channelId);

    //test
    boolean exists = uut.contractPeriodOverlapsExisting(one);

    //assert
    assertFalse(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns true if given
   * a contract period with a start date that overlaps an existing contract period
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodOverlapWithStartDate() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);//10 hours
    createContractPeriodInDB("2015-01-02T00:00:00Z", "2015-01-03T00:00:00Z", channelId);//1 day

    //test

    ContractPeriod contractPeriod =
        createContractPeriodInMemory("2015-01-02T10:00:00Z", "2015-01-05T00:00:00Z", channelId);
    boolean exists = uut.contractPeriodOverlapsExisting(contractPeriod);

    //assert
    assertTrue(exists);
  }


  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns false if given
   * a contract period with a start date that overlaps an existing contract period that is disabled
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodOverlapWithStartDateForDisabledContractPeriod() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);//10 hours
    ContractPeriod contractPeriodInDB =
        createContractPeriodInDB("2015-01-02T00:00:00Z", "2015-01-03T00:00:00Z", channelId);//1 day
    uut.delete(contractPeriodInDB.getId());

    //test
    ContractPeriod contractPeriod =
        createContractPeriodInMemory("2015-01-02T10:00:00Z", "2015-01-05T00:00:00Z", channelId);
    boolean exists = uut.contractPeriodOverlapsExisting(contractPeriod);

    //assert
    assertFalse(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns true if given
   * a contract period with an end date that overlaps an existing contract period
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodOverlapWithEndDate() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);//10 hours
    createContractPeriodInDB("2015-01-02T00:00:00Z", "2015-01-03T00:00:00Z", channelId);//1 day

    //test

    ContractPeriod contractPeriod =
        createContractPeriodInMemory("2014-01-02T10:00:00Z", "2015-01-02T10:00:00Z", channelId);
    boolean exists = uut.contractPeriodOverlapsExisting(contractPeriod);

    //assert
    assertTrue(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns true if given
   * a contract period with a start date that exactly matches a current contract period
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodExistsExactStartDate() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);//10 hours
    createContractPeriodInDB("2015-01-02T00:00:00Z", "2015-01-03T00:00:00Z", channelId);//1 day

    //test

    ContractPeriod contractPeriod =
        createContractPeriodInMemory("2015-01-02T00:00:00Z", "2015-01-05T00:00:00Z", channelId);
    boolean exists = uut.contractPeriodOverlapsExisting(contractPeriod);

    //assert
    assertTrue(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns true if given
   * a contract period with an end date that exactly matches a current contract period
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodExistsExactEndDate() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);//10 hours
    createContractPeriodInDB("2015-01-02T00:00:00Z", "2015-01-03T00:00:00Z", channelId);//1 day

    //test

    ContractPeriod contractPeriod =
        createContractPeriodInMemory("2014-01-02T00:00:00Z", "2015-01-03T00:00:00Z", channelId);
    boolean exists = uut.contractPeriodOverlapsExisting(contractPeriod);

    //assert
    assertTrue(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns false if given
   * an overlapping contract period but for a different channel
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodDoesNotExistForDifferentChannel() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);//10 hours
    createContractPeriodInDB("2015-01-02T00:00:00Z", "2015-01-03T00:00:00Z", channelId);//1 day

    //test
    ContractPeriod contractPeriod =
        createContractPeriodInMemory("2014-01-02T10:00:00Z", "2015-01-02T10:00:00Z", channelId);
    contractPeriod.setChannelId(addTemporaryChannel(456l, true));
    boolean exists = uut.contractPeriodOverlapsExisting(contractPeriod);

    //assert
    assertFalse(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns false if given
   * a new contract period between unrelated contract periods
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodDoesNotExistBetweenDates() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);//10 hours
    createContractPeriodInDB("2015-01-03T00:00:00Z", "2015-01-04T00:00:00Z", channelId);//1 day

    //test
    ContractPeriod contractPeriod =
        createContractPeriodInMemory("2015-01-01T11:00:00Z", "2015-01-02T00:00:00Z", channelId);
    boolean exists = uut.contractPeriodOverlapsExisting(contractPeriod);

    //assert
    assertFalse(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns true if given
   * a new contract period that completely encapsulates an existing contract period
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodExistsWhenEncapsulatingAnExistingContractPeriod() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);//10 hours
    createContractPeriodInDB("2015-01-03T00:00:00Z", "2015-01-04T00:00:00Z", channelId);//1 day

    //test
    ContractPeriod contractPeriod =
        createContractPeriodInMemory("2013-01-01T00:00:00Z", "2015-02-01T10:00:10Z", channelId);
    boolean exists = uut.contractPeriodOverlapsExisting(contractPeriod);

    //assert
    assertTrue(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns true if given
   * a new contract period that completely and exactly encapsulates an existing contract period
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodExistsWhenEncapsulatingAnExistingContractPeriodExactly() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);//10 hours
    createContractPeriodInDB("2015-01-03T00:00:00Z", "2015-01-04T00:00:00Z", channelId);//1 day

    //test
    ContractPeriod contractPeriod =
        createContractPeriodInMemory("2015-01-01T00:00:00Z", "2015-01-01T10:00:10Z", channelId);
    boolean exists = uut.contractPeriodOverlapsExisting(contractPeriod);

    //assert
    assertTrue(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#exists(Long, Long)} returns true if given an existing contract period's ID
   * and it's channel id
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodExists() throws Exception {
    //setup
    ContractPeriod contractPeriodInDB =
        createContractPeriodInDB("2015-01-03T00:00:00Z", "2015-01-04T00:00:00Z", channelId);//1 day

    //test
    boolean exists = uut.exists(contractPeriodInDB.getId(), channelId);

    //assert
    assertTrue(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#exists(Long, Long)} returns false if given an existing contract period's ID
   * and it's channel id, however the channel is deleted (inactive)
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodDoesNotExistIfChannelIsDeleted() throws Exception {
    //setup
    ContractPeriod contractPeriodInDB =
        createContractPeriodInDB("2015-01-03T00:00:00Z", "2015-01-04T00:00:00Z",
            addTemporaryChannel(456l, false));//1 day

    //test
    boolean exists = uut.exists(contractPeriodInDB.getId(), channelId);

    //assert
    assertFalse(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#exists(Long, Long)} returns false if given an existing contract period's ID
   * and it's channel id, however the channel is deleted (inactive)
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodNotFoundIfChannelIsDeleted() throws Exception {
    //setup
    ContractPeriod contractPeriodInDB =
        createContractPeriodInDB("2015-01-03T00:00:00Z", "2015-01-04T00:00:00Z",
            addTemporaryChannel(456l, false));//1 day

    //test
    try {
      uut.find(contractPeriodInDB.getId());
      fail("Expected ContractPeriodNotFoundException");
    } catch (ContractPeriodNotFoundException e) {
    }
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#exists(Long, Long)} returns false if given an existing contract period's ID
   * and but a different channel id
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodDoesNotExistWithSameIdAndDifferentChannelId() throws Exception {
    //setup
    ContractPeriod contractPeriodInDB =
        createContractPeriodInDB("2015-01-03T00:00:00Z", "2015-01-04T00:00:00Z", channelId);//1 day

    //test
    boolean exists = uut.exists(contractPeriodInDB.getId(), addTemporaryChannel(456l, false));

    //assert
    assertFalse(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#exists(Long, Long)} returns false if given a non-existent contract period's
   * ID
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodDoesNotExistWithNonExistentId() throws Exception {
    //setup

    //test
    boolean exists = uut.exists(123345l, 456l);

    //assert
    assertFalse(exists);
  }

  /**
   * Tests {@link JdbcContractPeriodDbDao#contractPeriodOverlapsExisting(ContractPeriod)} returns true if two
   * contract periods exist for a channel and a contract period is tested which overlaps the last one
   *
   * @throws Exception
   */
  @Test
  public void testContractPeriodOverlapsExistingWithMultipleContractsInPlace() throws Exception {
    //setup
    createContractPeriodInDB("2015-01-01T00:00:00Z", "2016-01-01T00:00:00Z", channelId);
    createContractPeriodInDB("2016-01-01T00:00:00Z", "2017-01-01T00:00:00Z", channelId);

    //test
    boolean exists = uut.contractPeriodOverlapsExisting(createContractPeriodInMemory("2016-05-01T00:00:00Z",
        "2016-06-01T00:00:00Z", channelId));

    //assert
    assertTrue(exists);
  }

  private ContractPeriod createContractPeriodInDB() {
    return createContractPeriodInDB("2015-01-01T00:00:00Z", "2015-10-10T10:10:10Z", channelId);
  }

  private ContractPeriod createContractPeriodInDB(String startDate, String endDate, long channelId) {
    ContractPeriod contractPeriod = createContractPeriodInMemory(startDate, endDate, channelId);
    ContractPeriod created = uut.create(contractPeriod);
    testContractPeriods.add(created.getId());
    return created;
  }

  private ContractPeriod createContractPeriodInMemory(String startDate, String endDate, long channelId) {
    ContractPeriod contractPeriod = new ContractPeriod();
    contractPeriod.setChannelId(channelId);

    contractPeriod.setStart(utcDateTime.parse(startDate).toDate());
    contractPeriod.setEnd(utcDateTime.parse(endDate).toDate());
    contractPeriod.setCreated(utcDateTime.parse("2015-01-01T00:00:00Z").toDate());
    return contractPeriod;
  }

  @Before
  public void setUp() throws Exception {
    utcDateTime = new DateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("UTC")));
    testContractPeriods = new ArrayList();

    channelId = addTemporaryChannel(123l, true);
  }

  @Override
  @After
  public void tearDown() {
    super.tearDown();
    //delete any test contract periods
    for (Long testContractPeriod : testContractPeriods) {
      jdbcTemplate.update("DELETE from contract_period where id = ?", testContractPeriod.longValue());
    }
  }
}