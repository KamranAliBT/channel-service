/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationDbDaoTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.common.user.Session;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationStatisticsBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Format;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration.Visibility;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

/**
 * Tests {@link JdbcCommunicationDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCommunicationDbDaoTest extends AbstractJdbcDbDaoTest {

  private static final long TEST_CHANNEL_ID = 9999;

  @Autowired
  private JdbcCommunicationDbDao uut;

  /**
   * Tests {@link JdbcCommunicationDbDao#create(Communication)} in case of creating a communication with full details
   * successfully.
   */
  @Test
  public void testCreateCommunication() {
    Communication testCommunication = buildCommunication(TEST_CHANNEL_ID);

    // do test
    uut.create(testCommunication);
    Long createdCommunicationId = testCommunication.getId();
    this.addCommunicationConfiguration(testCommunication);

    Communication foundCommunication = uut.find(createdCommunicationId);

    this.assertAllCommunicationFields(foundCommunication, testCommunication);

    temporaryCommunicationIds.add(createdCommunicationId);
  }

  @Test
  public void communicationHasCorrectDate() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    Communication communication = buildCommunication(channelId);

    final int expectedYear = 2008;
    final int expectedMonth = 02;
    final int expectedDay = 12;
    final int expectedHour = 10;
    final int expectedMinute = 25;
    final int expectedSecond = 1;
    Calendar calendar = Calendar.getInstance();
    calendar.set(expectedYear, expectedMonth, expectedDay, expectedHour, expectedMinute, expectedSecond);

    Date lastUpdated = calendar.getTime();
    communication.setLastUpdated(lastUpdated);

    Long communicationId = addTemporaryCommunication(channelId, communication);

    // do test
    Communication result = uut.find(communicationId);
    assertNotNull(result.getLastUpdated());

    calendar.setTime(result.getLastUpdated());

    assertEquals(expectedYear, calendar.get(Calendar.YEAR));
    assertEquals(expectedMonth, calendar.get(Calendar.MONTH));
    assertEquals(expectedDay, calendar.get(Calendar.DATE));
    assertEquals(expectedHour, calendar.get(Calendar.HOUR));
    assertEquals(expectedMinute, calendar.get(Calendar.MINUTE));
    assertEquals(expectedSecond, calendar.get(Calendar.SECOND));
  }

  /**
   * Testing find BrightTALK communication by communication id in master channel.
   */
  @Test
  public void testFindCommunicationForMasterChannel() {
    // setup
    Long channelId = MASTER_CHANNEL_ID;

    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // do test
    Communication result = uut.find(communicationId);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(0, result.getPresenters().size());
    Assert.assertEquals(communicationId, result.getId());
    Assert.assertEquals(Boolean.TRUE, Boolean.valueOf(result.getConfiguration().isInMasterChannel()));
    Assert.assertEquals(true, result.isUpcoming());
    Assert.assertTrue(result.isBrightTALK());
  }

  /**
   * Testing find BrightTALK communication by communication id for communication with no presenter.
   */
  @Test
  public void testLoadPresentersReturnsCommunicationWithNoPresenters() {
    // setup
    Long channelId = MASTER_CHANNEL_ID;

    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    Communication communication = uut.find(communicationId);

    // do test
    List<Session> presenters = uut.findPresenters(communication);

    // assertions
    Assert.assertNotNull(presenters);
    Assert.assertEquals(0, presenters.size());
  }

  /**
   * Testing find BrightTALK communication by communication id for communication with one presenter.
   */
  @Test
  public void testLoadPresentersReturnsCommunicationWithOnePresenter() {
    // setup
    Long channelId = MASTER_CHANNEL_ID;

    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);
    addPresenterSession(communicationId, SESSION_ID);

    Communication communication = uut.find(communicationId);

    // do test
    List<Session> presenters = uut.findPresenters(communication);

    // assertions
    Assert.assertNotNull(presenters);
    Assert.assertEquals(1, presenters.size());
  }

  /**
   * Testing find BrightTALK communication by communication id for communication with two presenter.
   */
  @Test
  public void testLoadPresentersReturnsCommunicationWithTwoPresenters() {
    // setup
    Long channelId = MASTER_CHANNEL_ID;

    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);
    addPresenterSession(communicationId, SESSION_ID);
    addPresenterSession(communicationId, "99999");
    Communication communication = uut.find(communicationId);

    // do test
    List<Session> presenters = uut.findPresenters(communication);

    // assertions
    Assert.assertNotNull(presenters);
    Assert.assertEquals(2, presenters.size());
  }

  /**
   * Testing find MEDIAZONE communication by communication id and channel id.
   */
  @Test
  public void testFindMediaZoneCommunication() {
    // setup
    Long channelId = MASTER_CHANNEL_ID;

    Long communicationId = addTemporaryCommunication(channelId, Provider.MEDIAZONE);

    // do test
    Communication result = uut.find(communicationId);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(0, result.getPresenters().size());
    Assert.assertEquals(communicationId, result.getId());
    Assert.assertEquals(Boolean.TRUE, Boolean.valueOf(result.getConfiguration().isInMasterChannel()));
    Assert.assertEquals(true, result.isUpcoming());
    Assert.assertTrue(result.isMediaZone());
  }

  /**
   * Testing find BrightTALK communication by communication id for not existing communication.
   */
  @Test(expected = NotFoundException.class)
  public void testFindCommunicationWithNotExistingId() {

    // do test
    uut.find(0L);
  }

  /**
   * Testing find BrightTALK communication by communication id and channel id for MasterChannel.
   */
  @Test
  public void testFindCommunicationByIdForMasterChannel() {
    // setup
    Long channelId = MASTER_CHANNEL_ID;

    Long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    // do test
    Communication result = uut.find(MASTER_CHANNEL_ID, communicationId);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(0, result.getPresenters().size());
    Assert.assertEquals(communicationId, result.getId());
    Assert.assertEquals(Boolean.TRUE, Boolean.valueOf(result.getConfiguration().isInMasterChannel()));
    Assert.assertEquals(true, result.isUpcoming());
  }

  /**
   * Testing find BrightTALK communication by communication id and channel id for not existing communication.
   */
  @Test(expected = NotFoundException.class)
  public void testFindCommunicationByIdWithNotExistingId() {

    // do test
    uut.find(0L, 0L);
  }

  /**
   * Testing find MEDIAZONE communication by communication id and channel id.
   */
  @Test
  public void testFindCommunicationByIdMediaZoneCommunication() {
    // setup
    Long channelId = MASTER_CHANNEL_ID;

    Long communicationId = addTemporaryCommunication(channelId, Provider.MEDIAZONE);

    // do test
    Communication result = uut.find(MASTER_CHANNEL_ID, communicationId);

    // assertions
    Assert.assertNotNull(result);
    Assert.assertEquals(0, result.getPresenters().size());
    Assert.assertEquals(communicationId, result.getId());
    Assert.assertEquals(Boolean.TRUE, Boolean.valueOf(result.getConfiguration().isInMasterChannel()));
    Assert.assertEquals(true, result.isUpcoming());
    Assert.assertTrue(result.isMediaZone());
  }

  /**
   * Test find a communication for a channel in the case of removing a communication from a channel after it has been
   * syndicated.
   * <p>
   * Communication can only be removed from consumer channels.
   */
  @Test
  public void findCommunicationForChannelForRemovedCommunication() {
    // Setup - Create a communication in master channel, then syndicate to another channel.
    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long masterChannelId = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long consumerChannelId = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Communication communication = buildCommunication(masterChannelId);
    addTemporaryCommunication(masterChannelId, communication);
    syndicateCommunication(consumerChannelId, communication);

    // Remove the created communication from the syndicate in channel.
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channelId", consumerChannelId);
    params.addValue("communicationId", communication.getId());
    String sql = "UPDATE asset SET is_active = 0 WHERE channel_id = :channelId AND target_id = :communicationId";
    namedParameterJdbcTemplate.update(sql, params);

    // Do Test -
    try {
      uut.find(consumerChannelId, communication.getId());
      fail("Find Communication should fail for removed communication.");
    } catch (CommunicationNotFoundException ex) {
      assertEquals(ErrorCode.NOT_FOUND.getName(), ex.getUserErrorCode());
    }

  }

  @Test
  public void findAllCommunicationsForAChannel() {

    // Set up
    final Long channelId = 666L;
    Long communicationId1 = addTemporaryCommunication(channelId, Provider.BRIGHTTALK,
        CommunicationSyndication.SyndicationType.OUT);
    Long communicationId2 = addTemporaryCommunication(channelId, Provider.MEDIAZONE,
        CommunicationSyndication.SyndicationType.OUT);
    Long communicationId3 = addTemporaryCommunication(channelId, Provider.VIDEO, null);
    addTemporaryCommunication(channelId, Provider.BRIGHTTALK, CommunicationSyndication.SyndicationType.IN);

    final Channel channel = new Channel();
    channel.setId(channelId);

    // Expectations
    List<Long> expectedCommunicationIds = new ArrayList<Long>();
    expectedCommunicationIds.add(communicationId1);
    expectedCommunicationIds.add(communicationId2);
    expectedCommunicationIds.add(communicationId3);

    // Do test
    List<Communication> communications = uut.findAll(channel.getId());

    // Assertions
    assertEquals(3, communications.size());

    assertTrue(expectedCommunicationIds.contains(communications.get(0).getId()));
    assertTrue(expectedCommunicationIds.contains(communications.get(1).getId()));
    assertTrue(expectedCommunicationIds.contains(communications.get(2).getId()));
  }

  @Test
  public void deleteCollectionOfCommunicationsByIds() {

    Long communicationId1 = addTemporaryCommunication(MASTER_CHANNEL_ID, Provider.BRIGHTTALK, null);
    Long communicationId2 = addTemporaryCommunication(MASTER_CHANNEL_ID, Provider.MEDIAZONE, null);
    Long communicationId3 = addTemporaryCommunication(MASTER_CHANNEL_ID, Provider.VIDEO, null);

    List<Long> communicationsToBeDeleted = new ArrayList<Long>();
    communicationsToBeDeleted.add(communicationId1);
    communicationsToBeDeleted.add(communicationId2);
    communicationsToBeDeleted.add(communicationId3);

    // do test
    uut.delete(communicationsToBeDeleted);

    // Check the results
    List<Long> communicationIdsToBeDeleted = new ArrayList<Long>();
    communicationIdsToBeDeleted.add(communicationId1);
    communicationIdsToBeDeleted.add(communicationId2);
    communicationIdsToBeDeleted.add(communicationId3);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("ids", communicationIdsToBeDeleted);

    List<Map<String, Object>> communicationsData = namedParameterJdbcTemplate.queryForList(
        "SELECT * FROM communication WHERE id IN ( :ids )", params);

    // assertions
    Status communicationStatus1 = Status.valueOf(communicationsData.get(0).get("status").toString().toUpperCase());
    assertEquals(Status.DELETED, communicationStatus1);

    Status communicationStatus2 = Status.valueOf(communicationsData.get(1).get("status").toString().toUpperCase());
    assertEquals(Status.DELETED, communicationStatus2);

    Status communicationStatus3 = Status.valueOf(communicationsData.get(2).get("status").toString().toUpperCase());
    assertEquals(Status.DELETED, communicationStatus3);
  }

  @Test
  public void findFeaturedCommunicationsWithNoChannelIds() {

    List<Long> channelIds = new ArrayList<Long>();

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void findFeaturedCommunicationsWithChannelThatHasNoCommunications() {

    final Long channelId = 666L;
    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void findFeaturedCommunicationsWithOneCommunication() {

    final Long channelId = 666L;
    Long expectedCommunicationId1 = addTemporaryCommunication(channelId, Provider.BRIGHTTALK,
        CommunicationSyndication.SyndicationType.OUT);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(expectedCommunicationId1, result.get(0).getId());
  }

  @Test
  public void findFeaturedCommunicationsWithTwoChannels() {

    final Long channelId = 666L;
    final Long channelId2 = 667l;

    Long expectedCommunicationId1 = addTemporaryCommunication(channelId, Provider.BRIGHTTALK,
        CommunicationSyndication.SyndicationType.OUT);

    Long expectedCommunicationId2 = addTemporaryCommunication(channelId2, Provider.BRIGHTTALK,
        CommunicationSyndication.SyndicationType.OUT);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);
    channelIds.add(channelId2);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(expectedCommunicationId1, result.get(0).getId());

    assertEquals(channelId2, result.get(1).getChannelId());
    assertEquals(expectedCommunicationId2, result.get(1).getId());
  }

  @Test
  public void findFeaturedCommunicationsWithSyndicatedInCommunicationBothApproved() {

    final Long channelId = 666L;
    final Long channelId2 = 667L;

    CommunicationConfigurationBuilder configurationBuilder =
        new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(communicationId, result.get(0).getId());

  }

  @Test
  public void findFeaturedCommunicationsWithSyndicatedInApprovedByMasterOnly() {

    final Long channelId = 666L;
    final Long channelId2 = 667L;

    CommunicationConfigurationBuilder configurationBuilder =
        new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(0, result.size());

  }

  @Test
  public void findFeaturedCommunicationsWithSyndicatedInApprovedByConsumerOnly() {

    final Long channelId = 666L;
    final Long channelId2 = 667L;

    CommunicationConfigurationBuilder configurationBuilder =
        new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(0, result.size());

  }

  @Test
  public void findFeaturedCommunicationsWithSyndicatedInNotApprovedByBoth() {

    final Long channelId = 666L;
    final Long channelId2 = 667L;

    CommunicationConfigurationBuilder configurationBuilder =
        new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(0, result.size());

  }

  @Test
  public void findFeaturedCommunicationsWithTwoChannelsOneOfWhichIsEmpty() {

    final Long channelId = 666L;
    final Long channelId2 = 667l;

    Long expectedCommunicationId1 = addTemporaryCommunication(channelId, Provider.BRIGHTTALK,
        CommunicationSyndication.SyndicationType.OUT);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);
    channelIds.add(channelId2);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(expectedCommunicationId1, result.get(0).getId());
  }

  @Test
  public void findFeaturedCommunicationsWithTwoFeaturedCommunictionsOneChannel() {

    final Long channelId = 666L;

    Communication communication1 = buildCommunication(channelId);
    Communication communication2 = buildCommunication(channelId);

    Date scheduledDateTime = new Date();
    communication1.setScheduledDateTime(scheduledDateTime);
    communication2.setScheduledDateTime(scheduledDateTime);

    addTemporaryCommunication(channelId, communication1);
    addTemporaryCommunication(channelId, communication2);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(2, result.size());
  }

  @Test
  public void findFeaturedCommunicationsWithTwoFeaturedCommunictionsOneChannelButOnePrivate() {

    final Long channelId = 666L;

    Communication communication1 = buildCommunication(channelId);
    Communication communication2 = buildCommunication(channelId);

    Date scheduledDateTime = new Date();
    communication1.setScheduledDateTime(scheduledDateTime);
    communication2.setScheduledDateTime(scheduledDateTime);

    communication2.getConfiguration().setVisibility(Visibility.PRIVATE);

    Long expectedCommunicationId1 = addTemporaryCommunication(channelId, communication1);
    addTemporaryCommunication(channelId, communication2);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(expectedCommunicationId1, result.get(0).getId());
  }

  @Test
  public void findFeaturedCommunicationsWithTwoFeaturedCommunictionsOneChannelButOneNotPublished() {

    final Long channelId = 666L;

    Communication communication1 = buildCommunication(channelId);
    Communication communication2 = buildCommunication(channelId);

    Date scheduledDateTime = new Date();
    communication1.setScheduledDateTime(scheduledDateTime);
    communication2.setScheduledDateTime(scheduledDateTime);

    communication2.setPublishStatus(PublishStatus.UNPUBLISHED);

    Long expectedCommunicationId1 = addTemporaryCommunication(channelId, communication1);
    addTemporaryCommunication(channelId, communication2);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(expectedCommunicationId1, result.get(0).getId());
  }

  @Test(expected = EmptyResultDataAccessException.class)
  public void removeCommunicationVoteAssets() {

    final Long channelId = 666L;
    Communication communication = buildCommunication(channelId);
    Long expectedCommunicationId = addTemporaryCommunication(channelId, communication);

    final Long voteId = 6666l;
    addCommunicationVoteAsset(voteId, expectedCommunicationId);

    // do test
    uut.removeVotes(expectedCommunicationId);

    // verify

    jdbcTemplate.queryForMap("SELECT * FROM communication_asset WHERE is_active = 1 AND communication_id = ?",
        expectedCommunicationId);

    // fail if we get pass that query
    assertTrue(false);
  }

  @Test
  public void findFeaturedCommunicationsChannelWithMultipleCommunicationsAndMultipleFeatured() {

    final Long channelId = 666L;

    Communication communication1 = buildCommunication(channelId);
    Communication communication2 = buildCommunication(channelId);
    Communication communication3 = buildCommunication(channelId);

    Date scheduledDateTime = new Date();
    communication1.setScheduledDateTime(scheduledDateTime);
    communication2.setScheduledDateTime(scheduledDateTime);
    communication3.setScheduledDateTime(DateUtils.addDays(scheduledDateTime, 1));

    Long expectedCommunicationId1 = addTemporaryCommunication(channelId, communication1);
    Long expectedCommunicationId2 = addTemporaryCommunication(channelId, communication2);
    addTemporaryCommunication(channelId, communication3);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, false);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(expectedCommunicationId1, result.get(0).getId());

    assertEquals(channelId, result.get(1).getChannelId());
    assertEquals(expectedCommunicationId2, result.get(1).getId());
  }

  @Test
  public void findFeaturedCommunicationsWithOneCommunicationIncludePrivate() {

    final Long channelId = 666L;
    Long expectedCommunicationId1 = addTemporaryCommunication(channelId, Provider.BRIGHTTALK,
        CommunicationSyndication.SyndicationType.OUT);

    List<Long> channelIds = new ArrayList<Long>();
    channelIds.add(channelId);

    // do test
    List<Communication> result = uut.findFeatured(channelIds, true);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(expectedCommunicationId1, result.get(0).getId());
  }

  @Test
  public void shouldReturnTotalsWhenExist() {

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, CHANNEL_TYPE_ID, true);

    final Long upcoming = 10L;
    final Long recorded = 2L;
    final Long live = 1L;
    final Long subscribers = 0L;
    addChannelStatistics(channelId, upcoming, recorded, live, subscribers);

    addChannelSubscription(channelId, USER_ID, true);

    Map<String, Long> webcastTotals = uut.findSubscribedChannelsCommunicationsTotals(USER_ID);

    long recordedCountResult = webcastTotals.get(Status.RECORDED.toString().toLowerCase());
    long liveCountResult = webcastTotals.get(Status.LIVE.toString().toLowerCase());
    long upcomingCountResult = webcastTotals.get(Status.UPCOMING.toString().toLowerCase());

    assertEquals(2, recordedCountResult);
    assertEquals(1, liveCountResult);
    assertEquals(10, upcomingCountResult);
    assertEquals(3, webcastTotals.size());

  }

  @Test
  public void shouldNotReturnTotalsWhenChannelNotActive() {

    boolean isChannelActive = false;

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, CHANNEL_TYPE_ID, isChannelActive);

    addChannelSubscription(channelId, USER_ID, true);

    Map<String, Long> webcastTotals = uut.findSubscribedChannelsCommunicationsTotals(USER_ID);

    Long countResult = webcastTotals.get(Status.RECORDED.toString().toLowerCase());

    assertNotNull(countResult);
    long recordedCountResult = webcastTotals.get(Status.RECORDED.toString().toLowerCase());
    long liveCountResult = webcastTotals.get(Status.LIVE.toString().toLowerCase());
    long upcomingCountResult = webcastTotals.get(Status.UPCOMING.toString().toLowerCase());

    assertEquals(0, recordedCountResult);
    assertEquals(0, liveCountResult);
    assertEquals(0, upcomingCountResult);
    assertEquals(3, webcastTotals.size());

  }

  @Test
  public void shouldNotReturnTotalsWhenSubscriptionIsNotActive() {

    Long channelId = addTemporaryChannel(CHANNEL_TYPE_ID, CHANNEL_CREATED_TYPE_ID, CHANNEL_TYPE_ID, true);

    boolean isSubscriptionActive = false;
    addChannelSubscription(channelId, USER_ID, isSubscriptionActive);

    Map<String, Long> webcastTotals = uut.findSubscribedChannelsCommunicationsTotals(USER_ID);

    Long countResult = webcastTotals.get(Status.RECORDED.toString().toLowerCase());

    assertNotNull(countResult);
    long recordedCountResult = webcastTotals.get(Status.RECORDED.toString().toLowerCase());
    long liveCountResult = webcastTotals.get(Status.LIVE.toString().toLowerCase());
    long upcomingCountResult = webcastTotals.get(Status.UPCOMING.toString().toLowerCase());

    assertEquals(0, recordedCountResult);
    assertEquals(0, liveCountResult);
    assertEquals(0, upcomingCountResult);
    assertEquals(3, webcastTotals.size());
  }

  @Test
  public void update() {

    final long channelId = 667;
    final long communicationId = addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    final String expectedTitle = "expectedTitle";
    final String expectedDescription = "expectedDescription";
    final String expectedKeywords = "expectedKeywords";
    final String expectedLivePhoneNumber = "expectedPhoneNumber";
    final Format expectedFormat = Format.AUDIO;
    final Status expectedStatus = Status.PROCESSING;
    final String expectedLiveUrl = "expectedLiveUrl";
    final Provider expectedProvider = new Provider(Provider.MEDIAZONE);
    final String expectedPinNumber = "expPin";
    final String expectedPresenter = "expectedPresenter";
    final String expectedThumbnail = "expectedThumbnail";
    final String expectedPreview = "expectedPreview";

    Communication communication = buildCommunication(channelId);
    communication.setId(communicationId);
    communication.setProvider(expectedProvider);
    communication.setTitle(expectedTitle);
    communication.setDescription(expectedDescription);
    communication.setKeywords(expectedKeywords);
    communication.setLivePhoneNumber(expectedLivePhoneNumber);
    communication.setLiveUrl(expectedLiveUrl);
    communication.setPinNumber(expectedPinNumber);
    communication.setAuthors(expectedPresenter);
    communication.setPreviewUrl(expectedPreview);
    communication.setThumbnailUrl(expectedThumbnail);
    communication.setFormat(expectedFormat);
    communication.setStatus(expectedStatus);

    // do test
    uut.pivot(communication);

    // verify
    Map<String, Object> updatedCommunication = jdbcTemplate.queryForMap("SELECT * FROM communication WHERE id = ?",
        communicationId);

    assertEquals(expectedTitle, updatedCommunication.get("title"));
    assertEquals(expectedDescription, updatedCommunication.get("description"));
    assertEquals(expectedKeywords, updatedCommunication.get("keywords"));
    assertEquals(expectedLivePhoneNumber, updatedCommunication.get("live_phone_number"));
    assertEquals(expectedLiveUrl, updatedCommunication.get("live_url"));
    assertEquals(expectedProvider.getName(), updatedCommunication.get("provider"));
    assertEquals(expectedPinNumber, updatedCommunication.get("pin_number"));
    assertEquals(expectedPresenter, updatedCommunication.get("presenter"));
    assertEquals(expectedPreview, updatedCommunication.get("preview_url"));
    assertEquals(expectedThumbnail, updatedCommunication.get("thumbnail"));
    assertEquals(expectedFormat.toString().toLowerCase(), updatedCommunication.get("format"));
    assertEquals(expectedStatus.toString().toLowerCase(), updatedCommunication.get("status"));
  }

  @Test
  public void restoreMediazoneAssets() {

    final Long channelId = 666L;
    Communication communication = buildCommunication(channelId);
    Long communicationId = addTemporaryCommunication(channelId, communication);

    addMediazoneCommunicationAsset("someUrl", "mzId", communicationId, false);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("id", communicationId);

    List<Map<String, Object>> assetsData = namedParameterJdbcTemplate.queryForList(
        "SELECT * FROM communication_asset WHERE is_active = 1 AND communication_id = :id", params);

    assertEquals(0, assetsData.size());

    uut.restoreMediazoneAssets(communicationId);

    assetsData = namedParameterJdbcTemplate.queryForList(
        "SELECT * FROM communication_asset WHERE is_active = 1 AND communication_id = :id", params);

    // assertions
    assertEquals(2, assetsData.size());
  }

  @Test
  public void updatePublicationStatus() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(666l).withPublishStatus(
        PublishStatus.UNPUBLISHED).build();
    Long commId = addTemporaryCommunication(666l, communication);
    List<Long> communicationIds = Arrays.asList(commId);

    // do test
    uut.updatePublishStatus(communicationIds, PublishStatus.PUBLISHED);

    String resultStatus = jdbcTemplate.queryForObject("SELECT publish_status FROM communication WHERE id = ?",
        new Object[] { commId }, String.class);

    String expectedResult = PublishStatus.PUBLISHED.toString().toLowerCase();
    assertEquals(expectedResult, resultStatus);
  }

  @Test
  public void findRenditionAssets() {
    // Set Up
    final Long channelId = 666L;
    Communication communication = buildCommunication(channelId);
    Long communicationId = addTemporaryCommunication(channelId, communication);

    final Long renditionId = addCommunicationRendition();
    addCommunicationRenditionAsset(renditionId, communicationId);

    // Excercise
    List<Rendition> renditions = uut.findRenditionAssets(communicationId);

    // Assertions
    assertNotNull(renditions);
    assertEquals(1, renditions.size());
  }

  @Test
  public void loadCommunicationStatistics() {
    // Set Up
    final Long channelId = 666L;
    Communication communication = buildCommunication(channelId);
    Long communicationId = addTemporaryCommunication(channelId, communication);

    communication.setId(communicationId);

    CommunicationStatistics viewingStatistics = new CommunicationStatisticsBuilder().withDefaultValues().build();

    communication.setCommunicationStatistics(viewingStatistics);

    addCommunicationStatistics(communication);

    // Excercise
    List<CommunicationStatistics> viewingStatisticsResults = uut.findCommunicationStatistics(communication);

    // Assertions
    assertNotNull(viewingStatisticsResults.get(0).getNumberOfViewings());
    assertEquals(CommunicationStatisticsBuilder.DEFAULT_NUMBER_OF_VIEWINGS,
        viewingStatisticsResults.get(0).getNumberOfViewings());
    assertNotNull(viewingStatistics.getAverageRating());
    assertEquals(CommunicationStatisticsBuilder.DEFAULT_AVERAGE_RATING,
        viewingStatisticsResults.get(0).getAverageRating(), 0.0);
    assertEquals(CommunicationStatisticsBuilder.DEFAULT_NUMBER_OF_RATINGS,
        viewingStatisticsResults.get(0).getNumberOfRatings());
  }

  @Test
  public void findByPinAndProviderReturnsCommunication() {
    final Long channelId = 666L;
    final String pin = "99988874";
    final String providerString = "brighttalkhd";

    Provider provider = new Provider(providerString);
    Communication communication = buildCommunication(channelId);
    communication.setPinNumber(pin);
    communication.setProvider(provider);

    Long communicationId = addTemporaryCommunication(channelId, communication);

    // do test
    Communication result = uut.find(pin, providerString);

    assertNotNull(result);
    assertEquals(pin, result.getPinNumber());
    assertEquals(providerString, communication.getProvider().getName());
    assertEquals(communicationId, result.getId());
  }

  @Test
  public void findByPinOnlyReturnsCommunication() {
    final Long channelId = 666L;
    final String pin = "99988874";
    final String providerString = "brighttalkhd";

    Provider provider = new Provider(providerString);
    Communication communication = buildCommunication(channelId);
    communication.setPinNumber(pin);
    communication.setProvider(provider);

    Long communicationId = addTemporaryCommunication(channelId, communication);

    // do test
    Communication result = uut.find(pin, null);

    assertNotNull(result);
    assertEquals(pin, result.getPinNumber());
    assertEquals(providerString, communication.getProvider().getName());
    assertEquals(communicationId, result.getId());
  }

  @Test(expected = NotFoundException.class)
  public void findByPinAndProviderReturnsNotFound() {
    final Long channelId = 666L;
    final String pin = "99988874";
    final String providerString = "brighttalkhd";

    Provider provider = new Provider(providerString);
    Communication communication = buildCommunication(channelId);
    communication.setPinNumber(pin);
    communication.setProvider(provider);

    addTemporaryCommunication(channelId, communication);

    // do test
    uut.find("88844455", providerString);

  }
}