/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FormCreateValidatorTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.survey.validator;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.brighttalk.common.form.dto.FormDto;
import com.brighttalk.common.form.dto.FormItemDto;
import com.brighttalk.service.channel.business.error.SurveyNotFoundException;

/**
 * Unit tests for {@link FormCreateValidator}.
 */
public class FormCreateValidatorTest {

  private FormCreateValidator uut;

  @Before
  public void setUp() {
    uut = new FormCreateValidator();

  }

  @Test
  public void testValidatePass() {

    // setup
    FormDto formDto = new FormDto();
    formDto.add(new FormItemDto());

    // do test
    try {
      uut.validate(formDto);

    } catch (Exception ie) {
      // assert
      Assert.fail();
    }
  }

  @Test(expected = SurveyNotFoundException.class)
  public void testFormItemsIsNull() {

    // setup
    FormDto formDto = new FormDto();
    formDto.setFormItems(null);

    // do test
    uut.validate(formDto);
  }

  @Test(expected = SurveyNotFoundException.class)
  public void testMissingFormItems() {

    // setup
    FormDto formDto = new FormDto();
    formDto.setFormItems(new ArrayList<FormItemDto>());

    // do test
    uut.validate(formDto);
  }

}
