/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcSubscriptionDbDaoTest.java 97779 2015-07-09 13:11:27Z build $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.Subscription.Referral;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.error.SubscriptionNotFoundException;
import com.brighttalk.service.utils.DomainFactoryUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcSubscriptionDbDaoTest extends AbstractJdbcDbDaoTest {

  private static final String TEST_LEAD_CONTEXT = "999";
  private static final Integer TEST_LEAD_ENGAGEMENT_SCORE = 77;

  @Autowired
  private JdbcSubscriptionDbDao uut;

  @Test
  public void unsubscribeAllUsersFromChannel() {

    final Long channelId = buildRandomLong();
    final Long userId1 = buildRandomLong();
    final Long userId2 = buildRandomLong();

    addChannelSubscription(channelId, userId1, true);
    addChannelSubscription(channelId, userId2, true);

    temporaryChannelIds.add(channelId);

    // do test
    uut.unsubscribeUsers(channelId);

    // Check the results
    List<Map<String, Object>> subscriptions = jdbcTemplate.queryForList(
        "SELECT is_active FROM subscription WHERE channel_id = ?", channelId);

    // assertions
    assertEquals(2, subscriptions.size());

    Boolean subscriptionIsActive1 = (Boolean) subscriptions.get(0).get("is_active");
    assertFalse(subscriptionIsActive1);

    Boolean subscriptionIsActive2 = (Boolean) subscriptions.get(1).get("is_active");
    assertFalse(subscriptionIsActive2);
  }

  @Test
  public void unsubscribeUsersFromChannels() {

    final Long channelId1 = buildRandomLong();
    final Long channelId2 = buildRandomLong();
    final Long userId = buildRandomLong();

    addChannelSubscription(channelId1, userId, true);
    addChannelSubscription(channelId2, userId, true);

    temporaryChannelIds.add(channelId1);
    temporaryChannelIds.add(channelId2);

    // do test
    uut.unsubscribeUser(userId);

    // Check the results
    List<Map<String, Object>> subscriptions = jdbcTemplate.queryForList(
        "SELECT is_active FROM subscription WHERE user_id = ? AND is_active = 1", userId);

    // assertions
    assertEquals(0, subscriptions.size());
  }

  @Test
  public void unsubscribeUsersFromChannelsWhenNoChannelsThrowsNoExceptions() {

    final Long userId = buildRandomLong();

    // do test
    uut.unsubscribeUser(userId);

    // Check the results
    List<Map<String, Object>> subscriptions = jdbcTemplate.queryForList(
        "SELECT is_active FROM subscription WHERE user_id = ? AND is_active = 1", userId);

    // assertions
    assertEquals(0, subscriptions.size());
  }

  @Test
  public void unsubscribeUserShouldDeactivateUserWhenUserIsSubscribedToExistingChannel() {

    final Long channelId = buildRandomLong();
    final Long userId1 = buildRandomLong();

    addChannelSubscription(channelId, userId1, true);

    // do test
    uut.unsubscribeUser(channelId, userId1);

    // Check the results
    List<Map<String, Object>> subscriptions = jdbcTemplate.queryForList(
        "SELECT is_active FROM subscription WHERE channel_id = ? AND user_id = ?", channelId, userId1);

    // assertions
    assertEquals(1, subscriptions.size());

    Boolean subscriptionIsActive1 = (Boolean) subscriptions.get(0).get("is_active");
    assertFalse(subscriptionIsActive1);

  }

  @Test
  public void unsubscribeUserShouldDeactivateUserWhenUserIsNotSubscribedToExistingChannel() {

    final Long channelId = buildRandomLong();
    final Long userId1 = buildRandomLong();

    addChannelSubscription(channelId, userId1, false);

    // do test
    uut.unsubscribeUser(channelId, userId1);

    // Check the results
    List<Map<String, Object>> subscriptions = jdbcTemplate.queryForList(
        "SELECT is_active FROM subscription WHERE channel_id = ? AND user_id = ?", channelId, userId1);

    // assertions
    assertEquals(1, subscriptions.size());

    Boolean subscriptionIsActive1 = (Boolean) subscriptions.get(0).get("is_active");
    assertFalse(subscriptionIsActive1);

  }

  @Test
  public void unsubscribeUserShouldNotDeactivateUserWhenUserIsNotSubscribedToExistingChannel() {

    final Long channelId = buildRandomLong();
    final Long userId1 = buildRandomLong();

    temporaryChannelIds.add(channelId);

    // do test
    uut.unsubscribeUser(channelId, userId1);

    // Check the results
    List<Map<String, Object>> subscriptions = jdbcTemplate.queryForList(
        "SELECT is_active FROM subscription WHERE channel_id = ? AND user_id = ?", channelId, userId1);

    // assertions
    assertEquals(0, subscriptions.size());

  }

  @Test
  public void unsubscribeUserShouldNotDeactivateUserWhenUserIsSubscribedToNotExistingChannel() {

    final Long channelId = buildRandomLong();
    final Long channelId2 = buildRandomLong();
    final Long userId1 = buildRandomLong();

    addChannelSubscription(channelId, userId1, true);

    // do test
    uut.unsubscribeUser(channelId2, userId1);

    // Check the results
    List<Map<String, Object>> subscriptions = jdbcTemplate.queryForList(
        "SELECT is_active FROM subscription WHERE channel_id = ? AND user_id = ?", channelId2, userId1);

    // assertions
    assertEquals(0, subscriptions.size());

  }

  @Test(expected = SubscriptionNotFoundException.class)
  public void findSubscriptionByChannelIdAndUserIdWhileNotFound() {

    // Set up
    final Long channelId = buildRandomLong();
    final Long userId = buildRandomLong();

    // Do test
    uut.findByChannelIdAndUserId(channelId, userId);

    // Assertions
  }

  @Test(expected = SubscriptionNotFoundException.class)
  public void findSubscriptionByChannelIdAndUserIdWhileInactive() {

    // Set up
    final Long channelId = buildRandomLong();
    final Long userId = buildRandomLong();

    addChannelSubscription(channelId, userId, false);

    // Do test
    uut.findByChannelIdAndUserId(channelId, userId);

    // Assertions
  }

  /**
   * Tests {@link JdbcSubscriptionDbDao#findByChannelIdAndUserId(Long, Long)} in success cases of finding subscription
   * by channel Id and user Id with no subscription context.
   */
  @Test
  public void findSubscriptionByChannelIdAndUserIdWhileActiveAndNoContext() {

    // Set up
    final Long channelId = buildRandomLong();
    final Long userId = buildRandomLong();

    addChannelSubscription(channelId, userId, true);

    // Do test
    Subscription result = uut.findByChannelIdAndUserId(channelId, userId);

    // Assertions
    assertNotNull(result);
    assertNull(result.getContext());
  }

  /**
   * Tests {@link JdbcSubscriptionDbDao#findByChannelIdAndUserId(Long, Long)} in success cases of finding subscription
   * by channel Id and user Id including fully populated subscription context.
   */
  @Test
  public void findSubscriptionByChannelIdAndUserIdWithContext() {

    // Set up
    final Long channelId = buildRandomLong();
    final Long userId = buildRandomLong();

    SubscriptionLeadType leadType = SubscriptionLeadType.KEYWORD;

    Long subscriptionId = addChannelSubscription(channelId, userId, true);
    addSubscriptionContext(subscriptionId, leadType, TEST_LEAD_CONTEXT, TEST_LEAD_ENGAGEMENT_SCORE);

    // Do test
    Subscription result = uut.findByChannelIdAndUserId(channelId, userId);

    // Assertions
    assertNotNull(result);
    assertEquals(leadType, result.getContext().getLeadType());
    assertEquals(TEST_LEAD_CONTEXT, result.getContext().getLeadContext());
    assertEquals(TEST_LEAD_ENGAGEMENT_SCORE.toString(), result.getContext().getEngagementScore());
  }

  /**
   * Tests {@link JdbcSubscriptionDbDao#findByChannelIdAndUserId(Long, Long)} in success cases of finding subscription
   * by channel Id and user Id including subscription context with no engagement score.
   */
  @Test
  public void findSubscriptionByChannelIdAndUserIdWithContextNoEngagementScore() {

    // Set up
    final Long channelId = buildRandomLong();
    final Long userId = buildRandomLong();

    SubscriptionLeadType leadType = SubscriptionLeadType.KEYWORD;

    Long subscriptionId = addChannelSubscription(channelId, userId, true);
    addSubscriptionContext(subscriptionId, leadType, TEST_LEAD_CONTEXT, null);

    // Do test
    Subscription result = uut.findByChannelIdAndUserId(channelId, userId);

    // Assertions
    assertNotNull(result);
    assertEquals(leadType, result.getContext().getLeadType());
    assertEquals(TEST_LEAD_CONTEXT, result.getContext().getLeadContext());
    assertNull(result.getContext().getEngagementScore());
  }

  @Test
  public void findSubscriptionsByChannelId() {

    // Set up
    final Long channelId = buildRandomLong();
    final Long userId1 = buildRandomLong();
    final Long userId2 = buildRandomLong();

    addChannelSubscription(channelId, userId1, true);
    addChannelSubscription(channelId, userId2, false);

    // Do test
    List<Subscription> result = uut.findByChannelId(channelId);

    // Assertions
    assertEquals(2, result.size());
  }

  @Test
  public void findSubscriptionsByChannelIdWhenNone() {

    // Set up
    final Long channelId = buildRandomLong();

    // Do test
    List<Subscription> result = uut.findByChannelId(channelId);

    // Assertions
    assertTrue(result.isEmpty());
  }

  /**
   * Tests {@link JdbcSubscriptionDbDao#findByChannelId(Long)} in success cases of finding subscription by channel Id
   * with no subscription context.
   */
  @Test
  public void findSubscriptionsByChannelIdWithNoContext() {

    // Set up
    final Long channelId = buildRandomLong();
    final Long userId1 = buildRandomLong();
    addChannelSubscription(channelId, userId1, true);

    // Do test
    List<Subscription> result = uut.findByChannelId(channelId);

    // Assertions
    assertEquals(1, result.size());
    Subscription subscription = result.get(0);
    assertNull(subscription.getContext());
  }

  /**
   * Tests {@link JdbcSubscriptionDbDao#findByChannelId(Long)} in success cases of finding subscription by channel Id
   * including subscription context with no engagement score.
   */
  @Test
  public void findSubscriptionsByChannelIdWithContextNoEngagementScore() {

    // Set up
    final Long channelId = buildRandomLong();
    final Long userId1 = buildRandomLong();
    Long subId = addChannelSubscription(channelId, userId1, true);
    addSubscriptionContext(subId, SubscriptionLeadType.CONTENT, TEST_LEAD_CONTEXT, null);

    // Do test
    List<Subscription> result = uut.findByChannelId(channelId);

    // Assertions
    assertEquals(1, result.size());
    Subscription subscription = result.get(0);
    SubscriptionContext context = subscription.getContext();
    assertNotNull(context);
    assertEquals(SubscriptionLeadType.CONTENT, context.getLeadType());
    assertEquals(TEST_LEAD_CONTEXT, context.getLeadContext());
    assertNull(context.getEngagementScore());
  }

  /**
   * Tests {@link JdbcSubscriptionDbDao#findByChannelId(Long)} in success cases of finding subscription by channel Id
   * including subscription context with no engagement score.
   */
  @Test
  public void findSubscriptionsByChannelIdWithContext() {

    // Set up
    final Long channelId = buildRandomLong();
    final Long userId1 = buildRandomLong();
    Long subId = addChannelSubscription(channelId, userId1, true);
    addSubscriptionContext(subId, SubscriptionLeadType.SUMMIT, TEST_LEAD_CONTEXT, TEST_LEAD_ENGAGEMENT_SCORE);

    // Do test
    List<Subscription> result = uut.findByChannelId(channelId);

    // Assertions
    assertEquals(1, result.size());
    Subscription subscription = result.get(0);
    SubscriptionContext context = subscription.getContext();
    assertNotNull(context);
    assertEquals(SubscriptionLeadType.SUMMIT, context.getLeadType());
    assertEquals(TEST_LEAD_CONTEXT, context.getLeadContext());
    assertEquals(TEST_LEAD_ENGAGEMENT_SCORE.toString(), context.getEngagementScore());
  }

  private Long buildRandomLong() {
    return new Long(new Random().nextInt(100));
  }

  @Test
  public void createSubscription() {

    // Set up
    Channel channel = new Channel();
    channel.setId(buildRandomLong());
    temporaryChannelIds.add(channel.getId());

    User user = new User();
    user.setId(buildRandomLong());

    Subscription subscription = new Subscription();
    subscription.setReferral(Referral.paid);
    subscription.setUser(user);

    // Do test
    Subscription result = uut.create(subscription, channel);

    // Assertions
    assertNotNull(result.getId());
    assertEquals(user.getId(), result.getUser().getId());
    assertEquals(subscription.getReferral(), result.getReferral());
    assertNotNull(result.getLastSubscribed());
  }

  /**
   * Tests {@link JdbcSubscriptionDbDao#findById(Long)} in the success case of finding subscription by Id.
   */
  @Test
  public void findByIdSuccess() {
    // Set up
    final Long channelId = buildRandomLong();
    final Long userId = buildRandomLong();
    SubscriptionLeadType leadType = SubscriptionLeadType.KEYWORD;
    String leadContext = "test";
    Integer engagementScore = 33;
    Long subscriptionId = addChannelSubscription(channelId, userId, true);
    addSubscriptionContext(subscriptionId, leadType, leadContext, engagementScore);

    // Do test
    Subscription result = uut.findById(subscriptionId);

    // Assertions
    assertNotNull(result);
    assertEquals(subscriptionId, result.getId());
    assertEquals(CHANNEL_REFERAL, result.getReferral().name());
    assertEquals(leadType, result.getContext().getLeadType());
    assertEquals(leadContext, result.getContext().getLeadContext());
    assertEquals(engagementScore.toString(), result.getContext().getEngagementScore());
  }

  /**
   * Tests {@link JdbcSubscriptionDbDao#findById(Long)} in the error case of not finding subscription by Id.
   */
  @Test(expected = SubscriptionNotFoundException.class)
  public void findByIdSubscriptionNotFound() {
    // Setup -
    Long testId = 768658l;

    // Do Test -
    uut.findById(testId);
  }

  /**
   * Tests {@link JdbcSubscriptionDbDao#updateReferral} in the success case of updating subscription referral.
   */
  @Test
  public void updateReferralSuccess() {
    // Setup -
    Channel channel = new Channel();
    channel.setId(buildRandomLong());
    temporaryChannelIds.add(channel.getId());
    Subscription subscription = DomainFactoryUtils.createSubscription();
    subscription.setReferral(Referral.dotcom);
    subscription = uut.create(subscription, channel);

    // Do test -
    uut.updateReferral(Referral.paid, subscription.getId());

    // Assertion
    Subscription foundSubscription = uut.findById(subscription.getId());
    assertNotNull(foundSubscription);
    assertEquals(Referral.paid, foundSubscription.getReferral());
  }

  @Test
  public void unsubscribeUserFromChannel() {

    // Set up
    Long userId = buildRandomLong();
    boolean isActive = true;

    final Long channelId = addTemporaryChannelWithTypes();

    addChannelSubscription(channelId, userId, isActive);

    // Expectations
    int expectedSubscriptions = 1;

    // Do test
    uut.unsubscribeUser(channelId, userId);

    // Assertions
    List<Map<String, Object>> subscriptions = jdbcTemplate.queryForList(
        "SELECT is_active FROM subscription WHERE channel_id = ? AND user_id IN(?)", channelId, userId);

    assertEquals(expectedSubscriptions, subscriptions.size());

    Boolean subscriptionIsActive = (Boolean) subscriptions.get(0).get("is_active");
    assertFalse(subscriptionIsActive);
  }

  @Test
  @SuppressWarnings("serial")
  public void unsubscribeUsersFromChannel() {

    // Set up
    final Long userId1 = buildRandomLong();
    final Long userId2 = buildRandomLong();
    boolean isActive = true;

    final Long channelId = addTemporaryChannelWithTypes();

    addChannelSubscription(channelId, userId1, isActive);
    addChannelSubscription(channelId, userId2, isActive);

    List<Long> userIds = new ArrayList<Long>() {
      {
        add(userId1);
        add(userId2);
      }
    };

    // Expectations
    int expectedSubscriptions = 2;

    // Do test
    uut.unsubscribeUsers(channelId, userIds);

    // Assertions
    List<Map<String, Object>> subscriptions =
        jdbcTemplate.queryForList(
            "SELECT is_active FROM subscription WHERE channel_id = ? AND user_id IN(?,?)", channelId, userId1, userId2);

    assertEquals(expectedSubscriptions, subscriptions.size());

    Boolean subscriptionIsActive = (Boolean) subscriptions.get(0).get("is_active");
    assertFalse(subscriptionIsActive);

    subscriptionIsActive = (Boolean) subscriptions.get(1).get("is_active");
    assertFalse(subscriptionIsActive);
  }
}
