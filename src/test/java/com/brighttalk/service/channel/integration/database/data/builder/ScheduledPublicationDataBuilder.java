package com.brighttalk.service.channel.integration.database.data.builder;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.database.data.ScheduledPublicationData;

import java.util.Date;

public class ScheduledPublicationDataBuilder
{
	public static final Long DEFAULT_COMMUNICATION_ID = 667l;

	private Long communicationId;
	private Date scheduledDate;
	private String jobId;
	private boolean completed;
	private Communication.PublishStatus publishStatus;
	private boolean active;

	public ScheduledPublicationDataBuilder withDefaultValues() {

		communicationId = DEFAULT_COMMUNICATION_ID;
		scheduledDate = new Date();
		publishStatus = Communication.PublishStatus.PUBLISHED;
		active = true;

		return this;
	}

	public ScheduledPublicationDataBuilder withScheduledDate(final Date value) {
		scheduledDate = value;
		return this;
	}

	public ScheduledPublicationDataBuilder withJobId(final String value) {
		jobId = value;
		return this;
	}

	public ScheduledPublicationDataBuilder withCompleted(final boolean value) {
		completed = value;
		return this;
	}

	public ScheduledPublicationDataBuilder withPublishStatus(final Communication.PublishStatus value) {
		publishStatus = value;
		return this;
	}

	public ScheduledPublicationDataBuilder withCommunicationId(final Long value)
	{
		communicationId = value;
		return this;
	}

	public ScheduledPublicationDataBuilder withActive(final boolean value)
	{
		active = value;
		return this;
	}

	public ScheduledPublicationData build() {

		ScheduledPublicationData data = new ScheduledPublicationData();
		data.setCommunicationId(communicationId);
		data.setScheduledDate(scheduledDate);
		data.setJobId(jobId);
		data.setCompleted(completed);
		data.setPublishStatus(publishStatus);
		data.setActive(active);

		return data;
	}

}