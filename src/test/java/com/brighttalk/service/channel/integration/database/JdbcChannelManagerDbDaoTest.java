package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.error.ChannelManagerNotFoundException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcChannelManagerDbDaoTest {

  private static final Long CHANNEL_ID = 999L;

  private static final Long USER_ID = 888L;

  private static final Long CHANNEL_MANAGER_ID = 777L;

  private static final String EMAIL_ADDRESS = "email@brighttalk.com";

  private static final boolean EMAIL_ALERTS_ENABLED = true;

  private static final boolean EMAIL_ALERTS_DISABLED = false;

  protected JdbcTemplate jdbcTemplate;

  @Autowired
  private JdbcChannelManagerDbDao uut;

  @Autowired
  public void setDataSource(final DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);
  }

  @Before
  public void setUp() {

  }

  @After
  public void tearDown() {
    jdbcTemplate.update("DELETE FROM channel_manager");
  }

  @Test
  public void testCreate() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = buildChannelManager(channel);

    // Do test
    ChannelManager newChannelManager = uut.create(channel, channelManager);

    // Assertions
    assertNotNull(newChannelManager.getId());
    assertEquals(newChannelManager.getUser(), channelManager.getUser());
    assertEquals(newChannelManager.getChannel().getId(), channelManager.getChannel().getId());
    assertFalse(newChannelManager.getEmailAlerts());
  }

  @Test
  public void testCreateWhenDuplicatedChannelManager() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = buildChannelManager(channel);
    ChannelManager duplicatedChannelManager = buildChannelManager(channel);

    // Do test
    ChannelManager newChannelManager = uut.create(channel, channelManager);
    ChannelManager newDuplicatedChannelManager = uut.create(channel, duplicatedChannelManager);

    // Assertions
    assertEquals(newChannelManager, newDuplicatedChannelManager);
  }

  @Test
  public void testUpdateWhenEmailAlertsEnabled() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = createChannelManager(channel);

    channelManager.setEmailAlerts(EMAIL_ALERTS_ENABLED);

    // Do test
    ChannelManager updatedChannelManager = uut.update(channelManager);

    // Assertions
    assertTrue(updatedChannelManager.getEmailAlerts());
  }

  @Test
  public void testUpdateWhenEmailAlertsDisabled() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = createChannelManager(channel);

    channelManager.setEmailAlerts(EMAIL_ALERTS_DISABLED);

    // Do test
    ChannelManager updatedChannelManager = uut.update(channelManager);

    // Assertions
    assertFalse(updatedChannelManager.getEmailAlerts());
  }

  @Test
  public void testDelete() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = createChannelManager(channel);

    // Do test
    uut.delete(channelManager.getId());

    // Assertions
    Map<String, Object> deletedChannelManager = jdbcTemplate.queryForMap("SELECT * FROM channel_manager WHERE id = ?",
        channelManager.getId());

    Boolean isChannelManagerActive = (Boolean) deletedChannelManager.get("is_active");

    assertFalse(isChannelManagerActive);
  }

  @Test
  public void testDeleteByUser() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = createChannelManager(channel);

    // Do test
    uut.deleteByUser(USER_ID);

    // Assertions
    Map<String, Object> deletedChannelManager = jdbcTemplate.queryForMap(
        "SELECT * FROM channel_manager WHERE user_id = ?", channelManager.getUser().getId());

    Boolean isChannelManagerActive = (Boolean) deletedChannelManager.get("is_active");

    assertFalse(isChannelManagerActive);
  }

  @Test
  public void testFind() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = buildChannelManager();
    channelManager = addTemporaryChannelManager(channel, channelManager);

    // Do test
    ChannelManager channelManagerFound = uut.find(channelManager.getId());

    // Assertions
    assertEquals(channelManager, channelManagerFound);
  }

  @Test(expected = ChannelManagerNotFoundException.class)
  public void testFindWhenNotFound() {

    // Set up

    // Do test
    uut.find(CHANNEL_MANAGER_ID);

    // Assertions
  }

  @Test
  public void testFindByChannelAndUser() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = buildChannelManager();
    channelManager = addTemporaryChannelManager(channel, channelManager);

    // Do test
    ChannelManager channelManagerFound = uut.findByChannelAndUser(channel.getId(), USER_ID);

    // Assertions
    assertEquals(channelManager, channelManagerFound);
  }

  @Test(expected = ChannelManagerNotFoundException.class)
  public void testFindByChannelAndUserWhenNotFound() {

    // Set up

    // Do test
    uut.findByChannelAndUser(CHANNEL_ID, USER_ID);

    // Assertions
  }

  @Test
  public void testFindByChannelAndChannelManager() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = buildChannelManager();
    channelManager = addTemporaryChannelManager(channel, channelManager);

    // Do test
    ChannelManager channelManagerFound = uut.findByChannelAndChannelManager(channel.getId(), channelManager.getId());

    // Assertions
    assertEquals(channelManager, channelManagerFound);
  }

  @Test(expected = ChannelManagerNotFoundException.class)
  public void testFindByChannelAndChannelManagerWhenNotFound() {

    // Set up

    // Do test
    uut.findByChannelAndChannelManager(CHANNEL_ID, CHANNEL_MANAGER_ID);

    // Assertions
  }

  @Test
  public void testFindByChannel() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    ChannelManager channelManager = buildChannelManager();
    channelManager = addTemporaryChannelManager(channel, channelManager);

    // Do test
    List<ChannelManager> channelManagers = uut.find(channel);

    // Assertions
    assertNotNull(channelManagers);
    assertFalse(channelManagers.isEmpty());
    assertEquals(channelManagers.get(0), channelManager);
  }

  @Test
  public void testFindByChannelWhenNotFound() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    // Do test
    uut.find(channel);

    // Do test
    List<ChannelManager> channelManagers = uut.find(channel);

    // Assertions
    assertNotNull(channelManagers);
    assertTrue(channelManagers.isEmpty());
  }

  @Test
  public void testCountChannelManagers() {

    // Set up
    Channel channel = new Channel(CHANNEL_ID);

    Integer noChannelManagers = 1;

    ChannelManager channelManager = buildChannelManager();
    addTemporaryChannelManager(channel, channelManager);

    // Do test
    Integer numberOfChannelManagers = uut.count(CHANNEL_ID);

    // Assertions
    assertEquals(numberOfChannelManagers, noChannelManagers);
  }

  @Test
  public void testCountChannelManagersWhenNone() {

    // Set up
    Integer noChannelManagers = 0;

    // Do test
    Integer numberOfChannelManagers = uut.count(CHANNEL_ID);

    // Assertions
    assertEquals(numberOfChannelManagers, noChannelManagers);
  }

  private ChannelManager createChannelManager(final Channel channel) {

    ChannelManager channelManager = buildChannelManager();

    return uut.create(channel, channelManager);
  }

  private ChannelManager addTemporaryChannelManager(final Channel channel, final ChannelManager channelManager) {

    return uut.create(channel, channelManager);
  }

  private ChannelManager buildChannelManager() {

    Channel channel = new Channel(CHANNEL_ID);

    return buildChannelManager(channel);
  }

  private ChannelManager buildChannelManager(final Channel channel) {

    User user = new User();
    user.setId(USER_ID);
    user.setEmail(EMAIL_ADDRESS);

    return buildChannelManager(channel, user, CHANNEL_MANAGER_ID);
  }

  private ChannelManager buildChannelManager(final Channel channel, final User user, final Long channelManagerId) {

    ChannelManager channelManager = new ChannelManager();
    channelManager.setId(channelManagerId);
    channelManager.setUser(user);
    channelManager.setChannel(channel);

    return channelManager;
  }
}
