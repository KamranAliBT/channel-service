/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpLiveStateServiceDaoTest.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.livestate;

import static org.easymock.EasyMock.isA;

import java.net.URI;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.livestate.dto.RequestDto;

public class XmlHttpLiveStateServiceDaoTest {

  private static final String SERVICE_BASE_URL = "http://test.booking.brighttalk.net";

  private XmlHttpLiveStateServiceDao uut;

  private RestTemplate mockRestTemplate;

  @Before
  public void setUp() {

    uut = new XmlHttpLiveStateServiceDao();
    uut.setServiceBaseUrl(SERVICE_BASE_URL);

    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    uut.setRestTemplate(mockRestTemplate);
  }

  @Test
  public void deleteCommunication() throws Exception {

    final Long communicationId = 4432l;

    EasyMock.expect(mockRestTemplate.postForLocation(isA(String.class), isA(RequestDto.class))).andReturn(new URI(""));
    EasyMock.replay(mockRestTemplate);

    // do test
    uut.deleteCommunication(communicationId);

    EasyMock.verify(mockRestTemplate);
  }
}