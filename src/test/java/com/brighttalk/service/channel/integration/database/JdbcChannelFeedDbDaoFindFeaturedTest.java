/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: JdbcChannelFeedDbDaoFindFeaturedTest.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.error.FeaturedCommunicationNotFoundException;

/**
 * Tests {link JdbcCommunicationDbDao} related to findFeed.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcChannelFeedDbDaoFindFeaturedTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcChannelFeedDbDao uut;

  /**
   * Testing Exceptions
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithNoCommunications() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Exceptions
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithInvalidChannel() {
    // Set up
    long channelId = 99999999;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Exceptions
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithNullChannelId() {
    // Set up
    Long channelId = null;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Standard Select
   */
  @Test
  public void findFeaturedWithOneCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    Date expectedResult = communication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Feature Rule - Closest to Now
   */
  @Test
  public void findFeaturedWithTwoCommunicationsAllFuture() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, 2));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, 1));
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    Date expectedResult = communication2.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Feature Rule - Closest to Now
   */
  @Test
  public void findFeaturedWithTwoCommunicationsAllPast() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, -2));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, -1));
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    Date expectedResult = communication2.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Feature Rule - Closest to Now
   */
  @Test
  public void findFeaturedWithTwoCommunicationsAllPastAndFuture() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, 2));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, -1));
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    Date expectedResult = communication2.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Multiple Communications as the same time
   */
  @Test
  public void findFeaturedWithTwoCommunicationsSameTime() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(now);
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(now);
    addTemporaryCommunication(channelId, communication2);

    // Expectations
    Date expectedResult = communication2.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Multiple Communications as the same time
   */
  @Test
  public void findFeaturedWithMultipleCommunicationsSomeWithSameTime() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Date now = new Date();

    Communication communication1 = buildCommunication(channelId);
    communication1.setScheduledDateTime(DateUtils.addDays(now, 2));
    addTemporaryCommunication(channelId, communication1);

    Communication communication2 = buildCommunication(channelId);
    communication2.setScheduledDateTime(DateUtils.addDays(now, 2));
    addTemporaryCommunication(channelId, communication2);

    Communication communication3 = buildCommunication(channelId);
    communication3.setScheduledDateTime(DateUtils.addDays(now, 1));
    addTemporaryCommunication(channelId, communication3);

    // Expectations
    Date expectedResult = communication3.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Filtering
   */
  @Test
  public void findFeaturedWithOneCommunicationInactiveChannel() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = false;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    Date expectedResult = communication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationDeletedCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setStatus(Communication.Status.DELETED);

    addTemporaryCommunication(channelId, communication);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationCancelledCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setStatus(Communication.Status.CANCELLED);

    addTemporaryCommunication(channelId, communication);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeaturedDateTime(Long, ChannelFeedSearchCriteria)} will select the
   * featured date from all communications if the status filter is not set in the criteria, except if they are cancelled
   * or deleted.
   */
  @Test
  public void findFeaturedWithoutSpecifyingCommunicationStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date now = new Date();

    Communication cancelledCommunication = buildCommunication(channelId);
    cancelledCommunication.setStatus(Communication.Status.CANCELLED);
    cancelledCommunication.setScheduledDateTime(DateUtils.addDays(now, 1));

    Communication deletedCommunication = buildCommunication(channelId);
    deletedCommunication.setStatus(Communication.Status.DELETED);
    deletedCommunication.setScheduledDateTime(DateUtils.addDays(now, 2));

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);
    upcomingCommunication.setScheduledDateTime(DateUtils.addDays(now, 3));

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);
    liveCommunication.setScheduledDateTime(DateUtils.addDays(now, 4));

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);
    recordedCommunication.setScheduledDateTime(DateUtils.addDays(now, 5));

    addTemporaryCommunication(channelId, upcomingCommunication);
    addTemporaryCommunication(channelId, liveCommunication);
    addTemporaryCommunication(channelId, recordedCommunication);
    addTemporaryCommunication(channelId, cancelledCommunication);
    addTemporaryCommunication(channelId, deletedCommunication);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    // Expectations
    Date expectedResult = upcomingCommunication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeaturedDateTime(Long, ChannelFeedSearchCriteria)} will only select the
   * featured date from upcoming communications if the status filter in the criteria is set to only include upcoming
   * communications.
   */
  @Test
  public void findFeaturedWithUpcomingStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);

    addTemporaryCommunication(channelId, upcomingCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.UPCOMING);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    Date expectedResult = upcomingCommunication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeaturedDateTime(Long, ChannelFeedSearchCriteria)} will only select the
   * featured date from live communications if the status filter in the criteria is set to only include live
   * communications.
   */
  @Test
  public void findFeaturedWithLiveStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);

    addTemporaryCommunication(channelId, liveCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.LIVE);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    Date expectedResult = liveCommunication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeaturedDateTime(Long, ChannelFeedSearchCriteria)} will only select the
   * featured date from recorded communications if the status filter in the criteria is set to only include recorded
   * communications.
   */
  @Test
  public void findFeaturedWithRecordedStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);

    addTemporaryCommunication(channelId, recordedCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.RECORDED);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    Date expectedResult = recordedCommunication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findFeaturedDateTime(Long, ChannelFeedSearchCriteria)} will only select the
   * featured date from upcoming or live communications if the status filter in the criteria is set to only include
   * upcoming or live communications.
   */
  @Test
  public void findFeaturedWithMultipleStatusesFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Date now = new Date();

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);
    upcomingCommunication.setScheduledDateTime(DateUtils.addDays(now, 3));

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);
    liveCommunication.setScheduledDateTime(DateUtils.addDays(now, 2));

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);
    recordedCommunication.setScheduledDateTime(DateUtils.addDays(now, 1));

    addTemporaryCommunication(channelId, upcomingCommunication);
    addTemporaryCommunication(channelId, liveCommunication);
    addTemporaryCommunication(channelId, recordedCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(
        CommunicationStatusFilter.UPCOMING, CommunicationStatusFilter.LIVE);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    Date expectedResult = liveCommunication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationUnpublishedCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setPublishStatus(Communication.PublishStatus.UNPUBLISHED);

    addTemporaryCommunication(channelId, communication);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Filtering
   */
  @Test
  public void findFeaturedWithOneCommunicationPublicCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.PUBLIC);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    Date expectedResult = communication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationPrivateCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.PRIVATE);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    Date expectedResult = communication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Filtering
   */
  @Test
  public void findFeaturedWithOneCommunicationFeedOnlyCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.FEEDONLY);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    Date expectedResult = communication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void findFeaturedWithOneCommunicationSyndicatedOut() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.OUT);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    Date expectedResult = communication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Syndication Filtering
   */
  @Test
  public void findFeaturedWithOneCommunicationSyndicatedInApprovedByBoth() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    Date expectedResult = communication.getScheduledDateTime();

    // Do Test
    Date result = uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
    assertDateSecondsEqual("incorrect communication featured", expectedResult, result);
  }

  /**
   * Testing Syndication Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationSyndicatedInNotApprovedByMaster() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    addTemporaryCommunication(channelId, communication);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Syndication Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationSyndicatedInNotApprovedByConsumer() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Syndication Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationSyndicatedInNotApprovedByBoth() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    addTemporaryCommunication(channelId, communication);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Syndication Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationSyndicatedInPendingBoth() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    addTemporaryCommunication(channelId, communication);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Syndication Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationSyndicatedInPendingMaster() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    addTemporaryCommunication(channelId, communication);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Testing Syndication Filtering
   */
  @Test(expected = FeaturedCommunicationNotFoundException.class)
  public void findFeaturedWithOneCommunicationSyndicatedInPendingConsumer() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    // Expectations

    // Do Test
    uut.findFeaturedDateTime(channelId, criteria);

    // Assertions
  }

  /**
   * Helper method to assert dates are equal to the same second
   * 
   * @param message AnAssertion message that should be logged on failure
   * @param date1 Date to compare
   * @param date2 Other Date to compare
   */
  private void assertDateSecondsEqual(final String message, final Date date1, final Date date2) {

    double time1 = Math.floor(date1.getTime() / 1000);
    double time2 = Math.floor(date2.getTime() / 1000);

    assertEquals(message, time1, time2, 0.1);

  }

  private ChannelFeedSearchCriteria buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(
      List<CommunicationStatusFilter> statusFilter) {

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(statusFilter);

    return criteria;
  }
}
