/**
 * *****************************************************************************
 * * Copyright BrightTALK Ltd, 2009-2010.
 * * All Rights Reserved.
 * * Id: JdbcCategoryDbDaoTest.java
 * *****************************************************************************
 * @package com.brighttalk.service.channel.integration.database
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;

/**
 * Test Case for {@link JdbcCategoryDbDao}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCategoryDbDaoTest extends AbstractJdbcDbDaoTest {

  private final static Long CHANNEL1_ID = new Long(1);

  private final static Long CHANNEL2_ID = new Long(2);

  private final static Long CHANNEL3_ID = new Long(3);

  private final static Long COMMUNICATION1_ID = new Long(1);

  private final static Long COMMUNICATION2_ID = new Long(2);

  private SimpleJdbcInsert simpleJdbcInsertCategory;

  private SimpleJdbcInsert simpleJdbcInsertCommunicationCategory;

  private final List<Long> temporaryCategoryIds = new ArrayList<Long>();

  private final List<Long> temporaryCommunicationCategoryIds = new ArrayList<Long>();

  private final List<Communication> communications = new ArrayList<Communication>();

  @Autowired
  private JdbcCategoryDbDao uut;

  @Autowired
  @Override
  public void setDataSource(DataSource dataSource) {
    super.setDataSource(dataSource);

    this.simpleJdbcInsertCategory = new SimpleJdbcInsert(dataSource);
    this.simpleJdbcInsertCategory.withTableName("category");
    this.simpleJdbcInsertCategory.usingGeneratedKeyColumns("id");
    this.simpleJdbcInsertCategory.usingColumns("channel_id", "category", "is_active", "created", "last_updated");

    this.simpleJdbcInsertCommunicationCategory = new SimpleJdbcInsert(dataSource);
    this.simpleJdbcInsertCommunicationCategory.withTableName("communication_category");
    this.simpleJdbcInsertCommunicationCategory.usingGeneratedKeyColumns("id");
    this.simpleJdbcInsertCommunicationCategory.usingColumns("channel_id", "communication_id", "category_id",
        "is_active", "created", "last_updated");
  }

  /**
   * Set up this test case. 
   * Create some categories for two communications in two channels. 
   */
  @Before
  public void setUp() {

    communications.add(new Communication(COMMUNICATION1_ID));
    communications.add(new Communication(COMMUNICATION2_ID));

    Long categoryId1 = insertActiveCategory(CHANNEL1_ID, "ch1 - test category 1");
    Long categoryId2 = insertActiveCategory(CHANNEL1_ID, "ch1 - test category 2");
    Long categoryId3 = insertInactiveCategory(CHANNEL1_ID, "ch1 - test category 3");
    Long categoryId4 = insertActiveCategory(CHANNEL1_ID, "ch1 - test category 4 - inactive");

    Long categoryId5 = insertActiveCategory(CHANNEL2_ID, "ch2 - test category 5");
    Long categoryId6 = insertActiveCategory(CHANNEL2_ID, "ch2 - test category 6");
    Long categoryId7 = insertInactiveCategory(CHANNEL2_ID, "ch2 - test category 7");
    Long categoryId8 = insertActiveCategory(CHANNEL2_ID, "ch2 - test category 8 - inactive");

    insertActiveCommunicationCategory(CHANNEL1_ID, COMMUNICATION1_ID, categoryId1);
    insertActiveCommunicationCategory(CHANNEL1_ID, COMMUNICATION1_ID, categoryId2);
    insertActiveCommunicationCategory(CHANNEL1_ID, COMMUNICATION1_ID, categoryId3);
    insertInactiveCommunicationCategory(CHANNEL1_ID, COMMUNICATION1_ID, categoryId4);

    insertActiveCommunicationCategory(CHANNEL2_ID, COMMUNICATION2_ID, categoryId5);
    insertActiveCommunicationCategory(CHANNEL2_ID, COMMUNICATION2_ID, categoryId6);
    insertActiveCommunicationCategory(CHANNEL2_ID, COMMUNICATION2_ID, categoryId7);
    insertInactiveCommunicationCategory(CHANNEL2_ID, COMMUNICATION2_ID, categoryId8);

  }

  /**
   * Tear down this test case. 
   * Delete categories we created in the DB.
   */
  @Override
  @After
  public void tearDown() {

    super.tearDown();

    for (Long categoryId : temporaryCategoryIds) {
      this.jdbcTemplate.update("DELETE FROM category WHERE id = ?", categoryId);
    }

    for (Long communicationCategoryId : temporaryCommunicationCategoryIds) {
      this.jdbcTemplate.update("DELETE FROM communication_category WHERE id = ?", communicationCategoryId);
    }
  }

  /**
   * Just sanity check we have no errors in the SQL in the DAO. 
   */
  @Test
  public void testGetSomeCategoriesSanityCheck() {

    uut.loadCategories(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);

    List<CommunicationCategory> categories = communication1.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 2, categories.size());

    Communication communication2 = communications.get(1);

    categories = communication2.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());
  }

  /**
   * Just check we can read some categories for channel 1 and none for channel 2
   */
  @Test
  public void testGetSomeCategoriesChannel1() {

    uut.loadCategories(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);

    List<CommunicationCategory> categories = communication1.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 2, categories.size());

    Communication communication2 = communications.get(1);

    categories = communication2.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());
  }

  /**
   * Just check we can read some categories for channel 2 and none for channel 1
   */
  @Test
  public void testGetSomeCategoriesChannel2() {

    uut.loadCategories(CHANNEL2_ID, communications);

    Communication communication1 = communications.get(0);

    List<CommunicationCategory> categories = communication1.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());

    Communication communication2 = communications.get(1);

    categories = communication2.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 2, categories.size());
  }

  /**
   * Test if a channel category is inactive, then no communication categories for that channel
   * category are returned. 
   */
  @Test
  public void testNoInactiveChannelCategoriesReturned() {

    uut.loadCategories(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);

    List<CommunicationCategory> categories = communication1.getCategories();

    for (CommunicationCategory category : categories) {
      assertTrue("got an inactive category", category.isActive());
    }
  }

  /**
   * Test if a communication category is inactive, then it isn't returned. 
   */
  @Test
  public void testNoInactiveCommunicationCategoriesReturned() {

    uut.loadCategories(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);

    List<CommunicationCategory> categories = communication1.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 2, categories.size());

    for (CommunicationCategory category : categories) {
      assertTrue("got an inactive category", category.isActive());
    }

  }

  /**
   * Detailed test to check every category returned as the correct attributes set
   */
  @Test
  public void testCommunicationCategoriesDetailsAreCorrect() {

    Long categoryId1 = insertActiveCategory(CHANNEL1_ID, "ch1 - test category 1.1");
    Long categoryId2 = insertActiveCategory(CHANNEL1_ID, "ch1 - test category 2.1");
    insertActiveCommunicationCategory(CHANNEL1_ID, COMMUNICATION1_ID, categoryId1);
    insertActiveCommunicationCategory(CHANNEL1_ID, COMMUNICATION1_ID, categoryId2);

    uut.loadCategories(CHANNEL1_ID, communications);

    Communication communication1 = communications.get(0);
    List<CommunicationCategory> categories = communication1.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 4, categories.size());

    // check a couple of categories have the correct info
    for (CommunicationCategory category : categories) {

      if (category.getId().equals(categoryId1)) {
        assertEquals("incorrect description", "ch1 - test category 1.1", category.getDescription());
      } else if (category.getId().equals(categoryId2)) {
        assertEquals("incorrect description", "ch1 - test category 2.1", category.getDescription());
      }

      assertEquals("incorrect channel id", CHANNEL1_ID, category.getChannelId());
      assertEquals("incorrect communication id", COMMUNICATION1_ID, category.getCommunicationId());
      assertTrue("category should be active", category.isActive());
    }
  }

  /**
   * Test we get no categories returned for a channel with no categories
   */
  @Test
  public void testChannelWithNoCategories() {
    uut.loadCategories(CHANNEL3_ID, communications);

    Communication communication1 = communications.get(0);
    List<CommunicationCategory> categories = communication1.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());

    Communication communication2 = communications.get(0);
    categories = communication2.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());
  }

  @Test
  public void testNullChannelIdReturnsNoCategories() {
    uut.loadCategories(null, communications);

    Communication communication1 = communications.get(0);
    List<CommunicationCategory> categories = communication1.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());

    Communication communication2 = communications.get(0);
    categories = communication2.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());
  }

  @Test
  public void testEmptyCommunicationsReturnsNoCategories() {
    uut.loadCategories(CHANNEL1_ID, new ArrayList<Communication>());

    Communication communication1 = communications.get(0);
    List<CommunicationCategory> categories = communication1.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());

    Communication communication2 = communications.get(0);
    categories = communication2.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());
  }

  @Test
  public void loadCategoriesWhenCommunicationIdIsNull() {

    Communication communication = new Communication();

    uut.loadCategories(CHANNEL1_ID, communication);

    List<CommunicationCategory> categories = communication.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());

  }

  @Test
  public void loadCategoriesWhenCommunicationAndChannelExistAndCategoryIsActive() {

    // setup
    Long communicationId = 999L;
    Long channelId = 9999L;
    String categoryDescription = "category  Test Title";

    Long categoryId = insertActiveCategory(channelId, categoryDescription);
    insertActiveCommunicationCategory(channelId, communicationId, categoryId);

    Communication result = new Communication(communicationId);

    uut.loadCategories(channelId, result);

    List<CommunicationCategory> categories = result.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 1, categories.size());
    assertEquals(categoryId, categories.get(0).getId());
    assertEquals(channelId, categories.get(0).getChannelId());
    assertEquals(communicationId, categories.get(0).getCommunicationId());
    assertEquals(categoryDescription, categories.get(0).getDescription());

  }

  @Test
  public void loadCategoriesWhenCommunicationAndChannelExistAndTwoCategoryAreActive() {

    // setup
    Long communicationId = 999L;
    Long channelId = 9999L;
    String categoryDescription = "category  Test Title";

    Long categoryId = insertActiveCategory(channelId, categoryDescription);
    insertActiveCommunicationCategory(channelId, communicationId, categoryId);

    String categoryDescription2 = "category  Test Title2";

    Long categoryId2 = insertActiveCategory(channelId, categoryDescription2);
    insertActiveCommunicationCategory(channelId, communicationId, categoryId2);

    Communication result = new Communication(communicationId);

    uut.loadCategories(channelId, result);

    List<CommunicationCategory> categories = result.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 2, categories.size());
    assertEquals(categoryId, categories.get(0).getId());
    assertEquals(categoryId2, categories.get(1).getId());
  }

  @Test
  public void loadCategoriesWhenCommunicationAndChannelExistAndChannelCategoryIsActiveAndCommunicationCategoryIsInactive() {

    // setup
    Long communicationId = 999L;
    Long channelId = 9999L;
    String categoryDescription = "category  Test Title";

    Long categoryId = insertActiveCategory(channelId, categoryDescription);
    insertInactiveCommunicationCategory(channelId, communicationId, categoryId);

    Communication result = new Communication(communicationId);

    uut.loadCategories(channelId, result);

    List<CommunicationCategory> categories = result.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());
  }

  @Test
  public void loadCategoriesWhenCommunicationAndChannelExistAndChannelCategoryIsinactiveAndCommunicationCategoryIsActive() {

    // setup
    Long communicationId = 999L;
    Long channelId = 9999L;
    String categoryDescription = "category  Test Title";

    Long categoryId = insertInactiveCategory(channelId, categoryDescription);
    insertActiveCommunicationCategory(channelId, communicationId, categoryId);

    Communication result = new Communication(communicationId);

    uut.loadCategories(channelId, result);

    List<CommunicationCategory> categories = result.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());
  }

  @Test
  public void loadCategoriesWhenCommunicationAndChannelExistAndChannelCategoryIsActiveAndCommunicationCategoryDoesNotExist() {

    // setup
    Long communicationId = 999L;
    Long channelId = 9999L;
    String categoryDescription = "category  Test Title";

    insertInactiveCategory(channelId, categoryDescription);

    Communication result = new Communication(communicationId);

    uut.loadCategories(channelId, result);

    List<CommunicationCategory> categories = result.getCategories();
    assertNotNull("expected a category list", categories);
    assertEquals("incorrect number of categories", 0, categories.size());
  }

  /**
   * Test load channel categories with one category.
   */
  @Test
  public void testLoadCategoriesWithOneCategory() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    addCategories(channelId, CATEGORY_NAME);

    Channel channel = (new Channel(channelId));
    // do test
    uut.loadCategories(channel);

    // assertions
    Assert.assertEquals(1, channel.getCategories().size());
    Assert.assertEquals(CATEGORY_NAME, channel.getCategories().get(0).getName());

  }

  /**
   * Test load channel categories with none category.
   */
  @Test
  public void testLoadCategoriesWithNoneCategory() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();

    Channel channel = (new Channel(channelId));
    // do test
    uut.loadCategories(channel);

    // assertions
    Assert.assertEquals(0, channel.getCategories().size());
  }

  /**
   * Test load channel categories with one category.
   */
  @Test
  public void testLoadCategoriesMultipleChannelsWithOneCategory() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();
    addCategories(channelId, CATEGORY_NAME);

    Channel channel = (new Channel(channelId));
    List<Channel> channels = Arrays.asList(channel);

    // do test
    uut.loadCategories(channels);

    // assertions
    Assert.assertEquals(1, channel.getCategories().size());
    Assert.assertEquals(CATEGORY_NAME, channel.getCategories().get(0).getName());

  }

  /**
   * Test load channel categories with none category.
   */
  @Test
  public void testLoadCategoriesMultipleChannelsWithNoneCategory() {
    // setup
    Long channelId = addTemporaryChannelWithTypes();

    Channel channel = (new Channel(channelId));
    List<Channel> channels = Arrays.asList(channel);

    // Expectations
    boolean expectedResult = false;

    // do test
    uut.loadCategories(channels);

    // assertions
    Assert.assertEquals(expectedResult, channel.hasCategories());
  }

  private Long insertActiveCategory(Long channelId, String categoryDescription) {
    return insertCategory(channelId, categoryDescription, true);
  }

  private Long insertInactiveCategory(Long channelId, String categoryDescription) {
    return insertCategory(channelId, categoryDescription, false);
  }

  private Long insertActiveCommunicationCategory(Long channelId, Long communicationId, Long categoryId) {
    return insertCommunicationCategory(channelId, communicationId, categoryId, true);
  }

  private Long insertInactiveCommunicationCategory(Long channelId, Long communicationId, Long categoryId) {
    return insertCommunicationCategory(channelId, communicationId, categoryId, false);
  }

  private Long insertCategory(Long channelId, String categoryDescription, boolean isActive) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("category", categoryDescription);
    params.addValue("is_active", isActive);

    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    Number primaryKey = simpleJdbcInsertCategory.executeAndReturnKey(params);

    Long categoryId = primaryKey.longValue();
    temporaryCategoryIds.add(categoryId);
    return categoryId;
  }

  private Long insertCommunicationCategory(Long channelId, Long communicationId, Long categoryId, boolean isActive) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("communication_id", communicationId);
    params.addValue("category_id", categoryId);
    params.addValue("is_active", isActive);

    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    Number primaryKey = simpleJdbcInsertCommunicationCategory.executeAndReturnKey(params);

    Long communicationCategoryId = primaryKey.longValue();
    temporaryCommunicationCategoryIds.add(communicationCategoryId);
    return communicationCategoryId;
  }
}
