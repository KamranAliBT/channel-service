/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: AuditRecordDtoTest.java 77652 2014-05-09 14:59:25Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit.dto;

import static org.junit.Assert.assertEquals;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;

/**
 * Tests for {@link com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto AuditRecordDto}.
 */
public class AuditRecordDtoTest {

  private static final Long USER_ID = 1L;

  private static final Long CHANNEL_ID = 2L;

  private static final Long COMMUNICATION_ID = 3L;

  private static final String ACTION = "create";

  private static final String ENTITY = "webcast";

  private static final String ACTION_DESCRIPTION = "Creating my first webcast";

  private AuditRecordDto uut;

  /**
   * Tests the getter and setters of the audit record.
   */
  @Test
  public void getterAndSetters() {

    // Set up
    uut = buildAuditRecordDto();

    // Expectation

    // Do test

    // Test the getters return the data that was set
    assertEquals(USER_ID, uut.getUserId());
    assertEquals(CHANNEL_ID, uut.getChannelId());
    assertEquals(COMMUNICATION_ID, uut.getCommunicationId());
    assertEquals(ACTION, uut.getAction());
    assertEquals(ENTITY, uut.getEntity());
    assertEquals(ACTION_DESCRIPTION, uut.getActionDescription());
  }

  /**
   * Tests the {@link com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto#toString()
   * AuditRecordDto.toString()} method.
   */
  @Test
  public void auditRecordToString() {

    // Set up
    uut = buildAuditRecordDto();

    // Expectations
    String expectedAuditRecordRepresentation = buildExpectedAuditRecordDtoRepresentation(uut);

    // Do test
    String auditRecordRepresentation = uut.toString();

    // Assertions
    assertEquals(auditRecordRepresentation, expectedAuditRecordRepresentation);
  }

  private AuditRecordDto buildAuditRecordDto() {

    AuditRecordDto auditRecord = new AuditRecordDto();

    auditRecord.setUserId(USER_ID);
    auditRecord.setChannelId(CHANNEL_ID);
    auditRecord.setCommunicationId(COMMUNICATION_ID);
    auditRecord.setAction(ACTION);
    auditRecord.setEntity(ENTITY);
    auditRecord.setActionDescription(ACTION_DESCRIPTION);

    return auditRecord;
  }

  private String buildExpectedAuditRecordDtoRepresentation(final AuditRecordDto auditRecordDto) {

    ToStringBuilder expectedToString = new ToStringBuilder(auditRecordDto);

    expectedToString.append("userId", USER_ID);
    expectedToString.append("channelId", CHANNEL_ID);
    expectedToString.append("communicationId", COMMUNICATION_ID);
    expectedToString.append("action", ACTION);
    expectedToString.append("entity", ENTITY);
    expectedToString.append("actionDescription", ACTION_DESCRIPTION);

    return expectedToString.toString();
  }
}
