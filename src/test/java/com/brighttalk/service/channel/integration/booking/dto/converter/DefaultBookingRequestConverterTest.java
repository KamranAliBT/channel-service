/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultBookingRequestConverterTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.integration.booking.dto.BookingRequestDto;

public class DefaultBookingRequestConverterTest {
  
  private DefaultBookingRequestConverter uut;
  
  @Before
  public void setUp() {
    
    uut = new DefaultBookingRequestConverter();
  }
  
  @Test
  public void convert() {
    
    final Long expectedCommunicationId1 = 921L;
    final Long expectedCommunicationId2 = 666L;
    
    List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add( expectedCommunicationId1 );
    communicationIds.add( expectedCommunicationId2 );
    
    //do test
    BookingRequestDto result = uut.convert( communicationIds );
    
    assertNotNull( result );
    
    assertEquals( 2, result.getCommunications().size() );
    assertEquals( expectedCommunicationId1, result.getCommunications().get(0).getId() );
    assertEquals( expectedCommunicationId2, result.getCommunications().get(1).getId() );
  }

}
