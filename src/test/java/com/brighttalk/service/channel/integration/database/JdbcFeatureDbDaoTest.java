/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: JdbcFeatureDbDaoTest.java 91279 2015-03-09 11:35:52Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcFeatureDbDaoTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcFeatureDbDao uut;

  @Test
  public void findByChannelIdWhileChannelNotFound() {
    // Set up
    String featureName = "ads_displayed";
    boolean isChannelTypeDefault = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = 9999L;

    // Do test
    List<Feature> features = uut.findByChannelId(channelId);

    // Assertions
    assertTrue(features.isEmpty());
  }

  @Test
  public void findByChannelIdWithEmptyFeatures() {
    // Set up
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);

    // Do test
    List<Feature> features = uut.findByChannelId(channelId);

    // Assertions
    assertTrue(features.isEmpty());
  }

  @Test
  public void findByChannelIdWithInvalidFeature() {
    // Set up
    String featureName = "invalid";
    String featureValue = null;

    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName, featureValue);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);

    // Do test
    List<Feature> features = uut.findByChannelId(channelId);

    // Assertions
    Feature feature = features.get(0);

    assertEquals(channelId, feature.getChannelId());
    assertEquals(Feature.Type.UNKNOWN, feature.getName());
    assertTrue(feature.isEnabled());
    assertNull(feature.getValue());
  }

  @Test
  public void findByChannelIdWithoutFeatureOverrideAndValue() {
    // Set up
    String featureName = "ads_displayed";
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);

    // Do test
    List<Feature> features = uut.findByChannelId(channelId);

    // Assertions
    Feature feature = features.get(0);

    assertEquals(channelId, feature.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
    assertTrue(feature.isEnabled());
    assertNull(feature.getValue());
  }

  @Test
  public void findByChannelIdWithoutFeatureOverride() {
    // Set up
    String featureName = "ads_displayed";
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName, FEATURE_VALUE);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);

    // Do test
    List<Feature> features = uut.findByChannelId(channelId);

    // Assertions
    Feature feature = features.get(0);

    assertEquals(channelId, feature.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
    assertTrue(feature.isEnabled());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
  }

  @Test
  public void findByChannelIdWithFeatureOverrideAndValue() {
    // Set up
    String featureName = "ads_displayed";
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;
    boolean isOverridedEnabled = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);
    addChannelFeatures(channelId, featureName, isOverridedEnabled);

    // Do test
    List<Feature> features = uut.findByChannelId(channelId);

    // Assertions
    Feature feature = features.get(0);

    assertEquals(channelId, feature.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
    assertTrue(feature.isEnabled());
    assertNull(feature.getValue());
  }

  @Test
  public void findByChannelIdWithFeatureOverride() {
    // Set up
    String featureName = "ads_displayed";
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;
    boolean isOverridedEnabled = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);
    addChannelFeatures(channelId, featureName, isOverridedEnabled, FEATURE_VALUE);

    // Do test
    List<Feature> features = uut.findByChannelId(channelId);

    // Assertions
    Feature feature = features.get(0);

    assertEquals(channelId, feature.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
    assertTrue(feature.isEnabled());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
  }

  @Test
  public void loadFeaturesWithEmptyFeatures() {
    // Set up
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);

    Channel channel = new Channel(channelId);
    channel.setType(new ChannelType(defaultChannelTypeId));

    // Do test
    uut.loadFeatures(channel);

    // Assertions
    assertTrue(channel.getFeatures().isEmpty());
  }

  @Test
  public void loadFeaturesWithoutFeatureOverrideAndValue() {
    // Set up
    String featureName = "ads_displayed";
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);

    Channel channel = new Channel(channelId);
    channel.setType(new ChannelType(defaultChannelTypeId));

    // Do test
    uut.loadFeatures(channel);

    // Assertions
    Feature feature = channel.getFeatures().get(0);

    assertEquals(channelId, feature.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
    assertTrue(feature.isEnabled());
    assertNull(feature.getValue());
  }

  @Test
  public void loadFeaturesWithoutFeatureOverride() {
    // Set up
    String featureName = "ads_displayed";
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName, FEATURE_VALUE);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);

    Channel channel = new Channel(channelId);
    channel.setType(new ChannelType(defaultChannelTypeId));

    // Do test
    uut.loadFeatures(channel);

    // Assertions
    Feature feature = channel.getFeatures().get(0);

    assertEquals(channelId, feature.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
    assertTrue(feature.isEnabled());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
  }

  @Test
  public void loadFeaturesWithFeatureOverrideAndValue() {
    // Set up
    String featureName = "ads_displayed";
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;
    boolean isOverridedEnabled = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);
    addChannelFeatures(channelId, featureName, isOverridedEnabled);

    Channel channel = new Channel(channelId);
    channel.setType(new ChannelType(defaultChannelTypeId));

    // Do test
    uut.loadFeatures(channel);

    // Assertions
    Feature feature = channel.getFeatures().get(0);

    assertEquals(channelId, feature.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
    assertTrue(feature.isEnabled());
    assertNull(feature.getValue());
  }

  @Test
  public void loadFeaturesWithFeatureOverride() {
    // Set up
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;
    boolean isOverridedEnabled = true;
    String featureName = "ads_displayed";

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);
    addChannelFeatures(channelId, featureName, isOverridedEnabled, FEATURE_VALUE);

    Channel channel = new Channel(channelId);
    channel.setType(new ChannelType(defaultChannelTypeId));

    // Do test
    uut.loadFeatures(channel);

    // Assertions
    Feature feature = channel.getFeatures().get(0);

    assertEquals(channelId, feature.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, feature.getName());
    assertTrue(feature.isEnabled());
  }

  @Test
  public void updateFeaturesWhileNoneProvided() {
    // Set up
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;
    String featureName = "ads_displayed";

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);
    Channel channel = new Channel(channelId);

    List<Feature> features = new ArrayList<>();

    // Do test
    uut.update(channel, features);

    // Assertions
    Feature result = uut.findByChannelId(channelId).iterator().next();

    assertEquals(channelId, result.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, result.getName());
    assertNull(result.getValue());
    assertTrue(result.isEnabled());
  }

  @Test
  public void updateFeaturesWhileNotOverrided() {
    // Set up
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;
    boolean isOverridedEnabled = true;
    String featureName = "ads_displayed";

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);
    Channel channel = new Channel(channelId);

    List<Feature> features = new ArrayList<>();

    Feature feature = new Feature(Feature.Type.ADS_DISPLAYED);
    feature.setIsEnabled(isOverridedEnabled);
    feature.setValue(FEATURE_VALUE);
    features.add(feature);

    // Do test
    uut.update(channel, features);

    // Assertions
    Feature result = uut.findByChannelId(channelId).iterator().next();

    assertEquals(channelId, result.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, result.getName());
    assertEquals(FEATURE_VALUE, result.getValue());
    assertTrue(result.isEnabled());
  }

  @Test
  public void updateFeaturesWhileOverrided() {
    // Set up
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;
    boolean isOverridedEnabled = true;
    String featureName = "ads_displayed";
    String featureValue = "updated feature value";

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);
    Channel channel = new Channel(channelId);
    addChannelFeatures(channelId, featureName, isOverridedEnabled, FEATURE_VALUE);

    List<Feature> features = new ArrayList<>();

    Feature feature = new Feature(Feature.Type.ADS_DISPLAYED);
    feature.setIsEnabled(isOverridedEnabled);
    feature.setValue(featureValue);
    features.add(feature);

    // Do test
    uut.update(channel, features);

    // Assertions
    Feature result = uut.findByChannelId(channelId).iterator().next();

    assertEquals(channelId, result.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, result.getName());
    assertEquals(featureValue, result.getValue());
    assertTrue(result.isEnabled());
  }

  @Test
  public void updateFeaturesWhileOverridedAndDisabled() {
    // Set up
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;
    boolean isOverridedEnabled = true;
    String featureName = "ads_displayed";

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, featureName);

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);
    Channel channel = new Channel(channelId);
    addChannelFeatures(channelId, featureName, isOverridedEnabled, FEATURE_VALUE);

    List<Feature> features = new ArrayList<>();

    Feature feature = new Feature(Feature.Type.ADS_DISPLAYED);
    feature.setIsEnabled(false);
    features.add(feature);

    // Do test
    uut.update(channel, features);

    // Assertions
    Feature result = uut.findByChannelId(channelId).iterator().next();

    assertEquals(channelId, result.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, result.getName());
    assertNull(result.getValue());
    assertFalse(result.isEnabled());
  }

  @Test
  public void updateMultipleFeatures() {
    // Set up
    boolean isChannelActive = true;
    boolean isChannelTypeDefault = true;

    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, "ads_displayed");
    addFeatures(defaultChannelTypeId, "allowed_realms");
    addFeatures(defaultChannelTypeId, "attendee_networking");

    Long channelId = addTemporaryChannel(defaultChannelTypeId, isChannelActive);
    Channel channel = new Channel(channelId);

    List<Feature> features = new ArrayList<>();

    Feature feature = new Feature(Feature.Type.ADS_DISPLAYED);
    feature.setIsEnabled(false);
    features.add(feature);

    feature = new Feature(Feature.Type.ATTENDEE_NETWORKING);
    feature.setChannelId(channelId);
    feature.setIsEnabled(false);
    features.add(feature);

    // Do test
    uut.update(channel, features);

    // Assertions
    Feature result = uut.findByChannelId(channelId).iterator().next();

    assertEquals(channelId, result.getChannelId());
    assertEquals(Feature.Type.ADS_DISPLAYED, result.getName());
    assertNull(result.getValue());
    assertFalse(result.isEnabled());
  }

  /**
   * Tests {@link JdbcFeatureDbDao#findDefaultFeatureByChannelTypeId(Long)} in the success case of finding channel
   * default features by channel type id.
   */
  @Test
  public void findDefaultFeaturesByChannelTypeId() {
    // Setup
    boolean isChannelTypeDefault = true;
    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, Type.ADS_DISPLAYED.getDbName());
    addFeatures(defaultChannelTypeId, Type.EMAIL_CHANNEL_SUBSCRIPTION.getDbName());
    addFeatures(defaultChannelTypeId, Type.EMAIL_COMM_REGISTRATION_CONFIRMATION.getDbName());

    // Do Test
    List<Feature> expectedDefaultFeatures = uut.findDefaultFeatureByChannelTypeId(defaultChannelTypeId);

    // Assertions
    assertNotNull(expectedDefaultFeatures);
    assertEquals(3, expectedDefaultFeatures.size());
    assertEquals(Type.ADS_DISPLAYED, expectedDefaultFeatures.get(0).getName());
    assertTrue(expectedDefaultFeatures.get(0).isEnabled());
    assertEquals(Type.EMAIL_CHANNEL_SUBSCRIPTION, expectedDefaultFeatures.get(1).getName());
    assertTrue(expectedDefaultFeatures.get(1).isEnabled());
    assertEquals(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION, expectedDefaultFeatures.get(2).getName());
    assertTrue(expectedDefaultFeatures.get(2).isEnabled());
  }

  /**
   * Tests {@link JdbcFeatureDbDao#deleteFeatureOverridesByNames(Long, List)} in the success case of deleting specific
   * channel features override values by features names.
   * <p>
   * Setup - This tests creates 2 channel features with default enabled values, then override these features and disable
   * their enabled flag, then delete 1 of the channel features override value.
   * <p>
   * Expected Result - The channel feature with deleted override value should default back to the default enabled value.
   */
  @Test
  public void deleteFeaturesByNamesSuccess() {
    // Setup - Create 2 channel features enabled by default.
    boolean isChannelTypeDefault = true;
    Long defaultChannelTypeId = addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, isChannelTypeDefault);
    addFeatures(defaultChannelTypeId, Type.ADS_DISPLAYED.getDbName());
    addFeatures(defaultChannelTypeId, Type.EMAIL_COMM_REGISTRATION_CONFIRMATION.getDbName());
    Long channelId = addTemporaryChannel(defaultChannelTypeId, true);
    Channel channel = new Channel(channelId);
    // Override both channel features and set to disabled.
    List<Feature> features = new ArrayList<>();
    Feature feature = new Feature(Feature.Type.ADS_DISPLAYED);
    feature.setIsEnabled(false);
    features.add(feature);
    feature = new Feature(Feature.Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
    feature.setChannelId(channelId);
    feature.setIsEnabled(false);
    features.add(feature);
    // Upsert channel features overridden values.
    uut.update(channel, features);

    // Assert channel features override values are both disabled.
    List<Feature> overridedFeatures = uut.findByChannelId(channelId);
    assertNotNull(overridedFeatures);
    assertEquals(2, overridedFeatures.size());
    assertEquals(Type.ADS_DISPLAYED, overridedFeatures.get(0).getName());
    assertFalse(overridedFeatures.get(0).isEnabled());
    assertEquals(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION, overridedFeatures.get(1).getName());
    assertFalse(overridedFeatures.get(1).isEnabled());

    // Do Test - Delete 1 channel feature override value.
    uut.deleteFeatureOverridesByNames(channelId, Arrays.asList(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION));

    // Assertion - Check the retrieved channel features has only 1 feature with overridden values (disabled) and 1
    // feature has the default value (enabled).
    List<Feature> overridedFeatures2 = uut.findByChannelId(channelId);
    assertNotNull(overridedFeatures2);
    assertEquals(2, overridedFeatures2.size());
    // Assert this feature has still the overridden enabled value.
    assertEquals(Type.ADS_DISPLAYED, overridedFeatures2.get(0).getName());
    assertFalse(overridedFeatures2.get(0).isEnabled());
    // Assert this feature with deleted overridden feature, default back to the default feature enabled value.
    assertEquals(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION, overridedFeatures2.get(1).getName());
    assertTrue(overridedFeatures2.get(1).isEnabled());
  }

}