/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ChannelAuditRecordDtoBuilderTest.java 97400 2015-07-01 08:36:42Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit.dto.builder;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.ChannelAuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder.AuditAction;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder.AuditEntity;

/**
 * Tests for {@link ChannelAuditRecordDtoBuilder}.
 */
public class ChannelAuditRecordDtoBuilderTest {

  private static final Long USER_ID = 1L;
  private static final Long CHANNEL_ID = 2L;
  private static final Long COMMUNICATION_ID = 3L;
  private static final String ACTION_DESCRIPTION = "Test audit description 123...";

  private AuditRecordDtoBuilder uut;

  /**
   * Test setup.
   */
  @Before
  public void setup() {
    uut = new ChannelAuditRecordDtoBuilder();
  }

  /**
   * Tests {@link ChannelAuditRecordDtoBuilder#create(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Channel creation event.
   * <p>
   * Expected Result: {@link UnsupportedOperationException} as auditing 'create channel event' is not supported yet.
   */
  @Test(expected = UnsupportedOperationException.class)
  public void createChannelAuditRecordDto() {
    // Do test
    uut.create(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);
  }

  /**
   * Tests {@link ChannelAuditRecordDtoBuilder#cancel(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Channel cancellation event.
   * <p>
   * Expected Result: {@link UnsupportedOperationException} as auditing 'cancel channel event' is not supported yet.
   */
  @Test(expected = UnsupportedOperationException.class)
  public void cancelChannelAuditRecordDto() {
    // Do test
    uut.cancel(USER_ID, CHANNEL_ID, COMMUNICATION_ID, ACTION_DESCRIPTION);
  }

  /**
   * Tests {@link ChannelAuditRecordDtoBuilder#update(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Channel update event.
   */
  @Test
  public void updateChannelAuditRecordDto() {
    // Do test
    AuditRecordDto auditRecord = uut.update(USER_ID, CHANNEL_ID, COMMUNICATION_ID,
        ACTION_DESCRIPTION);

    // Assert audit record is as expected
    assertEquals(USER_ID, auditRecord.getUserId());
    assertEquals(CHANNEL_ID, auditRecord.getChannelId());
    assertEquals(COMMUNICATION_ID, auditRecord.getCommunicationId());
    assertEquals(AuditAction.UPDATE.getAction(), auditRecord.getAction());
    assertEquals(AuditEntity.CHANNEL.getEntity(), auditRecord.getEntity());
    assertEquals(ACTION_DESCRIPTION, auditRecord.getActionDescription());
  }

  /**
   * Tests {@link ChannelAuditRecordDtoBuilder#delete(Long, Long, Long, String)} in case of creating
   * {@link AuditRecordDto} for Channel deletion event.
   */
  @Test
  public void deleteChannelAuditRecordDto() {
    // Do test
    AuditRecordDto auditRecord = uut.delete(USER_ID, CHANNEL_ID, COMMUNICATION_ID,
        ACTION_DESCRIPTION);

    // Assert audit record is as expected
    assertEquals(USER_ID, auditRecord.getUserId());
    assertEquals(CHANNEL_ID, auditRecord.getChannelId());
    assertEquals(COMMUNICATION_ID, auditRecord.getCommunicationId());
    assertEquals(AuditAction.DELETE.getAction(), auditRecord.getAction());
    assertEquals(AuditEntity.CHANNEL.getEntity(), auditRecord.getEntity());
    assertEquals(ACTION_DESCRIPTION, auditRecord.getActionDescription());
  }
}
