/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EnquiryRequestConverterTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.enquiry.dto.converter;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.integration.enquiry.dto.EnquiryRequestDto;
import com.brighttalk.service.channel.integration.enquiry.dto.FormAnswerDto;

public class EnquiryRequestConverterTest {

  private static final Long CHANNEL_ID = 666l;

  private static final String CHANNEL_TITLE = "channelType";

  private static final Long CHANNEL_TYPE_ID = 13L;

  private static final Long CHANNEL_PENDING_TYPE_ID = 7L;

  
  private EnquiryRequestConverter uut;
  
  @Before
  public void setUp() {
    
    uut = new EnquiryRequestConverter();
  }
  
  @Test
  public void convert() {
    
    final Channel channel = createChannel();
    
    //do test
    EnquiryRequestDto result = uut.convert(channel);
    
    assertNotNull(result);
    assertNotNull(result.getForm());
     
    assertEquals(EnquiryRequestConverter.ENQUIRY_CONTACT_ID, result.getForm().getId());
    
    final List<FormAnswerDto> resultFormAnswers = result.getForm().getFormAnswers();
    assertNotNull(resultFormAnswers);
    assertEquals(4, resultFormAnswers.size());
    
    assertEquals(1, resultFormAnswers.get(0).getId(), 0);
    final String expectedChannelId = CHANNEL_ID.toString();
    assertEquals(expectedChannelId, resultFormAnswers.get(0).getValue());
    
    assertEquals(2, resultFormAnswers.get(1).getId(), 0);
    assertEquals(CHANNEL_TITLE, resultFormAnswers.get(1).getValue());
    
    assertEquals(3, resultFormAnswers.get(2).getId(), 0);
    final String expectedCreatedTypeId = CHANNEL_TYPE_ID.toString();
    assertEquals(expectedCreatedTypeId, resultFormAnswers.get(2).getValue());

    assertEquals(4, resultFormAnswers.get(3).getId(), 0);
    final String expectedPendingTypeId = CHANNEL_PENDING_TYPE_ID.toString();
    assertEquals(expectedPendingTypeId, resultFormAnswers.get(3).getValue());
  }
  
  private Channel createChannel() {

    Channel channel = new Channel();
    channel.setId(CHANNEL_ID);
    channel.setTitle(CHANNEL_TITLE);

    ChannelType channelType = new ChannelType();
    channelType.setId(CHANNEL_TYPE_ID);
    channel.setCreatedType(channelType);

    ChannelType pendingType = new ChannelType();
    pendingType.setId(CHANNEL_PENDING_TYPE_ID);
    channel.setPendingType(pendingType);
    
    return channel;
  }
}