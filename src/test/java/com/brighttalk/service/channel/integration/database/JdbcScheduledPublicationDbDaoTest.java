/**
 * ****************************************************************************
a * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: codetemplates.xml 25175 2011-01-10 14:47:45Z nbrown $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.database.data.ScheduledPublicationData;
import com.brighttalk.service.channel.integration.database.data.builder.ScheduledPublicationDataBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcScheduledPublicationDbDaoTest extends AbstractJdbcDbDaoTest {

  private static final String JOB_ID = "testJobId";

  @Autowired
  private JdbcScheduledPublicationDbDao uut;

  @Test
  public void findScheduledCommunicationsWhenNoneExists() {

    // do test
    Map<Communication.PublishStatus, List<Long>> result = uut.findScheduledCommunications(JOB_ID);

    assertNotNull(result);
    assertNotNull(result.get(Communication.PublishStatus.PUBLISHED));
    assertEquals(0, result.get(Communication.PublishStatus.PUBLISHED).size());
    assertNotNull(result.get(Communication.PublishStatus.UNPUBLISHED));
    assertEquals(0, result.get(Communication.PublishStatus.UNPUBLISHED).size());
  }

  @Test
  public void findScheduledWhenOnePublishedTaskExists() {

    final Date scheduledDate = getTimeXMinAgo(-10);
    ScheduledPublicationData data = new ScheduledPublicationDataBuilder().withDefaultValues().withScheduledDate(
        scheduledDate).build();
    addTemporaryScheduledPublication(data);

    // do test
    Map<Communication.PublishStatus, List<Long>> result = uut.findScheduledCommunications(JOB_ID);

    assertNotNull(result);
    assertEquals(0, result.get(Communication.PublishStatus.UNPUBLISHED).size());
    assertEquals(1, result.get(Communication.PublishStatus.PUBLISHED).size());
    assertEquals(ScheduledPublicationDataBuilder.DEFAULT_COMMUNICATION_ID,
        result.get(Communication.PublishStatus.PUBLISHED).get(0));
  }

  @Test
  public void findScheduledWhenOneUnpublishedTaskExists() {

    final Date scheduledDate = getTimeXMinAgo(-10);
    ScheduledPublicationData data = new ScheduledPublicationDataBuilder().withDefaultValues().withPublishStatus(
        Communication.PublishStatus.UNPUBLISHED).withScheduledDate(scheduledDate).build();
    addTemporaryScheduledPublication(data);

    // do test
    Map<Communication.PublishStatus, List<Long>> result = uut.findScheduledCommunications(JOB_ID);

    assertNotNull(result);
    assertEquals(0, result.get(Communication.PublishStatus.PUBLISHED).size());
    assertEquals(1, result.get(Communication.PublishStatus.UNPUBLISHED).size());
    assertEquals(ScheduledPublicationDataBuilder.DEFAULT_COMMUNICATION_ID,
        result.get(Communication.PublishStatus.UNPUBLISHED).get(0));
  }

  @Test
  public void findScheduledWhenOneUnpublishedAndOnePublishedTasksExist() {

    final Date scheduledDate = getTimeXMinAgo(-10);
    ScheduledPublicationData data = new ScheduledPublicationDataBuilder().withDefaultValues().withPublishStatus(
        Communication.PublishStatus.UNPUBLISHED).withScheduledDate(scheduledDate).build();
    addTemporaryScheduledPublication(data);

    final Long expectedCommId2 = 345l;
    ScheduledPublicationData data2 = new ScheduledPublicationDataBuilder().withDefaultValues().withCommunicationId(
        expectedCommId2).withPublishStatus(Communication.PublishStatus.PUBLISHED).withScheduledDate(scheduledDate).build();
    addTemporaryScheduledPublication(data2);

    // do test
    Map<Communication.PublishStatus, List<Long>> result = uut.findScheduledCommunications(JOB_ID);

    assertNotNull(result);
    assertEquals(1, result.get(Communication.PublishStatus.PUBLISHED).size());
    assertEquals(expectedCommId2, result.get(Communication.PublishStatus.PUBLISHED).get(0));
    assertEquals(1, result.get(Communication.PublishStatus.UNPUBLISHED).size());
    assertEquals(ScheduledPublicationDataBuilder.DEFAULT_COMMUNICATION_ID,
        result.get(Communication.PublishStatus.UNPUBLISHED).get(0));
  }

  @Test
  public void findScheduledWhenOneExistsButIsCompleted() {

    final Date scheduledDate = getTimeXMinAgo(-10);
    ScheduledPublicationData data = new ScheduledPublicationDataBuilder().withDefaultValues().withScheduledDate(
        scheduledDate).withCompleted(true).build();
    addTemporaryScheduledPublication(data);

    // do test
    Map<Communication.PublishStatus, List<Long>> result = uut.findScheduledCommunications(JOB_ID);

    assertNotNull(result);
    assertEquals(0, result.get(Communication.PublishStatus.UNPUBLISHED).size());
    assertEquals(0, result.get(Communication.PublishStatus.PUBLISHED).size());
  }

  @Test
  public void findScheduledWhenOneExistsButIsNotActive() {

    final Date scheduledDate = getTimeXMinAgo(-10);
    ScheduledPublicationData data = new ScheduledPublicationDataBuilder().withDefaultValues().withScheduledDate(
        scheduledDate).withActive(false).build();
    addTemporaryScheduledPublication(data);

    // do test
    Map<Communication.PublishStatus, List<Long>> result = uut.findScheduledCommunications(JOB_ID);

    assertNotNull(result);
    assertEquals(0, result.get(Communication.PublishStatus.UNPUBLISHED).size());
    assertEquals(0, result.get(Communication.PublishStatus.PUBLISHED).size());
  }

  @Test
  public void findScheduledWhenOneExistsButJobIdIsAllocated() {

    final Date scheduledDate = getTimeXMinAgo(-10);
    final String jobId = "anotherJobId";
    ScheduledPublicationData data = new ScheduledPublicationDataBuilder().withDefaultValues().withScheduledDate(
        scheduledDate).withJobId(jobId).build();
    addTemporaryScheduledPublication(data);

    // do test
    Map<Communication.PublishStatus, List<Long>> result = uut.findScheduledCommunications(JOB_ID);

    assertNotNull(result);
    assertEquals(0, result.get(Communication.PublishStatus.UNPUBLISHED).size());
    assertEquals(0, result.get(Communication.PublishStatus.PUBLISHED).size());

    // verify that the job id was not overriden!
    String currentJobId = jdbcTemplate.queryForObject(
        "SELECT job_id FROM scheduled_publication WHERE communication_id = 667", String.class);
    assertEquals(jobId, currentJobId);
  }

  @Test
  public void markAsPublished() {

    ScheduledPublicationData data = new ScheduledPublicationDataBuilder().withDefaultValues().withJobId(JOB_ID).build();
    addTemporaryScheduledPublication(data);

    // do test
    uut.markAsCompleted(JOB_ID);

    Boolean completed = jdbcTemplate.queryForObject(
        "SELECT completed FROM scheduled_publication WHERE job_id = 'testJobId'", Boolean.class);
    assertTrue(completed);
  }

  protected Date getTimeXMinAgo(final int minutes) {

    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
    calendar.add(Calendar.MINUTE, minutes);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }
}