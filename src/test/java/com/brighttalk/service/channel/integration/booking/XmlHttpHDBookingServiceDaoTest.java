/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpBookingServiceDaoTest.java 81412 2014-07-30 16:19:17Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking;

import static org.easymock.EasyMock.isA;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.BookingCapacityExceededException;
import com.brighttalk.service.channel.integration.booking.dto.HDBookingDto;
import com.brighttalk.service.channel.integration.booking.dto.LivePhoneNumberDto;
import com.brighttalk.service.channel.integration.booking.dto.converter.BookingRequestConverter;
import com.brighttalk.service.channel.integration.booking.dto.converter.LivePhoneNumberConverter;

public class XmlHttpHDBookingServiceDaoTest {

  private static final Long CHANNEL_ID = 9999L;
  private static final Long COMMUNICATION_ID = 1111L;
  private static final String LIVE_PHONE_NUMBER = "012 540 8468";
  private static final String SERVICE_BASE_URL = "http://test.booking.brighttalk.net";

  private XmlHttpHDBookingServiceDao uut;

  private RestTemplate mockRestTemplate;

  private BookingRequestConverter mockBookingRequestConverter;
  private LivePhoneNumberConverter mockLivePhoneNumberConverter;

  @Before
  public void setUp() {

    uut = new XmlHttpHDBookingServiceDao();

    uut.setServiceBaseUrl(SERVICE_BASE_URL);

    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    uut.setRestTemplate(mockRestTemplate);

    mockBookingRequestConverter = EasyMock.createMock(BookingRequestConverter.class);
    uut.setConverter(mockBookingRequestConverter);

    mockLivePhoneNumberConverter = EasyMock.createMock(LivePhoneNumberConverter.class);
    uut.setLivePhoneNumberConverter(mockLivePhoneNumberConverter);

  }

  @SuppressWarnings("unchecked")
  @Test
  public void createBooking() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).build();
    HDBookingDto requestDto = new HDBookingDto();

    ResponseEntity<LivePhoneNumberDto> livePhoneNumberDto = new ResponseEntity<LivePhoneNumberDto>(HttpStatus.CREATED);

    EasyMock.expect(mockBookingRequestConverter.convertForCreateAndUpdate(communication)).andReturn(requestDto);

    EasyMock.expect(
        mockRestTemplate.exchange(isA(String.class), isA(HttpMethod.class), isA(HttpEntity.class), isA(Class.class))).andReturn(
        livePhoneNumberDto);

    EasyMock.expect(mockLivePhoneNumberConverter.convert(livePhoneNumberDto.getBody())).andReturn(LIVE_PHONE_NUMBER);

    EasyMock.replay(mockBookingRequestConverter, mockRestTemplate, mockLivePhoneNumberConverter);

    // do test
    uut.createBooking(communication);

    // verification
    EasyMock.verify(mockBookingRequestConverter, mockRestTemplate, mockLivePhoneNumberConverter);
  }

  @SuppressWarnings("unchecked")
  @Test(expected = BookingCapacityExceededException.class)
  public void createBookingThrowsBadRequest() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).build();
    HDBookingDto requestDto = new HDBookingDto();

    EasyMock.expect(mockBookingRequestConverter.convertForCreateAndUpdate(communication)).andReturn(requestDto);

    EasyMock.expect(
        mockRestTemplate.exchange(isA(String.class), isA(HttpMethod.class), isA(HttpEntity.class), isA(Class.class))).andThrow(
        new HttpClientErrorException(HttpStatus.BAD_REQUEST));

    EasyMock.replay(mockBookingRequestConverter, mockRestTemplate);

    // do test
    uut.createBooking(communication);

    // verification
    EasyMock.verify(mockBookingRequestConverter, mockRestTemplate);
  }

  @SuppressWarnings("unchecked")
  @Test(expected = ApplicationException.class)
  public void createBookingThrowsSystemError() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).build();
    HDBookingDto requestDto = new HDBookingDto();

    EasyMock.expect(mockBookingRequestConverter.convertForCreateAndUpdate(communication)).andReturn(requestDto);

    EasyMock.expect(
        mockRestTemplate.exchange(isA(String.class), isA(HttpMethod.class), isA(HttpEntity.class), isA(Class.class))).andThrow(
        new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR));

    EasyMock.replay(mockBookingRequestConverter, mockRestTemplate);

    // do test
    uut.createBooking(communication);

    // verification
    EasyMock.verify(mockBookingRequestConverter, mockRestTemplate);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void updateBooking() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).build();
    HDBookingDto requestDto = new HDBookingDto();

    ResponseEntity<LivePhoneNumberDto> livePhoneNumberDto = new ResponseEntity<LivePhoneNumberDto>(HttpStatus.CREATED);

    EasyMock.expect(mockBookingRequestConverter.convertForCreateAndUpdate(communication)).andReturn(requestDto);

    EasyMock.expect(
        mockRestTemplate.exchange(isA(String.class), isA(HttpMethod.class), isA(HttpEntity.class), isA(Class.class))).andReturn(
        livePhoneNumberDto);

    EasyMock.expect(mockLivePhoneNumberConverter.convert(livePhoneNumberDto.getBody())).andReturn(LIVE_PHONE_NUMBER);

    EasyMock.replay(mockBookingRequestConverter, mockRestTemplate, mockLivePhoneNumberConverter);

    // do test
    uut.updateBooking(communication);

    // verification
    EasyMock.verify(mockBookingRequestConverter, mockRestTemplate, mockLivePhoneNumberConverter);
  }

  @SuppressWarnings("unchecked")
  @Test(expected = BookingCapacityExceededException.class)
  public void updateBookingThrowsBadRequest() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).build();
    HDBookingDto requestDto = new HDBookingDto();

    EasyMock.expect(mockBookingRequestConverter.convertForCreateAndUpdate(communication)).andReturn(requestDto);

    EasyMock.expect(
        mockRestTemplate.exchange(isA(String.class), isA(HttpMethod.class), isA(HttpEntity.class), isA(Class.class))).andThrow(
        new HttpClientErrorException(HttpStatus.BAD_REQUEST));

    EasyMock.replay(mockBookingRequestConverter, mockRestTemplate);

    // do test
    uut.updateBooking(communication);

    // verification
    EasyMock.verify(mockBookingRequestConverter, mockRestTemplate);
  }

  @SuppressWarnings("unchecked")
  @Test(expected = ApplicationException.class)
  public void updateBookingThrowsSystemError() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).build();
    HDBookingDto requestDto = new HDBookingDto();

    EasyMock.expect(mockBookingRequestConverter.convertForCreateAndUpdate(communication)).andReturn(requestDto);

    EasyMock.expect(
        mockRestTemplate.exchange(isA(String.class), isA(HttpMethod.class), isA(HttpEntity.class), isA(Class.class))).andThrow(
        new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR));

    EasyMock.replay(mockBookingRequestConverter, mockRestTemplate);

    // do test
    uut.updateBooking(communication);

    // verification
    EasyMock.verify(mockBookingRequestConverter, mockRestTemplate);
  }

  @Test
  public void deleteBooking() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).build();

    mockRestTemplate.delete(isA(String.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockBookingRequestConverter, mockRestTemplate);

    // do test
    uut.deleteBooking(communication);

    // verification
    EasyMock.verify(mockBookingRequestConverter, mockRestTemplate);
  }

  @Test(expected = BookingCapacityExceededException.class)
  public void deleteBookingThrowsBadRequest() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).build();

    mockRestTemplate.delete(isA(String.class));
    EasyMock.expectLastCall().andThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));

    EasyMock.replay(mockBookingRequestConverter, mockRestTemplate);

    // do test
    uut.deleteBooking(communication);

    // verification
    EasyMock.verify(mockBookingRequestConverter, mockRestTemplate);
  }

  @Test(expected = ApplicationException.class)
  public void deleteBookingThrowsSystemError() {

    Communication communication = new CommunicationBuilder().withDefaultValues().withChannelId(CHANNEL_ID).withId(
        COMMUNICATION_ID).build();

    mockRestTemplate.delete(isA(String.class));
    EasyMock.expectLastCall().andThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR));

    EasyMock.replay(mockBookingRequestConverter, mockRestTemplate);

    // do test
    uut.deleteBooking(communication);

    // verification
    EasyMock.verify(mockBookingRequestConverter, mockRestTemplate);
  }

  @Test
  public void deleteBookingsSuccess() {

    mockRestTemplate.delete(isA(String.class));
    EasyMock.expectLastCall();

    EasyMock.replay(mockRestTemplate);

    // do test
    uut.deleteBookings(CHANNEL_ID);

    // verification
    EasyMock.verify(mockRestTemplate);
  }

}