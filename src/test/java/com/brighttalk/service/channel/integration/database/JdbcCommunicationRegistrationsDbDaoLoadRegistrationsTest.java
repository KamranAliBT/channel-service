/**
 * *****************************************************************************
 * * Copyright BrightTALK Ltd, 2012.
 * * All Rights Reserved.
 * * Id: JdbcCommunicationStatisticsDbDaoTest.java
 * *****************************************************************************
 * @package com.brighttalk.service.channel.integration.database
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Test Case for {@link JdbcCommunicationRegistrationsDbDao}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCommunicationRegistrationsDbDaoLoadRegistrationsTest extends AbstractJdbcDbDaoTest {

  private final static Long USER1_ID = new Long(1);

  private final static Long USER2_ID = new Long(2);

  private final static Long USER3_ID = new Long(3);

  @Autowired
  private JdbcCommunicationRegistrationsDbDao uut;

  private SimpleJdbcInsert simpleJdbcInsertCommunicationRegistration;

  private final List<ChannelIdCommunicationIdPrimaryKey> communicationRegistrationsToDelete = new ArrayList<ChannelIdCommunicationIdPrimaryKey>();

  private final List<Communication> communicationsChannel1 = new ArrayList<Communication>();

  private final List<Communication> communicationsChannel2 = new ArrayList<Communication>();

  private Long channel1Id;

  private Long channel2Id;

  @Autowired
  @Override
  public void setDataSource(final DataSource dataSource) {
    super.setDataSource(dataSource);

    simpleJdbcInsertCommunicationRegistration = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertCommunicationRegistration.withTableName("communication_registration");
    simpleJdbcInsertCommunicationRegistration.usingColumns("channel_id", "communication_id", "user_id", "is_active",
        "embed_url", "embed_track", "notification_status", "created", "last_updated");
  }

  /**
   * Set up this test case. Create some categories for two communicationsChannel1 in two channels.
   */
  @Before
  public void setUp() {

    // set up a temp channel
    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    channel1Id = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    channel2Id = super.addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    // add some communicationsChannel1
    Long communicationId1 = super.addTemporaryCommunication(channel1Id, Provider.BRIGHTTALK);
    Long communicationId2 = super.addTemporaryCommunication(channel1Id, Provider.BRIGHTTALK);
    Long communicationId3 = super.addTemporaryCommunication(channel1Id, Provider.BRIGHTTALK);

    Long communicationId4 = super.addTemporaryCommunication(channel2Id, Provider.BRIGHTTALK);
    Long communicationId5 = super.addTemporaryCommunication(channel2Id, Provider.BRIGHTTALK);
    Long communicationId6 = super.addTemporaryCommunication(channel2Id, Provider.BRIGHTTALK);

    communicationsChannel1.add(new Communication(communicationId1));
    communicationsChannel1.add(new Communication(communicationId2));
    communicationsChannel1.add(new Communication(communicationId3));

    communicationsChannel2.add(new Communication(communicationId4));
    communicationsChannel2.add(new Communication(communicationId5));
    communicationsChannel2.add(new Communication(communicationId6));

    this.insertActiveCommunicationRegistration(channel1Id, communicationId1, USER1_ID);
    this.insertActiveCommunicationRegistration(channel1Id, communicationId1, USER2_ID);
    this.insertInactiveCommunicationRegistration(channel1Id, communicationId1, USER3_ID);
    this.insertActiveCommunicationRegistration(channel1Id, communicationId2, USER1_ID);
    this.insertActiveCommunicationRegistration(channel1Id, communicationId3, USER1_ID);

    this.insertActiveCommunicationRegistration(channel2Id, communicationId4, USER1_ID);
    this.insertActiveCommunicationRegistration(channel2Id, communicationId4, USER2_ID);
    this.insertInactiveCommunicationRegistration(channel2Id, communicationId4, USER3_ID);
    this.insertActiveCommunicationRegistration(channel2Id, communicationId5, USER1_ID);
    this.insertActiveCommunicationRegistration(channel2Id, communicationId6, USER1_ID);
  }

  /**
   * Tear down this test case. Delete categories we created in the DB.
   */
  @After
  @Override
  public void tearDown() {
    super.tearDown();

    for (ChannelIdCommunicationIdPrimaryKey id : communicationRegistrationsToDelete) {
      Long channelId = id.getChannelId();
      Long communicationId = id.getCommunicationId();
      jdbcTemplate.update("DELETE FROM communication_registration WHERE channel_id = ? AND communication_id = ?",
          new Object[] { channelId, communicationId });
    }
  }

  /**
   * Just sanity check we have no errors in the SQL in the DAO.
   */
  @Test
  public void testAddCommunicationRegistrationsSanityCheck() {

    uut.loadRegistrations(channel1Id, communicationsChannel1);

    assertEquals("incorrec reg for comm 1", new Integer(2), communicationsChannel1.get(0).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 1", new Integer(1), communicationsChannel1.get(1).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 1", new Integer(1), communicationsChannel1.get(2).getNumberOfRegistrants());
  }

  @Test
  public void testInactiveRegistrationsNotCounted() {

    uut.loadRegistrations(channel1Id, communicationsChannel1);

    assertEquals("incorrec reg for comm 1", new Integer(2), communicationsChannel1.get(0).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 2", new Integer(1), communicationsChannel1.get(1).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 3", new Integer(1), communicationsChannel1.get(2).getNumberOfRegistrants());
  }

  @Test
  public void testAddRegistrationsToAnotherChannel() {

    uut.loadRegistrations(channel2Id, communicationsChannel2);

    assertEquals("incorrec reg for comm 4", new Integer(2), communicationsChannel2.get(0).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 5", new Integer(1), communicationsChannel2.get(1).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 6", new Integer(1), communicationsChannel2.get(2).getNumberOfRegistrants());

    assertEquals("incorrec reg for comm 1", new Integer(2), communicationsChannel2.get(0).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 2", new Integer(1), communicationsChannel2.get(1).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 3", new Integer(1), communicationsChannel2.get(2).getNumberOfRegistrants());
  }

  @Test
  public void testAddCommunicationRegistrationsForChannelThatDoesNotExist() {

    Long nonExistentChannelId = new Long(100000L);
    uut.loadRegistrations(nonExistentChannelId, communicationsChannel2);

    assertEquals("incorrec reg for comm 1", new Integer(0), communicationsChannel2.get(0).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 2", new Integer(0), communicationsChannel2.get(1).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 3", new Integer(0), communicationsChannel2.get(2).getNumberOfRegistrants());
  }

  /**
   * Just test DAO does not blow up if it gets null channel id.
   */
  @Test
  public void testAddCommunicationRegistrationsForNullChannelId() {

    uut.loadRegistrations(null, communicationsChannel2);

    assertEquals("incorrec reg for comm 1", new Integer(0), communicationsChannel2.get(0).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 2", new Integer(0), communicationsChannel2.get(1).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 3", new Integer(0), communicationsChannel2.get(2).getNumberOfRegistrants());
  }

  /**
   * Just test DAO does not blow up if it gets null communications.
   */
  @Test
  public void testAddCommunicationRegistrationsForNullCommunications() {
    uut.loadRegistrations(channel2Id, null);
  }

  /**
   * Just test DAO does not blow up if it gets empty communications.
   */
  @Test
  public void testAddCommunicationRegistrationsForEmptyCommunications() {
    uut.loadRegistrations(channel2Id, new ArrayList<Communication>());
  }

  @Test
  public void testAddCommunicationStatisticsForSyndicatedOutCommunications() {

    // syndicated two of the communications OUT from channel 1 into channel 2
    Communication communication1 = communicationsChannel1.get(0);
    Communication communication2 = communicationsChannel1.get(1);
    this.insertActiveCommunicationRegistration(channel2Id, communication1.getId(), USER1_ID);
    this.insertActiveCommunicationRegistration(channel2Id, communication2.getId(), USER1_ID);

    List<Communication> communications = new ArrayList<Communication>(communicationsChannel2);
    communications.add(communication1);
    communications.add(communication2);

    uut.loadRegistrations(communications);

    assertEquals("incorrec reg for comm 1", new Integer(2), communications.get(0).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 2", new Integer(1), communications.get(1).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 3", new Integer(1), communications.get(2).getNumberOfRegistrants());
    assertEquals("incorrec reg for comm 1 synd out", new Integer(3), communications.get(3).getNumberOfRegistrants());// synd
                                                                                                                     // out
    assertEquals("incorrec reg for comm 2 synd out", new Integer(2), communications.get(4).getNumberOfRegistrants());// synd
  }

  /**
   * Just test DAO does not blow up if it gets null communications.
   */
  @Test
  public void testAddCommunicationStatisticsForNullSyndicatedOutCommunications() {
    uut.loadRegistrations(null);
  }

  /**
   * Just test DAO does not blow up if it gets empty communications.
   */
  @Test
  public void testAddCommunicationStatisticsForEmptySyndicatedOutCommunications() {
    uut.loadRegistrations(new ArrayList<Communication>());
  }

  private void insertActiveCommunicationRegistration(final Long channelId, final Long communicationId, final Long userId) {
    this.insertCommunicationRegistration(channelId, communicationId, userId, true);
  }

  private void insertInactiveCommunicationRegistration(final Long channelId, final Long communicationId,
    final Long userId) {
    this.insertCommunicationRegistration(channelId, communicationId, userId, false);
  }

  /**
   * Helper to create communication registrations in the DB
   * 
   * @param channelId
   * @param communicationId
   * @param title
   * @param description
   * @param presenter
   * @param keywords
   */
  private void insertCommunicationRegistration(final Long channelId, final Long communicationId, final Long userId,
    final boolean isActive) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channel_id", channelId);
    params.addValue("communication_id", communicationId);
    params.addValue("user_id", userId);
    params.addValue("is_active", isActive);
    params.addValue("embed_url", "embedUrl");
    params.addValue("embed_track", "embedTrack");
    params.addValue("notification_status", "not-sent");

    // start date with 1970-01-01 01:00:00
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    simpleJdbcInsertCommunicationRegistration.execute(params);
    communicationRegistrationsToDelete.add(new ChannelIdCommunicationIdPrimaryKey(channelId, communicationId));
  }

  /**
   * The primary key for each entry in the communication_statistics table and communication_registration table. Primary
   * key is channel id and communication id. Used so we can delete all entries we set up in this test case.
   */
  private static class ChannelIdCommunicationIdPrimaryKey {

    private final Long channelId;

    private final Long communicationId;

    public ChannelIdCommunicationIdPrimaryKey(final Long channelId, final Long communicationId) {
      super();
      this.channelId = channelId;
      this.communicationId = communicationId;
    }

    public Long getChannelId() {
      return channelId;
    }

    public Long getCommunicationId() {
      return communicationId;
    }
  }// ChannelIdCommunicationIdPrimaryKey
}
