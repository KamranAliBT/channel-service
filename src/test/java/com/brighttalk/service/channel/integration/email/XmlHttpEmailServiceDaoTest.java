/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpEmailServiceDaoTest.java 86487 2014-11-21 15:15:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.email;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.email.dto.EmailRequestDto;
import com.brighttalk.service.channel.integration.email.dto.converter.EmailRequestConverter;

public class XmlHttpEmailServiceDaoTest {

  private XmlHttpEmailServiceDao uut;

  private RestTemplate mockRestTemplate;

  private EmailRequestConverter mockConverter;

  @Before
  public void setUp() {

    uut = new XmlHttpEmailServiceDao();

    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    uut.setRestTemplate(mockRestTemplate);

    mockConverter = EasyMock.createMock(EmailRequestConverter.class);
    uut.setConverter(mockConverter);
  }

  @Test
  public void sendChannelCreateConfirmation() throws Exception {

    final Channel channel = new Channel();
    final EmailRequestDto requestDto = new EmailRequestDto();

    final String serviceBaseUrl = "serviceBaseUrl";
    uut.setServiceBaseUrl(serviceBaseUrl);

    final String expectedRequestUrl = serviceBaseUrl + XmlHttpEmailServiceDao.REQUEST_PATH;

    EasyMock.expect(mockConverter.convert(channel)).andReturn(requestDto);
    EasyMock.expect(mockRestTemplate.postForLocation(expectedRequestUrl, requestDto)).andReturn(new URI(""));

    EasyMock.replay(mockConverter, mockRestTemplate);

    // do test
    uut.sendChannelCreateConfirmation(channel);

    EasyMock.verify(mockConverter, mockRestTemplate);
  }

  @Test
  public void sendCommunicationCreateConfirmation() throws Exception {
    // set up
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    EmailRequestDto requestDto = new EmailRequestDto();

    final String serviceBaseUrl = "serviceBaseUrl";
    uut.setServiceBaseUrl(serviceBaseUrl);

    final String expectedRequestUrl = serviceBaseUrl + XmlHttpEmailServiceDao.REQUEST_PATH;

    // expectations
    EasyMock.expect(mockConverter.convertForCreate(communication)).andReturn(requestDto);
    EasyMock.expect(mockRestTemplate.postForLocation(expectedRequestUrl, requestDto)).andReturn(new URI(""));

    EasyMock.replay(mockConverter, mockRestTemplate);

    // exercise
    uut.sendCommunicationCreateConfirmation(communication);

    // assertions
    EasyMock.verify(mockConverter, mockRestTemplate);

  }

  @Test
  public void sendCommunicationRescheduleConfirmation() throws Exception {
    // set up
    List<Communication> communications = new ArrayList<>();
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communications.add(communication);
    EmailRequestDto requestDto = new EmailRequestDto();

    final String serviceBaseUrl = "serviceBaseUrl";
    uut.setServiceBaseUrl(serviceBaseUrl);

    final String expectedRequestUrl = serviceBaseUrl + XmlHttpEmailServiceDao.REQUEST_PATH;

    // expectations
    EasyMock.expect(mockConverter.convertForUpdate(communication)).andReturn(requestDto);
    EasyMock.expect(mockRestTemplate.postForLocation(expectedRequestUrl, requestDto)).andReturn(new URI(""));

    EasyMock.replay(mockConverter, mockRestTemplate);

    // exercise
    uut.sendCommunicationRescheduleConfirmation(communications);

    // assertions
    EasyMock.verify(mockConverter, mockRestTemplate);

  }

  @Test
  public void sendCommunicationCancelConfirmation() throws Exception {
    // set up
    List<Communication> communications = new ArrayList<>();
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    communications.add(communication);
    EmailRequestDto requestDto = new EmailRequestDto();

    final String serviceBaseUrl = "serviceBaseUrl";
    uut.setServiceBaseUrl(serviceBaseUrl);

    final String expectedRequestUrl = serviceBaseUrl + XmlHttpEmailServiceDao.REQUEST_PATH;

    // expectations
    EasyMock.expect(mockConverter.convertForCancel(communication)).andReturn(requestDto);
    EasyMock.expect(mockRestTemplate.postForLocation(expectedRequestUrl, requestDto)).andReturn(new URI(""));

    EasyMock.replay(mockConverter, mockRestTemplate);

    // exercise
    uut.sendCommunicationCancelConfirmation(communications);

    // assertions
    EasyMock.verify(mockConverter, mockRestTemplate);

  }

  @Test
  public void sendVideoUploadError() throws Exception {
    // set up
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    EmailRequestDto requestDto = new EmailRequestDto();

    final String serviceBaseUrl = "serviceBaseUrl";
    uut.setServiceBaseUrl(serviceBaseUrl);

    final String expectedRequestUrl = serviceBaseUrl + XmlHttpEmailServiceDao.REQUEST_PATH;

    // expectations
    EasyMock.expect(mockConverter.convertForVideoUploadError(communication)).andReturn(requestDto);
    EasyMock.expect(mockRestTemplate.postForLocation(expectedRequestUrl, requestDto)).andReturn(new URI(""));

    EasyMock.replay(mockConverter, mockRestTemplate);

    // exercise
    uut.sendVideoUploadError(communication);

    // assertions
    EasyMock.verify(mockConverter, mockRestTemplate);

  }

  @Test
  public void sendVideoUploadComplete() throws Exception {
    // set up
    Communication communication = new CommunicationBuilder().withDefaultValues().build();
    EmailRequestDto requestDto = new EmailRequestDto();

    final String serviceBaseUrl = "serviceBaseUrl";
    uut.setServiceBaseUrl(serviceBaseUrl);

    final String expectedRequestUrl = serviceBaseUrl + XmlHttpEmailServiceDao.REQUEST_PATH;

    // expectations
    EasyMock.expect(mockConverter.convertForVideoUploadComplete(communication)).andReturn(requestDto);
    EasyMock.expect(mockRestTemplate.postForLocation(expectedRequestUrl, requestDto)).andReturn(new URI(""));

    EasyMock.replay(mockConverter, mockRestTemplate);

    // exercise
    uut.sendVideoUploadComplete(communication);

    // assertions
    EasyMock.verify(mockConverter, mockRestTemplate);

  }

}