/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationsConverterTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.community.dto.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.integration.community.dto.CommunicationsDto;

public class DefaultCommunicationsConverterTest {

  private DefaultCommunicationsConverter uut;
  
  @Before
  public void setUp() {
    
    uut = new DefaultCommunicationsConverter();
    
    final String pathTemplate = "http://test.brighttalk.net/community/communication/{id}/{channelId}";
    uut.setHrefTemplate( pathTemplate );
  }
  
  @Test
  public void convert() {
    
    final Long channelId = 129L;
    final Long communication1Id = 9L;
    final Long communication2Id = 99L;
    
    final List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add( communication1Id );
    communicationIds.add( communication2Id );
    
    //do test
    CommunicationsDto result = uut.convert( channelId, communicationIds );
    
    assertNotNull( result );
    assertEquals( 2, result.getCommunications().size() );
    
    String expectedHref1 = "http://test.brighttalk.net/community/communication/9/129";
    assertEquals( expectedHref1, result.getCommunications().get(0).getHref() );

    String expectedHref2 = "http://test.brighttalk.net/community/communication/99/129";
    assertEquals( expectedHref2, result.getCommunications().get(1).getHref() );
  }
  
}
