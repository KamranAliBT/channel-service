/**
 * *****************************************************************************
 * * Copyright BrightTALK Ltd, 2015.
 * * All Rights Reserved.
 * * Id: JdbcClientCredentialsDbDaoTest.java
 * *****************************************************************************
 * @package com.brighttalk.service.channel.integration.database
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test Case for {@link JdbcClientCredentialsDbDao}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcClientCredentialsDbDaoTest {

  private final String client = "client";

  private final String secret = "secret";

  private JdbcTemplate jdbcTemplate;

  private SimpleJdbcInsert simpleJdbcInsert;

  @Autowired
  private JdbcClientCredentialsDbDao uut;

  @Autowired
  public void setDataSource(final DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);

    simpleJdbcInsert = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsert.withTableName("api_client_credentials");
    simpleJdbcInsert.usingColumns("api_key", "api_secret", "created", "last_updated");
  }

  @Before
  public void setUp() {

  }

  @After
  public void tearDown() {
    this.jdbcTemplate.update("DELETE FROM api_client_credentials");
  }

  @Test
  public void testIfClientExistsWithValidCredentials() {
    // Set up
    insertClient(client, secret);

    // Expectations

    // Do test
    boolean result = uut.exists(client, secret);

    // Assertions
    assertTrue(result);
  }

  @Test
  public void testIfClientExistsWithInvalidClient() {
    // Set up
    insertClient(client, secret);

    String invalidClient = "invalid";

    // Expectations

    // Do test
    boolean result = uut.exists(invalidClient, secret);

    // Assertions
    assertFalse(result);
  }

  @Test
  public void testIfClientExistsWithInvalidSecrect() {
    // Set up
    insertClient(client, secret);

    String invalidSecret = "invalid";

    // Expectations

    // Do test
    boolean result = uut.exists(client, invalidSecret);

    // Assertions
    assertFalse(result);
  }

  private void insertClient(final String client, final String secret) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("api_key", client);
    params.addValue("api_secret", secret);
    params.addValue("created", new Date());
    params.addValue("last_updated", new Date());

    simpleJdbcInsert.execute(params);
  }

}
