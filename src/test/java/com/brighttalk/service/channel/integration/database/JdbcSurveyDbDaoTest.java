/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcSurveyDbDaoTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import junit.framework.Assert;

import java.util.Random;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Tests {link JdbcSurveyDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcSurveyDbDaoTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcSurveyDbDao uut;

  @Autowired
  @Override
  public void setDataSource(DataSource dataSource) {
    super.setDataSource(dataSource);
  }

  /**
   * Tear down this test case. Delete categories we created in the DB.
   */
  @After
  @Override
  public void tearDown() {
    super.tearDown();
  }

  @Test
  public void findChannelIdForChannelSurvey() {
    // setup
    Random r = new Random();
    Long channelId = (long) r.nextInt(999999);
    Long surveyId = (long) r.nextInt(999999);
    boolean isActive = true;
    addTemporarySurveyAsset(channelId, surveyId, isActive);
    
    // Expectations
    Survey expectedResult = new Survey(surveyId,channelId,isActive);
    
    // do test
    Survey result = uut.findChannelSurvey(surveyId);

    // assertions
    Assert.assertEquals(expectedResult, result);
    
  }

  @Test
  public void findChannelIdForChannelSurveyWithInActiveSurvey() {
    // setup
    Random r = new Random();
    Long channelId = (long) r.nextInt(999999);
    Long surveyId = (long) r.nextInt(999999);
    boolean isActive = false;
    addTemporarySurveyAsset(channelId, surveyId, isActive);

    // Expectations
    Survey expectedResult = new Survey(surveyId,channelId,isActive);
    
    // do test
    Survey result = uut.findChannelSurvey(surveyId);

    // assertions
    Assert.assertEquals(expectedResult, result);
  }

  @Test(expected = IncorrectResultSizeDataAccessException.class)
  public void findChannelIdForChannelSurveyWithDuplicatesSurvey() {
    // setup
    Random r = new Random();
    Long channelId = (long) r.nextInt(999999);
    Long surveyId = (long) r.nextInt(999999);
    boolean isActive = true;
    addTemporarySurveyAsset(channelId, surveyId, isActive);

    Long channelId2 = 999998L;
    addTemporarySurveyAsset(channelId2, surveyId, isActive);

    // do test
    Survey result = uut.findChannelSurvey(surveyId);

    // assertions
    Assert.assertNull(result);
  }

  @Test
  public void findChannelIdForChannelSurveyWithNotExistingId() {

    // do test
    Survey result = uut.findChannelSurvey(0L);

    // assertions
    Assert.assertNull(result);
  }

  @Test
  public void findChannelIdForCommunicationSurvey() {
    // Set up
    Random r = new Random();
    Long channelId = (long) r.nextInt(999999);
    Long surveyId = (long) r.nextInt(999999);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setSurveyId(surveyId);
    communication.getConfiguration().setSurveyActive(true);
    addTemporaryCommunication(channelId, communication);

    // Expectations
    Survey expectedResult = new Survey(surveyId,channelId,true);

    // Do Test
    Survey result = uut.findCommunicationSurvey(surveyId);

    // Assertions
    Assert.assertEquals(expectedResult, result);
  }
  
  @Test
  public void findChannelIdForCommunicationSurveyInActive() {
    // Set up
    Random r = new Random();
    Long channelId = (long) r.nextInt(999999);
    Long surveyId = (long) r.nextInt(999999);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setSurveyId(surveyId);
    communication.getConfiguration().setSurveyActive(false);
    addTemporaryCommunication(channelId, communication);

    // Expectations
    Survey expectedResult = new Survey(surveyId,channelId,false);

    // Do Test
    Survey result = uut.findCommunicationSurvey(surveyId);

    // Assertions
    Assert.assertEquals(expectedResult, result);
  }
  
  @Test
  public void findChannelIdForCommunicationSurveyNoSurvey() {
    // Set up
    Random r = new Random();
    Long channelId = (long) r.nextInt(999999);
    Long surveyId = null;

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setSurveyId(surveyId);
    addTemporaryCommunication(channelId, communication);

    // Expectations
    Long expectedResult = null;

    // Do Test
    Survey result = uut.findCommunicationSurvey(surveyId);

    // Assertions
    Assert.assertEquals(expectedResult, result);
  }
  
  @Test
  public void findChannelIdForCommunicationSurveyNoCommunication() {
    // Set up
    Random r = new Random();
    Long surveyId = (long) r.nextInt(999999);

    // Expectations
    Long expectedResult = null;

    // Do Test
    Survey result = uut.findCommunicationSurvey(surveyId);

    // Assertions
    Assert.assertEquals(expectedResult, result);
  }
}
