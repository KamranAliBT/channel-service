/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpAudienceServiceDaoTest.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audience;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.audience.dto.VoteDto;
import com.brighttalk.service.channel.integration.audience.dto.VotesDto;

public class XmlHttpAudienceServiceDaoTest {

  private static final String SERVICE_BASE_URL = "http://test.audience.brighttalk.net";

  private static final Long COMMUNICATION_ID = 666l;

  private XmlHttpAudienceServiceDao uut;

  private RestTemplate mockRestTemplate;

  @Before
  public void setUp() {

    uut = new XmlHttpAudienceServiceDao();

    uut.setServiceBaseUrl(SERVICE_BASE_URL);

    mockRestTemplate = EasyMock.createMock(RestTemplate.class);
    uut.setRestTemplate(mockRestTemplate);
  }

  @Test
  public void findVotes() {

    final List<VoteDto> votes = new ArrayList<VoteDto>();

    final Long expectedVoteId1 = 667l;
    VoteDto voteDto1 = new VoteDto();
    voteDto1.setId(expectedVoteId1);
    votes.add(voteDto1);

    final Long expectedVoteId2 = 668l;
    VoteDto voteDto2 = new VoteDto();
    voteDto2.setId(expectedVoteId2);
    votes.add(voteDto2);

    final VotesDto votesDto = new VotesDto();
    votesDto.setVotes(votes);

    final String expectedRequestUrl = SERVICE_BASE_URL + XmlHttpAudienceServiceDao.VOTES_REQUEST_PATH;
    EasyMock.expect(mockRestTemplate.getForObject(expectedRequestUrl, VotesDto.class, COMMUNICATION_ID)).andReturn(
        votesDto);

    EasyMock.replay(mockRestTemplate);

    // do test
    List<Long> result = uut.findVoteIds(COMMUNICATION_ID);

    assertNotNull(result);

    final int expectedResultSize = 2;
    assertEquals(expectedResultSize, result.size());

    assertEquals(expectedVoteId1, result.get(0));
    assertEquals(expectedVoteId2, result.get(1));

    EasyMock.verify(mockRestTemplate);
  }

  @Test
  public void findVotesWhenEmptyResponse() {

    final VotesDto votesDto = new VotesDto();

    final String expectedRequestUrl = SERVICE_BASE_URL + XmlHttpAudienceServiceDao.VOTES_REQUEST_PATH;
    EasyMock.expect(mockRestTemplate.getForObject(expectedRequestUrl, VotesDto.class, COMMUNICATION_ID)).andReturn(
        votesDto);

    EasyMock.replay(mockRestTemplate);

    // do test
    List<Long> result = uut.findVoteIds(COMMUNICATION_ID);

    assertNotNull(result);

    final int expectedResultSize = 0;
    assertEquals(expectedResultSize, result.size());

    EasyMock.verify(mockRestTemplate);
  }

  @Test
  public void deleteVote() {

    final Long voteId = 213l;

    final String expectedRequestUrl = SERVICE_BASE_URL + XmlHttpAudienceServiceDao.DELETE_VOTE_REQUEST_PATH;

    mockRestTemplate.delete(expectedRequestUrl, voteId);
    EasyMock.expectLastCall().once();

    EasyMock.replay(mockRestTemplate);

    // do test
    uut.deleteVote(voteId);

    EasyMock.verify(mockRestTemplate);
  }
}