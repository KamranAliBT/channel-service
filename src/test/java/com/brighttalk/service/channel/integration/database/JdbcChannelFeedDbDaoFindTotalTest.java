/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: JdbcChannelFeedDbDaoFindTotalTest.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;

/**
 * Tests {link JdbcCommunicationDbDao} related to findFeed.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcChannelFeedDbDaoFindTotalTest extends AbstractJdbcDbDaoTest {

  @Autowired
  private JdbcChannelFeedDbDao uut;

  @Test
  public void findFeaturedWithNoCommunications() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void findFeaturedWithInvalidChannel() {
    // Set up
    long channelId = 99999999;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void findFeaturedWithNullChannelId() {
    // Set up
    Long channelId = null;

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationInactiveChannel() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = false;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationDeletedCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setStatus(Communication.Status.DELETED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationCancelledCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setStatus(Communication.Status.CANCELLED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsInFeed(Long, ChannelFeedSearchCriteria)} will include
   * all communications if no status filter is set in the criteria specified, except if they are cancelled or deleted.
   */
  @Test
  public void findTotalWithoutSpecifyingCommunicationStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);

    Communication cancelledCommunication = buildCommunication(channelId);
    cancelledCommunication.setStatus(Communication.Status.CANCELLED);

    Communication deletedCommunication = buildCommunication(channelId);
    deletedCommunication.setStatus(Communication.Status.DELETED);

    addTemporaryCommunication(channelId, upcomingCommunication);
    addTemporaryCommunication(channelId, liveCommunication);
    addTemporaryCommunication(channelId, recordedCommunication);
    addTemporaryCommunication(channelId, cancelledCommunication);
    addTemporaryCommunication(channelId, deletedCommunication);

    List<CommunicationStatusFilter> statusFilter = Collections.<CommunicationStatusFilter>emptyList();
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 3;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsInFeed(Long, ChannelFeedSearchCriteria)} by only
   * including communications that match the upcoming webcast status filter in the search criteria.
   */
  @Test
  public void findTotalByUpcomingStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);

    addTemporaryCommunication(channelId, upcomingCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.UPCOMING);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsInFeed(Long, ChannelFeedSearchCriteria)} by only
   * including communications that match the live webcast status filter in the search criteria.
   */
  @Test
  public void findTotalByLiveStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);

    addTemporaryCommunication(channelId, liveCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.LIVE);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsInFeed(Long, ChannelFeedSearchCriteria)} by only
   * including communications that match the recorded webcast status filter in the search criteria.
   */
  @Test
  public void findTotalByRecordedStatusFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);

    addTemporaryCommunication(channelId, recordedCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(CommunicationStatusFilter.RECORDED);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  /**
   * Tests the {@link JdbcChannelFeedDbDao#findTotalCommunicationsInFeed(Long, ChannelFeedSearchCriteria)} by only
   * including communications that match the specified list of webcast statuses in the search criteria.
   */
  @Test
  public void findTotalByMultipleStatusesFilter() {

    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    Communication upcomingCommunication = buildCommunication(channelId);
    upcomingCommunication.setStatus(Communication.Status.UPCOMING);

    Communication liveCommunication = buildCommunication(channelId);
    liveCommunication.setStatus(Communication.Status.LIVE);

    Communication recordedCommunication = buildCommunication(channelId);
    recordedCommunication.setStatus(Communication.Status.RECORDED);

    addTemporaryCommunication(channelId, upcomingCommunication);
    addTemporaryCommunication(channelId, liveCommunication);
    addTemporaryCommunication(channelId, recordedCommunication);

    List<CommunicationStatusFilter> statusFilter = Arrays.<CommunicationStatusFilter>asList(
        CommunicationStatusFilter.UPCOMING, CommunicationStatusFilter.LIVE);
    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(statusFilter);

    // Expectations
    int expectedResult = 2;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationUnpublishedCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.setPublishStatus(Communication.PublishStatus.UNPUBLISHED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationFeedOnlyCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.FEEDONLY);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationPrivateCommunication() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().setVisibility(CommunicationConfiguration.Visibility.PRIVATE);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationSyndicatedOut() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.OUT);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationSyndicatedInApprovedByBoth() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 1;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationSyndicatedInNotApprovedByMaster() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationSyndicatedInNotApprovedByConsumer() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationSyndicatedInNotApprovedByBoth() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationSyndicatedInPendingBoth() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationSyndicatedInPendingMaster() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  @Test
  public void getFeedTotalOneCommunicationSyndicatedInPendingConsumer() {
    // Set up
    long channeType = 1L;
    boolean channelIsActive = true;
    long channelId = addTemporaryChannel(channeType, channeType, channeType, channelIsActive);

    ChannelFeedSearchCriteria criteria = buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(null);

    Communication communication = buildCommunication(channelId);
    communication.getConfiguration().getCommunicationSyndication().setSyndicationType(
        CommunicationSyndication.SyndicationType.IN);
    communication.getConfiguration().getCommunicationSyndication().setConsumerChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    communication.getConfiguration().getCommunicationSyndication().setMasterChannelSyndicationAuthorisationStatus(
        CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    addTemporaryCommunication(channelId, communication);

    // Expectations
    int expectedResult = 0;

    // Do Test
    int result = uut.findTotalCommunicationsInFeed(channelId, criteria);

    // Assertions
    assertEquals("incorrect feed total", expectedResult, result);
  }

  private ChannelFeedSearchCriteria buildChannelFeedSearchCriteriaWithSuppliedStatusFilter(
      List<CommunicationStatusFilter> statusFilter) {

    ChannelFeedSearchCriteria criteria = new ChannelFeedSearchCriteria();
    criteria.setCommunicationStatusFilter(statusFilter);

    return criteria;
  }
}
