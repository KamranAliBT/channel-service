/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationDbDaoSubscribedChannelsWebcastsTest.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationConfigurationBuilder;
import com.brighttalk.service.channel.business.domain.builder.CommunicationStatisticsBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration.Visibility;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria.CommunicationStatusFilterType;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria.SortBy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCommunicationDbDaoSubscribedChannelsWebcastsTest extends AbstractJdbcDbDaoTest {

  private static final long SUBSCRIBER_ID = 999932l;

  private static final int DEFAULT_PAGE_SIZE = 10;

  private static final int DEFAULT_PAGE_NUMBER = 1;

  @Autowired
  private JdbcCommunicationDbDao uut;

  @Test
  public void findSubscribedChannelsWebcasts() {

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
    assertEquals(DEFAULT_PAGE_NUMBER, result.getCurrentPageNumber());
    assertEquals(DEFAULT_PAGE_SIZE, result.getPageSize());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenNoSubscriptionsAtAll() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
    assertEquals(DEFAULT_PAGE_NUMBER, result.getCurrentPageNumber());
    assertEquals(DEFAULT_PAGE_SIZE, result.getPageSize());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenNoSubscriptionsForTheUser() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    addTemporaryCommunication(channelId, Provider.BRIGHTTALK);

    addChannelSubscription(channelId, SUBSCRIBER_ID + 1, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(0, result.size());
    assertEquals(DEFAULT_PAGE_NUMBER, result.getCurrentPageNumber());
    assertEquals(DEFAULT_PAGE_SIZE, result.getPageSize());
  }

  @Test
  public void findSubscribedChannelsWebcastsCheckCommunicationDetails() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(channelId, result.get(0).getMasterChannelId());
    assertEquals(CommunicationBuilder.DEFAULT_TITLE, result.get(0).getTitle());
    assertEquals(CommunicationBuilder.DEFAULT_DESCRIPTION, result.get(0).getDescription());
    assertEquals(CommunicationBuilder.DEFAULT_PRESENTER, result.get(0).getAuthors());
    assertEquals(CommunicationBuilder.DEFAULT_KEYWORDS, result.get(0).getKeywords());
    assertEquals(CommunicationBuilder.DEFAULT_DURATION, result.get(0).getDuration());
    assertEquals(CommunicationBuilder.DEFAULT_TIMEZONE, result.get(0).getTimeZone());
    assertEquals(CommunicationBuilder.DEFAULT_STATUS, result.get(0).getStatus());
    assertEquals(CommunicationBuilder.DEFAULT_PUBLISH_STATUS, result.get(0).getPublishStatus());

    assertEquals(Provider.BRIGHTTALK, result.get(0).getProvider().getName());

    assertEquals(CommunicationBuilder.DEFAULT_FORMAT, result.get(0).getFormat());
    assertNotNull(result.get(0).getConfiguration());
    assertEquals(CommunicationConfigurationBuilder.DEFAULT_VISIBILITY, result.get(0).getConfiguration().getVisibility());
    assertTrue(result.get(0).getConfiguration().isInMasterChannel());
  }

  @Test
  public void findSubscribedChannelsWebcastsCheckCommunicationDetailsWhenDefaultSizePageIsNull() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(null);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(channelId, result.get(0).getMasterChannelId());
    assertEquals(CommunicationBuilder.DEFAULT_TITLE, result.get(0).getTitle());
    assertNotNull(result.get(0).getConfiguration());
    assertEquals(CommunicationConfigurationBuilder.DEFAULT_VISIBILITY, result.get(0).getConfiguration().getVisibility());
    assertTrue(result.get(0).getConfiguration().isInMasterChannel());
  }

  @Test
  public void findSubscribedChannelsWebcastsCheckOverrides() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    final String expectedTitle = "overridenTitle";
    communication.setTitle(expectedTitle);

    final String expectedDescription = "overridenDescription";
    communication.setDescription(expectedDescription);

    final String expectedPresenter = "overridenPresenter";
    communication.setAuthors(expectedPresenter);

    final String expectedKeywords = "overridenKeywords";
    communication.setKeywords(expectedKeywords);

    addCommunicationDetailsOverride(communication);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(channelId, result.get(0).getMasterChannelId());
    assertEquals(expectedTitle, result.get(0).getTitle());
    assertEquals(expectedDescription, result.get(0).getDescription());
    assertEquals(expectedPresenter, result.get(0).getAuthors());
    assertEquals(expectedKeywords, result.get(0).getKeywords());
  }

  @Test
  public void findSubscribedChannelsWebcastsCheckCommunicationStatsWhenNoCreated() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertNotNull(result.get(0).getCommunicationStatistics());
    assertEquals(0, result.get(0).getCommunicationStatistics().getAverageRating(), 0);
    assertEquals(0, result.get(0).getCommunicationStatistics().getNumberOfViewings(), 0);
  }

  @Test
  public void findSubscribedChannelsWebcastsCheckCommunicationStats() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    final float expectedRating = 4.3f;
    final int expectedNumberOfViewings = 100;

    CommunicationStatisticsBuilder statsBuilder = new CommunicationStatisticsBuilder().withDefaultValues();
    statsBuilder.withCommunicationId(communicationId).withNumberOfViewings(expectedNumberOfViewings).withAverageRating(
        expectedRating);
    communication.setCommunicationStatistics(statsBuilder.build());

    addCommunicationStatistics(communication);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertNotNull(result.get(0).getCommunicationStatistics());
    assertEquals(expectedRating, result.get(0).getCommunicationStatistics().getAverageRating(), 0);
    assertEquals(expectedNumberOfViewings, result.get(0).getCommunicationStatistics().getNumberOfViewings(), 0);
  }

  @Test
  public void findSubscribedChannelsWebcastsNonPublicVisibility() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();
    communication.getConfiguration().setVisibility(Visibility.PRIVATE);

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsUnpublished() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId).withPublishStatus(PublishStatus.UNPUBLISHED);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsCancelled() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.CANCELLED);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsDeleted() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.DELETED);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsCancelledWhenFilterPresent() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.CANCELLED);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    List<CommunicationStatusFilterType> communicationFilter = new ArrayList<CommunicationStatusFilterType>();
    communicationFilter.add(CommunicationStatusFilterType.LIVE);

    criteria.setCommunicationsStatusFilter(communicationFilter);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenStatusDeletedAndFilterPresent() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.DELETED);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    List<CommunicationStatusFilterType> communicationFilter = new ArrayList<CommunicationStatusFilterType>();
    communicationFilter.add(CommunicationStatusFilterType.LIVE);

    criteria.setCommunicationsStatusFilter(communicationFilter);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenStatusFilterIsLive() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.LIVE);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    List<CommunicationStatusFilterType> communicationFilter = new ArrayList<CommunicationStatusFilterType>();
    communicationFilter.add(CommunicationStatusFilterType.LIVE);

    criteria.setCommunicationsStatusFilter(communicationFilter);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenStatusFilterIsRecorded() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.RECORDED);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    List<CommunicationStatusFilterType> communicationFilter = new ArrayList<CommunicationStatusFilterType>();
    communicationFilter.add(CommunicationStatusFilterType.RECORDED);

    criteria.setCommunicationsStatusFilter(communicationFilter);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenStatusFilterIsUpcoming() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.UPCOMING);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    List<CommunicationStatusFilterType> communicationFilter = new ArrayList<CommunicationStatusFilterType>();
    communicationFilter.add(CommunicationStatusFilterType.UPCOMING);

    criteria.setCommunicationsStatusFilter(communicationFilter);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenMultipleCommunicationsAndStatusFilterMatches() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(2000));
    builder.withStatus(Status.UPCOMING);

    Long communicationId1 = addTemporaryCommunication(channelId, builder.build());
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    builder.withStatus(Status.LIVE);
    builder.withScheduledDateTime(new Date(1000));
    Long communicationId2 = addTemporaryCommunication(channelId, builder.build());

    builder.withStatus(Status.RECORDED);
    builder.withScheduledDateTime(new Date(500));
    Long communicationId3 = addTemporaryCommunication(channelId, builder.build());

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    List<CommunicationStatusFilterType> communicationFilter = new ArrayList<CommunicationStatusFilterType>();
    communicationFilter.add(CommunicationStatusFilterType.UPCOMING);
    communicationFilter.add(CommunicationStatusFilterType.LIVE);
    communicationFilter.add(CommunicationStatusFilterType.RECORDED);

    criteria.setCommunicationsStatusFilter(communicationFilter);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(3, result.size());
    assertEquals(communicationId1, result.get(0).getId());
    assertEquals(communicationId2, result.get(1).getId());
    assertEquals(communicationId3, result.get(2).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenMultipleCommunicationsAndStatusFilterNoNotMatch() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(2000));
    builder.withStatus(Status.UPCOMING);

    addTemporaryCommunication(channelId, builder.build());
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    builder.withStatus(Status.LIVE);
    builder.withScheduledDateTime(new Date(1000));
    Long communicationId = addTemporaryCommunication(channelId, builder.build());

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    List<CommunicationStatusFilterType> communicationFilter = new ArrayList<CommunicationStatusFilterType>();
    communicationFilter.add(CommunicationStatusFilterType.LIVE);

    criteria.setCommunicationsStatusFilter(communicationFilter);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenStatusProcessingAndFilterIsUpcoming() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withStatus(Status.PROCESSING);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    List<CommunicationStatusFilterType> communicationFilter = new ArrayList<CommunicationStatusFilterType>();
    communicationFilter.add(CommunicationStatusFilterType.UPCOMING);

    criteria.setCommunicationsStatusFilter(communicationFilter);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsSyndicatedOut() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.OUT);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSyndicatedInApprovedByBoth() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSyndicatedInApprovedByMasterOnly() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsSyndicatedInApprovedByConsumerOnly() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsSyndicatedInNotApprovedByBoth() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.DECLINED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsSyndicatedInPendingBoth() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsSyndicatedInPendingMaster() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsSyndicatedInPendingConsumer() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationConfigurationBuilder configurationBuilder = new CommunicationConfigurationBuilder().withDefaultValues();
    configurationBuilder.withSyndicationType(CommunicationSyndication.SyndicationType.IN);
    configurationBuilder.withConsumerChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.PENDING);
    configurationBuilder.withMasterChannelSyndicationAuthorisationStatus(CommunicationSyndication.SyndicationAuthorisationStatus.APPROVED);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId2);
    builder.withConfiguration(configurationBuilder.build());

    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsMultipleCommunicationsSameChannel() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(2000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(1000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(communicationId1, result.get(0).getId());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(communicationId2, result.get(1).getId());
    assertEquals(channelId, result.get(1).getChannelId());
  }

  @Test
  public void findSubscribedChannelsWebcastsMultipleCommunicationsDifferentChannels() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);
    Long channelId2 = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(2000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    builder.withChannelId(channelId2).withMasterChannelId(channelId2).withScheduledDateTime(new Date(1000));
    Communication communication2 = builder.build();
    Long communicationId2 = addTemporaryCommunication(channelId2, communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);
    addChannelSubscription(channelId2, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(communicationId1, result.get(0).getId());
    assertEquals(channelId, result.get(0).getChannelId());
    assertEquals(communicationId2, result.get(1).getId());
    assertEquals(channelId2, result.get(1).getChannelId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortDesc() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(communicationId2, result.get(0).getId());
    assertEquals(communicationId1, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortAsc() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    builder.withScheduledDateTime(new Date(2000));
    Long communicationId2 = addTemporaryCommunication(channelId, builder.build());

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(communicationId1, result.get(0).getId());
    assertEquals(communicationId2, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortByRatingDescWhenNoStats() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder communicationBuilder = new CommunicationBuilder().withDefaultValues();
    communicationBuilder.withChannelId(channelId).withMasterChannelId(channelId);

    Long communicationId = addTemporaryCommunication(channelId, communicationBuilder.build());

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.AVERAGERATING);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortByRatingDesc() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder communicationBuilder = new CommunicationBuilder().withDefaultValues();
    communicationBuilder.withChannelId(channelId).withMasterChannelId(channelId);

    Communication communication1 = communicationBuilder.build();
    Long communicationId1 = addTemporaryCommunication(channelId, communication1);

    CommunicationStatisticsBuilder statsBuilder = new CommunicationStatisticsBuilder().withDefaultValues();
    statsBuilder.withAverageRating(1).withCommunicationId(communicationId1);
    CommunicationStatistics stats1 = statsBuilder.build();
    communication1.setCommunicationStatistics(stats1);

    addCommunicationStatistics(communication1);

    Communication communication2 = communicationBuilder.build();

    Long communicationId2 = addTemporaryCommunication(channelId, communication2);
    statsBuilder.withAverageRating(5).withCommunicationId(communicationId2);
    CommunicationStatistics stats2 = statsBuilder.build();
    communication2.setCommunicationStatistics(stats2);

    addCommunicationStatistics(communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.AVERAGERATING);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());

    assertEquals(communicationId2, result.get(0).getId());
    assertEquals(communicationId1, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortByRatingAsc() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder communicationBuilder = new CommunicationBuilder().withDefaultValues();
    communicationBuilder.withChannelId(channelId).withMasterChannelId(channelId);

    Communication communication1 = communicationBuilder.build();
    Long communicationId1 = addTemporaryCommunication(channelId, communication1);

    CommunicationStatisticsBuilder statsBuilder = new CommunicationStatisticsBuilder().withDefaultValues();
    statsBuilder.withAverageRating(1).withCommunicationId(communicationId1);
    CommunicationStatistics stats1 = statsBuilder.build();
    communication1.setCommunicationStatistics(stats1);

    addCommunicationStatistics(communication1);

    Communication communication2 = communicationBuilder.build();

    Long communicationId2 = addTemporaryCommunication(channelId, communication2);
    statsBuilder.withAverageRating(5).withCommunicationId(communicationId2);
    CommunicationStatistics stats2 = statsBuilder.build();
    communication2.setCommunicationStatistics(stats2);

    addCommunicationStatistics(communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setSortBy(SortBy.AVERAGERATING);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());

    assertEquals(communicationId1, result.get(0).getId());
    assertEquals(communicationId2, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortByViewingsWhenNoStats() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.TOTALVIEWINGS);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortByViewingsAndSortDesc() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder communicationBuilder = new CommunicationBuilder().withDefaultValues();
    communicationBuilder.withChannelId(channelId).withMasterChannelId(channelId);

    Communication communication1 = communicationBuilder.build();
    Long communicationId1 = addTemporaryCommunication(channelId, communication1);

    CommunicationStatisticsBuilder statsBuilder = new CommunicationStatisticsBuilder().withDefaultValues();
    statsBuilder.withNumberOfViewings(10).withCommunicationId(communicationId1);
    CommunicationStatistics stats1 = statsBuilder.build();
    communication1.setCommunicationStatistics(stats1);

    addCommunicationStatistics(communication1);

    Communication communication2 = communicationBuilder.build();

    Long communicationId2 = addTemporaryCommunication(channelId, communication2);
    statsBuilder.withNumberOfViewings(15).withCommunicationId(communicationId2);
    CommunicationStatistics stats2 = statsBuilder.build();
    communication2.setCommunicationStatistics(stats2);

    addCommunicationStatistics(communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.TOTALVIEWINGS);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());

    assertEquals(communicationId2, result.get(0).getId());
    assertEquals(communicationId1, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortByViewingsAndSortAsc() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder communicationBuilder = new CommunicationBuilder().withDefaultValues();
    communicationBuilder.withChannelId(channelId).withMasterChannelId(channelId);

    Communication communication1 = communicationBuilder.build();
    Long communicationId1 = addTemporaryCommunication(channelId, communication1);

    CommunicationStatisticsBuilder statsBuilder = new CommunicationStatisticsBuilder().withDefaultValues();
    statsBuilder.withNumberOfViewings(10).withCommunicationId(communicationId1);
    CommunicationStatistics stats1 = statsBuilder.build();
    communication1.setCommunicationStatistics(stats1);

    addCommunicationStatistics(communication1);

    Communication communication2 = communicationBuilder.build();

    Long communicationId2 = addTemporaryCommunication(channelId, communication2);
    statsBuilder.withNumberOfViewings(15).withCommunicationId(communicationId2);
    CommunicationStatistics stats2 = statsBuilder.build();
    communication2.setCommunicationStatistics(stats2);

    addCommunicationStatistics(communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortOrder(SortOrder.ASC);
    criteria.setSortBy(SortBy.TOTALVIEWINGS);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());

    assertEquals(communicationId1, result.get(0).getId());
    assertEquals(communicationId2, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortDescPageOneSizeOne() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(1);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(communicationId2, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortDescPageOneAndSizeNotDefinded() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(null);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(communicationId2, result.get(0).getId());
    assertEquals(communicationId, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortDescPageOneSizeOneHasMore() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    addTemporaryCommunication(channelId, communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(1);
    criteria.setPageSize(1);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertTrue(result.hasNextPage());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortDescPageTwoSizeOne() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    addTemporaryCommunication(channelId, communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(2);
    criteria.setPageSize(1);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(communicationId1, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortDescPageTwoAndSizeNotDefinded() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    Long communicationId1 = addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    Long communicationId2 = addTemporaryCommunication(channelId, communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(2);
    criteria.setPageSize(null);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertNotNull(result);
    assertEquals(2, result.size());
    assertEquals(communicationId2, result.get(0).getId());
    assertEquals(communicationId1, result.get(1).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsSortDescPageTwoSizeOneHasMore() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    builder.withScheduledDateTime(new Date(1000));
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);

    Communication communication2 = builder.withScheduledDateTime(new Date(2000)).build();
    addTemporaryCommunication(channelId, communication2);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(2);
    criteria.setPageSize(1);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertFalse(result.hasNextPage());
  }

  @Test
  public void findSubscribedChannelsWebcastsWhenChannelSubscriptionDeleted() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, false);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsToWhichUserIsRegisteredWhenNoRegistration() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setRegistered(true);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsToWhichUserIsRegisteredUserRegistrationRemoved() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    addTemporaryCommunication(channelId, communication);
    addCommunicationRegistration(SUBSCRIBER_ID, communication, false);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setRegistered(true);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(0, result.size());
  }

  @Test
  public void findSubscribedChannelsWebcastsToWhichUserIsRegistered() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);
    addCommunicationRegistration(SUBSCRIBER_ID, communication, true);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setRegistered(true);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }

  @Test
  public void findSubscribedChannelsWebcastsToWhichUserIsNotRegisteredWhichIsNotSupported() {

    Long defaultChannelTypeId = super.addTemporaryChannelType(DEFAULT_CHANNEL_TYPE, true, null);
    Long channelId = addTemporaryChannel(defaultChannelTypeId, defaultChannelTypeId, 99L, true);

    CommunicationBuilder builder = new CommunicationBuilder().withDefaultValues();
    builder.withChannelId(channelId).withMasterChannelId(channelId);
    Communication communication = builder.build();

    Long communicationId = addTemporaryCommunication(channelId, communication);

    addChannelSubscription(channelId, SUBSCRIBER_ID, true);

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(DEFAULT_PAGE_NUMBER);
    criteria.setPageSize(DEFAULT_PAGE_SIZE);
    criteria.setSortBy(SortBy.SCHEDULEDDATE);
    criteria.setSortOrder(SortOrder.DESC);
    criteria.setRegistered(false);

    // do test
    PaginatedList<Communication> result = uut.findPaginatedSubscribedChannelsCommunications(SUBSCRIBER_ID, criteria);

    assertEquals(1, result.size());
    assertEquals(communicationId, result.get(0).getId());
  }
}