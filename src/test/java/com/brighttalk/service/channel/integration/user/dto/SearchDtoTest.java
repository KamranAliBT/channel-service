/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $$Id: SearchDtoTest.java 101273 2015-10-09 09:30:23Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.user.dto;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

/**
 * Tests the {@link SearchDto} class. Normally one would not test a DTO class but in this instance there is some domain
 * specific logic that does not belong in a business layer (otherwise we would be forcing DTO-specific rules to business
 * -level services which means new services may not necessarily respect these rules).
 *
 * The remaining getters and setters are untested.
 */
public class SearchDtoTest {

  private SearchDto searchDto;

  @Before
  public void setUp() throws Exception {
    searchDto = new SearchDto();
  }

  /**
   * Tests that {@link SearchDto#setUserIds(List)} with a populated list results in the id value holding its values in
   * comma separated form
   *
   * @throws Exception
   */
  @Test
  public void testSetIdWithPopulatedListHasCommaSeparatedValues() throws Exception {
    List<Long> idList = new ArrayList<>();
    idList.add(123l);
    idList.add(456l);
    searchDto.setUserIds(idList);
    assertEquals("123,456,", searchDto.getUserIds());
  }

  /**
   * Tests that {@link SearchDto#setEmail(List)} with a populated list results in the email value holding its values in
   * comma separated form
   *
   * @throws Exception
   */
  @Test
  public void testSetEmailWithPopulatedListHasCommaSeparatedValues() throws Exception {
    List<String> emailsList = new LinkedList<>();
    emailsList.add("someone@brighttalk.com");
    emailsList.add("someoneelse@brighttalk.com, ,another@brighttalk.com,");
    searchDto.setEmail(emailsList);
    assertEquals("someone@brighttalk.com,someoneelse@brighttalk.com,another@brighttalk.com,", searchDto.getEmail());
  }

  /**
   * Tests that {@link SearchDto#setEmail(List)} with a populated list containing some empty values
   *
   * @throws Exception
   */
  @Test
  public void testSetEmailWithPopulatedPartiallyEmptyValueListHasCommaSeparatedValues() throws Exception {
    List<String> emailsList = new LinkedList<>();
    emailsList.add("someone@brighttalk.com");
    emailsList.add("");
    emailsList.add(" ");
    emailsList.add("someoneelse@brighttalk.com");
    searchDto.setEmail(emailsList);
    assertEquals("someone@brighttalk.com,someoneelse@brighttalk.com,", searchDto.getEmail());
  }

  /**
   * Tests that {@link SearchDto#setEmail(List)} with a populated list containing some empty values
   *
   * @throws Exception
   */
  @Test
  public void testSetEmailWithBlankEmailListIsNull() throws Exception {
    List<String> emailsList = new LinkedList<>();
    emailsList.add("");
    emailsList.add(" ");
    searchDto.setEmail(emailsList);
    assertThat(searchDto.getEmail(), is(nullValue()));
  }

  /**
   * Tests that {@link SearchDto#setEmail(List)} with an empty list results in the email value holding a null rather
   * than an empty string
   *
   * @throws Exception
   */
  @Test
  public void testSetEmailWithEmptyListKeepsEmailAsNull() throws Exception {
    searchDto.setEmail(new LinkedList<String>());
    assertThat(searchDto.getEmail(), is(nullValue()));
  }

  /**
   * Tests that {@link SearchDto#setEmail(List)} with a list containing a single comma keeps the email as null
   *
   * @throws Exception
   */
  @Test
  public void testSetEmailWithListOfCommaKeepsEmailAsNull() throws Exception {
    LinkedList<String> emails = new LinkedList<>();
    emails.add(" ,");
    searchDto.setEmail(emails);
    assertThat(searchDto.getEmail(), is(nullValue()));
  }

  /**
   * Tests that {@link SearchDto#setEmail(List)} with a list containing a single comma item and a valid email sets the
   * email only
   *
   * @throws Exception
   */
  @Test
  public void testSetEmailWithListOfCommaAndValidEmailSetsEmailOnly() throws Exception {
    LinkedList<String> emails = new LinkedList<>();
    emails.add(" ,");
    emails.add("someone@brighttalk.com");
    searchDto.setEmail(emails);
    assertEquals("someone@brighttalk.com,", searchDto.getEmail());
  }

  /**
   * Tests that {@link SearchDto#setEmail(List)} with a list containing a single comma item and multiple valid email
   * sets the emails only
   *
   * @throws Exception
   */
  @Test
  public void testSetEmailWithListOfCommaAndValidEmailsSetsEmailsOnly() throws Exception {
    LinkedList<String> emails = new LinkedList<>();
    emails.add(" ,");
    emails.add("someone@brighttalk.com");
    emails.add("someoneelse@brighttalk.com");
    searchDto.setEmail(emails);
    assertEquals("someone@brighttalk.com,someoneelse@brighttalk.com,", searchDto.getEmail());
  }

  /**
   * Tests that {@link SearchDto#setEmail(List)} with a null list results in the email value holding a null rather
   * than an empty string and does not throw any exceptions
   *
   * @throws Exception
   */
  @Test
  public void testSetEmailWithNullListKeepsEmailAsNull() throws Exception {
    searchDto.setEmail((List) null);
    assertThat(searchDto.getEmail(), is(nullValue()));
  }

  /**
   * Tests that {@link SearchDto#setUserIds(List)} with an empty list does results in the email value holding a null
   * rather than an empty string
   *
   * @throws Exception
   */
  @Test
  public void testSetIdWithEmptyListKeepsIdAsNull() throws Exception {
    searchDto.setUserIds(new LinkedList<Long>());
    assertThat(searchDto.getUserIds(), is(nullValue()));
  }

  /**
   * Tests that {@link SearchDto#setUserIds(List)} with a null list does results in the email value holding a null
   * rather than an empty string, and does not throw any exceptions
   *
   * @throws Exception
   */
  @Test
  public void testSetIdWithNullListKeepsIdAsNull() throws Exception {
    searchDto.setUserIds((List) null);
    assertThat(searchDto.getUserIds(), is(nullValue()));
  }

  /**
   * Tests that {@link SearchDto#hasEmail()} returns false if a null list of emails is set
   *
   * @throws Exception
   */
  @Test
  public void testHasEmailWithNullReturnsFalse() throws Exception {
    searchDto.setEmail((List) null);
    assertFalse(searchDto.hasEmail());
  }

  /**
   * Tests that {@link SearchDto#hasEmail()} returns false if a list of null emails is set
   *
   * @throws Exception
   */
  @Test
  public void testHasEmailWithNullListElementsReturnsFalse() throws Exception {
    ArrayList<String> emails = new ArrayList<>();
    emails.add(null);
    searchDto.setEmail(emails);
    assertFalse(searchDto.hasEmail());
  }

  /**
   * Tests that {@link SearchDto#hasEmail()} returns false if an empty list of emails is set
   *
   * @throws Exception
   */
  @Test
  public void testHasEmailWithEmptyListReturnsFalse() throws Exception {
    ArrayList<String> emails = new ArrayList<>();
    searchDto.setEmail(emails);
    assertFalse(searchDto.hasEmail());
  }

  /**
   * Tests that {@link SearchDto#hasEmail()} returns true if a list of emails is set
   *
   * @throws Exception
   */
  @Test
  public void testHasEmailWithEmptyListReturnsTrue() throws Exception {
    searchDto.setEmail("one@brighttalk.com,two@brighttalk.com");
    assertTrue(searchDto.hasEmail());
  }

  /**
   * Tests that {@link SearchDto#hasUserIds()} returns false if a null list of user ids is set
   *
   * @throws Exception
   */
  @Test
  public void testHasUserIdsWithNullReturnsFalse() throws Exception {
    searchDto.setUserIds((List) null);
    assertFalse(searchDto.hasUserIds());
  }

  /**
   * Tests that {@link SearchDto#hasUserIds()} returns false if a list of null user ids is set
   *
   * @throws Exception
   */
  @Test
  public void testHasUserIdsWithNullListElementsReturnsFalse() throws Exception {
    ArrayList<Long> ids = new ArrayList<>();
    ids.add(null);
    searchDto.setUserIds(ids);
    assertFalse(searchDto.hasUserIds());
  }

  /**
   * Tests that {@link SearchDto#hasUserIds()} returns false if an empty list of user ids is set
   *
   * @throws Exception
   */
  @Test
  public void testHasUserIdsWithEmptyListReturnsFalse() throws Exception {
    ArrayList<Long> ids = new ArrayList<>();
    searchDto.setUserIds(ids);
    assertFalse(searchDto.hasUserIds());
  }

  /**
   * Tests that {@link SearchDto#hasUserIds()} returns true if a csv of user ids is set
   *
   * @throws Exception
   */
  @Test
  public void testHasUserIdsWithCSVListReturnsTrue() throws Exception {
    searchDto.setUserIds("123,456");
    assertTrue(searchDto.hasUserIds());
  }

}