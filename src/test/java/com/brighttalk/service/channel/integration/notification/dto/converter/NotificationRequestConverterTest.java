/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: LiveEventRequestConverterTest.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.notification.dto.converter;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.brighttalk.service.channel.business.domain.builder.CommunicationBuilder;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.notification.dto.EventDto;

public class NotificationRequestConverterTest {

  private NotificationRequestConverter uut;

  @Before
  public void setUp() {

    uut = new NotificationRequestConverter();
  }

  @Test
  public void convertForCancel() {
    // Set up
    Long communicationId = 123L;

    Communication communication = new CommunicationBuilder().withDefaultValues().withId(communicationId).build();

    // Expectations

    // Excercise
    EventDto eventDto = uut.convert(communication);

    // Assertions
    Assert.assertEquals(eventDto.getCommunicationId(), communicationId);
    Assert.assertEquals(eventDto.getType(), EventDto.TYPE_LIVE_END);
  }

}