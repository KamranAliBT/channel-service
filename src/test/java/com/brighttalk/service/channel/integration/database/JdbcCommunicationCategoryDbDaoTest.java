/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationCategoryDbDaoTest.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;

/**
 * Tests {@link JdbcCommunicationCategoryDbDao}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-app-config.xml")
public class JdbcCommunicationCategoryDbDaoTest extends AbstractJdbcDbDaoTest {
  
  private static final long TEST_CHANNEL_ID = 9999;
  
  @Autowired
  private JdbcCommunicationCategoryDbDao uut;
  
  @Autowired
  private JdbcCommunicationDbDao communicationDbDao;
  

  /**
   * Tests {@link JdbcCommunicationCategoryDbDao#create()} in case of successfully creating communication category.
   */
  @Test
  public final void testCreateCommunicationCategory() {
    //setup
    Long commId = this.addTemporaryCommunication(TEST_CHANNEL_ID, "brighttalkhd");
    CommunicationCategory category = new CommunicationCategory(new Long(1), TEST_CHANNEL_ID, "Test category",true);
    category.setCommunicationId(commId);
    
    // do test
    this.uut.create(category);
    
    // Assert the communication category created the id is not null.
    assertNotNull(category.getId());
    
    this.temporaryCommunicationIds.add(commId); 
  }
}
