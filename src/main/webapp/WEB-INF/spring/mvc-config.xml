<?xml version="1.0" encoding="UTF-8"?>
<!-- 
  * **************************************************************************** 
  * Copyright BrightTALK Ltd, 2012. 
  * All Rights Reserved. 
  * $Id: mvc-config.xml 99413 2015-08-20 13:53:49Z ssarraj $ 
  * **************************************************************************** 
-->
<beans xmlns="http://www.springframework.org/schema/beans" xmlns:context="http://www.springframework.org/schema/context"
  xmlns:mvc="http://www.springframework.org/schema/mvc" xmlns:util="http://www.springframework.org/schema/util"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.2.xsd
       http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.2.xsd
       http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-3.2.xsd       
       http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-3.2.xsd">


  
  
   <!-- ===================================================================== -->
  <!-- HandlerAdapters -->
  <!-- ===================================================================== -->
  <!-- Custom annnotation resovler - This is used to resolve the annotations, running the necessary code -->
  <bean id="customUserAnnotationResovler" class="com.brighttalk.common.web.annotation.AuthenticationAnnontationResolver" />
  
   <!-- Custom annnotation resovler - This is used to resolve the annotations, running the necessary code -->
  <bean id="customTimeAnnotationResovler" class="com.brighttalk.common.web.annotation.DateTimeAnnotationResolver" />

  <!-- HandlerAdapter responsible for processing annotated handler methods in web Controller. Overrides default one configured 
    for DispatcherServlet in order to configure it with custom set of HttpMessageConverter -->
  <bean id="annotationMethodHandlerAdapter" class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter">

    <!-- Custom Annotaion Resovlers - used here to do Authorisation and authentication of users in the handlers and time related formatting -->
    <property name="customArgumentResolvers">
    	<util:list>
        	<ref bean="customUserAnnotationResovler" />
        	<ref bean="customTimeAnnotationResovler" />
        </util:list>
    </property>
  
    <!-- List of Spring HttpMessageConverter available for converting HTTP request and response bodies to/from Handler method 
      param object. Overrides the default set provided by Spring, for default AnnotationMethodHandlerAdapter for the following 
      reasons: 1) The required (XML) MarshallingHttpMessageConverter is not configured by default unless using JAXB-based marshaling. 
      2) Limits what formats are supported & used when writing responses for requests which don't specify an Accept header set 
      (or accept all media types) -->
    <property name="messageConverters">
      <util:list>
        <ref bean="apiServerMarshallingXmlHttpMsgConverter" />
        <ref bean="apiServerMarshallingAtomHttpMsgConverter" />
        <ref bean="apiServerMarshallingRssHttpMsgConverter" />
        <ref bean="apiServerMarshallingJsonHttpMsgConverter" />
        <ref bean="apiServerMarshallingTextHttpMsgConverter" />
      </util:list>
    </property>
  </bean>
  
  
  <!-- ===================================================================== -->
  <!-- Handler Interceptors (Spring filters for presentation layer Handlers) -->
  <!-- ===================================================================== -->
  <!-- Configure Interceptors that apply to all HandlerAdapters -->
  <mvc:interceptors>
    <bean class="org.springframework.web.servlet.mvc.WebContentInterceptor">
      <!-- Disable client-side caching of responses for all API calls -->
      <property name="cacheSeconds" value="0" />
      <property name="cacheMappings">
        <props>
          <!-- Public channel view is cached for 60 seconds. Ex. /channel/87394/public -->
          <prop key="/channel/*/public">60</prop>
          <!-- Channel atom feed is cached for 60 seconds. Ex. /channel/87394/feed -->
          <prop key="/channel/*/feed">60</prop>
          <!-- Channel rss feed is cached for 60 seconds. Ex. /channel/87394/feed -->
          <prop key="/channel/*/feed/rss">60</prop>
          <!-- Channel atom feed is cached for 60 seconds. Ex. /xml/feed/87394 -->
          <prop key="/xml/feed/*">60</prop>
          <!-- Channel rss feed is cached for 60 seconds. Ex. /xml/feedrss/87394 -->
          <prop key="/xml/feedrss/*">60</prop>
          <!-- Public channels view is cached for 60 seconds. Ex. /channels/public?ids=5005 -->
          <prop key="/channels/public*">60</prop>
          <!-- Public webcasts view is cached for 60 seconds. Ex. /webcasts/public?ids=5005 -->
          <prop key="/webcasts/public*">60</prop>
        </props>
       </property>
    </bean>

    <bean class="org.springframework.web.context.request.Log4jNestedDiagnosticContextInterceptor">
      <property name="includeClientInfo" value="true" />
    </bean>
 
    <!-- SessionKeepAliveInterceptor --> 
    <bean class="com.brighttalk.common.web.sessionkeepalive.SessionKeepAliveInterceptor">
      <property name="minimumUpdateCallOffset" value="300" />
      <property name="excludedPathPatterns">
        <util:list>
          <value>^/internal/(.*)</value>
          <!-- Exclude public channel view. Ex. /channel/87394/public -->
          <value>^/channel/(\d*)/public$</value>
          <!-- Exclude channel atom feed. Ex. /channel/87394/feed -->
          <value>^/channel/(\d*)/feed$</value>
          <!-- Exclude channel rss feed. Ex. /channel/87394/feed/rss-->
          <value>^/channel/(\d*)/feed/rss$</value>
          <!-- Exclude channel atom feed. Ex. /xml/feedrss/87394 -->
          <value>^/xml/feed/(\d*)$</value>
          <!-- Exclude channel rss feed. Ex. /xml/feedrss/87394-->
          <value>^/xml/feedrss/(\d*)$</value>
          <!-- Exclude channels public  Ex. /channels/public?ids=5001-->
          <value>^/channels/public(.*)$</value>
          <!-- Exclude webcasts public  Ex. /webcasts/public?ids=5001-60606-->
          <value>^/webcasts/public(.*)$</value>
        </util:list>
      </property>
    </bean>

    <!-- VaryHeaderInterceptor --> 
    <mvc:interceptor>
      <mvc:mapping path="/channel/*/public" />
      <mvc:mapping path="/channel/*/feed" />
      <mvc:mapping path="/channel/*/feed/rss" />
      <mvc:mapping path="/xml/feed/*" />
      <mvc:mapping path="/xml/feedrss/*" />
      <mvc:mapping path="/channels/public*" />
      <mvc:mapping path="/webcasts/public*" />
      
      <bean class="com.brighttalk.common.web.interceptor.ResponseHeaderSetterInterceptor">
        <property name="headerName" value="Vary" />
        <property name="headerValue" value="Accept, BrightTALK-API-Version" />
      </bean>
      
    </mvc:interceptor>
    
    <!-- Add Api version interceptor. This interceptor ensues that API version was supplied in the request. -->
    <mvc:interceptor>
        <mvc:mapping path="/channels/public"/>
        <bean class="com.brighttalk.common.web.versioning.interceptor.ApiVersionInterceptor" />
        
    </mvc:interceptor>
    
    <mvc:interceptor>
      <mvc:mapping path="/channel/*/features" />
      <bean class="com.brighttalk.service.channel.presentation.security.BasicAuthorizationHandlerInterceptor" />
    </mvc:interceptor>
    
  </mvc:interceptors>

  <!-- Sets user session cookie generator with set of environment specific properties.-->
  <bean id="sessionCookieGenerator" class="org.springframework.web.util.CookieGenerator" scope="prototype">
    <description>Config and management of the BrightTALK user session cookie</description>
    <property name="cookieName" value="${session.cookieName}" />
    <property name="cookieDomain" value="${session.cookieDomain}" />
    <property name="cookiePath" value="${session.cookiePath}" />
    <property name="cookieMaxAge" value="${session.cookieMaxAge}" />
    <property name="cookieSecure" value="${session.cookieSecure}" />
  </bean>

  <!-- ===================================================================== -->
  <!-- HTTP Message Converters (convert request/response bodies to/from objs) -->
  <!-- ===================================================================== -->
  <!-- XML Message Converter - use Jaxb to conver the DTO's to XML -->
  <bean id="apiServerMarshallingXmlHttpMsgConverter" class="org.springframework.http.converter.xml.MarshallingHttpMessageConverter">
    <qualifier value="default-converter" />
    <property name="marshaller" ref="apiServerJaxbMarshaller" />
    <property name="unmarshaller" ref="apiServerJaxbMarshaller" />
  </bean>

  <bean id="apiServerMarshallingAtomHttpMsgConverter" class="org.springframework.http.converter.xml.MarshallingHttpMessageConverter">
    <qualifier value="atom-converter" />
    <property name="marshaller" ref="atomJaxbMarshaller" />
    <property name="unmarshaller" ref="atomJaxbMarshaller" />
  </bean>

  <bean id="apiServerMarshallingRssHttpMsgConverter" class="org.springframework.http.converter.xml.MarshallingHttpMessageConverter">
    <qualifier value="rss-converter" />
    <property name="marshaller" ref="rssJaxbMarshaller" />
    <property name="unmarshaller" ref="rssJaxbMarshaller" />
  </bean>

  <!-- Enable a Json Message Converter so we can marshal to / from json as well -->
  <bean id="apiServerMarshallingJsonHttpMsgConverter" class="org.springframework.http.converter.json.MappingJacksonHttpMessageConverter">
    <property name="objectMapper" ref="jacksonObjectMapper" />
  </bean>

  <!-- Enable a Text Message Converter so we can marshal to / from text as well -->
  <bean id="apiServerMarshallingTextHttpMsgConverter" class="org.springframework.http.converter.StringHttpMessageConverter">
    <property name="supportedMediaTypes" value="text/plain;charset=UTF-8" />
    <property name="writeAcceptCharset" value="false" />    
  </bean>

  <!-- ===================================================================== -->
  <!-- HandlerExceptionResolver -->
  <!-- ===================================================================== -->
  <!-- Defines BrightTALK REST API resolver -->
  <bean class="com.brighttalk.common.web.RestApiHandlerExceptionResolver">
    <property name="httpMessageConverters">
      <util:list>
          <ref bean="apiServerMarshallingXmlHttpMsgConverter" />
          <ref bean="apiServerMarshallingAtomHttpMsgConverter" />
          <ref bean="apiServerMarshallingRssHttpMsgConverter" />
          <ref bean="apiServerMarshallingJsonHttpMsgConverter" />
	      <ref bean="apiServerMarshallingTextHttpMsgConverter" />
      </util:list>
    </property>
    
    <property name="messageSource" ref="messageSource" />
    <property name="httpResponseStatusCodeMap">
      <util:map>
        <!-- BrightTALK Exceptions -->
        <entry key="com.brighttalk.service.channel.presentation.error.BadRequestException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
        <entry key="com.brighttalk.service.channel.presentation.error.ResourceNotFoundException" value="#{T(org.springframework.http.HttpStatus).NOT_FOUND.value()}" />
        <entry key="com.brighttalk.service.channel.presentation.error.NotAuthorisedException" value="#{T(org.springframework.http.HttpStatus).FORBIDDEN.value()}" />
        <entry key="com.brighttalk.service.channel.presentation.error.NotAuthenticatedException" value="#{T(org.springframework.http.HttpStatus).UNAUTHORIZED.value()}" />
        <entry key="com.brighttalk.common.user.error.UserAuthenticationException" value="#{T(org.springframework.http.HttpStatus).UNAUTHORIZED.value()}" />
        <entry key="com.brighttalk.common.user.error.UserAuthorisationException" value="#{T(org.springframework.http.HttpStatus).FORBIDDEN.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.InvalidResourceException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.NotFoundException" value="#{T(org.springframework.http.HttpStatus).NOT_FOUND.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.SurveyNotFoundException" value="#{T(org.springframework.http.HttpStatus).NOT_FOUND.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.MustBeInMasterChannelException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST .value()}" />
        <entry key="com.brighttalk.service.channel.business.error.CommunicationStatusNotSupportedException" value="#{T(org.springframework.http.HttpStatus).CONFLICT.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.CannotUpdateCurrentProviderException" value="#{T(org.springframework.http.HttpStatus).CONFLICT.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.CommunicationOutsideAuthoringPeriodException" value="#{T(org.springframework.http.HttpStatus).CONFLICT.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.FeatureNotEnabledException" value="#{T(org.springframework.http.HttpStatus).CONFLICT.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.DataCleanUpFailedException" value="#{T(org.springframework.http.HttpStatus).INTERNAL_SERVER_ERROR.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.DialledInTooEarlyException" value="#{T(org.springframework.http.HttpStatus).FORBIDDEN.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.DialledInTooLateException" value="#{T(org.springframework.http.HttpStatus).FORBIDDEN.value()}" />
        <entry key="com.brighttalk.service.channel.business.error.ContentPlanException" value="#{T(org.springframework.http.HttpStatus).FORBIDDEN.value()}" />
        
       <entry key="com.brighttalk.common.error.ApplicationException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
      
        <!-- Spring Framework Exceptions - Replicates the mapping of Spring exceptions to HTTP status codes hard-coded in 
          Spring 3.1.x's org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver -->
      
        <!-- JSON parsing exceptions thrown when provided json attributes are not of the right type.
             For example: webcast id value is provide as string when Dto expects numeric value.-->
        <entry key="org.codehaus.jackson.map.JsonMappingException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
        <entry key="org.codehaus.jackson.map.exc.UnrecognizedPropertyException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
       
        <!-- JAXB parsing exception thrown when provided xml numeric attributes are not of the right type.  
        For example: webcast id value provided as string when expected is numeric value.-->
        <entry key="java.lang.NumberFormatException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
     
        <!-- Exception thrown when one parameters passed to the controller have invalid type. The exception is translated to Client error. -->
        <entry key="org.springframework.web.bind.annotation.support.HandlerMethodInvocationException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
        <entry key="org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException" value="#{T(org.springframework.http.HttpStatus).NOT_FOUND.value()}" />
        <entry key="org.springframework.web.HttpRequestMethodNotSupportedException" value="#{T(org.springframework.http.HttpStatus).METHOD_NOT_ALLOWED.value()}" />
        <entry key="org.springframework.web.HttpMediaTypeNotSupportedException" value="#{T(org.springframework.http.HttpStatus).UNSUPPORTED_MEDIA_TYPE.value()}" />
        <entry key="org.springframework.web.HttpMediaTypeNotAcceptableException" value="#{T(org.springframework.http.HttpStatus).NOT_ACCEPTABLE.value()}" />
        <entry key="org.springframework.web.bind.MissingServletRequestParameterException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
        <entry key="org.springframework.beans.ConversionNotSupportedException" value="#{T(org.springframework.http.HttpStatus).INTERNAL_SERVER_ERROR.value()}" />
        <entry key="org.springframework.beans.TypeMismatchException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
        <entry key="org.springframework.http.converter.HttpMessageNotReadableException" value="#{T(org.springframework.http.HttpStatus).BAD_REQUEST.value()}" />
        <entry key="org.springframework.http.converter.HttpMessageNotWritableException" value="#{T(org.springframework.http.HttpStatus).INTERNAL_SERVER_ERROR.value()}" />
      </util:map>
    </property>
    <!--  Map of suppressed exceptions.-->
    <property name="ignoredExceptionsMap">
      <util:map>
        
        <!-- The marshalling exception are related to broken connection between client and server, therefore they can be safely 
          suppressed and removed from error log. -->
        <entry key="org.springframework.oxm.MarshallingFailureException" value="(.*)java.net.SocketException: [(Connection reset)|(Broken pipe)](.*)" />
      </util:map>
    </property>
  </bean>
  
  <!-- ===================================================================== -->
  <!-- Default Spring MVC infrastructure beans -->
  <!-- ===================================================================== -->
  <!-- Registers implementations of HandlerMapping and HandlerAdapter beans required for dispatching requests to @Controllers. 
    The beans are configured with defaults based on services available on classpath. Note - Configured as the last element to 
    allow its actions to be overridden -->
  <mvc:annotation-driven />

</beans>