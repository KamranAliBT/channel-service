<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: jms-config.xml 98710 2015-08-03 09:52:40Z ikerbib $
 * ****************************************************************************
-->
<!-- Configuration of Spring managed objects relating to JMS messaging -->
<beans xmlns="http://www.springframework.org/schema/beans" 
       xmlns:jee="http://www.springframework.org/schema/jee"
       xmlns:jms="http://www.springframework.org/schema/jms"              
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"       
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.2.xsd       
       http://www.springframework.org/schema/jee http://www.springframework.org/schema/jee/spring-jee-3.2.xsd
       http://www.springframework.org/schema/jms  http://www.springframework.org/schema/jms/spring-jms-3.2.xsd">
  
  <!-- Strategy for resolving JMS Destination names configured for instances of JMSTemplate and MessageListenerContainer 
       beans -->
  <bean id="jndiDestinationResolver" class="org.springframework.jms.support.destination.JndiDestinationResolver" />  

  <!-- General purpose (generically configured) instance of Spring JmsTemplate which can be re-used when there are no 
       use-case specific configuration requirements. -->
  <bean id="jmsTemplate" class="org.springframework.jms.core.JmsTemplate">
    <qualifier value="generic" />
    <!-- Setting this flag to "true" will use a short local JMS transaction when running outside of a managed transaction, 
    	and a synchronized local JMS transaction in case of a managed transaction (other than an XA transaction) being present. 
    	The latter has the effect of a local JMS transaction being managed alongside the main transaction (which might be a native JDBC transaction), 
    	with the JMS transaction committing right after the main transaction. 
    	See: http://docs.spring.io/spring/docs/3.1.x/javadoc-api/org/springframework/jms/support/JmsAccessor.html#setSessionTransacted(boolean)
    -->
    <property name="sessionTransacted" value="true" />
    <property name="sessionAcknowledgeMode" value="#{T(javax.jms.Session).CLIENT_ACKNOWLEDGE}" />
    <property name="connectionFactory" ref="jmsConnectionFactory" />
    <!-- Add support for resolving destination names to JNDI managed destinations (acts as a service locator, performing 
         JNDI lookup) -->
    <property name="destinationResolver" ref="jndiDestinationResolver" />
  </bean>
  
  <bean id="createChannelJmsListener" class="com.brighttalk.service.channel.business.queue.JmsCreateChannelListener" />
  <bean id="createAuditRecordJmsListener" class="com.brighttalk.service.channel.business.queue.JmsCreateAuditRecordListener" />
  <bean id="deleteChannelJmsListener" class="com.brighttalk.service.channel.business.queue.JmsDeleteChannelListener" />
  
  <bean id="communityDeleteJmsListener" class="com.brighttalk.service.channel.business.queue.JmsCommunityDeleteListener" />
  
  <bean id="emailCreateJmsListener" class="com.brighttalk.service.channel.business.queue.JmsEmailCreateListener" />
  <bean id="emailRescheduleJmsListener" class="com.brighttalk.service.channel.business.queue.JmsEmailRescheduleListener" />
  <bean id="emailCancelJmsListener" class="com.brighttalk.service.channel.business.queue.JmsEmailCancelListener" />
  <bean id="emailMissedYouJmsListener" class="com.brighttalk.service.channel.business.queue.JmsEmailMissedYouListener" />
  <bean id="emailRecordingPublishedJmsListener" class="com.brighttalk.service.channel.business.queue.JmsEmailRecordingPublishedListener" />
  <bean id="emailVideoUploadCompleteJmsListener" class="com.brighttalk.service.channel.business.queue.JmsEmailVideoUploadCompleteListener" />
  <bean id="emailVideoUploadErrorJmsListener" class="com.brighttalk.service.channel.business.queue.JmsEmailVideoUploadErrorListener" />
  <bean id="emailVideoUploadHDCompleteJmsListener" class="com.brighttalk.service.channel.business.queue.JmsEmailVideoUploadHDCompleteListener" >
   <qualifier value="hdcommunicationupdateservice" />
  </bean> 
  <bean id="emailVideoUploadHDErrorJmsListener" class="com.brighttalk.service.channel.business.queue.JmsEmailVideoUploadHDErrorListener" >
   <qualifier value="hdcommunicationupdateservice" />
  </bean> 
  
  <bean id="imageConverterInformJmsListener" class="com.brighttalk.service.channel.business.queue.JmsImageConverterInformListener" />
  
  <bean id="lesRescheduleJmsListener" class="com.brighttalk.service.channel.business.queue.JmsLesRescheduleListener" />
  <bean id="lesVideoUploadCompleteJmsListener" class="com.brighttalk.service.channel.business.queue.JmsLesVideoUploadCompleteListener">
   <qualifier value="hdcommunicationupdateservice" />
  </bean> 
  <bean id="lesVideoUploadErrorJmsListener" class="com.brighttalk.service.channel.business.queue.JmsLesVideoUploadErrorListener" > 
   <qualifier value="hdcommunicationupdateservice" />
  </bean> 
  <bean id="lesCancelJmsListener" class="com.brighttalk.service.channel.business.queue.JmsLesCancelListener" />
  
  <bean id="notificationInformJmsListener" class="com.brighttalk.service.channel.business.queue.JmsNotificationInformListener" />
  
  <bean id="summitRescheduleJmsListener" class="com.brighttalk.service.channel.business.queue.JmsSummitRescheduleListener" />
  <bean id="summitDeleteJmsListener" class="com.brighttalk.service.channel.business.queue.JmsSummitDeleteListener" />
  
  <!-- Spring Message Listener Container - serves a similar purpose to an MDB, but can be configured with any no. of 
       different MessageListeners, and doesn't require an EJB container. Facilitates the use of POJO-based JMS 
       MessageListeners. 
       General purpose container which can be re-used when the generic configuration is acceptable. -->       
  <jms:listener-container connection-factory="jmsConnectionFactory" destination-resolver="jndiDestinationResolver" 
    acknowledge="auto" concurrency="1" error-handler="msgListenerErrorHandler">    
    <jms:listener destination="${jms.deleteChannelDestination}" ref="deleteChannelJmsListener" method="onMessage" />    
  </jms:listener-container>

  <!-- Spring Message Listener Container - serves a similar purpose to an MDB, but can be configured with any no. of 
       different MessageListeners, and doesn't require an EJB container. Facilitates the use of POJO-based JMS 
       MessageListeners. 
       General purpose container which can be re-used when the generic configuration is acceptable. -->       
  <jms:listener-container connection-factory="jmsConnectionFactory" destination-resolver="jndiDestinationResolver" 
    acknowledge="auto" concurrency="1" error-handler="msgListenerErrorHandler">    
    <jms:listener destination="${jms.createChannelDestination}" ref="createChannelJmsListener" method="onMessage" />    
  </jms:listener-container>
  
  <!-- Spring Message Listener Container For Audit Service. -->
  <jms:listener-container connection-factory="jmsConnectionFactory" destination-resolver="jndiDestinationResolver" 
    acknowledge="auto" concurrency="1" error-handler="msgListenerErrorHandler">    
    <jms:listener destination="${jms.createAuditRecordDestination}" ref="createAuditRecordJmsListener" method="onMessage" />    
  </jms:listener-container>
    
  <!-- Spring Message Listener Container For Community Service. -->
  <jms:listener-container connection-factory="jmsConnectionFactory" destination-resolver="jndiDestinationResolver" 
    acknowledge="auto" concurrency="1" error-handler="msgListenerErrorHandler">    
    <jms:listener destination="${jms.communityDeleteWebcastDestination}" ref="communityDeleteJmsListener" method="onMessage" />    
  </jms:listener-container>
    
  <!-- Spring Message Listener Container For Email Service. -->
  <jms:listener-container connection-factory="jmsConnectionFactory" destination-resolver="jndiDestinationResolver" 
    acknowledge="auto" concurrency="1" error-handler="msgListenerErrorHandler">    
    <jms:listener destination="${jms.emailCreateWebcastDestination}" ref="emailCreateJmsListener" method="onMessage"  />    
    <jms:listener destination="${jms.emailRescheduleWebcastDestination}" ref="emailRescheduleJmsListener" method="onMessage"  />    
    <jms:listener destination="${jms.emailCancelWebcastDestination}" ref="emailCancelJmsListener" method="onMessage"  />    
    <jms:listener destination="${jms.emailMissedYouWebcastDestination}" ref="emailMissedYouJmsListener" method="onMessage"  />    
    <jms:listener destination="${jms.emailRecordingPublishedWebcastDestination}" ref="emailRecordingPublishedJmsListener" method="onMessage"  />    
    <jms:listener destination="${jms.emailVideoUploadCompleteDestination}" ref="emailVideoUploadCompleteJmsListener" method="onMessage"  />    
    <jms:listener destination="${jms.emailVideoUploadErrorDestination}" ref="emailVideoUploadErrorJmsListener" method="onMessage"  />    
    <jms:listener destination="${jms.emailVideoUploadHDCompleteDestination}" ref="emailVideoUploadHDCompleteJmsListener" method="onMessage"  />    
    <jms:listener destination="${jms.emailVideoUploadHDErrorDestination}" ref="emailVideoUploadHDErrorJmsListener" method="onMessage"  />    
  </jms:listener-container>

  <!-- Spring Message Listener Container For Image Converter Service. -->
  <jms:listener-container connection-factory="jmsConnectionFactory" destination-resolver="jndiDestinationResolver" 
    acknowledge="auto" concurrency="1" error-handler="msgListenerErrorHandler">    
    <jms:listener destination="${jms.imageConverterInformDestination}" ref="imageConverterInformJmsListener" method="onMessage" />    
  </jms:listener-container>

  <!-- Spring Message Listener Container For LES Service. -->
  <jms:listener-container connection-factory="jmsConnectionFactory" destination-resolver="jndiDestinationResolver" 
    acknowledge="auto" concurrency="1" error-handler="msgListenerErrorHandler">    
    <jms:listener destination="${jms.lesRescheduleWebcastDestination}" ref="lesRescheduleJmsListener" method="onMessage" />    
    <jms:listener destination="${jms.lesCancelWebcastDestination}" ref="lesCancelJmsListener" method="onMessage" />    
    <jms:listener destination="${jms.lesVideoUploadCompleteDestination}" ref="lesVideoUploadCompleteJmsListener" method="onMessage" />    
    <jms:listener destination="${jms.lesVideoUploadErrorDestination}" ref="lesVideoUploadErrorJmsListener" method="onMessage" />    
  </jms:listener-container>

  <!-- Spring Message Listener Container For Notification Service. -->
  <jms:listener-container connection-factory="jmsConnectionFactory" destination-resolver="jndiDestinationResolver" 
    acknowledge="auto" concurrency="1" error-handler="msgListenerErrorHandler">    
    <jms:listener destination="${jms.notificationInformDestination}" ref="notificationInformJmsListener" method="onMessage" />    
  </jms:listener-container>

  <!-- Spring Message Listener Container For Summit Service. -->
  <jms:listener-container connection-factory="jmsConnectionFactory" destination-resolver="jndiDestinationResolver" 
    acknowledge="auto" concurrency="1" error-handler="msgListenerErrorHandler">    
    <jms:listener destination="${jms.summitRescheduleWebcastDestination}" ref="summitRescheduleJmsListener" method="onMessage" />    
    <jms:listener destination="${jms.summitDeleteWebcastDestination}" ref="summitDeleteJmsListener" method="onMessage" />    
  </jms:listener-container>

  <bean id="msgListenerErrorHandler" class="com.brighttalk.common.jms.MessageListenerErrorHandler"/>
  
</beans>