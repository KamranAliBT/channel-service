/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MyCommunicationsSearchCriteriaBuilder.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria.CommunicationStatusFilterType;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria.SortBy;
import com.brighttalk.service.channel.presentation.ApiRequestParam;

/**
 * Build Class to extract a {@link MyCommunicationsSearchCriteria} object from a request object.
 */
@Component
public class MyCommunicationsSearchCriteriaBuilder extends AbstractSearchCriteriaBuilder {

  /**
   * Build the criteria object.
   * 
   * @param request The request
   * 
   * @return The built criteria
   */
  public MyCommunicationsSearchCriteria build(HttpServletRequest request) {

    Boolean registered = getBooleanFromRequest(request, ApiRequestParam.REGISTERED);

    // registered=false is not supported
    if (registered != null && registered == false) {
      throw new ApplicationException("Invalid registered value requested [" + registered + "]", null, "ClientError");
    }

    String sortBy = request.getParameter(ApiRequestParam.SORT_BY);
    String sortOrder = request.getParameter(ApiRequestParam.SORT_ORDER);
    Integer pageNumber = getIntegerFromRequest(request, ApiRequestParam.PAGE_NUMBER);
    Integer pageSize = getIntegerFromRequest(request, ApiRequestParam.PAGE_SIZE);
    List<CommunicationStatusFilterType> statusFilter = convertWebcastStatusFilter(request.getParameter(ApiRequestParam.WEBCAST_STATUS));

    MyCommunicationsSearchCriteria criteria = new MyCommunicationsSearchCriteria();
    criteria.setPageNumber(pageNumber);
    criteria.setPageSize(pageSize);
    criteria.setSortBy(convertSortBy(sortBy));
    criteria.setSortOrder(convertSortOrder(sortOrder));
    criteria.setRegistered(registered);
    criteria.setCommunicationsStatusFilter(statusFilter);

    return criteria;
  }

  /**
   * Encapsulating method for converting a sort by string to an enum.
   * 
   * @param sortBy The String to convert
   * 
   * @return The converted Enum
   */
  private SortBy convertSortBy(String value) {
    return SortBy.convert(value);
  }

  /**
   * Encapsulating method to split string of webcast statuses into a list of Status enums.
   * 
   * @param webcastStatuses the string value to convert.
   */
  private List<CommunicationStatusFilterType> convertWebcastStatusFilter(String webcastStatuses) {

    if (StringUtils.isEmpty(webcastStatuses)) {
      return null;
    }

    List<CommunicationStatusFilterType> statuses = new ArrayList<CommunicationStatusFilterType>();

    for (String status : webcastStatuses.split(",")) {

      statuses.add(CommunicationStatusFilterType.convert(status));
    }

    return statuses;
  }
}