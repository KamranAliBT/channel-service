/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: ChannelsSearchCriteriaBuilder.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.presentation.ApiRequestParam;

/**
 * Builder Class to extract a {@link ChannelsSearchCriteria} from a request object.
 */
@Component
public class ChannelsSearchCriteriaBuilder extends AbstractSearchCriteriaBuilder {

  /**
   * Build the public channel search criteria from request context.
   * 
   * @param request the http request to convert to criteria
   * 
   * @return The built criteria
   */
  public ChannelsSearchCriteria build(final HttpServletRequest request) {

    Integer pageNumber = getIntegerFromRequest(request, ApiRequestParam.PAGE_NUMBER);
    Integer pageSize = getIntegerFromRequest(request, ApiRequestParam.PAGE_SIZE);
    String ids = request.getParameter(ApiRequestParam.IDS);

    ChannelsSearchCriteria criteria = new ChannelsSearchCriteria();
    criteria.setIds(ids);
    criteria.setPageNumber(pageNumber);
    criteria.setPageSize(pageSize);
    criteria.setPrivateWebcastsIncluded(false);

    return criteria;
  }
}