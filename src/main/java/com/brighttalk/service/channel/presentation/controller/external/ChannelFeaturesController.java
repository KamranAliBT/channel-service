/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ChannelFeaturesController.java 86487 2014-11-21 15:15:06Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.error.ErrorDto;
import com.brighttalk.common.user.User;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.service.channel.FeatureService;
import com.brighttalk.service.channel.presentation.controller.base.BaseController;
import com.brighttalk.service.channel.presentation.dto.FeatureDto;
import com.brighttalk.service.channel.presentation.dto.FeaturesDto;
import com.brighttalk.service.channel.presentation.dto.converter.FeatureConverter;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.UnsupportedFeatureException;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 */
@Controller
@RequestMapping(value = "/channel/{channelId}/features", produces = MediaType.APPLICATION_JSON_VALUE)
public class ChannelFeaturesController extends BaseController {

  private static final Logger LOGGER = Logger.getLogger(ChannelFeaturesController.class);

  @Autowired
  private FeatureService featureService;

  @Autowired
  private FeatureConverter featureConverter;

  /**
   * Handles a request to find the channel's features.
   * 
   * @param channelId the id of the channel.
   * 
   * @return channel's features
   */
  @RequestMapping(method = RequestMethod.GET)
  @ResponseBody
  public FeaturesDto find(@PathVariable final String channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Recieved a request to find features for channel [" + channelId + "].");
    }

    Long convertedChannelId = convertChannelId(channelId);

    List<Feature> features = featureService.findByChannelId(convertedChannelId);
    List<FeatureDto> featureDtos = featureConverter.convertFeatures(features);

    FeaturesDto featuresDto = new FeaturesDto();
    featuresDto.setFeatures(featureDtos);

    return featuresDto;
  }

  /**
   * Handles a request to update one or more channel's features.
   * 
   * @param channelId the id of the channel.
   * @param user the user requesting the update
   * @param featuresDtoToUpdate features to update
   * 
   * @return channel's features
   */
  @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public FeaturesDto update(@AuthenticatedUser final User user, @PathVariable final String channelId,
      @RequestBody final FeaturesDto featuresDtoToUpdate) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Recieved a request to update features for channel [" + channelId + "].");
    }

    Long convertedChannelId = convertChannelId(channelId);
    validator.validate(featuresDtoToUpdate);

    Channel channel = channelFinder.find(convertedChannelId);

    List<Feature> featuresToUpdate = featureConverter.convertFeaturesDto(featuresDtoToUpdate.getFeatures());
    List<Feature> features = featureService.update(user, channel, featuresToUpdate);
    List<FeatureDto> featureDtos = featureConverter.convertFeatures(features);

    FeaturesDto featuresDto = new FeaturesDto();
    featuresDto.setFeatures(featureDtos);

    return featuresDto;
  }

  /**
   * Exception handler method handles {@link UnsupportedFeatureException} thrown from the other layers of this API.
   * 
   * @param ex The {@link UnsupportedFeatureException} handled by this method.
   * 
   * @return {@link ErrorDto} contain {@link ErrorCode#INVALID_FEATURE_NAME} error code.
   */
  @ExceptionHandler(value = UnsupportedFeatureException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  private ErrorDto handleUnsupportedFeatureException(final UnsupportedFeatureException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Error while converting the feature [" + ex.getName() + "].");
    }
    return new ErrorDto(ErrorCode.INVALID_FEATURE_NAME.getName(), ex.getMessage());
  }

}
