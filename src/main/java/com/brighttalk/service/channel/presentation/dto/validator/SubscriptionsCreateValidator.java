/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsCreateValidator.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.presentation.dto.SubscriptionsDto;

/**
 * Validator to validate a {@link SubscriptionsDto Subscriptions DTO} for creation. This validator checks all mandatory
 * elements presents and all the mandatory values provided. This include the following:
 * <ul>
 * <li>The subscription list is not empty.</li>
 * <li>The subscription list size is not greater than 100.</li>
 * <li>The subscription element has a user element, containing either user id or email address.</li>
 * <li>The subscription user element has only id or email address.</li>
 * <li>The subscription element has only one user element.</li>
 * <li>The subscription element has a referral element with "paid" value.</li>
 * <li>The subscription element has a context element with leadType, leadContext and engagementScore.</li>
 * <li>The subscription element has a context element with leadType element with value of either "summit", "content" or
 * "keyword".</li>
 * <li>The subscription element has a context element with valid integer for engagement score element.</li>
 * <li>The subscription element has a context element with valid integer for lead context element when lead type is
 * "summit" and "content".</li>
 * </ul>
 */
@Component
public class SubscriptionsCreateValidator extends SubscriptionsValidator {

  /**
   * This flag used to check if subscription context is required. This is needed as subscription context is temporarily
   * optional until it is consider to be mandatory.
   */
  @Value("${subscription.context.required}")
  private boolean contextRequired;

  /**
   * Validate a subscriptions DTO for creation.
   * 
   * @param subscriptions to be validated.
   */
  @Override
  public void validate(final SubscriptionsDto subscriptions) {
    assertNotEmptySubscriptions(subscriptions);
    assertMaxSubscriptionsPerRequest(subscriptions);
    assertSubscriptions(subscriptions, contextRequired);
  }

  /**
   * @param contextRequired the contextRequired to set
   */
  protected void setContextRequired(boolean contextRequired) {
    this.contextRequired = contextRequired;
  }

}
