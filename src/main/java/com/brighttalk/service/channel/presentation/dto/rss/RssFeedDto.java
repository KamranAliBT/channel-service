/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RssFeedDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.rss;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Rss Feed Dto. 
 * 
 * Xml Root Element specifies what the root name is. The root's, default namespace definition
 * is declared not here, but in the {@link RssNamespacePrefixMapper} instance.
 * 
 * XmlType is used to sort properties for display purposes only, 
 * the response would be valid (but alphabetical) without it too. 
 */
@XmlRootElement( name = "rss" )
public class RssFeedDto {

  private double version;
  
  private FeedChannelDto channel;

  public double getVersion() {
    return version;
  }

  @XmlAttribute
  public void setVersion(double version) {
    this.version = version;
  }

  public FeedChannelDto getChannel() {
    return channel;
  }

  public void setChannel(FeedChannelDto channel) {
    this.channel = channel;
  }
}
