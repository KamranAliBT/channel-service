/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: WebcastSyndicationDto.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.syndication;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.service.channel.presentation.dto.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.statistics.CommunicationStatisticsDto;

/**
 * Represent a Communication syndication resource DTO within a consumer (distributed) channel and this includes:
 * <ul>
 * <li>The consumer channel details (id, title and organisation).</li>
 * <li>The syndication authorisation details (master and consumer channel owner approval).</li>
 * <li>The communication activities statistics in the distributed channel. Optional only present for master channel
 * syndication.</li>
 */
public class WebcastSyndicationDto {

  private ChannelDto channel;

  private List<AuthorisationDto> authorisations;

  private CommunicationStatisticsDto communicationStatistics;

  private WebcastSyndicationInDto syndicationIn;

  private List<WebcastSyndicationOutDto> syndicationOuts;

  /**
   * @return the channel
   */
  public ChannelDto getChannel() {
    return channel;
  }

  /**
   * @param channel the channel to set
   */
  public void setChannel(ChannelDto channel) {
    this.channel = channel;
  }

  /**
   * @return the authorisations
   */
  public List<AuthorisationDto> getAuthorisations() {
    return authorisations;
  }

  /**
   * @param authorisations the authorisations to set
   */
  public void setAuthorisations(List<AuthorisationDto> authorisations) {
    this.authorisations = authorisations;
  }

  public void addAuthorisation(AuthorisationDto authorisationDto) {
    if (this.authorisations == null) {
      this.authorisations = new ArrayList<>();
    }
    this.authorisations.add(authorisationDto);
  }

  /**
   * @return the communicationStatistics
   */
  public CommunicationStatisticsDto getCommunicationStatistics() {
    return communicationStatistics;
  }

  /**
   * @param communicationStatistics the communicationStatistics to set
   */
  public void setCommunicationStatistics(CommunicationStatisticsDto communicationStatistics) {
    this.communicationStatistics = communicationStatistics;
  }

  /**
   * @return the webcastSyndicationInDto
   */
  public WebcastSyndicationInDto getSyndicationIn() {
    return syndicationIn;
  }

  /**
   * @param syndicationIn the webcastSyndicationInDto to set
   */
  @XmlElement(name = "syndicationIn")
  public void setSyndicationIn(final WebcastSyndicationInDto syndicationIn) {
    this.syndicationIn = syndicationIn;
  }

  /**
   * @return the webcastSyndicationOutDto
   */
  public List<WebcastSyndicationOutDto> getSyndicationOuts() {
    return syndicationOuts;
  }

  /**
   * @param syndicationOuts the webcastSyndicationOutDto to set
   */
  @XmlElementWrapper(name = "syndicationOuts")
  @XmlElement(name = "syndicationOut")
  public void setSyndicationOuts(final List<WebcastSyndicationOutDto> syndicationOuts) {
    this.syndicationOuts = syndicationOuts;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this);
    builder.append("channel", channel);
    builder.append("authorisations", authorisations);
    builder.append("communicationStatistics", communicationStatistics);
    builder.append("syndicationIn", syndicationIn);
    builder.append("syndicationOuts", syndicationOuts);
    return builder.toString();
  }
}
