/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AtomNamespacePrefixMapper.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.atom;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

/**
 * Maps the namespaces to configured prefixes (if any).
 */
public class AtomNamespacePrefixMapper extends NamespacePrefixMapper {

  @Override
  public String[] getPreDeclaredNamespaceUris2() {
    
    return new String[] { "", "http://www.w3.org/2005/Atom" };
  }
  
  @Override
  public String getPreferredPrefix( String namespaceUri, String suggestion, boolean requirePrefix ) {
      
    if ( namespaceUri.equalsIgnoreCase( "http://brighttalk.com/2009/atom_extensions" ) ) {
      
      return "bt";
    } 
    
    return suggestion;
  }
}