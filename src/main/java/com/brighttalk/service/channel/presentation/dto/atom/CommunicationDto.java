/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.atom;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * CommunicationDto.
 */
public class CommunicationDto {

  private Long id;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(Long id) {
    this.id = id;
  }
}
