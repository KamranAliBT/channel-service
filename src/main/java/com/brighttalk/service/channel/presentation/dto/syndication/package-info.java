/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: codetemplates.xml 41756 2012-02-02 11:15:06Z rgreen $
 * ****************************************************************************
 */
/**
 * Syndication DTOs package.
 */
package com.brighttalk.service.channel.presentation.dto.syndication;