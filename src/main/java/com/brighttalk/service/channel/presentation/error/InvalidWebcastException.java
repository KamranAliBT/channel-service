/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: InvalidWebcastException.java 71274 2013-11-26 14:01:47Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.error;

import com.brighttalk.common.validation.ValidationException;

/**
 * Presentation Validation Exception that is thrown when one or more webcast validation rules is broken.
 */
@SuppressWarnings("serial")
public class InvalidWebcastException  extends ValidationException {

  private static final String VALIDATION_ERROR_CODE = "InvalidWebcast";

  /**
   * @param message see below.
   * @param userErrorMessage see below.
   * @see ValidationException#ValidationException(String, String)
   */
  public InvalidWebcastException(final String message, final String userErrorMessage) {
    super(message, userErrorMessage);
  }

  @Override
  public String getUserErrorCode() {
    return VALIDATION_ERROR_CODE;
  }
}