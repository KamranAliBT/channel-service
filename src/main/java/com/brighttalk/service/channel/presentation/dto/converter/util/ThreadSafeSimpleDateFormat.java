/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ThreadSafeSimpleDateFormat.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The class created to deal with the not-thread-safe nature of {@link SimpleDateFormat}.
 * The class wraps some of the methods that need to be synchronized
 */
public class ThreadSafeSimpleDateFormat {

  private DateFormat dateFormat;

  /**
   * Constructor. 
   * 
   * @param format required
   */
  public ThreadSafeSimpleDateFormat(String format)  {
    dateFormat = new SimpleDateFormat(format);
  }

  /**
   * Synchronized format call. 
   * 
   * For more information about 'synchronized' keyword 
   * see 
   * http://docs.oracle.com/javase/tutorial/essential/concurrency/syncmeth.html
   * 
   * The alternative to this solution is using FastDateFormat from Apache Commons library
   * see 
   * http://commons.apache.org/lang/api-2.4/org/apache/commons/lang/time/FastDateFormat.html
   * which was not used as that would require importing a new library
   * TODO: discuss if the alternative is needed? 
   * 
   * @param date to be formatted
   *
   * @return formatted date
   */
  public synchronized String format(Date date) {
    return dateFormat.format(date);
  }
  
}