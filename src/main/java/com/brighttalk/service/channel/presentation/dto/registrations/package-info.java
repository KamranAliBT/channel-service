/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the DTO classes  
 * used in marshalling the Webcast Registrations from business objects.
 */
package com.brighttalk.service.channel.presentation.dto.registrations;