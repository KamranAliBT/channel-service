/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastProviderDto.java 72961 2014-01-24 15:16:01Z ikerbib $
 * ****************************************************************************
 */

package com.brighttalk.service.channel.presentation.dto.syndication;


/**
 * DTO object representing webcast syndication.
 */
public class WebcastSyndicationInDto extends BaseWebcastSyndicationDto {

  /**
   * Default constructor.
   */
  public WebcastSyndicationInDto() {
  }

}