/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastController.java 75238 2014-03-14 14:55:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.service.channel.business.domain.ImageConverterSettings;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationUpdateService;
import com.brighttalk.service.channel.presentation.dto.TaskDto;
import com.brighttalk.service.channel.presentation.dto.converter.TaskConverter;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

/**
 * The internal webcast controller handling internal call from the image converter service when the image processing of
 * a feature image has completed. It does not require authentication.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller
@RequestMapping(value = "/internal")
public class ImageCallBackController {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(ImageCallBackController.class);

  @Autowired
  private CommunicationFinder communicationFinder;

  @Autowired
  @Qualifier(value = "hdcommunicationupdateservice")
  private CommunicationUpdateService communicationUpdateService;

  @Autowired
  private TaskConverter taskConverter;

  @Value(value = "${san.communication.dir}")
  private String sanBaseUrl;

  @Value(value = "${web.communication.dir}")
  private String webBaseUrl;

  /**
   * Handles a internal request to update the webcast thumbnail or preview image.
   * 
   * @param webcastId id of the webcast to update.
   * @param type type of the image processed.
   * @param taskDto the task dto containing the infos.
   * 
   * @throws ResourceNotFoundException when resource cannot be found.
   */
  @RequestMapping(value = "/channel/webcast/{webcastId}/imagecallback/{type:preview|thumbnail}", method = RequestMethod.POST)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void updateFeatureImage(@PathVariable final Long webcastId, @PathVariable final String type,
      @RequestBody final TaskDto taskDto) {

    if (!taskDto.getStatus().equals(ImageConverterSettings.Status.COMPLETE.toString().toLowerCase())) {
      LOGGER.error("Received [" + taskDto.getStatus() + "] status from image converter service.");
      return;
    }

    ImageConverterSettings imageConverterSettings = taskConverter.convert(taskDto);

    Communication communication = findCommunication(webcastId);

    if (type.equals(ImageConverterSettings.TYPE_PREVIEW)) {
      communication.setPreviewUrl(updateSanUrlForWeb(imageConverterSettings.getDestination()));

    }
    if (type.equals(ImageConverterSettings.TYPE_THUMBNAIL)) {
      communication.setThumbnailUrl(updateSanUrlForWeb(imageConverterSettings.getDestination()));
    }

    communicationUpdateService.updateFeatureImage(communication);
  }

  /**
   * Find the communication for the given webcast id.
   * 
   * @param webcastId of the communication to load.
   * @return communication when communication has been found
   * @throws ResourceNotFoundException when the communication has not been found
   */
  private Communication findCommunication(final Long webcastId) {
    try {
      return communicationFinder.find(webcastId);
    } catch (CommunicationNotFoundException nfe) {
      throw new ResourceNotFoundException(nfe.getMessage(), ErrorCode.RESOURCE_NOT_FOUND, webcastId);
    }
  }

  /**
   * Update the url of the destination to a web base url.
   * 
   * @param url to strip
   * @return the new web base url
   */
  private String updateSanUrlForWeb(final String url) {
    return url.replace(sanBaseUrl, webBaseUrl);
  }

  /**
   * @return the sanBaseUrl
   */
  public String getSanBaseUrl() {
    return sanBaseUrl;
  }

  /**
   * @param sanBaseUrl the sanBaseUrl to set
   */
  public void setSanBaseUrl(final String sanBaseUrl) {
    this.sanBaseUrl = sanBaseUrl;
  }

  /**
   * @return the webBaseUrl
   */
  public String getWebBaseUrl() {
    return webBaseUrl;
  }

  /**
   * @param webBaseUrl the webBaseUrl to set
   */
  public void setWebBaseUrl(final String webBaseUrl) {
    this.webBaseUrl = webBaseUrl;
  }
}