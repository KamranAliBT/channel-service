/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastRegistrationsConverter.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.utils.HrefBuilderUtil;
import com.brighttalk.service.channel.business.domain.PaginatedListWithTotalPages;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.registrations.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.registrations.WebcastRegistrationDto;
import com.brighttalk.service.channel.presentation.dto.registrations.WebcastRegistrationsDto;

/**
 * Converter - Responsible for converting between WebcastRegistrationsDto and paginated registrations details.
 */
@Component
public class WebcastRegistrationsConverter {

  /**
   * Converts (paginated) collection of business objects into a DTO.
   * 
   * @param registrations details of communication registrations to be converted
   * @param converterContext properties used in conversion
   * 
   * @return dto object
   */
  public WebcastRegistrationsDto convert(final PaginatedListWithTotalPages<CommunicationRegistration> registrations,
    final ConverterContext converterContext) {

    WebcastRegistrationsDto webcastRegistrationsDto = new WebcastRegistrationsDto();
    webcastRegistrationsDto.setTotal(registrations.getUnpaginatedTotal());

    if (registrations.getUnpaginatedTotal() == 0) {
      return webcastRegistrationsDto;
    }

    webcastRegistrationsDto.setRegistrations(convertRegistrations(registrations,
        converterContext.getDateTimeFormatter()));
    webcastRegistrationsDto.setLinks(convertLinks(registrations, converterContext));

    return webcastRegistrationsDto;
  }

  private List<WebcastRegistrationDto> convertRegistrations(
    final PaginatedListWithTotalPages<CommunicationRegistration> registrations, final DateTimeFormatter formatter) {

    List<WebcastRegistrationDto> registrationDtos = new ArrayList<WebcastRegistrationDto>();

    for (CommunicationRegistration communicationRegistration : registrations) {

      WebcastRegistrationDto webcastRegistrationDto = new WebcastRegistrationDto();
      webcastRegistrationDto.setId(communicationRegistration.getId());
      webcastRegistrationDto.setRegistered(formatter.convert(communicationRegistration.getRegistered()));

      ChannelDto channel = new ChannelDto();
      channel.setId(communicationRegistration.getChannelId());
      webcastRegistrationDto.setChannel(channel);

      WebcastDto webcast = new WebcastDto();
      webcast.setId(communicationRegistration.getCommunicationId());
      webcastRegistrationDto.setWebcast(webcast);

      registrationDtos.add(webcastRegistrationDto);
    }

    return registrationDtos;
  }

  private List<LinkDto> convertLinks(final PaginatedListWithTotalPages<CommunicationRegistration> registrations,
    final ConverterContext converterContext) {

    List<LinkDto> links = new ArrayList<LinkDto>();

    // If only one page - no pagination links
    if (registrations.getNumberOfPages() <= 1) {
      return links;
    }

    // Include the first and previous page links - unless we are already on the first page
    if (!registrations.isFirstPage()) {
      links.add(createFirstPageLink(registrations, converterContext));
      links.add(createPreviousPageLink(registrations, converterContext));
    }

    // Include the last and next page links - unless we are already on the last page
    if (!registrations.isLastPage()) {
      links.add(createLastPageLink(registrations, converterContext));
      links.add(createNextPageLink(registrations, converterContext));
    }

    return links;
  }

  private LinkDto createFirstPageLink(final PaginatedListWithTotalPages<CommunicationRegistration> registrations,
    final ConverterContext converterContext) {

    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(registrations.getFirstPageNumber()));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(registrations.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_FIRST);

    return linkDto;
  }

  private LinkDto createLastPageLink(final PaginatedListWithTotalPages<CommunicationRegistration> registrations,
    final ConverterContext converterContext) {

    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(registrations.getLastPageNumber()));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(registrations.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_LAST);

    return linkDto;
  }

  private LinkDto createPreviousPageLink(final PaginatedListWithTotalPages<CommunicationRegistration> registrations,
    final ConverterContext converterContext) {

    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(registrations.getPreviousPageNumber()));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(registrations.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_PREVIOUS);

    return linkDto;
  }

  private LinkDto createNextPageLink(final PaginatedListWithTotalPages<CommunicationRegistration> registrations,
    final ConverterContext converterContext) {

    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(registrations.getNextPageNumber()));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(registrations.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_NEXT);

    return linkDto;
  }
}