/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastPublicDto.java 65735 2013-06-19 09:49:16Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 * WebcastPublicDto.
 */
public class WebcastPublicDto {

  private Long id;

  private ChannelPublicDto channel;

  private String title;

  private String description;

  private String keywords;

  private String presenter;

  private String status;

  private String start;

  private String entryTime;

  private String closeTime;

  private String created;

  private String lastUpdated;

  private String visibility;

  private String url;

  private String format;

  private Integer duration;

  private Float rating;

  private Integer totalViewings;

  private List<LinkDto> links;

  private List<CategoryDto> categories;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(final Long id) {
    this.id = id;
  }

  public ChannelPublicDto getChannel() {
    return channel;
  }

  public void setChannel(final ChannelPublicDto value) {
    channel = value;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(final String status) {
    this.status = status;
  }

  public String getStart() {
    return start;
  }

  public void setStart(final String start) {
    this.start = start;
  }

  public String getEntryTime() {
    return entryTime;
  }

  public void setEntryTime(final String entryTime) {
    this.entryTime = entryTime;
  }

  public String getCloseTime() {
    return closeTime;
  }

  public void setCloseTime(final String closeTime) {
    this.closeTime = closeTime;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(final String created) {
    this.created = created;
  }

  public String getLastUpdated() {
    return lastUpdated;
  }

  public void setLastUpdated(final String lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public String getVisibility() {
    return visibility;
  }

  public void setVisibility(final String visibility) {
    this.visibility = visibility;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(final String format) {
    this.format = format;
  }

  public Integer getDuration() {
    return duration;
  }

  public void setDuration(final Integer duration) {
    this.duration = duration;
  }

  public Float getRating() {
    return rating;
  }

  public void setRating(final Float rating) {
    this.rating = rating;
  }

  public Integer getTotalViewings() {
    return totalViewings;
  }

  public void setTotalViewings(final Integer totalViewings) {
    this.totalViewings = totalViewings;
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  @XmlElement(name = "link")
  public void setLinks(final List<LinkDto> links) {
    this.links = links;
  }

  /**
   * Adds link to this webcasts public dto links list. 
   * 
   * @param link to be added
   */
  public void addLink(final LinkDto link) {
    if (this.links == null) {
      this.links = new ArrayList<LinkDto>();
    }
    this.links.add(link);

  }

  public List<CategoryDto> getCategories() {
    return categories;
  }

  @XmlElementWrapper(name = "categories")
  @XmlElement(name = "category")
  public void setCategories(final List<CategoryDto> categories) {
    this.categories = categories;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(final String keywords) {
    this.keywords = keywords;
  }

  public String getPresenter() {
    return presenter;
  }

  public void setPresenter(final String presenter) {
    this.presenter = presenter;
  }
}