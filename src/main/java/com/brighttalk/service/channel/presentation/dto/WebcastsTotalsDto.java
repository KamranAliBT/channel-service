/**
 * **************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastsTotalsDto.java 57672 2012-11-16 17:55:35Z aaugustyn $
 * **************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * WebcastTotals Dto. DTO object representing the total webcasts/status in the user subscribed channels.
 */
@XmlRootElement(name = "webcasts_totals")
public class WebcastsTotalsDto {

  private Long upcoming;

  private Long live;

  private Long processing;

  private Long recorded;

  public Long getUpcoming() {
    return upcoming;
  }

  public void setUpcoming(Long upcoming) {
    this.upcoming = upcoming;
  }

  public Long getLive() {
    return live;
  }

  public void setLive(Long live) {
    this.live = live;
  }

  public Long getProcessing() {
    return processing;
  }

  public void setProcessing(Long processing) {
    this.processing = processing;
  }

  public Long getRecorded() {
    return recorded;
  }

  public void setRecorded(Long recorded) {
    this.recorded = recorded;
  }

}
