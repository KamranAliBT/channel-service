/**
 * **************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastsTotalsConverter.java 74757 2014-02-27 11:18:36Z ikerbib $
 * **************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.dto.WebcastsTotalsDto;

/**
 * Converter - Responsible for converting between Map of webcast totals and WebcatsTotalsDto.
 */
@Component
public class WebcastsTotalsConverter {

  /**
   * Converts webcastTotals map to WebcastTotalsDto.
   * 
   * @param webcastTotals the webcastTotals.
   * 
   * @return the WebcastTotalsDto.
   */
  public WebcastsTotalsDto convert(Map<String, Long> webcastTotals) {

    WebcastsTotalsDto webcastTotalsDto = new WebcastsTotalsDto();
    webcastTotalsDto.setLive(webcastTotals.get(Communication.Status.LIVE.toString().toLowerCase()));
    webcastTotalsDto.setProcessing(webcastTotals.get(Communication.Status.PROCESSING.toString().toLowerCase()));
    webcastTotalsDto.setRecorded(webcastTotals.get(Communication.Status.RECORDED.toString().toLowerCase()));
    webcastTotalsDto.setUpcoming(webcastTotals.get(Communication.Status.UPCOMING.toString().toLowerCase()));

    return webcastTotalsDto;
  }
}