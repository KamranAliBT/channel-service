/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagersDto.java 57059 2012-11-06 17:22:01Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.ArrayList;
import java.util.List;

import com.brighttalk.service.channel.business.domain.validator.Validator;

/**
 * DTO object representing a channel managers list.
 */
public class ChannelManagersPublicDto {

  private List<ChannelManagerPublicDto> channelManagers;

  private List<LinkDto> links;

  private List<ErrorDto> errors;

  public List<ChannelManagerPublicDto> getChannelManagers() {
    return channelManagers;
  }

  public void setChannelManagers(final List<ChannelManagerPublicDto> channelManagers) {
    this.channelManagers = channelManagers;
  }

  /**
   * Add a single channel manager.
   * 
   * @param channelManager channel manager
   */
  public void addChannelManager(final ChannelManagerPublicDto channelManager) {
    if (channelManagers == null) {
      channelManagers = new ArrayList<ChannelManagerPublicDto>();
    }
    channelManagers.add(channelManager);
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  public void setLinks(final List<LinkDto> links) {
    this.links = links;
  }

  public List<ErrorDto> getErrors() {
    return errors;
  }

  public void setErrors(final List<ErrorDto> errors) {
    this.errors = errors;
  }

  /**
   * Add a single error.
   * 
   * @param error error dto
   */
  public void addError(final ErrorDto error) {
    if (errors == null) {
      errors = new ArrayList<ErrorDto>();
    }
    errors.add(error);
  }

  /**
   * Validates this dto with given validator.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<ChannelManagersPublicDto> validator) {
    validator.validate(this);
  }

}