/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceAuthorisationController.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.service.AuthorisationService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform authorisation operations on resource.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 * Authorisation can be requested on a single resource, identified by the id, and the business layer will decide if the
 * current user has the necessary authorisation to write to this resource.
 */
@Controller
@RequestMapping(value = "/resource")
public class ResourceAuthorisationController {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(ResourceAuthorisationController.class);

  /**
   * Communication finder
   */
  @Autowired
  private CommunicationFinder communicationFinder;

  /**
   * Communication service
   */
  @Autowired
  private AuthorisationService authorisationService;

  /** Channel finder */
  @Autowired
  private ChannelFinder channelFinder;

  /**
   * Resource Dao
   */
  @Autowired
  private ResourceDbDao resourceDbDao;

  /**
   * Authorise the current user for write access to a resource.
   * 
   * @param user The Current User
   * @param resourceId The Id of the target resource
   * 
   * @throws UserAuthorisationException when user is not allowed to access given resource.
   */
  @RequestMapping(value = "/{resourceId}/authorisation", method = RequestMethod.GET)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void authorise(@AuthenticatedUser final User user, @PathVariable final Long resourceId) {

    Map<String, Long> detailsMap = null;
    try {
      detailsMap = resourceDbDao.findChannelIdAndCommunicationId(resourceId);
    } catch (NotFoundException nfe) {
      throw new UserAuthorisationException("Resource [" + resourceId + "] doesnt exist.", nfe);
    }

    Long channelId = detailsMap.get("channelId");
    Long communicationId = detailsMap.get("communicationId");

    Channel channel = null;
    Communication communication = null;
    try {
      channel = channelFinder.find(channelId);
      communication = communicationFinder.find(channel, communicationId);
    } catch (NotFoundException nfe) {
      throw new UserAuthorisationException("Channel [" + channelId + "] or Communication  [" + communicationId
          + "] doesnt exist.", nfe);
    }

    authorisationService.authoriseCommunicationAccess(user, channel, communication);
  }

  /**
   * Sets the communication finder of this controller.
   * 
   * @param communicationFinder the communication service to be set.
   */
  public void setCommunicationFinder(final CommunicationFinder communicationFinder) {
    this.communicationFinder = communicationFinder;
  }

  /**
   * Sets the authorisation service of this controller.
   * 
   * @param authorisationService the authorisation service to be set.
   */
  public void setAuthorisationService(final AuthorisationService authorisationService) {
    this.authorisationService = authorisationService;
  }

  /**
   * Sets the resource Dao of this controller.
   * 
   * @param resourceDbDao the resource Dao to be set.
   */
  public void setResourceDbDao(final ResourceDbDao resourceDbDao) {
    this.resourceDbDao = resourceDbDao;
  }

  /**
   * @param channelFinder the channelFinder to set
   */
  public void setChannelFinder(final ChannelFinder channelFinder) {
    this.channelFinder = channelFinder;
  }

}
