/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: package-info.java 70203 2013-10-24 18:01:03Z ssarraj $
 * ****************************************************************************
 */
/**
 * This package contains the validation marker interfaces that are used to identify the validation context of an object.
 */
package com.brighttalk.service.channel.presentation.dto.validation.scenario;

