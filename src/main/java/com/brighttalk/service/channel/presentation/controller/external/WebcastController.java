/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastController.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.error.BookingCapacityExceededException;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.error.ContentPlanException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.error.InvalidCommunicationException;
import com.brighttalk.service.channel.business.error.ProviderNotSupportedException;
import com.brighttalk.service.channel.business.service.communication.CommunicationCreateService;
import com.brighttalk.service.channel.business.service.communication.CommunicationUpdateService;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.presentation.controller.base.BaseController;
import com.brighttalk.service.channel.presentation.dto.ChannelPublicDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.validator.HDWebcastCreateValidator;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles API calls that
 * perform CRUD operations on Webcast resource.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller(value = "externalWebcastController")
@RequestMapping(value = "/channel")
public class WebcastController extends BaseController {

  /** Logger for this class */
  private static final Logger LOGGER = Logger.getLogger(WebcastController.class);

  /** Action description for audit record when webcast is cancelled **/
  private static final String CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION =
      "Cancelled through channel (Java) external API call";

  @Autowired
  private CommunicationCreateService communicationCreateService;

  @Autowired
  @Qualifier(value = "hdcommunicationupdateservice")
  private CommunicationUpdateService hdCommunicationUpdateService;

  @Autowired
  private HDWebcastCreateValidator hDWebcastCreateValidator;

  /**
   * Handles requests to get a webcast details for the given webcast id in the given channel.
   * 
   * @param user The Current User
   * @param requestedChannelId id of the channel the webcast is contained into.
   * @param requestedWebcastId id of the webcast to load.
   * @param formatter the date formatter
   * 
   * @return webcastDto The found webcast DTO.
   * 
   * @throws ResourceNotFoundException when either the requested channel or the requested webcast doesn't identify an
   * existing resource.
   */
  @RequestMapping(value = "/{channelId}/webcast/{webcastId}", method = RequestMethod.GET)
  @ResponseBody
  public WebcastDto get(@AuthenticatedUser final User user, @PathVariable("channelId") final String requestedChannelId,
    @PathVariable("webcastId") final String requestedWebcastId, @DateTimeFormat final DateTimeFormatter formatter) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Received request to get the identified webcast [" + requestedWebcastId + "] in the given channel ["
          + requestedChannelId + "].");
    }

    // Validate and convert the supplied channel and webcast Id.
    Long channelId = convertChannelId(requestedChannelId);
    Long webcastId = convertWebcastId(requestedWebcastId);

    // Found the supplied channel and checks if it identifies an existing channel.
    Channel channel = findChannel(channelId);

    // Assert if the user is channel owner or manager
    channel.isAuthorised(user);

    Communication communication = findCommunication(channel, webcastId);

    WebcastDto webcastDto = webcastConverter.convert(communication, formatter);

    return webcastDto;
  }

  /**
   * Handles requests to get a webcast details for the given webcast id in the given channel.
   * 
   * @param webcastId id of the webcast to load.
   * @param formatter the date formatter
   * 
   * @return webcastDto The found webcast DTO.
   * 
   * @throws ResourceNotFoundException when either the requested channel or the requested webcast doesn't identify an
   * existing resource.
   */
  @RequestMapping(value = "/webcast/{webcastId}", method = RequestMethod.GET)
  @ResponseBody
  public WebcastDto get(@PathVariable final Long webcastId, @DateTimeFormat final DateTimeFormatter formatter) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Received request to get the identified webcast [" + webcastId + "].");
    }

    Communication communication = findCommunication(webcastId);

    WebcastDto webcastDto = webcastConverter.convert(communication, formatter);

    return webcastDto;
  }

  /**
   * Handles requests to create a webcast in the given channel.
   * 
   * @param user The Current User
   * @param channelId The id of the channel in which the webcast will created in.
   * @param webcastDto The requested webcast to be created.
   * @param formatter the date formatter
   * 
   * @throws BadRequestException In case of the supplied channel is different to the master channel in the requested
   * HTTP body.
   * @throws ResourceNotFoundException when eith the channel or the webcast has not been found.
   * @throws NotAuthorisedException when the user is not authorised.
   * @throws Exception is thrown if the create fails
   * @return The created webcast.
   */
  @RequestMapping(value = "/{channelId}/webcast", method = RequestMethod.POST)
  @ResponseStatus(value = HttpStatus.CREATED)
  @ResponseBody
  public WebcastDto create(@AuthenticatedUser final User user, @PathVariable final Long channelId,
    @RequestBody final WebcastDto webcastDto, @DateTimeFormat final DateTimeFormatter formatter) throws Exception {

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Received request to book a webcast in the given channel[" + channelId + "].");
    }

    // Validate the supplied channel in the URL is the same as the channel supplied in the POST body.
    validateRequestedChannelId(channelId, webcastDto.getChannel());

    // Validate the webcast dto fields.
    webcastDto.validate(hDWebcastCreateValidator);

    Communication webcast = webcastConverter.convert(webcastDto);

    Communication createdWebcast = null;
    Long createdCommunicationId = null;
    WebcastDto webcastDtoToReturn = null;

    // Find the channel by the given channel id. If the channel has not been found, we throw a
    // ChannelNotFoundException.
    Channel channel = findChannel(channelId);

    try {

      createdCommunicationId = communicationCreateService.create(user, channel, webcast);
      createdWebcast = communicationFinder.find(channel, createdCommunicationId);
      webcastDtoToReturn = webcastConverter.convert(createdWebcast, formatter);

      // Audit the creation request
      AuditRecordDto auditRecordDto = getCreateWebcastAuditRecordDto(user.getId(), channelId, webcast.getId());
      auditQueue.create(auditRecordDto);

    } catch (ContentPlanException cpe) {
      throw new BadRequestException(cpe.getMessage(), cpe.getUserErrorCode());
    } catch (InvalidCommunicationException ice) {
      throw new BadRequestException(ice.getMessage(), ice.getUserErrorCode());
    } catch (ProviderNotSupportedException pnse) {
      throw new BadRequestException(pnse.getMessage(), pnse.getUserErrorCode());
    } catch (CommunicationNotFoundException nfe) {
      throw new ResourceNotFoundException(nfe.getMessage(), ErrorCode.RESOURCE_NOT_FOUND.getName(),
          new Object[] { createdCommunicationId });
    } catch (UserAuthorisationException uae) {
      throw new NotAuthorisedException(uae.getMessage(), ErrorCode.USER_NOT_AUTHORISED.getName(),
          new Object[] { channelId });
    } catch (BookingCapacityExceededException be) {
      throw new BadRequestException(be.getMessage(), be.getUserErrorCode());
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }
    return webcastDtoToReturn;
  }

  /**
   * Handles requests to update a webcast in the given channel.
   * 
   * @param user The Current User
   * @param webcastId The id of the webcast to be updated.
   * @param channelId The id of the channel in which the webcast belong to.
   * @param webcastDto The requested webcast to be updated.
   * @param formatter the date formatter
   * 
   * @throws BadRequestException In case of the supplied channel is different to the master channel in the requested
   * HTTP body.
   * @throws ResourceNotFoundException when eith the channel or the webcast has not been found.
   * @throws NotAuthorisedException when the user is not authorised.
   * @throws Exception is thrown if the update fails
   * @return webcastDto The updated webcast.
   */
  @RequestMapping(value = "/{channelId}/webcast/{webcastId}", method = RequestMethod.PUT)
  @ResponseBody
  public WebcastDto update(@AuthenticatedUser final User user, @PathVariable final Long channelId,
    @PathVariable final Long webcastId, @RequestBody final WebcastDto webcastDto,
    @DateTimeFormat final DateTimeFormatter formatter) throws Exception {

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Received request to update the webcast [" + webcastId + "] in the channel [" + channelId + "].");
    }

    // Validate the supplied channel to be the same as the master channel.
    validateRequestedChannelId(channelId, webcastDto.getChannel());

    // Validate that the supplied webcast id is the same as the id provided in the request.
    validateRequestedWebcastId(webcastId, webcastDto.getId());

    // Validate the webcast dto fields.
    webcastDto.validate(webcastUpdateValidator);

    // Convert the webcast dto into a commnuication object
    Communication communication = webcastConverter.convert(webcastDto);

    Communication returnedCommunication = null;
    Channel channel = findChannel(channelId);

    try {
      // Assert if the user is channel owner or manager. You can only update a communication if you
      // are the channel owner (or manager)
      channel.isAuthorised(user);

      Long updatedCommunicationId = null;

      // Update the communication in DB
      if (webcastDto.hasStatus() && StringUtils.equalsIgnoreCase(webcastDto.getStatus(), Status.CANCELLED.toString())) {
        updatedCommunicationId = communicationCancelService.cancel(communication);

        // Audit the cancellation request
        AuditRecordDto auditRecordDto = getCancelWebcastAuditRecordDto(user.getId(), channelId, communication.getId());
        auditQueue.create(auditRecordDto);

      } else {
        updatedCommunicationId = hdCommunicationUpdateService.update(user, channel, communication);
      }
      // return the updated communication
      returnedCommunication = findCommunication(channel, updatedCommunicationId);
    } catch (ContentPlanException cpe) {
      throw new BadRequestException(cpe.getMessage(), cpe.getUserErrorCode());
    } catch (InvalidCommunicationException ice) {
      throw new BadRequestException(ice.getMessage(), ice.getUserErrorCode());
    } catch (ProviderNotSupportedException pnse) {
      throw new BadRequestException(pnse.getMessage(), pnse.getUserErrorCode(),
          new Object[] { webcastDto.getProvider().getName() });
    } catch (BadRequestException bre) {
      throw new BadRequestException(bre.getMessage(), bre.getUserErrorCode());
    } catch (FeatureNotEnabledException fnee) {
      throw new BadRequestException(fnee.getMessage(), fnee.getUserErrorCode());
    } catch (CommunicationNotFoundException nfe) {
      throw new ResourceNotFoundException(nfe.getMessage(), ErrorCode.RESOURCE_NOT_FOUND.getName(),
          new Object[] { webcastId });
    } catch (UserAuthorisationException uae) {
      throw new NotAuthorisedException(uae.getMessage(), ErrorCode.USER_NOT_AUTHORISED.getName(),
          new Object[] { channelId });
    } catch (BookingCapacityExceededException be) {
      throw new BadRequestException(be.getMessage(), be.getUserErrorCode());
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }

    return webcastConverter.convert(returnedCommunication, formatter);
  }

  /**
   * Handles requests to delete the requested webcast in the given channel.
   * 
   * @param channelId id of the channel the webcast is contained into.
   * @param webcastId id of the webcast to load.
   * 
   * @throws BadRequestException as the api has not been implemented yet
   */
  @RequestMapping(value = "/{channelId}/webcast/{webcastId}", method = RequestMethod.DELETE)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  @ResponseBody
  public void delete(@PathVariable final Long channelId, @PathVariable final Long webcastId) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Received request to delete the identified webcast [" + webcastId + "] in the given channel ["
          + channelId + "].");
    }
    throw new BadRequestException("not yet implemented");
  }

  /**
   * Validates the supplied channel is the same as the master channel.
   * 
   * @param channelId The supplied Channel Id in the request URL.
   * @param masterChannel The requested master channel;
   */
  private void validateRequestedChannelId(final long channelId, final ChannelPublicDto masterChannel) {
    if (masterChannel != null && channelId != masterChannel.getId()) {
      throw new BadRequestException("Invalid channel Id", ErrorCode.INVALID_CHANNEL.getName(),
          new Object[] { channelId });
    }
  }

  /**
   * Validates the supplied channel is the same as the master channel.
   * 
   * @param channelId The supplied Channel Id in the request URL.
   * @param masterChannel The requested master channel;
   */
  private void validateRequestedWebcastId(final long webcastId, final long requestId) {
    if (webcastId != requestId) {
      throw new BadRequestException("Invalid webcast Id", ErrorCode.INVALID_WEBCAST_ID.getName(),
          new Object[] { webcastId });
    }
  }

  /**
   * Creates an instance of the {@link AuditRecordDto} for the webcast creation event.
   * 
   * @param user ID of user that webcast was created by.
   * @param channel ID of channel that the webcast is created in.
   * @param webcast ID of webcast that has been created.
   * 
   * @return Instance of {@link AuditRecordDto}
   */
  private AuditRecordDto getCreateWebcastAuditRecordDto(final long userId, final long channelId, final long webcastId) {
    LOGGER.info("Create audit record for create webcast [" + webcastId + "] by user [" + userId + "] in channel ["
        + channelId + "]");
    return auditRecordDtoBuilder.create(userId, channelId, webcastId, null);
  }

  /**
   * Creates an instance of the {@link AuditRecordDto} for the webcast cancellation event.
   * 
   * @param user ID of user that webcast was cancelled by.
   * @param channel ID of channel that the webcast is in.
   * @param webcast ID of webcast that has been cancelled.
   * 
   * @return Instance of {@link AuditRecordDto}
   */
  private AuditRecordDto getCancelWebcastAuditRecordDto(final long userId, final long channelId, final long webcastId) {
    LOGGER.info("Create audit record for cancel webcast [" + webcastId + "] by user [" + userId + "] in channel ["
        + channelId + "]");
    return auditRecordDtoBuilder.cancel(userId, channelId, webcastId, CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION);
  }

}