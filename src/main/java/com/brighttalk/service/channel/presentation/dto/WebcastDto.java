/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastDto.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.domain.validator.Validator;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationDto;
import com.brighttalk.service.channel.presentation.dto.validation.constraint.ISO8601Date;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.CreateWebcast;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateWebcast;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.WebcastStatusLive;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.WebcastStatusProcessing;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.WebcastStatusRecorded;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.WebcastStatusUpcoming;

/**
 * WebcastPublicDto.
 */
@XmlRootElement(name = "webcast")
public class WebcastDto {

  /** Max length of a webcast title */
  private static final int MAX_LENGTH_TITLE = 80;

  /** Max length of a webcast description */
  private static final int MAX_LENGTH_DESCRIPTION = 2000;

  /** Max length of a webcast keywords */
  private static final int MAX_LENGTH_KEYWORDS = 52;

  /** Max length of a webcast presenters */
  private static final int MAX_LENGTH_PRESENTER = 125;

  /** Max length of a webcast campaign reference */
  private static final int MAX_LENGTH_CAMPAIGN_REFERENCE = 255;

  @Null(groups = CreateWebcast.class)
  @NotNull(groups = UpdateWebcast.class)
  private Long id;

  @NotNull(groups = CreateWebcast.class, message = "{error.invalid.channel.nullValue}")
  private ChannelPublicDto channel;

  @NotEmpty(groups = CreateWebcast.class, message = "{error.invalid.title.nullValue}")
  @Size(max = MAX_LENGTH_TITLE, groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{error.invalid.title.tooLong}")
  private String title;

  @NotEmpty(groups = CreateWebcast.class, message = "{error.invalid.description.nullValue}")
  @Length(max = MAX_LENGTH_DESCRIPTION, groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{error.invalid.description.tooLong}")
  private String description;

  @NotEmpty(groups = CreateWebcast.class, message = "{error.invalid.keywords.nullValue}")
  @Length(max = MAX_LENGTH_KEYWORDS, groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{error.invalid.keywords.tooLong}")
  private String keywords;

  @NotEmpty(groups = CreateWebcast.class, message = "{error.invalid.presenter.nullValue}")
  @Length(max = MAX_LENGTH_PRESENTER, groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{error.invalid.presenter.tooLong}")
  private String presenter;

  @Pattern(regexp = "upcoming|live|processing|recorded|cancelled", groups = UpdateWebcast.class, message = "{error.invalid.status.values}")
  private String status;
  // CHECKSTYLE:OFF
  @Null(groups = { WebcastStatusLive.class, WebcastStatusProcessing.class, WebcastStatusRecorded.class }, message = "{error.invalid.status.scheduled}")
  // CHECKSTYLE:ON
  @NotEmpty(groups = CreateWebcast.class, message = "{error.invalid.scheduledDate}")
  @Size(min = 1, groups = UpdateWebcast.class, message = "{error.invalid.scheduledDate}")
  @ISO8601Date(groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{error.invalid.scheduledDate}")
  private String scheduled;

  @NotNull(groups = { CreateWebcast.class }, message = "{error.invalid.duration.nullValue}")
  @Null(groups = { WebcastStatusLive.class, WebcastStatusProcessing.class }, message = "{error.invalid.status.duration}")
  private Integer duration;

  private String start;

  @Null(groups = { WebcastStatusLive.class, WebcastStatusProcessing.class, WebcastStatusRecorded.class }, message = "{error.invalid.status.timezone}")
  @NotEmpty(groups = CreateWebcast.class, message = "{error.invalid.timeZone.nullValue}")
  private String timeZone;

  private String entryTime;

  private String closeTime;

  private WebcastProviderDto provider;

  @NotEmpty(groups = CreateWebcast.class, message = "{error.invalid.webcastFormat}")
  @Pattern(regexp = "video", groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{error.invalid.webcastFormat}")
  private String format;

  @Null(groups = { WebcastStatusUpcoming.class, WebcastStatusLive.class }, message = "{error.invalid.status.publishStatus}")
  private String publishStatus;

  @NotEmpty(groups = { CreateWebcast.class }, message = "{error.invalid.webcastVisibility}")
  @Null(groups = { WebcastStatusLive.class }, message = "{error.invalid.status.visibility}")
  @Pattern(regexp = "public|private", groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{error.invalid.webcastVisibility}")
  private String visibility;

  @Pattern(regexp = "true|false", groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{invalid.bool.value} allowAnonymous")
  private String allowAnonymous;

  @Pattern(regexp = "true|false", groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{invalid.bool.value} excludeFromChannelCapacity")
  private String excludeFromChannelCapacity;

  @Pattern(regexp = "true|false", groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{invalid.bool.value} showChannelSurvey")
  private String showChannelSurvey;

  @Length(max = MAX_LENGTH_CAMPAIGN_REFERENCE, groups = { CreateWebcast.class, UpdateWebcast.class }, message = "{invalid.clientReference.tooLong}")
  private String clientBookingReference;

  private Float rating;

  private Integer totalViewings;

  private String pinNumber;

  private String livePhoneNumber;

  private List<LinkDto> links;

  private List<CategoryDto> categories;

  private String created;

  private String lastUpdated;

  private String publicationDate;

  private String unpublicationDate;

  private String url;
  private String customUrl;
  private String channelWebcastUrl;

  private WebcastSyndicationDto syndication;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * check if the webcast dto has an id set.
   * 
   * @return true if the id is set else false
   */
  public boolean hasId() {
    return id != null;
  }

  public ChannelPublicDto getChannel() {
    return channel;
  }

  public void setChannel(final ChannelPublicDto value) {
    channel = value;
  }

  /**
   * check that the webcast dto has a channel object set.
   * 
   * @return true if the channel is set else false.
   */
  public boolean hasChannel() {
    return channel != null;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(final String status) {
    this.status = status;
  }

  public String getStart() {
    return start;
  }

  public void setStart(final String start) {
    this.start = start;
  }

  public String getEntryTime() {
    return entryTime;
  }

  public void setEntryTime(final String entryTime) {
    this.entryTime = entryTime;
  }

  public String getCloseTime() {
    return closeTime;
  }

  public void setCloseTime(final String closeTime) {
    this.closeTime = closeTime;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(final String created) {
    this.created = created;
  }

  public String getLastUpdated() {
    return lastUpdated;
  }

  public void setLastUpdated(final String lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public String getVisibility() {
    return visibility;
  }

  public void setVisibility(final String visibility) {
    this.visibility = visibility;
  }

  /**
   * @return the timeZone
   */
  public String getTimeZone() {
    return timeZone;
  }

  /**
   * @param timeZone the timeZone to set
   */
  public void setTimeZone(final String timeZone) {
    this.timeZone = timeZone;
  }

  /**
   * @return the publishStatus
   */
  public String getPublishStatus() {
    return publishStatus;
  }

  /**
   * @param publishStatus the publishStatus to set
   */
  public void setPublishStatus(final String publishStatus) {
    this.publishStatus = publishStatus;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public String getCustomUrl() {
    return customUrl;
  }

  public void setCustomUrl(final String customUrl) {
    this.customUrl = customUrl;
  }

  /**
   * @return the channelWebcastUrl
   */
  public String getChannelWebcastUrl() {
    return channelWebcastUrl;
  }

  /**
   * @param channelWebcastUrl the channelWebcastUrl to set
   */
  public void setChannelWebcastUrl(final String channelWebcastUrl) {
    this.channelWebcastUrl = channelWebcastUrl;
  }

  /**
   * Indicates if this webcast has custom url set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasCustomUrl() {
    return StringUtils.isNotEmpty(customUrl);
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(final String format) {
    this.format = format;
  }

  /**
   * @return the provider
   */
  public WebcastProviderDto getProvider() {
    return provider;
  }

  /**
   * @param provider the provider to set
   */
  public void setProvider(final WebcastProviderDto provider) {
    this.provider = provider;
  }

  public Integer getDuration() {
    return duration;
  }

  public void setDuration(final Integer duration) {
    this.duration = duration;
  }

  public Float getRating() {
    return rating;
  }

  public void setRating(final Float rating) {
    this.rating = rating;
  }

  public Integer getTotalViewings() {
    return totalViewings;
  }

  public void setTotalViewings(final Integer totalViewings) {
    this.totalViewings = totalViewings;
  }

  public String getPinNumber() {
    return pinNumber;
  }

  public void setPinNumber(final String pinNumber) {
    this.pinNumber = pinNumber;
  }

  public String getLivePhoneNumber() {
    return livePhoneNumber;
  }

  public void setLivePhoneNumber(final String livePhoneNumber) {
    this.livePhoneNumber = livePhoneNumber;
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  @XmlElement(name = "link")
  public void setLinks(final List<LinkDto> links) {
    this.links = links;
  }

  /**
   * Check if the webcast dto has links set.
   * 
   * @return true if links are set, else false
   */
  public boolean hasLinks() {
    return !CollectionUtils.isEmpty(links);
  }

  /**
   * Adds link to this webcasts public dto links list.
   * 
   * @param link to be added
   */
  public void addLink(final LinkDto link) {
    if (links == null) {
      links = new ArrayList<LinkDto>();
    }
    links.add(link);

  }

  public List<CategoryDto> getCategories() {
    return categories;
  }

  @XmlElementWrapper(name = "categories")
  @XmlElement(name = "category")
  public void setCategories(final List<CategoryDto> categories) {
    this.categories = categories;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(final String keywords) {
    this.keywords = keywords;
  }

  public String getPresenter() {
    return presenter;
  }

  public void setPresenter(final String presenter) {
    this.presenter = presenter;
  }

  /**
   * @return the allowAnonymous
   */
  public String getAllowAnonymous() {
    return allowAnonymous;
  }

  /**
   * @param allowAnonymous the allowAnonymous to set
   */
  public void setAllowAnonymous(final String allowAnonymous) {
    this.allowAnonymous = allowAnonymous;
  }

  /**
   * @return the excludeFromChannelCapacity
   */
  public String getExcludeFromChannelCapacity() {
    return excludeFromChannelCapacity;
  }

  /**
   * @param excludeFromChannelContentPlan the excludeFromChannelCapacity to set
   */
  public void setExcludeFromChannelCapacity(final String excludeFromChannelContentPlan) {
    excludeFromChannelCapacity = excludeFromChannelContentPlan;
  }

  /**
   * @return the showChannelSurvey
   */
  public String getShowChannelSurvey() {
    return showChannelSurvey;
  }

  /**
   * @param showChannelSurvey the showChannelSurvey to set
   */
  public void setShowChannelSurvey(final String showChannelSurvey) {
    this.showChannelSurvey = showChannelSurvey;
  }

  /**
   * @return the clientBookingReference
   */
  public String getClientBookingReference() {
    return clientBookingReference;
  }

  /**
   * @param clientBookingReference the clientBookingReference to set
   */
  public void setClientBookingReference(final String clientBookingReference) {
    this.clientBookingReference = clientBookingReference;
  }

  /**
   * @return the scheduled
   */
  public String getScheduled() {
    return scheduled;
  }

  /**
   * @param scheduled the scheduled to set
   */
  public void setScheduled(final String scheduled) {
    this.scheduled = scheduled;
  }

  public String getPublicationDate() {
    return publicationDate;
  }

  public void setPublicationDate(final String publicationDate) {
    this.publicationDate = publicationDate;
  }

  /**
   * @return the unpublicationDate
   */
  public String getUnpublicationDate() {
    return unpublicationDate;
  }

  /**
   * @param unpublicationDate the unpublicationDate to set
   */
  public void setUnpublicationDate(final String unpublicationDate) {
    this.unpublicationDate = unpublicationDate;
  }

  /**
   * Indicate if this webcast DTO has categories set.
   * 
   * @return <code>true</code> or <code>false</code>
   * 
   */
  public boolean hasCategories() {
    return categories != null && categories.size() > 0;
  }

  /**
   * Indicates if this webcast has visibility set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasVisibility() {
    return StringUtils.isNotEmpty(visibility);
  }

  /**
   * Indicates if this webcast has provider set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasProvider() {
    return provider != null && StringUtils.isNotEmpty(provider.getName());
  }

  /**
   * Indicates if this webcast has format set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasFormat() {
    return StringUtils.isNotEmpty(format);
  }

  /**
   * Indicates if this webcast has scheduled date set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasScheduled() {
    return StringUtils.isNotEmpty(scheduled);
  }

  /**
   * Indicates if this webcast has duration set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasDuration() {
    return duration != null;
  }

  /**
   * Indicates if this webcast has status set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasStatus() {
    return StringUtils.isNotEmpty(status);
  }

  /**
   * Indicates if this webcast has publish status set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasPublishStatus() {
    return StringUtils.isNotEmpty(publishStatus);
  }

  /**
   * Indicates if this webcast has publication date set. The publication date can be set to an empty element
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasPublicationDate() {
    return publicationDate != null;
  }

  /**
   * Indicates if this webcast has unpublication date set. The publication date can be set to an empty element
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasUnpublicationDate() {
    return unpublicationDate != null;
  }

  /**
   * @return the webcastSyndicationDto
   */
  public WebcastSyndicationDto getSyndication() {
    return syndication;
  }

  /**
   * @param syndication the webcastSyndicationDto to set
   */
  @XmlElement(name = "syndication")
  public void setSyndication(final WebcastSyndicationDto syndication) {
    this.syndication = syndication;
  }

  /**
   * Validates this dto with given validator.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<WebcastDto> validator) {
    validator.validate(this);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this);
    builder.append("id", id);
    builder.append("title", title);
    builder.append("description", description);
    builder.append("keywords", keywords);
    return builder.toString();
  }
}
