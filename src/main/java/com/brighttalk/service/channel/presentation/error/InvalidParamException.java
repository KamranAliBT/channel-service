/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: InvalidParamException.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that provided param is invalid.
 */
@SuppressWarnings("serial")
public class InvalidParamException extends ApplicationException {

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public InvalidParamException(final String message, final ErrorCode errorCode) {
    super(message, errorCode.getName());
  }
}