/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastProviderConverter.java 69149 2013-10-02 09:17:11Z build $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.presentation.dto.WebcastProviderDto;

/**
 * Converter - Responsible for converting between WebcastProviderDto and Provider types.
 */
@Component
public class WebcastProviderConverter {

  /**
   * Converts a WebcastProviderDto to Provider.
   * 
   * @param webcastProviderDto to be converted
   * 
   * @return Provider
   */
  public Provider convert(final WebcastProviderDto webcastProviderDto) {
    return new Provider(webcastProviderDto.getName());
  }

  /**
   * Converts a Provider to WebcastProviderDto.
   * 
   * @param provider to be converted
   * 
   * @return dto
   */
  public WebcastProviderDto convert(final Provider provider) {

    final WebcastProviderDto webcastProviderDto = new WebcastProviderDto();
    webcastProviderDto.setName(provider.getName());

    return webcastProviderDto;
  }
}