/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationAuthorisationController.java 94590 2015-05-06 12:05:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.service.AuthorisationService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform authorisation operations on communication.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 * Authorisation can be requested on a single communication, identified by the id, and the business layer will decide if
 * the current user has the necessary authorisation to access this communication.
 */
@Controller
@RequestMapping(value = "/communication")
public class CommunicationAuthorisationController {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(CommunicationAuthorisationController.class);

  /**
   * Communication service
   */
  @Autowired
  private AuthorisationService authorisationService;

  /**
   * Communication finder
   */
  @Autowired
  private CommunicationFinder communicationFinder;

  /** Channel finder */
  @Autowired
  private ChannelFinder channelFinder;

  /**
   * Authorise the current user for access to a communication.
   * 
   * @param user The Current User
   * @param communicationId The Id of the communication to be authorised.
   * 
   * @throws UserAuthorisationException when user is not allowed to access given communiction.
   */
  @RequestMapping(value = "/{communicationId}/authorisation", method = RequestMethod.GET)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void authorise(@AuthenticatedUser final User user, @PathVariable final Long communicationId) {

    Communication communication = null;
    Channel channel = null;
    try {
      communication = communicationFinder.find(communicationId);
    } catch (CommunicationNotFoundException nfe) {
      throw new UserAuthorisationException("Communication [" + communicationId + "] doesnt exist.", nfe);
    }

    try {
      channel = channelFinder.find(communication.getChannelId());
    } catch (ChannelNotFoundException nfe) {
      throw new UserAuthorisationException("Channel [" + communication.getChannelId()
          + "] doesnt exist, for communication [" + communicationId + "].", nfe);
    }

    authorisationService.authoriseCommunicationAccess(user, channel, communication);

    logger.debug("User [" + user.getId() + "] is authorised to access the commnuication [" + communicationId
        + "] in channel [" + channel.getId() + "]");
  }

  /**
   * Sets the authorisation service of this controller.
   * 
   * @param authorisationService the authorisation service to be set.
   */
  public void setAuthorisationService(final AuthorisationService authorisationService) {
    this.authorisationService = authorisationService;
  }

  /**
   * Sets the communication finder of this controller.
   * 
   * @param communicationFinder the communication finder to be set.
   */
  public void setCommunicationFinder(final CommunicationFinder communicationFinder) {
    this.communicationFinder = communicationFinder;
  }

  /**
   * @param channelFinder the channelFinder to set
   */
  public void setChannelFinder(final ChannelFinder channelFinder) {
    this.channelFinder = channelFinder;
  }
}
