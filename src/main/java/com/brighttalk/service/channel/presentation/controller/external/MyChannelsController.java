/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MyChannelsController.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.presentation.dto.converter.context.builder.ExternalConverterContextBuilder;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.PresentationCriteria;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.ChannelsPublicDto;
import com.brighttalk.service.channel.presentation.dto.WebcastsTotalsDto;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelConverter;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastsTotalsConverter;
import com.brighttalk.service.channel.presentation.util.MyChannelsSearchCriteriaBuilder;
import com.brighttalk.service.channel.presentation.util.PresentationCriteriaBuilder;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls requesting lists of channels owned by the current user or the channels the current user is subscribed to.
 * 
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 */
@Controller
@RequestMapping(value = "/my")
public class MyChannelsController {

  private static final Set<String> ALLOWED_REQUEST_PARAMS = new HashSet<String>(Arrays.asList(
          ApiRequestParam.DATE_TIME_FORMAT, ApiRequestParam.PAGE_NUMBER, ApiRequestParam.PAGE_SIZE,
          ApiRequestParam.SORT_BY, ApiRequestParam.SORT_ORDER));

  /** Channel finder */
  @Autowired
  @Qualifier(value = "readOnlyChannelFinder")
  private ChannelFinder channelFinder;

  @Autowired
  @Qualifier(value = "readOnlyCommunicationFinder")
  private CommunicationFinder communicationFinder;

  @Autowired
  private ChannelConverter channelConverter;

  @Autowired
  private MyChannelsSearchCriteriaBuilder searchCriteriaBuilder;

  @Autowired
  private ExternalConverterContextBuilder contextBuilder;

  @Autowired
  private WebcastsTotalsConverter webcastsTotalsConverter;

  /** Presentation criteria builder */
  @Autowired
  private PresentationCriteriaBuilder presentationCriteriaBuilder;

  /** Brighttalk validator. */
  @Autowired
  private BrighttalkValidator validator;

  /**
   * Handles a request to find user owned channels using given search criteria context.
   * 
   * @param request The Request
   * @param user The Current User
   * @param formatter date formatter
   * 
   * @return ChannelsPublicDto
   */
  @RequestMapping(value = "/channels", method = RequestMethod.GET)
  @ResponseBody
  public ChannelsPublicDto getOwnedChannels(final HttpServletRequest request, @AuthenticatedUser final User user,
                                            @DateTimeFormat final DateTimeFormatter formatter) {

    PresentationCriteria presentationCriteria = presentationCriteriaBuilder.buildWithDefaultExpandedFeaturedWebcasts(request);
    validator.validate(presentationCriteria, ValidationException.class);

    MyChannelsSearchCriteria searchCriteria = searchCriteriaBuilder.build(request);

    PaginatedList<Channel> channels = this.channelFinder.findOwnedChannels(user, searchCriteria);

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);

    ChannelsPublicDto channelsDto = channelConverter.convertForPublic(channels, converterContext, presentationCriteria);

    return channelsDto;
  }

  /**
   * Handles a request to find user subscribed channels using given search criteria context.
   * 
   * @param request The Request
   * @param user The Current User
   * @param formatter date formatter
   * 
   * @return ChannelsPublicDto
   */
  @RequestMapping(value = "/channel_subscriptions", method = RequestMethod.GET)
  @ResponseBody
  public ChannelsPublicDto getSubscribedChannels(final HttpServletRequest request, @AuthenticatedUser final User user,
                                                 @DateTimeFormat final DateTimeFormatter formatter) {

    PresentationCriteria presentationCriteria = presentationCriteriaBuilder.buildWithDefaultExpandedFeaturedWebcasts(request);
    validator.validate(presentationCriteria, ValidationException.class);

    MyChannelsSearchCriteria searchCriteria = searchCriteriaBuilder.build(request);

    PaginatedList<Channel> channels = this.channelFinder.findSubscribedChannels(user, searchCriteria);

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);

    ChannelsPublicDto channelsDto = channelConverter.convertForPublic(channels, converterContext, presentationCriteria);

    return channelsDto;
  }

  /**
   * Handles requests to retrieve total numbers of webcasts in the user subscribed channels based on the status of the
   * webcast.
   * 
   * @param user The Current User
   * 
   * @return WebcastTotalsDto
   */
  @RequestMapping(value = "/channel_subscriptions/webcasts_totals", method = RequestMethod.GET)
  @ResponseBody
  public WebcastsTotalsDto getSubscribedChannelsWebcastTotals(@AuthenticatedUser final User user) {

    Map<String, Long> webcastTotals = communicationFinder.findSubscribedChannelsCommunicationsTotals(user.getId());
    WebcastsTotalsDto webcastTotalsDto = webcastsTotalsConverter.convert(webcastTotals);

    return webcastTotalsDto;
  }
}