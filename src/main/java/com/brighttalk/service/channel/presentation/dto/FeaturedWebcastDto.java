/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeaturedWebcastDto.java 57059 2012-11-06 17:22:01Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * FeaturedWebcastDto.
 */
public class FeaturedWebcastDto {

  private Long id;

  private String title;

  private String status;

  private String start;

  private Integer duration;

  private LinkDto thumbnailLink;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStart() {
    return start;
  }

  public void setStart(String start) {
    this.start = start;
  }

  public Integer getDuration() {
    return duration;
  }

  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  public LinkDto getThumbnailLink() {
    return thumbnailLink;
  }

  @XmlElement(name = "link")
  public void setThumbnailLink(LinkDto thumbnailLink) {
    this.thumbnailLink = thumbnailLink;
  }
}