/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ListNotBlankValidator.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.annotation.validator;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Channel Validator checking the supplied List of Strings is not null or empty.
 */
@Component
public class ListNotBlankValidator implements ConstraintValidator<ListNotBlank, List<String>> {

  private static final Logger LOGGER = Logger.getLogger(ListNotBlankValidator.class);

  @Override
  public void initialize(ListNotBlank listNotBlank) {
  }

  /**
   * Validating the supplied List of Strings is not null or empty.
   * 
   * @return true if valid, false otherwise.
   * <p>
   * {@inheritDoc}
   */
  @Override
  public boolean isValid(List<String> strValues, ConstraintValidatorContext arg1) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Validating the supplied String List [" + strValues + "].");
    }

    return strValues != null && !strValues.isEmpty();
  }

}
