/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: UpdateContactDetails.java 88863 2015-01-27 11:35:19Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validation.scenario;

/**
 * Validation group type for updating contact details scenario.
 */
public interface UpdateContactDetails {

}
