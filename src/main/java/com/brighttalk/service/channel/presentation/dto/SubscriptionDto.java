/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionDto.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Dto to hold the subscription details.
 */
public class SubscriptionDto {

  private Long id;

  private UserDto user;

  private SubscriptionContextDto context;

  private String referral;

  private String lastSubscribed;

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(final UserDto user) {
    this.user = user;
  }

  public String getReferral() {
    return referral;
  }

  public void setReferral(final String referral) {
    this.referral = referral;
  }

  public String getLastSubscribed() {
    return lastSubscribed;
  }

  public void setLastSubscribed(final String lastSubscribed) {
    this.lastSubscribed = lastSubscribed;
  }

  /**
   * @return the context
   */
  public SubscriptionContextDto getContext() {
    return context;
  }

  /**
   * @param context the context to set
   */
  public void setContext(SubscriptionContextDto context) {
    this.context = context;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("id", id);
    builder.append("user", user);
    builder.append("context", context);
    builder.append("referral", referral);
    builder.append("lastSubscribed", lastSubscribed);
    return builder.toString();
  }

}
