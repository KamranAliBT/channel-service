/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContractPeriodController.java 101600 2015-10-19 12:50:50Z kali $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import com.brighttalk.common.error.ErrorDto;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthenticationException;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.*;
import com.brighttalk.service.channel.business.service.channel.ContractPeriodService;
import com.brighttalk.service.channel.presentation.dto.ContractPeriodDto;
import com.brighttalk.service.channel.presentation.dto.ContractPeriodsDto;
import com.brighttalk.service.channel.presentation.dto.converter.ContractPeriodConverter;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.CreateContractPeriod;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateContractPeriod;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform CRUD operations on contract periods.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 */
@Controller
public class ContractPeriodController {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(ContractPeriodController.class);

  /** Contract period service */
  private ContractPeriodService contractPeriodService;

  /** Brighttalk validator. */
  private BrighttalkValidator validator;

  private ContractPeriodConverter contractPeriodConverter;

  @Autowired
  public ContractPeriodController(
      ContractPeriodService contractPeriodService, BrighttalkValidator validator,
      ContractPeriodConverter contractPeriodConverter) {
    this.contractPeriodService = contractPeriodService;
    this.validator = validator;
    this.contractPeriodConverter = contractPeriodConverter;
  }

  /**
   * Handles a request to create a contract period.
   *
   * @param user The current User
   * @param contractPeriodDto The contract period details to create.
   * @param formatter used to format dates in ContractPeriodDto response
   *
   * @return The created {@link ContractPeriodDto} object.
   *
   * @throws ValidationException if the given {@link ContractPeriodDto} is invalid
   * @throws UserAuthorisationException if the current user is not authorised to create a contract period
   * @throws ResourceNotFoundException if the channel that this contract period should belong to does not exist
   */
  @RequestMapping(value = "/contract_period", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  @ResponseStatus(value = HttpStatus.CREATED)
  public ContractPeriodDto create(@AuthenticatedUser final User user,
      @RequestBody final ContractPeriodDto contractPeriodDto, @DateTimeFormat final DateTimeFormatter formatter) {

    validator.validate(contractPeriodDto, CreateContractPeriod.class);

    ContractPeriod contractPeriod = contractPeriodConverter.convert(contractPeriodDto);
    ContractPeriod createdContractPeriod = contractPeriodService.create(user, contractPeriod);
    ContractPeriodDto createdChannelDto = contractPeriodConverter.convert(createdContractPeriod, formatter);
    return createdChannelDto;
  }

  /**
   * Handles a request to update a contract period.
   *
   * @param user The current User
   * @param contractPeriodId The Id of the contract period to be updated.
   * @param contractPeriodDto The contract period details to update.
   * @param formatter used to format dates in ContractPeriodDto response
   *
   * @return The updated {@link ContractPeriodDto} object.
   *
   * @throws ValidationException if the given {@link ContractPeriodDto} is invalid
   * @throws UserAuthorisationException if the current user is not authorised to create a contract period
   * @throws ResourceNotFoundException if the channel that this contract period should belong to does not exist, or the
   * contract period ID itself points to a contract period that does not exist
   */
  @RequestMapping(value = "/contract_period/{contractPeriodId}", method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ContractPeriodDto update(@AuthenticatedUser final User user, @PathVariable final String contractPeriodId,
      @Valid @RequestBody final ContractPeriodDto contractPeriodDto,
      @DateTimeFormat final DateTimeFormatter formatter) {

    contractPeriodDto.setId(contractPeriodId);
    validator.validate(contractPeriodDto, UpdateContractPeriod.class);

    ContractPeriod contractPeriod = contractPeriodConverter.convert(contractPeriodDto);
    ContractPeriod updatedContractPeriod = contractPeriodService.update(user, contractPeriod);
    ContractPeriodDto updatedContractPeriodDto = contractPeriodConverter.convert(updatedContractPeriod, formatter);
    return updatedContractPeriodDto;
  }

  /**
   * Handles a request to find a contract period.
   *
   * @param user The Current User
   * @param contractPeriodId The Id of the channel to be retrieved.
   * @param formatter used to format dates in ContractPeriodDto response
   *
   * @return The found {@link ContractPeriodDto} object.
   *
   * @throws ResourceNotFoundException if the contract period does not exist
   */
  @RequestMapping(value = "/contract_period/{contractPeriodId}", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ContractPeriodDto findById(@AuthenticatedUser final User user, @PathVariable final String contractPeriodId,
      @DateTimeFormat final DateTimeFormatter formatter) {

    ContractPeriod contractPeriod = null;
    contractPeriod =
        contractPeriodService.find(user, parseLongFromString(contractPeriodId, ErrorCode.INVALID_CONTRACT_PERIOD_ID));
    ContractPeriodDto contractPeriodDto = contractPeriodConverter.convert(contractPeriod, formatter);

    return contractPeriodDto;
  }

  /**
   * Handles a request to find all contract periods for a channel.
   *
   * @param user The Current User
   * @param channelId The Id of the channel for which the contract periods are to be retrieved.
   * @param formatter used to format dates in ContractPeriodDto response
   *
   * @return {@link ContractPeriodsDto} object.
   *
   * @throws ResourceNotFoundException if the channel does not exist
   */
  @RequestMapping(value = "/channel/{channelId}/contract_periods", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ContractPeriodsDto findAllByChannel(@AuthenticatedUser final User user, @PathVariable final String channelId,
      @DateTimeFormat final DateTimeFormatter formatter) {

    //find all by Channel
    List<ContractPeriod> contractPeriods = null;
    contractPeriods =
        contractPeriodService.findByChannelId(user, parseLongFromString(channelId, ErrorCode.INVALID_CHANNEL_ID));

    //convert all to collection DTO and return
    ContractPeriodsDto contractPeriodsDto = contractPeriodConverter.convert(contractPeriods, formatter);
    return contractPeriodsDto;
  }

  /**
   * Handles a request to delete contract period.
   *
   * @param user authenticated user
   * @param contractPeriodId id of the contract period to be deleted
   *
   * @throws UserAuthorisationException if the current user is not authorised to create a contract period
   * @throws ResourceNotFoundException if the contract period that does not exist
   */
  @RequestMapping(value = "/contract_period/{contractPeriodId}", method = RequestMethod.DELETE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void delete(@AuthenticatedUser final User user, @PathVariable final String contractPeriodId) {
    contractPeriodService.delete(user, parseLongFromString(contractPeriodId, ErrorCode.INVALID_CONTRACT_PERIOD_ID));
  }

  /**
   * Converts a {@link String} into a {@link Long}
   *
   * @param string the {@link String} to be converted
   * @param errorCode the {@link ErrorCode} to be thrown if the conversion cannot be done, e.g. the {@link String} is
   * invalid
   *
   * @return a {@link Long} representation of the given {@link String}
   *
   * @throws {@link BadRequestException} with the given error code if the string cannot be parsed into a Long
   */
  private Long parseLongFromString(String string, ErrorCode errorCode) {
    try {
      long value = Long.parseLong(string);
      if(value < 0){
        throw new BadRequestException("Not a valid number.", errorCode);
      }
      return value;
    } catch (NumberFormatException e) {
      throw new BadRequestException("Not a valid number.", errorCode);
    }
  }

  /**
   * Exception handler method handles {@link BadRequestException} thrown from this API.
   *
   * @param ex The {@link BadRequestException} handled by this method.
   *
   * @return {@link ErrorDto} containing {@link ErrorCode} of this BadRequestException.
   */
  @ExceptionHandler(value = BadRequestException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  private ErrorDto handleException(final BadRequestException ex) {
    return new ErrorDto(ex.getUserErrorCode(), ex.getMessage());
  }

  /**
   * Exception handler method handles {@link ContractPeriodNotFoundException} thrown from this API.
   *
   * @param ex The {@link ContractPeriodNotFoundException} handled by this method.
   *
   * @return {@link ErrorDto} containing {@link ErrorCode} of this ResourceNotFoundException.
   */
  @ExceptionHandler(value = ContractPeriodNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  private ErrorDto handleException(final ContractPeriodNotFoundException ex) {
    return new ErrorDto(ErrorCode.CONTRACT_PERIOD_NOT_FOUND.getName(), ex.getMessage());
  }

  /**
   * Exception handler method handles {@link ChannelNotFoundException} thrown from this API.
   *
   * @param ex The {@link ChannelNotFoundException} handled by this method.
   *
   * @return {@link ErrorDto} containing {@link ErrorCode} of this ChannelNotFoundException.
   */
  @ExceptionHandler(value = ChannelNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  private ErrorDto handleException(final ChannelNotFoundException ex) {
    return new ErrorDto(ErrorCode.CHANNEL_NOT_FOUND.getName(), ex.getMessage());
  }

  /**
   * Exception handler method handles {@link InvalidContractPeriodException} thrown from this API.
   *
   * @param ex The {@link InvalidContractPeriodException} handled by this method.
   *
   * @return {@link ErrorDto} containing {@link ErrorCode} of this InvalidContractPeriodException.
   */
  @ExceptionHandler(value = InvalidContractPeriodException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  private ErrorDto handleException(final InvalidContractPeriodException ex) {
    return new ErrorDto(ex.getUserErrorCode(), ex.getMessage());
  }

  /**
   * Exception handler method handles {@link OverlappingContractPeriod} thrown from this API.
   *
   * @param ex The {@link OverlappingContractPeriod} handled by this method.
   *
   * @return {@link ErrorDto} containing {@link ErrorCode} of this OverlappingContractPeriod.
   */
  @ExceptionHandler(value = OverlappingContractPeriod.class)
  @ResponseStatus(value = HttpStatus.CONFLICT)
  @ResponseBody
  private ErrorDto handleException(final OverlappingContractPeriod ex) {
    return new ErrorDto(ex.getUserErrorCode(), ex.getMessage());
  }

  /**
   * Exception handler method handles {@link UserAuthorisationException} thrown from this API.
   *
   * @param ex The {@link UserAuthorisationException} handled by this method.
   *
   * @return {@link ErrorDto} containing {@link ErrorCode} of this UserAuthorisationException.
   */
  @ExceptionHandler(value = UserAuthorisationException.class)
  @ResponseStatus(value = HttpStatus.FORBIDDEN)
  @ResponseBody
  private ErrorDto handleException(final UserAuthorisationException ex) {
    return new ErrorDto(ex.getUserErrorCode(), ex.getMessage());
  }

  /**
   * Exception handler method handles {@link ValidationException} thrown from this API.
   *
   * @param ex The {@link ValidationException} handled by this method.
   *
   * @return {@link ErrorDto} containing {@link ErrorCode} of this ValidationException.
   */
  @ExceptionHandler(value = ValidationException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  private ErrorDto handleException(final ValidationException ex) {
    return new ErrorDto(ex.getUserErrorCode(), ex.getMessage());
  }

  /**
   * Exception handler method handles {@link UserAuthenticationException} thrown from this API.
   *
   * @param ex The {@link UserAuthenticationException} handled by this method.
   *
   * @return {@link ErrorDto} containing {@link ErrorCode} of this UserAuthenticationException.
   */
  @ExceptionHandler(value = UserAuthenticationException.class)
  @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
  @ResponseBody
  private ErrorDto handleException(final UserAuthenticationException ex) {
    return new ErrorDto(ex.getUserErrorCode(), ex.getMessage());
  }

}
