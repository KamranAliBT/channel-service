/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelsDto.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * ChannelsDto. DTO object representing list of channels DTO. The object is used to wrap list of ChannelDto.
 */
@XmlRootElement(name = "channels")
public class ChannelsDto {

  private List<ChannelDto> channels;
  
  /**
   * @return the collection of ChannelDto objects or empty collection if this.channels is null.
   */
  public List<ChannelDto> getChannels() {
    if (channels == null) {
      channels = new ArrayList<ChannelDto>();
    }
    return channels;
  }

  /**
   * Sets channels property. Requires @XmlElement to make sure that list of channel elements will be returned. In case
   * the @XmlElement is not set correctly <channels><channelDto> will be returned.
   * 
   * @param channels list of channelDto
   */
  @XmlElement(name = "channel")
  public void setChannels(List<ChannelDto> channels) {
    this.channels = channels;
  }
}
