/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: ISO8601DateValidator.java 70202 2013-10-24 17:31:29Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validation.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import com.brighttalk.common.time.DateTimeParser;

/**
 * Validator implementation which ensures a non-null date string is of correct format.
 */
public class ISO8601DateValidator implements ConstraintValidator<ISO8601Date, String> {

  private static final DateTimeParser DATE_TIME_PARSER = new DateTimeParser();

  @Override
  public void initialize(final ISO8601Date constaint) {
    // do nothing
  }

  @Override
  public boolean isValid(final String date, final ConstraintValidatorContext context) {

    if (StringUtils.isEmpty(date)) {
      return true;
    }

    return DATE_TIME_PARSER.isValidFormat(date);
  }
}