/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: NotAuthenticatedException.java 57384 2012-11-09 16:58:22Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Exception thrown to signal that an operation failed because it was not made by or on behalf of an authenticated
 * client.
 */
@SuppressWarnings("serial")
public class NotAuthenticatedException extends ApplicationException {

  /**
   * Creates an instance of this class with a default technical diagnostics message.
   */
  public NotAuthenticatedException() {
    super("Operation must be performed by or on behalf of an authenticated user.",
        ErrorCode.NOT_AUTHENTICATED.getName());
  }

  /**
   * Creates an instance of this class with a default technical diagnostics message.
   * 
   * @param message the message
   */
  public NotAuthenticatedException(final String message) {
    super(message, ErrorCode.NOT_AUTHENTICATED.getName());
  }

}
