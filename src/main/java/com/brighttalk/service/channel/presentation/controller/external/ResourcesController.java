/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourcesController.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.common.user.User;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.service.ResourceService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.presentation.dto.ResourcesDto;
import com.brighttalk.service.channel.presentation.dto.converter.ResourceConverter;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform operations on collection of {@link Resource resource}.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 * The resources can only be get or reordered by authenticated and authorized user who is channel owner, manager or
 * presenter.
 */
@Controller
@RequestMapping(value = "/channel/{channelId}/communication/{communicationId}/resources")
public class ResourcesController {

  /**
   * Resource service
   */
  @Autowired
  private ResourceService resourceService;
  
  /** Channel finder */
  @Autowired
  private ChannelFinder channelFinder;

  /**
   * Resource converter
   */
  @Autowired
  private ResourceConverter converter;

  /**
   * Handles a request to find resources.
   * 
   * @param user the user making the request
   * @param channelId id of the channel
   * @param communicationId the communication to find the resources on
   * 
   * @return collection of resources
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when channel or
   * communication does not exist.
   */
  @RequestMapping(method = RequestMethod.GET)
  @ResponseBody
  public ResourcesDto getResources(@AuthenticatedUser User user, @PathVariable Long channelId,
      @PathVariable Long communicationId) {

    Channel channel = this.channelFinder.find(channelId);
    List<Resource> resources = resourceService.findResources(user, channel, communicationId);
    ResourcesDto resourcesDto = converter.convertResources(resources);

    return resourcesDto;
  }

  /**
   * Handles a request to update resources order. The request should contain resources in the new order only with their
   * id.
   * 
   * @param user the user making the request
   * @param channelId id of the channel
   * @param communicationId the communication to update the resources order on
   * @param resourcesDto resources in the new order
   * 
   * @return collection of resources in updated order
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when channel or communication does not
   * exist.
   * @throws com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException when communication is LIVE or
   * in 15 minute window to go-live time. The resources cannot be reordered during this time.
   * @throws com.brighttalk.service.channel.business.error.MustBeInMasterChannelException when the communication is not in
   * a master channel.
   */
  @RequestMapping(method = RequestMethod.PUT)
  @ResponseBody
  public ResourcesDto updateResourcesOrder(@AuthenticatedUser User user, @PathVariable Long channelId,
      @PathVariable Long communicationId, @RequestBody ResourcesDto resourcesDto) {

    Channel channel = this.channelFinder.find(channelId);
    List<Resource> resourcesToBeUpdated = converter.convertResources(resourcesDto);
    List<Resource> updatedResources = resourceService.updateResourcesOrder(user, channel, communicationId,
        resourcesToBeUpdated);
    ResourcesDto resources = converter.convertResources(updatedResources);

    return resources;
  }

}
