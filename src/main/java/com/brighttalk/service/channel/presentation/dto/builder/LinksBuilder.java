/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: LinksBuilder.java 69583 2013-10-10 16:51:20Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.utils.HrefBuilderUtil;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.LinkDto;

/**
 * Converter - Responsible for building the link dtos.
 */
@Component
public class LinksBuilder {

  /**
   * Build the links for a given paginated list.
   * 
   * @param paginatedList paginated list
   * @param converterContext context converter
   * 
   * @return links
   */
  public List<LinkDto> build(final PaginatedList<?> paginatedList, final ConverterContext converterContext) {

    List<LinkDto> links = new ArrayList<LinkDto>();

    if (includeLinks(paginatedList)) {
      if (paginatedList.hasPreviousPage()) {
        links.add(addPreviousPageLink(paginatedList, converterContext));
      }

      if (paginatedList.hasNextPage()) {
        links.add(addNextPageLink(paginatedList, converterContext));
      }
    }

    return links;
  }

  private boolean includeLinks(final PaginatedList<?> channels) {
    return channels.hasNextPage() || channels.hasPreviousPage();
  }

  private LinkDto addPreviousPageLink(final PaginatedList<?> paginatedList, final ConverterContext converterContext) {
    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(paginatedList.getCurrentPageNumber() - 1));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(paginatedList.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_PREVIOUS);

    return linkDto;
  }

  private LinkDto addNextPageLink(final PaginatedList<?> paginatedList, final ConverterContext converterContext) {
    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(paginatedList.getCurrentPageNumber() + 1));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(paginatedList.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_NEXT);

    return linkDto;
  }

}
