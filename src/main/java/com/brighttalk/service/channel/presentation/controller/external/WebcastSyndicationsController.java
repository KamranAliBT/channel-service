/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: WebcastSyndicationsController.java 99470 2015-08-24 12:38:22Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndications;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.service.communication.CommunicationSyndicationService;
import com.brighttalk.service.channel.presentation.controller.base.BaseController;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationsDto;
import com.brighttalk.service.channel.presentation.dto.syndication.converter.WebcastSyndicationConverter;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls related to communication syndications.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller
@RequestMapping(value = "/channel/{channelId}/communication/{commId}/syndications")
public class WebcastSyndicationsController extends BaseController {
  private static final Logger LOGGER = Logger.getLogger(WebcastSyndicationsController.class);

  @Autowired
  private CommunicationSyndicationService commSyndicationService;

  @Autowired
  private WebcastSyndicationConverter syndicationsConverter;

  /**
   * Handle a request to retrieve all the syndication details for the identified communication in the identified
   * channel.
   * <p>
   * The reported {@link WebcastSyndicationsDto communication syndications} resource will contain the communication
   * master channel details, the communication details (title) in the master channel and a list of all the syndicated in
   * (distributed) channels syndication details, this list will contain all the distributed channels syndication details
   * if the a request was made for a master channel, otherwise the list will only contain the identified distributed
   * channel syndication details if the request was made for a distributed channel.
   * <p>
   * Each {@link WebcastSyndicationDto distributed syndication} contains the syndicated channel details, the syndication
   * authorisation details and the communication activities statistics in this distributed channel (optional only
   * present when the request made for a master channel).
   * 
   * @param user the user requesting the get.
   * @param requestedChannelId The requested channel Id.
   * @param requestedCommId The requested communication Id.
   * @param formatter the date formatter.
   * @return {@link WebcastSyndicationsDto} containing the syndication details of the identified communication.
   * @throws ChannelNotFoundException in case of the supplied channel does not identify an existing channel.
   * @throws CommunicationNotFoundException in case of the supplied communication does not identify an existing
   * communication.
   * @throws UserAuthorisationException in case of the supplied user not authorised on the supplied channel.
   */
  @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public WebcastSyndicationsDto get(@AuthenticatedUser final User user,
      @PathVariable("channelId") final String requestedChannelId,
      @PathVariable("commId") final String requestedCommId,
      @DateTimeFormat final DateTimeFormatter formatter) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Recieved request to retrieve syndications details for communication [" + requestedCommId
          + "] belonging to channel [" + requestedChannelId + "].");
    }

    long channelId = convertChannelId(requestedChannelId);
    long communicationId = convertWebcastId(requestedCommId);

    // Find the identified channel details and validate supplied user authorised on this channel.
    Channel channel = channelFinder.find(user, channelId);
    // Find and validate the identified communication exists.
    Communication communication = communicationFinder.find(channel, communicationId);

    CommunicationSyndications communicationSyndications = this.commSyndicationService.get(channel, communication);
    WebcastSyndicationsDto commSyndicationsDto = syndicationsConverter.convert(communicationSyndications,
        formatter);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully retrieved communication syndications details [" + communicationSyndications
          + "] for communication [" + requestedCommId + "] belonging to channel [" + requestedChannelId + "].");
    }
    return commSyndicationsDto;
  }
}
