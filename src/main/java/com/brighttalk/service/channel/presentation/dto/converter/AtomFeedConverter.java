/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AtomFeedConverter.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.domain.channel.PaginatedChannelFeed;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.atom.AtomFeedDto;
import com.brighttalk.service.channel.presentation.dto.atom.FeedEntryDto;
import com.brighttalk.service.channel.presentation.dto.converter.util.ThreadSafeSimpleDateFormat;

/**
 * Converter - Responsible for converting between AtomFeedDto and business types.
 */
@Component
public class AtomFeedConverter {

  private static final Logger LOGGER = Logger.getLogger(AtomFeedConverter.class);

  /**
   * The date pattern used in the last updated nodes of the feed.
   */
  public static final String LAST_UPDATED_DATE_PATTERN = "yyyy-MM-dd'T'kk:mm:ss'Z'";

  private static final String QUESTION_SIGN = "?";

  private static final String AMPERSAND_SIGN = "&";

  private static final String PAGE = "page=";

  private static final String SIZE = "size=";

  private static final String WEBCAST_STATUS_FILTER = "webcastStatus=";

  private static final char WEBCAST_STATUS_FILTER_DELIMITER = ',';

  @Autowired
  private AtomFeedEntryConverter entryConverter;

  public AtomFeedEntryConverter getEntryConverter() {
    return entryConverter;
  }

  public void setEntryConverter(AtomFeedEntryConverter entryConverter) {
    this.entryConverter = entryConverter;
  }

  /**
   * Converts a channel and a collection of communications to an atom dto.
   * 
   * @param feed the feed to convert
   * 
   * @return {@link AtomFeedDto}
   */
  public AtomFeedDto convert(PaginatedChannelFeed feed) {

    if (feed.isEmpty()) {
      return new AtomFeedDto();
    }

    Channel channel = feed.getChannel();

    AtomFeedDto atomFeedDto = new AtomFeedDto();

    String constructedChannelId = createChannelId(channel);
    atomFeedDto.setId(constructedChannelId);
    atomFeedDto.setTitle(channel.getTitle());
    atomFeedDto.setDescription(channel.getDescription());
    atomFeedDto.setSubtitle(channel.getStrapline());

    ThreadSafeSimpleDateFormat format = new ThreadSafeSimpleDateFormat(LAST_UPDATED_DATE_PATTERN);
    String lastUpdatedString = format.format(channel.getLastUpdated());
    atomFeedDto.setUpdated(lastUpdatedString);

    List<LinkDto> links = convertLinks(channel, feed);
    atomFeedDto.setLinks(links);

    List<Communication> communications = feed.getAllCommunications();
    if (communications != null && communications.size() > 0) {

      List<FeedEntryDto> entries = new ArrayList<FeedEntryDto>();

      for (Communication communication : communications) {

        FeedEntryDto entry = entryConverter.convert(communication);
        entries.add(entry);
      }

      atomFeedDto.setEntries(entries);
    }

    return atomFeedDto;
  }

  private List<LinkDto> convertLinks(Channel channel, PaginatedChannelFeed feed) {

    List<LinkDto> links = new ArrayList<LinkDto>();
    links.add(createCurrentPageFeedUrlLink(channel, feed));

    if (channel.hasUrl()) {

      links.add(createChannelUrlLink(channel));
    }

    // If only one page - no pagination links
    if (feed.getNumberOfPages() <= 1) {
      return links;
    }

    // Include the first and previous page links - unless we are already on the first page
    if (!feed.isFirstPage()) {

      links.add(createFirstPageFeedUrlLink(channel, feed));
      links.add(createPreviousPageFeedUrlLink(channel, feed));
    }

    // Include the last and next page links - unless we are already on the last page
    if (!feed.isLastPage()) {

      links.add(createLastPageFeedUrlLink(channel, feed));
      links.add(createNextPageFeedUrlLink(channel, feed));
    }

    return links;
  }

  /**
   * Get a constructed ID for a channel Feed.
   * 
   * @param channel details
   * 
   * @return the id for the channel
   */
  private String createChannelId(Channel channel) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(channel.getCreated());
    int year = calendar.get(Calendar.YEAR);

    StringBuilder builder = new StringBuilder();
    builder.append(AtomFeedDto.BRIGHTTALK_TAG).append(year);
    builder.append(":channel:").append(channel.getId());

    return builder.toString();
  }

  /**
   * Construct {@link LinkDto} for channel url.
   *
   * @param channel details
   * 
   * @return dto
   */
  private LinkDto createChannelUrlLink(Channel channel) {

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(channel.getUrl());
    linkDto.setRel(LinkDto.RELATIONSHIP_ALTERNATE);
    linkDto.setType(LinkDto.TYPE_TEXT_HTML);

    return linkDto;
  }

  /**
   * Construct {@link LinkDto} for channel atom feed url.
   *
   * @param channel details
   * @param feed details
   * 
   * @return dto
   */
  private LinkDto createCurrentPageFeedUrlLink(Channel channel, PaginatedChannelFeed feed) {

    LinkDto linkDto = new LinkDto();

    StringBuilder hrefBuilder = new StringBuilder();
    hrefBuilder.append(channel.getFeedUrlAtom()).append(QUESTION_SIGN);
    hrefBuilder.append(PAGE).append(feed.getCurrentPageNumber()).append(AMPERSAND_SIGN);
    hrefBuilder.append(SIZE).append(feed.getPageSize());
    appendCommunicationStatusFilterToUrlIfNeeded(hrefBuilder, feed.getCommunicationStatusFilter());

    linkDto.setHref(hrefBuilder.toString());
    linkDto.setRel(LinkDto.RELATIONSHIP_SELF);
    linkDto.setType(LinkDto.TYPE_ATOM_XML);

    return linkDto;
  }

  /**
   * Construct {@link LinkDto} for channel atom feed first page url.
   *
   * @param channel details
   * @param feed details
   * 
   * @return dto
   */
  private LinkDto createFirstPageFeedUrlLink(Channel channel, PaginatedChannelFeed feed) {

    LinkDto linkDto = new LinkDto();

    StringBuilder hrefBuilder = new StringBuilder();
    hrefBuilder.append(channel.getFeedUrlAtom()).append(QUESTION_SIGN);
    hrefBuilder.append(PAGE).append(feed.getFirstPageNumber()).append(AMPERSAND_SIGN);
    hrefBuilder.append(SIZE).append(feed.getPageSize());
    appendCommunicationStatusFilterToUrlIfNeeded(hrefBuilder, feed.getCommunicationStatusFilter());

    linkDto.setHref(hrefBuilder.toString());
    linkDto.setRel(LinkDto.RELATIONSHIP_FIRST);
    linkDto.setType(LinkDto.TYPE_ATOM_XML);

    return linkDto;
  }

  /**
   * Construct {@link LinkDto} for channel atom feed last page url.
   *
   * @param channel details
   * @param feed details
   * 
   * @return dto
   */
  private LinkDto createLastPageFeedUrlLink(Channel channel, PaginatedChannelFeed feed) {

    LinkDto linkDto = new LinkDto();

    StringBuilder hrefBuilder = new StringBuilder();
    hrefBuilder.append(channel.getFeedUrlAtom()).append(QUESTION_SIGN);
    hrefBuilder.append(PAGE).append(feed.getLastPageNumber()).append(AMPERSAND_SIGN);
    hrefBuilder.append(SIZE).append(feed.getPageSize());
    appendCommunicationStatusFilterToUrlIfNeeded(hrefBuilder, feed.getCommunicationStatusFilter());

    linkDto.setHref(hrefBuilder.toString());
    linkDto.setRel(LinkDto.RELATIONSHIP_LAST);
    linkDto.setType(LinkDto.TYPE_ATOM_XML);

    return linkDto;
  }

  /**
   * Construct {@link LinkDto} for channel atom feed next page url.
   *
   * @param channel details
   * @param feed details
   * 
   * @return dto
   */
  private LinkDto createNextPageFeedUrlLink(Channel channel, PaginatedChannelFeed feed) {

    LinkDto linkDto = new LinkDto();

    StringBuilder hrefBuilder = new StringBuilder();
    hrefBuilder.append(channel.getFeedUrlAtom()).append(QUESTION_SIGN);
    hrefBuilder.append(PAGE).append(feed.getNextPageNumber()).append(AMPERSAND_SIGN);
    hrefBuilder.append(SIZE).append(feed.getPageSize());
    appendCommunicationStatusFilterToUrlIfNeeded(hrefBuilder, feed.getCommunicationStatusFilter());

    linkDto.setHref(hrefBuilder.toString());
    linkDto.setRel(LinkDto.RELATIONSHIP_NEXT);
    linkDto.setType(LinkDto.TYPE_ATOM_XML);

    return linkDto;
  }

  /**
   * Construct {@link LinkDto} for channel atom feed previous page url.
   *
   * @param channel details
   * @param feed details
   * 
   * @return dto
   */
  private LinkDto createPreviousPageFeedUrlLink(Channel channel, PaginatedChannelFeed feed) {

    LinkDto linkDto = new LinkDto();

    StringBuilder hrefBuilder = new StringBuilder();
    hrefBuilder.append(channel.getFeedUrlAtom()).append(QUESTION_SIGN);
    hrefBuilder.append(PAGE).append(feed.getPreviousPageNumber()).append(AMPERSAND_SIGN);
    hrefBuilder.append(SIZE).append(feed.getPageSize());
    appendCommunicationStatusFilterToUrlIfNeeded(hrefBuilder, feed.getCommunicationStatusFilter());

    linkDto.setHref(hrefBuilder.toString());
    linkDto.setRel(LinkDto.RELATIONSHIP_PREVIOUS);
    linkDto.setType(LinkDto.TYPE_ATOM_XML);

    return linkDto;
  }

  private void appendCommunicationStatusFilterToUrlIfNeeded(StringBuilder url,
      List<CommunicationStatusFilter> webcastStatuses) {

    if (CollectionUtils.isEmpty(webcastStatuses)) {
      LOGGER.debug("No communication status filter to add to URL");
    } else {
      String webcastStatusAsString = buildWebcastStatusFilterAsString(webcastStatuses);

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Adding communication status filter to URL: " + webcastStatusAsString);
      }

      url.append(AMPERSAND_SIGN).append(WEBCAST_STATUS_FILTER).append(webcastStatusAsString);
    }
  }

  private String buildWebcastStatusFilterAsString(List<CommunicationStatusFilter> webcastStatuses) {

    List<String> webcastStatusFilterListAsStrings = new ArrayList<>();

    for (CommunicationStatusFilter webcastStatus : webcastStatuses) {

      webcastStatusFilterListAsStrings.add(webcastStatus.name().toLowerCase());
    }

    return StringUtils.join(webcastStatusFilterListAsStrings, WEBCAST_STATUS_FILTER_DELIMITER);
  }
}