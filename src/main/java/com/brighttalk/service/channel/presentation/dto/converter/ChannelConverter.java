/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelConverter.java 96319 2015-06-09 12:25:26Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.utils.HrefBuilderUtil;
import com.brighttalk.service.channel.business.domain.Category;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Keywords;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.PresentationCriteria;
import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.CategoryDto;
import com.brighttalk.service.channel.presentation.dto.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.ChannelPublicDto;
import com.brighttalk.service.channel.presentation.dto.ChannelsPublicDto;
import com.brighttalk.service.channel.presentation.dto.FeaturedWebcastDto;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.StatisticsDto;
import com.brighttalk.service.channel.presentation.dto.SurveyDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;

/**
 * Converter - Responsible for converting between ChannelDto and Channel types.
 */
@Component
public class ChannelConverter {

  /**
   * Public feature list. Use @Resource annotation to get set of public annotation from config file.
   */
  private Set<Feature.Type> publicFeatures;

  @Autowired
  private FeatureConverter featureConverter;

  /**
   * Keywords utility class used to convert {@ink Channel} keywords from/to comma separated string to list of string
   * keywords.
   */
  @Autowired
  private Keywords keywords;

  /**
   * Converts a {@link ChannelDto} to {@link Channel}.
   * 
   * @param channelDto to convert
   * 
   * @return {@link Channel}
   */
  public Channel convert(final ChannelDto channelDto) {
    Validate.notNull(channelDto, "The supplied Channel DTO object must not be null.");

    Channel channel = new Channel();
    channel.setId(channelDto.getId());
    channel.setTitle(channelDto.getTitle());
    channel.setDescription(channelDto.getDescription());
    channel.setOrganisation(channelDto.getOrganisation());
    channel.setStrapline(channelDto.getStrapline());
    channel.setKeywords(channelDto.getKeywordsList());

    if (channelDto.getUser() != null) {
      channel.setOwnerUserId(channelDto.getUser().getId());
    }

    return channel;
  }

  /**
   * Converts a {@link Channel} to {@link ChannelDto}.
   * 
   * @param channel to convert.
   * @param formatter the formatter used to format dates.
   * 
   * @return {@link ChannelDto} or one of its subclasses, this is decided based on the supplied channelDtoClass
   * parameter.
   */
  public ChannelDto convert(final Channel channel, final DateTimeFormatter formatter) {
    Validate.notNull(channel, "The supplied Channel domain object must not be null.");

    ChannelDto channelDto = new ChannelDto(channel.getId());
    channelDto.setTitle(channel.getTitle());
    channelDto.setDescription(channel.getDescription());
    channelDto.setOrganisation(channel.getOrganisation());

    channelDto.setKeywords(keywords.convertListOfKeywordsToKeywordsString(channel.getKeywords()));
    channelDto.setKeywordsList(keywords.removeQuotes(channel.getKeywords()));

    channelDto.setStrapline(channel.getStrapline());
    channelDto.setUrl(channel.getUrl());
    channelDto.setCreated(formatter.convert(channel.getCreated()));
    channelDto.setLastUpdated(formatter.convert(channel.getLastUpdated()));
    channelDto.setCurrentType(channel.getType().getName());
    channelDto.setCreatedType(channel.getCreatedType().getName());
    channelDto.setPromoteable(channel.isPromotedOnHomepage());
    channelDto.setSearchVisibility(channel.getSearchable().name().toLowerCase());

    if (channel.hasStatistics()) {
      channelDto.setRating(channel.getStatistics().getRating());

      // statistics data
      StatisticsDto stats = new StatisticsDto();

      stats.setViewedSeconds(channel.getStatistics().getViewedFor());
      stats.setSubscribers(channel.getStatistics().getNumberOfSubscribers());
      stats.setUpcomingCommunications(channel.getStatistics().getNumberOfUpcomingCommunications());
      stats.setRecordedCommunications(channel.getStatistics().getNumberOfRecordedCommunications());
      channelDto.setStatistics(stats);

    }
    // channel owner
    channelDto.setUser(new UserDto(channel.getOwnerUserId()));

    // only set the pending type if it exists
    if (channel.isPendingApproval()) {
      channelDto.setPendingType(channel.getPendingType().getName());
    }

    // set optional field features
    if (channel.hasFeatures()) {
      channelDto.setFeatures(featureConverter.convertFeatures(channel.getFeatures()));
    }

    // set optional categories
    if (channel.hasCategories()) {
      channelDto.setCategories(convertCategories(channel.getCategories()));
    }

    if (channel.hasSurvey()) {
      channelDto.setSurvey(convertSurvey(channel.getSurvey()));
    }

    return channelDto;
  }

  /**
   * Convert the given channel into its Channel Public DTO and exposes only public Features.
   * 
   * @param channel the channel to convert
   * @param criteria presentation criteria
   * @param formatter the date time formatter
   * 
   * @return the channel public DTO for that channel.
   */
  public ChannelPublicDto convertForPublic(final Channel channel, final DateTimeFormatter formatter,
      final PresentationCriteria criteria) {

    ChannelPublicDto publicDto = new ChannelPublicDto(channel.getId());
    publicDto.setTitle(channel.getTitle());
    publicDto.setDescription(channel.getDescription());
    publicDto.setUrl(channel.getUrl());
    publicDto.setStrapline(channel.getStrapline());
    publicDto.setKeywords(keywords.convertListOfKeywordsToKeywordsString(channel.getKeywords()));
    publicDto.setOrganisation(channel.getOrganisation());

    if (channel.getSearchable() != null) {
      publicDto.setSearchVisibility(channel.getSearchable().name().toLowerCase());
    }

    if (channel.hasFeedUrlAtom()) {
      LinkDto linkDto = new LinkDto();
      linkDto.setHref(channel.getFeedUrlAtom());
      linkDto.setRel(LinkDto.RELATIONSHIP_SELF);
      linkDto.setType(LinkDto.TYPE_ATOM_XML);

      publicDto.setLink(linkDto);
    }

    if (channel.hasStatistics()) {
      publicDto.setRating(channel.getStatistics().getRating());

      // statistics data
      StatisticsDto stats = new StatisticsDto();

      stats.setViewedSeconds(channel.getStatistics().getViewedFor());
      stats.setSubscribers(channel.getStatistics().getNumberOfSubscribers());
      stats.setUpcomingCommunications(channel.getStatistics().getNumberOfUpcomingCommunications());
      stats.setRecordedCommunications(channel.getStatistics().getNumberOfRecordedCommunications());
      stats.setLiveCommunications(channel.getStatistics().getNumberOfLiveCommunications());
      publicDto.setStatistics(stats);
    }

    // set optional field features
    if (channel.hasFeatures()) {
      // Filter Public features only
      List<Feature> filteredfeatures = filterPublicFeatures(channel.getFeatures());
      publicDto.setFeatures(featureConverter.convertFeatures(filteredfeatures));
    }

    if (criteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)) {
      if (channel.hasFeaturedCommunications()) {
        publicDto.setFeaturedWebcasts(convertFeaturedWebcasts(channel.getFeaturedCommunications(), formatter));
      } else {
        publicDto.setFeaturedWebcasts(new ArrayList<FeaturedWebcastDto>());
      }
    }

    return publicDto;
  }

  /**
   * Convert the given list of channels into Channels Public DTO wrapper With Pagination links.
   * 
   * Depending on the details of the paginated channels the returned dto might also include navigation links.
   * 
   * @param channels cursorable list of channels
   * @param converterContext properties used in conversion
   * @param criteria presentation criteria
   * 
   * @return Public channels
   */
  public ChannelsPublicDto convertForPublic(final List<Channel> channels, final ConverterContext converterContext,
      final PresentationCriteria criteria) {

    List<ChannelPublicDto> channelsDtos = new ArrayList<ChannelPublicDto>();

    for (Channel channel : channels) {
      channelsDtos.add(convertForPublic(channel, converterContext.getDateTimeFormatter(), criteria));
    }

    ChannelsPublicDto publicDto = new ChannelsPublicDto();
    publicDto.setChannels(channelsDtos);

    return publicDto;
  }

  /**
   * Convert the given list of channels into Channels Public DTO wrapper With Pagination links.
   * 
   * Depending on the details of the paginated channels the returned dto might also include navigation links.
   * 
   * @param channels paginated, cursorable list of channels
   * @param converterContext properties used in conversion
   * @param criteria presentation criteria
   * 
   * @return publicDto
   */
  public ChannelsPublicDto convertForPublic(final PaginatedList<Channel> channels,
      final ConverterContext converterContext, final PresentationCriteria criteria) {

    ChannelsPublicDto publicDto = convertForPublic((ArrayList<Channel>) channels, converterContext, criteria);

    if (includeLinks(channels)) {
      List<LinkDto> links = new ArrayList<LinkDto>();

      if (channels.hasPreviousPage()) {
        links.add(addPreviousPageLink(channels, converterContext));
      }

      if (channels.hasNextPage()) {
        links.add(addNextPageLink(channels, converterContext));
      }

      publicDto.setLinks(links);
    }

    return publicDto;
  }

  private List<FeaturedWebcastDto> convertFeaturedWebcasts(final List<Communication> featuredCommunications,
      final DateTimeFormatter formatter) {

    List<FeaturedWebcastDto> webcasts = new ArrayList<FeaturedWebcastDto>();

    for (Communication communication : featuredCommunications) {
      FeaturedWebcastDto webcastDto = new FeaturedWebcastDto();
      webcastDto.setId(communication.getId());
      webcastDto.setTitle(communication.getTitle());
      webcastDto.setStart(formatter.convert(communication.getScheduledDateTime()));
      webcastDto.setDuration(communication.getDuration());
      webcastDto.setStatus(communication.getStatus().toString().toLowerCase());

      if (communication.hasThumbnailUrl()) {
        LinkDto thumbnailLink = new LinkDto();
        thumbnailLink.setHref(communication.getThumbnailUrl());
        thumbnailLink.setRel(LinkDto.RELATIONSHIP_ENCLOSURE);
        thumbnailLink.setType(LinkDto.TYPE_IMAGE_PNG);
        thumbnailLink.setTitle(LinkDto.TITLE_THUMBNAIL);
        webcastDto.setThumbnailLink(thumbnailLink);
      }

      webcasts.add(webcastDto);
    }

    return webcasts;
  }

  /**
   * Filter public features out of channels feature list.
   * 
   * This Method takes a list of features for a channel and only returns features that care available to be viewed
   * publically.
   * 
   * Other features should not be available to see on a public channel - should only be visible once the user has been
   * authenticated as having sufficient permissions to see the details of this channel.
   * 
   * The list of features that are public are configured in Spring Config - see business-config.xml :: publicFeatures
   * bean
   * 
   * @param features collection to be filtered
   * @return filtered features
   */
  private List<Feature> filterPublicFeatures(final List<Feature> features) {

    List<Feature> filteredFeatures = new ArrayList<Feature>();

    for (Feature feature : features) {
      if (publicFeatures.contains(feature.getName())) {
        filteredFeatures.add(feature);
      }
    }

    return filteredFeatures;
  }

  /**
   * Converts a list of categories to a list of CategoryDto.
   * 
   * @param categories - collection of categories to convert
   * @return {@link CategoryDto} or empty object when the provided list being null or empty
   */
  private List<CategoryDto> convertCategories(final List<Category> categories) {

    List<CategoryDto> categoriesDto = new ArrayList<CategoryDto>();

    for (Category category : categories) {
      categoriesDto.add(convertCategory(category));
    }

    return categoriesDto;
  }

  /**
   * Converts a Category to CategoryDto.
   * 
   * @param category to convert
   * @return {@link CategoryDto}
   */
  private CategoryDto convertCategory(final Category category) {

    CategoryDto categoryDto = new CategoryDto(category.getId());
    categoryDto.setTerm(category.getName());

    return categoryDto;
  }

  /**
   * Converts a Survey to SurveyDto.
   * 
   * @param survey to convert
   * @return {@link SurveyDto}
   */
  private SurveyDto convertSurvey(final Survey survey) {

    SurveyDto surveyDto = new SurveyDto(survey.getId(), survey.isActive());
    return surveyDto;
  }

  public void setPublicFeatures(final Set<Feature.Type> publicFeatures) {
    this.publicFeatures = publicFeatures;
  }

  private boolean includeLinks(final PaginatedList<Channel> channels) {
    return channels.hasNextPage() || channels.hasPreviousPage();
  }

  private LinkDto addPreviousPageLink(final PaginatedList<Channel> channels, final ConverterContext converterContext) {
    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(channels.getCurrentPageNumber() - 1));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(channels.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_PREVIOUS);

    return linkDto;
  }

  private LinkDto addNextPageLink(final PaginatedList<Channel> channels, final ConverterContext converterContext) {
    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(channels.getCurrentPageNumber() + 1));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(channels.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_NEXT);

    return linkDto;
  }
}
