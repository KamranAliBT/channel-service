/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ContactDetailsController.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.service.channel.ChannelService;
import com.brighttalk.service.channel.presentation.dto.ContactDetailsPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.ContactDetailsConverter;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateContactDetails;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform operations on {@link ContactDetails}.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller
@RequestMapping(value = "/channel")
public class ContactDetailsController {
  
  @Autowired
  private BrighttalkValidator validator;
  
  @Autowired
  private ChannelService channelService;
  
  @Autowired
  private ContactDetailsConverter contactDetailsConverter; 

  /**
   * Get contact details for the identified channel. Contact details are empty (but not null) if none are found. 
   * 
   * @param user User requesting contact details for identified channel.
   * @param channelId Identified channel to retrieve contact details for.
   * @return Presentation DTO of contact details for identified channel.
   */
  @RequestMapping(value = "/{channelId}/contact_details", method = RequestMethod.GET, produces = "application/json")
  @ResponseBody
  public ContactDetailsPublicDto getContactDetails(@AuthenticatedUser final User user, @PathVariable final Long channelId) {
 
    ContactDetails contactDetails;
    try {
      contactDetails = channelService.getContactDetails(user, channelId);
    } catch (UserAuthorisationException uae) {
      throw new NotAuthorisedException(uae.getMessage(), ErrorCode.USER_NOT_AUTHORISED, channelId);
    } catch (ChannelNotFoundException cnfe) {
      throw new ResourceNotFoundException(cnfe.getMessage(), ErrorCode.CHANNEL_NOT_FOUND, cnfe.getChannelId());
    }
    
    return contactDetailsConverter.convertForPublic(contactDetails);
  }

  /**
   * Updates the contact details for the identified channel.
   * 
   * @param user User requesting update to contact details for identified channel.
   * @param channelId Identified channel to update contact details for.
   * @param contactDetailsPublicDto Presentation DTO of contact details to update channel with.
   * @return Presentation DTO of the new contact details for identified channel.
   */
  @RequestMapping(value = "/{channelId}/contact_details", method = RequestMethod.PUT, consumes = "application/json", 
      produces = "application/json")
  @ResponseBody
  public ContactDetailsPublicDto updateContactDetails(@AuthenticatedUser final User user, @PathVariable final Long channelId,
      @RequestBody final ContactDetailsPublicDto contactDetailsPublicDto) {
    
    validator.validate(contactDetailsPublicDto, UpdateContactDetails.class);
    
    ContactDetails contactDetails =  contactDetailsConverter.convertFromPublic(contactDetailsPublicDto);
    
    try {
      contactDetails = channelService.updateContactDetails(user, channelId, contactDetails);
    } catch (UserAuthorisationException uae) {
      throw new NotAuthorisedException(uae.getMessage(), ErrorCode.USER_NOT_AUTHORISED, channelId);
    } catch (ChannelNotFoundException cnfe) {
      throw new ResourceNotFoundException(cnfe.getMessage(), ErrorCode.CHANNEL_NOT_FOUND, cnfe.getChannelId());
    }
    
    return contactDetailsConverter.convertForPublic(contactDetails);
  }
}
