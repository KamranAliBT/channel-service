/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceFileDto.java 83985 2014-09-29 12:52:50Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * ResourceFileDto. DTO object representing resource file.
 */
@XmlRootElement(name = "file")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResourceFileDto {

  @XmlAttribute
  private String href;
  @XmlAttribute
  private String hosted;
  @XmlAttribute
  private String mimeType;
  @XmlAttribute
  private Long size;

  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public String getHosted() {
    return hosted;
  }

  public void setHosted(String hosted) {
    this.hosted = hosted;
  }

  public String getMimeType() {
    return mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  public Long getSize() {
    return size;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  /**
   * Indicates whether the resourceDto has href.
   * 
   * Note: @JsonIgnore is used to make sure that Json marshalling is not creating additional property based on this
   * method.
   * 
   * @return true or false
   */
  @JsonIgnore
  public boolean hasHref() {
    return this.getHref() != null;
  }

  /**
   * Indicates whether the resourceDto has mime type.
   * 
   * Note: @JsonIgnore is used to make sure that Json marshalling is not creating additional property based on this
   * method.
   * 
   * @return true or false
   */
  @JsonIgnore
  public boolean hasMimeType() {
    return this.getMimeType() != null;
  }

  /**
   * Indicates whether the resourceDto has size.
   * 
   * Note: @JsonIgnore is used to make sure that Json marshalling is not creating additional property based on this
   * method.
   * 
   * @return true or false
   */
  @JsonIgnore
  public boolean hasSize() {
    return this.getSize() != null;
  }
}
