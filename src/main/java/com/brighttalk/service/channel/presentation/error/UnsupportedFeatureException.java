/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: UnsupportedFeatureException.java 97779 2015-07-09 13:11:27Z build $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.error;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Feature;

/**
 * Presentation Exception that is thrown when an attempt is made to return an exception that should not be returned to
 * the client.
 * 
 * Used when an unknown feature is loaded form the database and set in the list of features for a channel. This should
 * not be returned back to the client.
 */
@SuppressWarnings("serial")
public class UnsupportedFeatureException extends ApplicationException {

  private String name;

  /**
   * Constructor to accept a message.
   */
  public UnsupportedFeatureException() {
    super("Unsupported feature.");
  }

  /**
   * Constructor to accept a message.
   * 
   * @param featureType the feature type.
   */
  public UnsupportedFeatureException(final Feature.Type featureType) {
    super("Unsupported feature [" + featureType + "].");
  }

  /**
   * Constructor to accept a message.
   * 
   * @param name of the feature.
   * @param message the message.
   */
  public UnsupportedFeatureException(final String name, final String message) {
    super(message);
    this.name = name;
  }

  /**
   * Get the feature name in error.
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }

}
