/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ErrorDto.java 57059 2012-11-06 17:22:01Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

/**
 * DTO object representing a presentation error.
 */
public class ErrorDto {

  private String code;

  private String message;

  private Integer resourceIndex;

  public String getCode() {
    return code;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  public Integer getResourceIndex() {
    return resourceIndex;
  }

  public void setResourceIndex(final Integer resourceIndex) {
    this.resourceIndex = resourceIndex;
  }

}
