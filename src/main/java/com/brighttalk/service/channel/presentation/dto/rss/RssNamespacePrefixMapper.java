/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RssNamespacePrefixMapper.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.rss;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

/**
 * Maps the namespaces to configured prefixes (if any).
 */
public class RssNamespacePrefixMapper extends NamespacePrefixMapper {

  @Override
  public String getPreferredPrefix( String namespaceUri, String suggestion, boolean requirePrefix ) {
      
    if ( namespaceUri.equalsIgnoreCase( "http://www.w3.org/2005/Atom" ) ) {
      
      return "atom";
    }
    
    if ( namespaceUri.equalsIgnoreCase( "http://brighttalk.com/2009/rss_extensions" ) ) {
      
      return "brighttalk";
    }   
    
    return suggestion;
  }
}