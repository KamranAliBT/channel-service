/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: FeaturesDto.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.List;

import javax.validation.Valid;

/**
 * Object representing a list of {@link FeatureDto features}.
 */
public class FeaturesDto {

  @Valid
  private List<FeatureDto> features;

  public List<FeatureDto> getFeatures() {
    return features;
  }

  public void setFeatures(final List<FeatureDto> features) {
    this.features = features;
  }

}
