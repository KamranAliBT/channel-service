/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelsPublicDto.java 57059 2012-11-06 17:22:01Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * ChannelsPublicDto. DTO object representing list of public channels DTO. The object is used to wrap list of ChannelPublicDto.
 */
@XmlRootElement(name = "channels")
public class ChannelsPublicDto {

  private List<ChannelPublicDto> channels;

  private List<LinkDto> links;
  
  public List<ChannelPublicDto> getChannels() {
    return channels;
  }

  @XmlElement(name = "channel")
  public void setChannels(List<ChannelPublicDto> channels) {
    this.channels = channels;
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  @XmlElement(name = "link")
  public void setLinks(List<LinkDto> links) {
    this.links = links;
  }
}