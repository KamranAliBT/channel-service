/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsUpdateValidator.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.error.SubscriptionRequestException;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsDto;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

/**
 * Validator to validate a {@link SubscriptionsDto Subscriptions DTO} for update. This validator checks the supplied
 * subscription contains all the mandatory expected elements and it perform the following checks:
 * 
 * <ul>
 * <li>The subscription list is not empty.</li>
 * <li>The subscription list size is not greater than 100.</li>
 * <li>The subscription element has a Id element with a valid integer value.</li>
 * <li>The subscription element has a referral element with "paid" value.</li>
 * <li>The subscription element has a context element with leadType, leadContext and engagementScore.</li>
 * <li>The subscription element has a context element with leadType element with value of either "summit", "content" or
 * "keyword".</li>
 * <li>The subscription element has a context element with valid integer for engagement score element.</li>
 * <li>The subscription element has a context element with valid integer for lead context element when lead type is
 * either "summit" and "content".</li>
 * </ul>
 */
@Component
public class SubscriptionsUpdateValidator extends SubscriptionsValidator {

  private static final String ERROR_MESSAGE_SUBSCRIPTION_IDS =
      "The request body contains duplicate subscriptions with the same Ids.";

  /**
   * This flag used to check if subscription context is required. This is needed as subscription context is temporarily
   * optional until it is consider to be mandatory.
   */
  @Value("${subscription.context.required}")
  private boolean contextRequired;

  /**
   * {@inheritDoc}
   */
  @Override
  public void validate(SubscriptionsDto subscriptions) {
    assertNotEmptySubscriptions(subscriptions);
    assertMaxSubscriptionsPerRequest(subscriptions);
    assertSubscriptions(subscriptions, contextRequired);
  }

  /** {@inheritDoc} */
  @Override
  protected void assertSubscriptions(SubscriptionsDto subscriptions, boolean contextRequired) {
    List<Long> ids = extractSubscriptionIds(subscriptions.getSubscriptions());

    for (SubscriptionDto subscription : subscriptions.getSubscriptions()) {
      Long id = subscription.getId();

      // Assert if the requested subscriptions has duplicate subscriptions with same ids.
      if (ids.contains(id)) {
        ids.remove(id);
        if (ids.contains(id)) {
          throw new SubscriptionRequestException(ERROR_MESSAGE_SUBSCRIPTION_IDS, ErrorCode.DUPLICATE_RESOURCE);
        }
      }

      assertSubscriptionId(subscription);
      assertReferral(subscription);
      // Only Validate subscription context if it is set as mandatory and provided.
      if (contextRequired || subscription.getContext() != null) {
        assertContext(subscription);
      }
    }
  }

  /**
   * @param contextRequired the contextRequired to set
   */
  protected void setContextRequired(boolean contextRequired) {
    this.contextRequired = contextRequired;
  }

  /**
   * Extract the subscriptions Ids.
   * 
   * @param subscriptions The subscriptions.
   * 
   * @return List of subscriptions Ids
   */
  private List<Long> extractSubscriptionIds(final List<SubscriptionDto> subscriptions) {
    List<Long> ids = new ArrayList<>();
    for (SubscriptionDto subscription : subscriptions) {
      ids.add(subscription.getId());
    }
    return ids;
  }
}
