/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagersController.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.common.presentation.ApiRequestParam;
import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.presentation.dto.converter.context.builder.ExternalConverterContextBuilder;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.error.InvalidChannelManagerException;
import com.brighttalk.service.channel.business.error.MaxChannelManagersFeatureException;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;
import com.brighttalk.service.channel.business.service.channel.ChannelManagerFinder;
import com.brighttalk.service.channel.business.service.channel.ChannelManagerService;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelManagerConverter;
import com.brighttalk.service.channel.presentation.dto.validator.ChannelManagerCreateValidator;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;
import com.brighttalk.service.channel.presentation.util.ChannelManagersSearchCriteriaBuilder;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform CRUD operations on channel managers.
 */
@Controller
@RequestMapping(value = "/channel/{channelId}/channel_managers", produces = "application/json")
public class ChannelManagersController {

  /** Allowed request parameters. */
  protected static final Set<String> ALLOWED_REQUEST_PARAMS = new HashSet<>(Arrays.asList(ApiRequestParam.PAGE_NUMBER,
      ApiRequestParam.PAGE_SIZE, ApiRequestParam.SORT_BY, ApiRequestParam.SORT_ORDER));

  private static final Object CHANNEL_MANAGERS_FEATURE = "ChannelManagersFeature";

  @Autowired
  private ChannelManagerService channelManagerService;

  @Autowired
  private ChannelManagerFinder channelManagerFinder;

  @Autowired
  private ChannelManagerConverter converter;

  @Autowired
  private ChannelManagersSearchCriteriaBuilder searchCriteriaBuilder;

  @Autowired
  private ExternalConverterContextBuilder contextBuilder;

  /**
   * Handles a request to create the public information of a channel manager.
   * 
   * @param user channel owner
   * @param channelId The Id of the channel to be retrieved
   * @param channelManagersDto new channel manager
   * @param response http response
   * 
   * @return public channel manager
   */
  @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
  @ResponseBody
  public ChannelManagersPublicDto create(@AuthenticatedUser final User user, @PathVariable final Long channelId,
      @RequestBody final ChannelManagersDto channelManagersDto, final HttpServletResponse response) {

    List<ChannelManager> createdChannelManagers = null;

    List<ChannelManager> channelManagers = null;

    try {
      channelManagersDto.validate(new ChannelManagerCreateValidator());

      channelManagers = converter.convert(channelManagersDto);

      createdChannelManagers = channelManagerService.create(user, channelId, channelManagers);

    } catch (InvalidChannelManagerException e) {
      throw new BadRequestException(e.getMessage(), ErrorCode.INVALID_CHANNEL_MANAGER);
    } catch (ChannelNotFoundException e) {
      throw new ResourceNotFoundException(e.getMessage(), ErrorCode.CHANNEL_NOT_FOUND, e.getChannelId());
    } catch (FeatureNotEnabledException e) {
      throw new NotAuthorisedException(e.getMessage(), ErrorCode.FEATURE_DISABLED, CHANNEL_MANAGERS_FEATURE);
    } catch (MaxChannelManagersFeatureException e) {
      throw new NotAuthorisedException(e.getMessage(), ErrorCode.MAX_CHANNEL_MANAGES_EXCEEDED, e.getActual(),
          e.getLimit());
    }

    ChannelManagersPublicDto channelManagersPublicDto = converter.convertForPublic(channelManagers,
        createdChannelManagers);

    setStatusResponse(response, channelManagersPublicDto);

    return channelManagersPublicDto;
  }

  private void setStatusResponse(final HttpServletResponse response,
      final ChannelManagersPublicDto channelManagersPublicDto) {

    if (channelManagersPublicDto.getChannelManagers() == null
        || channelManagersPublicDto.getChannelManagers().isEmpty()) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
  }

  /**
   * Handles a request to get the channel managers.
   * 
   * @param user channel owner
   * @param channelId The Id of the channel to be retrieved.
   * @param request the http request
   * @param formatter the date time formatter
   * 
   * @return public channel manager
   */
  @RequestMapping(method = RequestMethod.GET)
  @ResponseBody
  public ChannelManagersPublicDto find(@AuthenticatedUser final User user, @PathVariable final Long channelId,
      final HttpServletRequest request, @DateTimeFormat final DateTimeFormatter formatter) {

    PaginatedList<ChannelManager> channelManagers = null;

    try {
      ChannelManagersSearchCriteria searchCriteria = searchCriteriaBuilder.buildWithDefaultValues(request);

      channelManagers = channelManagerFinder.find(user, channelId, searchCriteria);

    } catch (SearchCriteriaException e) {
      throw new BadRequestException(e.getMessage(), ErrorCode.SEARCH_CRITERIA, e.getCriteria(), e.getValue());
    } catch (ChannelNotFoundException e) {
      throw new ResourceNotFoundException(e.getMessage(), ErrorCode.CHANNEL_NOT_FOUND, e.getChannelId());
    } catch (FeatureNotEnabledException e) {
      throw new NotAuthorisedException(e.getMessage(), ErrorCode.FEATURE_DISABLED, CHANNEL_MANAGERS_FEATURE);
    }

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);

    ChannelManagersPublicDto channelManagersPublicDto = converter.convertForPublic(channelManagers, converterContext);

    return channelManagersPublicDto;
  }

}
