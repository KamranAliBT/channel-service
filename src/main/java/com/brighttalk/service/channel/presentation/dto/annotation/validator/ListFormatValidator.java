/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ListFormatValidator.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.annotation.validator;

import java.util.List;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Channel Validator checking the supplied String List contain (the list all individual values) is in a valid format,
 * any supplied String values that contain values in the configured Pattern regular expression will be rejected by this
 * Validator.
 */
@Component
public class ListFormatValidator implements ConstraintValidator<ListFormat, List<String>> {

  private static final Logger LOGGER = Logger.getLogger(ListFormatValidator.class);

  private Pattern pattern;

  @Override
  public void initialize(ListFormat listFormat) {
    this.pattern = Pattern.compile(listFormat.regex());
  }

  /**
   * Validating The supplied String List values contain (all individual values) are in a valid format.
   * 
   * @return true if valid, false otherwise.
   * <p>
   * {@inheritDoc}
   */
  @Override
  public boolean isValid(List<String> strValues, ConstraintValidatorContext arg1) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Validating the supplied string list values [" + strValues + "] format.");
    }

    if (strValues != null) {
      for (String strValue : strValues) {
        if (this.pattern.matcher(strValue).matches()) {
          if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Invalid supplied string value: [" + strValue + "] format.");
          }
          return false;
        }
      }
    }

    return true;
  }

  /**
   * Only provided to support testing.
   * 
   * @param pattern the pattern to set
   */
  protected void setPattern(Pattern pattern) {
    this.pattern = pattern;
  }

}
