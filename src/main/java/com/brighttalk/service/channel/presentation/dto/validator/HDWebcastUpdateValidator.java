/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerCreateValidator.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.validator.Validator;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateWebcast;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.WebcastStatusLive;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.WebcastStatusProcessing;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.WebcastStatusRecorded;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.WebcastStatusUpcoming;
import com.brighttalk.service.channel.presentation.error.InvalidWebcastException;

/**
 * Syntatical Presentation Layer Validator to validate fields for different status.
 */
@Component
public class HDWebcastUpdateValidator implements Validator<WebcastDto> {

  @Autowired
  private BrighttalkValidator validator;

  /**
   * {@inheritDoc}
   */
  @Override
  public void validate(final WebcastDto webcastDto) {
    String webcastDtoStatus = webcastDto.getStatus();

    if (webcastDtoStatus.equals(Status.UPCOMING.toString().toLowerCase())) {
      validator.validate(webcastDto, InvalidWebcastException.class, UpdateWebcast.class, WebcastStatusUpcoming.class);

    } else if (webcastDtoStatus.equals(Status.LIVE.toString().toLowerCase())) {
      validator.validate(webcastDto, InvalidWebcastException.class, UpdateWebcast.class, WebcastStatusLive.class);

    } else if (webcastDtoStatus.equals(Status.PROCESSING.toString().toLowerCase())) {
      validator.validate(webcastDto, InvalidWebcastException.class, UpdateWebcast.class, WebcastStatusProcessing.class);

    } else if (webcastDtoStatus.equals(Status.RECORDED.toString().toLowerCase())) {
      validator.validate(webcastDto, InvalidWebcastException.class, UpdateWebcast.class, WebcastStatusRecorded.class);

    }

  }

}
