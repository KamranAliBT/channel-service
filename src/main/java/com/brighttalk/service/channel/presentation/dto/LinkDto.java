/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: LinkDto.java 63781 2013-04-29 10:40:35Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * LinkDto.
 */
public class LinkDto {

  /**
   * Link type text/html value.
   */
  public static final String TYPE_TEXT_HTML = "text/html";

  /**
   * Link type text/calendar value.
   */
  public static final String TYPE_TEXT_CALENDAR = "text/calendar";

  /**
   * Link type application/atom+xml value.
   */
  public static final String TYPE_ATOM_XML = "application/atom+xml";

  /**
   * Link type image/png value.
   */
  public static final String TYPE_IMAGE_PNG = "image/png";

  /**
   * The rel attribute - alternate value.
   */
  public static final String RELATIONSHIP_ALTERNATE = "alternate";

  /**
   * The rel attribute - self value.
   */
  public static final String RELATIONSHIP_SELF = "self";

  /**
   * The rel attribute - next value.
   */
  public static final String RELATIONSHIP_NEXT = "next";

  /**
   * The rel attribute - previous value.
   */
  public static final String RELATIONSHIP_PREVIOUS = "previous";

  /**
   * The rel attribute - first value.
   */
  public static final String RELATIONSHIP_FIRST = "first";

  /**
   * The rel attribute - last value.
   */
  public static final String RELATIONSHIP_LAST = "last";

  /**
   * The rel attribute - enclosure value.
   */
  public static final String RELATIONSHIP_ENCLOSURE = "enclosure";

  /**
   * The rel attribute - related value.
   */
  public static final String RELATIONSHIP_RELATED = "related";

  /**
   * Thumbnail link title.
   */
  public static final String TITLE_THUMBNAIL = "thumbnail";

  /**
   * Calendar link title.
   */
  public static final String TITLE_CALENDAR = "calendar";

  /**
   * Preview link title.
   */
  public static final String TITLE_PREVIEW = "preview";

  private String href;

  private String rel;

  private String type;

  private String title;

  public String getHref() {
    return href;
  }

  @XmlAttribute
  public void setHref(String href) {
    this.href = href;
  }

  public String getRel() {
    return rel;
  }

  @XmlAttribute
  public void setRel(String rel) {
    this.rel = rel;
  }

  public String getType() {
    return type;
  }

  @XmlAttribute
  public void setType(String type) {
    this.type = type;
  }

  public String getTitle() {
    return title;
  }

  @XmlAttribute
  public void setTitle(String title) {
    this.title = title;
  }
}