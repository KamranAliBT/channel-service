/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ListNotBlank.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.annotation.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Validate the supplied String List is not null or empty list of values.
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ListNotBlankValidator.class)
@Documented
public @interface ListNotBlank {

  /** Default error message */
  String message() default "String list must not be blank";

  /** Default groups */
  Class<?>[] groups() default {

  };

  /** Default pay load */
  Class<? extends Payload>[] payload() default {

  };

}
