/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: UpdateChannel.java 83494 2014-09-17 16:13:00Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validation.scenario;

/**
 * Validation group type for channel update scenario.
 */
public interface UpdateChannel {

}
