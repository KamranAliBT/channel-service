/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastsPublicDto.java 57672 2012-11-16 17:55:35Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * WebcastsPublicDto. The object is used to wrap list of WebcastPublicDto.
 */
@XmlRootElement(name = "webcasts")
public class WebcastsPublicDto {

  private List<WebcastPublicDto> webcasts;

  private List<LinkDto> links;

  public List<LinkDto> getLinks() {
    return links;
  }

  @XmlElement(name = "link")
  public void setLinks(List<LinkDto> links) {
    this.links = links;
  }

  public List<WebcastPublicDto> getWebcasts() {
    return webcasts;
  }

  @XmlElement(name = "webcast")
  public void setWebcasts(List<WebcastPublicDto> webcasts) {
    this.webcasts = webcasts;
  }
}
