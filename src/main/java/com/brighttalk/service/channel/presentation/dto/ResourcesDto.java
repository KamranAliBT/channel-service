/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourcesDto.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * DTO object representing list of resource DTO. The object is used to wrap list of ResourceDto.
 */
@XmlRootElement(name = "resources")
public class ResourcesDto {

  private List<ResourceDto> resources;

  /**
   * @return the collection of ResourceDto objects or empty collection if this.resources is null.
   */
  public List<ResourceDto> getResources() {
    if (resources == null) {
      resources = new ArrayList<ResourceDto>();
    }
    return resources;
  }

  /**
   * Sets resources property. Requires @XmlElement to make sure that list of resources elements will be returned. In
   * case the @XmlElement is not set correctly <resources><resourceDto> will be returned.
   * 
   * @param resources list of resourceDto
   */
  @XmlElement(name = "resource")
  public void setResources(List<ResourceDto> resources) {
    this.resources = resources;
  }
}