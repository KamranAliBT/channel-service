/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerCreateValidator.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.service.channel.business.domain.validator.Validator;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.CreateWebcast;
import com.brighttalk.service.channel.presentation.error.InvalidWebcastException;

/**
 * Syntatical Presentation Layer Validator to validate fields for different status.
 */
@Component
public class HDWebcastCreateValidator implements Validator<WebcastDto> {

  @Autowired
  private BrighttalkValidator validator;

  /**
   * {@inheritDoc}
   */
  @Override
  public void validate(final WebcastDto webcastDto) {
    validator.validate(webcastDto, InvalidWebcastException.class, CreateWebcast.class);
  }

}
