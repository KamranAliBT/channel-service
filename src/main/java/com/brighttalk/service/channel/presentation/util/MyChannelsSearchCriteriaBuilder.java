/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: MyChannelsSearchCriteriaBuilder.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;
import com.brighttalk.service.channel.presentation.ApiRequestParam;

/**
 * Build Class to extract a {@link MyChannelsSearchCriteria} object from a request object.
 */
@Component
public class MyChannelsSearchCriteriaBuilder extends AbstractSearchCriteriaBuilder {

  /**
   * Build the criteria object.
   * 
   * @param request The request
   * 
   * @return The built criteria
   */
  public MyChannelsSearchCriteria build(final HttpServletRequest request) {

    String sortBy = request.getParameter(ApiRequestParam.SORT_BY);
    String sortOrder = request.getParameter(ApiRequestParam.SORT_ORDER);
    Integer pageNumber = getIntegerFromRequest(request, ApiRequestParam.PAGE_NUMBER);
    Integer pageSize = getIntegerFromRequest(request, ApiRequestParam.PAGE_SIZE);

    MyChannelsSearchCriteria criteria = new MyChannelsSearchCriteria();
    criteria.setPageNumber(pageNumber);
    criteria.setPageSize(pageSize);
    criteria.setSortBy(convertSortBy(sortBy));
    criteria.setSortOrder(convertSortOrder(sortOrder));

    return criteria;
  }

  /**
   * Encapsulating method for converting a sort by string to an enum.
   * 
   * @param sortBy The String to convert
   * @return The converted Enum
   */
  private MyChannelsSearchCriteria.SortBy convertSortBy(final String sortBy) {
    return MyChannelsSearchCriteria.SortBy.convert(sortBy);
  }
}