/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MySubscribedChannelsController.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.PresentationCriteria;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.channel.ChannelService;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.ChannelsPublicDto;
import com.brighttalk.service.channel.presentation.dto.WebcastsPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelConverter;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastsPublicConverter;
import com.brighttalk.service.channel.presentation.util.InternalConverterContextBuilder;
import com.brighttalk.service.channel.presentation.util.MyChannelsSearchCriteriaBuilder;
import com.brighttalk.service.channel.presentation.util.MyCommunicationsSearchCriteriaBuilder;
import com.brighttalk.service.channel.presentation.util.PresentationCriteriaBuilder;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls requesting lists of channels subscribed to the current user.
 * 
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 */
@Controller("mySubscribedChannelsControllerInternal")
@RequestMapping(value = "/internal/user/{userId}/channel_subscriptions")
public class MySubscribedChannelsController {

  private static final Set<String> SUBSCRIBED_CHANNELS_WEBCASTS_ALLOWED_REQUEST_PARAMS = new HashSet<String>(
      Arrays.asList(ApiRequestParam.DATE_TIME_FORMAT, ApiRequestParam.PAGE_NUMBER, ApiRequestParam.PAGE_SIZE,
          ApiRequestParam.WEBCAST_STATUS, ApiRequestParam.REGISTERED, ApiRequestParam.SORT_BY,
          ApiRequestParam.SORT_ORDER));

  private static final Set<String> SUBSCRIBED_CHANNELS_ALLOWED_REQUEST_PARAMS = new HashSet<String>(Arrays.asList(
      ApiRequestParam.DATE_TIME_FORMAT, ApiRequestParam.PAGE_NUMBER, ApiRequestParam.PAGE_SIZE,
      ApiRequestParam.SORT_BY, ApiRequestParam.SORT_ORDER));

  @Autowired
  private CommunicationFinder communicationFinder;

  @Autowired
  private MyCommunicationsSearchCriteriaBuilder communicationsSearchCriteriaBuilder;

  @Autowired
  private InternalConverterContextBuilder contextBuilder;

  @Autowired
  private WebcastsPublicConverter webcastsPublicConverter;

  @Autowired
  private ChannelService channelService;
  
  /** Channel finder */
  @Autowired
  private ChannelFinder channelFinder;

  @Autowired
  private ChannelConverter channelConverter;

  @Autowired
  private MyChannelsSearchCriteriaBuilder channelsSearchCriteriaBuilder;

  /** Presentation criteria builder */
  @Autowired
  private PresentationCriteriaBuilder presentationCriteriaBuilder;

  /** Brighttalk validator. */
  @Autowired
  private BrighttalkValidator validator;

  /**
   * Retrieve a paginated list of webcasts in the user's subscribed channels.
   * 
   * @param request The Request
   * @param userId The id of a user
   * @param formatter date formatter
   * 
   * @return WebcastsPublicDto
   */
  @RequestMapping(value = "/webcasts", method = RequestMethod.GET)
  @ResponseBody
  public WebcastsPublicDto getSubscribedChannelsWebcasts(final HttpServletRequest request,
    @PathVariable final Long userId, @DateTimeFormat final DateTimeFormatter formatter) {

    MyCommunicationsSearchCriteria searchCriteria = communicationsSearchCriteriaBuilder.build(request);

    User user = new User(userId);
    PaginatedList<Communication> communications = communicationFinder.findSubscribedChannelsCommunications(user,
        searchCriteria);
    ConverterContext converterContext = contextBuilder.build(request, formatter,
        SUBSCRIBED_CHANNELS_WEBCASTS_ALLOWED_REQUEST_PARAMS);

    WebcastsPublicDto webcastsDto = webcastsPublicConverter.convert(communications, converterContext);

    return webcastsDto;
  }

  /**
   * Handles a request to unsubscribe user from all channels.
   * 
   * @param userId The id of a user
   */
  @RequestMapping(method = RequestMethod.DELETE)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteSubscribedChannelsWebcasts(@PathVariable final Long userId) {

    User user = new User(userId);

    channelService.unsubscribeUserFromChannels(user);
  }

  /**
   * Handles a request to find user subscribed channels using given search criteria context.
   * 
   * @param request The Request
   * @param formatter date formatter
   * @param userId The user id
   * 
   * @return ChannelsPublicDto
   */
  @RequestMapping(method = RequestMethod.GET)
  @ResponseBody
  public ChannelsPublicDto getSubscribedChannels(final HttpServletRequest request, @PathVariable final Long userId,
    @DateTimeFormat final DateTimeFormatter formatter) {

    PresentationCriteria presentationCriteria = presentationCriteriaBuilder.buildWithDefaultExpandedFeaturedWebcasts(request);
    validator.validate(presentationCriteria, ValidationException.class);

    User user = new User(userId);
    MyChannelsSearchCriteria searchCriteria = channelsSearchCriteriaBuilder.build(request);

    PaginatedList<Channel> channels = this.channelFinder.findSubscribedChannels(user, searchCriteria);

    ConverterContext converterContext = contextBuilder.build(request, formatter,
        SUBSCRIBED_CHANNELS_ALLOWED_REQUEST_PARAMS);

    ChannelsPublicDto channelsDto = channelConverter.convertForPublic(channels, converterContext, presentationCriteria);

    return channelsDto;
  }

}
