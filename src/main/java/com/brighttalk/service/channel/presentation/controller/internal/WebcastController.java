/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastController.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.error.ProviderNotSupportedException;
import com.brighttalk.service.channel.business.service.communication.CommunicationUpdateService;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.presentation.controller.base.BaseController;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

/**
 * The internal webcast controller handling internal call from other services/clients. As all internal controller it
 * does not require authentication.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller(value = "internalWebcastController")
@RequestMapping(value = "/internal")
public class WebcastController extends BaseController {

  /** Logger for this class */
  private static final Logger LOGGER = Logger.getLogger(WebcastController.class);

  /** Action description for audit record when webcast is cancelled **/
  private static final String CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION = "Cancelled through channel (Java) internal API call";

  private static final String PROVIDER_NOT_SUPPORTED_YET_ERROR_MESSAGE = "Provider not supported yet.";

  @Autowired
  @Qualifier(value = "hdcommunicationupdateservice")
  private CommunicationUpdateService hDCommunicationUpdateService;

  @Autowired
  @Qualifier(value = "VideoCommunicationUpdateService")
  private CommunicationUpdateService videoCommunicationUpdateService;

  /**
   * Handles a internal request to get webcast details for a given channel id and webcast id.
   * 
   * @param channelId id of the channel the webcast is contained into.
   * @param webcastId id of the webcast to load.
   * @param formatter the date formatter
   * 
   * @return webcastDto
   * 
   * @throws ResourceNotFoundException when resource cannot be found.
   */
  @RequestMapping(value = "/channel/{channelId}/webcast/{webcastId}", method = RequestMethod.GET)
  @ResponseBody
  public WebcastDto get(@PathVariable final Long channelId, @PathVariable final Long webcastId,
    @DateTimeFormat final DateTimeFormatter formatter) {

    Channel channel = findChannel(channelId);

    Communication communication = null;
    try {
      communication = communicationFinder.find(channel, webcastId);
    } catch (CommunicationNotFoundException nfe) {
      throw new ResourceNotFoundException(nfe.getMessage(), ErrorCode.RESOURCE_NOT_FOUND, webcastId);
    }

    WebcastDto webcastDto = webcastConverter.convert(communication, channel, formatter);

    return webcastDto;
  }

  /**
   * Handles a internal request to get webcast details for a given webcast id. The webcast returned is the webcast in
   * the master channel id
   * 
   * @param webcastId id of the webcast to load.
   * @param formatter the date formatter
   * 
   * @return webcastDto
   * 
   * @throws ResourceNotFoundException when resource cannot be found.
   */
  @RequestMapping(value = { "/channel/webcast/{webcastId}", "/webcast/{webcastId}" }, method = RequestMethod.GET)
  @ResponseBody
  public WebcastDto get(@PathVariable final Long webcastId, @DateTimeFormat final DateTimeFormatter formatter) {

    // find communication
    Communication communication = findCommunication(webcastId);

    // find channel
    Channel channel = findChannel(communication.getChannelId());

    WebcastDto webcastDto = webcastConverter.convert(communication, channel, formatter);

    return webcastDto;
  }

  /**
   * Handles requests to update a webcast in the given channel.
   * 
   * @param webcastId The id of the webcast to be updated.
   * @param webcastDto The requested webcast to be updated.
   * 
   * @throws BadRequestException In case of the supplied channel is different to the master channel in the requested
   * HTTP body.
   * @throws ResourceNotFoundException when eith the channel or the webcast has not been found.
   * @throws Exception is thrown if the update fails
   */
  @RequestMapping(value = "/webcast/{webcastId}", method = RequestMethod.PUT)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void update(@PathVariable final Long webcastId, @RequestBody final WebcastDto webcastDto) throws Exception {

    LOGGER.info("Received internal request to update the webcast [" + webcastId + "].");

    // Validate the webcast dto fields.
    webcastDto.validate(webcastUpdateValidator);

    // Validate that the supplied webcast id is the same as the id provided in the request.
    validateRequestedWebcastId(webcastId, webcastDto.getId());

    // Convert the webcast dto into a commnuication object
    Communication communication = convert(webcastDto);

    // find exisiting communication
    Communication existingCommunication = findCommunication(webcastId);

    // merge communicationtion
    existingCommunication.merge(communication);

    // find channel
    Channel channel = findChannel(existingCommunication.getChannelId());

    // Perform the update
    Long updatedCommunicationId = null;
    if (communication.isBrightTALKHD()) {
      updatedCommunicationId = hDCommunicationUpdateService.update(channel, existingCommunication);
      LOGGER.info("Updated communication [" + updatedCommunicationId + "].");
    } else if (communication.isVideo()) {
      updatedCommunicationId = videoCommunicationUpdateService.update(channel, existingCommunication);
      LOGGER.info("Updated communication [" + updatedCommunicationId + "]");
    } else {
      throw new ProviderNotSupportedException(PROVIDER_NOT_SUPPORTED_YET_ERROR_MESSAGE);
    }
  }

  /**
   * Handles a internal request to update the webcast status.
   * 
   * @param webcastId id of the webcast to load.
   * @param webcastDto the webcast dto
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when resource cannot be found.
   */
  @RequestMapping(value = "/channel/webcast/{webcastId}", method = RequestMethod.PUT)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void status(@PathVariable final Long webcastId, @RequestBody final WebcastDto webcastDto) {

    // validate the request
    validateRequest(webcastDto, false);

    // Validate that the supplied webcast id is the same as the id provided in the request.
    validateRequestedWebcastId(webcastId, webcastDto.getId());

    Communication communication = findCommunication(webcastId);

    validateStatus(communication, webcastDto.getStatus());

    processUpdateStatus(communication.getMasterChannelId(), webcastDto, communication);
  }

  /**
   * Handles a internal request to update the webcast status.
   * 
   * @param channelId in which the webcast lives
   * @param webcastId id of the webcast to load.
   * @param webcastDto is the payload to update status
   * @throws ResourceNotFoundException when resource cannot be found.
   */
  @RequestMapping(value = "/channel/{channelId}/webcast/{webcastId}", method = RequestMethod.PUT)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void status(@PathVariable final Long channelId, @PathVariable final Long webcastId,
    @RequestBody final WebcastDto webcastDto) {

    // validate the request
    validateRequest(webcastDto, true);

    // Validate that the supplied webcast id is the same as the id provided in the request.
    validateRequestedChannelId(channelId, webcastDto.getChannel().getId());

    // Validate that the supplied webcast id is the same as the id provided in the request.
    validateRequestedWebcastId(webcastId, webcastDto.getId());

    Communication communication = findCommunication(webcastId);

    validateStatus(communication, webcastDto.getStatus());

    processUpdateStatus(channelId, webcastDto, communication);
  }

  /**
   * Validates the supplied webcast id in the url is the same as the webcast id from the payload.
   * 
   * @param channelId set in the url
   * @param requestId set in the request
   * @throws BadRequestException when the ids are different.
   */
  private void validateRequestedChannelId(final long channelId, final long requestId) {
    if (channelId != requestId) {
      throw new BadRequestException("Invalid channel Id", ErrorCode.INVALID_CHANNEL_ID.getName(),
          new Object[] { channelId });
    }
  }

  /**
   * Validates the supplied webcast id in the url is the same as the webcast id from the payload.
   * 
   * @param webcastId set in the url
   * @param requestId set in the request
   * @throws BadRequestException when the ids are different.
   */
  private void validateRequestedWebcastId(final long webcastId, final long requestId) {
    if (webcastId != requestId) {
      throw new BadRequestException("Invalid webcast Id", ErrorCode.INVALID_WEBCAST_ID.getName(),
          new Object[] { webcastId });
    }
  }

  /**
   * Validate that all the expected elements are set in the request.
   * 
   * @param webcastDto received as request
   * @param validateChannelIdIsSet allows checking for the channel element and id
   */
  private void validateRequest(final WebcastDto webcastDto, final boolean validateChannelIdIsSet) {
    if (!webcastDto.hasId()) {
      throw new BadRequestException("Webcast id must be set", BadRequestException.BAD_REQUEST_EXCEPTION_ERROR_CODE);
    }

    if (validateChannelIdIsSet) {
      if (!webcastDto.hasChannel() || webcastDto.getChannel().getId() == null) {
        throw new BadRequestException("Channel id must be set", BadRequestException.BAD_REQUEST_EXCEPTION_ERROR_CODE);
      }
    }

    if (!webcastDto.hasStatus()) {
      throw new BadRequestException("Status must be set", BadRequestException.BAD_REQUEST_EXCEPTION_ERROR_CODE);
    }
  }

  /**
   * Process the status update.
   * 
   * Depending on the value of the status set in the request, we either perform a cancel or an update.
   * 
   * @param channelId of the master channel
   * @param webcastDto from the request
   * @param communication to be updated.
   */
  private void processUpdateStatus(final Long channelId, final WebcastDto webcastDto, final Communication communication) {

    if (webcastDto.getStatus().equals(Communication.Status.CANCELLED.toString().toLowerCase())) {
      communication.setStatus(Status.CANCELLED);
      communicationCancelService.cancel(communication);

      // Audit the cancellation request
      AuditRecordDto auditRecordDto = auditRecordDtoBuilder.cancel(null, channelId, webcastDto.getId(),
          CANCELLED_WEBCAST_AUDIT_RECORD_DESCRIPTION);
      auditQueue.create(auditRecordDto);
    } else {
      Channel channel = findChannel(channelId);

      Communication webcast = webcastConverter.convertBasicFields(webcastDto);
      communication.setStatus(webcast.getStatus());
      communication.setRenditions(webcast.getRenditions());
      hDCommunicationUpdateService.updateStatus(channel, communication);
    }

  }

  /**
   * Validate that the status we want to update the webcast to is being allowed by the current webcast status.
   * 
   * if the current status of the webcast is UPCOMING, we can only set the status to LIVE or CANCELLED. if the current
   * status of the webcast is LIVE, we can only set the status to PROCESSING. All other status combination should fail
   * and throw a BadRequestException
   * 
   * @param communication in the system
   * @param status to update the communication status to.
   * @throws BadRequestException if the combination fails.
   */
  private void validateStatus(final Communication communication, final String status) {
    Status communicationStatus = communication.getStatus();
    String statusUppercase = status.toUpperCase();

    if (communicationStatus.equals(Status.UPCOMING)) {
      if (!statusUppercase.equals(Status.LIVE.name()) && !statusUppercase.equals(Status.CANCELLED.name())) {
        throw new BadRequestException("Status cannot be updated from [" + communicationStatus + "] to ["
            + statusUppercase + "]", ErrorCode.INVALID_WEBCAST_STATUS.getName(), new Object[] { status });
      }
    } else if (communicationStatus.equals(Status.LIVE)) {
      if (!statusUppercase.equals(Status.PROCESSING.name())) {
        throw new BadRequestException("Status cannot be updated from [" + communicationStatus + "] to ["
            + statusUppercase + "]", ErrorCode.INVALID_WEBCAST_STATUS.getName(), new Object[] { status });
      }
    } else if (communicationStatus.equals(Status.PROCESSING)) {
      if (!statusUppercase.equals(Status.RECORDED.name())) {
        throw new BadRequestException("Status cannot be updated from [" + communicationStatus + "] to ["
            + statusUppercase + "]", ErrorCode.INVALID_WEBCAST_STATUS.getName(), new Object[] { status });
      }
    } else {
      throw new BadRequestException("Status cannot be updated from [" + communicationStatus + "] to ["
          + statusUppercase + "]", ErrorCode.INVALID_WEBCAST_STATUS.getName(), new Object[] { status });
    }
  }

  /**
   * Return the communication converted from the webcast DTO.
   * 
   * @param webcastDto to converted in a communication object
   * @return communication to be used.
   */
  private Communication convert(final WebcastDto webcastDto) {

    switch (webcastDto.getProvider().getName().toString()) {
      case Provider.BRIGHTTALK_HD:
        return webcastConverter.convertBasicFields(webcastDto);
      case Provider.VIDEO:
        return webcastConverter.convertBasicFields(webcastDto);
      default:
        throw new ProviderNotSupportedException(PROVIDER_NOT_SUPPORTED_YET_ERROR_MESSAGE);
    }
  }

}
