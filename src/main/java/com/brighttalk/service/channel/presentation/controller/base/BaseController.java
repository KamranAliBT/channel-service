/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastController.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.base;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.error.ErrorDto;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.queue.AuditQueue;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationCancelService;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastConverter;
import com.brighttalk.service.channel.presentation.dto.validator.WebcastUpdateValidator;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.InvalidParamException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

/**
 * Abstract base controller used by Channel Service APIs, containing common methods to find the requested channel and
 * communication and list of {@link ExceptionHandler} methods to handle exceptions thrown from other layers of the APIs.
 */
public abstract class BaseController {

  /** logger for this class. */
  protected static final Logger LOGGER = Logger.getLogger(BaseController.class);

  /** Channel finder */
  @Autowired
  protected ChannelFinder channelFinder;

  /** Communication finder. */
  @Autowired
  protected CommunicationFinder communicationFinder;

  /** Webcast update validator. */
  @Autowired
  protected WebcastUpdateValidator webcastUpdateValidator;

  /** BrightTALK validator. */
  @Autowired
  protected BrighttalkValidator validator;

  /** webcast converter. */
  @Autowired
  protected WebcastConverter webcastConverter;

  /** Audit queuer. */
  @Autowired
  protected AuditQueue auditQueue;

  @Autowired
  @Qualifier("webcastAuditRecordDtoBuilder")
  protected AuditRecordDtoBuilder auditRecordDtoBuilder;

  /** Communication cancel service. */
  @Autowired
  protected CommunicationCancelService communicationCancelService;

  /**
   * Find the channel for the given channel id.
   * 
   * @param channelId of the channel to load.
   * @return channel when channel has been found
   * @throws ResourceNotFoundException when the channel has not been found
   */
  protected Channel findChannel(final Long channelId) {
    LOGGER.debug("Find the channel for the given channel id [" + channelId + "]");
    try {
      // Find the channel
      return channelFinder.find(channelId);
    } catch (ChannelNotFoundException nfe) {
      throw new ResourceNotFoundException(nfe.getMessage(), ErrorCode.RESOURCE_NOT_FOUND, channelId);
    }
  }

  /**
   * Find the channel for the given channel id and validate if the requested user authorised on the supplied channel.
   * 
   * @param channelId of the channel to load.
   * @param user The requested user.
   * @return channel when channel has been found
   * @throws ResourceNotFoundException when the channel has not been found
   * @throws UserAuthorisationException when the requested not authorised for channel.
   */
  protected Channel findChannel(final User user, final Long channelId) {
    LOGGER.debug("Find the channel for the given channel id [" + channelId + "]");
    try {
      // Find the channel
      return channelFinder.find(user, channelId);
    } catch (ChannelNotFoundException nfe) {
      throw new ResourceNotFoundException(nfe.getMessage(), ErrorCode.RESOURCE_NOT_FOUND, channelId);
    }
  }

  /**
   * Find the communication for the given webcast id.
   * 
   * @param webcastId of the communication to load.
   * @return communication when communication has been found
   * @throws ResourceNotFoundException when the communication has not been found
   */
  protected Communication findCommunication(final Long webcastId) {
    try {
      return communicationFinder.find(webcastId);
    } catch (CommunicationNotFoundException nfe) {
      throw new ResourceNotFoundException(nfe.getMessage(), ErrorCode.RESOURCE_NOT_FOUND, webcastId);
    }
  }

  /**
   * Find the communication for the given webcast id and channel.
   * 
   * @param channel of the communication to load.
   * @param webcastId of the communication to load.
   * @return communication when communication has been found
   * @throws ResourceNotFoundException when the communication has not been found
   */
  protected Communication findCommunication(final Channel channel, final Long webcastId) {
    try {
      return communicationFinder.find(channel, webcastId);
    } catch (CommunicationNotFoundException nfe) {
      throw new ResourceNotFoundException(nfe.getMessage(), ErrorCode.RESOURCE_NOT_FOUND, webcastId);
    }
  }

  /**
   * Convert and validate the supplied channel Id.
   * 
   * @param channelId The channel Id to convert.
   * @return The converted channel Id from {@link String} to {@link Long}.
   * @throws InvalidParamException in the case of the supplied channel Id is not a valid Long or has a negative value.
   */
  protected Long convertChannelId(final String channelId) {
    Long convertedChannelId;

    try {
      convertedChannelId = Long.valueOf(channelId);
      if (convertedChannelId <= 0) {
        throw new NumberFormatException();
      }
    } catch (NumberFormatException e) {
      throw new InvalidParamException("Invalid channel id [" + channelId + "].", ErrorCode.INVALID_CHANNEL_ID);
    }
    return convertedChannelId;
  }

  /**
   * Convert and validate the supplied webcast Id.
   * 
   * @param webcastId The webcast Id to convert.
   * @return The converted webcast Id from {@link String} to {@link Long}.
   * @throws InvalidParamException in the case of the supplied webcast Id is not a valid Long or has a negative value.
   */
  protected Long convertWebcastId(final String webcastId) {
    Long convertedWebcastId;

    try {
      convertedWebcastId = Long.valueOf(webcastId);
      if (convertedWebcastId <= 0) {
        throw new NumberFormatException();
      }
    } catch (NumberFormatException e) {
      throw new InvalidParamException("Invalid webcast id [" + webcastId + "].", ErrorCode.INVALID_WEBCAST_ID);
    }
    return convertedWebcastId;
  }

  /**
   * Exception handler method handles {@link ResourceNotFoundException} thrown from the other layers.
   * 
   * @param ex The {@link ResourceNotFoundException} handled by this method.
   * @return {@link ErrorDto} contain {@link ErrorCode#RESOURCE_NOT_FOUND} error code.
   */
  @ExceptionHandler(value = ResourceNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  private ErrorDto handleResourceNotFoundException(ResourceNotFoundException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("The supplied resource does not identify an exisitng resource; cause["
          + ex.getMessage() + "].");
    }
    ErrorDto errorDto = new ErrorDto(ErrorCode.RESOURCE_NOT_FOUND.getName(), ex.getMessage());
    return errorDto;
  }

  /**
   * Exception handler method handles {@link ChannelNotFoundException} thrown from the other layers.
   * 
   * @param ex The {@link ChannelNotFoundException} handled by this method.
   * @return {@link ErrorDto} contain {@link ErrorCode#CHANNEL_NOT_FOUND} error code.
   */
  @ExceptionHandler(value = ChannelNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  private ErrorDto handleChannelNotFoundException(ChannelNotFoundException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("The supplied channel id [" + ex.getChannelId() + "] does not identify an exisitng channel; cause["
          + ex.getMessage() + "].");
    }
    ErrorDto errorDto = new ErrorDto(ErrorCode.CHANNEL_NOT_FOUND.getName(), ex.getMessage());
    return errorDto;
  }

  /**
   * Exception handler method handles {@link CommunicationNotFoundException} thrown from the other layers.
   * 
   * @param ex The {@link CommunicationNotFoundException} handled by this method.
   * @return {@link ErrorDto} contain {@link ErrorCode#COMMUNICATION_NOT_FOUND} error code.
   */
  @ExceptionHandler(value = CommunicationNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  private ErrorDto handleCommunicationNotFoundException(CommunicationNotFoundException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("The supplied communication id [" + ex.getCommunicationId()
          + "] does not identify an exisitng channel; cause["
          + ex.getMessage() + "].");
    }
    ErrorDto errorDto = new ErrorDto(ErrorCode.COMMUNICATION_NOT_FOUND.getName(), ex.getMessage());
    return errorDto;
  }

  /**
   * Exception handler method handles {@link UserAuthorisationException} thrown from the other layers.
   * 
   * @param ex The {@link UserAuthorisationException} handled by this method.
   * @return {@link ErrorDto} contain {@link ErrorCode#NOT_AUTHORISED} error code.
   */
  @ExceptionHandler(value = UserAuthorisationException.class)
  @ResponseStatus(value = HttpStatus.FORBIDDEN)
  @ResponseBody
  private ErrorDto handleUserAuthorisationException(UserAuthorisationException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("The supplied user not authorised for channel; cause[" + ex.getMessage() + "].");
    }
    ErrorDto errorDto = new ErrorDto(ErrorCode.NOT_AUTHORISED.getName(), ex.getMessage());
    return errorDto;
  }

  /**
   * Exception handler method handles {@link InvalidParamException} thrown from the presentation layer.
   * 
   * @param ex The {@link InvalidParamException} handled by this method.
   * 
   * @return {@link ErrorDto} contain the invalid error code.
   */
  @ExceptionHandler(value = InvalidParamException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  private ErrorDto handleInvalidParamException(final InvalidParamException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Error while processing the request.", ex);
    }
    return new ErrorDto(ex.getUserErrorCode(), ex.getMessage());
  }
}