/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RssFeedConverter.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.PaginatedChannelFeed;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.rss.FeedChannelDto;
import com.brighttalk.service.channel.presentation.dto.rss.FeedItemDto;
import com.brighttalk.service.channel.presentation.dto.rss.RssFeedDto;

/**
 * Converter - Responsible for converting between RssFeedDto and business types.
 */
@Component
public class RssFeedConverter {

  /**
   * Rss version.
   */
  protected static final double RSS_VERSION = 2.0;
  
  @Autowired
  private RssFeedItemConverter itemConverter;
  
  /**
   * Converts a channel and a collection of communications to a rss dto.
   * 
   * @param feed the feed to convert 
   * 
   * @return {@link RssFeedDto} 
   */
  public RssFeedDto convert(PaginatedChannelFeed feed) {
    
    if ( feed.isEmpty() ) {
      
      return new RssFeedDto();
    }
    
    RssFeedDto rssFeedDto = new RssFeedDto();
    rssFeedDto.setVersion( RSS_VERSION );
    
    FeedChannelDto channelDto = new FeedChannelDto();
    
    Channel channel = feed.getChannel();
    channelDto.setTitle( channel.getTitle() );
    channelDto.setDescription( channel.getDescription() );
    channelDto.setStrapline( channel.getStrapline() );
    channelDto.setAtomLink( createAtomLink( channel ) );
    
    if ( channel.hasUrl() ) {
      
      channelDto.setChannelLink( channel.getUrl() );
    }
    
    List<FeedItemDto> items = new ArrayList<FeedItemDto>();

    for ( Communication communication : feed.getAllCommunications() ) {
      
      FeedItemDto itemDto = itemConverter.convert( communication );
      items.add( itemDto );
    }
    
    channelDto.setItems( items );
    
    rssFeedDto.setChannel( channelDto );
    
    return rssFeedDto;
  }
  
  private LinkDto createAtomLink( Channel channel ) {
    
    LinkDto linkDto = new LinkDto();
    linkDto.setHref( channel.getFeedUrlAtom() );
    linkDto.setRel( LinkDto.RELATIONSHIP_SELF );
    linkDto.setType( LinkDto.TYPE_ATOM_XML );
    
    return linkDto;
  }
  
  public void setItemConverter(RssFeedItemConverter itemConverter) {
    this.itemConverter = itemConverter;
  }
  
}