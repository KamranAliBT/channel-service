/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 41851 2012-02-02 17:06:45Z afernandes $
 * ****************************************************************************
 */
/**
 * This package contains the Builder Classes used in the presentation dtos.
 */
package com.brighttalk.service.channel.presentation.dto.builder;