/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeedChannelDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.rss;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.brighttalk.service.channel.presentation.dto.LinkDto;

/**
 * FeedChannelDto used as the main node in rss feed.
 */
@XmlType( propOrder = { "title", "description", "strapline", "channelLink", "atomLink", "items" } )
public class FeedChannelDto {
  
  private String title;
  
  private String description;
  
  private String strapline;
  
  private String channelLink;
  
  private LinkDto atomLink;
  
  private List<FeedItemDto> items;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStrapline() {
    return strapline;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/rss_extensions" )
  public void setStrapline(String strapline) {
    this.strapline = strapline;
  }

  public String getChannelLink() {
    return channelLink;
  }

  @XmlElement( name = "link" )
  public void setChannelLink(String channelLink) {
    this.channelLink = channelLink;
  }

  public LinkDto getAtomLink() {
    return atomLink;
  }

  @XmlElement( namespace = "http://www.w3.org/2005/Atom", name = "link" )
  public void setAtomLink(LinkDto atomLink) {
    this.atomLink = atomLink;
  }

  public List<FeedItemDto> getItems() {
    return items;
  }

  @XmlElement(name = "item")
  public void setItems(List<FeedItemDto> items) {
    this.items = items;
  }
}
