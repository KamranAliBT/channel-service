/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: UserDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import org.codehaus.jackson.map.annotate.JsonRootName;

/**
 * User Dto. DTO object representing channel owner. The channel owner object is wrapped in user element.
 */
@JsonRootName("user")
public class UserPublicDto {

  private String externalId;

  private String firstName;

  private String lastName;

  private String email;

  private String timeZone;

  private String jobTitle;

  private String level;

  private String companyName;

  private String industry;

  private String companySize;

  private String stateRegion;

  private String country;

  /**
   * Constructs user Dto object as a new object.
   */
  public UserPublicDto() {
  }

  public String getExternalId() {
    return externalId;
  }

  public void setExternalId(final String externalId) {
    this.externalId = externalId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getTimeZone() {
    return timeZone;
  }

  public void setTimeZone(final String timeZone) {
    this.timeZone = timeZone;
  }

  public String getJobTitle() {
    return jobTitle;
  }

  public void setJobTitle(final String jobTitle) {
    this.jobTitle = jobTitle;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(final String level) {
    this.level = level;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(final String companyName) {
    this.companyName = companyName;
  }

  public String getIndustry() {
    return industry;
  }

  public void setIndustry(final String industry) {
    this.industry = industry;
  }

  public String getCompanySize() {
    return companySize;
  }

  public void setCompanySize(final String companySize) {
    this.companySize = companySize;
  }

  public String getStateRegion() {
    return stateRegion;
  }

  public void setStateRegion(final String stateRegion) {
    this.stateRegion = stateRegion;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(final String country) {
    this.country = country;
  }

}
