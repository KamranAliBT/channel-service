/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: GuidDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.rss;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * GuidDto.
 */
public class GuidDto {

  private Boolean isPermaLink;

  private String value;
  
  public Boolean getIsPermaLink() {
    return isPermaLink;
  }

  @XmlAttribute
  public void setIsPermaLink(Boolean isPermaLink) {
    this.isPermaLink = isPermaLink;
  }

  public String getValue() {
    return value;
  }

  @XmlValue
  public void setValue(String value) {
    this.value = value;
  } 
}