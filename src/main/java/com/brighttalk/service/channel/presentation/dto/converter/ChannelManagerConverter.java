/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerConverter.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerPublicDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersPublicDto;
import com.brighttalk.service.channel.presentation.dto.ErrorDto;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.UserPublicDto;
import com.brighttalk.service.channel.presentation.dto.builder.ErrorBuilder;
import com.brighttalk.service.channel.presentation.dto.builder.LinksBuilder;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

/**
 * Converter - Responsible for converting between {@link ChannelManagerDto} and {@link ChannelManager} types.
 */
@Component
public class ChannelManagerConverter extends AbstractConverter {

  @Autowired
  private LinksBuilder linksBuilder;

  @Autowired
  private ErrorBuilder errorBuilder;

  /**
   * Converts a {@link ChannelManagersDto} to {@link ChannelManager Channel Managers}.
   * 
   * @param channelManagersDto channel managers
   * 
   * @return {@link ChannelManager Channel Managers}
   */
  public List<ChannelManager> convert(final ChannelManagersDto channelManagersDto) {

    List<ChannelManager> channelManagers = new ArrayList<ChannelManager>();

    for (ChannelManagerDto channelManagerDto : channelManagersDto.getChannelManagers()) {
      channelManagers.add(convert(channelManagerDto));
    }

    return channelManagers;
  }

  /**
   * Converts a {@link ChannelManagerDto} to {@link ChannelManager}.
   * 
   * @param channelManagerDto new channel manager
   * 
   * @return {@link ChannelManager}
   */
  public ChannelManager convert(final ChannelManagerDto channelManagerDto) {

    ChannelManager channelManager = new ChannelManager();
    channelManager.setId(channelManagerDto.getId());
    channelManager.setEmailAlerts(channelManagerDto.getEmailAlerts());

    if (channelManagerDto.getUser() != null) {
      User user = new User();
      user.setEmail(channelManagerDto.getUser().getEmail());

      channelManager.setUser(user);
    }

    return channelManager;
  }

  /**
   * Converts a {@link ChannelManager} to {@link ChannelManagerDto}.
   * 
   * @param channelManagers channel managers
   * @param createdChannelManagers new channel managers
   * 
   * @return {@link ChannelManagersDto}
   */
  public ChannelManagersPublicDto convertForPublic(final List<ChannelManager> channelManagers,
      final List<ChannelManager> createdChannelManagers) {

    ChannelManagersPublicDto channelManagersPublicDto = new ChannelManagersPublicDto();

    ErrorDto errorDto;

    int index;

    for (ChannelManager channelManager : channelManagers) {
      boolean found = false;

      for (ChannelManager createdChannelManager : createdChannelManagers) {
        if (channelManager.getUser().getEmail().equalsIgnoreCase(createdChannelManager.getUser().getEmail())) {
          channelManagersPublicDto.addChannelManager(convertForPublic(createdChannelManager));
          found = true;
          break;
        }
      }

      if (!found) {
        index = channelManagers.indexOf(channelManager);
        errorDto = errorBuilder.build(ErrorCode.USER_NOT_FOUND, index + 1);
        channelManagersPublicDto.addError(errorDto);
      }
    }

    return channelManagersPublicDto;
  }

  /**
   * Converts a {@link ChannelManager} to {@link ChannelManagerDto}.
   * 
   * @param channelManager new channel manager
   * 
   * @return {@link ChannelManagerDto}
   */
  public ChannelManagerPublicDto convertForPublic(final ChannelManager channelManager) {

    ChannelManagerPublicDto channelManagerPublicDto = new ChannelManagerPublicDto();
    channelManagerPublicDto.setId(channelManager.getId());
    channelManagerPublicDto.setUser(convertPublicUser(channelManager.getUser()));
    channelManagerPublicDto.setEmailAlerts(channelManager.getEmailAlerts());

    return channelManagerPublicDto;
  }

  /**
   * Converts a paginated list of {@link ChannelManager} to {@link ChannelManagersDto}.
   * 
   * @param channelManagers channel managers
   * @param converterContext context
   * 
   * @return channel managers dto
   */
  public ChannelManagersPublicDto convertForPublic(final PaginatedList<ChannelManager> channelManagers,
      final ConverterContext converterContext) {

    List<ChannelManagerPublicDto> channelManagerPublicDtos = new ArrayList<ChannelManagerPublicDto>();

    for (ChannelManager channelManager : channelManagers) {
      channelManagerPublicDtos.add(convertForPublic(channelManager));
    }

    ChannelManagersPublicDto channelManagersPublicDto = new ChannelManagersPublicDto();
    channelManagersPublicDto.setChannelManagers(channelManagerPublicDtos);

    List<LinkDto> links = linksBuilder.build(channelManagers, converterContext);

    if (!links.isEmpty()) {
      channelManagersPublicDto.setLinks(links);
    }

    return channelManagersPublicDto;
  }

  private UserPublicDto convertPublicUser(final User user) {

    UserPublicDto userPublicDto = new UserPublicDto();
    userPublicDto.setExternalId(user.getExternalId());
    userPublicDto.setFirstName(user.getFirstName());
    userPublicDto.setLastName(user.getLastName());
    userPublicDto.setEmail(user.getEmail());
    userPublicDto.setTimeZone(user.getTimeZone());
    userPublicDto.setJobTitle(user.getJobTitle());
    userPublicDto.setLevel(user.getLevel());
    userPublicDto.setCompanyName(user.getCompanyName());
    userPublicDto.setIndustry(user.getIndustry());
    userPublicDto.setCompanySize(user.getCompanySize());
    userPublicDto.setStateRegion(user.getStateRegion());
    userPublicDto.setCountry(user.getCountry());

    return userPublicDto;
  }

}
