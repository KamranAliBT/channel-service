/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: PingController.java 96705 2015-06-17 12:29:57Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Web {@link org.springframework.web.servlet.mvc.Controller Controller} that handles the ping request
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller
@RequestMapping(value = "/internal/xml/ping")
public class PingController {

  /**
   * Handles the Ping Request returning host name and host IP address in following format.
   * "PING OK | hostName | IPaddress"
   * 
   * @param response The current response.
   * @throws IOException exception
   */
  @RequestMapping(method = RequestMethod.GET)
  @ResponseBody
  public void handlePing(HttpServletResponse response) throws IOException {
    InetAddress ia = InetAddress.getLocalHost();
    PrintWriter writer = response.getWriter();
    writer.write("PING OK | " + ia.getHostName() + " | " + ia.getHostAddress());
  }
}
