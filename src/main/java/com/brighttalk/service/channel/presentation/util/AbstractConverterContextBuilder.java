/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: AbstractConverterContextBuilder.java 64842 2013-05-23 15:23:59Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.time.DateTimeFormatter;

/**
 * Abstract Builder Class used to construct instances of {@link ConverterContext} Objects.
 */
public abstract class AbstractConverterContextBuilder {

  /**
   * Gets service url, different implementations of the ConverterContextBuilder will provided different urls.
   * 
   * @return service url
   */
  protected abstract String getServiceUrl();

  /**
   * Build the context needed for the converter.
   * 
   * @param request The Request Object
   * @param formatter The Formatter to use
   * @param allowedQueryParams The list of allowed request parameters
   * @return The constructed Context
   */
  public ConverterContext build(final HttpServletRequest request, final DateTimeFormatter formatter,
    final Set<String> allowedQueryParams) {

    URL requestUrl = getFullRequestUrl(request);

    ConverterContext converterContext = new ConverterContext();
    converterContext.setRequestUrl(requestUrl);
    converterContext.setAllowedQueryParamsFilter(allowedQueryParams);
    converterContext.setDateTimeFormatter(formatter);

    return converterContext;
  }

  /**
   * Creates a full request URL based on the request details.
   * 
   * This includes reconstructing request query parameters.
   * 
   * @param request the request details
   * 
   * @return full request URL
   */
  private URL getFullRequestUrl(final HttpServletRequest request) {

    StringBuilder fullUrlBuilder = new StringBuilder();
    fullUrlBuilder.append(getServiceUrl());
    fullUrlBuilder.append(request.getServletPath());

    String queryString = request.getQueryString();
    if (queryString != null) {
      fullUrlBuilder.append("?").append(queryString);
    }

    URL apiUrl;

    try {

      apiUrl = new URL(fullUrlBuilder.toString());

    } catch (MalformedURLException exception) {

      throw new ApplicationException("Invalid url created [" + fullUrlBuilder.toString() + "].", exception);
    }

    return apiUrl;
  }

}