/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: package-info.java 98729 2015-08-03 14:44:25Z ssarraj $
 * ****************************************************************************
 */
/**
 * Communication Statistics DTOs package.
 */
package com.brighttalk.service.channel.presentation.dto.statistics;