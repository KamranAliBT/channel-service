/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: UpdateWebcast.java 72818 2014-01-21 15:28:21Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validation.scenario;

/**
 * Validation group type for webcast update scenario.
 */
public interface UpdateWebcast {

}
