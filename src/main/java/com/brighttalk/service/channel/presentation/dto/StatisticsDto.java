/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: StatisticsDto.java 59945 2013-01-29 12:13:58Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

/**
 * Dto to hold channel statistic details.
 */
public class StatisticsDto {

  private long viewedSeconds;

  private long upcomingCommunications;

  private long liveCommunications;

  private long recordedCommunications;

  private long subscribers;

  public long getViewedSeconds() {
    return viewedSeconds;
  }

  public void setViewedSeconds(long viewedSeconds) {
    this.viewedSeconds = viewedSeconds;
  }

  public long getLiveCommunications() {
    return liveCommunications;
  }

  public void setLiveCommunications(final long liveCommunications) {
    this.liveCommunications = liveCommunications;
  }

  public long getUpcomingCommunications() {
    return upcomingCommunications;
  }

  public void setUpcomingCommunications(long upcomingCommunications) {
    this.upcomingCommunications = upcomingCommunications;
  }

  public long getRecordedCommunications() {
    return recordedCommunications;
  }

  public void setRecordedCommunications(long recordedCommunications) {
    this.recordedCommunications = recordedCommunications;
  }

  public long getSubscribers() {
    return subscribers;
  }

  public void setSubscribers(long subscribers) {
    this.subscribers = subscribers;
  }
}
