/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastRegistrationsController.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.presentation.dto.converter.context.builder.ExternalConverterContextBuilder;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.Realm;
import com.brighttalk.common.user.Session;
import com.brighttalk.common.user.User;
import com.brighttalk.common.web.annotation.AuthenticatedSession;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.common.web.annotation.UserAuthenticator;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria.CommunicationStatusFilterType;
import com.brighttalk.service.channel.business.domain.PaginatedListWithTotalPages;
import com.brighttalk.service.channel.business.service.communication.CommunicationRegistrationService;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastRegistrationsConverter;
import com.brighttalk.service.channel.presentation.dto.registrations.WebcastRegistrationsDto;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls requesting webcast registrations.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 */
@Controller
public class WebcastRegistrationsController {

  /** A filter option indicating webcast status(es) that is(are) being requested. Notation using underscore. */
  protected static final String WEBCAST_STATUS_WITH_UNDERSCORE = "webcast_status";

  /**
   * The date time format to be returned in the time-related nodes of the response. This parameter triggers the usage of
   * {@link DateTimeFormat} annotation. Notation using underscore.
   */
  protected static final String DATE_TIME_FORMAT_WITH_UNDERSCORE = "datetime_format";

  private static final Set<String> ALLOWED_REQUEST_PARAMS = new HashSet<String>(Arrays.asList(
      DATE_TIME_FORMAT_WITH_UNDERSCORE, ApiRequestParam.PAGE_NUMBER, ApiRequestParam.PAGE_SIZE,
      WEBCAST_STATUS_WITH_UNDERSCORE));

  @Autowired
  private CommunicationRegistrationService communicationRegistrationService;

  @Autowired
  private WebcastRegistrationsConverter converter;

  @Autowired
  private UserAuthenticator authenticator;

  @Autowired
  private ExternalConverterContextBuilder contextBuilder;

  /**
   * Handles a request to find webcast registrations for a given user using search criteria context.
   * 
   * @param request The Request
   * @param user The Current User
   * @param formatter date formatter
   * @param webcastStatus webcast status filter (optional)
   * @param pageNumber the page number of the paginated registrations to retrieve (optional)
   * @param pageSize the size of the page to retrieve (optional)
   * 
   * @return dto
   */
  @RequestMapping(value = "/my/webcast_registrations", method = RequestMethod.GET)
  @ResponseBody
  public WebcastRegistrationsDto getWebcastRegistrations(final HttpServletRequest request,
    @AuthenticatedUser final User user,
    @RequestParam(value = WEBCAST_STATUS_WITH_UNDERSCORE, required = false) final String webcastStatus,
    @RequestParam(value = ApiRequestParam.PAGE_NUMBER, required = false) final Integer pageNumber,
    @RequestParam(value = ApiRequestParam.PAGE_SIZE, required = false) final Integer pageSize,
    @DateTimeFormat final DateTimeFormatter formatter) {

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(pageNumber);
    criteria.setPageSize(pageSize);
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.convert(webcastStatus));

    PaginatedListWithTotalPages<CommunicationRegistration> registrations = communicationRegistrationService.findRegistrations(
        user, criteria);

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);
    WebcastRegistrationsDto webcastRegistrationsDto = converter.convert(registrations, converterContext);

    return webcastRegistrationsDto;
  }

  /**
   * Handles a request to find webcast registrations for a given user using search criteria context.
   * 
   * @param request The Request
   * @param user The Current User
   * @param formatter date formatter
   * @param channelIds the comma seperated list of channel ids
   * @param webcastStatus webcast status filter (optional)
   * @param pageNumber the page number of the paginated registrations to retrieve (optional)
   * @param pageSize the size of the page to retrieve (optional)
   * 
   * @return dto
   */
  @RequestMapping(value = "/my/channel/{channelIds}/webcast_registrations", method = RequestMethod.GET)
  @ResponseBody
  public WebcastRegistrationsDto getWebcastRegistrationsInChannels(final HttpServletRequest request,
    @AuthenticatedUser final User user,
    @RequestParam(value = WEBCAST_STATUS_WITH_UNDERSCORE, required = false) final String webcastStatus,
    @RequestParam(value = ApiRequestParam.PAGE_NUMBER, required = false) final Integer pageNumber,
    @RequestParam(value = ApiRequestParam.PAGE_SIZE, required = false) final Integer pageSize,
    @DateTimeFormat final DateTimeFormatter formatter, @PathVariable final String channelIds) {

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(pageNumber);
    criteria.setPageSize(pageSize);
    criteria.setChannelIds(splitChannelIds(channelIds));
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.convert(webcastStatus));

    PaginatedListWithTotalPages<CommunicationRegistration> registrations = communicationRegistrationService.findRegistrations(
        user, criteria);

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);
    WebcastRegistrationsDto webcastRegistrationsDto = converter.convert(registrations, converterContext);

    return webcastRegistrationsDto;
  }

  /**
   * Handles a request to find webcast registrations for a user in a given realm using search criteria context.
   * 
   * @param request The Request
   * @param session the session of user making the request
   * @param formatter date formatter
   * @param realmId the id of the realm
   * @param webcastStatus webcast status filter (optional)
   * @param pageNumber the page number of the paginated registrations to retrieve (optional)
   * @param pageSize the size of the page to retrieve (optional)
   * 
   * @return dto
   */
  @RequestMapping(value = "/my/realm/{realmId}/webcast_registrations", method = RequestMethod.GET)
  @ResponseBody
  public WebcastRegistrationsDto getWebcastRegistrationsInRealm(final HttpServletRequest request,
    @AuthenticatedSession final Session session,
    @RequestParam(value = WEBCAST_STATUS_WITH_UNDERSCORE, required = false) final String webcastStatus,
    @RequestParam(value = ApiRequestParam.PAGE_NUMBER, required = false) final Integer pageNumber,
    @RequestParam(value = ApiRequestParam.PAGE_SIZE, required = false) final Integer pageSize,
    @DateTimeFormat final DateTimeFormatter formatter, @PathVariable final Integer realmId) {

    User user = getUser(session, realmId);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(pageNumber);
    criteria.setPageSize(pageSize);
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.convert(webcastStatus));

    PaginatedListWithTotalPages<CommunicationRegistration> registrations = communicationRegistrationService.findRegistrations(
        user, criteria);

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);
    WebcastRegistrationsDto webcastRegistrationsDto = converter.convert(registrations, converterContext);

    return webcastRegistrationsDto;
  }

  /**
   * Handles a request to find webcast registrations for a user in a given realm using search criteria context.
   * 
   * @param request The Request
   * @param session the session of user making the request
   * @param formatter date formatter
   * @param channelIds the comma seperated list of channel ids
   * @param realmId the id of the realm
   * @param webcastStatus webcast status filter (optional)
   * @param pageNumber the page number of the paginated registrations to retrieve (optional)
   * @param pageSize the size of the page to retrieve (optional)
   * 
   * @return dto
   */
  @RequestMapping(value = "/my/realm/{realmId}/channel/{channelIds}/webcast_registrations", method = RequestMethod.GET)
  @ResponseBody
  public WebcastRegistrationsDto getWebcastRegistrationsInChannelsInRealm(final HttpServletRequest request,
    @AuthenticatedSession final Session session, @PathVariable final String channelIds,
    @PathVariable final Integer realmId,
    @RequestParam(value = WEBCAST_STATUS_WITH_UNDERSCORE, required = false) final String webcastStatus,
    @RequestParam(value = ApiRequestParam.PAGE_NUMBER, required = false) final Integer pageNumber,
    @RequestParam(value = ApiRequestParam.PAGE_SIZE, required = false) final Integer pageSize,
    @DateTimeFormat final DateTimeFormatter formatter) {

    User user = getUser(session, realmId);

    CommunicationRegistrationSearchCriteria criteria = new CommunicationRegistrationSearchCriteria();
    criteria.setPageNumber(pageNumber);
    criteria.setPageSize(pageSize);
    criteria.setChannelIds(splitChannelIds(channelIds));
    criteria.setCommunicationStatusFilter(CommunicationStatusFilterType.convert(webcastStatus));

    PaginatedListWithTotalPages<CommunicationRegistration> registrations = communicationRegistrationService.findRegistrations(
        user, criteria);

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);
    WebcastRegistrationsDto webcastRegistrationsDto = converter.convert(registrations, converterContext);

    return webcastRegistrationsDto;
  }

  /**
   * Helper method to findRegistrations user based on session and realm.
   * 
   * @param session the authenticated session
   * @param realmId the id of the realm
   * 
   * @return User authenticated user
   * 
   * @throws com.brighttalk.common.user.error.UserAuthenticationException when the user is not authenticated.
   */
  private User getUser(final Session session, final Integer realmId) {

    Realm realm = new Realm(realmId, null);

    return authenticator.authoriseUser(session, User.Role.USER, realm);
  }

  /**
   * Helper method to split string of channel ids into a list of Longs.
   * 
   * @param ids the string value to convert.
   * 
   * @return collection of ids
   */
  private List<Long> splitChannelIds(final String ids) {

    List<Long> channelIds = new ArrayList<Long>();

    for (String id : ids.split(",")) {

      try {

        channelIds.add(Long.valueOf(id));

      } catch (NumberFormatException exception) {

        throw new ApplicationException("Invalid channel Id", exception, "ClientError");
      }
    }

    return channelIds;
  }
}