/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ListMaxLength.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.annotation.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Validate that the supplied String List max Length is less than or equal to the supplied max length.
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ListMaxLengthValidator.class)
@Documented
public @interface ListMaxLength {

  // CHECKSTYLE:OFF
  /**
   * @return length of String List must be lower or equal to the supplied value. Default value is the maximum allowed
   * integer.
   */
  int max() default Integer.MAX_VALUE;

  // CHECKSTYLE:ON

  /** Default error message */
  String message() default "Invalid list Length";

  /** Default groups */
  Class<?>[] groups() default {

  };

  /** Default pay load */
  Class<? extends Payload>[] payload() default {

  };

}
