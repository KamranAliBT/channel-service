/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagersDto.java 57059 2012-11-06 17:22:01Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.ArrayList;
import java.util.List;

import com.brighttalk.service.channel.business.domain.validator.Validator;

/**
 * DTO object representing a channel managers list.
 */
public class ChannelManagersDto {

  private List<ChannelManagerDto> channelManagers;

  public List<ChannelManagerDto> getChannelManagers() {
    return channelManagers;
  }

  public void setChannelManagers(final List<ChannelManagerDto> channelManagers) {
    this.channelManagers = channelManagers;
  }

  /**
   * Add a single channel manager.
   * 
   * @param channelManager channel manager
   */
  public void addChannelManager(final ChannelManagerDto channelManager) {
    if (channelManagers == null) {
      channelManagers = new ArrayList<ChannelManagerDto>();
    }
    channelManagers.add(channelManager);
  }

  /**
   * Validates this dto with given validator.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<ChannelManagersDto> validator) {
    validator.validate(this);
  }

}