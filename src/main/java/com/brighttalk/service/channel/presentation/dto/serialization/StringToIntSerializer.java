/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: StringToIntSerializer.java 101273 2015-10-09 09:30:23Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.serialization;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

public class StringToIntSerializer extends JsonSerializer<String> {

  @Override
  public void serialize(String string,
      JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider)
      throws IOException, JsonProcessingException {
    jsonGenerator.writeObject(Integer.parseInt(string));
  }
}