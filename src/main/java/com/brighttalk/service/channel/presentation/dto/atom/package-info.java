/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the DTO classes and helper classes 
 * used in marshalling the Atom feed XML from business objects.
 */
package com.brighttalk.service.channel.presentation.dto.atom;