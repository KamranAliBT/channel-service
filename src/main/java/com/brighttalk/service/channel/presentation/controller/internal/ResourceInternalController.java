/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceInternalController.java 63781 2013-04-29 10:40:35Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.service.ResourceService;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.converter.ResourceConverter;
import com.brighttalk.service.channel.presentation.dto.validator.ResourceInternalUpdateValidator;

/**
 * The internal Resource controller handling internal call from other services. It performs operations on {@link Resource resource}.
 * As all internal controller it does not require authentication.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller
@RequestMapping(value = "/internal/resource")
public class ResourceInternalController {

  /**
   * Communication service
   */
  @Autowired
  private ResourceService resourceService;

  /**
   * Resource converter
   */
  @Autowired
  private ResourceConverter converter;

  /**
   * Handles a internal request to update resource.
   * 
   * @param resourceId the resource id
   * @param resourceDto the resource DTO to be updated
   * 
   * @return resource
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when resource cannot be found.
   * @throws com.brighttalk.service.channel.business.error.InvalidResourceException when provided resourceDto is
   * syntactically invalid
   */
  @RequestMapping(value = "/{resourceId}", method = RequestMethod.PUT)
  @ResponseBody
  public ResourceDto updateResource(@PathVariable Long resourceId, @RequestBody ResourceDto resourceDto) {

    resourceDto.validate(new ResourceInternalUpdateValidator(resourceId));
    Resource resource = converter.convertForUpdate(resourceDto);

    Resource updatedResource = resourceService.updateResource(resource);
    ResourceDto resourceDtoToReturn = converter.convert(updatedResource);

    return resourceDtoToReturn;
  }

}