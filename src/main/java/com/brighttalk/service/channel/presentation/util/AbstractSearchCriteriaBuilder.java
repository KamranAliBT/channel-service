/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AbstractSearchCriteriaBuilder.java 57951 2012-11-26 16:48:57Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import javax.servlet.http.HttpServletRequest;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;

/**
 * Abstract Build Class with common methods helpful when extracting business objects from request objects. 
 */
public abstract class AbstractSearchCriteriaBuilder {

  /**
   * Extract boolean from request object.
   * 
   * @param request the request
   * @param requestParameter the request parameter
   * 
   * @return extracted boolean
   */
  protected Boolean getBooleanFromRequest(HttpServletRequest request, String requestParameter) {

    String parameterValue = request.getParameter(requestParameter);

    if (parameterValue != null) {

      if (isBooleanString(parameterValue)) {
        return Boolean.valueOf(parameterValue);

      } else {
        throw new ApplicationException("Invalid boolean value requested [" + parameterValue + "]", null, "ClientError");
      }
    }

    return null;
  }

  private boolean isBooleanString(String value) {
    return "true".equals(value) || "false".equals(value);
  }

  /**
   * Extract integer from request object.
   * 
   * @param request the request
   * @param requestParameter the request parameter
   * 
   * @return extracted integer
   */
  protected Integer getIntegerFromRequest(HttpServletRequest request, String requestParameter) {

    String parameterValue = request.getParameter(requestParameter);

    if (parameterValue != null) {
      return Integer.valueOf(parameterValue);
    }

    return null;
  }

  /**
   * Encapulating method for converting a sort order string to an enum.
   * 
   * @param sortOrder The String to convert
   * @return The converted Enum
   */
  protected SortOrder convertSortOrder(String sortOrder) {
    return SortOrder.convert(sortOrder);
  }
}