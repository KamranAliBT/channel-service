/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelController.java 86487 2014-11-21 15:15:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.PresentationCriteria;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.ChannelUpdateAuthorisationException;
import com.brighttalk.service.channel.business.error.InvalidChannelException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.error.UserNotFoundException;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.channel.ChannelService;
import com.brighttalk.service.channel.presentation.dto.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.ChannelPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelConverter;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateChannel;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;
import com.brighttalk.service.channel.presentation.util.PresentationCriteriaBuilder;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform CRUD operations on channel.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 */
@Controller
@RequestMapping(value = "/channel")
public class ChannelController {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(ChannelController.class);

  /** Channel service */
  @Autowired
  private ChannelService channelService;

  /** Channel finder */
  @Autowired
  private ChannelFinder channelFinder;

  /** Channel finder */
  @Autowired
  @Qualifier(value = "readOnlyChannelFinder")
  private ChannelFinder readOnlyChannelFinder;

  /** Channel converter. */
  @Autowired
  private ChannelConverter channelConverter;

  /** Brighttalk validator. */
  @Autowired
  private BrighttalkValidator validator;

  /** Presentation criteria builder */
  @Autowired
  private PresentationCriteriaBuilder presentationCriteriaBuilder;

  /**
   * Handles a request to find the public information of a channel.
   * 
   * @param channelId The Id of the channel to be retrieved.
   * @param request the http request
   * @param formatter the date time formatter
   * 
   * @return public channel
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException if the channel is not found -
   */
  @RequestMapping(value = "/{channelId}/public", method = RequestMethod.GET)
  @ResponseBody
  public ChannelPublicDto find(final HttpServletRequest request, @PathVariable final Long channelId,
      @DateTimeFormat final DateTimeFormatter formatter) {

    PresentationCriteria criteria = presentationCriteriaBuilder.build(request);
    validator.validate(criteria, ValidationException.class);

    Channel channel = readOnlyChannelFinder.find(channelId);

    ChannelPublicDto channelPublicDto = channelConverter.convertForPublic(channel, formatter, criteria);

    return channelPublicDto;
  }

  /**
   * Handles a request to find a channel.
   * 
   * @param user The Current User
   * @param channelId The Id of the channel to be retrieved.
   * @param formatter used to format dates in ChannelDto response
   * 
   * @return The found channel {@link ChannelDto channel DTO} object.
   * 
   * @throws UserAuthorisationException when user is not allowed to access given channel.
   */
  @RequestMapping(value = "/{channelId}", method = RequestMethod.GET)
  @ResponseBody
  public ChannelDto find(@AuthenticatedUser final User user, @PathVariable final Long channelId,
      @DateTimeFormat final DateTimeFormatter formatter) {

    Channel channel = null;

    try {
      channel = channelFinder.find(user, channelId);
    } catch (NotFoundException nfe) {
      throw new UserAuthorisationException("Channel [" + channelId + "] doesnt exist.", nfe);
    }

    ChannelDto channelDto = channelConverter.convert(channel, formatter);

    return channelDto;
  }

  /**
   * Handles a request to update a channel.
   * 
   * @param user The Current User
   * @param channelId The Id of the channel to be retrieved.
   * @param channelDto The channel details to update.
   * @param formatter used to format dates in ChannelDto response
   * 
   * @return The updated channel {@link ChannelDto channel DTO} object.
   * 
   * @throws UserAuthorisationException when user is not allowed to access given channel.
   */
  @RequestMapping(value = "/{channelId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ChannelDto update(@AuthenticatedUser final User user, @PathVariable final Long channelId,
      @RequestBody final ChannelDto channelDto, @DateTimeFormat final DateTimeFormatter formatter) {

    channelDto.setId(channelId);
    validator.validate(channelDto, UpdateChannel.class);

    Channel channel = channelConverter.convert(channelDto);

    Channel updatedChannel;

    try {
      updatedChannel = channelService.update(user, channel);
    } catch (ChannelNotFoundException e) {
      throw new ResourceNotFoundException(e.getMessage(), ErrorCode.CHANNEL_NOT_FOUND, e.getChannelId());
    } catch (InvalidChannelException e) {
      throw new BadRequestException(e.getMessage(), ErrorCode.CHANNEL_TITLE_IN_USE);
    } catch (UserNotFoundException e) {
      throw new ResourceNotFoundException(e.getMessage(), ErrorCode.CHANNEL_USER_NOT_FOUND, e.getId());
    } catch (ChannelUpdateAuthorisationException e) {
      throw new NotAuthorisedException(e.getMessage(), ErrorCode.CHANNEL_UPDATE_AUTHORISATION, e.getId());
    } catch (UserAuthorisationException e) {
      throw new NotAuthorisedException(e.getMessage(), e.getUserErrorCode());
    }

    ChannelDto updatedChannelDto = channelConverter.convert(updatedChannel, formatter);

    return updatedChannelDto;
  }

  /**
   * Handles a request to delete channel.
   * 
   * @param user authenticated user
   * @param channelId of a channel to be deleted
   */
  @RequestMapping(value = "/{channelId}", method = RequestMethod.DELETE)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void delete(@AuthenticatedUser final User user, @PathVariable final Long channelId) {

    channelService.delete(user, channelId);
  }

}
