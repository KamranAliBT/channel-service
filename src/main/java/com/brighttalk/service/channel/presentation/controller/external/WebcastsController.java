/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastsController.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.presentation.dto.converter.context.builder.ExternalConverterContextBuilder;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.WebcastsPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastsPublicConverter;
import com.brighttalk.service.channel.presentation.util.CommunicationsSearchCriteriaBuilder;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform operations on {@link Communication communications}.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller
@RequestMapping(value = "/webcasts")
public class WebcastsController {

  /** Allowed request parameters. */
  protected static final Set<String> ALLOWED_REQUEST_PARAMS = new HashSet<String>(Arrays.asList(
          ApiRequestParam.DATE_TIME_FORMAT, ApiRequestParam.IDS, ApiRequestParam.EXPAND, ApiRequestParam.PAGE_NUMBER,
          ApiRequestParam.PAGE_SIZE));

  /** Communication service */
  @Autowired
  @Qualifier(value = "readOnlyCommunicationFinder")
  private CommunicationFinder communicationFinder;

  /** Converter converts collection of Communications to WebcastsDto. */
  @Autowired
  private WebcastsPublicConverter webcastsPublicConverter;

  @Autowired
  private ExternalConverterContextBuilder contextBuilder;

  /** Find public communication search criteria builder. Used to wrap the request parameters to object. */
  @Autowired
  private CommunicationsSearchCriteriaBuilder communicationsSearchCriteriaBuilder;

  /**
   * Brighttalk validator.
   */
  @Autowired
  private BrighttalkValidator validator;

  /**
   * Handles a request to get multiple coomunications in a single call.
   * 
   * @param request The Http Request
   * @param formatter The dateTimeFormatter - instantiated using the standard anotation
   * 
   * @return Dto of public webcasts
   */
  @RequestMapping(value = "/public", method = RequestMethod.GET)
  @ResponseBody
  public WebcastsPublicDto getPublicWebcasts(final HttpServletRequest request,
                                             @DateTimeFormat final DateTimeFormatter formatter) {

    CommunicationsSearchCriteria criteria = communicationsSearchCriteriaBuilder.build(request);

    validator.validate(criteria, ValidationException.class);

    PaginatedList<Communication> communications = communicationFinder.find(criteria);

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);

    WebcastsPublicDto webcastsDto = webcastsPublicConverter.convertPublic(communications, converterContext);

    return webcastsDto;

  }
}
