/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsController.java 92672 2015-04-01 08:27:25Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.error.ErrorDto;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionsSummary;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.error.SubscriptionRequestException;
import com.brighttalk.service.channel.business.error.SummitNotFoundException;
import com.brighttalk.service.channel.business.error.UserNotFoundException;
import com.brighttalk.service.channel.business.service.channel.SubscriptionsService;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsSummaryDto;
import com.brighttalk.service.channel.presentation.dto.converter.SubscriptionsConverter;
import com.brighttalk.service.channel.presentation.dto.converter.SubscriptionsSummaryConverter;
import com.brighttalk.service.channel.presentation.dto.validator.SubscriptionsCreateValidator;
import com.brighttalk.service.channel.presentation.dto.validator.SubscriptionsUpdateValidator;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

/**
 * Web {@link org.springframework.web.servlet.mvc.Controller Controller} that provides _internal_ web API request for
 * subscriptions.
 */
@Controller
@RequestMapping(value = "/internal")
public class SubscriptionsController {

  private static final Logger LOGGER = Logger.getLogger(SubscriptionsController.class);

  @Autowired
  private SubscriptionsCreateValidator createValidator;

  @Autowired
  private SubscriptionsUpdateValidator updateValidator;

  @Autowired
  private SubscriptionsConverter subscriptionsConverter;

  @Autowired
  private SubscriptionsSummaryConverter subscriptionsSummaryConverter;

  @Autowired
  private SubscriptionsService subscriptionsService;

  /**
   * Subscribe a list of users with the given user IDs or email addresses to the given channel, also creates the
   * subscriber subscription context.
   * 
   * @param channelId the channel ID.
   * @param subscriptionsDto the subscriptions to create.
   * @param dryRun if it's a dry run call.
   * @param formatter date formatter.
   * 
   * @return {@link ResponseEntity} containing the {@link SubscriptionsSummaryDto} and the relevant {@link HttpStatus}.
   * {@link HttpStatus#OK} if the at least one subscription created, otherwise {@link HttpStatus#BAD_REQUEST} if no
   * subscriptions was created.
   */
  @RequestMapping(value = "/channel/{channelId}/subscriptions", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<SubscriptionsSummaryDto> createSubscriptions(@PathVariable final Long channelId,
      @RequestBody final SubscriptionsDto subscriptionsDto,
      @RequestParam(required = false, defaultValue = "false") final Boolean dryRun,
      @DateTimeFormat final DateTimeFormatter formatter) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Recieved a request to create a list of subscriptions for the supplied channel [" + channelId + "].");
    }

    subscriptionsDto.validate(createValidator);
    List<Subscription> subscriptions = subscriptionsConverter.convert(subscriptionsDto.getSubscriptions());
    SubscriptionsSummary summary = subscriptionsService.create(channelId, subscriptions, dryRun);
    SubscriptionsSummaryDto summaryDto = subscriptionsSummaryConverter.convert(summary, formatter);
    HttpStatus status = getHttpStatus(summaryDto);

    return new ResponseEntity<SubscriptionsSummaryDto>(summaryDto, status);
  }

  /**
   * Handle a request to update the supplied list of subscriptions referral and creates subscription context.
   * 
   * @param subscriptionsDto The subscriptions to update.
   * @param dryRun if it's a dry run call.
   * @param formatter date formatter.
   * 
   * @return {@link ResponseEntity} containing the {@link SubscriptionsSummaryDto} and the relevant {@link HttpStatus}.
   * {@link HttpStatus#OK} if the at least one subscription updated, otherwise {@link HttpStatus#BAD_REQUEST} if no
   * subscriptions was updated.
   */
  @RequestMapping(value = "/subscriptions", method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<SubscriptionsSummaryDto> updateSubscriptions(
      @RequestBody final SubscriptionsDto subscriptionsDto,
      @RequestParam(required = false, defaultValue = "false") final Boolean dryRun,
      @DateTimeFormat final DateTimeFormatter formatter) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Recieved a request to update the supplied list of subscriptions [" + subscriptionsDto + "].");
    }

    subscriptionsDto.validate(updateValidator);
    List<Subscription> subscriptions = subscriptionsConverter.convert(subscriptionsDto.getSubscriptions());
    SubscriptionsSummary summary = subscriptionsService.update(subscriptions, dryRun);
    SubscriptionsSummaryDto summaryDto = subscriptionsSummaryConverter.convert(summary, formatter);
    HttpStatus status = getHttpStatus(summaryDto);

    return new ResponseEntity<SubscriptionsSummaryDto>(summaryDto, status);
  }

  /**
   * Exception handler method handles {@link SubscriptionRequestException} thrown from the other layers of this API.
   * 
   * @param ex The {@link SubscriptionRequestException} handled by this method.
   * @return {@link ErrorDto} contain the relevant error code.
   */
  @ExceptionHandler(SubscriptionRequestException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ResponseBody
  private ErrorDto handleSubscriptionRequestException(SubscriptionRequestException ex) {
    LOGGER.debug("The supplied subscriptions contain invalid fields.");
    ErrorDto errorDto = new ErrorDto(ex.getErrorCode().getName(), ex.getMessage());
    return errorDto;
  }

  /**
   * Exception handler method handles {@link ChannelNotFoundException} thrown from the other layers of this API.
   * 
   * @param ex The {@link ChannelNotFoundException} handled by this method.
   * @return {@link ErrorDto} contain {@link ErrorCode#CHANNEL_NOT_FOUND} error code.
   */
  @ExceptionHandler(value = ChannelNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  private ErrorDto handleChannelNotFoundException(ChannelNotFoundException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("The supplied channel id [" + ex.getChannelId()
          + "] does not identify an exisitng channel; cause["
          + ex.getMessage() + "].");
    }
    ErrorDto errorDto = new ErrorDto(ErrorCode.CHANNEL_NOT_FOUND.getName(), ex.getMessage());
    return errorDto;
  }

  /**
   * Exception handler method handles {@link SummitNotFoundException} thrown from the other layers of this API.
   * 
   * @param ex The {@link SummitNotFoundException} handled by this method.
   * @return {@link ErrorDto} contain {@link ErrorCode#SUMMIT_NOT_FOUND} error code.
   */
  @ExceptionHandler(SummitNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  private ErrorDto handleSummitNotFoundException(SummitNotFoundException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("The supplied summit id [" + ex.getSummitId() + "] does not identify an exisitng summit; cause["
          + ex.getMessage() + "].");
    }
    ErrorDto errorDto = new ErrorDto(ErrorCode.SUMMIT_NOT_FOUND.getName(), ex.getMessage());
    return errorDto;
  }

  /**
   * Exception handler method handles {@link CommunicationNotFoundException} thrown from the other layers of this API.
   * 
   * @param ex The {@link CommunicationNotFoundException} handled by this method.
   * @return {@link ErrorDto} contain {@link ErrorCode#COMMUNICATION_NOT_FOUND} error code.
   */
  @ExceptionHandler(value = CommunicationNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  private ErrorDto handleCommunicationNotFoundException(CommunicationNotFoundException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("The supplied communication id [" + ex.getCommunicationId()
          + "] does not identify an exisitng communication; cause["
          + ex.getMessage() + "].");
    }
    ErrorDto errorDto = new ErrorDto(ErrorCode.COMMUNICATION_NOT_FOUND.getName(), ex.getMessage());
    return errorDto;
  }

  /**
   * Exception handler method handles {@link UserNotFoundException} thrown from the other layers of this API.
   * 
   * @param ex The {@link UserNotFoundException} handled by this method.
   * @return {@link ErrorDto} contain {@link ErrorCode#USER_NOT_FOUND} error code.
   */
  @ExceptionHandler(value = UserNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ResponseBody
  private ErrorDto handleUserNotFoundException(UserNotFoundException ex) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("The supplied user id [" + ex.getId() + "] does not identify an exisitng user; cause["
          + ex.getMessage() + "].");
    }
    ErrorDto errorDto = new ErrorDto(ErrorCode.USER_NOT_FOUND.getName(), ex.getMessage());
    return errorDto;
  }

  /**
   * Retrieve the HTTP status based on the created subscriptions.
   * 
   * @param summary The subscription summary DTO to check.
   * @return {@link HttpStatus#OK} if there are list of subscription summary DTO, otherwise
   * {@link HttpStatus#BAD_REQUEST} if the summary contain no subscriptions.
   */
  private HttpStatus getHttpStatus(final SubscriptionsSummaryDto summary) {
    HttpStatus status = HttpStatus.OK;
    if (CollectionUtils.isEmpty(summary.getSubscriptions())) {
      status = HttpStatus.BAD_REQUEST;
    }
    return status;
  }
}