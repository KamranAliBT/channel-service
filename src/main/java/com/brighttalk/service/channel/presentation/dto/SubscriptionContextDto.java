/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionContextDto.java 91908 2015-03-18 16:46:30Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;

/**
 * Represents a channel subscription context DTO.
 */
public class SubscriptionContextDto {

  private String leadType;
  private String leadContext;
  private String engagementScore;

  /**
   * Default Constructor.
   */
  public SubscriptionContextDto() {
  }

  /**
   * @param leadType The lead type.
   * @param leadContext The lead context.
   * @param engagementScore The lead engagement score.
   */
  public SubscriptionContextDto(String leadType, String leadContext, String engagementScore) {
    this.leadType = leadType;
    this.leadContext = leadContext;
    this.engagementScore = engagementScore;
  }

  /**
   * @return the leadType
   */
  public String getLeadType() {
    return leadType;
  }

  /**
   * @return the leadContext
   */
  public String getLeadContext() {
    return leadContext;
  }

  /**
   * @return the engagementScore
   */
  public String getEngagementScore() {
    return engagementScore;
  }

  /**
   * @param leadType the leadType to set
   */
  public void setLeadType(String leadType) {
    this.leadType = leadType;
  }

  /**
   * @param leadContext the leadContext to set
   */
  public void setLeadContext(String leadContext) {
    this.leadContext = leadContext;
  }

  /**
   * @param engagementScore the engagementScore to set
   */
  public void setEngagementScore(String engagementScore) {
    this.engagementScore = engagementScore;
  }

  /**
   * Checks this subscription context DTO is for summit lead type.
   * 
   * @return <code>true</code> if the lead type is summit, <code>false</code> otherwise.
   */
  @JsonIgnore
  public boolean isLeadTypeSummit() {
    return leadType != null && SubscriptionLeadType.isLeadTypeSummit(leadType);
  }

  /**
   * Checks this subscription context DTO is for content lead type.
   * 
   * @return <code>true</code> if the lead type is content, <code>false</code> otherwise.
   */
  @JsonIgnore
  public boolean isLeadTypeContent() {
    return leadType != null && SubscriptionLeadType.isLeadTypeContent(leadType);
  }

  /**
   * Checks this subscription context DTO is for keyword lead type.
   * 
   * @return <code>true</code> if the lead type is keyword, <code>false</code> otherwise.
   */
  @JsonIgnore
  public boolean isLeadTypeKeyword() {
    return leadType != null && SubscriptionLeadType.isLeadTypeKeyword(leadType);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("leadType", leadType);
    builder.append("leadContext", leadContext);
    builder.append("engagementScore", engagementScore);
    return builder.toString();
  }
}
