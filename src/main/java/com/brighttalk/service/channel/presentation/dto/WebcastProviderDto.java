/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastProviderDto.java 97153 2015-06-25 15:15:16Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.domain.validator.Validator;

/**
 * DTO object representing webcast provider.
 * 
 * Webcast Provider Dto is used to update communication type.
 */
@XmlRootElement(name = "provider")
public class WebcastProviderDto {

  @NotNull(message = "{error.invalid.provider.nullName}")
  @Pattern(regexp = "mediazone|video|brighttalkhd", message = "{error.invalid.provider.invalidName}")
  private String name;

  private List<RenditionDto> renditionsDto;

  private ResourceDto resource;

  private LinkDto link;

  private String updated;

  /**
   * Default constructor.
   */
  public WebcastProviderDto() {
  }

  /**
   * @param name The provider name.
   */
  public WebcastProviderDto(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @XmlAttribute
  public void setName(final String name) {
    this.name = name;
  }

  @XmlElement(name = "rendition")
  public void setRenditions(final List<RenditionDto> renditionsDto) {
    this.renditionsDto = renditionsDto;
  }

  public List<RenditionDto> getRenditions() {
    return renditionsDto;
  }

  /**
   * Indicates if this webcast has provider set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasRenditions() {
    return !CollectionUtils.isEmpty(renditionsDto);
  }

  /**
   * @return the resourcedto
   */
  public ResourceDto getResource() {
    return resource;
  }

  /**
   * @param resource the mediazoneResourceDto to set
   */
  @XmlElement(name = "resource")
  public void setResource(final ResourceDto resource) {
    this.resource = resource;
  }

  /**
   * @return the linkDto
   */
  public LinkDto getLink() {
    return link;
  }

  /**
   * @param link the linkDto to set
   */
  @XmlElement(name = "link")
  public void setLink(final LinkDto link) {
    this.link = link;
  }

  /**
   * @return the updated
   */
  public String getUpdated() {
    return updated;
  }

  /**
   * @param updated the updated to set
   */
  public void setUpdated(final String updated) {
    this.updated = updated;
  }

  /**
   * Validates this dto with given validator.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<WebcastProviderDto> validator) {
    validator.validate(this);
  }

  @Override
  public String toString() {
    return "Name: " + name;

  }

}