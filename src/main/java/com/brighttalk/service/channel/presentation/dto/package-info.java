/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 41969 2012-02-06 11:13:46Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the DTO classes used in marshalling the XML to and from business objects.
 */
package com.brighttalk.service.channel.presentation.dto;