/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsSummaryConverter.java 92672 2015-04-01 08:27:25Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError.SubscriptionErrorCode;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionsSummary;
import com.brighttalk.service.channel.presentation.dto.SubscriptionContextDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionErrorDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsSummaryDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;

/**
 * Converter - Responsible for converting {@link SubscriptionsSummary} to {@link SubscriptionsSummaryDto}.
 */
@Component
public class SubscriptionsSummaryConverter {

  /**
   * Converts a subscriptions summary DTO to a subscriptions summary object.
   * 
   * @param summary the summary to convert
   * @param formatter the date time formatter
   * 
   * @return the subscriptions summary DTO
   */
  public SubscriptionsSummaryDto convert(final SubscriptionsSummary summary, final DateTimeFormatter formatter) {
    SubscriptionsSummaryDto summaryDto = new SubscriptionsSummaryDto();
    summaryDto.setSubscriptions(convertSubscriptions(summary.getSubscriptions(), formatter));
    summaryDto.setErrors(convertErrors(summary.getErrors(), formatter));

    return summaryDto;
  }

  private List<SubscriptionDto> convertSubscriptions(final List<Subscription> subscriptions,
      final DateTimeFormatter formatter) {

    List<SubscriptionDto> subscriptionDtos = null;

    if (!CollectionUtils.isEmpty(subscriptions)) {
      subscriptionDtos = new ArrayList<>();

      for (Subscription subscription : subscriptions) {
        subscriptionDtos.add(convertSubscription(subscription, formatter));
      }
    }

    return subscriptionDtos;
  }

  private List<SubscriptionErrorDto> convertErrors(final List<SubscriptionError> errors,
      final DateTimeFormatter formatter) {

    List<SubscriptionErrorDto> errorDtos = null;

    if (!CollectionUtils.isEmpty(errors)) {
      errorDtos = new ArrayList<>();

      for (SubscriptionError error : errors) {
        SubscriptionErrorDto errorDto = new SubscriptionErrorDto();
        errorDto.setMessage(error.getMessage());

        if (error.getSubscription() != null) {
          errorDto.setSubscription(convertSubscription(error.getSubscription(), formatter));
        }

        if (error.getErrorCodes() != null) {
          errorDto.setCodes(convertErrorCodes(error.getErrorCodes()));
        }

        errorDtos.add(errorDto);
      }
    }

    return errorDtos;
  }

  private SubscriptionDto convertSubscription(final Subscription subscription, final DateTimeFormatter formatter) {
    SubscriptionDto subscriptionDto = new SubscriptionDto();
    subscriptionDto.setId(subscription.getId());
    subscriptionDto.setLastSubscribed(formatter.convert(subscription.getLastSubscribed()));

    if (subscription.getUser() != null) {
      subscriptionDto.setUser(convertUser(subscription.getUser()));
    }

    if (subscription.getReferral() != null) {
      subscriptionDto.setReferral(subscription.getReferral().name());
    }

    if (subscription.getContext() != null) {
      subscriptionDto.setContext(convertSubscriptionContext(subscription.getContext()));
    }

    return subscriptionDto;
  }

  private SubscriptionContextDto convertSubscriptionContext(final SubscriptionContext context) {
    SubscriptionContextDto contextDto = new SubscriptionContextDto();
    contextDto.setLeadType(context.getLeadType().getType());
    contextDto.setLeadContext(context.getLeadContext());
    contextDto.setEngagementScore(context.getEngagementScore());
    return contextDto;
  }

  private UserDto convertUser(final User user) {
    UserDto userDto = new UserDto();
    userDto.setId(user.getId());
    userDto.setEmail(user.getEmail());
    return userDto;
  }

  private List<String> convertErrorCodes(List<SubscriptionErrorCode> subscriptionErrorCodes) {
    List<String> errorCodes = null;

    if (!CollectionUtils.isEmpty(subscriptionErrorCodes)) {
      errorCodes = new ArrayList<>();

      for (SubscriptionErrorCode code : subscriptionErrorCodes) {
        errorCodes.add(code.getCode());
      }
    }
    return errorCodes;
  }

}
