/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ApiRequestParam.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * Names of HTTP request parameters which are commonly used, and have the same meaning, across more than one channel
 * service API.
 * <p>
 * These are declared as string constants rather than enums, primarily so they can be used in 3rd party annotations that
 * require a compile-time constant.
 */
public final class ApiRequestParam {

  /**
   * The date time format to be returned in the time-related nodes of the response. This parameter triggers the usage of
   * {@link com.brighttalk.common.web.annotation.DateTimeFormat} annotation
   */
  public static final String DATE_TIME_FORMAT = "dateTimeFormat";

  /** The number of requested page to be returned in a paged API. */
  public static final String PAGE_NUMBER = "page";

  /** The maximum number of results (resources) to return in response to a paged API. */
  public static final String PAGE_SIZE = "pageSize";

  /** The sort type value that needs to be applied when searching for requested resources. */
  public static final String SORT_BY = "sortBy";

  /** The sort order that needs to be applied when searching for requested resources. */
  public static final String SORT_ORDER = "sortOrder";

  /** A filter option indicating webcast status(es) that is(are) being requested. Default, camelcase notation. */
  public static final String WEBCAST_STATUS = "webcastStatus";

  /** A filter flag indicating webcast registration status. */
  public static final String REGISTERED = "registered";

  /** A filter indicating comma separated list of ids. */
  public static final String IDS = "ids";

  /** A filter flag indicating whether expanding need to be applied response. */
  public static final String EXPAND = "expand";

  /** The pin number of the communication */
  @NotNull
  @Pattern(regexp = "(^[0-9]{8}$)", message = "error.invalid.pin.invalid.values")
  public static final String PIN_NUMBER = "pin";

  /** The provider of the webcast . */
  public static final String PROVIDER = "provider";

  /**
   * Class cannot be instantiated and is static only.
   */
  private ApiRequestParam() {
  }
}