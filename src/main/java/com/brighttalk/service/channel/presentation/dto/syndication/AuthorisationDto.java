/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastProviderDto.java 72961 2014-01-24 15:16:01Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.syndication;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Authorisation DTO used for syndication.
 */
public class AuthorisationDto {

  /** Authorisation type. */
  private String type;

  /** Date of the authorisation. */
  private String date;

  /** Status of the authorisation. */
  private String status;

  /**
   * Default constructor.
   */
  public AuthorisationDto() {
  }

  /**
   * Constructor Authorisation DTO with status and approval date.
   * 
   * @param status The Authorisation status;
   * @param date The Authorisation Date.
   */
  public AuthorisationDto(String status, String date) {
    this.status = status;
    this.date = date;
  }

  /**
   * @return the date
   */
  public String getDate() {
    return date;
  }

  /**
   * @param date the date to set
   */
  public void setDate(final String date) {
    this.date = date;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(final String status) {
    this.status = status;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this);
    builder.append("type", type);
    builder.append("status", status);
    builder.append("date", date);
    return builder.toString();
  }
}