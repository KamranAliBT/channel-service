/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: package-info.java 70203 2013-10-24 18:01:03Z ssarraj $
 * ****************************************************************************
 */
/**
 * Package contains all the custom bean validation related classes.
 */
package com.brighttalk.service.channel.presentation.dto.validation.constraint;