/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastsPublicConverter.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.utils.HrefBuilderUtil;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.WebcastPublicDto;
import com.brighttalk.service.channel.presentation.dto.WebcastsPublicDto;

/**
 * Converter - Responsible for converting between {@link WebcastPublicDto} and paginated webcasts details.
 */
@Component
public class WebcastsPublicConverter {

  @Autowired
  private WebcastPublicConverter webcastPublicConverter;

  /**
   * Convert the given list of communications into WebcastsPublicDto wrapper. 
   * 
   * Depending on the details of the paginated communications the returned dto might also include navigation links.
   * 
   * @param communications paginated collection of communications
   * @param context converter context
   * 
   * @return dto
   */
  public WebcastsPublicDto convert(final PaginatedList<Communication> communications, final ConverterContext context) {

    if (CollectionUtils.isEmpty(communications)) {

      return new WebcastsPublicDto();
    }

    List<WebcastPublicDto> webcastDtos = new ArrayList<WebcastPublicDto>();

    for (Communication communication : communications) {

      WebcastPublicDto webcastPublicDto = webcastPublicConverter.convert(communication, context.getDateTimeFormatter());
      webcastDtos.add(webcastPublicDto);
    }

    WebcastsPublicDto publicDto = new WebcastsPublicDto();
    publicDto.setWebcasts(webcastDtos);

    if (includeLinks(communications)) {

      List<LinkDto> links = new ArrayList<LinkDto>();

      if (communications.hasPreviousPage()) {
        links.add(addPreviousPageLink(communications, context));
      }

      if (communications.hasNextPage()) {
        links.add(addNextPageLink(communications, context));
      }

      publicDto.setLinks(links);
    }

    return publicDto;
  }

  /**
   * Convert the given list of communications into WebcastsPublicDto wrapper. 
   * 
   * Depending on the details of the paginated communications the returned dto might also include navigation links.
   * 
   * @param communications paginated collection of communications
   * @param context converter context
   * 
   * @return dto
   */
  public WebcastsPublicDto convertPublic(final PaginatedList<Communication> communications,
    final ConverterContext context) {

    List<WebcastPublicDto> webcastDtos = new ArrayList<WebcastPublicDto>();

    for (Communication communication : communications) {

      WebcastPublicDto webcastPublicDto = webcastPublicConverter.convertPublic(communication,
          context.getDateTimeFormatter());
      webcastDtos.add(webcastPublicDto);
    }

    WebcastsPublicDto publicDto = new WebcastsPublicDto();
    publicDto.setWebcasts(webcastDtos);

    if (includeLinks(communications)) {

      List<LinkDto> links = new ArrayList<LinkDto>();

      if (communications.hasPreviousPage()) {
        links.add(addPreviousPageLink(communications, context));
      }

      if (communications.hasNextPage()) {
        links.add(addNextPageLink(communications, context));
      }

      publicDto.setLinks(links);
    }

    return publicDto;
  }

  private boolean includeLinks(final PaginatedList<Communication> communications) {

    return communications.hasNextPage() || communications.hasPreviousPage();
  }

  private LinkDto addPreviousPageLink(final PaginatedList<Communication> communications,
    final ConverterContext converterContext) {

    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(communications.getCurrentPageNumber() - 1));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(communications.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_PREVIOUS);

    return linkDto;
  }

  private LinkDto addNextPageLink(final PaginatedList<Communication> communications,
    final ConverterContext converterContext) {

    Map<String, String> queryParams = new HashMap<String, String>();
    queryParams.put(ApiRequestParam.PAGE_NUMBER, String.valueOf(communications.getCurrentPageNumber() + 1));
    queryParams.put(ApiRequestParam.PAGE_SIZE, String.valueOf(communications.getPageSize()));

    final String href = HrefBuilderUtil.build(converterContext.getRequestUrl(),
        converterContext.getAllowedQueryParamsFilter(), queryParams);

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(href);
    linkDto.setRel(LinkDto.RELATIONSHIP_NEXT);

    return linkDto;
  }
}