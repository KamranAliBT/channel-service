/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerController.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.user.User;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.error.ChannelManagerNotFoundException;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.error.InvalidChannelManagerException;
import com.brighttalk.service.channel.business.service.channel.ChannelManagerService;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelManagerConverter;
import com.brighttalk.service.channel.presentation.dto.validator.ChannelManagerUpdateValidator;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;
import com.brighttalk.service.channel.presentation.error.ResourceNotFoundException;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform CRUD operations on a channel manager.
 */
@Controller
@RequestMapping(value = "/channel/{channelId}/channel_manager", produces = "application/json")
public class ChannelManagerController {

  private static final Object CHANNEL_MANAGERS_FEATURE = "channel_managers";

  @Autowired
  private ChannelManagerService channelManagerService;

  @Autowired
  private ChannelManagerConverter converter;

  /**
   * Handles a request to update the public information of a channel manager.
   * 
   * @param user channel owner
   * @param channelId The Id of the channel to be retrieved.
   * @param channelManagerId The Id of the channel manager to be updated.
   * @param channelManagerDto channel manager
   * 
   * @return public channel manager
   */
  @RequestMapping(value = "/{channelManagerId}", method = RequestMethod.PUT, consumes = "application/json")
  @ResponseBody
  public ChannelManagerPublicDto update(@AuthenticatedUser final User user, @PathVariable final Long channelId,
      @PathVariable final Long channelManagerId, @RequestBody final ChannelManagerDto channelManagerDto) {

    ChannelManager updatedChannelManager = null;

    try {

      channelManagerDto.validate(new ChannelManagerUpdateValidator(channelManagerId));

      ChannelManager channelManager = converter.convert(channelManagerDto);

      updatedChannelManager = channelManagerService.update(user, channelId, channelManager);

    } catch (InvalidChannelManagerException e) {
      throw new BadRequestException(e.getMessage(), ErrorCode.INVALID_CHANNEL_MANAGER);
    } catch (ChannelNotFoundException e) {
      throw new ResourceNotFoundException(e.getMessage(), ErrorCode.CHANNEL_NOT_FOUND, e.getChannelId());
    } catch (ChannelManagerNotFoundException e) {
      throw new ResourceNotFoundException(e.getMessage(), ErrorCode.CHANNEL_MANAGER_NOT_FOUND, e.getChannelManagerId());
    } catch (FeatureNotEnabledException e) {
      throw new NotAuthorisedException(e.getMessage(), ErrorCode.FEATURE_DISABLED, CHANNEL_MANAGERS_FEATURE);
    }

    ChannelManagerPublicDto channelManagerPublicDto = converter.convertForPublic(updatedChannelManager);

    return channelManagerPublicDto;
  }

  /**
   * Handles a request to delete the public information of a channel manager.
   * 
   * @param user channel owner
   * @param channelId The Id of the channel to be retrieved.
   * @param channelManagerId The Id of the channel manager to be updated.
   * 
   */
  @RequestMapping(value = "/{channelManagerId}", method = RequestMethod.DELETE)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  @ResponseBody
  public void delete(@AuthenticatedUser final User user, @PathVariable final Long channelId,
      @PathVariable final Long channelManagerId) {

    try {

      channelManagerService.delete(user, channelId, channelManagerId);

    } catch (ChannelNotFoundException e) {
      throw new ResourceNotFoundException(e.getMessage(), ErrorCode.CHANNEL_NOT_FOUND, e.getChannelId());
    } catch (ChannelManagerNotFoundException e) {
      throw new ResourceNotFoundException(e.getMessage(), ErrorCode.CHANNEL_MANAGER_NOT_FOUND, e.getChannelManagerId());
    } catch (FeatureNotEnabledException e) {
      throw new NotAuthorisedException(e.getMessage(), ErrorCode.FEATURE_DISABLED, CHANNEL_MANAGERS_FEATURE);
    }

  }

}
