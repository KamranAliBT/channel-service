/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ErrorCode.java 101545 2015-10-16 11:26:42Z kali $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.error;

/**
 * Defines the set of error codes which the application can return in response to an erroneous or failed request.
 * <p>
 * An error code is a unique string which describes the type of error, written in camel-case format, e.g.
 * AuthenticationFailed. (Strings are used instead of numbers to aid human interpretation / readability).
 * <p>
 * An error code is used to communicate the cause of an error back to (API) clients, so they can react appropriately. It
 * can also potentially be used to drive the runtime lookup of a separate 'display' message suitable for end users,
 * whether they be API clients or humans accessing the application via a UI.
 */
public enum ErrorCode {

  // Please keep these enum values in alphabetical order.

  /**
   * The user ID or email is missing for one or more subscription.
   */
  BLANK_USER_EMAIL("BlankUserEmail"),

  /**
   * The given manager was not found.
   */
  CHANNEL_MANAGER_NOT_FOUND("ChannelManagerNotFound"),

  /**
   * The given channel was not found.
   */
  CHANNEL_NOT_FOUND("ChannelNotFound"),

  /**
   * The given manager was not found.
   */
  CHANNEL_TITLE_IN_USE("ChannelTitleInUse"),

  /**
   * The given user is not authorised to update the channel.
   */
  CHANNEL_UPDATE_AUTHORISATION("ChannelUpdateAuthorisation"),

  /**
   * The given user was not found.
   */
  CHANNEL_USER_NOT_FOUND("ChannelUserNotFound"),

  /** Indicates the communication does not identify an existing communication. */
  COMMUNICATION_NOT_FOUND("CommunicationNotFound"),

  /**
   * The given contract period is ineligible for deletion
   */
  CONTRACT_PERIOD_INELIGIBLE_FOR_DELETION ("ContractPeriodIneligibleForDeletion"),
  /**
   * The given contract period was not found.
   */
  CONTRACT_PERIOD_NOT_FOUND("ContractPeriodNotFound"),

  /** The given subscriptions has duplicate resource. */
  DUPLICATE_RESOURCE("DuplicateResource"),

  /**
   * The specific feature is disabled.
   */
  FEATURE_DISABLED("FeatureDisabled"),

  /** The supplied webcast has invalid channel. */
  INVALID_CHANNEL("InvalidChannel"),

  /** The supplied channel has invalid channel id. */
  INVALID_CHANNEL_ID("InvalidChannelId"),

  /**
   * The given channel manager is invalid.
   */
  INVALID_CHANNEL_MANAGER("InvalidChannelManager"),

  /**
   * The given contract period ID is invalid.
   */
  INVALID_CONTRACT_PERIOD_ID("InvalidContractPeriodId"),

  /**
   * The given contract period is invalid.
   */
  INVALID_CONTRACT_PERIOD("InvalidContractPeriod"),

  /**
   * The given contract period end date is invalid.
   */
  INVALID_CONTRACT_PERIOD_END("InvalidContractPeriodEnd"),

  /**
   * The given contract period start date is invalid.
   */
  INVALID_CONTRACT_PERIOD_START("InvalidContractPeriodStart"),

  /** The subscription context lead context engagement score is invalid integer. */
  INVALID_ENGAGEMENT_SCORE("InvalidEngagementScore"),

  /**
   * Invalid feature name being converted.
   */
  INVALID_FEATURE_NAME("InvalidFeatureName"),

  /** The subscription context lead context is invalid. */
  INVALID_LEAD_CONTEXT("InvalidLeadContext"),

  /** The subscription context lead type is invalid. */
  INVALID_LEAD_TYPE("InvalidLeadType"),

  /** The referral is invalid for one or more subscription. */
  INVALID_REFERRAL("InvalidReferral"),

  /** The subscription Id is invalid for one or more subscription. */
  INVALID_SUBSCRIPTION_ID("InvalidId"),

  /**
   * The user email is invalid for one or more subscription.
   */
  INVALID_USER_EMAIL("InvalidUserEmail"),

  /**
   * The user ID is invalid for one or more subscription.
   */
  INVALID_USER_ID("InvalidUserId"),

  /** The supplied webcast has invalid webcast id. */
  INVALID_WEBCAST_ID("InvalidWebcastId"),

  /** The supplied webcast has invalid webcast status. */
  INVALID_WEBCAST_STATUS("InvalidWebcastStatus"),

  /**
   * The given channel has reached the max numbers of channel managers.
   */
  MAX_CHANNEL_MANAGES_EXCEEDED("MaxChannelManagersExceeded"),

  /**
   * The given subscription missing subscription context.
   */
  MISSING_CONTEXT("MissingContext"),

  /**
   * The referral is missing for one or more subscription.
   */
  MISSING_REFERRAL("MissingReferral"),

  /** The subscription context lead context is missing. */
  MISSING_LEAD_CONTEXT("MissingLeadContext"),

  /** The subscription context lead engagement score is missing. */
  MISSING_ENGAGEMENT_SCORE("MissingEngagementScore"),

  /** The subscription context lead type is missing. */
  MISSING_LEAD_TYPE("MissingLeadType"),

  /**
   * The request body does not contain subscription element.
   */
  MISSING_SUBSCRIPTION("MissingSubscription"),

  /** The subscription Id is missing for one or more subscriptions. */
  MISSING_SUBSCRIPTION_ID("MissingId"),

  /**
   * The user ID or email is missing for one or more subscription.
   */
  MISSING_USER("MissingUser"),

  /**
   * The supplied resource does not identify any existing resource. The error code is generic since the portal only
   * expect "NotFound" error code for any non-existing resource.
   */
  NOT_FOUND("NotFound"),

  /**
   * The client credentials are not valid.
   */
  NOT_AUTHENTICATED("NotAuthenticated"),

  /**
   * This contract period overlaps another contract period on the same channel
   */
  OVERLAPPING_CONTRACT_PERIOD("OverlappingContractPeriod"),

  /**
   * The supplied resource does not identify any existing resource. The error code is generic since the portal only
   * expect "ResourceNotFound" error code for any non-existing resource.
   */
  RESOURCE_NOT_FOUND("ResourceNotFound"),

  /**
   * Channel search criteria.
   */
  SEARCH_CRITERIA("SearchCriteria"),

  /**
   * The request body contains more than 100 subscriptions.
   */
  SUBSCRIPTION_MAX_LIMIT("MaxSubscriptionRequest"),

  /**
   * The user ID and email can not be combined in a single request.
   */
  SUBSCRIPTION_USER_ID_EMAIL_COMBINATION("SubscriptionUserIdEmailCombination"),

  /** Indicates the summit does not identify an existing communication. */
  SUMMIT_NOT_FOUND("SummitNotFound"),

  /**
   * The given manager was not found.
   */
  USER_NOT_FOUND("UserNotFound"),

  /**
   * The API user is not authorised to access a requested resource. The error code is generic since the portal only
   * expect "NotAuthorised" error code for any not authorised request.
   */
  USER_NOT_AUTHORISED("UserNotAuthorised"),

  /**
   * Generic Not Authorised error code, expected in portal for any not authorised request.
   */
  NOT_AUTHORISED("NotAuthorised");

  private String errorCode;

  private ErrorCode(final String errorCode) {
    this.errorCode = errorCode;
  }

  public String getName() {
    return errorCode;
  }

}
