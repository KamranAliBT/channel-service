/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EnclosureDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.rss;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * EnclosureDto.
 */
public class EnclosureDto {

  private String url;
  
  private String type;
  
  private int length;

  public String getUrl() {
    return url;
  }

  @XmlAttribute
  public void setUrl(String url) {
    this.url = url;
  }

  public String getType() {
    return type;
  }

  @XmlAttribute
  public void setType(String type) {
    this.type = type;
  }

  public int getLength() {
    return length;
  }

  @XmlAttribute
  public void setLength(int length) {
    this.length = length;
  }
}