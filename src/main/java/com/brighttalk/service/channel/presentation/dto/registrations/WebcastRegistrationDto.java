/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastRegistrationDto.java 69149 2013-10-02 09:17:11Z build $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.registrations;

import javax.xml.bind.annotation.XmlAttribute;

import com.brighttalk.service.channel.presentation.dto.WebcastDto;

/**
 * WebcastRegistrationDto. Individual webcast registration details.
 */
public class WebcastRegistrationDto {

  private Long id;

  private ChannelDto channel;

  private WebcastDto webcast;

  private String registered;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(final Long id) {
    this.id = id;
  }

  public ChannelDto getChannel() {
    return channel;
  }

  public void setChannel(final ChannelDto channel) {
    this.channel = channel;
  }

  public WebcastDto getWebcast() {
    return webcast;
  }

  public void setWebcast(final WebcastDto webcast) {
    this.webcast = webcast;
  }

  public String getRegistered() {
    return registered;
  }

  public void setRegistered(final String registered) {
    this.registered = registered;
  }
}