/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ErrorBuilder.java 57951 2012-11-26 16:48:57Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.builder;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.presentation.dto.ErrorDto;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

/**
 * Error Builder Class.
 */
@Component
public class ErrorBuilder {

  /**
   * Build error dto.
   * 
   * @param errorCode presentation error code
   * @param index element position
   * 
   * @return error dto
   */
  public ErrorDto build(final ErrorCode errorCode, final Integer index) {

    ErrorDto errorDto = new ErrorDto();
    errorDto.setCode(errorCode.getName());
    errorDto.setResourceIndex(index);

    return errorDto;
  }

}