/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SubscriptionController.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.user.User;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.service.channel.business.service.channel.ChannelService;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform CRUD operations on user subscriptions.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 */
@Controller
@RequestMapping(value = "/channel/{channelId}/my/subscription")
public class SubscriptionController {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(SubscriptionController.class);

  /**
   * Channel service
   */
  @Autowired
  private ChannelService channelService;

  /**
   * Handles a request to unsubscribe user from the given channel.
   * 
   * @param user authenticated user
   * @param channelId of a channel to unsubscribe the user from
   */
  @RequestMapping(method = RequestMethod.DELETE)
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void delete(@AuthenticatedUser User user, @PathVariable Long channelId) {

    channelService.unsubscribeUserFromChannel(user, channelId);
  }

}
