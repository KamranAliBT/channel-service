/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AtomFeedDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.atom;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.brighttalk.service.channel.presentation.dto.LinkDto;

/**
 * Atom Feed Dto. 
 * 
 * Xml Root Element specifies what the root name is. The root, default namespace definition
 * is declared not here, but in the {@link AtomNamespacePrefixMapper} instance.
 * 
 * XmlType is used to sort properties for display purposes only, 
 * the response would be valid (alphabetical) without it too. 
 */
@XmlRootElement( name = "feed" )
@XmlType( propOrder = { "id", "updated", "title", "subtitle", "description", "links", "entries" } )
public class AtomFeedDto {

  /**
   * The tag used to generated channel and communication ids.
   */
  public static final String BRIGHTTALK_TAG = "tag:brighttalk.com,";
  
  
  private String title;
  
  private String id;
  
  private String description;
  
  private String updated;
  
  private String subtitle;
  
  private List<LinkDto> links;
  
  private List<FeedEntryDto> entries;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setDescription(String description) {
    this.description = description;
  }

  public String getUpdated() {
    return updated;
  }

  public void setUpdated(String updated) {
    this.updated = updated;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  @XmlElement(name = "link")
  public void setLinks(List<LinkDto> links) {
    this.links = links;
  }

  public List<FeedEntryDto> getEntries() {
    return entries;
  }

  @XmlElement(name = "entry")
  public void setEntries(List<FeedEntryDto> entries) {
    this.entries = entries;
  }
  
}