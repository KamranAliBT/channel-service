/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RenditionDto.java 86487 2014-11-21 15:15:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * DTO object representing a rendition.
 */
public class RenditionDto {

  private Long id;
  private String isActive;
  private String url;
  private String type;
  private String container;
  private String codecs;
  private Long width;
  private Long bitrate;

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * return true if id is set.
   * 
   * @return boolean true if set
   */
  public Boolean hasId() {
    return id != null;
  }

  /**
   * @return the isActive
   */
  public String getIsActive() {
    return isActive;
  }

  /**
   * @param isActive the isActive to set
   */
  @XmlAttribute
  public void setIsActive(final String isActive) {
    this.isActive = isActive;
  }

  /**
   * return true if isActive is set.
   * 
   * @return boolean true if set
   */
  public Boolean hasIsActive() {
    return isActive != null;
  }

  /**
   * @return the url
   */
  public String getUrl() {
    return url;
  }

  /**
   * @param url the url to set
   */
  @XmlAttribute
  public void setUrl(final String url) {
    this.url = url;
  }

  /**
   * return true if url is set.
   * 
   * @return boolean true if set
   */
  public Boolean hasUrl() {
    return url != null;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  @XmlAttribute
  public void setType(final String type) {
    this.type = type;
  }

  /**
   * return true if type is set.
   * 
   * @return boolean true if set
   */
  public Boolean hasType() {
    return type != null;
  }

  /**
   * @return the container
   */
  public String getContainer() {
    return container;
  }

  /**
   * @param container the container to set
   */
  @XmlAttribute
  public void setContainer(final String container) {
    this.container = container;
  }

  /**
   * return true if container is set.
   * 
   * @return boolean true if set
   */
  public Boolean hasContainer() {
    return container != null;
  }

  /**
   * @return the codecs
   */
  public String getCodecs() {
    return codecs;
  }

  /**
   * @param codecs the codecs to set
   */
  @XmlAttribute
  public void setCodecs(final String codecs) {
    this.codecs = codecs;
  }

  /**
   * return true if codec is set.
   * 
   * @return boolean true if set
   */
  public Boolean hasCodecs() {
    return codecs != null;
  }

  /**
   * @return the width
   */
  public Long getWidth() {
    return width;
  }

  /**
   * @param width the width to set
   */
  @XmlAttribute
  public void setWidth(final Long width) {
    this.width = width;
  }

  /**
   * return true if width is set.
   * 
   * @return boolean true if set
   */
  public Boolean hasWidth() {
    return width != null;
  }

  /**
   * @return the bitrate
   */
  public Long getBitrate() {
    return bitrate;
  }

  /**
   * @param bitrate the bitrate to set
   */
  @XmlAttribute
  public void setBitrate(final Long bitrate) {
    this.bitrate = bitrate;
  }

  /**
   * return true if bitrate is set.
   * 
   * @return boolean true if set
   */
  public Boolean hasBitrate() {
    return bitrate != null;
  }

}
