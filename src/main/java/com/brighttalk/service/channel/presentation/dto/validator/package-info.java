/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the validator classes to validate presentation layer objects.
 */
package com.brighttalk.service.channel.presentation.dto.validator;