/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelFeedSearchCriteriaBuilder.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;

/**
 * Builder for instances of {@link ChannelFeedSearchCriteria}.
 */
@Component
public class ChannelFeedSearchCriteriaBuilder {

  @Value("${feed.defaultPageSize}")
  private int defaultPageSize;

  @Value("${feed.maxPageSize}")
  private int defaultMaxPageSize;

  @Value("${feed.defaultPageNumber}")
  private int defaultPageNumber;

  /**
   * Builds an instance of {@link ChannelFeedSearchCriteria}.
   * 
   * @return An instance of {@link ChannelFeedSearchCriteria}.
   */
  public ChannelFeedSearchCriteria build() {
    return new ChannelFeedSearchCriteria();
  }

  /**
   * Builds an instance of {@link ChannelFeedSearchCriteria}.
   * 
   * @param pageNumber Page number criteria.
   * @param pageSize Page size criteria.
   * @param filterType Filter type criteria.
   * @param statusFilterAsString Comma separated list of {@link CommunicationStatusFilter} that determines what webcasts
   * to include that matches this status.
   * 
   * @return An instance of {@link ChannelFeedSearchCriteria}.
   */
  public ChannelFeedSearchCriteria build(final Integer pageNumber, final Integer pageSize, final String filterType,
      final String statusFilterAsString) {

    ChannelFeedSearchCriteria channelFeedSearchCriteria = new ChannelFeedSearchCriteria(pageNumber, pageSize,
        filterType);

    List<CommunicationStatusFilter> statusFilter = convertStatusFilterStringToList(statusFilterAsString);

    channelFeedSearchCriteria.setCommunicationStatusFilter(statusFilter);

    return channelFeedSearchCriteria;
  }

  /**
   * Builds an instance of {@link ChannelFeedSearchCriteria} with values initialised from the supplied instance of
   * {@link ChannelFeedSearchCriteria}.
   * 
   * @param channelFeedSearchCriteria Instance to initialise from.
   * 
   * @return An instance of {@link ChannelFeedSearchCriteria}.
   */
  public ChannelFeedSearchCriteria build(ChannelFeedSearchCriteria channelFeedSearchCriteria) {

    if (channelFeedSearchCriteria == null) {
      throw new IllegalStateException("ChannelFeedSearchCriteria argument cannot be null");
    }

    ChannelFeedSearchCriteria channelFeedSearchCriteriaCopy = new ChannelFeedSearchCriteria();

    channelFeedSearchCriteriaCopy.setPageNumber(channelFeedSearchCriteria.getPageNumber());
    channelFeedSearchCriteriaCopy.setPageSize(channelFeedSearchCriteria.getPageSize());
    channelFeedSearchCriteriaCopy.setFilterType(channelFeedSearchCriteria.getFilterType());
    channelFeedSearchCriteriaCopy.setCommunicationStatusFilter(channelFeedSearchCriteria.getCommunicationStatusFilter());

    return channelFeedSearchCriteriaCopy;
  }

  /**
   * Build an instance of {@link ChannelFeedSearchCriteria} where null values are defaulted.
   * 
   * @param pageNumber Page number criteria.
   * @param pageSize Page size criteria.
   * @param filterType Filter type criteria.
   * @param statusFilterAsString Comma separated list of {@link CommunicationStatusFilter} that determines what webcasts
   * to include that matches this status.
   * 
   * @return An instance of {@link ChannelFeedSearchCriteria}.
   */
  public ChannelFeedSearchCriteria buildWithDefaults(final Integer pageNumber, final Integer pageSize,
      final String filterType, final String statusFilterAsString) {

    ChannelFeedSearchCriteria channelFeedSearchCriteria = build(pageNumber, pageSize, filterType, statusFilterAsString);

    setDefaultValues(channelFeedSearchCriteria);

    return channelFeedSearchCriteria;
  }

  /**
   * Build an instance of {@link ChannelFeedSearchCriteria} with default values.
   * 
   * @return An instance of {@link ChannelFeedSearchCriteria}.
   */
  public ChannelFeedSearchCriteria buildWithDefaults() {

    ChannelFeedSearchCriteria channelFeedSearchCriteria = build();

    setDefaultValues(channelFeedSearchCriteria);

    return channelFeedSearchCriteria;
  }

  private void setDefaultValues(final ChannelFeedSearchCriteria channelFeedSearchCriteria) {
    if (channelFeedSearchCriteria.getPageNumber() == null) {
      channelFeedSearchCriteria.setPageNumber(defaultPageNumber);
    }

    if (channelFeedSearchCriteria.getPageSize() == null) {
      channelFeedSearchCriteria.setPageSize(defaultPageSize);
    } else if (channelFeedSearchCriteria.getPageSize() > defaultMaxPageSize) {
      channelFeedSearchCriteria.setPageSize(defaultMaxPageSize);
    }
  }

  /**
   * Converts a comma separated list of status filters in to a list of {@link CommunicationStatusFilter}.
   * 
   * @param statusFilterAsString Comma separated list of status filters to convert in to enums.
   * 
   * @return List of {@link CommunicationStatusFilter} equivalents of the string.
   */
  private List<CommunicationStatusFilter> convertStatusFilterStringToList(final String statusFilterAsString) {

    List<CommunicationStatusFilter> statusFilter = null;

    if (!StringUtils.isEmpty(statusFilterAsString)) {

      statusFilter = new ArrayList<CommunicationStatusFilter>();

      for (String status : statusFilterAsString.split(",")) {

        statusFilter.add(CommunicationStatusFilter.convert(status));
      }
    }

    return statusFilter;
  }
}
