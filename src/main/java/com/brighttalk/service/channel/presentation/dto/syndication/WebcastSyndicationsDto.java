/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: WebcastSyndicationsDto.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.syndication;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.presentation.dto.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;

/**
 * Represent a Communication syndications resource DTO which provides the details of a communication syndication details
 * in a master channel and all the syndication details in consumer channels and this includes:
 * <ul>
 * <li>Master channel details (id, title and organisation).</li>
 * <li>The communication title in the master channel.</li>
 * <li>List of all the {@link WebcastSyndicationDto communication syndication} details in the distributed
 * channels.</li>
 */
public class WebcastSyndicationsDto {

  private ChannelDto channel;

  private WebcastDto communication;

  private List<WebcastSyndicationDto> syndications;

  /**
   * @return The Webcast syndications.
   */
  public List<WebcastSyndicationDto> getSyndications() {
    return syndications;
  }

  /**
   * @param syndications The Webcast syndications to set.
   */
  public void setSyndications(List<WebcastSyndicationDto> syndications) {
    this.syndications = syndications;
  }

  /**
   * @param syndication The add webcast syndication.
   */
  public void addSyndications(WebcastSyndicationDto syndication) {
    if (CollectionUtils.isEmpty(syndications)) {
      this.syndications = new ArrayList<>();
    }

    this.syndications.add(syndication);
  }

  /**
   * @return the channel
   */
  public ChannelDto getChannel() {
    return channel;
  }

  /**
   * @return the communication
   */
  public WebcastDto getCommunication() {
    return communication;
  }

  /**
   * @param channel the channel to set
   */
  public void setChannel(ChannelDto channel) {
    this.channel = channel;
  }

  /**
   * @param communication the communication to set
   */
  public void setCommunication(WebcastDto communication) {
    this.communication = communication;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this);
    builder.append("channel", channel);
    builder.append("communication", communication);
    builder.append("syndications", syndications);
    return builder.toString();
  }
}