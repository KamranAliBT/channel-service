/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: CommunicationsSearchCriteriaBuilder.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.presentation.ApiRequestParam;

/**
 * Builder Class to extract a {@link CommunicationsSearchCriteria} from a request
 * object.
 */
@Component
public class CommunicationsSearchCriteriaBuilder extends AbstractSearchCriteriaBuilder {

  /**
   * Build the public communication search criteria from request context. 
   * 
   * @param request the http request to convert to criteria
   * 
   * @return The built criteria
   */
  public CommunicationsSearchCriteria build(HttpServletRequest request) {

    Integer pageNumber = getIntegerFromRequest(request, ApiRequestParam.PAGE_NUMBER);
    Integer pageSize = getIntegerFromRequest(request, ApiRequestParam.PAGE_SIZE);
    String ids = request.getParameter(ApiRequestParam.IDS);
    String expand = request.getParameter(ApiRequestParam.EXPAND);

    final CommunicationsSearchCriteria communicationsSearchCriteria = new CommunicationsSearchCriteria();
    communicationsSearchCriteria.setIds(ids);
    communicationsSearchCriteria.setExpand(expand);
    communicationsSearchCriteria.setPageNumber(pageNumber);
    communicationsSearchCriteria.setPageSize(pageSize);

    return communicationsSearchCriteria;
  }
}