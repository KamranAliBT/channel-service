/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 57059 2012-11-06 17:22:01Z aaugustyn $
 * ****************************************************************************
 */
/**
 * Presentation layer util classes.
 */
package com.brighttalk.service.channel.presentation.util;