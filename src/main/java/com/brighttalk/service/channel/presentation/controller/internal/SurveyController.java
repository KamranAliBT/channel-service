/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SurveyController.java 83833 2014-09-24 09:50:49Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.common.form.converter.FormConverter;
import com.brighttalk.common.form.dto.FormDto;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.InvalidRequestingUserIdException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.error.SurveyNotFoundException;
import com.brighttalk.service.channel.business.service.AuthorisationService;
import com.brighttalk.service.channel.business.service.SurveyService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;

/**
 * The internal Survey controller handling internal call from other services.
 * 
 * It performs operations on {@link com.brighttalk.service.channel.business.domain.Survey survey}.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller
@RequestMapping(value = "/internal")
public class SurveyController {
  private static final Logger logger = Logger.getLogger(SurveyController.class);

  /**
   * Authorisation service
   */
  @Autowired
  private AuthorisationService authorisationService;

  /** Channel finder */
  @Autowired
  private ChannelFinder channelFinder;

  /**
   * Communication service
   */
  @Autowired
  private CommunicationFinder communicationFinder;

  /**
   * Survey service
   */
  @Autowired
  private SurveyService surveyService;

  /**
   * Form Convertr
   */
  @Autowired
  private FormConverter formConverter;

  /**
   * Handles a internal request to get survey by id.
   * 
   * Authorisation may be required depending on whether a user Id is provided as a header.
   * 
   * @param requestUserId the id of a user to be authorized
   * @param surveyId the survey id
   * 
   * @return survey form dto
   * @throws SurveyNotFoundException when survey retrieved from Survey Service doesn't exist.
   * @throws InvalidRequestingUserIdException when provided request user id doesn't have numeric value.
   * @throws com.brighttalk.service.channel.business.error.NotFoundException channel found for given survey doesn't
   * exist.
   */
  @RequestMapping(value = "/survey/{surveyId}", method = RequestMethod.GET)
  @ResponseBody
  public FormDto getSurvey(
      @RequestHeader(value = "X-BrightTALK-Requesting-UserId", required = false) final String requestUserId,
      @PathVariable final Long surveyId) {

    User user = convertUserId(requestUserId);

    // find survey for a channel survey
    Survey survey = surveyService.findSurvey(surveyId);

    // find the channel
    Channel channel = null;
    try {

      channel = this.channelFinder.find(survey.getChannelId());

    } catch (NotFoundException notFoundException) {

      throw new SurveyNotFoundException("No channel found for survey [" + surveyId + "].", notFoundException);
    }

    if (authorisationRequired(user)) {

      // Authorise
      authorisationService.channelByUserId(channel, user);
    }

    // Find survey
    survey = surveyService.loadSurvey(survey);

    // converts survey form to Dto
    FormDto formDto = formConverter.convert(survey.getForm());
    formDto.setIsActive(survey.isActive());

    if (logger.isDebugEnabled()) {
      logger.debug("Successfully returning Survey form [" + formDto + "].");
    }

    return formDto;

  }

  /**
   * Handles a internal request to get survey by channel id.
   * 
   * Authorisation may be required depending on whether a user Id is provided as a header.
   * 
   * @param requestUserId the id of a user to be authorized
   * @param channelId The id of the channel
   * 
   * @return The populated FormDto
   * @throws SurveyNotFoundException when provided channel doesn't have a survey. Or survey retrieved from Survey
   * Service doesn't exist.
   * @throws InvalidRequestingUserIdException when provided request user id doesn't have numeric value.
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when channel with given id doesn't exist.
   */
  @RequestMapping(value = "/channel/{channelId}/survey", method = RequestMethod.GET)
  @ResponseBody
  public FormDto getChannelSurvey(
      @RequestHeader(value = "X-BrightTALK-Requesting-UserId", required = false) final String requestUserId,
      @PathVariable final Long channelId) {

    User user = convertUserId(requestUserId);

    // find the channel
    Channel channel = this.channelFinder.find(channelId);

    if (authorisationRequired(user)) {
      authorisationService.channelByUserId(channel, user);
    }

    // Find survey
    Survey survey = channel.getSurvey();

    if (survey == null) {
      throw new SurveyNotFoundException("Survey for channel Id [" + channelId + "] could not be found");
    }

    survey = surveyService.loadSurvey(survey);

    // converts survey form to Dto
    FormDto formDto = formConverter.convert(survey.getForm());
    formDto.setIsActive(survey.isActive());

    if (logger.isDebugEnabled()) {
      logger.debug("Successfully returning Survey form [" + formDto + "].");
    }
    return formDto;
  }

  /**
   * Handles a internal request to get survey by Channel and Communication ids.
   * 
   * Authorisation may be required depending on whether a user Id is provided as a header.
   * 
   * @param requestUserId the id of a user to be authorized
   * @param channelId The id of the channel
   * @param communicationId The id of the communication
   * 
   * @return The populated FormDto
   * @throws SurveyNotFoundException when communication doesn't have a survey. Or survey retrieved from Survey Service
   * doesn't exist.
   * @throws InvalidRequestingUserIdException when provided request user id doesn't have numeric value.
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when channel or communication provided in
   * the request doesn't exist.
   */
  @RequestMapping(value = "/channel/{channelId}/communication/{communicationId}/survey", method = RequestMethod.GET)
  @ResponseBody
  public FormDto getCommunicationSurvey(
      @RequestHeader(value = "X-BrightTALK-Requesting-UserId", required = false) final String requestUserId,
      @PathVariable final Long channelId, @PathVariable final Long communicationId) {

    User user = convertUserId(requestUserId);

    // find the channel
    Channel channel = this.channelFinder.find(channelId);

    if (authorisationRequired(user)) {
      authorisationService.channelByUserId(channel, user);
    }

    Communication communication = communicationFinder.find(channel, communicationId);

    // Find survey
    Survey survey = communication.getSurvey();

    if (survey == null) {
      throw new SurveyNotFoundException("Survey for channel Id [" + channelId + "] could not be found");
    }

    survey = surveyService.loadSurvey(survey);

    // converts survey form to Dto
    FormDto formDto = formConverter.convert(survey.getForm());
    formDto.setIsActive(survey.isActive());

    if (logger.isDebugEnabled()) {
      logger.debug("Successfully returning Survey form [" + formDto + "].");
    }
    return formDto;
  }

  /**
   * Convert incoming user Id to a user object.
   * 
   * This validates that the user id is a valid format.
   * 
   * @param incomingUserId
   * 
   * @return {@link User}
   * @throws InvalidRequestingUserIdException when provided incomingUserId doesn' have numeric value.
   */
  private User convertUserId(final String incomingUserId) {
    if (incomingUserId == null) {
      return null;
    }

    Long userId;
    try {
      userId = Long.valueOf(incomingUserId);
    } catch (NumberFormatException e) {
      throw new InvalidRequestingUserIdException("Invalid User Id value given [" + incomingUserId + "]", e);
    }

    User user = new User(userId);

    return user;
  }

  /**
   * Do we need to do authorisation on this request.
   * 
   * We only authorise if a user was provided.
   * 
   * We could add some rules related to the service calling this api in the future.
   * 
   * @param user The given user
   * @return <code>TRUE<code> returned when authorisation is required.
   */
  private boolean authorisationRequired(final User user) {

    if (user != null) {
      return true;
    }
    return false;
  }

  /**
   * Set the Authorisation Service for this controller.
   * 
   * @param authorisationService The Authorisation service to use.
   */
  public void setAuthorisationService(final AuthorisationService authorisationService) {
    this.authorisationService = authorisationService;
  }

  /**
   * @param channelFinder the channelFinder to set
   */
  public void setChannelFinder(ChannelFinder channelFinder) {
    this.channelFinder = channelFinder;
  }

  /**
   * Set the Communication Service for this controller.
   * 
   * @param communicationFinder The Communication service to use.
   */
  public void setCommunicationFinder(final CommunicationFinder communicationFinder) {
    this.communicationFinder = communicationFinder;
  }

  /**
   * Sets the survey service of this controller.
   * 
   * @param surveyService the survey service to be set.
   */
  public void setSurveyService(final SurveyService surveyService) {
    this.surveyService = surveyService;
  }

  /**
   * Sets the form converter of this controller.
   * 
   * @param formConverter the form converter to be set.
   */
  public void setFormConverter(final FormConverter formConverter) {
    this.formConverter = formConverter;
  }
}