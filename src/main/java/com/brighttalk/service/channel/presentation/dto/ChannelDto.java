/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelDto.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;

import com.brighttalk.service.channel.presentation.dto.annotation.validator.ListFormat;
import com.brighttalk.service.channel.presentation.dto.annotation.validator.ListMaxLength;
import com.brighttalk.service.channel.presentation.dto.annotation.validator.ListNotBlank;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateChannel;

/**
 * ChannelDto. DTO object representing channel.
 */
@XmlRootElement(name = "channel")
public class ChannelDto {

  private static final int MAX_LENGTH_TITLE = 255;

  private static final int MAX_LENGTH_DESCRIPTION = 4000;

  private static final int MAX_LENGTH_ORGANISATION = 255;

  private static final int MAX_LENGTH_STRAPLINE = 255;

  private static final int MAX_LENGTH_KEYWORDS = 100;

  /**
   * Keywords regular expression, the supplied keywords will be rejected if it contain comma. This regular expression
   * used for the keywords version that represents channel keywords as a list of String keywords instead of comma
   * separated string.
   */
  private static final String KEYWORD_REGEX = ".*[,]+.*";

  @NotNull(groups = { UpdateChannel.class }, message = "{ChannelIdEmpty}")
  private Long id;

  @NotBlank(groups = { UpdateChannel.class }, message = "{ChannelTitleEmpty}")
  @Size(max = MAX_LENGTH_TITLE, groups = { UpdateChannel.class }, message = "{ChannelTitleMaxLength}")
  private String title;

  @NotBlank(groups = { UpdateChannel.class }, message = "{ChannelDescriptionEmpty}")
  @Size(max = MAX_LENGTH_DESCRIPTION, groups = { UpdateChannel.class }, message = "{ChannelDescriptionMaxLength}")
  private String description;

  @NotBlank(groups = { UpdateChannel.class }, message = "{ChannelOrganisationEmpty}")
  @Size(max = MAX_LENGTH_ORGANISATION, groups = { UpdateChannel.class }, message = "{ChannelOrganisationMaxLength}")
  private String organisation;

  /**
   * Comma Separated list of keywords. This is still needed to maintain backward compatibility, since it is still
   * required in Get Channel Details API.
   **/
  private String keywords;

  /**
   * List of keywords strings.
   */
  @ListNotBlank(groups = { UpdateChannel.class }, message = "{ChannelKeywordsEmpty}")
  @ListMaxLength(max = MAX_LENGTH_KEYWORDS, groups = { UpdateChannel.class }, message = "{ChannelKeywordsMaxLength}")
  @ListFormat(regex = KEYWORD_REGEX, groups = { UpdateChannel.class }, message = "{ChannelKeywordsFormat}")
  private List<String> keywordsList;

  @NotBlank(groups = { UpdateChannel.class }, message = "{ChannelStraplineEmpty}")
  @Size(max = MAX_LENGTH_STRAPLINE, groups = { UpdateChannel.class }, message = "{ChannelStraplineMaxLength}")
  private String strapline;

  private String url;

  private Boolean promoteable;

  private String searchVisibility;

  private Float rating;

  private String currentType;

  private String createdType;

  private String pendingType;

  private String created;

  private String lastUpdated;

  private StatisticsDto statistics;

  private UserDto user;

  private SurveyDto survey;

  private List<FeatureDto> features;

  private List<CategoryDto> categories;

  /**
   * Creates new ChannelDto with provided id.
   * 
   * @param id of a channel
   */
  public ChannelDto(final Long id) {
    this.id = id;
  }

  /**
   * Constructs channel Dto object as a new object.
   */
  public ChannelDto() {
  }

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(final Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getOrganisation() {
    return organisation;
  }

  public void setOrganisation(final String organisation) {
    this.organisation = organisation;
  }

  public String getStrapline() {
    return strapline;
  }

  public void setStrapline(final String strapline) {
    this.strapline = strapline;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public Boolean getPromoteable() {
    return promoteable;
  }

  public void setPromoteable(final Boolean promoteable) {
    this.promoteable = promoteable;
  }

  public String getSearchVisibility() {
    return searchVisibility;
  }

  public void setSearchVisibility(final String searchVisibility) {
    this.searchVisibility = searchVisibility;
  }

  public Float getRating() {
    return rating;
  }

  public void setRating(final Float rating) {
    this.rating = rating;
  }

  public String getCurrentType() {
    return currentType;
  }

  public void setCurrentType(final String currentType) {
    this.currentType = currentType;
  }

  public String getCreatedType() {
    return createdType;
  }

  public void setCreatedType(final String createdType) {
    this.createdType = createdType;
  }

  public String getPendingType() {
    return pendingType;
  }

  public void setPendingType(final String pendingType) {
    this.pendingType = pendingType;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(final String created) {
    this.created = created;
  }

  public String getLastUpdated() {
    return lastUpdated;
  }

  public void setLastUpdated(final String lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public StatisticsDto getStatistics() {
    return statistics;
  }

  public void setStatistics(final StatisticsDto statistics) {
    this.statistics = statistics;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(final UserDto user) {
    this.user = user;
  }

  public SurveyDto getSurvey() {
    return survey;
  }

  public void setSurvey(final SurveyDto survey) {
    this.survey = survey;
  }

  public List<FeatureDto> getFeatures() {
    return features;
  }

  @XmlElementWrapper(name = "features")
  @XmlElement(name = "feature")
  public void setFeatures(final List<FeatureDto> features) {
    this.features = features;
  }

  public List<CategoryDto> getCategories() {
    return categories;
  }

  @XmlElementWrapper(name = "categories")
  @XmlElement(name = "category")
  public void setCategories(final List<CategoryDto> categories) {
    this.categories = categories;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(final String keywords) {
    this.keywords = keywords;
  }

  public List<String> getKeywordsList() {
    return keywordsList;
  }

  @XmlElementWrapper(name = "keywordsList")
  @XmlElement(name = "keyword")
  public void setKeywordsList(final List<String> keywordsList) {
    this.keywordsList = keywordsList;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this);
    builder.append("id", id);
    builder.append("title", title);
    builder.append("description", description);
    builder.append("organisation", organisation);
    return builder.toString();
  }

}
