/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsConverter.java 91908 2015-03-18 16:46:30Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.Subscription.Referral;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.presentation.dto.SubscriptionContextDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;

/**
 * Converter - Responsible for converting between SubscriptionsDto and business types.
 */
@Component
public class SubscriptionsConverter {

  /**
   * Converts a list of subscription DTOs to a list of subscriptions.
   * 
   * @param subscriptionDtos the list to convert from
   * 
   * @return {@link Subscription subscriptions}
   */
  public List<Subscription> convert(final List<SubscriptionDto> subscriptionDtos) {

    List<Subscription> subscriptions = new ArrayList<>();

    if (CollectionUtils.isEmpty(subscriptionDtos)) {
      return Collections.emptyList();
    }

    for (SubscriptionDto subscriptionDto : subscriptionDtos) {
      subscriptions.add(convert(subscriptionDto));
    }

    return subscriptions;
  }

  /**
   * Converts the supplied {@link SubscriptionDto} to {@link Subscription}.
   * 
   * @param subscriptionDto The subscription DTO to convert.
   * @return The converted {@link Subscription}.
   */
  private Subscription convert(final SubscriptionDto subscriptionDto) {

    Subscription subscription = new Subscription();
    subscription.setId(subscriptionDto.getId());
    subscription.setReferral(Referral.valueOf(subscriptionDto.getReferral()));

    if (subscriptionDto.getUser() != null) {
      subscription.setUser(convertUser(subscriptionDto.getUser()));
    }

    SubscriptionContextDto contextDto = subscriptionDto.getContext();
    if (contextDto != null) {
      subscription.setContext(new SubscriptionContext(SubscriptionLeadType.getType(contextDto.getLeadType()),
          contextDto.getLeadContext(), contextDto.getEngagementScore()));
    }

    return subscription;
  }

  /**
   * Converts the supplied {@link UserDto} to {@link User}.
   * 
   * @param userDto The user DTO to convert.
   * @return The converted {@link User}.
   */
  private User convertUser(final UserDto userDto) {

    User user = new User();
    user.setId(userDto.getId());
    user.setEmail(userDto.getEmail());

    return user;
  }
}
