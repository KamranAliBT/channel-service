/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeedController.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.PaginatedChannelFeed;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;
import com.brighttalk.service.channel.business.service.channel.ChannelFeedService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.presentation.dto.atom.AtomFeedDto;
import com.brighttalk.service.channel.presentation.dto.converter.AtomFeedConverter;
import com.brighttalk.service.channel.presentation.dto.converter.RssFeedConverter;
import com.brighttalk.service.channel.presentation.dto.rss.RssFeedDto;
import com.brighttalk.service.channel.presentation.util.ChannelFeedSearchCriteriaBuilder;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls requesting channel feed representations.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 */
@Controller
public class FeedController {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(FeedController.class);

  @Autowired
  private AtomFeedConverter atomFeedConverter;

  @Autowired
  private RssFeedConverter rssFeedConverter;

  /** Channel finder */
  @Autowired
  private ChannelFinder channelFinder;

  @Autowired
  private ChannelFeedService channelFeedService;

  @Autowired
  private ChannelFeedSearchCriteriaBuilder channelFeedSearchCriteriaBuilder;

  /**
   * Get a paginated Atom representation of channel feed.
   * 
   * Feed is ordered by most recent first. So page 1 will contain the most recent webcasts.
   * <p>
   * URL is <code>/{channelId}/feed/?page={page number}&size={page size}&filter=closestToNow</code>
   * <p>
   * <ul>
   * <li>page - the number of the page to get. Optional. Defaults to 1. Not relevant if the filter param is supplied</li>
   * <li>size - the number of entries for the page. Optional. Defaults to 1000.</li>
   * <li>filter - indicate we want the entries closest to now. Optional. Not relevant if the page param is supplied</li>
   * </ul>
   * 
   * Note: Checkstyle exemptions due to rule failure around '}' - says there should be whitspace before '}'
   * 
   * @param channelId The Id of the channel to be retrieved.
   * @param pageNumber the page number of the feed to retrieve (optional)
   * @param pageSize the size of the page to retrieve (optional)
   * @param filter the filter to apply (only closestToNow is supported (optional)
   * @param communicationStatusFilter Webcasts to include that match this status.
   * 
   * @return {@link AtomFeedDto}. In case of any error scenario, such as the channel doesn't exists or given id is not
   * of type Long - an empty feed is returned. The error is swallowed. This prevents from DDoS attacks/guessing which
   * channel id exists.
   */
  @RequestMapping(value = { "/channel/{channelId}/feed", "/xml/feed/{channelId}" }, produces = {
      "application/atom+xml; charset=utf-8", "application/json; charset=utf-8" })
  @ResponseBody
  public AtomFeedDto getAtom(@PathVariable final String channelId,
      @RequestParam(value = "page", required = false) final Integer pageNumber,
      @RequestParam(value = "size", required = false) final Integer pageSize,
      @RequestParam(value = "filter", required = false) final String filter,
      @RequestParam(value = "webcastStatus", required = false) final String communicationStatusFilter) {

    PaginatedChannelFeed feed;

    try {
      ChannelFeedSearchCriteria channelFeedSearchCriteria = channelFeedSearchCriteriaBuilder.buildWithDefaults(
          pageNumber, pageSize, filter, communicationStatusFilter);

      feed = getFeed(channelId, channelFeedSearchCriteria);
    } catch (SearchCriteriaException ex) {
      // If there was a problem building the criteria from the URL parameters due to them being invalid, we want to
      // return a successful response and an empty feed to the client.
      LOGGER.error("Error building search criteria", ex);
      feed = new PaginatedChannelFeed();
    }

    return atomFeedConverter.convert(feed);
  }

  /**
   * Get Rss representation of channel feed. Not paginated.
   * 
   * We return the page 1 with the default number of webcasts ordered by most recent first.
   * 
   * Note: Checkstyle exemptions due to rule failure around '}' - says there should be whitspace before '}'
   * 
   * @param channelId The Id of the channel to be retrieved.
   * 
   * @return {@link RssFeedDto}
   */
  @RequestMapping(value = { "/channel/{channelId}/feed/rss", "/xml/feedrss/{channelId}" }, produces = {
      "application/rss+xml; charset=utf-8", "application/json; charset=utf-8" })
  @ResponseBody
  public RssFeedDto getRss(@PathVariable final String channelId) {

    ChannelFeedSearchCriteria channelFeedSearchCriteria = channelFeedSearchCriteriaBuilder.buildWithDefaults();

    PaginatedChannelFeed feed = getFeed(channelId, channelFeedSearchCriteria);

    RssFeedDto rssFeedDto = rssFeedConverter.convert(feed);
    return rssFeedDto;
  }

  /**
   * Get a feed of communications (webcasts) from the channel.
   * 
   * @param channelId the id of the channel to get the feed from.
   * @param searchCriteria the search criteria
   * 
   * @return the feed
   */
  private PaginatedChannelFeed getFeed(final String channelId, final ChannelFeedSearchCriteria searchCriteria) {

    PaginatedChannelFeed feed;

    try {
      Channel channel = channelFinder.find(Long.valueOf(channelId));
      feed = channelFeedService.getFeed(channel, searchCriteria);
    } catch (NotFoundException nfe) {
      // we just swallow not found exceptions so we don't report exception messages in logs for
      // incorrect channel ids. these aren't really an issue.
      LOGGER.debug("Feed not found.", nfe);

      // return an empty feed
      feed = new PaginatedChannelFeed();
    } catch (Exception e) {
      // For all exceptions we return an empty feed, after logging a warning.
      LOGGER.warn("Got an exception retrieving the RSS feed for channel [" + channelId + "]", e);
      feed = new PaginatedChannelFeed();
    }

    return feed;
  }

}