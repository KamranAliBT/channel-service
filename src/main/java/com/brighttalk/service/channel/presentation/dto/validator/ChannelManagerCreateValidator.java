/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerCreateValidator.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import java.util.List;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.validator.Validator;
import com.brighttalk.service.channel.business.error.InvalidChannelManagerException;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerDto;
import com.brighttalk.service.channel.presentation.dto.ChannelManagersDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;

/**
 * Syntatical Presentation Layer Validator to validate a chanel manager for creation.
 */
@Component
public class ChannelManagerCreateValidator implements Validator<ChannelManagersDto> {

  /**
   * Validate a channel manager for creation.
   * 
   * @param channelManagersDto to be validated.
   */
  @Override
  public void validate(final ChannelManagersDto channelManagersDto) {

    List<ChannelManagerDto> channelManagers = channelManagersDto.getChannelManagers();

    assertManagerEmailAddress(channelManagers);
  }

  private void assertManagerEmailAddress(final List<ChannelManagerDto> channelManagers) {

    for (ChannelManagerDto channelManager : channelManagers) {
      UserDto user = channelManager.getUser();

      if (user == null || user.getEmail() == null) {
        throw new InvalidChannelManagerException();
      }
    }
  }

}
