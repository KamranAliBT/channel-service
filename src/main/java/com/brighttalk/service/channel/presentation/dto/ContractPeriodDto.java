/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodDto.java 101520 2015-10-15 16:29:24Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import com.brighttalk.service.channel.presentation.dto.serialization.StringToIntSerializer;
import com.brighttalk.service.channel.presentation.dto.validation.constraint.ISO8601Date;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.CreateContractPeriod;
import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateContractPeriod;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ContractPeriodDto {

  @Min(value = 0, groups = { UpdateContractPeriod.class },
      message = "{InvalidContractPeriodId}")
  @Digits(groups = { UpdateContractPeriod.class },
      message = "{InvalidContractPeriodId}", integer = 10, fraction = 0)
  @JsonSerialize(using = StringToIntSerializer.class)
  private String id;

  @Min(value = 0, groups = { CreateContractPeriod.class, UpdateContractPeriod.class },
      message = "{InvalidChannelId}")
  @Digits(groups = { CreateContractPeriod.class, UpdateContractPeriod.class },
      message = "{InvalidChannelId}", integer = 10, fraction = 0)
  @NotNull(groups = { CreateContractPeriod.class, UpdateContractPeriod.class }, message = "{ChannelIdEmpty}")
  @JsonSerialize(using = StringToIntSerializer.class)
  private String channelId;

  @NotEmpty(groups = { CreateContractPeriod.class,
      UpdateContractPeriod.class }, message = "{InvalidContractPeriodStart}")
  @ISO8601Date(groups = { UpdateContractPeriod.class,
      CreateContractPeriod.class }, message = "{InvalidContractPeriodStart}")
  private String start;

  @NotEmpty(groups = { CreateContractPeriod.class, UpdateContractPeriod.class }, message = "{InvalidContractPeriodEnd}")
  @ISO8601Date(groups = { UpdateContractPeriod.class,
      CreateContractPeriod.class }, message = "{InvalidContractPeriodEnd}")
  private String end;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getChannelId() {
    return channelId;
  }

  public void setChannelId(String channelId) {
    this.channelId = channelId;
  }

  public String getStart() {
    return start;
  }

  public void setStart(String start) {
    this.start = start;
  }

  public String getEnd() {
    return end;
  }

  public void setEnd(String end) {
    this.end = end;
  }

}
