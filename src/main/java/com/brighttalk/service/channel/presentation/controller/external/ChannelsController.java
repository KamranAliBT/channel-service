/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelsController.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.common.presentation.dto.converter.context.ConverterContext;
import com.brighttalk.common.presentation.dto.converter.context.builder.ExternalConverterContextBuilder;
import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.user.User;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.common.web.annotation.DateTimeFormat;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.PresentationCriteria;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.presentation.ApiRequestParam;
import com.brighttalk.service.channel.presentation.dto.ChannelsPublicDto;
import com.brighttalk.service.channel.presentation.dto.converter.ChannelConverter;
import com.brighttalk.service.channel.presentation.util.ChannelsSearchCriteriaBuilder;
import com.brighttalk.service.channel.presentation.util.PresentationCriteriaBuilder;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform operations on {@link Channel channels}.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller
@RequestMapping(value = "/channels")
public class ChannelsController {

  /** Allowed request parameters. */
  protected static final Set<String> ALLOWED_REQUEST_PARAMS = new HashSet<String>(Arrays.asList(
          ApiRequestParam.DATE_TIME_FORMAT, ApiRequestParam.IDS, ApiRequestParam.PAGE_NUMBER, ApiRequestParam.PAGE_SIZE));

  /** Channel finder */
  @Autowired
  @Qualifier(value = "readOnlyChannelFinder")
  private ChannelFinder channelFinder;

  /** Channel converter. */
  @Autowired
  private ChannelConverter channelConverter;

  @Autowired
  private ExternalConverterContextBuilder contextBuilder;

  /** Find public channel search criteria builder. Used to wrap the request parameters to object. */
  @Autowired
  private ChannelsSearchCriteriaBuilder channelsSearchCriteriaBuilder;

  /** Presentation criteria builder builder */
  @Autowired
  private PresentationCriteriaBuilder presentationCriteriaBuilder;

  /** Brighttalk validator. */
  @Autowired
  private BrighttalkValidator validator;

  /**
   * Handles a request to get the authenticated user subscribed channels.
   * 
   * @param user Authenticated user
   * @param request the http request
   * @param formatter the date time formatter
   * 
   * @return collection of subscribed channels or empty collection if the user is not subscribed to any channel.
   */
  @SuppressWarnings("deprecation")
  @RequestMapping(value = "/subscribed", method = RequestMethod.GET)
  @ResponseBody
  public ChannelsPublicDto getSubscribedChannels(@AuthenticatedUser final User user, final HttpServletRequest request,
                                                 @DateTimeFormat final DateTimeFormatter formatter) {

    PresentationCriteria criteria = presentationCriteriaBuilder.build(request);
    validator.validate(criteria, ValidationException.class);

    List<Channel> subscribedChannels = this.channelFinder.findSubscribedChannels(user);

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);

    ChannelsPublicDto channels = channelConverter.convertForPublic(subscribedChannels, converterContext, criteria);

    return channels;
  }

  /**
   * Handles a request to get multiple channels in a single call.
   * 
   * @param request The Http Request
   * @param formatter The dateTimeFormatter - instantiated using the standard anotation
   * 
   * @return Dto of public channels
   */
  @RequestMapping(value = "/public", method = RequestMethod.GET)
  @ResponseBody
  public ChannelsPublicDto getPublicChannels(final HttpServletRequest request,
                                             @DateTimeFormat final DateTimeFormatter formatter) {

    ChannelsSearchCriteria searchCriteria = channelsSearchCriteriaBuilder.build(request);
    validator.validate(searchCriteria, ValidationException.class);

    PresentationCriteria presentationCriteria = presentationCriteriaBuilder.build(request);
    validator.validate(presentationCriteria, ValidationException.class);

    PaginatedList<Channel> channels = this.channelFinder.find(searchCriteria);

    ConverterContext converterContext = contextBuilder.build(request, formatter, ALLOWED_REQUEST_PARAMS);

    ChannelsPublicDto channelsDto = channelConverter.convertForPublic(channels, converterContext, presentationCriteria);

    return channelsDto;
  }
}
