/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: ChannelsSearchCriteriaBuilder.java 69499 2013-10-09 11:19:13Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria;
import com.brighttalk.service.channel.presentation.ApiRequestParam;

/**
 * Builder Class to extract a {@link ChannelManagersSearchCriteriaBuilder} from a request object.
 */
@Component
public class ChannelManagersSearchCriteriaBuilder extends AbstractSearchCriteriaBuilder {

  @Value("${channelManager.search.defaultPageNumber}")
  private int defaultPageNumber;

  @Value("${channelManager.search.defaultPageSize}")
  private int defaultPageSize;

  @Value("${channelManager.search.maxPageSize}")
  private int maxPageSize;

  @Value("${channelManager.search.minPageSize}")
  private int minPageSize;

  /**
   * Build the {@link ChannelManagersSearchCriteria} from request context, with default values.
   * 
   * @param request the {@link HttpServletRequest} to convert to criteria
   * 
   * @return the built criteria
   */
  public ChannelManagersSearchCriteria buildWithDefaultValues(final HttpServletRequest request) {

    ChannelManagersSearchCriteria criteria = build(request);

    ensureSearchCriteriaDefaultValues(criteria);

    return criteria;
  }

  /**
   * Build the public channel search criteria from request context.
   * 
   * @param request the http request to convert to criteria
   * 
   * @return The built criteria
   */
  private ChannelManagersSearchCriteria build(final HttpServletRequest request) {

    Integer pageNumber = getIntegerFromRequest(request, ApiRequestParam.PAGE_NUMBER);
    Integer pageSize = getIntegerFromRequest(request, ApiRequestParam.PAGE_SIZE);
    String sortBy = request.getParameter(ApiRequestParam.SORT_BY);
    String sortOrder = request.getParameter(ApiRequestParam.SORT_ORDER);

    ChannelManagersSearchCriteria searchCriteria = new ChannelManagersSearchCriteria();
    searchCriteria.setPageNumber(pageNumber);
    searchCriteria.setPageSize(pageSize);
    searchCriteria.setSortBy(sortBy);
    searchCriteria.setSortOrder(sortOrder);

    return searchCriteria;
  }

  /**
   * Set default params to the {@link BaseSearchCriteria} if not present.
   * 
   * @param criteria the {@link BaseSearchCriteria}
   */
  private void ensureSearchCriteriaDefaultValues(final BaseSearchCriteria criteria) {

    if (criteria.getPageNumber() == null || criteria.getPageNumber() <= 0) {
      // Set default page number
      criteria.setPageNumber(defaultPageNumber);
    }

    if (criteria.getPageSize() == null) {
      // Set default Page Size
      criteria.setPageSize(defaultPageSize);
    } else if (criteria.getPageSize() < minPageSize) {
      // Limit Page Size to min allowed
      criteria.setPageSize(minPageSize);
    } else if (criteria.getPageSize() > maxPageSize) {
      // Limit Page Size to max allowed
      criteria.setPageSize(maxPageSize);
    }
  }

}