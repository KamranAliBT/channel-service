/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: UserSearchCriteriaBuilder.java 69499 2013-10-09 11:19:13Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;

/**
 * Builder Class to extract a {@link UserSearchCriteriaBuilder} from a request object.
 */
@Component
public class UserSearchCriteriaBuilder extends AbstractSearchCriteriaBuilder {

  @Value("${user.search.defaultPageNumber}")
  private int defaultPageNumber;

  @Value("${user.search.defaultPageSize}")
  private int defaultPageSize;

  @Value("${user.search.maxPageSize}")
  private int maxPageSize;

  /**
   * Build the public user search criteria.
   * 
   * @return the built criteria
   */
  public UsersSearchCriteria buildWithDefaultValues() {

    UsersSearchCriteria criteria = new UsersSearchCriteria();
    ensureSearchCriteriaDefaultValues(criteria);
    return criteria;
  }

  /**
   * Build the public user search criteria.
   * 
   * @param pageNumber page number
   * @param pageSize page size
   * 
   * @return The built criteria
   */
  public UsersSearchCriteria build(final Integer pageNumber, final Integer pageSize) {

    UsersSearchCriteria searchCriteria = new UsersSearchCriteria();
    searchCriteria.setPageNumber(pageNumber);
    searchCriteria.setPageSize(pageSize);
    return searchCriteria;
  }

  /**
   * Build the public user search criteria.
   * 
   * @param pageNumber page number
   * @param pageSize page size
   * @param sortBy sort by
   * @param sortOrder sort order
   * 
   * @return The built criteria
   */
  public UsersSearchCriteria build(final Integer pageNumber, final Integer pageSize, final String sortBy,
      final String sortOrder) {

    UsersSearchCriteria searchCriteria = build(pageNumber, pageSize);
    searchCriteria.setSortBy(sortBy);
    searchCriteria.setSortOrder(sortOrder);

    return searchCriteria;
  }

  /**
   * Set default params to the {@link BaseSearchCriteria} if not present.
   * 
   * @param criteria the {@link BaseSearchCriteria}
   */
  private void ensureSearchCriteriaDefaultValues(final BaseSearchCriteria criteria) {

    // Set default page number
    if (criteria.getPageNumber() == null) {
      criteria.setPageNumber(defaultPageNumber);
    }

    // Set default Page Size
    if (criteria.getPageSize() == null) {
      criteria.setPageSize(defaultPageSize);
    }

    // Limit Page Size to max allowed
    if (criteria.getPageSize() > maxPageSize) {
      criteria.setPageSize(maxPageSize);
    }
  }

}