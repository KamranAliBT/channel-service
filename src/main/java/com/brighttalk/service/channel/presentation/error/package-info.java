/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 50129 2012-06-22 16:01:36Z tberthon $
 * ****************************************************************************
 */
/**
 * This package contains the Presentation error classes.
 */
package com.brighttalk.service.channel.presentation.error;