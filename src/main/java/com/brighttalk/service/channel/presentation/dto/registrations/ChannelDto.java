/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.registrations;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * ChannelDto.
 * 
 * Needs to have a name specified in the XmlType annotation as there already is a dto with exact same class name
 */
@XmlType(name = "communicationRegistrationChannelDto")
public class ChannelDto {

  private Long id;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(Long id) {
    this.id = id;
  }
}