/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: PivotingController.java 87634 2014-12-16 12:31:02Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.User.Role;
import com.brighttalk.common.validation.BrighttalkValidator;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.ProviderNotSupportedException;
import com.brighttalk.service.channel.business.service.communication.CommunicationPivotService;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.presentation.dto.WebcastProviderDto;
import com.brighttalk.service.channel.presentation.dto.converter.WebcastProviderConverter;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles requests for a
 * communication to be pivoted from one provider type to another (e.g. HD to Video).
 * 
 * Internal controller so does not support authentication/authorisation. Intended for trusted internal clients (such as
 * other services) to do auto-pivoting.
 * 
 * At the time of writing...the HD LiveEvent service uses this to automatically pivot from a HD webcast to a recorded
 * video webcast.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 */
@Controller(value = "externalPivotingController")
@RequestMapping(value = "/webcast/{webcastId}/provider")
public class PivotingController {

  /** logger for this class. */
  protected static final Logger LOGGER = Logger.getLogger(PivotingController.class);

  /** communication db Dao. */
  @Autowired
  protected CommunicationDbDao communicationDbDao;

  /** validator used to check the dto. */
  @Autowired
  protected BrighttalkValidator validator;

  /** audio and slide to mediazone pivot service. */
  @Autowired
  @Qualifier(value = "audioslidestomediazonepivotservice")
  protected CommunicationPivotService audioSlidesToMediazonePivotService;

  /** mediazone to video pivot service. */
  @Autowired
  @Qualifier(value = "mediazonetovideopivotservice")
  protected CommunicationPivotService mediazoneToVideoPivotService;

  /** webcast provider converter. */
  @Autowired
  protected WebcastProviderConverter providerConverter;

  /**
   * Update webcast provider.
   * 
   * @param user The Current User
   * @param webcastProviderDto the request body
   * @param webcastId The Id of the webcast to be updated.
   * 
   * @return confirmation dto
   */
  @RequestMapping(method = RequestMethod.PUT)
  @ResponseStatus(value = HttpStatus.OK)
  @ResponseBody
  public WebcastProviderDto pivot(@AuthenticatedUser(role = Role.OPERATIONS) final User user,
    @RequestBody final WebcastProviderDto webcastProviderDto, @PathVariable final Long webcastId) {

    // validate webcast provider dto
    try {
      validator.validate(webcastProviderDto);
    } catch (ApplicationException ae) {
      throw new ProviderNotSupportedException(ae.getMessage(), new Object[] { webcastProviderDto.getName() });
    }

    LOGGER.info("Handling a request to update provider for communication [" + webcastId + "] to ["
        + webcastProviderDto.getName() + "].");

    Provider provider = providerConverter.convert(webcastProviderDto);

    // find communication to be pivoted
    final Communication communication = communicationDbDao.find(webcastId);

    // pivot the webcast
    Provider updatedProvider = getPivotService(communication).pivot(communication, provider);

    WebcastProviderDto responseDto = providerConverter.convert(updatedProvider);

    LOGGER.info("Successfully updated provider of communication [" + webcastId + "] to [" + updatedProvider + "].");

    return responseDto;
  }

  /**
   * Return the pivot service depending on the communication type.
   * 
   * @param communication used to be able to distinguish the pivot service to be used.
   * @return pivot service to be used.
   */
  private CommunicationPivotService getPivotService(final Communication communication) {

    if (communication.isBrightTALK()) {
      return audioSlidesToMediazonePivotService;
    } else if (communication.isMediaZone() || communication.isVideo()) {
      return mediazoneToVideoPivotService;
    } else {
      // we should not ever get here - guaranteed by validation
      throw new ProviderNotSupportedException("Provider [" + communication.getProvider().getName()
          + "] unsupported for communication [" + communication.getId() + "]",
          new Object[] { communication.getProvider().getName() });

    }

  }

}