/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ConferenceRoomController.java 72818 2014-01-21 15:28:21Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.brighttalk.service.channel.business.error.DialledInTooEarlyException;
import com.brighttalk.service.channel.business.error.DialledInTooLateException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;

/**
 * The internal Conference room controller handling internal call from other services (PBX).
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 */
@Controller
@RequestMapping(value = "/internal/")
public class ConferenceRoomController {

  @Autowired
  private CommunicationFinder communicationFinder;

  /** Bad Request error code. */
  protected static final String BAD_REQUEST_ERROR_CODE = "BadRequest";

  /**
   * Handles a internal request to get a conference room for a given pin number and optional provider. Set the produces
   * to force the response content-type to be text-plain.
   * 
   * @param pin mandatory pin of the communication to find
   * @param provider optional provider field of the communication to look for
   * @param response used to set the response status code
   * 
   * @return the conference room url or error code (TooEarly, TooLate, NotFound)
   */
  @RequestMapping(value = "conference_room", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
  @ResponseBody
  public String getConferenceRoom(@RequestParam(value = "pin", required = true) final String pin,
    @RequestParam(value = "provider", required = false) final String provider, final HttpServletResponse response) {

    // verify the presenter can dial in. If they can return the conference room url they should be sent to
    // so they can present the gig.
    // Otherwise return the appropriate error response.
    // Note - we don't use the usual plot of throwing exceptions.
    // This API returns text/plain for its content type since its invoked by the PBX which cannot handle
    // xml or json. If we throw exceptions then our response will be xml. We don't want this, we want text.
    // So we just explicitly set the response status and return the text error message.
    try {
      String conferenceRoomUrl = communicationFinder.find(pin, provider);
      return conferenceRoomUrl;

    } catch (DialledInTooEarlyException dialledInTooEarlyException) {
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
      return dialledInTooEarlyException.getUserErrorCode();

    } catch (DialledInTooLateException dialledInTooLateException) {
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
      return dialledInTooLateException.getUserErrorCode();

    } catch (NotFoundException notFoundException) {
      response.setStatus(HttpServletResponse.SC_NOT_FOUND);
      return notFoundException.getUserErrorCode();
    }

  }
}