/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodConverter.java 101520 2015-10-15 16:29:24Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.time.DateTimeParser;
import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.presentation.dto.ContractPeriodDto;
import com.brighttalk.service.channel.presentation.dto.ContractPeriodsDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Converter to convert between {@link ContractPeriod} and {@link ContractPeriodDto} and vice-versa.
 */
@Component
public class ContractPeriodConverter {

  private DateTimeParser parser;

  @Autowired
  public ContractPeriodConverter(DateTimeParser parser) {
    this.parser = parser;
  }

  /**
   * Convert a {@link ContractPeriod} to a {@link ContractPeriodDto} using the supplied {@link DateTimeFormatter} to
   * format the dates
   *
   * @param contractPeriod the ContractPeriod to be converted
   * @param formatter the DateTimeFormatter to be used to convert the dates
   *
   * @return
   */
  public ContractPeriodDto convert(final ContractPeriod contractPeriod, final DateTimeFormatter formatter) {
    ContractPeriodDto contractPeriodDto = new ContractPeriodDto();
    contractPeriodDto.setId(Long.toString(contractPeriod.getId()));
    contractPeriodDto.setChannelId(Long.toString(contractPeriod.getChannelId()));
    contractPeriodDto.setStart(formatter.convert(contractPeriod.getStart()));
    contractPeriodDto.setEnd(formatter.convert(contractPeriod.getEnd()));
    return contractPeriodDto;
  }

  /**
   * Converts a {@link List} of {@link ContractPeriod} to a {@link ContractPeriodsDto} object using
   * {@link ContractPeriodConverter#convert(ContractPeriod, DateTimeFormatter)}
   *
   * @param contractPeriodList the list of ContractPeriods to be converted
   * @param formatter the DateTimeFormatter to be used to convert the dates
   *
   * @return a {@link ContractPeriodsDto}
   */
  public ContractPeriodsDto convert(final List<ContractPeriod> contractPeriodList, final DateTimeFormatter formatter) {
    List<ContractPeriodDto> contractPeriodDtos = new ArrayList<>();
    for (ContractPeriod contractPeriod : contractPeriodList) {
      contractPeriodDtos.add(convert(contractPeriod, formatter));
    }

    ContractPeriodsDto contractPeriodsDto = new ContractPeriodsDto();
    contractPeriodsDto.setContractPeriods(contractPeriodDtos);
    return contractPeriodsDto;
  }

  /**
   * Convert a {@link ContractPeriodDto} to a {@link ContractPeriod} using the supplied {@link DateTimeFormatter} to
   * format the dates
   *
   * @param contractPeriodDto the ContractPeriodDto to be converted
   *
   * @return
   */
  public ContractPeriod convert(final ContractPeriodDto contractPeriodDto) {
    ContractPeriod contractPeriod = new ContractPeriod();
    if (!StringUtils.isEmpty(contractPeriodDto.getId())) {
      contractPeriod.setId(Long.parseLong(contractPeriodDto.getId()));
    }
    contractPeriod.setChannelId(Long.parseLong(contractPeriodDto.getChannelId()));
    contractPeriod.setStart(parser.parse(contractPeriodDto.getStart()));
    contractPeriod.setEnd(parser.parse(contractPeriodDto.getEnd()));
    return contractPeriod;
  }
}
