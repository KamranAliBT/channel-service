/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerCreateValidator.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.validator.Validator;
import com.brighttalk.service.channel.business.error.InvalidChannelManagerException;
import com.brighttalk.service.channel.presentation.dto.ChannelManagerDto;

/**
 * Syntatical Presentation Layer Validator to validate a chanel manager for creation.
 */
@Component
public class ChannelManagerUpdateValidator implements Validator<ChannelManagerDto> {

  private Long channelManagerId;

  /**
   * Default constructor.
   */
  public ChannelManagerUpdateValidator() {

  }

  /**
   * Constructor to set the channel manager id.
   * 
   * @param channelManagerId channel manager id
   */
  public ChannelManagerUpdateValidator(final Long channelManagerId) {
    this.channelManagerId = channelManagerId;
  }

  /**
   * Validate a channel manager for creation.
   * 
   * @param channelManagerDto to be validated.
   */
  @Override
  public void validate(final ChannelManagerDto channelManagerDto) {

    assertId(channelManagerDto);
  }

  private void assertId(final ChannelManagerDto channelManagerDto) {

    if (channelManagerId == null || !channelManagerId.equals(channelManagerDto.getId())) {
      throw new InvalidChannelManagerException();
    }
  }

}
