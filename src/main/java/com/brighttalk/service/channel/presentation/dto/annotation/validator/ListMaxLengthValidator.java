/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ListMaxLengthValidator.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.annotation.validator;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Channel Validator checking the supplied String List is within a specific length, any supplied String List contain
 * length (the length of all individual values combined together) that are larger than the configured max allowed length
 * will be rejected by this Validator.
 */
@Component
public class ListMaxLengthValidator implements ConstraintValidator<ListMaxLength, List<String>> {

  private static final Logger LOGGER = Logger.getLogger(ListMaxLengthValidator.class);

  private int maxAllowedLength;

  @Override
  public void initialize(ListMaxLength listMaxLength) {
    this.maxAllowedLength = listMaxLength.max();
  }

  /**
   * Validating The supplied String List values contain (all individual values length combined together) to be less than
   * or equal to the allowed configured String List max allowed length.
   * 
   * @return true if valid, false otherwise.
   * <p>
   * {@inheritDoc}
   */
  @Override
  public boolean isValid(List<String> strValues, ConstraintValidatorContext arg1) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Validating the supplied String List [" + strValues + "] length.");
    }

    int allValuesLength = 0;
    if (strValues != null) {
      for (String strValue : strValues) {
        allValuesLength += strValue.length();
      }
    }

    return allValuesLength <= this.maxAllowedLength;
  }

  /**
   * Only provided to support testing.
   * 
   * @param maxAllowedLength the maxAllowedLength to set
   */
  protected void setMaxAllowedLength(int maxAllowedLength) {
    this.maxAllowedLength = maxAllowedLength;
  }

}
