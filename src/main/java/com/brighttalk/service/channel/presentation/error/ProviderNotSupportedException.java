/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ProviderNotSupportedException.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.error;

import com.brighttalk.common.validation.ValidationException;

/**
 * Presentation Validation Exception that is thrown when an incorrect value of communication provider has been passed in
 * the request.
 */
@SuppressWarnings("serial")
public class ProviderNotSupportedException extends ValidationException {

  private static final String VALIDATION_ERROR_CODE = "ProviderNotSupported";

  /**
   * @param message see below.
   * @param userErrorMessage see below.
   * @see ValidationException#ValidationException(String, String)
   */
  public ProviderNotSupportedException(final String message, final String userErrorMessage) {
    super(message, userErrorMessage);
  }

  @Override
  public String getUserErrorCode() {
    return VALIDATION_ERROR_CODE;
  }
}