/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastProviderDto.java 72961 2014-01-24 15:16:01Z ikerbib $
 * ****************************************************************************
 */

package com.brighttalk.service.channel.presentation.dto.syndication;

import javax.xml.bind.annotation.XmlElement;

import com.brighttalk.service.channel.presentation.dto.ChannelDto;

/**
 * DTO object representing webcast syndication.
 */
public abstract class BaseWebcastSyndicationDto {

  /** Channel Dto. */
  private ChannelDto channel;

  /** Master authorisation dto. */
  private AuthorisationDto masterAuthorisation;

  /** Consumer authorisation dto. */
  private AuthorisationDto consumerAuthorisation;

  /**
   * Default constructor.
   */
  public BaseWebcastSyndicationDto() {
  }

  /**
   * @return the channelDto
   */
  public ChannelDto getChannel() {
    return channel;
  }

  /**
   * @param channel the channelDto to set
   */
  @XmlElement(name = "channel")
  public void setChannel(final ChannelDto channel) {
    this.channel = channel;
  }

  /**
   * @return the masterAuthorisationDto
   */
  public AuthorisationDto getMasterAuthorisation() {
    return masterAuthorisation;
  }

  /**
   * @param masterAuthorisation the masterAuthorisationDto to set
   */
  @XmlElement(name = "masterAuthorisation")
  public void setMasterAuthorisation(final AuthorisationDto masterAuthorisation) {
    this.masterAuthorisation = masterAuthorisation;
  }

  /**
   * @return the consumerAuthorisationDto
   */
  public AuthorisationDto getConsumerAuthorisation() {
    return consumerAuthorisation;
  }

  /**
   * @param consumerAuthorisation the consumerAuthorisationDto to set
   */
  @XmlElement(name = "consumerAuthorisation")
  public void setConsumerAuthorisation(final AuthorisationDto consumerAuthorisation) {
    this.consumerAuthorisation = consumerAuthorisation;
  }

}