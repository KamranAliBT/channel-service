/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EventController.java 81759 2014-08-11 10:53:40Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.internal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.service.channel.business.service.user.UserService;
import com.brighttalk.service.channel.presentation.dto.UserDto;

/**
 * Web {@link org.springframework.web.servlet.mvc.Controller Controller} that provides _internal_ web API request that
 * could be used by the User Service to notify the Channel Service about the BrightTALK user status.
 */
@Controller
@RequestMapping(value = "/internal/event")
public class EventController {
  private static final Logger logger = Logger.getLogger(EventController.class);

  @Autowired
  private UserService userService;

  /**
   * Handles a request that notifies the channel service that the identified user has been deleted.
   * 
   * @param userDto The deleted user.
   */
  @RequestMapping(value = "/user_deleted", method = RequestMethod.POST)
  @ResponseStatus(value = HttpStatus.OK)
  @ResponseBody
  public void notifyUserDeleted(@RequestBody final UserDto userDto) {
    if (logger.isDebugEnabled()) {
      logger.debug("Recieved a request to notify the channel service that the user [" + userDto.getId()
          + "] has been deleted.");
    }

    userService.delete(userDto.getId());
  }

  /**
   * @param userService the userService to set
   */
  protected void setUserService(UserService userService) {
    this.userService = userService;
  }

}