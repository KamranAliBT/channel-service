/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AbstractResourceValidator.java 84340 2014-10-07 17:03:10Z ssarraj $ 
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.brighttalk.service.channel.business.domain.validator.Validator;
import com.brighttalk.service.channel.business.error.InvalidResourceException;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;

/**
 * Base class for Presentation Layer Resource Validators.
 * 
 */
public abstract class AbstractResourceValidator implements Validator<ResourceDto> {

  /** The maximum length of the title of a resource */
  protected static final int MAX_TITLE_LENGTH = 255;

  /** The maximum length of the description of a resource. */
  protected static final int MAX_DESCRIPTION_LENGTH = 4000;

  /** The maximum length of the url of a resource. */
  protected static final int MAX_URL_LENGTH = 2000;

  /** The maximum length of the mime type of a resource. */
  protected static final int MAX_MIMETYPE_LENGTH = 255;

  /** The message error code indicating that resource type is not allowed. */
  protected static final String ERRORCODE_RESOURCE_TYPE = "ResourceTypeNotAllowed";

  /** The message error code indicating that resource size is not allowed. */
  protected static final String ERRORCODE_RESOURCE_ID_MISMATCH = "ResourceIdsMismatch";

  /** The message indicating that resource size is not allowed. */
  protected static final String MESSAGE_RESOURCE_ID_MISMATCH = "Given Resources Ids do not match";

  /** The message error code indicating that resource size is not allowed. */
  protected static final String ERRORCODE_RESOURCE_SIZE_NOT_ALLOWED = "ResourceSizeNotAllowed";

  /** The message indicating that resource size is not allowed. */
  protected static final String MESSAGE_RESOURCE_SIZE_NOT_ALLOWED = "Resource size is not allowed";

  /** The message error code indicating that resource mime type is not allowed. */
  protected static final String ERRORCODE_RESOURCE_MIME_TYPE_NOT_ALLOWED = "ResourceMimeTypeNotAllowed";

  /** The message indicating that resource mime type is not allowed. */
  protected static final String MESSAGE_RESOURCE_MIME_TYPE_NOT_ALLOWED = "Resource mime type is not allowed";

  /** The message error code indicating that resource url is not allowed. */
  protected static final String ERRORCODE_RESOURCE_URL_NOT_ALLOWED = "ResourceUrlNotAllowed";

  /** The message indicating that resource url is not allowed. */
  protected static final String MESSAGE_RESOURCE_URL_NOT_ALLOWED = "Resource URL is not allowed";

  /** The message error code indicating that resource title is missing. */
  protected static final String ERRORCODE_RESOURCE_TITLE_MISSING = "ResourceTitleMissing";

  /** The message indicating that resource title is missing. */
  protected static final String MESSAGE_RESOURCE_TITLE_MISSING = "Resource title is missing.";

  /** The message error code indicating that resource url is missing. */
  protected static final String ERRORCODE_RESOURCE_URL_MISSING = "ResourceUrlMissing";

  /** The message indicating that resource url is missing. */
  protected static final String MESSAGE_RESOURCE_URL_MISSING = "Resource url is missing.";

  /** The message error code indicating that resource url is too long. */
  protected static final String ERRORDOCE_RESOURCE_URL_TOO_LONG = "ResourceUrlTooLong";

  /** The message indicating that resource url is too long. */
  protected static final String MESSAGE_RESOURCE_URL_TOO_LONG = "Resource url is too long";

  /** The message error code indicating that resource title is too long. */
  protected static final String ERRORCODE_RESOURCE_TITLE_TOO_LONG = "ResourceTitleTooLong";

  /** The message indicating that resource title is too long. */
  protected static final String MESSAGE_RESOURCE_TITLE_TOO_LONG = "Resource title is too long";

  /** The message error code indicating that resource description is too long. */
  protected static final String ERRORCODE_RESOURCE_DESCRIPTION_TOO_LONG = "ResourceDescriptionTooLong";

  /** The message indicating that resource description is too long. */
  protected static final String MESSAGE_RESOURCE_DESCRIPTION_TOO_LONG = "Resource description is too long";

  /** The message error code indicating that resource mime type is too long. */
  protected static final String ERRORCODE_RESOURCE_MIMETYPE_TOO_LONG = "ResourceMimeTypeTooLong";

  /** The message indicating that resource mime type is too long. */
  protected static final String MESSAGE_RESOURCE_MIMETYPE_TOO_LONG = "Resource mime type is too long";

  /** The message indicating that resource is incorrectly specified. */
  protected static final String MESSAGE_RESOURCE_NOCORRECTLY_SPECIFIED = "Resource is incorrectly specified.";

  /** The message error code indicating that resource hosted is missing. */
  protected static final String ERRORCODE_RESOURCE_HOSTED_MISSING = "ResourceFileHostedMissing";

  /** The message indicating that resource hosted is missing. */
  protected static final String MESSAGE_RESOURCE_HOSTED_MISSING = "File hosted attribute must be provided";

  /** The message error code indicating that resource url is invalid. */
  protected static final String ERRORCODE_RESOURCE_URL_INVALID = "ResourceUrlIsInvalid";

  /** The message indicating that resource url is invalid. */
  protected static final String MESSAGE_RESOURCE_URL_INVALID = "Resource URL is invalid";

  /** URL regex used to determine if the url is in valid format. **/
  protected static final String URL_REGEX = "^(https|http)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

  /**
   * Helper method to assert a mandatory String field is set.
   * 
   * @param field the field to assert.
   * @param logMessage the message for the system log if the field isn't set
   * @param errorCode the error code to return to the client if the field isn't set.
   * 
   * @throws InvalidResourceException with the message and error code if the field does not assert.
   */
  protected void assertMandatoryField(final String field, final String logMessage, final String errorCode) {

    if (StringUtils.isEmpty(field)) {
      throw new InvalidResourceException(logMessage, errorCode);
    }
  }

  /**
   * Helper method to assert not delete able field.
   * 
   * @param field the field to assert.
   * @param logMessage the message for the system log if the field isn't set
   * @param errorCode the error code to return to the client if the field isn't set.
   * 
   * @throws InvalidResourceException with the message and error code if the field does not assert.
   */
  protected void assertNotDeletableField(final String field, final String logMessage, final String errorCode) {

    if (field != null && field.isEmpty()) {
      throw new InvalidResourceException(logMessage, errorCode);
    }
  }

  /**
   * Helper method to assert the max length of the String field.
   * 
   * @param field the field to assert.
   * @param maxLength the max field length
   * @param logMessage the message for the system log if the field isn't set
   * @param errorCode the error code to return to the client if the field isn't set.
   * 
   * @throws InvalidResourceException with the message and error code if the field does not assert.
   */
  protected void assertMaxFieldLength(final String field, final int maxLength, final String logMessage,
      final String errorCode) {

    if (!StringUtils.isEmpty(field) && field.length() > maxLength) {
      throw new InvalidResourceException(logMessage, errorCode);
    }
  }

  /**
   * Assert that the given resource is a valid type.
   * 
   * Type is determined from whether the resource is a file or a link.
   * 
   * We assert that only one file or link is set for a resource.
   * 
   * @param resourceDto The resource to validate
   * 
   * @throws InvalidResourceException Thrown if a file and link are set in the same resource
   */
  protected void assertType(final ResourceDto resourceDto) throws InvalidResourceException {
    if (resourceDto.hasFile() && resourceDto.hasLink()) {
      throw new InvalidResourceException(MESSAGE_RESOURCE_NOCORRECTLY_SPECIFIED);
    }
  }

  /**
   * Helper method to assert the field against regex.
   * 
   * @param field the field to assert.
   * @param regex to validate against
   * @param logMessage the message for the system log if the field isn't set
   * @param errorCode the error code to return to the client if the field isn't set.
   * 
   * @throws InvalidResourceException with the message and error code if the field does not assert.
   */
  protected void assertFieldAgainstRegex(final String field, final String regex, final String logMessage,
      final String errorCode) {
    if (StringUtils.isEmpty(field)) {
      return;
    }

    try {
      Pattern pattern = Pattern.compile(regex);
      Matcher matcher = pattern.matcher(field);
      if (matcher.matches()) {
        return;
      }
    } catch (RuntimeException e) {
      throw new InvalidResourceException(logMessage, errorCode);
    }
    throw new InvalidResourceException(logMessage, errorCode);

  }
}