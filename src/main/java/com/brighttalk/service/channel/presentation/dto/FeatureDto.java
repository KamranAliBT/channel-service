/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeatureDto.java 97779 2015-07-09 13:11:27Z build $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Feature Dto. DTO object representing feature.
 */
@XmlRootElement(name = "feature")
public class FeatureDto {

  @NotEmpty(message = "{InvalidFeatureName}")
  private String name;

  @NotNull(message = "{InvalidFeatureEnabled}")
  private Boolean enabled;

  private String value;

  /**
   * Creates new FeatureDto with provided name.
   * 
   * @param name of a feature
   */
  public FeatureDto(final String name) {
    this.name = name;
  }

  /**
   * Constructs feature Dto object as a new object.
   */
  public FeatureDto() {
  }

  public String getName() {
    return name;
  }

  @XmlAttribute
  public void setName(final String name) {
    this.name = name;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  @XmlAttribute
  public void setEnabled(final Boolean enabled) {
    this.enabled = enabled;
  }

  public String getValue() {
    return value;
  }

  @XmlValue
  public void setValue(final String value) {
    this.value = value;
  }
}
