/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
/**
 * This package contains the DTOs annotation validators classes and interfaces.
 */
package com.brighttalk.service.channel.presentation.dto.annotation.validator;