/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastPublicConverter.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.presentation.dto.CategoryDto;
import com.brighttalk.service.channel.presentation.dto.ChannelPublicDto;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.WebcastPublicDto;

/**
 * Converter - Responsible for converting between {@link WebcastPublicDto} and {@link Communication} for public.
 */
@Component
public class WebcastPublicConverter {

  /**
   * Convert the given communication into its Webcast Public DTO.
   * 
   * @param communication the communication to convert
   * @param formatter date time formatter
   * 
   * @return the communication public DTO for that communication.
   */
  public WebcastPublicDto convert(final Communication communication, final DateTimeFormatter formatter) {

    if (communication == null) {
      return new WebcastPublicDto();
    }

    WebcastPublicDto webcastDto = new WebcastPublicDto();
    webcastDto.setId(communication.getId());
    webcastDto.setTitle(communication.getTitle());
    webcastDto.setDescription(communication.getDescription());
    webcastDto.setPresenter(communication.getAuthors());
    webcastDto.setStart(formatter.convert(communication.getScheduledDateTime()));
    webcastDto.setDuration(communication.getDuration());
    webcastDto.setStatus(communication.getStatus().toString().toLowerCase());
    webcastDto.setChannel(convertChannel(communication.getChannelId()));
    webcastDto.setRating(communication.getCommunicationStatistics().getAverageRating());
    webcastDto.setTotalViewings(communication.getCommunicationStatistics().getNumberOfViewings());

    if (communication.hasThumbnailUrl()) {

      webcastDto.addLink(createThumbnailLink(communication));

    }

    return webcastDto;
  }

  /**
   * Convert the given communication into its Webcast Public DTO.
   * 
   * @param communication the communication to convert
   * @param formatter date time formatter
   * 
   * @return the communication public DTO for that communication.
   */
  public WebcastPublicDto convertPublic(final Communication communication, final DateTimeFormatter formatter) {

    WebcastPublicDto webcastDto = new WebcastPublicDto();
    webcastDto.setId(communication.getId());
    webcastDto.setTitle(communication.getTitle());
    webcastDto.setDescription(communication.getDescription());
    webcastDto.setKeywords(communication.getKeywords());
    webcastDto.setPresenter(communication.getAuthors());
    webcastDto.setStart(formatter.convert(communication.getScheduledDateTime()));
    webcastDto.setEntryTime(formatter.convert(communication.getEntryTime()));
    webcastDto.setCloseTime(formatter.convert(communication.getCloseTime()));
    webcastDto.setCreated(formatter.convert(communication.getCreated()));
    webcastDto.setLastUpdated(formatter.convert(communication.getLastUpdated()));
    webcastDto.setVisibility(communication.getConfiguration().getVisibility().name().toLowerCase());
    webcastDto.setFormat(communication.getFormat().toString().toLowerCase());

    webcastDto.setDuration(communication.getDuration());
    webcastDto.setChannel(convertChannel(communication.getChannelId()));
    webcastDto.setRating(communication.getCommunicationStatistics().getAverageRating());

    webcastDto.setUrl(communication.getUrl());

    if (communication.hasThumbnailUrl()) {

      webcastDto.addLink(createThumbnailLink(communication));
    }

    if (communication.hasPreviewUrl()) {

      webcastDto.addLink(createPreviewLink(communication));
    }

    if (communication.isUpcoming() && communication.hasCalendarUrl()) {

      webcastDto.addLink(createCalendarLink(communication));
    }

    if (communication.hasCategories()) {
      webcastDto.setCategories(convert(communication.getCategories()));
    }

    return webcastDto;
  }

  private ChannelPublicDto convertChannel(final Long channelId) {

    ChannelPublicDto channelPublicDto = new ChannelPublicDto();
    channelPublicDto.setId(channelId);

    return channelPublicDto;
  }

  private LinkDto createThumbnailLink(final Communication communication) {

    LinkDto linkDto = new LinkDto();

    linkDto.setHref(communication.getThumbnailUrl());
    linkDto.setRel(LinkDto.RELATIONSHIP_ENCLOSURE);
    linkDto.setType(LinkDto.TYPE_IMAGE_PNG);
    linkDto.setTitle(LinkDto.TITLE_THUMBNAIL);

    return linkDto;
  }

  private LinkDto createPreviewLink(final Communication communication) {

    LinkDto linkDto = new LinkDto();

    linkDto.setHref(communication.getPreviewUrl());
    linkDto.setRel(LinkDto.RELATIONSHIP_RELATED);
    linkDto.setType(LinkDto.TYPE_IMAGE_PNG);
    linkDto.setTitle(LinkDto.TITLE_PREVIEW);

    return linkDto;
  }

  private LinkDto createCalendarLink(final Communication communication) {

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(communication.getCalendarUrl());
    linkDto.setRel(LinkDto.RELATIONSHIP_RELATED);
    linkDto.setType(LinkDto.TYPE_TEXT_CALENDAR);
    linkDto.setTitle(LinkDto.TITLE_CALENDAR);

    return linkDto;
  }

  private List<CategoryDto> convert(final List<CommunicationCategory> categories) {

    List<CategoryDto> categoriesDto = new ArrayList<CategoryDto>();

    for (CommunicationCategory communicationCategory : categories) {

      CategoryDto categoryDto = new CategoryDto();
      categoryDto.setId(communicationCategory.getId());
      categoryDto.setTerm(communicationCategory.getDescription());

      categoriesDto.add(categoryDto);
    }
    return categoriesDto;
  }
}