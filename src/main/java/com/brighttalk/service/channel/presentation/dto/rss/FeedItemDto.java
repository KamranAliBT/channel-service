/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeedItemDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.rss;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FeedItemDto.
 */
@XmlType( propOrder = { "itemId", "title", "pubDate", 
    "description", "guid", "presenter", "communication", "link", 
    "enclosure", "categories", "calendar" } )
public class FeedItemDto {

  private ItemIdDto itemId;
  
  private String title;
  
  private String pubDate;
  
  private String description; 
  
  private GuidDto guid;
   
  private String presenter;
  
  private FeedCommunicationDto communication;
  
  private String link;
  
  private EnclosureDto enclosure;

  private String calendar;
  
  private List<String> categories;

  public ItemIdDto getItemId() {
    return itemId;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/rss_extensions", name = "itemid" )
  public void setItemId(ItemIdDto itemId) {
    this.itemId = itemId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPubDate() {
    return pubDate;
  }

  public void setPubDate(String pubDate) {
    this.pubDate = pubDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public GuidDto getGuid() {
    return guid;
  }

  public void setGuid(GuidDto guid) {
    this.guid = guid;
  }

  public String getPresenter() {
    return presenter;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/rss_extensions" )
  public void setPresenter(String presenter) {
    this.presenter = presenter;
  }

  public FeedCommunicationDto getCommunication() {
    return communication;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/rss_extensions" )
  public void setCommunication(FeedCommunicationDto communication) {
    this.communication = communication;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public EnclosureDto getEnclosure() {
    return enclosure;
  }

  public void setEnclosure(EnclosureDto enclosure) {
    this.enclosure = enclosure;
  }

  public String getCalendar() {
    return calendar;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/rss_extensions" )
  public void setCalendar(String calendar) {
    this.calendar = calendar;
  }

  public List<String> getCategories() {
    return categories;
  }

  @XmlElement(name = "category")
  public void setCategories(List<String> categories) {
    this.categories = categories;
  }
}