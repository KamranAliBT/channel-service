/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ListFormat.java 87015 2014-12-03 12:07:44Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.annotation.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Validates the String List contain (the list all individual values) following the configured regular expression.
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ListFormatValidator.class)
@Documented
public @interface ListFormat {

  // CHECKSTYLE:OFF
  /**
   * @return The regular expression string used to validate the supplied String List values. Default value allow any
   * string format.
   */
  String regex() default ".*";

  // CHECKSTYLE:ON

  /** Default error message */
  String message() default "Invalid Format";

  /** Default groups */
  Class<?>[] groups() default {

  };

  /** Default pay load */
  Class<? extends Payload>[] payload() default {

  };
}
