/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastProviderDto.java 72961 2014-01-24 15:16:01Z ikerbib $
 * ****************************************************************************
 */

package com.brighttalk.service.channel.presentation.dto.syndication;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * DTO object representing webcast syndication.
 */
@XmlRootElement(name = "SyndicationOut")
public class WebcastSyndicationOutDto extends BaseWebcastSyndicationDto {

  /**
   * Default constructor.
   */
  public WebcastSyndicationOutDto() {
  }

}