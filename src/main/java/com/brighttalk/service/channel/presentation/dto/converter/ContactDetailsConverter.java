/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContactDetailsConverter.java 88863 2015-01-27 11:35:19Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.presentation.dto.ContactDetailsPublicDto;

/**
 * Converter for converting between {@link ContactDetails} and {@link ContactDetailsPublicDto}.
 */
@Component
public class ContactDetailsConverter {

  /**
   * Converts a {@link ContactDetails} in to a {@link ContactDetailsPublicDto}.
   * 
   * @param contactDetails {@link ContactDetails} to convert.
   * 
   * @return An instance of {@link ContactDetailsPublicDto}.
   */
  public ContactDetailsPublicDto convertForPublic(ContactDetails contactDetails) {
    
    ContactDetailsPublicDto contactDetailsPublicDto = new ContactDetailsPublicDto();
    
    contactDetailsPublicDto.setFirstName(contactDetails.getFirstName());
    contactDetailsPublicDto.setLastName(contactDetails.getLastName());
    contactDetailsPublicDto.setEmail(contactDetails.getEmail());
    contactDetailsPublicDto.setTelephone(contactDetails.getTelephone());
    contactDetailsPublicDto.setJobTitle(contactDetails.getJobTitle());
    contactDetailsPublicDto.setCompanyName(contactDetails.getCompany());
    contactDetailsPublicDto.setAddress1(contactDetails.getAddress1());
    contactDetailsPublicDto.setAddress2(contactDetails.getAddress2());
    contactDetailsPublicDto.setCity(contactDetails.getCity());
    contactDetailsPublicDto.setStateRegion(contactDetails.getState());
    contactDetailsPublicDto.setPostcode(contactDetails.getPostcode());
    contactDetailsPublicDto.setCountry(contactDetails.getCountry()); 
    
    return contactDetailsPublicDto;
  }

  /**
   * Converts a {@link ContactDetailsPublicDto} in to a {@link ContactDetails}.
   * 
   * @param contactDetailsPublicDto {@link ContactDetailsPublicDto} to convert.
   * 
   * @return An instance of {@link ContactDetails}.
   */
  public ContactDetails convertFromPublic(ContactDetailsPublicDto contactDetailsPublicDto) {
    
    ContactDetails contactDetails = new ContactDetails();
    
    contactDetails.setFirstName(contactDetailsPublicDto.getFirstName());
    contactDetails.setLastName(contactDetailsPublicDto.getLastName());
    contactDetails.setEmail(contactDetailsPublicDto.getEmail());
    contactDetails.setTelephone(contactDetailsPublicDto.getTelephone());
    contactDetails.setJobTitle(contactDetailsPublicDto.getJobTitle());
    contactDetails.setCompany(contactDetailsPublicDto.getCompanyName());
    contactDetails.setAddress1(contactDetailsPublicDto.getAddress1());
    contactDetails.setAddress2(contactDetailsPublicDto.getAddress2());
    contactDetails.setCity(contactDetailsPublicDto.getCity());
    contactDetails.setState(contactDetailsPublicDto.getStateRegion());
    contactDetails.setPostcode(contactDetailsPublicDto.getPostcode());
    contactDetails.setCountry(contactDetailsPublicDto.getCountry()); 
    
    return contactDetails;
  }
}
