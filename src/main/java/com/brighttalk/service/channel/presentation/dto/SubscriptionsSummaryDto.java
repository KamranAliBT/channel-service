/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsSummaryDto.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.List;

/**
 * Dto to hold the subscriptions summary details.
 */
public class SubscriptionsSummaryDto {

  private List<SubscriptionDto> subscriptions;

  private List<SubscriptionErrorDto> errors;

  public List<SubscriptionDto> getSubscriptions() {
    return subscriptions;
  }

  public void setSubscriptions(final List<SubscriptionDto> subscriptions) {
    this.subscriptions = subscriptions;
  }

  public List<SubscriptionErrorDto> getErrors() {
    return errors;
  }

  public void setErrors(final List<SubscriptionErrorDto> errors) {
    this.errors = errors;
  }

}
