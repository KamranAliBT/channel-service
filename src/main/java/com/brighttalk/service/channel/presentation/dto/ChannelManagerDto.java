/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerDto.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.brighttalk.service.channel.business.domain.validator.Validator;

/**
 * DTO object representing a channel manager.
 */
@JsonRootName("channelManager")
public class ChannelManagerDto {

  private Long id;

  private UserDto user;

  private boolean emailAlerts;

  /**
   * Default constructor.
   */
  public ChannelManagerDto() {

  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(final UserDto user) {
    this.user = user;
  }

  public boolean getEmailAlerts() {
    return emailAlerts;
  }

  public void setEmailAlerts(final boolean emailAlerts) {
    this.emailAlerts = emailAlerts;
  }

  /**
   * Validates this dto with given validator.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<ChannelManagerDto> validator) {
    validator.validate(this);
  }

}
