/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SurveyDto.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Survey Dto. DTO object representing survey.
 */
@XmlRootElement(name = "survey")
public class SurveyDto {

  private Long id;

  private Boolean isActive;

  /**
   * Creates new SurveyDto with provided id and is active indicator.
   * 
   * @param id of a survey
   * @param isActive indicator of the survey
   */
  public SurveyDto(Long id, Boolean isActive) {
    this.id = id;
    this.isActive = isActive;
  }

  /**
   * Constructs survey Dto object as a new object.
   */
  public SurveyDto() {
  }

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(Long id) {
    this.id = id;
  }

  public Boolean getIsActive() {
    return isActive;
  }

  @XmlAttribute
  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

}
