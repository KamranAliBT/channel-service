/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceExternalUpdateValidator.java 84340 2014-10-07 17:03:10Z ssarraj $ 
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import com.brighttalk.service.channel.business.error.InvalidResourceException;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourceFileDto;
import com.brighttalk.service.channel.presentation.dto.ResourceLinkDto;

/**
 * Presentation Layer Validator to validate a resource for an external API update.
 */
public class ResourceExternalUpdateValidator extends AbstractResourceValidator {

  /**
   * The id of the resource being validated
   */
  private final Long resourceId;

  /**
   * Construct this validator with the id of the resource to be validated. 
   * @param resourceId the id of the resource to be validated. 
   */
  public ResourceExternalUpdateValidator(Long resourceId) {
    this.resourceId = resourceId;
  }

  /**
   * Validate the given resource for (external) update.  
   * 
   * Validate that the given resourceDto contains all required fields. 
   * Title - mandatory and length
   * Description - length
   * 
   * disallow any attempt to set details of an internal hosted file.
   * 
   * @param resourceDto to be validated.
   * 
   */
  @Override
  public void validate(ResourceDto resourceDto) {

    assertId(resourceDto, resourceId);
    assertTitle(resourceDto);
    assertDescription(resourceDto);
    assertType(resourceDto);

    if (resourceDto.hasFile()) {
      ResourceFileDto file = resourceDto.getFile();

      assertFileHosted(file);

      if (resourceDto.isHostedInternal()) {

        assertFileDetailsAreEmpty(file);

      } else {

        assertFileDetails(file);

      }

    } else if (resourceDto.hasLink()) {

      ResourceLinkDto link = resourceDto.getLink();
      assertLinkDetails(link);
    }
  }

  /**
   * Assert the file details are syntactically allowed. 
   *
   * @param ResourceFileDto the resource file to be validated.
   * 
   * @throws InvalidResourceException if the resource cannot be validated.
   */
  private void assertFileDetails(ResourceFileDto file) {

    assertUrl(file.getHref());
    assertMimeType(file.getMimeType());

  }

  /**
   * Assert there are no details set on a file object.
   * 
   * @param resourceFileDto the file to be validated.
   * 
   * @throws InvalidResourceException if the resource cannot be validated.
   */
  private void assertFileDetailsAreEmpty(ResourceFileDto file) {

    // not allowed fields are url, size and mimetype
    if (file.hasHref()) {
      throw new InvalidResourceException(MESSAGE_RESOURCE_URL_NOT_ALLOWED, ERRORCODE_RESOURCE_URL_NOT_ALLOWED);
    }

    if (file.hasMimeType()) {
      throw new InvalidResourceException(MESSAGE_RESOURCE_MIME_TYPE_NOT_ALLOWED,
              ERRORCODE_RESOURCE_MIME_TYPE_NOT_ALLOWED);
    }

    if (file.hasSize()) {
      throw new InvalidResourceException(MESSAGE_RESOURCE_SIZE_NOT_ALLOWED, ERRORCODE_RESOURCE_SIZE_NOT_ALLOWED);
    }

  }

  /**
   * Assert details of a link are syntactically correct.
   * 
   * @param ResourceLinkDto the link to be validated.
   * 
   * @throws InvalidResourceException if the resource cannot be validated.
   */
  private void assertLinkDetails(ResourceLinkDto link) {

    // url is optional for an link resource not not delete able
    assertUrl(link.getHref());
  }

  /**
   * Asserts a the given id of a resource is the same as set in the object.
   * 
   * @param resourceDto the resource Dto to be validated.
   * @param resourceId the id given to compare.
   * 
   * @throws InvalidResourceException if the resource Dto id is no matching this.passedResourceId
   */
  private void assertId(ResourceDto resourceDto, Long resourceId) {

    if (!resourceId.equals(resourceDto.getId())) {
      throw new InvalidResourceException(MESSAGE_RESOURCE_ID_MISMATCH, ERRORCODE_RESOURCE_ID_MISMATCH);
    }

  }

  /**
   * Asserts a title is an resource Dto title.
   * 
   * @param resourceDto the resource Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource title is missing or is too long.
   */
  private void assertTitle(ResourceDto resourceDto) {
    assertNotDeletableField(resourceDto.getTitle(), MESSAGE_RESOURCE_TITLE_MISSING, ERRORCODE_RESOURCE_TITLE_MISSING);
    assertMaxFieldLength(resourceDto.getTitle(), MAX_TITLE_LENGTH, MESSAGE_RESOURCE_URL_MISSING,
        ERRORCODE_RESOURCE_TITLE_TOO_LONG);
  }

  /**
   * Asserts an resource Dto url.
   * 
   * @param resourceDto the resource Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource url is missing or is too long.
   */
  private void assertUrl(String url) {

    assertNotDeletableField(url, MESSAGE_RESOURCE_URL_MISSING, ERRORCODE_RESOURCE_URL_MISSING);
    assertMaxFieldLength(url, MAX_URL_LENGTH, MESSAGE_RESOURCE_URL_TOO_LONG, ERRORDOCE_RESOURCE_URL_TOO_LONG);
    assertFieldAgainstRegex(url, URL_REGEX, MESSAGE_RESOURCE_URL_INVALID, ERRORCODE_RESOURCE_URL_INVALID);
  }

  /**
   * Asserts an resource Dto description.
   * 
   * @param resourceDto the resource Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource description is too long.
   */
  private void assertDescription(ResourceDto resourceDto) {
    assertMaxFieldLength(resourceDto.getDescription(), MAX_DESCRIPTION_LENGTH, MESSAGE_RESOURCE_DESCRIPTION_TOO_LONG,
                                                       ERRORCODE_RESOURCE_DESCRIPTION_TOO_LONG);
  }

  /**
   * Asserts an resource Dto mime type.
   * 
   * @param resourceDto the resource Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource mime type is missing or is too long.
   */
  private void assertMimeType(String mimeType) {
    assertMaxFieldLength(mimeType, MAX_MIMETYPE_LENGTH, MESSAGE_RESOURCE_MIMETYPE_TOO_LONG,
        ERRORCODE_RESOURCE_MIMETYPE_TOO_LONG);
  }

  /**
   * Asserts a file has the hosted attribute set
   *
   * @param resourceFileDto the resource file Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource mime type is missing or is too long.
   */
  private void assertFileHosted(ResourceFileDto file) {
    assertMandatoryField(file.getHosted(), ERRORCODE_RESOURCE_HOSTED_MISSING, MESSAGE_RESOURCE_HOSTED_MISSING);
  }
}
