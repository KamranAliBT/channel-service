/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastConverter.java 72818 2014-01-21 15:28:21Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.syndication.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication.SyndicationAuthorisationType;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndications;
import com.brighttalk.service.channel.presentation.dto.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.statistics.CommunicationStatisticsDto;
import com.brighttalk.service.channel.presentation.dto.syndication.AuthorisationDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationInDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationOutDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationsDto;

/**
 * Converter - Responsible for converting Communication syndications domain objects to webcast syndications DTOs.
 */
@Component
public class WebcastSyndicationConverter {

  /**
   * Convert the given communication syndication into a WebcastSyndicationDto.
   * 
   * @param communicationSyndication the communication syndication to convert
   * @param formatter date time formatter
   * 
   * @return the WebcastSyndication Dto for that communication syndication.
   */
  public WebcastSyndicationInDto convertSyndicatedIn(final CommunicationSyndication communicationSyndication,
      final DateTimeFormatter formatter) {

    WebcastSyndicationInDto webcastSyndicationInDto = new WebcastSyndicationInDto();

    webcastSyndicationInDto.setChannel(convertChannel(communicationSyndication));

    webcastSyndicationInDto.setMasterAuthorisation(convertAuthorisation(communicationSyndication, formatter,
        SyndicationAuthorisationType.MASTER.getType()));

    webcastSyndicationInDto.setConsumerAuthorisation(convertAuthorisation(communicationSyndication, formatter,
        SyndicationAuthorisationType.CONSUMER.getType()));

    return webcastSyndicationInDto;
  }

  /**
   * Convert the given communication into a list of webcast syndication DTO.
   * 
   * @param communicationSyndications the list of communication syndications to convert
   * @param formatter date time formatter
   * 
   * @return the list of webcast syndication DTOs.
   */
  public List<WebcastSyndicationOutDto> convertSyndicatedOut(
      final List<CommunicationSyndication> communicationSyndications, final DateTimeFormatter formatter) {

    List<WebcastSyndicationOutDto> webcastSyndicationOutDtos = new ArrayList<>();

    if (CollectionUtils.isEmpty(communicationSyndications)) {
      return webcastSyndicationOutDtos;
    }

    for (CommunicationSyndication communicationSyndication : communicationSyndications) {
      WebcastSyndicationOutDto webcastSyndicationOutDto = new WebcastSyndicationOutDto();

      webcastSyndicationOutDto.setChannel(convertChannel(communicationSyndication));

      webcastSyndicationOutDto.setMasterAuthorisation(convertAuthorisation(communicationSyndication, formatter,
          SyndicationAuthorisationType.MASTER.getType()));

      webcastSyndicationOutDto.setConsumerAuthorisation(convertAuthorisation(communicationSyndication, formatter,
          SyndicationAuthorisationType.CONSUMER.getType()));

      webcastSyndicationOutDtos.add(webcastSyndicationOutDto);

    }

    return webcastSyndicationOutDtos;
  }

  /**
   * Convert a communication syndication into an authorisation dto.
   * 
   * @param communicationSyndication to be converted
   * @param formatter for the date and time
   * @param type of the authorisation (master or consumer)
   * 
   * @return the authorisation dto.
   */
  public AuthorisationDto convertAuthorisation(final CommunicationSyndication communicationSyndication,
      final DateTimeFormatter formatter, final String type) {

    String approvalStatus = null;
    String approvalDate = null;

    if (type.equals(SyndicationAuthorisationType.MASTER.getType())) {
      approvalStatus = communicationSyndication.getMasterChannelSyndicationAuthorisationStatus().getStatus();
      approvalDate = formatter.convert(communicationSyndication.getMasterChannelSyndicationAuthorisationTimestamp());
    } else {
      approvalStatus = communicationSyndication.getConsumerChannelSyndicationAuthorisationStatus().getStatus();
      approvalDate = formatter.convert(communicationSyndication.getConsumerChannelSyndicationAuthorisationTimestamp());
    }

    AuthorisationDto authorisationDto = new AuthorisationDto();

    authorisationDto.setStatus(approvalStatus);
    authorisationDto.setDate(approvalDate);

    return authorisationDto;
  }

  /**
   * Converts a {@link CommunicationSyndications} domain object to {@link WebcastSyndicationsDto} DTO.
   * 
   * @param communicationSyndications The {@link CommunicationSyndications} to convert.
   * @param formatter date time formatter
   * @return The converted {@link WebcastSyndicationsDto}.
   */
  public WebcastSyndicationsDto convert(final CommunicationSyndications communicationSyndications,
      final DateTimeFormatter formatter) {
    Validate.notNull(communicationSyndications, "Communication syndications must not be null.");

    WebcastSyndicationsDto communicationSyndicationsDto = new WebcastSyndicationsDto();

    // Convert the supplied communication master channel details and master channel communication details.
    communicationSyndicationsDto.setChannel(convertChannel(communicationSyndications.getChannel()));
    communicationSyndicationsDto.setCommunication(convertSyndicatedCommunication(communicationSyndications.getCommunication()));

    // Convert the supplied communication distributed channels syndication if exists, otherwise assign empty
    // syndications array.
    if (!CollectionUtils.isEmpty(communicationSyndications.getCommunicationSyndications())) {
      for (CommunicationSyndication communicationSyndication : communicationSyndications.getCommunicationSyndications()) {
        communicationSyndicationsDto.addSyndications(convert(communicationSyndication, formatter));
      }
    } else {
      communicationSyndicationsDto.setSyndications(new ArrayList<WebcastSyndicationDto>());
    }

    return communicationSyndicationsDto;
  }

  /**
   * Converts a {@link CommunicationSyndication} domain object to {@link WebcastSyndicationDto} DTO.
   * 
   * @param communicationSyndication The {@link CommunicationSyndication} to convert.
   * @param formatter date time formatter
   * @return The converted {@link WebcastSyndicationDto}.
   */
  public WebcastSyndicationDto convert(final CommunicationSyndication communicationSyndication,
      final DateTimeFormatter formatter) {
    Validate.notNull(communicationSyndication, "Communication syndication must not be null.");

    WebcastSyndicationDto communicationSyndicationDto = new WebcastSyndicationDto();

    communicationSyndicationDto.setChannel(convertChannel(communicationSyndication.getChannel()));

    // Convert Syndication master and consumer authorisation syndication details.
    AuthorisationDto masterAuthDto = convertAuthorisation(communicationSyndication, formatter,
        SyndicationAuthorisationType.MASTER.getType());
    masterAuthDto.setType(SyndicationAuthorisationType.MASTER.getType());
    communicationSyndicationDto.addAuthorisation(masterAuthDto);

    AuthorisationDto consumerAuthDto = convertAuthorisation(communicationSyndication,
        formatter, SyndicationAuthorisationType.CONSUMER.getType());
    consumerAuthDto.setType(SyndicationAuthorisationType.CONSUMER.getType());
    communicationSyndicationDto.addAuthorisation(consumerAuthDto);

    // Communication statistics is optional, only present for master channel syndication.
    if (communicationSyndication.getCommunicationStatistics() != null) {
      communicationSyndicationDto.setCommunicationStatistics(convertStatistics(communicationSyndication.getCommunicationStatistics()));
    }

    return communicationSyndicationDto;
  }

  /**
   * Converts a {@link CommunicationStatistics} domain object to {@link CommunicationStatisticsDto} DTO.
   * 
   * @param communicationStatistics The {@link CommunicationStatistics} to convert.
   * @return The converted {@link CommunicationStatisticsDto}.
   */
  private CommunicationStatisticsDto convertStatistics(final CommunicationStatistics communicationStatistics) {
    Validate.notNull(communicationStatistics, "Communication statistics must not be null.");

    CommunicationStatisticsDto communicationStatisticsDto = new CommunicationStatisticsDto(
        communicationStatistics.getTotalRegistration(), communicationStatistics.getNumberOfViewings(),
        communicationStatistics.getTotalViewingDuration(), communicationStatistics.getAverageRating());

    return communicationStatisticsDto;
  }

  /**
   * Converts the {@link Channel} domain object to {@link ChannelDto} DTO. The converted DTO created only with the
   * channel id, title and organisation.
   * 
   * @param channel The channel to convert.
   * @return The converted {@link ChannelDto}.
   */
  private ChannelDto convertChannel(final Channel channel) {
    ChannelDto channelDto = null;
    if (channel != null) {
      channelDto = new ChannelDto(channel.getId());
      channelDto.setTitle(channel.getTitle());
      channelDto.setOrganisation(channel.getOrganisation());
    }
    return channelDto;
  }

  /**
   * convert the communication syndication into a channel dto.
   * 
   * @param communicationSyndication to be converted
   * @return the channel dto.
   */
  private ChannelDto convertChannel(final CommunicationSyndication communicationSyndication) {

    ChannelDto channelDto = new ChannelDto();
    if (communicationSyndication.getChannel() != null) {
      channelDto.setId(communicationSyndication.getChannel().getId());
    }

    return channelDto;
  }

  /**
   * Converts the syndicated {@link Communication} domain object to {@link WebcastDto} DTO. The converted DTO created
   * only with the communication title.
   * 
   * @param communication The communication to convert.
   * @return The converted {@link WebcastDto}.
   */
  private WebcastDto convertSyndicatedCommunication(final Communication communication) {
    WebcastDto webcastDto = null;
    if (communication != null) {
      webcastDto = new WebcastDto();
      webcastDto.setTitle(communication.getTitle());
    }
    return webcastDto;
  }

}
