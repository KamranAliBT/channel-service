/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: ISO8601Date.java 70202 2013-10-24 17:31:29Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validation.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Bean validation constraint used to ensure string is a correctly formatted ISO8601 date.
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ISO8601DateValidator.class)
public @interface ISO8601Date {

  /**
   * Validation error message with default value.
   */
  String message() default "Invalid date format";

  /**
   * Validation scenarios definition.
   */
  // CHECKSTYLE:OFF
  Class<?>[] groups() default {};

  // CHECKSTYLE:ON

  /**
   * Can be used by clients of the API to assign custom objects to a constraint.
   */
  // CHECKSTYLE:OFF
  Class<? extends Payload>[] payload() default {};
  // CHECKSTYLE:ON
}