/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastConverter.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.time.DateTimeFormatter;
import com.brighttalk.common.time.DateTimeParser;
import com.brighttalk.service.channel.business.domain.Keywords;
import com.brighttalk.service.channel.business.domain.MediazoneConfig;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationScheduledPublication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.presentation.dto.CategoryDto;
import com.brighttalk.service.channel.presentation.dto.ChannelOwnerDto;
import com.brighttalk.service.channel.presentation.dto.ChannelPublicDto;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.RenditionDto;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.WebcastDto;
import com.brighttalk.service.channel.presentation.dto.WebcastProviderDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationInDto;
import com.brighttalk.service.channel.presentation.dto.syndication.WebcastSyndicationOutDto;
import com.brighttalk.service.channel.presentation.dto.syndication.converter.WebcastSyndicationConverter;

/**
 * Converter - Responsible for converting between {@link WebcastDto} and {@link Communication}.
 */
@Component
public class WebcastConverter {

  @Autowired
  private DateTimeParser dateTimeParser;

  @Autowired
  private Keywords keywords;

  @Autowired
  private WebcastSyndicationConverter webcastSyndicationConverter;

  /**
   * Convert the given communication into its Webcast DTO. This returns the full webcast dto set with the communication
   * data.
   * 
   * @param communication the communication to convert
   * @param formatter date time formatter
   * 
   * @return the communication DTO for that communication.
   */
  public WebcastDto convert(final Communication communication, final DateTimeFormatter formatter) {

    if (communication == null) {
      return new WebcastDto();
    }

    WebcastDto webcastDto = convertCommonProperties(communication, formatter);

    webcastDto.setEntryTime(formatter.convert(communication.getEntryTime()));
    webcastDto.setCloseTime(formatter.convert(communication.getCloseTime()));
    webcastDto.setChannel(convertChannel(communication.getChannelId(), null));

    webcastDto.setRating(communication.getCommunicationStatistics().getAverageRating());
    webcastDto.setTotalViewings(communication.getCommunicationStatistics().getNumberOfViewings());
    webcastDto.setPublishStatus(communication.getPublishStatus().toString().toLowerCase());
    webcastDto.setCreated(formatter.convert(communication.getCreated()));

    if (communication.isUpcoming() && communication.hasCalendarUrl()) {
      webcastDto.addLink(createCalendarLink(communication));
    }

    if (communication.hasCategories()) {
      webcastDto.setCategories(convert(communication.getCategories()));
    }

    setCommunicationConfiguration(communication, webcastDto);

    setScheduledPublication(communication, webcastDto, formatter);

    setSyndication(communication, webcastDto, formatter);

    return webcastDto;
  }

  /**
   * Convert the given communication into its Webcast DTO.
   * 
   * This converter will only set what LES / AMS needs which are the basic communication details.
   * 
   * @param communication the communication to convert
   * @param channel the channel to be converted.
   * @param formatter date time formatter
   * 
   * @return the communication DTO for that communication.
   */
  public WebcastDto convert(final Communication communication, final Channel channel, final DateTimeFormatter formatter) {

    if (communication == null) {
      return new WebcastDto();
    }

    WebcastDto webcastDto = convertCommonProperties(communication, formatter);
    webcastDto.setChannel(convertChannel(communication.getChannelId(), channel));

    return webcastDto;
  }

  /**
   * Convert the common properties of a communication into a webcast DTO.
   * 
   * @param communication to be converted into a webcastDTO
   * @param formatter
   * @return the converted communication into a webcastdto.
   */
  private WebcastDto convertCommonProperties(final Communication communication, final DateTimeFormatter formatter) {
    WebcastDto webcastDto = new WebcastDto();
    webcastDto.setId(communication.getId());
    webcastDto.setTitle(communication.getTitle());
    webcastDto.setDescription(communication.getDescription());
    webcastDto.setKeywords(sanitizeKeywords(communication.getKeywords()));
    webcastDto.setPresenter(communication.getAuthors());
    webcastDto.setStart(formatter.convert(communication.getScheduledDateTime()));
    webcastDto.setDuration(communication.getDuration());
    webcastDto.setStatus(communication.getStatus().toString().toLowerCase());
    webcastDto.setChannel(convertChannel(communication.getChannelId(), null));
    webcastDto.setVisibility(communication.getConfiguration().getVisibility().name().toLowerCase());
    webcastDto.setFormat(communication.getFormat().toString().toLowerCase());
    webcastDto.setUrl(communication.getUrl());
    webcastDto.setChannelWebcastUrl(communication.getChannelWebcastUrl());
    webcastDto.setTimeZone(communication.getTimeZone());

    webcastDto.setProvider(convertProvider(communication, formatter));

    if (communication.isUpcoming() || communication.isLive()) {
      webcastDto.setPinNumber(communication.getPinNumber());
      webcastDto.setLivePhoneNumber(communication.getLivePhoneNumber());
    }

    if (communication.hasThumbnailUrl()) {
      webcastDto.addLink(createThumbnailLink(communication));
    }

    if (communication.hasPreviewUrl()) {
      webcastDto.addLink(createPreviewLink(communication));
    }

    return webcastDto;
  }

  /**
   * Convert the given Webcast DTO into a Communication domain object.
   * 
   * This is the generic convert method that will set all the data from a webcast dto to a communication object.
   * 
   * @param webcastDto the webcast DTO to convert
   * @return the Communication domain object.
   */
  public Communication convert(final WebcastDto webcastDto) {
    if (webcastDto == null) {
      return new Communication();
    }

    Communication communication = this.convertBasicFields(webcastDto);
    communication.setConfiguration(this.convertCommunicationConfiguration(webcastDto));

    if (webcastDto.hasCategories()) {
      List<CategoryDto> categoryDtos = webcastDto.getCategories();
      for (CategoryDto categoryDto : categoryDtos) {
        communication.addCategory(new CommunicationCategory(categoryDto.getId(), webcastDto.getChannel().getId(),
            categoryDto.getTerm(), true));
      }
    }

    convertScheduledPublication(communication, webcastDto);
    return communication;
  }

  /**
   * Convert the given Webcast DTO into a Communication domain object. This is used when LES calls back to update the
   * status of an HD webcast.
   * 
   * @param webcastDto the webcast DTO to convert
   * @return the Communication domain object.
   */
  public Communication convertForStatus(final WebcastDto webcastDto) {
    Communication communication = new Communication();
    communication.setId(webcastDto.getId());
    if (webcastDto.hasStatus()) {
      communication.setStatus(Communication.Status.valueOf(webcastDto.getStatus().toUpperCase()));
    }

    if (webcastDto.hasProvider()) {
      communication.setProvider(new Provider(webcastDto.getProvider().getName()));
      List<Rendition> renditions = this.convertRenditions(webcastDto);
      communication.setRenditions(renditions);
    }

    if (webcastDto.hasDuration()) {
      communication.setDuration(webcastDto.getDuration());
    }

    return communication;
  }

  /**
   * Convert a webcast dto into a communication. This sets the basic communication properties.
   * 
   * @param webcastDto to convert into a communication object
   * @return the communication object
   */
  public Communication convertBasicFields(final WebcastDto webcastDto) {
    Communication communication = new Communication();
    communication.setId(webcastDto.getId());
    if (webcastDto.getChannel() != null) {
      communication.setChannelId(webcastDto.getChannel().getId());
      communication.setMasterChannelId(webcastDto.getChannel().getId());
    }
    communication.setTitle(webcastDto.getTitle());
    communication.setDescription(webcastDto.getDescription());
    communication.setKeywords(webcastDto.getKeywords());
    communication.setAuthors(webcastDto.getPresenter());
    communication.setTimeZone(webcastDto.getTimeZone());

    if (webcastDto.hasDuration()) {
      communication.setDuration(webcastDto.getDuration());
    }

    if (webcastDto.hasScheduled()) {
      communication.setScheduledDateTime(dateTimeParser.parse(webcastDto.getScheduled()));
    }

    if (webcastDto.hasProvider()) {
      communication.setProvider(new Provider(webcastDto.getProvider().getName()));
      communication.setRenditions(convertRenditions(webcastDto));
    }

    if (webcastDto.hasFormat()) {
      communication.setFormat(Communication.Format.valueOf(webcastDto.getFormat().toUpperCase()));
    }

    if (webcastDto.hasStatus()) {
      communication.setStatus(Communication.Status.valueOf(webcastDto.getStatus().toUpperCase()));
    }

    if (webcastDto.hasPublishStatus()) {
      communication.setPublishStatus(Communication.PublishStatus.valueOf(webcastDto.getPublishStatus().toUpperCase()));
    }

    if (webcastDto.hasLinks()) {
      List<LinkDto> links = webcastDto.getLinks();

      for (LinkDto linkDto : links) {
        if (linkDto.getTitle().equals(LinkDto.TITLE_THUMBNAIL)) {
          communication.setThumbnailUrl(linkDto.getHref());
        }
        if (linkDto.getTitle().equals(LinkDto.TITLE_PREVIEW)) {
          communication.setPreviewUrl(linkDto.getHref());
        }
      }
    }

    return communication;
  }

  /**
   * Set the communication configuration on the object.
   * 
   * @param communication
   * @param webcastDto
   */
  private void setCommunicationConfiguration(final Communication communication, final WebcastDto webcastDto) {

    CommunicationConfiguration config = communication.getConfiguration();

    webcastDto.setClientBookingReference(config.getClientBookingReference());

    if (config.hasAllowAnonymousViewings()) {
      webcastDto.setAllowAnonymous(getStringFromBoolean(config.allowAnonymousViewings()));
    }

    if (config.hasExcludeFromChannelContentPlan()) {
      webcastDto.setExcludeFromChannelCapacity(getStringFromBoolean(config.excludeFromChannelContentPlan()));
    }

    if (config.hasShowChannelSurvey()) {
      webcastDto.setShowChannelSurvey(getStringFromBoolean(config.showChannelSurvey()));
    }

    if (config.hasVisibility()) {
      webcastDto.setVisibility(config.getVisibility().toString().toLowerCase());
    }

    if (config.hasCustomUrl()) {
      webcastDto.setCustomUrl(config.getCustomUrl());
    }

  }

  /**
   * Set the scheduled publication date.
   * 
   * @param communication
   * @param webcastDto
   * @param formatter
   */
  private void setScheduledPublication(final Communication communication, final WebcastDto webcastDto,
    final DateTimeFormatter formatter) {

    if (!communication.hasScheduledPublication()) {
      return;
    }

    CommunicationScheduledPublication communicationScheduledPublication = communication.getScheduledPublication();

    if (communicationScheduledPublication.hasPublicationDate()) {
      webcastDto.setPublicationDate(formatter.convert(communicationScheduledPublication.getPublicationDate()));
    }

    if (communicationScheduledPublication.hasUnpublicationDate()) {
      webcastDto.setUnpublicationDate(formatter.convert(communicationScheduledPublication.getUnpublicationDate()));
    }

  }

  /**
   * Set syndication property.
   * 
   * @param communication to add the syndication to
   * @param webcastDto the dto that contains the data
   * @param formatter
   */
  private void setSyndication(final Communication communication, final WebcastDto webcastDto,
    final DateTimeFormatter formatter) {

    CommunicationSyndication communicationSyndication = communication.getConfiguration().getCommunicationSyndication();
    WebcastSyndicationDto webcastSyndicationDto = new WebcastSyndicationDto();

    if (communication.isMasterCommunication()) {
      List<CommunicationSyndication> consumerSyndications = communicationSyndication.getConsumerSyndications();
      if (!CollectionUtils.isEmpty(consumerSyndications)) {

        List<WebcastSyndicationOutDto> webcastSyndicationOutDtos = webcastSyndicationConverter.convertSyndicatedOut(
            consumerSyndications, formatter);
        webcastSyndicationDto.setSyndicationOuts(webcastSyndicationOutDtos);
        // webcastSyndicationOutDtos.setWebcastSyndicationOutDtos(webcastSyndicationOutDtos);
      }

    } else {
      CommunicationSyndication masterSyndication = communicationSyndication.getMasterSyndication();

      if (masterSyndication != null) {
        WebcastSyndicationInDto webcastSyndicationInDto = webcastSyndicationConverter.convertSyndicatedIn(
            masterSyndication, formatter);
        webcastSyndicationDto.setSyndicationIn(webcastSyndicationInDto);

        // webcastDto.setWebcastSyndicationInDto(webcastSyndicationInDto);
      }
    }

    webcastDto.setSyndication(webcastSyndicationDto);
  }

  /**
   * Sets the scheduled publication to the communication object.
   * 
   * If the publication or unpublication date is set in the request, we need to set it on the communication object. If
   * the element is set with a defined date, we use the one provided in the request else we use a default one, when the
   * element is set but empty.
   * 
   * @param communication to set the scheduled publication to.
   * @param webcastDto to get the scheduled publication from
   */
  private void convertScheduledPublication(final Communication communication, final WebcastDto webcastDto) {

    if (webcastDto.hasPublicationDate() || webcastDto.hasUnpublicationDate()) {

      CommunicationScheduledPublication communicationScheduledPublication = new CommunicationScheduledPublication();

      String publicationDate = webcastDto.getPublicationDate();
      String unpublicationDate = webcastDto.getUnpublicationDate();

      Date publicationDateValue = communicationScheduledPublication.getDefaultScheduledPublicationDate();
      if (StringUtils.isNotEmpty(publicationDate)) {
        publicationDateValue = dateTimeParser.parse(publicationDate);
      }

      Date unpublicationDateValue = communicationScheduledPublication.getDefaultScheduledPublicationDate();
      if (StringUtils.isNotEmpty(unpublicationDate)) {
        unpublicationDateValue = dateTimeParser.parse(unpublicationDate);
      }

      communicationScheduledPublication.setPublicationDate(publicationDateValue);
      communicationScheduledPublication.setUnpublicationDate(unpublicationDateValue);

      communication.setScheduledPublication(communicationScheduledPublication);
    }

  }

  private CommunicationConfiguration convertCommunicationConfiguration(final WebcastDto webcastDto) {
    CommunicationConfiguration configuration = new CommunicationConfiguration(webcastDto.getId(),
        webcastDto.getChannel().getId());

    if (webcastDto.hasVisibility()) {
      configuration.setVisibility(CommunicationConfiguration.Visibility.valueOf(webcastDto.getVisibility().toUpperCase()));
    }

    configuration.setClientBookingReference(webcastDto.getClientBookingReference());

    configuration.setAllowAnonymousViewings(new Boolean(webcastDto.getAllowAnonymous()));

    configuration.setExcludeFromChannelContentPlan(new Boolean(webcastDto.getExcludeFromChannelCapacity()));

    configuration.setShowChannelSurvey(new Boolean(webcastDto.getShowChannelSurvey()));

    configuration.setCustomUrl(webcastDto.getCustomUrl());

    return configuration;
  }

  private WebcastProviderDto convertProvider(final Communication communication, final DateTimeFormatter formatter) {
    WebcastProviderDto webcastProviderDto = new WebcastProviderDto();
    webcastProviderDto.setName(communication.getProvider().getName());

    if (communication.isVideo()) {
      webcastProviderDto.setRenditions(convertRenditions(communication.getRenditions()));
    } else if (communication.isMediaZone()) {
      if (communication.hasMediazoneConfig()) {
        webcastProviderDto.setResource(convertMediazoneResourceDto(communication.getMediazoneConfig()));
        webcastProviderDto.setLink(convertMediazoneLinkDto(communication.getMediazoneConfig()));
        webcastProviderDto.setUpdated(formatter.convert(communication.getMediazoneConfig().getLastUpdated()));
      }
    }

    return webcastProviderDto;
  }

  private List<Rendition> convertRenditions(final WebcastDto webcastDto) {
    if (CollectionUtils.isEmpty(webcastDto.getProvider().getRenditions())) {
      return null;
    }

    List<Rendition> renditions = new ArrayList<Rendition>();

    for (RenditionDto renditionDto : webcastDto.getProvider().getRenditions()) {
      Rendition rendition = new Rendition();

      if (renditionDto.hasIsActive()) {
        Boolean isActive = renditionDto.getIsActive().toString().equals(true) ? true : false;
        rendition.setIsActive(isActive);
      }

      if (renditionDto.hasUrl()) {
        rendition.setUrl(renditionDto.getUrl());
      }

      if (renditionDto.hasType()) {
        rendition.setType(renditionDto.getType());
      }

      if (renditionDto.hasContainer()) {
        rendition.setContainer(renditionDto.getContainer());
      }

      if (renditionDto.hasCodecs()) {
        rendition.setCodecs(renditionDto.getCodecs());
      }

      if (renditionDto.hasWidth()) {
        rendition.setWidth(renditionDto.getWidth());
      }

      if (renditionDto.hasBitrate()) {
        rendition.setBitrate(renditionDto.getBitrate());
      }

      renditions.add(rendition);
    }

    return renditions;
  }

  private List<RenditionDto> convertRenditions(final List<Rendition> renditions) {
    if (CollectionUtils.isEmpty(renditions)) {
      return null;
    }

    List<RenditionDto> renditionsDto = new ArrayList<RenditionDto>();

    for (Rendition rendition : renditions) {
      RenditionDto renditionDto = new RenditionDto();

      renditionDto.setIsActive(rendition.getIsActive().toString());
      renditionDto.setUrl(rendition.getUrl());
      renditionDto.setType(rendition.getType());
      renditionDto.setContainer(rendition.getContainer());
      renditionDto.setCodecs(rendition.getCodecs());
      renditionDto.setWidth(rendition.getWidth());
      renditionDto.setBitrate(rendition.getBitrate());

      renditionsDto.add(renditionDto);
    }

    return renditionsDto;
  }

  private ResourceDto convertMediazoneResourceDto(final MediazoneConfig mediazoneConfig) {

    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setType(MediazoneConfig.MEDIAZONE_WEBCAST);
    resourceDto.setId(Long.valueOf(mediazoneConfig.getTargetMediazoneId()));

    return resourceDto;
  }

  private LinkDto convertMediazoneLinkDto(final MediazoneConfig mediazoneConfig) {

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(mediazoneConfig.getTargetMediazoneConfigUrl());
    linkDto.setType(MediazoneConfig.MEDIAZONE_CONFIG);

    return linkDto;
  }

  private ChannelPublicDto convertChannel(final Long channelId, final Channel channel) {

    ChannelPublicDto channelPublicDto = new ChannelPublicDto();
    channelPublicDto.setId(channelId);

    if (channel != null) {
      ChannelOwnerDto channelOwnerDto = new ChannelOwnerDto();
      channelOwnerDto.setId(channel.getOwnerUserId());
      channelPublicDto.setChannelOwner(channelOwnerDto);
    }

    return channelPublicDto;
  }

  private LinkDto createThumbnailLink(final Communication communication) {

    LinkDto linkDto = new LinkDto();

    linkDto.setHref(communication.getThumbnailUrl());
    linkDto.setRel(LinkDto.RELATIONSHIP_ENCLOSURE);
    linkDto.setType(LinkDto.TYPE_IMAGE_PNG);
    linkDto.setTitle(LinkDto.TITLE_THUMBNAIL);

    return linkDto;
  }

  private LinkDto createPreviewLink(final Communication communication) {

    LinkDto linkDto = new LinkDto();

    linkDto.setHref(communication.getPreviewUrl());
    linkDto.setRel(LinkDto.RELATIONSHIP_RELATED);
    linkDto.setType(LinkDto.TYPE_IMAGE_PNG);
    linkDto.setTitle(LinkDto.TITLE_PREVIEW);

    return linkDto;
  }

  private LinkDto createCalendarLink(final Communication communication) {

    LinkDto linkDto = new LinkDto();
    linkDto.setHref(communication.getCalendarUrl());
    linkDto.setRel(LinkDto.RELATIONSHIP_RELATED);
    linkDto.setType(LinkDto.TYPE_TEXT_CALENDAR);
    linkDto.setTitle(LinkDto.TITLE_CALENDAR);

    return linkDto;
  }

  private List<CategoryDto> convert(final List<CommunicationCategory> categories) {

    List<CategoryDto> categoriesDto = new ArrayList<CategoryDto>();

    for (CommunicationCategory communicationCategory : categories) {

      CategoryDto categoryDto = new CategoryDto();
      categoryDto.setId(communicationCategory.getId());
      categoryDto.setTerm(communicationCategory.getDescription());

      categoriesDto.add(categoryDto);
    }
    return categoriesDto;
  }

  /**
   * Removes unwanted quotes and commas from the list of keywords.
   * 
   * @param listOfKeywords
   * @return the sanitized list of keywords
   */
  private String sanitizeKeywords(final String listOfKeywords) {
    return keywords.clean(listOfKeywords);
  }

  /**
   * @param dateTimeParser the dateTimeParser to set
   */
  protected void setDateTimeParser(final DateTimeParser dateTimeParser) {
    this.dateTimeParser = dateTimeParser;
  }

  private String getStringFromBoolean(final boolean value) {
    return value ? "true" : "false";
  }

}
