/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2010.
 * All Rights Reserved.
 * $Id: ResourceNotFoundException.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Exception thrown to signal that a requested BrightTALK resource does not yet exist (404).
 */
@SuppressWarnings("serial")
public class ResourceNotFoundException extends ApplicationException {

  /**
   * @param message see below.
   * 
   * @param errorCode see below.
   * 
   * @param errorArgs see below.
   * 
   * @see #ResourceNotFoundException(String, String, Throwable, Object[])
   */
  public ResourceNotFoundException(final String message, final String errorCode, final Object... errorArgs) {
    this(message, errorCode, null, errorArgs);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @param errorArgs The array of objects to be used as parameters in the tokenised user error message that is resolved
   * for this exception. Can be null.
   * 
   * @param cause The {@link Throwable} which is the source of the original exception, which the created exception
   * should wrap. Can be null.
   * 
   * @see ApplicationException#ApplicationException(String, String, Object[], Throwable)
   */
  public ResourceNotFoundException(final String message, final String errorCode, final Throwable cause,
      final Object... errorArgs) {
    super(message, errorCode, errorArgs, cause);
  }

  /**
   * @param message see below.
   * 
   * @param errorCode see below.
   * 
   * @param errorArgs see below.
   * 
   * @see #ResourceNotFoundException(String, String, Throwable, Object[])
   */
  public ResourceNotFoundException(final String message, final ErrorCode errorCode, final Object... errorArgs) {
    this(message, errorCode, null, errorArgs);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @param errorArgs The array of objects to be used as parameters in the tokenised user error message that is resolved
   * for this exception. Can be null.
   * 
   * @param cause The {@link Throwable} which is the source of the original exception, which the created exception
   * should wrap. Can be null.
   * 
   * @see ApplicationException#ApplicationException(String, String, Object[], Throwable)
   */
  public ResourceNotFoundException(final String message, final ErrorCode errorCode, final Throwable cause,
      final Object... errorArgs) {
    super(message, errorCode.getName(), errorArgs, cause);
  }

}