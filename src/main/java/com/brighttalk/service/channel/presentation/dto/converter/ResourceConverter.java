/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceConverter.java 84340 2014-10-07 17:03:10Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourceFileDto;
import com.brighttalk.service.channel.presentation.dto.ResourceLinkDto;
import com.brighttalk.service.channel.presentation.dto.ResourcesDto;

/**
 * Converter - Responsible for converting between ResourceDto and Resource types.
 */
@Component
public class ResourceConverter extends AbstractConverter {

  /**
   * Converts a Resource to ResourceDto.
   * 
   * @param resource to convert
   * 
   * @return {@link ResourceDto}
   */
  public ResourceDto convert(Resource resource) {

    ResourceDto resourceDto = new ResourceDto();
    resourceDto.setId(resource.getId());
    resourceDto.setTitle(resource.getTitle());
    resourceDto.setDescription(resource.getDescription());

    if (resource.isFile()) {
      ResourceFileDto resourceFileDto = new ResourceFileDto();
      resourceFileDto.setHref(resource.getUrl());
      resourceFileDto.setMimeType(resource.getMimeType());
      resourceFileDto.setSize(resource.getSize());
      resourceFileDto.setHosted(resource.getHosted().name().toLowerCase());
      resourceDto.setFile(resourceFileDto);
    }
    
    if (resource.isLink()) {
      ResourceLinkDto resourceLinkDto = new ResourceLinkDto();
      resourceLinkDto.setHref(resource.getUrl());
      resourceDto.setLink(resourceLinkDto);
    }

    return resourceDto;
  }

  /**
   * Converts a ResourceDto to Resource for a create.
   * 
   * @param resourceDto to convert
   * 
   * @return {@link Resource}
   */
  public Resource convertForCreate(ResourceDto resourceDto) {

    String title = convertEmptyStringToNull(resourceDto.getTitle());
    String description = convertEmptyStringToNull(resourceDto.getDescription());

    Resource resource = new Resource();
    resource.setTitle(title);
    resource.setDescription(description);

    if (resourceDto.hasFile()) {
      resource.setType(Resource.Type.FILE);

      String url = convertEmptyStringToNull(resourceDto.getFile().getHref());
      String mimeType = convertEmptyStringToNull(resourceDto.getFile().getMimeType());

      resource.setUrl(url);
      resource.setMimeType(mimeType);
      resource.setSize(resourceDto.getFile().getSize());

      if (resourceDto.isHostedExternal()) {
        resource.setHosted(Resource.Hosted.EXTERNAL);
      } else if (resourceDto.isHostedInternal()) {
        resource.setHosted(Resource.Hosted.INTERNAL);
      }
    }

    if (resourceDto.hasLink()) {
      resource.setType(Resource.Type.LINK);
      
      String url = convertEmptyStringToNull(resourceDto.getLink().getHref());
      resource.setUrl(url);
    }

    return resource;
  }

  /**
   * Converts a ResourceDto to Resource on update.
   * 
   * @param resourceDto to convert
   * 
   * @return {@link Resource}
   */
  public Resource convertForUpdate(ResourceDto resourceDto) {

    String title = convertStringToDeleteIndicator(resourceDto.getTitle());
    String description = convertStringToDeleteIndicator(resourceDto.getDescription());

    Resource resource = new Resource();
    resource.setId(resourceDto.getId());
    resource.setTitle(title);
    resource.setDescription(description);

    if (resourceDto.hasFile()) {

      String url = convertStringToDeleteIndicator(resourceDto.getFile().getHref());
      String mimeType = convertStringToDeleteIndicator(resourceDto.getFile().getMimeType());

      resource.setType(Resource.Type.FILE);
      resource.setUrl(url);
      resource.setMimeType(mimeType);
      resource.setSize(resourceDto.getFile().getSize());

      if (resourceDto.isHostedExternal()) {
        resource.setHosted(Resource.Hosted.EXTERNAL);
      } else if (resourceDto.isHostedInternal()) {
        resource.setHosted(Resource.Hosted.INTERNAL);
      }
    }

    if (resourceDto.hasLink()) {
      String url = convertStringToDeleteIndicator(resourceDto.getLink().getHref());

      resource.setUrl(url);
      resource.setType(Resource.Type.LINK);
    }

    return resource;
  }

  /**
   * Converts a list of resources to a ResourcesDto.
   * 
   * @param resources - collection of resources to convert
   * 
   * @return {@link ResourcesDto} or empty object when the provided list being null or empty
   */
  public ResourcesDto convertResources(List<Resource> resources) {

    ResourcesDto resourcesDto = new ResourcesDto();
    resourcesDto.setResources(new ArrayList<ResourceDto>());

    if (resources != null) {
      for (Resource resource : resources) {
        resourcesDto.getResources().add(this.convert(resource));
      }
    }

    return resourcesDto;
  }

  /**
   * Converts a ResourcesDto to list of resources.
   * 
   * @param resourcesDto - object DTO to convert
   * 
   * @return collection of Resource
   */
  public List<Resource> convertResources(ResourcesDto resourcesDto) {

    List<Resource> resources = new ArrayList<Resource>();

    if (resourcesDto != null && resourcesDto.getResources() != null) {
      for (ResourceDto resourceDto : resourcesDto.getResources()) {
        resources.add(this.convertForUpdate(resourceDto));
      }
    }

    return resources;
  } 

}