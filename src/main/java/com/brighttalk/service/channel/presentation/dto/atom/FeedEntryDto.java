/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeedEntryDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.atom;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.brighttalk.service.channel.presentation.dto.LinkDto;

/**
 * FeedEntryDto.
 */
@XmlType( propOrder = { "id", "updated", "title", "author", 
    "summary", "communication", "channel", "featured", "status", 
    "format", "duration", "start", "entryTime", "closeTime", "rating", "hidden", "categories", "links" } )
public class FeedEntryDto {

  private String id;
  
  private String title;
  
  private String status;

  private String updated;
  
  private String summary;
  
  private AuthorDto author;
  
  private CommunicationDto communication;
  
  private ChannelDto channel;
  
  private String format;
  
  private Integer duration;
  
  private long start;
  
  private Long entryTime;
  
  private Long closeTime;
  
  private Float rating;
  
  private List<LinkDto> links;
  
  private List<CategoryDto> categories;
  
  private Boolean hidden;
  
  private Boolean featured;
  
  
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getStatus() {
    return status;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setStatus(String status) {
    this.status = status;
  }

  public String getUpdated() {
    return updated;
  }

  public void setUpdated(String updated) {
    this.updated = updated;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public AuthorDto getAuthor() {
    return author;
  }

  public void setAuthor(AuthorDto author) {
    this.author = author;
  }

  public CommunicationDto getCommunication() {
    return communication;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setCommunication(CommunicationDto communication) {
    this.communication = communication;
  }

  public ChannelDto getChannel() {
    return channel;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setChannel(ChannelDto channel) {
    this.channel = channel;
  }

  public String getFormat() {
    return format;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setFormat(String format) {
    this.format = format;
  }

  public Integer getDuration() {
    return duration;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  public long getStart() {
    return start;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setStart(long start) {
    this.start = start;
  }

  public Long getEntryTime() {
    return entryTime;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setEntryTime(Long entryTime) {
    this.entryTime = entryTime;
  }

  public Long getCloseTime() {
    return closeTime;
  }
  
  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setCloseTime(Long value) {
    
    closeTime = value;
  }

  public Float getRating() {
    return rating;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setRating(Float rating) {
    this.rating = rating;
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  @XmlElement(name = "link")
  public void setLinks(List<LinkDto> links) {
    this.links = links;
  }

  public List<CategoryDto> getCategories() {
    return categories;
  }

  @XmlElement(name = "category")
  public void setCategories(List<CategoryDto> categories) {
    this.categories = categories;
  }

  public Boolean getHidden() {
    return hidden;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setHidden(Boolean hidden) {
    this.hidden = hidden;
  }

  public Boolean getFeatured() {
    return featured;
  }

  @XmlElement( namespace = "http://brighttalk.com/2009/atom_extensions" )
  public void setFeatured(Boolean featured) {
    this.featured = featured;
  }
  
}