/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsDto.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.common.validator.Validator;
import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Dto to hold the subscriptions details.
 */
public class SubscriptionsDto {

  private List<SubscriptionDto> subscriptions;

  public List<SubscriptionDto> getSubscriptions() {
    return subscriptions;
  }

  public void setSubscriptions(final List<SubscriptionDto> subscriptions) {
    this.subscriptions = subscriptions;
  }

  /**
   * Validates this dto with given validator.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<SubscriptionsDto> validator) {
    validator.validate(this);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("subscriptions", subscriptions);
    return builder.toString();
  }
}
