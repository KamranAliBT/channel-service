/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastRegistrationsDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.registrations;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.brighttalk.service.channel.presentation.dto.LinkDto;

/**
 * WebcastRegistrationsDto. Dto representing webcast registrations API body structure.
 */
@XmlRootElement(name = "webcastRegistrations")
public class WebcastRegistrationsDto {

  private Integer total;

  private List<LinkDto> links;

  private List<WebcastRegistrationDto> registrations;

  public Integer getTotal() {
    return total;
  }

  @XmlAttribute
  public void setTotal(Integer total) {
    this.total = total;
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  @XmlElement(name = "link")
  public void setLinks(List<LinkDto> links) {
    this.links = links;
  }

  public List<WebcastRegistrationDto> getRegistrations() {
    return registrations;
  }

  @XmlElement(name = "webcastRegistration")
  public void setRegistrations(List<WebcastRegistrationDto> registrations) {
    this.registrations = registrations;
  }
}