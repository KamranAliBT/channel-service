/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: InternalConverterContextBuilder.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

/**
 * Internal Builder Class used to construct instances of {@link com.brighttalk.service.channel.presentation.util.ConverterContext} Objects.
 */
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Internal Builder Class used to construct instances of ConverterContext Objects.
 */
@Component
public class InternalConverterContextBuilder extends AbstractConverterContextBuilder {

  /**
   * Internal service url.
   * <p>
   * i.e. http://channel.{environment}.brightalk.net/
   */
  @Value(value = "${internal.serviceUrl}")
  private String serviceUrl;

  /** {@inheritDoc} */
  @Override
  protected String getServiceUrl() {
    return serviceUrl;
  }
}
