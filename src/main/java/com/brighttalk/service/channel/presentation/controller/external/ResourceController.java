/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceController.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.controller.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.brighttalk.common.user.User;
import com.brighttalk.common.web.annotation.AuthenticatedUser;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.service.ResourceService;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.converter.ResourceConverter;
import com.brighttalk.service.channel.presentation.dto.validator.ResourceCreateValidator;
import com.brighttalk.service.channel.presentation.dto.validator.ResourceExternalUpdateValidator;

/**
 * Web presentation layer {@link org.springframework.web.servlet.mvc.Controller Controller} that handles external API
 * calls that perform operations on {@link Resource resource}.
 * <p>
 * Singleton. Controller handling methods must be thread-safe.
 * <p>
 * The resource can be found, created, edited and deleted only by an authenticated and authorized user who is the
 * channel owner, manager or presenter.
 */
@Controller
@RequestMapping(value = "/channel/{channelId}/communication/{communicationId}/resource")
public class ResourceController {

  /**
   * Resource service
   */
  @Autowired
  private ResourceService resourceService;
  
  /** Channel finder */
  @Autowired
  private ChannelFinder channelFinder;

  /**
   * Resource converter
   */
  @Autowired
  private ResourceConverter converter;

  /**
   * Handles a request to find a resource.
   * 
   * @param user the user making the request
   * @param channelId id of the channel
   * @param communicationId the communication to find the resource on
   * @param resourceId the resource id
   * 
   * @return resource
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the channel, communication or resource
   * cannot be found.
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the user is not authorized to access the
   * channel.
   */
  @RequestMapping(value = "/{resourceId}", method = RequestMethod.GET)
  @ResponseBody
  public ResourceDto find(@AuthenticatedUser User user, @PathVariable Long channelId,
                          @PathVariable Long communicationId, @PathVariable Long resourceId) {

    Channel channel = this.channelFinder.find(channelId);
    Resource resource = resourceService.findResource(user, channel, communicationId, resourceId);

    ResourceDto resourceDto = converter.convert(resource);
    return resourceDto;
  }

  /**
   * Handles a request to create resource.
   * 
   * @param user the user making the request
   * @param channelId id of the channel that the resource is to be created in
   * @param communicationId the communication to create the resource on
   * @param resourceDto the newly created resource
   * 
   * @return resource
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the channel or communication cannot be
   * found.
   * @throws com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException when the communication is LIVE
   * or in 15 minute window to go-live time. A resource cannot be created during this time.
   * @throws com.brighttalk.service.channel.business.error.MustBeInMasterChannelException when the communication is not
   * in a master channel.
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the user is not authorized to access the
   * channel.
   * @throws com.brighttalk.service.channel.business.error.InvalidResourceException when provided resourceDto is
   * syntactically invalid
   */
  @RequestMapping(method = RequestMethod.POST)
  @ResponseStatus(value = HttpStatus.CREATED)
  @ResponseBody
  public ResourceDto create(@AuthenticatedUser User user, @PathVariable Long channelId,
                            @PathVariable Long communicationId, @RequestBody ResourceDto resourceDto) {

    resourceDto.validate(new ResourceCreateValidator());
    Resource resource = converter.convertForCreate(resourceDto);

    Channel channel = this.channelFinder.find(channelId);
    Resource createdResource = resourceService.createResource(user, channel, communicationId, resource);

    ResourceDto resourceDtoToReturn = converter.convert(createdResource);
    return resourceDtoToReturn;
  }

  /**
   * Handles a request to edit resource.
   * 
   * @param user the user making the request
   * @param channelId id of the channel
   * @param communicationId the communication to update the resource on
   * @param resourceId id of the the resource to be updated
   * @param resourceDto the resource DTO to be updated
   * 
   * @return resource
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the channel, communication or resource
   * cannot be found.
   * @throws com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException when the communication is LIVE
   * or in 15 minute window to go-live time. A resource cannot be edited during this time.
   * @throws com.brighttalk.service.channel.business.error.MustBeInMasterChannelException when the communication is not
   * in a master channel.
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the user is not authorized to access the
   * channel.
   * @throws com.brighttalk.service.channel.business.error.InvalidResourceException when provided resourceDto is
   * syntactically invalid
   */
  @RequestMapping(value = "/{resourceId}", method = RequestMethod.PUT)
  @ResponseBody
  public ResourceDto edit(@AuthenticatedUser User user, @PathVariable Long channelId,
                          @PathVariable Long communicationId, @PathVariable Long resourceId,
      @RequestBody ResourceDto resourceDto) {

    resourceDto.validate(new ResourceExternalUpdateValidator(resourceId));
    Resource resource = converter.convertForUpdate(resourceDto);

    Channel channel = this.channelFinder.find(channelId);
    Resource resourceCreated = resourceService.updateResource(user, channel, communicationId, resource);

    ResourceDto resourceDtoToReturn = converter.convert(resourceCreated);
    return resourceDtoToReturn;
  }

  /**
   * Handles a request to delete resource.
   * 
   * @param user the user making the request
   * @param channelId id of the channel
   * @param communicationId the communication to delete the resource on
   * @param resourceId id of the the resource to be deleted
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the channel, communication or resource
   * cannot be found.
   * @throws com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException when the communication is LIVE
   * or in 15 minute window to go-live time. A resource cannot be deleted during this time.
   * @throws com.brighttalk.service.channel.business.error.MustBeInMasterChannelException when the communication is not
   * in a master channel.
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the user is not authorized to access the
   * channel.
   */
  @RequestMapping(value = "/{resourceId}", method = RequestMethod.DELETE)
  @ResponseBody
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void delete(@AuthenticatedUser User user, @PathVariable Long channelId, @PathVariable Long communicationId,
                     @PathVariable Long resourceId) {

    Channel channel = this.channelFinder.find(channelId);
    resourceService.deleteResource(user, channel, communicationId, resourceId);
  }

}
