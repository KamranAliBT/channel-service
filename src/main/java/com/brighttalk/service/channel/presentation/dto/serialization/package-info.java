/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: package-info.java 101522 2015-10-15 16:58:46Z kali $
 * ****************************************************************************
 */
/**
 * This package contains classes needed for serialization between the XML and business objects.
 */
package com.brighttalk.service.channel.presentation.dto.serialization;