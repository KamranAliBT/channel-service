/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsValidator.java 95153 2015-05-18 08:20:33Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.validator.Validator;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.error.SubscriptionRequestException;
import com.brighttalk.service.channel.presentation.dto.SubscriptionContextDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionDto;
import com.brighttalk.service.channel.presentation.dto.SubscriptionsDto;
import com.brighttalk.service.channel.presentation.dto.UserDto;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

/**
 * Validator to validate a {@link SubscriptionsDto Subscriptions DTO}.
 */
public abstract class SubscriptionsValidator implements Validator<SubscriptionsDto> {

  private static final String ERROR_MESSAGE_SUBSCRIPTION_INVALID_REFERRAL =
      "The referral is invalid for one or more subscription.";
  private static final String ERROR_MESSAGE_SUBSCRIPTION_MISSING_REFERRAL =
      "The referral is missing for one or more subscription.";
  private static final String ERROR_MESSAGE_SUBSCRIPTION_INVALID_USER_ID =
      "The user ID is invalid for one or more subscription.";
  private static final String ERROR_MESSAGE_SUBSCRIPTION_MISSING_USER =
      "The user ID or email is missing for one or more subscription.";
  private static final String ERROR_MESSAGE_SUBSCRIPTION_MAX_LIMIT =
      "The request body contains more than 100 subscriptions.";
  private static final String ERROR_MESSAGE_SUBSCRIPTION_MISSING =
      "The request body does not contain subscription element.";
  private static final String ERROR_MESSAGE_ONLY_IDS_OR_EMAILS =
      "A combination of user IDs and email addresses are not allowed.";
  private static final String ERROR_MESSAGE_MISSING_CONTEXT =
      "The subscription context is missing for one or more subscription.";
  private static final String ERROR_MESSAGE_MISSING_LEAD_TYPE =
      "The subscription context lead type is missing for one or more subscription.";
  private static final String ERROR_MESSAGE_MISSING_LEAD_CONTEXT =
      "The subscription context lead context is missing for one or more subscription.";
  private static final String ERROR_MESSAGE_MISSING_LEAD_ENGAGEMENT_SCORE =
      "The subscription context lead engagement scrore is missing for one or more subscription.";
  private static final String ERROR_MESSAGE_INVALID_LEAD_ENGAGEMENT_SCORE =
      "The subscription context lead engagement scrore is invalid integer for one or more subscription.";
  private static final String ERROR_MESSAGE_INVALID_LEAD_CONTEXT =
      "The subscription context lead context value contain invalid summit or webniar id for one or more subscription.";
  private static final String ERROR_MESSAGE_SUBSCRIPTION_MISSING_ID =
      "The subscription ID is missing for one or more subscription.";
  private static final String ERROR_MESSAGE_SUBSCRIPTION_INVALID_ID =
      "The subscription ID is invalid for one or more subscription.";
  private static final String ERROR_MESSAGE_DUPLICATE_USERS =
      "The request body contains duplicate subscriptions with same subscriber user details.";

  private static final String REFERRAL_REGEX = "paid";
  private static final Integer MAX_SUBSCRIPTIONS_PER_REQUEST = 100;

  /**
   * Assert the supplied subscriptions is not missing and it contain at least one subscription.
   * 
   * @param subscriptions The subscriptions to assert.
   * @throws SubscriptionRequestException in case of the supplied subscriptions is empty.
   */
  protected void assertNotEmptySubscriptions(final SubscriptionsDto subscriptions) {
    if (subscriptions == null || CollectionUtils.isEmpty(subscriptions.getSubscriptions())) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_SUBSCRIPTION_MISSING, ErrorCode.MISSING_SUBSCRIPTION);
    }
  }

  /**
   * Assert the supplied subscriptions count is not greater than expected.
   * 
   * @param subscriptions The subscriptions to assert.
   * @throws SubscriptionRequestException in case of the supplied subscriptions contain subscriptions with subscription
   * size greater than expected.
   */
  protected void assertMaxSubscriptionsPerRequest(final SubscriptionsDto subscriptions) {
    if (subscriptions.getSubscriptions().size() > MAX_SUBSCRIPTIONS_PER_REQUEST) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_SUBSCRIPTION_MAX_LIMIT, ErrorCode.SUBSCRIPTION_MAX_LIMIT);
    }
  }

  /**
   * Assert the supplied subscription contain.
   * 
   * @param subscriptions The subscriptions to assert.
   * @param contextRequired Flag indicating if subscription context is mandatory or optional.
   * @throws SubscriptionRequestException in case of the supplied subscriptions contain invalid contain.
   */
  protected void assertSubscriptions(final SubscriptionsDto subscriptions, boolean contextRequired) {
    boolean assertUserId = false;
    boolean assertUserEmail = false;
    List<UserDto> users = extractUsers(subscriptions.getSubscriptions());

    for (SubscriptionDto subscription : subscriptions.getSubscriptions()) {
      UserDto user = subscription.getUser();

      assertUser(user);

      assertUserId = assertUserId || user.getId() != null;
      assertUserEmail = assertUserEmail || StringUtils.isNotBlank(user.getEmail());

      if (assertUserId && assertUserEmail) {
        throw new SubscriptionRequestException(ERROR_MESSAGE_ONLY_IDS_OR_EMAILS,
            ErrorCode.SUBSCRIPTION_USER_ID_EMAIL_COMBINATION);
      } else if (assertUserId) {
        assertUserId(user);
      }

      // Assert if the requested subscriptions has duplicate user elements.
      if (users.contains(user)) {
        users.remove(user);
        if (users.contains(user)) {
          throw new SubscriptionRequestException(ERROR_MESSAGE_DUPLICATE_USERS, ErrorCode.DUPLICATE_RESOURCE);
        }
      }

      assertReferral(subscription);
      // Only Validate subscription context if it is set as mandatory and provided.
      if (contextRequired || subscription.getContext() != null) {
        assertContext(subscription);
      }
    }
  }

  private void assertUser(final UserDto user) {
    if (user == null || (user.getId() == null && StringUtils.isBlank(user.getEmail()))) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_SUBSCRIPTION_MISSING_USER,
          ErrorCode.MISSING_USER);
    }
  }

  private void assertUserId(final UserDto user) {
    try {
      Integer.parseInt(user.getId().toString());
    } catch (NumberFormatException nfe) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_SUBSCRIPTION_INVALID_USER_ID,
          ErrorCode.INVALID_USER_ID);
    }
  }

  /**
   * Assert the supplied subscription id is a valid integer.
   * 
   * @param subscription The subscription to validate.
   * @throws SubscriptionRequestException In case of the supplied subscription Id is not a valid integer.
   */
  protected void assertSubscriptionId(final SubscriptionDto subscription) {
    Long id = subscription.getId();
    if (id == null) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_SUBSCRIPTION_MISSING_ID,
          ErrorCode.MISSING_SUBSCRIPTION_ID);
    } else {
      try {
        Integer.parseInt(id.toString());
      } catch (NumberFormatException nfe) {
        throw new SubscriptionRequestException(ERROR_MESSAGE_SUBSCRIPTION_INVALID_ID,
            ErrorCode.INVALID_SUBSCRIPTION_ID);
      }
    }
  }

  /**
   * Assert the supplied subscription referral.
   * 
   * @param subscription The subscription to assert.
   * @throws SubscriptionRequestException in case of the supplied subscriptions contain invalid or missing referral.
   */
  protected void assertReferral(final SubscriptionDto subscription) {
    if (StringUtils.isBlank(subscription.getReferral())) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_SUBSCRIPTION_MISSING_REFERRAL,
          ErrorCode.MISSING_REFERRAL);
    }

    Pattern pattern = Pattern.compile(REFERRAL_REGEX);
    Matcher matcher = pattern.matcher(subscription.getReferral());

    if (!matcher.matches()) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_SUBSCRIPTION_INVALID_REFERRAL,
          ErrorCode.INVALID_REFERRAL);
    }
  }

  /**
   * Validate the supplied subscription context.
   * 
   * @param subscription The subscription to assert its context.
   * @throws SubscriptionRequestException in case of missing subscription context.
   */
  protected void assertContext(final SubscriptionDto subscription) {
    SubscriptionContextDto context = subscription.getContext();
    if (context == null) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_MISSING_CONTEXT, ErrorCode.MISSING_CONTEXT);
    } else {
      assertLeadType(context);
      assertLeadContext(context);
      assertEngagementScore(context);
    }
  }

  /**
   * Validate the supplied lead type.
   * 
   * @param context The context to validate.
   * @throws SubscriptionRequestException in case of invalid or missing subscription lead type.
   */
  private void assertLeadType(final SubscriptionContextDto context) {
    String contextLeadType = context.getLeadType();
    if (StringUtils.isBlank(contextLeadType)) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_MISSING_LEAD_TYPE, ErrorCode.MISSING_LEAD_TYPE);
    } else {
      SubscriptionLeadType.getType(contextLeadType);
    }
  }

  /**
   * Validate the supplied lead context. Lead context must be a valid integer when the context lead type is either
   * "summit" or "content".
   * 
   * @param context The context to validate.
   * @throws SubscriptionRequestException in case of invalid or missing subscription lead context.
   */
  private void assertLeadContext(final SubscriptionContextDto context) {
    String leadContext = context.getLeadContext();
    if (StringUtils.isBlank(leadContext)) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_MISSING_LEAD_CONTEXT, ErrorCode.MISSING_LEAD_CONTEXT);
    }
    // If lead type is not keyword, the lead context value, must be a valid integer.
    if (!context.isLeadTypeKeyword()) {
      try {
        Integer.parseInt(context.getLeadContext());
      } catch (NumberFormatException nfe) {
        throw new SubscriptionRequestException(ERROR_MESSAGE_INVALID_LEAD_CONTEXT, ErrorCode.INVALID_LEAD_CONTEXT);
      }
    }
  }

  /**
   * Validate the supplied engagement score.
   * <p>
   * Engagement score is optional for subscription context with "content" lead type and if provided it must between 0
   * and 100.
   * 
   * @param context The context to validate.
   * @throws SubscriptionRequestException in case of invalid or missing subscription lead context.
   */
  private void assertEngagementScore(final SubscriptionContextDto context) {
    String engagementScore = context.getEngagementScore();
    if (StringUtils.isBlank(engagementScore) && !context.isLeadTypeContent()) {
      throw new SubscriptionRequestException(ERROR_MESSAGE_MISSING_LEAD_ENGAGEMENT_SCORE,
          ErrorCode.MISSING_ENGAGEMENT_SCORE);
    }
    // Validates the supplied engagementScore is a valid integer.
    if (engagementScore != null) {
      try {
        Integer.parseInt(engagementScore);
      } catch (NumberFormatException nfe) {
        throw new SubscriptionRequestException(ERROR_MESSAGE_INVALID_LEAD_ENGAGEMENT_SCORE,
            ErrorCode.INVALID_ENGAGEMENT_SCORE);
      }
    }
  }

  /**
   * Extract user from subscriptions DTO.
   * 
   * @param subscriptions The subscriptions.
   * 
   * @return List of channel subscriptions users
   */
  private List<UserDto> extractUsers(final List<SubscriptionDto> subscriptions) {
    List<UserDto> users = new ArrayList<>();
    for (SubscriptionDto subscription : subscriptions) {
      users.add(subscription.getUser());
    }
    return users;
  }
}