/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContractPeriodsDto.java 100999 2015-10-05 13:53:18Z kali $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * ContractPeriodsDto. DTO object representing a {@link List} of {@link ContractPeriodDto} by wrapping a list of them.
 */
public class ContractPeriodsDto {

  private List<ContractPeriodDto> contractPeriods;

  /**
   * @return the collection of {@link ContractPeriodDto} objects or empty collection if this.channels is null.
   */
  public List<ContractPeriodDto> getContractPeriods() {
    if (contractPeriods == null) {
      contractPeriods = new ArrayList<>();
    }
    return contractPeriods;
  }

  /**
   * Sets contractPeriods property. Requires @XmlElement to make sure that list of contractPeriod elements will be
   * returned. In case the @XmlElement is not set correctly <contractPeriods><contractPeriodDto> will be returned.
   *
   * @param contractPeriods list of ContractPeriodDto
   */
  public void setContractPeriods(List<ContractPeriodDto> contractPeriods) {
    this.contractPeriods = contractPeriods;
  }
}
