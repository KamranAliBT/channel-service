/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AbstractConverter.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import com.brighttalk.service.channel.business.domain.Entity;

/**
 *  Base class for all Converters. Containing helper methods for conversion:
 *   from empty string to delete indicator
 *   from empty string to null 
 */
public abstract class AbstractConverter {

  /**
   * Converts the provided value to DELETE_INDICATOR when the provided value is an empty string
   * Or just returns the provided value.
   * 
   * If a dto is being used for update and its value is an empty string, that means we want it deleted
   * from the Database. Therefore we set it to this special value called DELETE_INDICATOR in the biz object as we do the 
   * conversion from dto to biz object. 
   * 
   * @param value to be converted
   * @return value or the DELETE_INDICATOR.
   */
  protected String convertStringToDeleteIndicator(String value) {
    if (value != null && value.isEmpty()) {
      return Entity.DELETE_INDICATOR;
    }
    return value;
  }

  /**
   * Converts the provided value to null when the provided value equals is an empty string.
   * Or just returns the provided value. 
   * 
   * We don't want empty strings in the database, we want nulls. So if we can an empty string from the the API
   * we nullify it using this method as part of converting a Dto to a Biz Object. 
   * 
   * @param value to be converted
   * @return value or the null
   */
  protected String convertEmptyStringToNull(String value) {
    if (value != null && value.isEmpty()) {
      return null;
    }
    return value;
  }
  
}
