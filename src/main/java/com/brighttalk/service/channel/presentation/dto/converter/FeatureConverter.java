/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: FeatureConverter.java 91950 2015-03-19 10:30:29Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.presentation.dto.FeatureDto;
import com.brighttalk.service.channel.presentation.error.UnsupportedFeatureException;

/**
 * Converter responsible for converting {@link Feature} types.
 */
@Component
public class FeatureConverter {

  /**
   * Converts a list of {@link Feature} to {@link FeatureDto}.
   * 
   * @param features a collection of features to convert
   * 
   * @return {@link FeatureDto} or empty object when the provided list being null or empty
   */
  public List<FeatureDto> convertFeatures(final List<Feature> features) {

    List<FeatureDto> featuresDto = new ArrayList<FeatureDto>();

    if (CollectionUtils.isEmpty(features)) {
      return featuresDto;
    }

    for (Feature feature : features) {
      if (Feature.Type.UNKNOWN.equals(feature.getName())) {
        continue;
      }

      try {
        FeatureDto featureDto = convertFeature(feature);
        featuresDto.add(featureDto);
      } catch (UnsupportedFeatureException ufe) {
        continue;
      }
    }

    return featuresDto;
  }

  /**
   * Converts a {@link Feature} to {@link FeatureDto}.
   * 
   * @param feature to convert
   * 
   * @return {@link FeatureDto}
   * 
   * @throws UnsupportedFeatureException Thrown when attempting to convert an unknown feature
   */
  private FeatureDto convertFeature(final Feature feature) {

    FeatureDto featureDto = new FeatureDto(feature.getName().getApiName());
    featureDto.setEnabled(feature.isEnabled());
    featureDto.setValue(feature.getValue());

    return featureDto;
  }

  /**
   * Converts a list of {@link FeatureDto} to {@link Feature}.
   * 
   * @param featuresDto a collection of features to convert
   * 
   * @return {@link Feature} or empty object when the provided list being null or empty
   * 
   * @throws UnsupportedFeatureException Thrown when attempting to convert an unknown feature
   */
  public List<Feature> convertFeaturesDto(final List<FeatureDto> featuresDto) {

    List<Feature> features = new ArrayList<Feature>();

    if (CollectionUtils.isEmpty(featuresDto)) {
      return features;
    }

    for (FeatureDto featureDto : featuresDto) {
      Feature feature = convertFeatureDto(featureDto);
      features.add(feature);
    }

    return features;
  }

  /**
   * Converts a {@link FeatureDto} to {@link Feature}.
   * 
   * @param feature to convert
   * 
   * @return {@link Feature}
   * 
   * @throws UnsupportedFeatureException Thrown when attempting to convert an unknown feature
   */
  private Feature convertFeatureDto(final FeatureDto featureDto) {

    Feature feature = new Feature(Feature.Type.convertApiName(featureDto.getName()));
    feature.setIsEnabled(Boolean.valueOf(featureDto.getEnabled()));
    feature.setValue(featureDto.getValue());

    return feature;
  }

}
