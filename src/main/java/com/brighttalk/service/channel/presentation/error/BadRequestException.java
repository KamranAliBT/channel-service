/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: BadRequestException.java 75656 2014-03-25 12:24:02Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Exception thrown to report that a request could not be processed because it was invalid either in its format or data
 * content / value(s). The exception has a mandatory error code for identifying the specific reason which caused the
 * request to be rejected, for the purposes of reporting the error back to the API client.
 */
@SuppressWarnings("serial")
public class BadRequestException extends ApplicationException {

  /** Bad request exception error code. */
  public static final String BAD_REQUEST_EXCEPTION_ERROR_CODE = "BadRequestException";

  /**
   * @param message The exception error message.
   */
  public BadRequestException(final String message) {
    super(message);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @see ApplicationException#ApplicationException(String, String)
   */
  public BadRequestException(final String message, final String errorCode) {
    super(message, errorCode);
    this.setUserErrorMessage(message);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @param errorArgs An array of objects to be used to replace placeholders/tokens in the user error message which is
   * resolved using the {@code userErrorCode}.
   * 
   * @see ApplicationException#ApplicationException(String, String)
   */
  public BadRequestException(final String message, final String errorCode, final Object... errorArgs) {
    super(message, errorCode, errorArgs);
    this.setUserErrorMessage(message);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @param errorArgs An array of objects to be used to replace placeholders/tokens in the user error message which is
   * resolved using the {@code userErrorCode}.
   * 
   * @param cause Throwable providing additional info about the context in which the error occurred. Can be null.
   * 
   * @see ApplicationException#ApplicationException(String, String)
   */
  public BadRequestException(final String message, final String errorCode, final Throwable cause,
      final Object... errorArgs) {
    super(message, errorCode, errorArgs, cause);
    this.setUserErrorMessage(message);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @see ApplicationException#ApplicationException(String, String)
   */
  public BadRequestException(final String message, final ErrorCode errorCode) {
    super(message, errorCode.getName());
    this.setUserErrorMessage(message);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @param errorArgs An array of objects to be used to replace placeholders/tokens in the user error message which is
   * resolved using the {@code userErrorCode}.
   * 
   * @see ApplicationException#ApplicationException(String, String)
   */
  public BadRequestException(final String message, final ErrorCode errorCode, final Object... errorArgs) {
    super(message, errorCode.getName(), errorArgs);
    this.setUserErrorMessage(message);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @param errorArgs An array of objects to be used to replace placeholders/tokens in the user error message which is
   * resolved using the {@code userErrorCode}.
   * 
   * @param cause Throwable providing additional info about the context in which the error occurred. Can be null.
   * 
   * @see ApplicationException#ApplicationException(String, String)
   */
  public BadRequestException(final String message, final ErrorCode errorCode, final Throwable cause,
      final Object... errorArgs) {
    super(message, errorCode.getName(), errorArgs, cause);
    this.setUserErrorMessage(message);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param cause Throwable providing additional info about the context in which the error occurred. Can be null.
   * 
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public BadRequestException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
