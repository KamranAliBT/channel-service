/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CategoryDto.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Category Dto. DTO object representing channel category.
 */
@XmlRootElement(name = "feature")
public class CategoryDto {

  private Long id;

  private String term;

  /**
   * Creates new CategoryDto with provided id.
   * 
   * @param id of a category
   */
  public CategoryDto(Long id) {
    this.id = id;
  }

  /**
   * Constructs category Dto object as a new object.
   */
  public CategoryDto() {
  }

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(Long id) {
    this.id = id;
  }

  public String getTerm() {
    return term;
  }

  @XmlAttribute
  public void setTerm(String term) {
    this.term = term;
  }

}
