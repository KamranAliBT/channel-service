/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelPublicDto.java 85869 2014-11-11 11:17:11Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * ChannelDto. DTO object representing channel.
 */
@XmlRootElement(name = "channel")
public class ChannelPublicDto {

  private Long id;

  private ChannelOwnerDto channelOwnerDto;

  private String title;

  private String description;

  private String strapline;

  private String searchVisibility;

  private String keywords;

  private String organisation;

  private String url;

  private LinkDto link;

  private StatisticsDto statistics;

  private Float rating;

  private List<FeaturedWebcastDto> featuredWebcasts;

  private List<FeatureDto> features;

  /**
   * Creates this dto.
   * 
   * @param id of a channel
   */
  public ChannelPublicDto(final Long id) {
    this.id = id;
  }

  /**
   * Constructs channel Dto object as a new object.
   */
  public ChannelPublicDto() {
  }

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(final Long id) {
    this.id = id;
  }

  public ChannelOwnerDto getChannelOwner() {
    return channelOwnerDto;
  }

  public void setChannelOwner(final ChannelOwnerDto channelOwnerDto) {
    this.channelOwnerDto = channelOwnerDto;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getStrapline() {
    return strapline;
  }

  public void setStrapline(final String strapline) {
    this.strapline = strapline;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(final String keywords) {
    this.keywords = keywords;
  }

  public String getOrganisation() {
    return organisation;
  }

  public void setOrganisation(final String organisation) {
    this.organisation = organisation;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public String getSearchVisibility() {
    return searchVisibility;
  }

  public void setSearchVisibility(final String searchVisibility) {
    this.searchVisibility = searchVisibility;
  }

  public LinkDto getLink() {
    return link;
  }

  public void setLink(final LinkDto link) {
    this.link = link;
  }

  public Float getRating() {
    return rating;
  }

  public void setRating(final Float rating) {
    this.rating = rating;
  }

  public StatisticsDto getStatistics() {
    return statistics;
  }

  public void setStatistics(final StatisticsDto statistics) {
    this.statistics = statistics;
  }

  public List<FeaturedWebcastDto> getFeaturedWebcasts() {
    return featuredWebcasts;
  }

  @XmlElementWrapper(name = "featuredWebcasts")
  @XmlElement(name = "webcast")
  public void setFeaturedWebcasts(final List<FeaturedWebcastDto> featuredWebcasts) {
    this.featuredWebcasts = featuredWebcasts;
  }

  public List<FeatureDto> getFeatures() {
    return features;
  }

  @XmlElementWrapper(name = "features")
  @XmlElement(name = "feature")
  public void setFeatures(final List<FeatureDto> features) {
    this.features = features;
  }

}
