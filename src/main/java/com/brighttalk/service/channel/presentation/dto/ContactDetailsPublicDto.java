/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ContactDetailsPublicDto.java 88863 2015-01-27 11:35:19Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.brighttalk.service.channel.presentation.dto.validation.scenario.UpdateContactDetails;


/**
 * Public representation of a {@link com.brighttalk.service.channel.business.domain.ContactDetails} object.
 */
public class ContactDetailsPublicDto {
  
  private static final int MAX_LENGTH_FIRST_NAME = 128;
  private static final int MAX_LENGTH_LAST_NAME = 128;
  private static final int MAX_LENGTH_EMAIL = 128;
  private static final int MAX_LENGTH_TELEPHONE = 128;
  private static final int MAX_LENGTH_JOB_TITLE = 128;
  private static final int MAX_LENGTH_COMPANY_NAME = 128;
  private static final int MAX_LENGTH_ADDRESS_1 = 128;
  private static final int MAX_LENGTH_ADDRESS_2 = 128;
  private static final int MAX_LENGTH_CITY = 128;
  private static final int MAX_LENGTH_STATE_REGION = 128;
  private static final int MAX_LENGTH_POSTCODE = 128;
  private static final int MAX_LENGTH_COUNTRY = 128;  

  @NotBlank(groups = { UpdateContactDetails.class }, message = "{FirstNameEmpty}")
  @Size(max = MAX_LENGTH_FIRST_NAME, groups = { UpdateContactDetails.class }, message = "{FirstNameMaxLength}")
  private String firstName;
  
  @NotBlank(groups = { UpdateContactDetails.class }, message = "{LastNameEmpty}")
  @Size(max = MAX_LENGTH_LAST_NAME, groups = { UpdateContactDetails.class }, message = "{LastNameMaxLength}")
  private String lastName;
  
  @NotBlank(groups = { UpdateContactDetails.class }, message = "{EmailEmpty}")
  @Size(max = MAX_LENGTH_EMAIL, groups = { UpdateContactDetails.class }, message = "{EmailMaxLength}")
  private String email;
  
  @NotBlank(groups = { UpdateContactDetails.class }, message = "{TelephoneEmpty}")
  @Size(max = MAX_LENGTH_TELEPHONE, groups = { UpdateContactDetails.class }, message = "{TelephoneMaxLength}")
  private String telephone;
  
  @NotBlank(groups = { UpdateContactDetails.class }, message = "{JobTitleEmpty}")
  @Size(max = MAX_LENGTH_JOB_TITLE, groups = { UpdateContactDetails.class }, message = "{JobTitleMaxLength}")
  private String jobTitle;
  
  @NotBlank(groups = { UpdateContactDetails.class }, message = "{CompanyNameEmpty}")
  @Size(max = MAX_LENGTH_COMPANY_NAME, groups = { UpdateContactDetails.class }, message = "{CompanyNameMaxLength}")
  private String companyName;
  
  @NotBlank(groups = { UpdateContactDetails.class }, message = "{Address1Empty}")
  @Size(max = MAX_LENGTH_ADDRESS_1, groups = { UpdateContactDetails.class }, message = "{Address1MaxLength}")
  private String address1;
  
  @Size(max = MAX_LENGTH_ADDRESS_2, groups = { UpdateContactDetails.class }, message = "{Address2MaxLength}")
  private String address2;
  
  @NotBlank(groups = { UpdateContactDetails.class }, message = "{CityEmpty}")
  @Size(max = MAX_LENGTH_CITY, groups = { UpdateContactDetails.class }, message = "{CityMaxLength}")
  private String city;
  
  @Size(max = MAX_LENGTH_STATE_REGION, groups = { UpdateContactDetails.class }, message = "{StateRegionMaxLength}")
  private String stateRegion;
  
  @NotBlank(groups = { UpdateContactDetails.class }, message = "{PostcodeEmpty}")
  @Size(max = MAX_LENGTH_POSTCODE, groups = { UpdateContactDetails.class }, message = "{PostcodeMaxLength}")
  private String postcode;
  
  @NotBlank(groups = { UpdateContactDetails.class }, message = "{CountryEmpty}")
  @Size(max = MAX_LENGTH_COUNTRY, groups = { UpdateContactDetails.class }, message = "{CountryMaxLength}")
  private String country;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getJobTitle() {
    return jobTitle;
  }

  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getStateRegion() {
    return stateRegion;
  }

  public void setStateRegion(String state) {
    this.stateRegion = state;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }
}