/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: BasicWebcastScenario.java 70202 2013-10-24 17:31:29Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validation.scenario;

/**
 * Validation group type used every time .
 */
public interface BasicWebcastScenario {

}
