/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeedCommunicationDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.rss;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * FeedCommunicationDto.
 */
public class FeedCommunicationDto {

  private Long id;
  
  private String status;
  
  private long utc;
  
  private int duration;
  
  private float rating;
  
  private String format;
  
  private String thumbnailUrl;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(Long id) {
    this.id = id;
  }

  public String getStatus() {
    return status;
  }

  @XmlAttribute
  public void setStatus(String status) {
    this.status = status;
  }

  public long getUtc() {
    return utc;
  }

  @XmlAttribute
  public void setUtc(long utc) {
    this.utc = utc;
  }

  public int getDuration() {
    return duration;
  }

  @XmlAttribute
  public void setDuration(int duration) {
    this.duration = duration;
  }

  public float getRating() {
    return rating;
  }

  @XmlAttribute
  public void setRating(float rating) {
    this.rating = rating;
  }

  public String getFormat() {
    return format;
  }

  @XmlAttribute
  public void setFormat(String format) {
    this.format = format;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  @XmlAttribute( name = "thumbnail_url" )
  public void setThumbnailUrl(String value) {
    
    thumbnailUrl = value;
  }
}