/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceDto.java 97153 2015-06-25 15:15:16Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.brighttalk.service.channel.business.domain.validator.Validator;

/**
 * DTO object representing resource.
 * 
 * Resource Dto can represent either a link resource or a file resource.
 */
@XmlRootElement(name = "resource")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResourceDto {

  @XmlAttribute
  private Long id;
  private String title;
  private String description;
  @XmlElementRef
  private ResourceFileDto file;
  @XmlElementRef
  private ResourceLinkDto link;

  @XmlAttribute
  private String type;

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public ResourceFileDto getFile() {
    return file;
  }

  public void setFile(final ResourceFileDto file) {
    this.file = file;
  }

  public ResourceLinkDto getLink() {
    return link;
  }

  public void setLink(final ResourceLinkDto link) {
    this.link = link;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(final String type) {
    this.type = type;
  }

  /**
   * Indicates whether the resourceDto represents a file resource.
   * 
   * @return true or false
   */
  public boolean hasFile() {
    return file != null;
  }

  /**
   * Indicates whether the resourceDto represents a link resource.
   * 
   * @return true or false
   */
  public boolean hasLink() {
    return link != null;
  }

  /**
   * Indicates whether the resourceDto is hosted externally.
   * 
   * Note: @JsonIgnore is used to make sure that Json marshalling is not creating an additional property based on this
   * method.
   * 
   * @return true or false
   */
  @JsonIgnore
  public boolean isHostedExternal() {
    return "external".equals(this.getFile().getHosted());
  }

  /**
   * Indicates whether the resourceDto is hosted internally.
   * 
   * Note: @JsonIgnore is used to make sure that Json marshalling is not creating an additional property based on this
   * method.
   * 
   * @return true or false
   */
  @JsonIgnore
  public boolean isHostedInternal() {
    return "internal".equals(this.getFile().getHosted());
  }

  /**
   * Validates this dto with given validator.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<ResourceDto> validator) {
    validator.validate(this);
  }
}
