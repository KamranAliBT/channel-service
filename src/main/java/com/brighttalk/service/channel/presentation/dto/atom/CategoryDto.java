/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CategoryDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.atom;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * CategoryDto.
 */
public class CategoryDto {

  /**
   * Category schema for communication's keywords.
   */
  public static final String CATEGORY_SCHEMA_KEYWORD = "keyword";
  
  /**
   * Category schema for communication's categories.
   */
  public static final String CATEGORY_SCHEMA_CATEGORY = "category";
  
  
  private String scheme;
  
  private String term;

  public String getScheme() {
    return scheme;
  }

  @XmlAttribute
  public void setScheme(String scheme) {
    this.scheme = scheme;
  }

  public String getTerm() {
    return term;
  }

  @XmlAttribute
  public void setTerm(String term) {
    this.term = term;
  }
}
