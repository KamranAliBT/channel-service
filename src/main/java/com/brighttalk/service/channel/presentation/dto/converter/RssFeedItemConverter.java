/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RssFeedItemConverter.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.converter.util.ThreadSafeSimpleDateFormat;
import com.brighttalk.service.channel.presentation.dto.rss.EnclosureDto;
import com.brighttalk.service.channel.presentation.dto.rss.FeedCommunicationDto;
import com.brighttalk.service.channel.presentation.dto.rss.FeedItemDto;
import com.brighttalk.service.channel.presentation.dto.rss.GuidDto;
import com.brighttalk.service.channel.presentation.dto.rss.ItemIdDto;

/**
 * Converter - Responsible for converting between FeedItemDto and business type.
 */
@Component
public class RssFeedItemConverter {

  /**
   * The date pattern used in the last updated nodes of the feed.
   */
  public static final String PUBLISHED_DATE_PATTERN = "EEE, d MMM yyyy HH:mm:ss Z";
  
  /**
   * Constant for enclosure length. 
   */
  protected static final int IMAGE_LENGTH_DEFAULT = 8;
  
  private static final long ONE_SECOND = 1000;
  
  /**
   * Converts a single communication into an item dto. 
   * 
   * @param communication details
   * 
   * @return {@link FeedItemDto}
   */
  public FeedItemDto convert( Communication communication ) {
    
    FeedItemDto itemDto = new FeedItemDto();
      
    ItemIdDto itemIdDto = new ItemIdDto();
    itemIdDto.setId( communication.getId() );
    itemDto.setItemId( itemIdDto );
    
    itemDto.setTitle( communication.getTitle() );
    itemDto.setDescription( communication.getDescription() );
    itemDto.setPresenter( communication.getAuthors() );
    itemDto.setLink( communication.getUrl() );
    
    ThreadSafeSimpleDateFormat format = new ThreadSafeSimpleDateFormat( PUBLISHED_DATE_PATTERN );
    String pubDateString = format.format( communication.getScheduledDateTime() );
    itemDto.setPubDate( pubDateString );
    
    GuidDto guidDto = new GuidDto();
    guidDto.setIsPermaLink( true ); 
    guidDto.setValue( communication.getUrl() );
    itemDto.setGuid( guidDto );
    
    FeedCommunicationDto communicationDto = new FeedCommunicationDto();
    communicationDto.setId( communication.getId() );
    communicationDto.setDuration( communication.getDuration() );
    communicationDto.setRating( communication.getCommunicationStatistics().getFormattedRating() );
    communicationDto.setStatus( communication.getStatus().toString().toLowerCase() );
    communicationDto.setThumbnailUrl( communication.hasThumbnailUrl() ? communication.getThumbnailUrl() : null );
    communicationDto.setUtc( communication.getScheduledDateTime().getTime() / ONE_SECOND );
    communicationDto.setFormat( communication.getFormat().toString().toLowerCase() );
    itemDto.setCommunication( communicationDto );
    
    if ( communication.isUpcoming() ) {
      
      itemDto.setCalendar( communication.getCalendarUrl() );
    }
    
    if ( communication.hasThumbnailUrl() ) {
      
      EnclosureDto enclosureDto = new EnclosureDto();
      enclosureDto.setUrl( communication.getThumbnailUrl() );
      enclosureDto.setLength( IMAGE_LENGTH_DEFAULT );
      enclosureDto.setType( LinkDto.TYPE_IMAGE_PNG );
      
      itemDto.setEnclosure( enclosureDto );
    }
    
    if ( communication.getKeywords() != null ) {

      List<String> categories = new ArrayList<String>();
      String[] keywords = communication.getKeywords().split(",");
      
      for ( String keyword : keywords ) {
        
        categories.add( keyword.trim() );
      }
      itemDto.setCategories( categories );
    }
    
    return itemDto;
  }
}
