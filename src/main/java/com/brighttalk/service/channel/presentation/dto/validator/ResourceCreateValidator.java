/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceCreateValidator.java 84340 2014-10-07 17:03:10Z ssarraj $ 
 * *****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validator;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.error.InvalidResourceException;
import com.brighttalk.service.channel.presentation.dto.ResourceDto;
import com.brighttalk.service.channel.presentation.dto.ResourceFileDto;
import com.brighttalk.service.channel.presentation.dto.ResourceLinkDto;

/**
 * Syntatical Presentation Layer Validator to validate a resource for creation.
 */
@Component
public class ResourceCreateValidator extends AbstractResourceValidator {

  /**
   * Validate a resource for creation.
   * 
   * title is mandatory and must be not greater than.... description is optional and must not be greater than...
   * 
   * @param resourceDto to be validated.
   * 
   * @throws InvalidResourceException if the resource type is unknown.
   */
  @Override
  public void validate(final ResourceDto resourceDto) {

    assertTitle(resourceDto);
    assertDescription(resourceDto);
    assertType(resourceDto);

    if (resourceDto.hasFile()) {

      ResourceFileDto file = resourceDto.getFile();

      assertFileHosted(file);

      if (resourceDto.isHostedInternal()) {

        assertFileDetailsAreEmpty(file);

      } else if (resourceDto.isHostedExternal()) {

        assertFileDetails(file);
      }

    } else if (resourceDto.hasLink()) {

      assertLinkDetails(resourceDto.getLink());
    } else {

      throw new InvalidResourceException(ERRORCODE_RESOURCE_TYPE);
    }

  }

  /**
   * Assert the file details are syntactically allowed.
   * 
   * @param ResourceFileDto the resource file to be validated.
   * 
   * @throws InvalidResourceException if the resource cannot be validated.
   */
  private void assertFileDetails(final ResourceFileDto file) {

    assertUrl(file.getHref());
    assertMimeType(file.getMimeType());

  }

  /**
   * Assert there are no details set on a file object.
   * 
   * @param resourceFileDto the file to be validated.
   * 
   * @throws InvalidResourceException if the resource cannot be validated.
   */
  private void assertFileDetailsAreEmpty(final ResourceFileDto file) {

    // not allowed fields are url, size and mimetype
    if (file.hasHref()) {
      throw new InvalidResourceException(MESSAGE_RESOURCE_URL_NOT_ALLOWED, ERRORCODE_RESOURCE_URL_NOT_ALLOWED);
    }

    if (file.hasMimeType()) {
      throw new InvalidResourceException(MESSAGE_RESOURCE_MIME_TYPE_NOT_ALLOWED,
          ERRORCODE_RESOURCE_MIME_TYPE_NOT_ALLOWED);
    }

    if (file.hasSize()) {
      throw new InvalidResourceException(MESSAGE_RESOURCE_SIZE_NOT_ALLOWED, ERRORCODE_RESOURCE_SIZE_NOT_ALLOWED);
    }

  }

  /**
   * Assert details of a link are syntactically correct.
   * 
   * @param ResourceLinkDto the link to be validated.
   * 
   * @throws InvalidResourceException if the resource cannot be validated.
   */
  private void assertLinkDetails(final ResourceLinkDto link) {

    // url is optional for an link resource not not delete able
    assertUrl(link.getHref());
  }

  /**
   * Asserts a title is an resource Dto title.
   * 
   * @param resourceDto the resource Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource title is missing or is too long.
   */
  private void assertTitle(final ResourceDto resourceDto) {
    assertMandatoryField(resourceDto.getTitle(), MESSAGE_RESOURCE_TITLE_MISSING, ERRORCODE_RESOURCE_TITLE_MISSING);
    assertMaxFieldLength(resourceDto.getTitle(), MAX_TITLE_LENGTH, MESSAGE_RESOURCE_TITLE_TOO_LONG,
        ERRORCODE_RESOURCE_TITLE_TOO_LONG);
  }

  /**
   * Asserts an resource Dto url.
   * 
   * @param resourceDto the resource Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource title is missing or is too long.
   */
  private void assertUrl(final String url) {

    assertMandatoryField(url, MESSAGE_RESOURCE_URL_MISSING, ERRORCODE_RESOURCE_URL_MISSING);
    assertMaxFieldLength(url, MAX_URL_LENGTH, "Resource URL is too long", "ResourceUrlTooLong");
    assertFieldAgainstRegex(url, URL_REGEX, MESSAGE_RESOURCE_URL_INVALID, ERRORCODE_RESOURCE_URL_INVALID);

  }

  /**
   * Asserts an resource Dto description.
   * 
   * @param resourceDto the resource Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource description is too long.
   */
  private void assertDescription(final ResourceDto resourceDto) {
    assertMaxFieldLength(resourceDto.getDescription(), MAX_DESCRIPTION_LENGTH, MESSAGE_RESOURCE_DESCRIPTION_TOO_LONG,
        ERRORCODE_RESOURCE_DESCRIPTION_TOO_LONG);
  }

  /**
   * Asserts an resource Dto mime type.
   * 
   * @param resourceDto the resource Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource mime type is missing or is too long.
   */
  private void assertMimeType(final String mimeType) {
    assertMaxFieldLength(mimeType, MAX_MIMETYPE_LENGTH, MESSAGE_RESOURCE_MIMETYPE_TOO_LONG,
        ERRORCODE_RESOURCE_MIMETYPE_TOO_LONG);
  }

  /**
   * Asserts an file has the hosted attribute set
   * 
   * @param resourceFileDto the resource file Dto to be validated.
   * 
   * @throws InvalidResourceException if the resource mime type is missing or is too long.
   */
  private void assertFileHosted(final ResourceFileDto file) {

    String hostedValue = file.getHosted();

    assertMandatoryField(hostedValue, ERRORCODE_RESOURCE_HOSTED_MISSING, MESSAGE_RESOURCE_HOSTED_MISSING);

    if (!"internal".equals(hostedValue) && !"external".equals(hostedValue)) {
      throw new InvalidResourceException(ERRORCODE_RESOURCE_HOSTED_MISSING, MESSAGE_RESOURCE_HOSTED_MISSING);
    }

  }
}
