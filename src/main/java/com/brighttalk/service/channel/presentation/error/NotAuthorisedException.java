/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2010.
 * All Rights Reserved.
 * $Id: NotAuthorisedException.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Exception thrown to signal that a requested operation failed because an authenticated API user did not have the
 * necessary permission. For example, a request to view a communication in a channel which does not belong to the user's
 * associated organisation.
 */
@SuppressWarnings("serial")
public class NotAuthorisedException extends ApplicationException {

  /**
   * @param message see below.
   * 
   * @param errorCode see below.
   * 
   * @param errorArgs see below.
   * 
   * @see #NotAuthorisedException(String, String, Throwable, Object[])
   */
  public NotAuthorisedException(final String message, final String errorCode, final Object... errorArgs) {
    this(message, errorCode, null, errorArgs);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @param errorArgs The array of objects to be used as parameters in the tokenised user error message that is resolved
   * for this exception. Can be null.
   * 
   * @param cause The {@link Throwable} which is the source of the original exception, which the created exception
   * should wrap. Can be null.
   * 
   * @see ApplicationException#ApplicationException(String, String, Object[], Throwable)
   */
  public NotAuthorisedException(final String message, final String errorCode, final Throwable cause,
      final Object... errorArgs) {
    super(message, errorCode, errorArgs, cause);
  }

  /**
   * @param message see below.
   * 
   * @param errorCode see below.
   * 
   * @param errorArgs see below.
   * 
   * @see #NotAuthorisedException(String, String, Throwable, Object[])
   */
  public NotAuthorisedException(final String message, final ErrorCode errorCode, final Object... errorArgs) {
    this(message, errorCode, null, errorArgs);
  }

  /**
   * @param message The detail message, for internal consumption.
   * 
   * @param errorCode The error code to be reported back to the end user (API client).
   * 
   * @param errorArgs The array of objects to be used as parameters in the tokenised user error message that is resolved
   * for this exception. Can be null.
   * 
   * @param cause The {@link Throwable} which is the source of the original exception, which the created exception
   * should wrap. Can be null.
   * 
   * @see ApplicationException#ApplicationException(String, String, Object[], Throwable)
   */
  public NotAuthorisedException(final String message, final ErrorCode errorCode, final Throwable cause,
      final Object... errorArgs) {
    super(message, errorCode.getName(), errorArgs, cause);
  }

}