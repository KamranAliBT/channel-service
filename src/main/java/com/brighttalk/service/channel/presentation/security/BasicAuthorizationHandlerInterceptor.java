/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: BasicAuthenticator.java 57384 2012-11-09 16:58:22Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.security;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.brighttalk.service.channel.integration.database.ClientCredentialsDbDao;
import com.brighttalk.service.channel.presentation.controller.external.ChannelController;
import com.brighttalk.service.channel.presentation.error.NotAuthenticatedException;

/**
 * Basic interceptor to handle the client authorization.
 * <p>
 * This interceptor is used only to authorize the SfChannel client. As soon as new clients need access to this service
 * the way we store the username and password should be redesigned.
 * <p>
 * The possibility of using the Spring Security model should also be evaluated. This current approach is the
 * easiest/quickest one to deliver this API to the SfChannel client.
 * <p>
 * The server is expecting the client to send the client username and password in the Authorisation header in the format
 * "Authorization: Basic U2ZDaGFubmVsOlRMR0twd0NQQjQ="
 */
public class BasicAuthorizationHandlerInterceptor extends HandlerInterceptorAdapter {

  /** Class logger. */
  protected static final Logger LOGGER = Logger.getLogger(ChannelController.class);

  private static final Map<String, String> excluedPaths;

  private static final String HTTP_HEADER_AUTHORIZATION = "Authorization";

  private static final String BASIC_AUTH_SCHEME_PREFIX = "Basic ";

  private final AntPathMatcher antPathMatcher = new AntPathMatcher();

  static {
    excluedPaths = new HashMap<>();
    excluedPaths.put("/channel/*/features", "PUT");
  }

  @Autowired
  private ClientCredentialsDbDao clientCredentialsDbDao;

  @Override
  public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
    throws Exception {

    logDebug("Received a request to authenticate a client.");

    if (isExcludedPath(request)) {
      logDebug("The path [%s] is excluded from the basic authentication.", request.getServletPath());
      return true;
    }

    String header = request.getHeader(HTTP_HEADER_AUTHORIZATION);

    logDebug("Authenticating client with authorization header [%s].", header);

    if (!isAuthorizationSchemeBasicAuth(header)) {
      throw new NotAuthenticatedException("The authorization scheme is invalid.");
    }

    byte[] base64Token = header.substring(6).getBytes(StandardCharsets.UTF_8);
    String token = new String(Base64.decodeBase64(base64Token));

    logDebug("Authenticating client with token [%s].", token);

    String tokens[] = token.split(":");

    if (tokens.length != 2) {
      throw new NotAuthenticatedException("The authorization token format is invalid.");
    }

    String username = tokens[0];
    String password = tokens[1];

    boolean clientExists = clientCredentialsDbDao.exists(username, password);

    if (!clientExists) {
      throw new NotAuthenticatedException("The client credentials are not valid.");
    }

    logDebug("Client authenticated.");

    return true;
  }

  private boolean isExcludedPath(final HttpServletRequest request) {
    String requestPath = request.getServletPath();
    String requestMethod = request.getMethod();

    Iterator<Entry<String, String>> it = excluedPaths.entrySet().iterator();

    while (it.hasNext()) {
      Entry<String, String> entry = it.next();
      String path = entry.getKey();
      String method = entry.getValue();

      if (antPathMatcher.match(path, requestPath) && method.equals(requestMethod)) {
        return true;
      }
    }

    return false;
  }

  private boolean isAuthorizationSchemeBasicAuth(final String headerValue) {
    return StringUtils.isNotEmpty(headerValue) && headerValue.startsWith(BASIC_AUTH_SCHEME_PREFIX);
  }

  private void logDebug(final String message, final Object... params) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(String.format(message, params));
    }
  }

}
