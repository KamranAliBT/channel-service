/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastConverter.java 75434 2014-03-19 14:40:14Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.ImageConverterSettings;
import com.brighttalk.service.channel.presentation.dto.TaskDto;

/**
 * Converter - Responsible for converting between {@link TaskDto} and {@link ImageConverterSettings}.
 */
@Component
public class TaskConverter {

  /**
   * Convert the given imageConverterSettings into its Webcast DTO.
   * 
   * @param imageConverterSettings the imageConverterSettings to convert
   * 
   * @return the imageConverterSettings DTO for that imageConverterSettings.
   */
  public TaskDto convert(final ImageConverterSettings imageConverterSettings) {

    if (imageConverterSettings == null) {
      return new TaskDto();
    }

    TaskDto taskDto = new TaskDto();
    taskDto.setSource(imageConverterSettings.getSource());
    taskDto.setDestination(imageConverterSettings.getDestination());
    taskDto.setNotify(imageConverterSettings.getNotify());
    taskDto.setFormat(imageConverterSettings.getFormat());
    taskDto.setWidth(imageConverterSettings.getWidth());
    taskDto.setHeight(imageConverterSettings.getHeight());
    taskDto.setScaleMode(imageConverterSettings.getScaleMode());
    taskDto.setStatus(imageConverterSettings.getStatus().toString().toLowerCase());

    return taskDto;
  }

  /**
   * Convert the given Webcast DTO into a Communication domain object.
   * 
   * @param taskDto the webcast DTO to convert
   * @return the Communication domain object.
   */
  public ImageConverterSettings convert(final TaskDto taskDto) {
    if (taskDto == null) {
      return new ImageConverterSettings();
    }

    ImageConverterSettings imageConverterSettings = new ImageConverterSettings();
    imageConverterSettings.setSource(taskDto.getSource());
    imageConverterSettings.setDestination(taskDto.getDestination());
    imageConverterSettings.setNotify(taskDto.getNotify());
    imageConverterSettings.setFormat(taskDto.getFormat());
    imageConverterSettings.setWidth(taskDto.getWidth());
    imageConverterSettings.setHeight(taskDto.getHeight());
    imageConverterSettings.setScaleMode(taskDto.getScaleMode());
    imageConverterSettings.setStatus(ImageConverterSettings.Status.valueOf(taskDto.getStatus().toUpperCase()));

    return imageConverterSettings;

  }
}
