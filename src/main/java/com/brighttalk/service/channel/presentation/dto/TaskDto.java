/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2011.
 * All Rights Reserved.
 * $Id: TaskDto.java 41526 2012-01-30 10:43:06Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * TaskDto.
 */
@XmlRootElement(name = "task")
public class TaskDto {

  private String source;

  private String destination;

  private String notify;

  private String format;

  private Integer width;

  private Integer height;

  private String scaleMode;

  private String status;

  public String getSource() {
    return source;
  }

  public void setSource(final String source) {
    this.source = source;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(final String destination) {
    this.destination = destination;
  }

  public String getNotify() {
    return notify;
  }

  public void setNotify(final String notify) {
    this.notify = notify;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(final String format) {
    this.format = format;
  }

  public Integer getWidth() {
    return width;
  }

  public void setWidth(final Integer width) {
    this.width = width;
  }

  public Integer getHeight() {
    return height;
  }

  public void setHeight(final Integer height) {
    this.height = height;
  }

  public String getScaleMode() {
    return scaleMode;
  }

  public void setScaleMode(final String scaleMode) {
    this.scaleMode = scaleMode;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(final String status) {
    this.status = status;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("source: [").append(this.getSource()).append("] ");
    builder.append("destination: [").append(this.getDestination()).append("] ");
    builder.append("notify: [").append(this.getNotify()).append("] ");
    builder.append("format: [").append(this.getFormat()).append("] ");
    builder.append("width: [").append(this.getWidth()).append("] ");
    builder.append("height: [").append(this.getHeight()).append("] ");
    builder.append("scaleMode: [").append(this.getScaleMode()).append("] ");
    builder.append("status: [").append(this.getStatus()).append("] ");

    return builder.toString();
  }
}
