/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012-2013.
 * All Rights Reserved.
 * $Id: PaginatedListBuilder.java 69924 2013-10-18 14:47:18Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.domain.PaginatedList;

/**
 * Converter Responsible for converting between {@link PaginatedList} and {@link List} types.
 */
@Component
public class PaginatedListBuilder {

  /**
   * A helper method to build a {@link PaginatedList} wrapper.
   * 
   * @param list a collection elements
   * @param pageNumber page number
   * @param pageSize page size
   * @param <T> element to be paginated
   * 
   * @return paginated list
   */
  public <T> PaginatedList<T> build(final List<T> list, final int pageNumber, final int pageSize) {

    boolean isFinalPage = true;

    if (!CollectionUtils.isEmpty(list) && list.size() > pageSize) {
      isFinalPage = false;

      // Remove extra community added to support peeking for next page
      list.remove(list.size() - 1);
    }

    PaginatedList<T> result = new PaginatedList<T>();

    result.addAll(list);
    result.setIsFinalPage(isFinalPage);
    result.setCurrentPageNumber(pageNumber);
    result.setPageSize(pageSize);

    return result;
  }

}
