/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionErrorDto.java 92672 2015-04-01 08:27:25Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import java.util.List;

/**
 * Dto to hold the subscription error details.
 */
public class SubscriptionErrorDto {

  private List<String> codes;

  private String message;

  private SubscriptionDto subscription;

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  public SubscriptionDto getSubscription() {
    return subscription;
  }

  public void setSubscription(final SubscriptionDto subscription) {
    this.subscription = subscription;
  }

  public List<String> getCodes() {
    return codes;
  }

  public void setCodes(List<String> codes) {
    this.codes = codes;
  }

}
