/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AtomFeedEntryConverter.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.converter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.presentation.dto.LinkDto;
import com.brighttalk.service.channel.presentation.dto.atom.AtomFeedDto;
import com.brighttalk.service.channel.presentation.dto.atom.AuthorDto;
import com.brighttalk.service.channel.presentation.dto.atom.CategoryDto;
import com.brighttalk.service.channel.presentation.dto.atom.ChannelDto;
import com.brighttalk.service.channel.presentation.dto.atom.CommunicationDto;
import com.brighttalk.service.channel.presentation.dto.atom.FeedEntryDto;
import com.brighttalk.service.channel.presentation.dto.converter.util.ThreadSafeSimpleDateFormat;

/**
 * Converter - Responsible for converting between FeedEntryDto and business type.
 */
@Component
public class AtomFeedEntryConverter {

  /**
   * A value used as a title in thumbnail link dto object.
   */
  protected static final String THUMBNAIL_LINK_TITLE = "thumbnail";

  /**
   * A value used as a title in preview link dto object.
   */
  protected static final String PREVIEW_LINK_TITLE = "preview";
  
  /**
   * A value used as a title in calendar link dto object.
   */
  protected static final String CALENDAR_LINK_TITLE = "calendar";
  
  private static final long ONE_SECOND = 1000;
  
  /**
   * Converts a single communication into an entry dto. 
   * 
   * @param communication details
   * 
   * @return {@link FeedEntryDto}
   */
  public FeedEntryDto convert( Communication communication ) {
    
    FeedEntryDto entry = new FeedEntryDto();
    
    String constructedCommunicationId = createCommunicationId( communication );
    entry.setId( constructedCommunicationId );
    entry.setTitle( communication.getTitle() );
    entry.setStatus( communication.getStatus().toString().toLowerCase() );
    entry.setSummary( communication.getDescription() );
    entry.setFormat( communication.getFormat().toString().toLowerCase() );
    entry.setDuration( communication.getDuration() );
    entry.setRating( communication.getCommunicationStatistics().getFormattedRating() );
    
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(communication.getScheduledDateTime());
    long startTimeInSeconds = calendar.getTimeInMillis() / ONE_SECOND;
    entry.setStart(startTimeInSeconds);
    
    if ( communication.isFeatured() ) {
      entry.setFeatured(true);
    }
    
    if ( communication.getConfiguration().isFeedOnly() ) {
      entry.setHidden(true);
    }
    
    if ( communication.hasEntryTime() ) {
      
      calendar.setTime(communication.getEntryTime());
      long entryTimeInSeconds = calendar.getTimeInMillis() / ONE_SECOND;
      entry.setEntryTime(entryTimeInSeconds);
    }
    
    if ( communication.hasCloseTime() ) {
      
      Date closeTime = communication.getCloseTime();
      if ( closeTime != null ) {
    
        calendar.setTime( communication.getCloseTime() );
        long closeTimeInSeconds = calendar.getTimeInMillis() / ONE_SECOND;
        entry.setCloseTime( closeTimeInSeconds );
      }
    }
    
    List<LinkDto> links = new ArrayList<LinkDto>();
    links.add( createCommunicationUrl( communication ) );
    
    if ( communication.hasThumbnailUrl() ) {
      
      links.add( createThumbnailLink( communication ) );
    }
    
    if ( communication.hasPreviewUrl() ) {
      
      links.add( createPreviewLink( communication ) );
    }
    
    if ( communication.isUpcoming() && communication.hasCalendarUrl() ) {
      
      links.add( createCalendarLink( communication ) );
    }
    
    entry.setLinks( links );
    
    ThreadSafeSimpleDateFormat format = new ThreadSafeSimpleDateFormat( AtomFeedConverter.LAST_UPDATED_DATE_PATTERN );
    String lastUpdatedString = format.format( communication.getLastUpdated() );
    entry.setUpdated( lastUpdatedString );
    
    AuthorDto authorDto = new AuthorDto();
    authorDto.setName( communication.getAuthors() );
    entry.setAuthor( authorDto );
    
    CommunicationDto communicationDto = new CommunicationDto();
    communicationDto.setId( communication.getId() );
    entry.setCommunication( communicationDto );
    
    ChannelDto channelDto = new ChannelDto();
    channelDto.setId( communication.getChannelId() );
    entry.setChannel( channelDto );
    
    List<CategoryDto> categories = new ArrayList<CategoryDto>();
    CategoryDto categoryDto = null;
    
    if ( communication.getKeywords() != null ) {
      String[] keywords = communication.getKeywords().split(",");
      
      for ( String keyword : keywords ) {
        categoryDto = new CategoryDto();
        categoryDto.setScheme( CategoryDto.CATEGORY_SCHEMA_KEYWORD );
        categoryDto.setTerm( keyword.trim() );
        
        categories.add( categoryDto );
      }
    }
    
    if ( communication.hasCategories() ) { 
      for ( CommunicationCategory communicationCategory : communication.getCategories() ) {  
        categoryDto = new CategoryDto();
        categoryDto.setScheme( CategoryDto.CATEGORY_SCHEMA_CATEGORY );
        categoryDto.setTerm( communicationCategory.getDescription().trim() );
        
        categories.add( categoryDto );
      }
    }
    
    if ( categories.size() > 0 ) {    
      entry.setCategories( categories );
    }
    
    return entry;
  }
  
  private String createCommunicationId( Communication communication ) {
    
    Calendar calendar = Calendar.getInstance();
    calendar.setTime( communication.getCreated() );
    int year = calendar.get( Calendar.YEAR );
    
    StringBuilder builder = new StringBuilder();
    builder.append( AtomFeedDto.BRIGHTTALK_TAG ).append( year );
    builder.append(":communication:").append( communication.getId() );
    
    return builder.toString();
  }
  
  private LinkDto createThumbnailLink( Communication communication ) {
    
    LinkDto linkDto = new LinkDto();
    
    linkDto.setHref( communication.getThumbnailUrl() );
    linkDto.setRel( LinkDto.RELATIONSHIP_ENCLOSURE );
    linkDto.setType( LinkDto.TYPE_IMAGE_PNG );
    linkDto.setTitle( THUMBNAIL_LINK_TITLE );
    
    return linkDto;
  }
  
  private LinkDto createPreviewLink( Communication communication ) {
    
    LinkDto linkDto = new LinkDto();
    
    linkDto.setHref( communication.getPreviewUrl() );
    linkDto.setRel( LinkDto.RELATIONSHIP_RELATED );
    linkDto.setType( LinkDto.TYPE_IMAGE_PNG );
    linkDto.setTitle( PREVIEW_LINK_TITLE );
    
    return linkDto;
  }
  
  private LinkDto createCalendarLink( Communication communication ) {
    
    LinkDto linkDto = new LinkDto();
    
    linkDto.setHref( communication.getCalendarUrl() );
    linkDto.setRel( LinkDto.RELATIONSHIP_RELATED );
    linkDto.setType( LinkDto.TYPE_TEXT_CALENDAR );
    linkDto.setTitle( CALENDAR_LINK_TITLE );
    
    return linkDto;
  }
  
  private LinkDto createCommunicationUrl( Communication communication ) {
    
    LinkDto linkDto = new LinkDto();
    
    linkDto.setHref( communication.getUrl() );
    linkDto.setRel( LinkDto.RELATIONSHIP_ALTERNATE );
    linkDto.setType( LinkDto.TYPE_TEXT_HTML );
    
    return linkDto;
  }
  
}