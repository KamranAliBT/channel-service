/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: package-info.java 98704 2015-07-31 10:36:32Z ssarraj $
 * ****************************************************************************
 */
/**
 * Syndication DTO converter package.
 */
package com.brighttalk.service.channel.presentation.dto.syndication.converter;