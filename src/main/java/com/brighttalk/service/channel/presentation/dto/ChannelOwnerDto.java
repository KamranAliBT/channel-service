/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2011.
 * All Rights Reserved.
 * $Id: ChannelOwnerDto.java 47312 2012-04-30 10:59:24Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * ChannelOwnerDto.
 */
public class ChannelOwnerDto {

  private Long id;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(Long id) {
    this.id = id;
  }
  
  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  public String toString() {
    
    StringBuilder builder = new StringBuilder();
    builder.append("id: [").append( id ).append("] ");

    return builder.toString();
  }
}
