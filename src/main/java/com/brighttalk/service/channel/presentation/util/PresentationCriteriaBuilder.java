/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: PresentationCriteriaBuilder.java 71274 2013-11-26 14:01:47Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.PresentationCriteria;
import com.brighttalk.service.channel.presentation.ApiRequestParam;

/**
 * Builder Class to extract a {@link PresentationCriteria} from a request object.
 */
@Component
public class PresentationCriteriaBuilder {

  /**
   * Build the expand criteria from request context with default expanded featured webcasts.
   * 
   * @param request the http request to convert to criteria
   * 
   * @return The built criteria
   */
  public PresentationCriteria buildWithDefaultExpandedFeaturedWebcasts(final HttpServletRequest request) {

    PresentationCriteria criteria = build(request);

    if (!criteria.expandIncludes(PresentationCriteria.EXPAND_FEATURED_WEBCASTS)) {
      criteria.addExpand(PresentationCriteria.EXPAND_FEATURED_WEBCASTS);
    }

    return criteria;
  }

  /**
   * Build the expand criteria from request context without default expanded featured webcasts.
   * 
   * @param request the http request to convert to criteria
   * 
   * @return The built criteria
   */
  public PresentationCriteria build(final HttpServletRequest request) {

    PresentationCriteria criteria = new PresentationCriteria();

    String expand = request.getParameter(ApiRequestParam.EXPAND);
    criteria.setExpand(expand);

    return criteria;
  }
}