/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: CommunicationStatisticsDto.java 98704 2015-07-31 10:36:32Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.statistics;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * DTO representing a communication activities statistics (this includes viewings total, total viewing duration,
 * pre-registration total and average rating).
 */
public class CommunicationStatisticsDto {

  private final int totalRegistration;
  private final int totalViews;
  private final long totalViewingDuration;
  private final float averageRating;

  /**
   * @param totalRegistration The communication registration count.
   * @param totalViews The communication total views count.
   * @param totalViewingDuration The communication total viewing for duration in seconds.
   * @param averageRating The communication average rating.
   */
  public CommunicationStatisticsDto(int totalRegistration, int totalViews, long totalViewingDuration,
      float averageRating) {
    this.totalRegistration = totalRegistration;
    this.totalViews = totalViews;
    this.totalViewingDuration = totalViewingDuration;
    this.averageRating = averageRating;
  }

  /**
   * @return the totalRegistration
   */
  public int getTotalRegistration() {
    return totalRegistration;
  }

  /**
   * @return the totalViews
   */
  public int getTotalViews() {
    return totalViews;
  }

  /**
   * @return the totalViewingDuration
   */
  public long getTotalViewingDuration() {
    return totalViewingDuration;
  }

  /**
   * @return the averageRating
   */
  public float getAverageRating() {
    return averageRating;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this);
    builder.append("totalRegistration", totalRegistration);
    builder.append("totalViews", totalViews);
    builder.append("totalViewingDuration", totalViewingDuration);
    builder.append("averageRating", averageRating);
    return builder.toString();
  }
}
