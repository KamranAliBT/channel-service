/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: CreateContractPeriod.java 100930 2015-10-01 15:42:05Z kali $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.presentation.dto.validation.scenario;

/**
 * Validation group type for Contract Period create scenario.
 */
public interface CreateContractPeriod {

}
