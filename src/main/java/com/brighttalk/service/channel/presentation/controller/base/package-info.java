/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 86174 2014-11-18 13:20:28Z ssarraj $
 * ****************************************************************************
 */
/**
 * This package contains presentation base Controllers.
 */
package com.brighttalk.service.channel.presentation.controller.base;