/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

/**
 * The Scheduled Publication Update Service Business API.
 * <p>
 * Allows for automatic publication/un publication for communications. 
 * <p>
 * The channel owner can set dates in the future to publish or unpublish their communication. 
 * <p>
 * This service is then responsible for make sure that given the dates set by the channel owner the publish status is set at the correct 
 * time to publish or unpublished. 
 */
public interface ScheduledPublicationUpdateService {

  /**
   * Perform scheduled publication and unpublication of communications. 
   */
  void performScheduledPublicationStatusUpdate();

}
