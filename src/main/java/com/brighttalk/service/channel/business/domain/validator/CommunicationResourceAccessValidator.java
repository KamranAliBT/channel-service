/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationResourceAccessValidator.java 74757 2014-02-27 11:18:36Z ikerbib $ 
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.NotAllowedToAccessMediaZoneCommunicationException;

/**
 * {@link Validator} that validates a communication is in a valid state for finding resources on. 
 */
public class CommunicationResourceAccessValidator implements Validator<Communication> {

  /**
   * Validate the given communication is valid for resource access.
   * 
   * Biz Rules:
   * - a Mediazone communications cannot have resources.

   * 
   * @param communication the communication to validate
   * 
   * @throws NotAllowedToAccessMediaZoneCommunicationException if the communication provided by MediaZone.
   */
  public void validate(Communication communication) {
    
    if (communication.isMediaZone()) {
      throw new NotAllowedToAccessMediaZoneCommunicationException("Communication [" + communication.getId()
          + "] is a MediaZone communication. MediaZone communications do not support resources.", "IsMediaZoneCommunication");
    }
  }
}
