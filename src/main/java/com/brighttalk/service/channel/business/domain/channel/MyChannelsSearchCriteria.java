/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MyChannelsSearchCriteria.java 78460 2014-05-23 10:58:44Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import org.apache.commons.lang.StringUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;

/**
 * Search Criteria for retrieving the paginated channels list related to my channel pages.
 * <p>
 * Contains the page number being requested, the size of the page being requested and any filter criteria for the feed.
 * <p>
 * Flags on how the channels should be sorted are part of this class, too.
 */
public class MyChannelsSearchCriteria extends BaseSearchCriteria {

  private SortBy sortBy;

  /**
   * 'Sort by' types.
   */
  public static enum SortBy {

    /**
     * Sort by date.
     */
    CREATEDDATE("created"),

    /**
     * .Sort by number of subscribers
     */
    SUBSCRIBERS("subscriber_count"),

    /**
     * .Sort by average rating
     */
    AVERAGERATING("rating"),

    /**
     * .Sort by number of upcoming webcasts
     */
    UPCOMINGWEBCASTS("upcoming_count"),

    /**
     * .Sort by number of recorded webcasts
     */
    RECORDEDWEBCASTS("recorded_count");

    private final String value;

    /**
     * Default constructor.
     * 
     * @param value Column value
     */
    private SortBy(final String value) {
      this.value = value;
    }

    /**
     * Get collumn name.
     * 
     * @return column name
     */
    public String getValue() {
      return value;
    }

    /**
     * Converts a string into a SortBy.
     * 
     * An empty value means that default sorting is requested - sorted by CREATEDDATE.
     * 
     * @param value to be converted
     * 
     * @return SortBy
     * 
     * @throws ApplicationException if the value is invalid
     */
    public static SortBy convert(final String value) {

      if (StringUtils.isEmpty(value)) {
        return SortBy.CREATEDDATE;
      }

      try {

        return SortBy.valueOf(value.trim().toUpperCase());

      } catch (IllegalArgumentException exception) {

        throw new ApplicationException("Invalid sort by value requested [" + value + "]", exception);
      }
    }
  }

  public SortBy getSortBy() {
    return sortBy;
  }

  public void setSortBy(final SortBy value) {
    sortBy = value;
  }

  /**
   * Indicates if this search criteria has sort by set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasSortBy() {
    return sortBy != null;
  }
}