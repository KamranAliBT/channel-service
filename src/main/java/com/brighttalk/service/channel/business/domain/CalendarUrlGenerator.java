/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CalendarUrlGenerator.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationUrlGenerator;

/**
 * Implementation of a Communication URl Generator that generates communication calendar url.
 */
public class CalendarUrlGenerator implements CommunicationUrlGenerator {

  private String calendarUriTemplate;
  
  /**
   * This template is used to create calendar url for a communication 
   * and is set when the bean is instantiated in Spring config.
   * 
   * @return uri template
   */
  public String getCalendarUriTemplate() {
    return calendarUriTemplate;
  }

  public void setCalendarUriTemplate(String calendarUriTemplate) {
    this.calendarUriTemplate = calendarUriTemplate;
  }

  /** {@inheritDoc} */
  @Override
  public String generate(Channel channel, Communication communication) {
    
    String url = calendarUriTemplate;
    url = url.replace("{channelId}", channel.getId().toString() );
    url = url.replace("{communicationId}", communication.getId().toString() );
    
    return url;
  }
}