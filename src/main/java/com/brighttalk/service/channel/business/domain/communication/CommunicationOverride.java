/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationOverride.java 74717 2014-02-26 11:48:06Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import org.apache.commons.lang.StringUtils;

import com.brighttalk.service.channel.business.domain.Entity;

/**
 * Represents override data for a communication.
 * <p>
 * When a communication is syndicated into another channel, some of its attributes can be changed for that channel.
 * Currently these are: title, description, authors and keywords. One or more or none of these attributes can be changed
 * for a syndicated webcast>
 * <p>
 * A client may want to syndicate a webcast into another channel and change its title to more accurately reflect that
 * channels theme.
 * <p>
 * Typically this will be used to for example change the language of the title, description, authors and keywords since
 * the syndicated channel might be e.g. a German channel.
 */
public class CommunicationOverride extends Entity {

  private static final long serialVersionUID = 1L;

  /**
   * The overridden title of the communication.
   */
  private final String title;
  /**
   * The overridden description of the communication.
   */
  private final String description;
  /**
   * The overridden authors of the communication.
   */
  private final String authors;
  /**
   * The overridden keywords of the communication.
   */
  private final String keywords;

  /**
   * Construct this communication override.
   * 
   * @param communicationId the id of the communication the overrides are for.
   * @param title the title to be overridden. Set <code>null</code> or empty if not overridden.
   * @param description the description to be overridden. Set <code>null</code> or empty if not overridden.
   * @param authors the authors to be overridden. Set <code>null</code> or empty if not overridden.
   * @param keywords the keywords to be overridden. Set <code>null</code> or empty if not overridden.
   */
  public CommunicationOverride(final Long communicationId, final String title, final String description,
      final String authors, final String keywords) {
    super(communicationId);
    this.title = title;
    this.description = description;
    this.authors = authors;
    this.keywords = keywords;
  }

  /**
   * Get the overridden title.
   * 
   * @return the overridden title or <code>null</code> if not overridden.
   * 
   * @see #isTitleOverridden()
   */
  public String getTitle() {
    return title;
  }

  /**
   * Return true if the communication override has title set.
   * 
   * @return true is title is set.
   */
  public boolean hasTitle() {
    return title != null;
  }

  /**
   * Get the overridden description.
   * 
   * @return the overridden description or <code>null</code> if not overridden.
   * 
   * @see #isDescriptionOverridden()
   */
  public String getDescription() {
    return description;
  }

  /**
   * Return true if the communication override has description set.
   * 
   * @return true is description is set.
   */
  public boolean hasDescription() {
    return description != null;
  }

  /**
   * Get the overridden authors.
   * 
   * @return the overridden authors or <code>null</code> if not overridden.
   * 
   * @see #isAuthorsOverridden()
   */
  public String getAuthors() {
    return authors;
  }

  /**
   * Return true if the communication override has authors set.
   * 
   * @return true is authors is set.
   */
  public boolean hasAuthors() {
    return authors != null;
  }

  /**
   * Get the overridden keywords.
   * 
   * @return the overridden title or <code>null</code> if not overridden.
   * 
   * @see #isKeywordsOverridden()
   */
  public String getKeywords() {
    return keywords;
  }

  /**
   * Return true if the communication override has keywords set.
   * 
   * @return true is keywords is set.
   */
  public boolean hasKeywords() {
    return keywords != null;
  }

  /**
   * Indicate if the title field is overridden.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isTitleOverridden() {
    return StringUtils.isNotEmpty(this.getTitle());
  }

  /**
   * Indicate if the description field is overridden.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isDescriptionOverridden() {
    return StringUtils.isNotEmpty(this.getDescription());
  }

  /**
   * Indicate if the authors field is overridden.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isAuthorsOverridden() {
    return StringUtils.isNotEmpty(this.getAuthors());
  }

  /**
   * Indicate if the keywords field is overridden.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isKeywordsOverridden() {
    return StringUtils.isNotEmpty(this.getKeywords());
  }

  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder("CommunicationOverride");
    builder.append(super.toString());
    builder.append("[title=" + this.getTitle() + "]\n");
    builder.append("[description=" + this.getDescription() + "]\n");
    builder.append("[authors=" + this.getAuthors() + "]\n");
    builder.append("[keywords=" + this.getKeywords() + "]\n");
    return builder.toString();

  }

}
