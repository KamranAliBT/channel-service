/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the business error classes of the Channel Service. Application specific errors and exceptions 
 * should be defined in this package.
 */
package com.brighttalk.service.channel.business.error;