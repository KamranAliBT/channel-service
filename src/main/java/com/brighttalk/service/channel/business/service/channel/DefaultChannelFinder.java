/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelFinder.java 75381 2014-03-18 15:16:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelUrlGenerator;
import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.database.ChannelManagerDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.FeatureDbDao;

/**
 * Default implementation of the {@link ChannelFinder}.
 */
@Primary
@Service
public class DefaultChannelFinder implements ChannelFinder {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(DefaultChannelFinder.class);

  /** Channel database DAO. */
  @Autowired
  protected ChannelDbDao channelDbDao;

  /** Feature database DAO. */
  @Autowired
  protected FeatureDbDao featureDbDao;

  /** Channel manager database DAO. */
  @Autowired
  protected ChannelManagerDbDao channelManagerDbDao;

  /** Category Database Dao. */
  @Autowired
  protected CategoryDbDao categoryDbDao;

  /** Communications Database Dao. */
  @Autowired
  protected CommunicationDbDao communicationDbDAO;

  @Value("#{channelPortalUrlGenerator}")
  private ChannelUrlGenerator channelUrlGenerator;

  @Value("#{channelAtomFeedUrlGenerator}")
  private ChannelUrlGenerator channelAtomFeedUrlGenerator;

  @Value("#{channelRssFeedUrlGenerator}")
  private ChannelUrlGenerator channelRssFeedUrlGenerator;

  @Value("${channels.list.defaultPageSize}")
  private int defaultPageSize;

  @Value("${channels.list.maxPageSize}")
  private int maxPageSize;

  @Value("${channels.list.defaultPageNumber}")
  private int defaultPageNumber;

  /** {@inheritDoc} */
  @Override
  public Channel find(final Long id) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding channel  [" + id + "].");
    }

    Channel channel = channelDbDao.find(id);
    loadDetailsOfChannelExcludingPrivateWebcasts(channel);

    return channel;
  }

  /** {@inheritDoc} */
  @Override
  public Channel find(final User user, final Long channelId) {

    Channel channel = find(channelId);
    authoriseChannelAccess(user, channel);

    return channel;
  }

  /** {@inheritDoc} */
  @Override
  public List<Channel> find(final List<Long> ids) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding channels  [" + ids + "].");
    }

    List<Channel> channels = channelDbDao.find(ids);
    loadDetailsOfChannelsExcludingPrivateWebcasts(channels);

    return channels;
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<Channel> find(final ChannelsSearchCriteria criteria) {

    populateSearchCriteriaDefaultValues(criteria);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding channels  [" + criteria.getChanelIds() + "] using search criteria.");
    }

    PaginatedList<Channel> channels = channelDbDao.find(criteria);
    loadDetailsOfChannels(channels, criteria.includePrivateWebcasts());

    return channels;
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<Channel> findSubscribedChannels(final User user, final MyChannelsSearchCriteria searchCriteria) {
    long userId = user.getId();
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding subscribed channels for user [" + userId + "] using search criteria.");
    }

    populateSearchCriteriaDefaultValues(searchCriteria);
    PaginatedList<Channel> paginatedChannels = channelDbDao.findPaginatedChannelsSubscribed(userId, searchCriteria);
    loadDetailsOfChannelsExcludingPrivateWebcasts(paginatedChannels);

    return paginatedChannels;
  }

  /** {@inheritDoc} */
  @Override
  public List<Channel> findSubscribedChannels(final User user) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding subscribed channels for user [" + user.getId() + "].");
    }

    @SuppressWarnings("deprecation")
    List<Channel> subscribedChannels = channelDbDao.findSubscribedChannels(user.getId().longValue());

    return subscribedChannels;
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<Channel> findOwnedChannels(final User user, final MyChannelsSearchCriteria searchCriteria) {

    long userId = user.getId();
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding owned channels for user [" + userId + "] using search criteria.");
    }

    // For Owned channels we can include private webcasts in the list of featured communications
    populateSearchCriteriaDefaultValues(searchCriteria);
    PaginatedList<Channel> paginatedChannels = channelDbDao.findPaginatedChannelsOwned(userId, searchCriteria);
    loadDetailsOfChannelsIncludingPrivateWebcasts(paginatedChannels);

    return paginatedChannels;
  }

  /**
   * Load statistics, features, categories, survey, URLs and featured communications for the given channel, excluding
   * private Webcasts.
   * 
   * @param channel channel to be loaded
   */
  private void loadDetailsOfChannelExcludingPrivateWebcasts(final Channel channel) {
    // Careful!! Order is important here it seems. Unit tests fail if we change the order.
    // This is due to the load urls having ot be called after the features have been loaded.
    channelDbDao.loadStatistics(channel);
    featureDbDao.loadFeatures(channel);
    categoryDbDao.loadCategories(channel);
    channelDbDao.loadSurvey(channel);
    channelManagerDbDao.load(channel);
    loadUrls(channel);
    loadFeaturedCommunications(channel, false);
  }

  /**
   * Load statistics, features, categories, survey, URLs and featured communications for the given channels, including
   * private Webcasts.
   * 
   * @param channels channels to be loaded
   */
  private void loadDetailsOfChannelsIncludingPrivateWebcasts(final List<Channel> channels) {
    loadDetailsOfChannels(channels, true);
  }

  /**
   * Load statistics, features, categories, survey, URLs and featured communications for the given channels, including
   * private Webcasts.
   * 
   * @param channels channels to be loaded
   */
  private void loadDetailsOfChannelsExcludingPrivateWebcasts(final List<Channel> channels) {
    loadDetailsOfChannels(channels, false);
  }

  /**
   * Load statistics, features, categories, survey, URLs and featured communications for the given channels.
   * 
   * @param channels channels to be loaded
   * @param includePrivateWebcasts a flag indicating if the search for featured communications should include private
   * communications
   */
  private void loadDetailsOfChannels(final List<Channel> channels, final boolean includePrivateWebcasts) {
    if (!channels.isEmpty()) {
      // Careful!! Order is important here it seems. Unit tests fail if we change the order.
      // This is due to the load urls having ot be called after the features have been loaded.
      channelDbDao.loadStatistics(channels);
      featureDbDao.loadFeatures(channels);
      categoryDbDao.loadCategories(channels);
      channelDbDao.loadSurvey(channels);
      loadUrls(channels);
      loadFeaturedCommunications(channels, includePrivateWebcasts);
    }
  }

  /**
   * Load featured communications for a single channel.
   * 
   * @param channel for which the featured communications are to be loaded
   * @param includePrivate a flag indicating if the search for featured communications should include private
   * communications
   */
  private void loadFeaturedCommunications(final Channel channel, final boolean includePrivate) {
    List<Channel> channels = new ArrayList<Channel>();
    channels.add(channel);
    loadFeaturedCommunications(channels, includePrivate);
  }

  /**
   * Load featured communications for a list of channels.
   * 
   * @param channels a collection of channels for which the featured communications are to be loaded
   * @param includePrivate a flag indicating if the search for featured communications should include private
   * communications
   */
  private void loadFeaturedCommunications(final List<Channel> channels, final boolean includePrivate) {

    List<Long> channelIds = getChannelIds(channels);

    List<Communication> featuredCommunications = communicationDbDAO.findFeatured(channelIds, includePrivate);

    if (CollectionUtils.isEmpty(featuredCommunications)) {
      return;
    }

    Map<Long, List<Communication>> mappedFeaturedCommunications = getFeaturedCommunicationsData(featuredCommunications);

    for (Channel channel : channels) {
      if (mappedFeaturedCommunications.containsKey(channel.getId())) {
        channel.setFeaturedCommunications(mappedFeaturedCommunications.get(channel.getId()));
      }
    }
  }

  /**
   * A helper method used to create a map of featured communications where the key is the channel id and the value is a
   * collection of featured communications.
   * 
   * @param featuredCommunications a collection of featured communications
   * 
   * @return a map with channel id / featured communications key/value pair
   */
  private Map<Long, List<Communication>> getFeaturedCommunicationsData(final List<Communication> featuredCommunications) {

    Map<Long, List<Communication>> mappedFeaturedCommunications = new HashMap<Long, List<Communication>>();

    for (Communication communication : featuredCommunications) {

      List<Communication> channelFeaturedCommunications =
          mappedFeaturedCommunications.get(communication.getChannelId());

      if (channelFeaturedCommunications == null) {
        channelFeaturedCommunications = new ArrayList<Communication>();
        mappedFeaturedCommunications.put(communication.getChannelId(), channelFeaturedCommunications);
      }

      channelFeaturedCommunications.add(communication);
    }

    return mappedFeaturedCommunications;
  }

  /**
   * Given a list of channels return a list of their ids.
   * 
   * @param channels the channels
   * @return the ids
   */
  private List<Long> getChannelIds(final List<Channel> channels) {

    if (CollectionUtils.isEmpty(channels)) {
      return new ArrayList<Long>();
    }

    List<Long> channelIds = new ArrayList<Long>();
    for (Channel channel : channels) {
      channelIds.add(channel.getId());
    }

    return channelIds;
  }

  /**
   * Loads all the urls onto the given list of channels.
   * 
   * This includes channel url on portal, channel atom feed url and channel rss feed url.
   * 
   * @param List<Channel> the list of channels to load the urls onto
   */
  private void loadUrls(final List<Channel> channels) {

    for (Channel channel : channels) {
      loadUrls(channel);
    }
  }

  /**
   * Loads all the urls onto the given channel. This includes channel url on portal, channel atom feed url and channel
   * rss feed url.
   * 
   * @param channel the channel to load the URLs on.
   */
  private void loadUrls(final Channel channel) {

    String channelUrl = channelUrlGenerator.generate(channel);
    channel.setUrl(channelUrl);

    String channelAtomFeedUrl = channelAtomFeedUrlGenerator.generate(channel);
    channel.setFeedUrlAtom(channelAtomFeedUrl);

    String channelRssFeedUrl = channelRssFeedUrlGenerator.generate(channel);
    channel.setFeedUrlRss(channelRssFeedUrl);
  }

  /**
   * Populate the given search criteria with its default values.
   * 
   * @param searchCriteria the search criteria to populate.
   */
  private void populateSearchCriteriaDefaultValues(final BaseSearchCriteria searchCriteria) {

    // Set default page number
    if (searchCriteria.getPageNumber() == null) {
      searchCriteria.setPageNumber(defaultPageNumber);
    }

    // Set default Page Size
    if (searchCriteria.getPageSize() == null) {
      searchCriteria.setPageSize(defaultPageSize);
    }

    // Limit Page Size to max allowed
    if (searchCriteria.getPageSize() > maxPageSize) {
      searchCriteria.setPageSize(maxPageSize);
    }
  }

  /**
   * Authorise a user to access a channel. channel.isAuthorised(user) will throw a UserAuthorisationException if the
   * user does not validate the requirements to access the channel
   * 
   * @param user the user to validate access
   * @param channel the channel in which the validation happens
   */
  private void authoriseChannelAccess(final User user, final Channel channel) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Doing Authorisation check on channel [" + channel.getId() + "] for user [" + user.getId() + "].");
    }

    // Presenters of any communication have access
    boolean isPresenter = channelDbDao.isPresenter(user, channel);

    channel.isAuthorised(user, isPresenter);

  }
}
