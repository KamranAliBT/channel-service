/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationFinder.java 70061 2013-10-21 16:34:03Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.ContentPlan;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.TimestampToolbox;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.ContentPlanException;
import com.brighttalk.service.channel.integration.database.ContentPlanDbDao;

/**
 * Default implementation of the {@link ContentPlan}.
 */
@Service
public class ChannelContentPlan implements ContentPlan {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(ChannelContentPlan.class);

  /** Content plan db dao */
  @Autowired
  private ContentPlanDbDao contentPlanDbDao;

  /** Timestamp tool box */
  @Autowired
  private TimestampToolbox timestampToolBox;

  /**
   * @return the contentPlanDbDao
   */
  public ContentPlanDbDao getContentPlanDbDao() {
    return contentPlanDbDao;
  }

  /**
   * @param contentPlanDbDao the contentPlanDbDao to set
   */
  public void setContentPlanDbDao(final ContentPlanDbDao contentPlanDbDao) {
    this.contentPlanDbDao = contentPlanDbDao;
  }

  /**
   * @return the timestampToolBox
   */
  public TimestampToolbox getTimestampToolBox() {
    return timestampToolBox;
  }

  /**
   * @param timestampToolBox the timestampToolBox to set
   */
  public void setTimestampToolBox(final TimestampToolbox timestampToolBox) {
    this.timestampToolBox = timestampToolBox;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void assertForCreate(final User user, final Channel channel, final Communication communication) {
    LOGGER.info("Validating content plan during create phase in channel [" + channel.getId() + "].");
    if (communication.getConfiguration().excludeFromChannelContentPlan()) {

      // Validate that we are authorised to exclude from content plan checks
      authoriseForExclusionOfContentPlanChecks(user);
    } else if (communication.getConfiguration().useLimitRules()) {

      // Do the Capacity checks for a new communication
      doContentPlanCheck(channel, communication, false);
    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void assertForUpdate(final User user, final Channel channel, final Communication communication) {
    LOGGER.info("Validating content plan for [" + communication.getId() + "] during update phase in channel ["
        + channel.getId() + "].");
    if (communication.getConfiguration().excludeFromChannelContentPlan()) {

      // Validate that we are authorised to exclude from content plan checks
      authoriseForExclusionOfContentPlanChecks(user);
    } else if (communication.getConfiguration().useLimitRules()) {

      // Do the Capacity checks for a new communication
      doContentPlanCheck(channel, communication, true);
    }
  }

  /**
   * Validate that the given user is authorised to use exclude a commnication from the channel content plan checks
   * 
   * @param user the user to authorise for exclusion of content plan checks
   * 
   * @throws UserAuthorisationException when the user is not a manager
   */
  private void authoriseForExclusionOfContentPlanChecks(final User user) {
    if (!user.isManager()) {
      throw new UserAuthorisationException("Cannot use Channel Content Plan Override");
    }
  }

  /**
   * Do the Content Plan Check
   * 
   * @param The channel in which the content plan is going to be performed.
   * @param The communication to check
   * @param isReschedule informs if this is a reschedule - i.e. exclude the given communication from the check.
   * @throws ContentPlanException Thrown if the content plan has been reached or not allowed.
   */
  private void doContentPlanCheck(final Channel channel, final Communication communication, final boolean isReschedule) {

    LOGGER.debug("Doing a content plan check for channel [" + channel.getId() + "] and communication ["
        + communication.getId() + "]");

    boolean includeSyndicatedContent = channel.includeSyndicatedContent();

    // Syndicated Communications allowed if not included in content plan
    if (!includeSyndicatedContent && communication.getConfiguration().getCommunicationSyndication().isSyndicatedIn()) {
      LOGGER.debug("Communication [" + communication.getId() + "] is syndicated in, so no check required.");
      return;
    }

    Date scheduled = communication.getScheduledDateTime();
    Date created = channel.getCreated();

    boolean inFirstMonth = getTimestampToolBox().inFirstMonth(created, scheduled);

    Feature contentPlanFeature = null;
    HashMap<String, Date> period = new HashMap<String, Date>();

    if (inFirstMonth) {

      period = getTimestampToolBox().getFirstMonthPeriod(created);

      contentPlanFeature = channel.getFeature(Type.MAX_COMM_FIRST_MONTH);
    } else {
      Feature periodLengthFeature = channel.getFeature(Type.MAX_COMM_PERIOD_LENGTH);
      int periodInMonths = Integer.valueOf(periodLengthFeature.getValue());

      period = getTimestampToolBox().getPeriod(created, scheduled, periodInMonths);

      contentPlanFeature = channel.getFeature(Type.MAX_COMM_PER_PERIOD);
    }

    // If the ContentPlan is not enabled - there is no limit to the number of webcasts allowed in this channel
    if (!contentPlanFeature.isEnabled()) {
      LOGGER.debug("Content Plan feature not enabled for channel [" + channel.getId()
          + "] - Unlimited webcasts allowed.");
      return;
    }

    Long contentPlanLimit = Long.valueOf(contentPlanFeature.getValue());

    if (contentPlanLimit == 0) {
      throw new ContentPlanException("Booking not allowed - ContentPlan is zero.",
          ContentPlanException.ErrorCode.BookingNotAllowed.toString());
    }

    Date startPeriod = period.get(TimestampToolbox.START_PERIOD);
    Date endPeriod = period.get(TimestampToolbox.END_PERIOD);
    Long communicationCount = 0L;

    if (isReschedule) {
      communicationCount = getContentPlanDbDao().countCommunicationsInPeriod(channel.getId(), startPeriod, endPeriod,
          includeSyndicatedContent, communication.getId());
    } else {
      communicationCount = getContentPlanDbDao().countCommunicationsInPeriod(channel.getId(), startPeriod, endPeriod,
          includeSyndicatedContent, null);
    }

    if (communicationCount >= contentPlanLimit) {

      String message = "Max Communication Limit Reached - Max Communications [" + contentPlanLimit
          + "], Current Count [" + communicationCount + "].";

      if (inFirstMonth) {
        message = message.concat(" Error code [" + ContentPlanException.ErrorCode.FirstMonthLimitReached + "]");
        throw new ContentPlanException(message, ContentPlanException.ErrorCode.FirstMonthLimitReached.toString());

      } else {
        message = message.concat(" Error code [" + ContentPlanException.ErrorCode.PeriodLimitReached + "]");
        throw new ContentPlanException(message, ContentPlanException.ErrorCode.PeriodLimitReached.toString());
      }
    }
  }
}