/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionFilter.java 87022 2014-12-03 13:00:40Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.filter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.error.SubscriptionNotFoundException;

/**
 * Helper to filter subscriptions.
 */
@Component
public class SubscriptionFilter {

  @Autowired
  private CollectionFilter collectionFilter;

  /**
   * Filter a subscription by user id.
   * 
   * @param subscriptions the subscriptions to filter
   * @param userId the user id
   * 
   * @return the filtered subscription
   */
  public Subscription filterByUserId(final List<Subscription> subscriptions, final Long userId) {
    Predicate<Subscription> equalsUserId = new Predicate<Subscription>() {
      @Override
      public boolean apply(final Subscription subscription) {
        return subscription.getUser().getId().equals(userId);
      }
    };

    List<Subscription> filteredRecords = collectionFilter.filter(subscriptions, equalsUserId);

    if (CollectionUtils.isEmpty(filteredRecords)) {
      throw new SubscriptionNotFoundException();
    }

    return filteredRecords.iterator().next();
  }

}
