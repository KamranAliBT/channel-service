/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationCategory.java 74717 2014-02-26 11:48:06Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import com.brighttalk.service.channel.business.domain.Entity;

/**
 * Represents a single category for a communication.
 * <p>
 * A category is just a simple text string which is then typically returned in feeds and which can be used by clients to
 * categorize a communication for display purposes.
 * <p>
 * A channel has a number of categories set up on it. One or more (or none) of these categories can then be added to a
 * communication. This class represents a category assigned to a communication. It has no relevance outside the context
 * of a communication.
 * <p>
 */
public class CommunicationCategory extends Entity {

  private static final long serialVersionUID = 1L;

  /**
   * The id of the channel this communication category is assigned to.
   */
  private Long channelId;

  /**
   * The id of the communication this communication category is assigned to.
   */
  private Long communicationId;

  /**
   * This communication categories description.
   */
  private final String description;

  /**
   * Indicates if this communication category is isActive.
   */
  private boolean isActive;

  /**
   * Construct this communication category.
   * 
   * @param categoryId the id of the category.
   * @param channelId the id of the channel the category is assigned to.
   * @param communicationId the id of the communication the category is assigned to.
   * @param description the category description
   * @param isActive whether this category is isActive or not.
   */
  public CommunicationCategory(final Long categoryId, final Long channelId, final Long communicationId,
      final String description, final boolean isActive) {
    super(categoryId);
    this.channelId = channelId;
    this.communicationId = communicationId;
    this.isActive = isActive;
    this.description = description;
  }

  /**
   * Construct this communication category.
   * 
   * @param categoryId the id of the category.
   * @param channelId the id of the channel the category is assigned to.
   * @param description the category description
   * @param isActive whether this category is isActive or not.
   */
  public CommunicationCategory(final Long categoryId, final Long channelId, final String description,
      final boolean isActive) {
    super(categoryId);
    this.channelId = channelId;
    this.isActive = isActive;
    this.description = description;
  }

  /**
   * Get the channel id this communication category is assigned to.
   * 
   * @return the channel id
   */
  public Long getChannelId() {
    return channelId;
  }

  /**
   * @param channelId the channelId to set
   */
  public void setChannelId(final Long channelId) {
    this.channelId = channelId;
  }

  /**
   * Get the communication id this communication category is assigned to.
   * 
   * @return the communication id
   */
  public Long getCommunicationId() {
    return communicationId;
  }

  /**
   * @param communicationId the communicationId to set
   */
  public void setCommunicationId(final Long communicationId) {
    this.communicationId = communicationId;
  }

  /**
   * Get this communication categories description.
   * 
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * Indicate if this communication category is active.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isActive() {
    return isActive;
  }

  /**
   * Sets the is active status of a communication.
   * 
   * @param isActive set is active to the communication category
   */
  public void setIsActive(final boolean isActive) {
    this.isActive = isActive;
  }

  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder("CommunicationCategory");

    builder.append(super.toString());
    builder.append("[id=" + this.getId() + "]\n");
    builder.append("[channelId=" + this.getChannelId() + "]\n");
    builder.append("[communicationId=" + this.getCommunicationId() + "]\n");
    builder.append("[description=" + this.getDescription() + "]\n");
    builder.append("[isActive=" + this.isActive() + "]\n");

    return builder.toString();

  }
}
