/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: Communication.java 74727 2014-02-26 14:15:00Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import com.brighttalk.service.channel.business.domain.communication.Communication.Status;

/**
 * Communication Merger is an helper class.
 * 
 * It allows merging communication data from one communication to another.
 * 
 */
public class CommunicationMerger {

  /**
   * Merge the given communication into the current instance. This will replace the values of the properties in the
   * current instance with the value of the corresponding property from the given instance, but only if set. The
   * properties merged are only the ones required for communication update.
   * 
   * @param communicationA that will be merged into
   * @param communicationB to be merged
   */
  public void mergeCommunication(final Communication communicationA, final Communication communicationB) {
    if (communicationB.hasTitle()) {
      communicationA.setTitle(communicationB.getTitle());
    }
    if (communicationB.hasDescription()) {
      communicationA.setDescription(communicationB.getDescription());
    }
    if (communicationB.hasKeywords()) {
      communicationA.setKeywords(communicationB.getKeywords());
    }
    if (communicationB.hasAuthors()) {
      communicationA.setAuthors(communicationB.getAuthors());
    }
    if (communicationB.hasScheduledDateTime()) {
      communicationA.setScheduledDateTime(communicationB.getScheduledDateTime());
    }
    if (communicationB.hasDuration()) {
      communicationA.setDuration(communicationB.getDuration());
      if (communicationB.hasStatus() && communicationB.getStatus().equals(Status.UPCOMING)) {
        communicationA.setBookingDuration(communicationB.getDuration());
      }
    }
    if (communicationB.hasTimeZone()) {
      communicationA.setTimeZone(communicationB.getTimeZone());
    }
    if (communicationB.hasStatus()) {
      communicationA.setStatus(communicationB.getStatus());
    }
    if (communicationB.hasPublishStatus()) {
      communicationA.setPublishStatus(communicationB.getPublishStatus());
    }
    if (communicationB.hasThumbnailUrl()) {
      communicationA.setThumbnailUrl(communicationB.getThumbnailUrl());
    }
    if (communicationB.hasPreviewUrl()) {
      if (communicationB.getPreviewUrl().equals(Communication.FEATURE_IMAGE_DELETED)) {
        communicationA.setThumbnailUrl(null);
        communicationA.setPreviewUrl(null);
      } else {
        communicationA.setPreviewUrl(communicationB.getPreviewUrl());
      }
    }

    communicationA.setCategories(communicationB.getCategories());
    if (communicationA.hasConfiguration() && communicationB.hasConfiguration()) {
      mergeCommunicationConfiguration(communicationA.getConfiguration(), communicationB.getConfiguration());
    }
    mergeCommunicationScheduledPublication(communicationA, communicationB);

    if (communicationB.hasRenditions()) {
      communicationA.setRenditions(communicationB.getRenditions());
    }
  }

  /**
   * Merge the communication configuration data.
   * 
   * @param configB the communication configuration
   */
  private void mergeCommunicationConfiguration(final CommunicationConfiguration configA,
    final CommunicationConfiguration configB) {
    if (configB.hasVisibility()) {
      configA.setVisibility(configB.getVisibility());
    }
    if (configB.hasAllowAnonymousViewings()) {
      configA.setAllowAnonymousViewings(configB.allowAnonymousViewings());
    }
    if (configB.hasExcludeFromChannelContentPlan()) {
      configA.setExcludeFromChannelContentPlan(configB.excludeFromChannelContentPlan());
    }
    if (configB.hasShowChannelSurvey()) {
      configA.setShowChannelSurvey(configB.showChannelSurvey());
    }
    configA.setCustomUrl(configB.getCustomUrl());
    configA.setClientBookingReference(configB.getClientBookingReference());
  }

  /**
   * Merge the communication scheduled publication data.
   * 
   * @param config the communication scheduled publication
   */
  private void mergeCommunicationScheduledPublication(final Communication communicationA,
    final Communication communicationB) {
    if (!communicationA.hasScheduledPublication()) {
      CommunicationScheduledPublication scheduledPublication = new CommunicationScheduledPublication();
      communicationA.setScheduledPublication(scheduledPublication);
    }
    if (communicationB.hasScheduledPublication()) {
      CommunicationScheduledPublication scheduledPublication = communicationB.getScheduledPublication();
      if (scheduledPublication.hasPublicationDate()) {
        communicationA.getScheduledPublication().setPublicationDate(scheduledPublication.getPublicationDate());
      }
      if (scheduledPublication.hasUnpublicationDate()) {
        communicationA.getScheduledPublication().setUnpublicationDate(scheduledPublication.getUnpublicationDate());
      }
    }
  }

  /**
   * Merge any syndicated override data into this communication.
   * 
   * @param communication that will be merged into
   * @param syndicatedOverrideData the syndicated override data to add.
   */
  public void mergeSyndicatedOverrideData(final Communication communication,
    final CommunicationOverride syndicatedOverrideData) {
    if (syndicatedOverrideData.isTitleOverridden()) {
      communication.setTitle(syndicatedOverrideData.getTitle());
    }
    if (syndicatedOverrideData.isDescriptionOverridden()) {
      communication.setDescription(syndicatedOverrideData.getDescription());
    }
    if (syndicatedOverrideData.isAuthorsOverridden()) {
      communication.setAuthors(syndicatedOverrideData.getAuthors());
    }
    if (syndicatedOverrideData.isKeywordsOverridden()) {
      communication.setKeywords(syndicatedOverrideData.getKeywords());
    }
  }

}