/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CreateChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Interface for rescheduling a webcast related operations queue for Email.
 * 
 * The operations that need to be done asynchronously include tasks that must be performed by other services.
 * 
 * To perform those operations service API has a separate callback method.
 */
public interface EmailQueuer {

  /**
   * Perform asynchronous operations on a given communication required when performing a create webcast operation.
   * 
   * @param webcastId id of the webcast created
   */
  void create(Long webcastId);

  /**
   * Perform asynchronous operations on a given communication required when performing a reschedule webcast operation.
   * 
   * @param webcastId id of the webcast rescheduled
   */
  void reschedule(Long webcastId);

  /**
   * Perform asynchronous operations on a given communication required when performing a cancel webcast operation.
   * 
   * @param webcastId id of the webcast cancelled
   */
  void cancel(Long webcastId);

  /**
   * Perform asynchronous operations on a given communication required when performing a missed you webcast operation.
   * 
   * @param webcastId id of the webcast the registered have missed.
   */
  void missedYou(Long webcastId);

  /**
   * Perform asynchronous operations on a given communication required when performing a recording published webcast
   * operation.
   * 
   * @param webcastId id of the webcast that is recorded.
   */
  void recordingPublished(Long webcastId);

  /**
   * Perform asynchronous operations on a given communication required when a video upload operation has been
   * successful.
   * 
   * @param webcastId id of the webcast cancelled
   * @param provider of the webcast
   */
  void videoUploadComplete(Long webcastId, String provider);

  /**
   * Perform asynchronous operations on a given communication required when a video upload operation has been failed.
   * 
   * @param webcastId id of the webcast cancelled
   * @param provider of the webcast
   */
  void videoUploadError(Long webcastId, String provider);
}
