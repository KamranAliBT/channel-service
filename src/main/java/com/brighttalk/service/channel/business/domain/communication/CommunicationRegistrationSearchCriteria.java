/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationRegistrationSearchCriteria.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.brighttalk.common.error.ApplicationException;

/**
 * Search Criteria for retrieving the paginated collection of communication registrations.
 * Contains the page number being requested, the size of the page being requested and communication status filter
 * criteria.
 * The default communication status filter is 'all'.
 */
public class CommunicationRegistrationSearchCriteria {

  private Integer pageNumber;

  private Integer pageSize;

  private List<Long> channelIds;

  private CommunicationStatusFilterType communicationStatusFilter;

  /**
   * Supported communication status filters.
   */
  public static enum CommunicationStatusFilterType {

    /**
     * Filter upcoming communication status.
     */
    UPCOMING,

    /**
     * Filter live communication status.
     */
    LIVE,

    /**
     * Filter recorded communication status.
     */
    RECORDED;

    /**
     * Converts a string into a CommunicationStatusFilterType.
     * 
     * An empty value means that no filtering is requested.
     * 
     * @param value to be converted.
     *
     * @return {@link CommunicationStatusFilterType}
     * 
     * @throws ApplicationException if the value is invalid 
     */
    public static CommunicationStatusFilterType convert(String value) {

      if (StringUtils.isEmpty(value)) {
        return null;
      }

      try {

        return CommunicationStatusFilterType.valueOf(value.trim().toUpperCase());

      } catch (IllegalArgumentException exception) {

        throw new ApplicationException("Invalid communication status filter requested [" + value + "]", exception);
      }
    }
  }

  public Integer getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(Integer value) {
    pageNumber = value;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer value) {
    pageSize = value;
  }

  /**
   * Indicates if this criteria have list of filter channel ids defined.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasChannelIds() {
    return channelIds != null;
  }

  public List<Long> getChannelIds() {
    return channelIds;
  }

  public void setChannelIds(List<Long> value) {
    channelIds = value;
  }

  /**
   * Indicates if this criteria has communication status filter defined.
   * 
   * Default value "all" indicates that there is no filtering on communication status
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasCommunicationStatusFilter() {
    return communicationStatusFilter != null;
  }

  public CommunicationStatusFilterType getCommunicationStatusFilter() {
    return communicationStatusFilter;
  }

  public void setCommunicationStatusFilter(CommunicationStatusFilterType value) {
    communicationStatusFilter = value;
  }
}