/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: Channel.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.Category;
import com.brighttalk.service.channel.business.domain.Entity;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.validator.Validator;

/**
 * Represents a BrightTALK channel.
 */
public class Channel extends Entity implements Serializable {

  /**
   * Class version identifier. Used to determine whether previously serialised instances can be safely de-serialised
   * back to this version of the class.
   * <p>
   * Manually manage the version id, rather than having it generated by the JVM, to avoid unexpected class version
   * mismatches.
   * 
   * @see Serializable
   */
  private static final long serialVersionUID = 1L;

  /** The title of this channel. */
  private String title;

  /** List of Keywords for this channel */
  private List<String> keywords;

  /** The strap line for this channel */
  private String strapline;

  /** The organiation who created this channel */
  private String organisation;

  /** The description of this channel */
  private String description;

  /** The owner id of this channel */
  private Long ownerUserId;

  /** The managers of this channel */
  private List<ChannelManager> channelManagers;

  /** Indicates if this channel is active or not. */
  private boolean active;

  /** Indicates if this channel is promoted on BrightTALK. */
  private boolean promotedOnHomepage;

  /** Indicates if this channel is searchable on BrightTALK. */
  private Searchable searchable;

  /** This channels actual type. */
  private ChannelType type;

  /** The type this channel was created as. */
  private ChannelType createdType;

  /** The type this channel is pending to be set to. */
  private ChannelType pendingType;

  /**
   * The channel statistics data of this channel.
   * 
   * ChannelStatistics are defined as rating, viewedFor, numberOfLiveCommunications, numberOfUpcomingCommunications,
   * numberOfRecordedCommunications
   * */
  private ChannelStatistics statistics;

  /** This channels survey. */
  private Survey survey;

  /** This channels list of features. */
  private List<Feature> features;

  /** The categories associated with this channel. */
  private List<Category> categories;

  /**
   * The URL for this channel on BrightTALK.com. Typically points to this channels page on the BrightTALK portal and
   * supplied in this channels ATOM or RSS feed.
   */
  private String url;

  /**
   * The URL of this channels feed (ATOM version). Typically used in the public ATOM feed to point to this channels
   * feed. A configurable property.
   */
  private String feedUrlAtom;

  /**
   * The URL of this channels feed (RSS Version). Typically used in the public RSS feed to point to the feed.
   */
  private String feedUrlRss;

  private List<Communication> featuredCommunications;

  /** The searchable types for a channel. */
  public static enum Searchable {

    /** This channel is included in searching. */
    INCLUDED,
    /** This channel is excluded from searching. */
    EXCLUDED,
    /** This channel will only be included in searching if it meets search rules. */
    RULE_BASED
  }

  /**
   * Flag used to indicate if the channel title has been updated. This flag set when merging an existing channel with a
   * new channel, then used in the channel title validation.
   */
  private boolean titleChanged;

  /**
   * Construct this channel as a new channel.
   */
  public Channel() {
  }

  /**
   * Construct this channel as an existing channel with an id.
   * 
   * @param id the id of the channel
   */
  public Channel(final Long id) {
    super(id);
  }

  /**
   * Checks if this channel is owned by the given user.
   * 
   * @param user the user to be verified
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isChannelOwner(final User user) {

    return ownerUserId.equals(user.getId());
  }

  /**
   * Checks if this channel is managed by the given user.
   * 
   * @param user the user to be verified
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isChannelManager(final User user) {

    boolean isChannelManager = false;
    Feature channelManagersFeature = getFeature(Feature.Type.CHANNEL_MANAGERS);
    boolean isChannelManagersFeatureEnabled = channelManagersFeature.isEnabled();

    if (isChannelManagersFeatureEnabled) {
      for (ChannelManager channelManager : channelManagers) {
        if (isChannelManager = channelManager.getUser().getId().equals(user.getId())) {
          break;
        }
      }
    }

    return isChannelManager;
  }

  /**
   * Indicates if the channel title has been changed.
   * 
   * @return <code>true</code> if the Channel title has modified through a channel update, <code>false</code> otherwise.
   */
  public boolean isTitleChanged() {
    return titleChanged;
  }

  /**
   * @param titleHasChanged the titleHasChanged to set
   */
  public void setTitleChanged(boolean titleHasChanged) {
    this.titleChanged = titleHasChanged;
  }

  /**
   * @return the title of this channel or <code>null</code> if no title.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Indicates if this channel has a title.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasTitle() {
    return title != null;
  }

  /**
   * Set the title on this channel.
   * 
   * @param title the title to set.
   */
  public void setTitle(final String title) {
    this.title = title;
  }

  /**
   * @return the keywords for this channel as a string list or <code>null</code> if no keywords.
   */
  public List<String> getKeywords() {
    return keywords;
  }

  /**
   * Indicates if this channel has a title.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasKeywords() {
    return keywords != null;
  }

  /**
   * Set the keywords on this channel.
   * 
   * @param keywords List of string keywords.
   */
  public void setKeywords(final List<String> keywords) {
    this.keywords = keywords;
  }

  /**
   * @return this channels organisation or <code>null</code> if no organisation
   */
  public String getOrganisation() {
    return organisation;
  }

  /**
   * Set this channels organisation.
   * 
   * @param organisation the organisation to set
   */
  public void setOrganisation(final String organisation) {
    this.organisation = organisation;
  }

  /**
   * Indicates if this channel has a organisation.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasOrganisation() {
    return organisation != null;
  }

  /**
   * @return this channels description or <code>null</code> if no description
   */
  public String getDescription() {
    return description;
  }

  /**
   * Set this channels description.
   * 
   * @param description the description to set
   */
  public void setDescription(final String description) {
    this.description = description;
  }

  /**
   * Indicates if this channel has a description.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasDescription() {
    return description != null;
  }

  /**
   * @return this channels strapline or <code>null</code> if not set
   */
  public String getStrapline() {
    return strapline;
  }

  /**
   * Set this channels strapline.
   * 
   * @param strapline the strapline to set
   */
  public void setStrapline(final String strapline) {
    this.strapline = strapline;
  }

  /**
   * Indicates if this channel has a strapline.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasStrapline() {
    return strapline != null;
  }

  /**
   * @return the id of the user who owns this channel or <code>null</code> if not set.
   */
  public Long getOwnerUserId() {
    return ownerUserId;
  }

  /**
   * Set the user who owns this channel.
   * 
   * @param ownerUserId the owner owner id to set
   */
  public void setOwnerUserId(final Long ownerUserId) {
    this.ownerUserId = ownerUserId;
  }

  /**
   * Indicates if this channel has an owner id.
   * 
   * @return "<code>true</code> if channel has owner user ID, <code>false</code> otherwise"
   */
  public boolean hasOwnerUserId() {
    return ownerUserId != null;
  }

  /**
   * @return the channel managers for this channel.
   */
  public List<ChannelManager> getChannelManagers() {
    return channelManagers;
  }

  /**
   * Set the channel managers for this channel.
   * 
   * @param channelManagers the channel managers to set
   */
  public void setChannelManagers(final List<ChannelManager> channelManagers) {
    this.channelManagers = channelManagers;
  }

  /**
   * @return the statistics of this channel or <code>null</code> if not set.
   */
  public ChannelStatistics getStatistics() {
    return statistics;
  }

  /**
   * Set this channel statistics.
   * 
   * @param statistics the channel statistics to be set.
   */
  public void setStatistics(final ChannelStatistics statistics) {
    this.statistics = statistics;
  }

  /**
   * Set this channel as active or inactive.
   * 
   * @param active the active status.
   */
  public void setActive(final boolean active) {
    this.active = active;
  }

  /**
   * Indicates if this channel is active.
   * 
   * @return true or false
   */
  public boolean isActive() {
    return active;
  }

  /**
   * @return the searchable type of this channel.
   */
  public Searchable getSearchable() {
    return searchable;
  }

  /**
   * Set this searchable type for this channel.
   * 
   * @param searchable the searchable type to set.
   */
  public void setSearchable(final Searchable searchable) {
    this.searchable = searchable;
  }

  /**
   * Indicates if this channel is included in search queries.
   * 
   * @return true or false
   */
  public boolean isIncluded() {
    return Searchable.INCLUDED.equals(searchable);
  }

  /**
   * Indicates if this channel is excluded from search queries.
   * 
   * @return true or false
   */
  public boolean isExcluded() {
    return Searchable.EXCLUDED.equals(searchable);
  }

  /**
   * Indicates if this channel is included in search queries based on search rules. Typically these are that the channel
   * must be viewed for a certain amount of minutes.
   * 
   * @return true or false
   */
  public boolean isRuleBased() {
    return Searchable.RULE_BASED.equals(searchable);
  }

  /**
   * Set the promote on BrightTALK indicator on this channel.
   * 
   * @param promotedOnHomepage the promotedOnHomepage to set
   */
  public void setPromotedOnHomepage(final boolean promotedOnHomepage) {
    this.promotedOnHomepage = promotedOnHomepage;
  }

  /**
   * Indicates if this channel is promoted on BrightTALK.
   * 
   * @return true or false
   */
  public boolean isPromotedOnHomepage() {
    return promotedOnHomepage;
  }

  /**
   * @return the type of this channel.
   */
  public ChannelType getType() {
    return type;
  }

  /**
   * Set this channels type.
   * 
   * @param type the channel type to set.
   */
  public void setType(final ChannelType type) {
    this.type = type;
  }

  /**
   * @return the channel type used when this channel was created.
   */
  public ChannelType getCreatedType() {
    return createdType;
  }

  /**
   * Set the created type for this channel.
   * 
   * @param createdType the channel type to set.
   */
  public void setCreatedType(final ChannelType createdType) {
    this.createdType = createdType;
  }

  /**
   * @return the pending type for this channel.
   */
  public ChannelType getPendingType() {
    return pendingType;
  }

  /**
   * Indicates if this channel is pending for approval of another channel type.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isPendingApproval() {
    return pendingType != null;
  }

  /**
   * Set the pending type for this channel.
   * 
   * @param pendingType the channel type to set.
   */
  public void setPendingType(final ChannelType pendingType) {
    this.pendingType = pendingType;
  }

  /**
   * @return this channels survey or <code>null</code> if no survey.
   */
  public Survey getSurvey() {
    return survey;
  }

  /**
   * Set the survey for this channel.
   * 
   * @param survey the survey to set.
   */
  public void setSurvey(final Survey survey) {
    this.survey = survey;
  }

  /**
   * Indicates if this channel has a survey.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasSurvey() {
    return getSurvey() != null;
  }

  /**
   * @return the list of features of this channel.
   */
  public List<Feature> getFeatures() {
    return features;
  }

  /**
   * Set the list of features for this channel.
   * 
   * @param features the list of features to set.
   */
  public void setFeatures(final List<Feature> features) {
    this.features = features;
  }

  /**
   * Indicates if this channel has any features.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasFeatures() {
    return getFeatures() != null;
  }

  /**
   * Add a single feature to a channel.
   * 
   * @param feature The Feature to add
   */
  public void addFeature(final Feature feature) {
    if (features == null) {
      features = new ArrayList<Feature>();
    }

    features.add(feature);
  }

  /**
   * Get the requested feature from this channel.
   * 
   * @param featureType the requested feature
   * 
   * @return the feature.
   * 
   * @throws ApplicationException if feature for a given feature type is not found
   */
  public Feature getFeature(final Feature.Type featureType) {

    for (Feature feature : features) {
      if (feature.getName().equals(featureType)) {
        return feature;
      }
    }

    throw new ApplicationException("Feature [" + featureType.toString() + "] not found for channel [" + getId() + "].");
  }

  /**
   * Get this channels categories.
   * 
   * @return this channels categories or an empty list if this channel has no categories.
   */
  public List<Category> getCategories() {
    return categories;
  }

  /**
   * Set this channels categories.
   * 
   * @param categories the categories to set.
   */
  public void setCategories(final List<Category> categories) {
    this.categories = categories;
  }

  /**
   * Add a single category to a channel.
   * 
   * @param category The Category to add
   */
  public void addCategory(final Category category) {
    if (categories == null) {
      categories = new ArrayList<Category>();
    }

    categories.add(category);
  }

  /**
   * Indicates if this channel has any categories.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasCategories() {
    return getCategories() != null;
  }

  /**
   * Get the URL of this channel on the brighttalk portal. Typically sent out in this channels feed.
   * 
   * @return the URL of this channel. E.g. http://www.brighttalk.com/channel/288
   */
  public String getUrl() {
    return url;
  }

  /**
   * Set the URL of this channel on the portal.
   * 
   * @param url the url to set.
   */
  public void setUrl(final String url) {
    this.url = url;
  }

  /**
   * Indicates if this channel has its portal channel url set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasUrl() {
    return StringUtils.isNotEmpty(url);
  }

  /**
   * Get the ATOM feed URL for this channel. Used in the ATOM feed itself.
   * 
   * @return the atom feed URL, e.g. http://www.test01.brighttalk.net/channel/5030/feed
   */
  public String getFeedUrlAtom() {
    return feedUrlAtom;
  }

  /**
   * Is there an atom feed url for channel.
   * 
   * @return true or false
   */
  public boolean hasFeedUrlAtom() {
    return feedUrlAtom != null;
  }

  /**
   * Set the ATOM feed URL on this channel.
   * 
   * @param feedUrlAtom the atom feed url to set.
   */
  public void setFeedUrlAtom(final String feedUrlAtom) {
    this.feedUrlAtom = feedUrlAtom;
  }

  /**
   * Get the RSS feed URL for this channel. Used in the RSSfeed itself.
   * 
   * @return the atom feed URL, e.g. http://www.test01.brighttalk.net/channel/5030/feed/rss
   */
  public String getFeedUrlRss() {
    return feedUrlRss;
  }

  /**
   * Set the RSS feed URL on this channel.
   * 
   * @param feedUrlRss the rss feed url to set.
   */
  public void setFeedUrlRss(final String feedUrlRss) {
    this.feedUrlRss = feedUrlRss;
  }

  public List<Communication> getFeaturedCommunications() {
    return featuredCommunications;
  }

  public void setFeaturedCommunications(final List<Communication> value) {
    featuredCommunications = value;
  }

  /**
   * Are featured communications set on this channel.
   * 
   * @return true or false
   */
  public boolean hasFeaturedCommunications() {
    return featuredCommunications != null;
  }

  /**
   * Are statistics set on this channel.
   * 
   * @return true or false
   */
  public boolean hasStatistics() {
    return statistics != null;
  }

  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder("Channel\n");
    builder.append(super.toString());

    builder.append("[active=" + isActive() + "]\n");
    builder.append("[title=" + getTitle() + "]\n");
    builder.append("[strapline=" + getStrapline() + "]\n");
    builder.append("[keywords=" + getKeywords() + "]\n");
    builder.append("[organisation=" + getOrganisation() + "]\n");
    builder.append("[ownerUserId=" + getOwnerUserId() + "]\n");

    builder.append("[statistics=" + getStatistics() + "]\n");

    builder.append("[features=" + getFeatures() + "]\n");
    builder.append("[categories=" + getCategories() + "]\n");
    builder.append("[survey=" + getSurvey() + "]\n");

    builder.append("[searchable=" + getSearchable() + "]\n");
    builder.append("[promotedOnBTALK=" + isPromotedOnHomepage() + "]\n");

    builder.append("[url=" + getUrl() + "]\n");

    builder.append("[type=" + getType() + "]\n");
    builder.append("[createdType=" + getCreatedType() + "]\n");
    builder.append("[pendingType=" + getPendingType() + "]\n");

    return builder.toString();
  }

  /**
   * Validates this vote with the given validator.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<Channel> validator) {

    validator.validate(this);
  }

  /**
   * Merge the given channel into the current instance.
   * 
   * This will replace the values of the properties in the current instance with the value of the corresponding property
   * from the given instance, but only if set. The properties merged are only the ones required for channel update.
   * 
   * @param channel to be merged
   */
  public void merge(final Channel channel) {

    if (channel.hasTitle()) {
      setTitleChanged(!channel.getTitle().equals(title));
      setTitle(channel.getTitle());
    }

    if (channel.hasDescription()) {
      setDescription(channel.getDescription());
    }

    if (channel.hasKeywords()) {
      setKeywords(channel.getKeywords());
    }

    if (channel.hasOrganisation()) {
      setOrganisation(channel.getOrganisation());
    }

    if (channel.hasStrapline()) {
      setStrapline(channel.getStrapline());
    }

    if (channel.hasOwnerUserId()) {
      setOwnerUserId(channel.getOwnerUserId());
    }
  }

  /**
   * Authorise a user to perform some action on the channel. The user can either be a channel owner or a BrightTALK
   * manager.
   * 
   * @param user the user to authorise
   * @throws UserAuthorisationException thrown if the user does not match any of the conditions.
   */
  public void isAuthorised(final User user) {
    if (!isChannelOwner(user) && !isChannelManager(user) && !user.isManager()) {
      throw new UserAuthorisationException("User [" + user.getId() + "] is not authorised in this channel.");
    }
  }

  /**
   * Check if a user is authorised on the channel. We validated if a presenter is authorised as well.
   * 
   * @param user the user to authorise on the channel.
   * @param presenter boolean to allow validating if a user is allowed or not.
   * @throws UserAuthorisationException if the user is not authorised on the channel.
   */
  public void isAuthorised(final User user, final boolean presenter) {
    if (!isChannelOwner(user) && !isChannelManager(user) && !user.isManager() && !presenter) {
      throw new UserAuthorisationException("User [" + user.getId() + "] is not authorised in this channel.");
    }
  }

  /**
   * Assert that the communication duration is not greater than the max communication duration, set as a feature on the
   * channel.
   * 
   * @param communication the communication to validate.
   * @return true if the communication duration is valid else false
   */
  public boolean isCommunicationDurationValid(final Communication communication) {
    Feature maxDurationInChannelFeature = getFeature(Type.MAX_COMM_LENGTH);

    int maxDurationInChannelValue = Integer.valueOf(maxDurationInChannelFeature.getValue());

    if (maxDurationInChannelFeature.isEnabled() && maxDurationInChannelValue > 0) {
      if (communication.getDuration() > maxDurationInChannelValue) {
        return false;
      }
    }
    return true;
  }

  /**
   * returns if the syndications included in content plan feature is enabled for the channel.
   * 
   * @return the true if the feature is enabled.
   */
  public boolean includeSyndicatedContent() {
    Feature includeSyndicatedContentFeature = getFeature(Type.SYNDICATIONS_INCLUDED_IN_CONTENT_PLAN);
    return includeSyndicatedContentFeature.isEnabled();

  }

}
