/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SurveyService.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import com.brighttalk.service.channel.business.domain.Survey;

/**
 * The Survey Service Business API.
 * <p>
 * The Survey Service is responsible for managing {@link Survey surveys}.
 */
public interface SurveyService {

  /**
   * Find the Survey a given survey id.
   * 
   * This supports both channel and communication surveys.
   * 
   * @param surveyId The Id of the survey to look for
   * 
   * @return {@link Survey}
   * @throws com.brighttalk.service.channel.business.error.SurveyNotFoundException when there is neither channel nor
   * communication survey with given id exist. 
   */
  Survey findSurvey(Long surveyId);

  /**
   * Load the full details of the given survey.
   * 
   * This will Load the full form details into the given survey.
   * 
   * @param survey The survey to populate
   *
   * @return The populated survey
   * @throws com.brighttalk.service.channel.business.error.SurveyNotFoundException when form retrieved from
   * Survey Service doesn't exist or is empty
   */
  Survey loadSurvey(Survey survey);
}
