/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: ChannelsSearchCriteria.java 69499 2013-10-09 11:19:13Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;

/**
 * Search Criteria for retrieving the paginated channel managers list.
 */
public class ChannelManagersSearchCriteria extends BaseSearchCriteria {

  private static final String ERROR_SORT_ORDER_CRITERIA = "sortOrder";

  private static final String ERROR_SORT_BY_CRITERIA = "sortBy";

  private static final String CRITERIA_LAST_NAME = "lastName";

  private static final String CRITERIA_EMAIL = "email";

  private SortBy sortBy;

  /**
   * Sort Order types.
   */
  public static enum SortBy {

    /** Last name. */
    LAST_NAME(CRITERIA_LAST_NAME),

    /** Email address. */
    EMAIL_ADDRESS(CRITERIA_EMAIL);

    private final String name;

    /**
     * Default constructor.
     * 
     * @param name criteria name
     */
    SortBy(final String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    /**
     * Default converter.
     * 
     * @param value sort by criteria
     * 
     * @return sort by
     */
    public static SortBy convert(final String value) {

      if (StringUtils.isEmpty(value)) {
        return SortBy.LAST_NAME;
      }

      for (SortBy s : SortBy.values()) {
        if (value.equals(s.name)) {
          return s;
        }
      }

      throw new SearchCriteriaException(ERROR_SORT_BY_CRITERIA, value);
    }
  }

  public SortBy getSortBy() {
    return sortBy;
  }

  public void setSortBy(final SortBy sortBy) {
    this.sortBy = sortBy;
  }

  public void setSortBy(final String sortBy) {
    this.sortBy = SortBy.convert(sortBy);
  }

  /**
   * Set the string criteria as a sort order object.
   * 
   * @param sortOrder sort order criteria
   */
  public void setSortOrder(final String sortOrder) {

    if (StringUtils.isEmpty(sortOrder)) {
      super.setSortOrder(SortOrder.ASC);
      return;
    }

    try {

      super.setSortOrder(SortOrder.convert(sortOrder));

    } catch (ApplicationException e) {
      throw new SearchCriteriaException(ERROR_SORT_ORDER_CRITERIA, sortOrder);
    }
  }

  @Override
  public String toString() {

    ToStringBuilder builder = new ToStringBuilder(this);

    builder.append("pageNumber", getPageNumber());
    builder.append("pageSize", getPageSize());
    builder.append("sortBy", getSortBy().getName());
    builder.append("sortOrder", getSortOrder().name());

    return builder.toString();
  }

}
