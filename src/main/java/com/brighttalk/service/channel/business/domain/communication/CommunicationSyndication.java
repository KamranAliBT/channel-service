/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: codetemplates.xml 41756 2012-02-02 11:15:06Z rgreen $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.validator.CommunicationUpdateValidator;

/**
 * Class that manages the syndication of a webcast.
 */
public class CommunicationSyndication extends CommunicationConfiguration {

  /**
   * Class version identifier. Used to determine whether previously serialized instances can be safely de-serialized
   * back to this version of the class.
   * <p>
   * Manually manage the version id, rather than having it generated by the JVM, to avoid unexpected class version
   * mismatches.
   * 
   */
  private static final long serialVersionUID = 1L;

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(CommunicationUpdateValidator.class);

  /** The types of syndications the communication can have. */
  public static enum SyndicationType {
    /** The communication is syndicated in from another channel. */
    IN("in"),
    /** The communication is syndicated out to another channel. */
    OUT("out"),
    /** Unknown - should never happen. Probably a programming error. */
    UNKNOWN("unknown");

    private String type;

    /** @param type The syndication type. */
    private SyndicationType(String type) {
      this.type = type;
    }

    /** @return The syndication type. */
    public String getType() {
      return type;
    }

  }

  /** The status of syndication authorisation for the communication. */
  public static enum SyndicationAuthorisationStatus {
    /** Syndication has been approved. */
    APPROVED("approved"),
    /** Syndication has been declined. */
    DECLINED("declined"),
    /** Syndication approval is pending. */
    PENDING("pending"),
    /** Unknown - should never happen. Probably a programming error. */
    UNKNOWN("unknown");

    private String status;

    /** @param status The syndication status. */
    private SyndicationAuthorisationStatus(String status) {
      this.status = status;
    }

    /** @return The syndication status. */
    public String getStatus() {
      return status;
    }
  }

  /** The type of syndication authorisation. */
  public static enum SyndicationAuthorisationType {
    /** Master authorisation. */
    MASTER("master"),
    /** Consumer authorisation. */
    CONSUMER("consumer");

    private String type;

    /**
     * @param type The type authorisation type.
     */
    private SyndicationAuthorisationType(String type) {
      this.type = type;
    }

    /**
     * @return The type authorisation type, this could be either master or consumer.
     */
    public String getType() {
      return type;
    }

  }

  /**
   * The syndication type of the communication.
   * 
   * Indicates if the communication is syndicated in from another channel or out to another channel or not syndicated at
   * all.
   */
  private SyndicationType syndicationType;

  /** The channel in which the webcast has been syndicated. */
  private Channel channel;

  /**
   * The authorisation of the master communication.
   * 
   * Indicates if a master webcast has been approved, declined or pending to be syndicated out.
   */
  private SyndicationAuthorisationStatus masterChannelSyndicationAuthorisationStatus;

  /**
   * The time of authorisation of the master communication.
   * 
   * Indicates if time of the master webcast approval, decline or pending to be syndicated out.
   */
  private Date masterChannelSyndicationAuthorisationTimestamp;

  /**
   * The authorisation of the consumer communication.
   * 
   * Indicates if a consumer webcast has been approved, declined or pending to be syndicated in.
   */
  private SyndicationAuthorisationStatus consumerChannelSyndicationAuthorisationStatus;

  /**
   * The time of authorisation of the consumer communication.
   * 
   * Indicates if time of the consumer webcast approval, decline or pending to be syndicated in.
   */
  private Date consumerChannelSyndicationAuthorisationTimestamp;

  /** Get the master syndication. */
  private CommunicationSyndication masterSyndication;

  /** List of consumer syndications. */
  private List<CommunicationSyndication> consumerSyndications;

  /** The syndicated communication activity statistics. */
  private CommunicationStatistics communicationStatistics;

  /**
   * Default constructor.
   */
  public CommunicationSyndication() {
  }

  /**
   * @return The channel
   */
  public Channel getChannel() {
    return channel;
  }

  /**
   * @param channel the channel to set
   */
  public void setChannel(final Channel channel) {
    this.channel = channel;
  }

  /**
   * @return the masterChannelSyndicationAuthorisationStatus
   */
  public SyndicationAuthorisationStatus getMasterChannelSyndicationAuthorisationStatus() {
    return masterChannelSyndicationAuthorisationStatus;
  }

  /**
   * Set the master channel syndication authorization status on the communication.
   * 
   * @param masterChannelSyndicationAuthorisationStatus the syndicationAuthorisationMaster to set
   */
  public void setMasterChannelSyndicationAuthorisationStatus(
      final SyndicationAuthorisationStatus masterChannelSyndicationAuthorisationStatus) {
    this.masterChannelSyndicationAuthorisationStatus = masterChannelSyndicationAuthorisationStatus;
  }

  /**
   * Set the master channel syndication authorization status on the communication, based on the supplied String status.
   * 
   * @param status The syndication status to set.
   */
  public void setMasterChannelSyndicationAuthorisationStatus(final String status) {
    if (StringUtils.isNotEmpty(status)) {
      this.masterChannelSyndicationAuthorisationStatus = SyndicationAuthorisationStatus.valueOf(status.toUpperCase());
    }
  }

  /**
   * @return the masterChannelSyndicationAuthorisationTimestamp
   */
  public Date getMasterChannelSyndicationAuthorisationTimestamp() {
    return masterChannelSyndicationAuthorisationTimestamp;
  }

  /**
   * @param masterChannelSyndicationAuthorisationTimestamp the syndicationAuthorisationMasterTimestamp to set
   */
  public void setMasterChannelSyndicationAuthorisationTimestamp(
      final Date masterChannelSyndicationAuthorisationTimestamp) {
    this.masterChannelSyndicationAuthorisationTimestamp = masterChannelSyndicationAuthorisationTimestamp;
  }

  /**
   * @return the consumerChannelSyndicationAuthorisationStatus
   */
  public SyndicationAuthorisationStatus getConsumerChannelSyndicationAuthorisationStatus() {
    return consumerChannelSyndicationAuthorisationStatus;
  }

  /**
   * Set the consumer channel syndication authorization status on the communication.
   * 
   * @param consumerChannelSyndicationAuthorisationStatus the syndicationAuthorisationConsumer to set
   */
  public void setConsumerChannelSyndicationAuthorisationStatus(
      final SyndicationAuthorisationStatus consumerChannelSyndicationAuthorisationStatus) {
    this.consumerChannelSyndicationAuthorisationStatus = consumerChannelSyndicationAuthorisationStatus;
  }

  /**
   * Set the consumer channel syndication authorization status on the communication, based on the supplied String
   * status.
   * 
   * @param status The syndication status to set.
   */
  public void setConsumerChannelSyndicationAuthorisationStatus(final String status) {
    if (StringUtils.isNotEmpty(status)) {
      this.consumerChannelSyndicationAuthorisationStatus = SyndicationAuthorisationStatus.valueOf(status.toUpperCase());
    }
  }

  /**
   * @return the consumerChannelSyndicationAuthorisationTimestamp
   */
  public Date getConsumerChannelSyndicationAuthorisationTimestamp() {
    return consumerChannelSyndicationAuthorisationTimestamp;
  }

  /**
   * @param consumerChannelSyndicationAuthorisationTimestamp the syndicationAuthorisationConsumerTimestamp to set
   */
  public void setConsumerChannelSyndicationAuthorisationTimestamp(
      final Date consumerChannelSyndicationAuthorisationTimestamp) {
    this.consumerChannelSyndicationAuthorisationTimestamp = consumerChannelSyndicationAuthorisationTimestamp;
  }

  /**
   * Indicate if the communication is NOT syndicated.
   * 
   * @return <code>true</code> if the communication is NOT syndicated else <code>false</code>
   * 
   * @see #isSyndicated()
   */
  public boolean isNotSyndicated() {
    return this.getSyndicationType() == null;
  }

  /**
   * Indicate if the communication is syndicated.
   * 
   * @return <code>true</code> if the communication is syndicated else <code>false</code>
   * 
   * @see #isSyndicatedIn()
   * @see #isSyndicatedOut()
   */
  public boolean isSyndicated() {
    return this.getSyndicationType() != null;
  }

  /**
   * Indicate if the communication is syndicated IN.
   * 
   * @return <code>true</code> if the communication is syndicated in else <code>false</code>
   * 
   * @see #isSyndicated()
   * @see #isSyndicatedOut()
   */
  public boolean isSyndicatedIn() {
    return SyndicationType.IN.equals(syndicationType);
  }

  /**
   * Indicate if the communication is syndicated OUT.
   * 
   * @return <code>true</code> if the communication is syndicated out else <code>false</code>
   * 
   * @see #isSyndicated()
   * @see #isSyndicatedIn()
   */
  public boolean isSyndicatedOut() {
    return SyndicationType.OUT.equals(syndicationType);
  }

  /**
   * Get the syndicated type for the communication.
   * 
   * A communication can be syndicated IN to a channel, syndicated OUT to a channel or not syndicated. The syndicated
   * type indicates which.
   * 
   * @return the syndicated type.
   * 
   * @see #isSyndicated()
   * @see #isSyndicatedOut()
   * @see #isSyndicatedIn()
   */
  public SyndicationType getSyndicationType() {
    return syndicationType;
  }

  /**
   * Set the syndication type for the communication i.e. if this communication is syndicated in or out or not
   * syndicated.
   * 
   * @param syndicationType the syndicated type to set.
   */
  public void setSyndicationType(final SyndicationType syndicationType) {
    this.syndicationType = syndicationType;
  }

  /**
   * Set the syndication type for the communication i.e. if this communication is syndicated in or out or not
   * syndicated, based on the supplied String type.
   * 
   * @param type the syndicated type to set.
   */
  public void setSyndicationType(final String type) {
    if (StringUtils.isNotEmpty(type)) {
      this.syndicationType = SyndicationType.valueOf(type.toUpperCase());
    }
  }

  /**
   * Indicate if the communication is fully authorized for syndication.
   * <p>
   * A communication is fully authorized for syndication if and only if its been authorized in the master channel (the
   * channel owner is happy to syndicated it out) AND its been authorized in the consuming channel (the channel owner is
   * happy to accept it).
   * 
   * @return <code>true</code> if the communication is fully authorized else <code>false</code>.
   */
  public boolean isFullyAuthorisedForSyndication() {
    return this.isAuthorisedForSyndicationInMasterChannel() && this.isAuthorisedForSyndicationInConsumerChannel();
  }

  /**
   * Indicate if the communication is authorized for syndication in its master channel.
   * <p>
   * A communication has to be authorized for syndication in its master channel. (the channel owner is happy to
   * syndicated it out) AND consuming channel (the channel owner is happy to accept it).
   * 
   * @return <code>true</code> if the communication is authorized for syndication in its master channel.
   * <code>false</code>.
   */
  private boolean isAuthorisedForSyndicationInMasterChannel() {
    return SyndicationAuthorisationStatus.APPROVED.equals(masterChannelSyndicationAuthorisationStatus);
  }

  /**
   * Indicate if the communication is authorized for syndication in its consumer channel.
   * <p>
   * A communication has to be authorized for syndication in its master channel. (the channel owner is happy to
   * syndicated it out) AND consuming channel (the channel owner is happy to accept it).
   * 
   * @return <code>true</code> if the communication is authorized for syndication in its consuming channel.
   * <code>false</code>.
   */
  private boolean isAuthorisedForSyndicationInConsumerChannel() {
    return SyndicationAuthorisationStatus.APPROVED.equals(consumerChannelSyndicationAuthorisationStatus);
  }

  /**
   * @return the masterSyndication
   */
  public CommunicationSyndication getMasterSyndication() {
    return masterSyndication;
  }

  /**
   * @param masterSyndication the masterSyndication to set
   */
  public void setMasterSyndication(final CommunicationSyndication masterSyndication) {
    this.masterSyndication = masterSyndication;
  }

  /**
   * @return the consumerSyndications
   */
  public List<CommunicationSyndication> getConsumerSyndications() {
    return consumerSyndications;
  }

  /**
   * @param consumerSyndications the consumerSyndications to set
   */
  public void setConsumerSyndications(final List<CommunicationSyndication> consumerSyndications) {
    this.consumerSyndications = consumerSyndications;
  }

  /**
   * @return the communicationStatistics
   */
  public CommunicationStatistics getCommunicationStatistics() {
    return communicationStatistics;
  }

  /**
   * @param communicationStatistics the communicationStatistics to set
   */
  public void setCommunicationStatistics(CommunicationStatistics communicationStatistics) {
    this.communicationStatistics = communicationStatistics;
  }

  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("syndicationType", syndicationType);
    builder.append("channel", channel);
    builder.append("masterChannelSyndicationAuthorisationStatus", masterChannelSyndicationAuthorisationStatus);
    builder.append("masterChannelSyndicationAuthorisationTimestamp",
        masterChannelSyndicationAuthorisationTimestamp);
    builder.append("consumerChannelSyndicationAuthorisationStatus",
        consumerChannelSyndicationAuthorisationStatus);
    builder.append("consumerChannelSyndicationAuthorisationTimestamp",
        consumerChannelSyndicationAuthorisationTimestamp);
    builder.append("communicationStatistics", communicationStatistics);
    return builder.toString();
  }
}
