/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SummitNotFoundException.java 98710 2015-08-03 09:52:40Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

/**
 * Indicates that the summit was not found.
 */
public class SummitNotFoundException extends NotFoundException {

  /**
   * serial version uid.
   */
  private static final long serialVersionUID = 1L;
  private final Long summitId;

  /**
   * @param summitId see below.
   * 
   * @see SummitNotFoundException#SummitNotFoundException(Long, Throwable)
   */
  public SummitNotFoundException(final Long summitId) {
    this(summitId, null);
  }

  /**
   * @param summitId see below.
   * @param cause see below.
   * 
   * @see NotFoundException#NotFoundException(String, Throwable)
   */
  public SummitNotFoundException(final Long summitId, final Throwable cause) {
    super("Summit [" + summitId + "] not found.", cause);
    this.summitId = summitId;
  }

  /**
   * throw communication not found exception.
   * 
   * @param message to set
   * @param cause of the throwing
   */
  public SummitNotFoundException(final String message, final Throwable cause) {
    super(message, cause);
    summitId = null;
  }

  /**
   * throw communication not found exception.
   * 
   * @param message The exception message to set.
   */
  public SummitNotFoundException(final String message) {
    super(message);
    summitId = null;
  }

  /**
   * @return The summit Id.
   */
  public Long getSummitId() {
    return summitId;
  }

}
