/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: Subscription.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Represents a channel subscription.
 */
public class Subscription {

  /** Referral types. */
  public enum Referral {
    /** BrightTALK referral. */
    dotcom,

    /** External referral. */
    notdotcom,

    /** Audience Import referral. */
    paid
  }

  private Long id;

  private User user;

  private Referral referral;

  private SubscriptionContext context;

  private Date lastSubscribed;

  private Boolean active;

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(final User user) {
    this.user = user;
  }

  public Referral getReferral() {
    return referral;
  }

  public void setReferral(final Referral referral) {
    this.referral = referral;
  }

  public Date getLastSubscribed() {
    return lastSubscribed;
  }

  public void setLastSubscribed(final Date lastSubscribed) {
    this.lastSubscribed = lastSubscribed;
  }

  public Boolean isActive() {
    return active;
  }

  /**
   * Validates if this subscription has paid referral.
   * 
   * @return <code>true</code> if the referral is paid, otherwise <code>false</code>
   */
  public boolean isPaidReferral() {
    return Referral.paid.equals(referral);
  }

  public void setActive(final Boolean active) {
    this.active = active;
  }

  /**
   * @return the context
   */
  public SubscriptionContext getContext() {
    return context;
  }

  /**
   * @param context the context to set
   */
  public void setContext(SubscriptionContext context) {
    this.context = context;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if (!(obj instanceof Subscription)) {
      return false;
    }

    Subscription subscription = (Subscription) obj;

    if (id != null) {
      return id.equals(subscription.getId());
    }

    return false;
  }

  @Override
  public int hashCode() {
    int result = id == null ? 0 : id.hashCode();
    return result * 37;
  }

  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("id", id);
    builder.append("referral", referral);
    builder.append("context", context);
    builder.append("user", user);
    builder.append("lastSubscribed", lastSubscribed);
    builder.append("active", active);
    return builder.toString();
  }

}
