/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CannotUpdateCurrentProviderException.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that the requested communication cannot be updated, because the current provider value cannot be changed.
 */
@SuppressWarnings("serial")
public class CannotUpdateCurrentProviderException extends ApplicationException {

  private static final String ERROR_CODE = "CannotUpdateCurrentProvider";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE}.
   */
  private final String errorCode = ERROR_CODE;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public CannotUpdateCurrentProviderException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public CannotUpdateCurrentProviderException(final String message, final String errorCode) {
    super(message, ERROR_CODE);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public CannotUpdateCurrentProviderException(final String message) {
    super(message, ERROR_CODE);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }
}