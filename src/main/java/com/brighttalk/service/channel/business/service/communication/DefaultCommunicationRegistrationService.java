/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationRegistrationService.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedListWithTotalPages;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria;
import com.brighttalk.service.channel.integration.database.CommunicationRegistrationsDbDao;

/**
 * Default implementation of the {@link CommunicationRegistrationService}.
 */
@Service
@Primary
public class DefaultCommunicationRegistrationService implements CommunicationRegistrationService {

  @Value("${communication.registrations.defaultPageSize}")
  private int defaultPageSize;

  @Value("${communication.registrations.maxPageSize}")
  private int maxPageSize;

  @Value("${communication.registrations.defaultPageNumber}")
  private int defaultPageNumber;

  @Autowired
  private CommunicationRegistrationsDbDao communicationRegistrationsDbDao;

  @Override
  public PaginatedListWithTotalPages<CommunicationRegistration> findRegistrations(User user,
      CommunicationRegistrationSearchCriteria searchCriteria) {

    int totalCommunicationRegistrations = this.communicationRegistrationsDbDao.findTotalRegistrations(user.getId(),
        searchCriteria);

    if (totalCommunicationRegistrations == 0) {
      return new PaginatedListWithTotalPages<CommunicationRegistration>();
    }

    int pageNumber = getPageNumber(searchCriteria);
    int pageSize = getPageSize(searchCriteria);

    searchCriteria.setPageNumber(pageNumber);
    searchCriteria.setPageSize(pageSize);

    PaginatedListWithTotalPages<CommunicationRegistration> paginatedRegistrations = new PaginatedListWithTotalPages<CommunicationRegistration>();
    paginatedRegistrations.setUnpaginatedTotal(totalCommunicationRegistrations);
    paginatedRegistrations.setCurrentPageNumber(pageNumber);
    paginatedRegistrations.setPageSize(pageSize);

    // validate if the page requested is within available page range.
    if (pageNumber > paginatedRegistrations.getLastPageNumber()
        || pageNumber < paginatedRegistrations.getFirstPageNumber()) {

      throw new ApplicationException("Accessing out of bounds communication registrations page [" + pageNumber
          + "] out of total [" + paginatedRegistrations.getLastPageNumber() + "]", "noSuchPage");
    }

    List<CommunicationRegistration> communicationRegistrations = this.communicationRegistrationsDbDao.findRegistrations(
        user.getId(), searchCriteria);
    paginatedRegistrations.addAll(communicationRegistrations);

    return paginatedRegistrations;
  }

  /**
   * Get the page number for a search.
   * 
   * If no page number given in the search criteria this will use the default page number
   * 
   * @param searchCriteria The search criteria
   * 
   * @return The page number to use
   */
  private int getPageNumber(CommunicationRegistrationSearchCriteria searchCriteria) {

    Integer pageNumber = searchCriteria.getPageNumber();

    if (pageNumber == null) {
      return this.defaultPageNumber;
    }

    return pageNumber;
  }

  /**
   * Get the Page size for a search.
   * 
   * If no page size is given in the request the default page size will be used
   * 
   * If the requested page size is larger than the requested size the max page size will be used
   * 
   * @param searchCriteria The search criteria
   * 
   * @return The page size to use
   */
  private int getPageSize(CommunicationRegistrationSearchCriteria searchCriteria) {

    Integer pageSize = searchCriteria.getPageSize();

    if (pageSize == null) {
      return this.defaultPageSize;
    }

    if (pageSize > this.maxPageSize) {
      return this.maxPageSize;
    }
    return pageSize;
  }

  public int getDefaultPageSize() {
    return this.defaultPageSize;
  }

  public void setDefaultPageSize(int defaultPageSize) {
    this.defaultPageSize = defaultPageSize;
  }

  public int getMaxPageSize() {
    return this.maxPageSize;
  }

  public void setMaxPageSize(int maxPageSize) {
    this.maxPageSize = maxPageSize;
  }

  public int getDefaultPageNumber() {
    return this.defaultPageNumber;
  }

  public void setDefaultPageNumber(int defaultPageNumber) {
    this.defaultPageNumber = defaultPageNumber;
  }

  public CommunicationRegistrationsDbDao getCommunicationRegistrationsDbDao() {
    return this.communicationRegistrationsDbDao;
  }

  public void setCommunicationRegistrationsDbDao(CommunicationRegistrationsDbDao communicationRegistrationsDbDao) {
    this.communicationRegistrationsDbDao = communicationRegistrationsDbDao;
  }
}