/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: Communication.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.Session;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Entity;
import com.brighttalk.service.channel.business.domain.MediazoneConfig;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.validator.Validator;
import com.brighttalk.service.channel.business.error.DialledInTooEarlyException;
import com.brighttalk.service.channel.business.error.DialledInTooLateException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.presentation.error.BadRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

/**
 * Represents a BrightTALK communication (i.e. a webcast).
 * <p/>
 * A communication can be an audio only, audio and slides or video communication. It can be published or unpublished. It
 * has the following states: upcoming, pending (a derived state based on time - 5 mins before live), live, processing
 * and recorded. Not all communications have all the states, for example video communications only have processing then
 * recorded states.
 * <p/>
 * A communication is scheduled (booked) for a certain date in given timezone, and a fixed duration. After the
 * communication is recorded it also has a recorded duration (which may differ from the booked duration as the live
 * communication may have run for a shorter time or may have overrun).
 * <p/>
 * A communication is booked in a channel (its master channel) then can be syndicated across many channels.
 * <p/>
 * A communication can be re-run if the original live gig had problems. The re-run count indicates how many times the
 * communication has been re-run.
 * <p/>
 * A (live) communication is presented by presenters who must dial into brighttalk and must enter the PIN code for the
 * communication.
 * <p/>
 * A communication has url for its thumbnail and preview images. These are images of the first slide (audio/slides
 * communication) or can be uploaded by presenters/channel owners (mediazone, video communications) or can be
 * automatically captured from the video (video communications).
 * <p/>
 * A communication can be published or unpublished. Unpublished communications cannot be viewed.
 * <p/>
 * If a communication is syndicated then various attributes on the syndicated communication can change in different
 * channels (e.g. title, description, whether its anonymous).
 * <p/>
 * 
 * @see CommunicationConfiguration communication configuration attributes for more details.
 */
public class Communication extends Entity implements Serializable {

  /** Class version identifier. */
  private static final long serialVersionUID = 1L;

  /**
   * Authoring offset time in milliseconds (900000 ms = 15 minutes). It used to determine if communication is in the 15
   * minute window before go live (out of authoring period).
   */
  private static final int AUTHORING_OFFSET_MILLI_SECONDS = 900000;

  /**
   * Pending offset time in milliseconds (30000 ms = 5 minutes). It used to determine if communication is in the 5
   * minute window before go live (pending starting).
   */
  private static final int PENDING_OFFSET_MILLI_SECONDS = 300000;

  /**
   * The pending offset in minutes. Used to determine the entry time for the communication. Entry time is 5 minutes
   * before the scheduled time (i.e. 5 minutes before go live).
   */
  private static final int PENDING_OFFSET_MINUTES = -5;

  /**
   * The time (offset) a communication is forced to close. Defined as a 5 min offset from the scheduled end time
   * (scheduled start + booked duration). So a view can overrun by 5 mins.
   */
  private static final int ENFORCED_CLOSE_OFFSET_MINUTES = 5;

  /** code when a feature image has been deleted from the portal. */
  public static final String FEATURE_IMAGE_DELETED = "deleted";

  /**
   * The channel id of this communication. If this communication is not syndicated, this is the same as the master
   * channel id. If this communication is syndicated this is the channel id the communication is syndicated out to or
   * the syndicated in to. The master (originating, original) channel id can be retrieved from this communications
   * master channel id.
   * 
   * @see #masterChannelId
   */
  private Long channelId;

  /**
   * The master channel id of this communication. If this communication is not syndicated, this is the same as the
   * channel id. If this communication is syndicated this is the id of the master channel - the (origination or
   * original) channel this communication was booked in.
   * 
   * @see #channelId
   */
  private Long masterChannelId;

  /** This communications title. */
  private String title;

  /** This communications description. */
  private String description;

  /** This communications keywords. Comma separated list. E.g finance, marketing. */
  private String keywords;

  /** This communications authors (aka presenters). Just a free text fields detailing one or more authors. */
  private String authors;

  /** The go-Live date and time of this communication. */
  private Date scheduledDateTime;

  /** The timezone this communication was booked in. e.g. Europe/London. */
  private String timeZone;

  /** Indicates if this communication is the featured communication, i.e. the one closest to now. */
  private boolean featured;

  /**
   * The duration of this communication. If the communication is upcoming this is the same as the booking duration. If
   * the communication is recorded this is the actual duration the communication ran for.
   * 
   * @see #bookingDuration
   */
  private Integer duration;

  /**
   * The booked duration for this communication (in seconds). This is the duration an upcoming communication was booked
   * for. It might, and probably will not be the same as the actual duration of the communication once it has run. (The
   * communication might end earlier or overrun).
   * 
   * @see #duration
   */
  private Integer bookingDuration;

  /**
   * The number of people who have registered to watch this communication. Before viewing a upcoming webcast, a user
   * must register to view that webcast, unless its anonymous.
   */
  private Integer numberOfRegistrants = 0;

  /** The status of this communication. E.g. Upcoming, Live, Recorded etc. */
  private Communication.Status status;

  /** The format of this communication. Indicates is this is an audio or video communication */
  private Communication.Format format;

  /** The published status of this communication. Indicates if this communication is published or unpublished. */
  private Communication.PublishStatus publishStatus;

  /** The provider of this communication eg: brighttalk, brighttalkhd, video or mediazone */
  private Provider provider;

  /**
   * The re-run count of this communication. Indicates how many times this communication has been re-run. A
   * communication is typically re-run if there has been a problem with its live gig and has to be run again. Normally
   * zero.
   */
  private Integer rerunCount = new Integer(0);

  /**
   * The pin number for this communication. A live communication is protected by a pin number and the presenter of this
   * communication must provide this pin number before they can present this communication.
   */
  private String pinNumber;

  /**
   * The live phone number for this communication. A live communication cannot be presented without dialling into
   * BrightTALK using this phone number.
   */
  private String livePhoneNumber;

  /**
   * The URL for the thumbnail image for this communication. Currently the relative path to the thumbnail image on the
   * SAN e.g. /communication/1/tn1_1.png
   */
  private String thumbnailUrl;

  private String calendarUrl;

  /**
   * The URL for the (full size) preview image for this communication. Currently the relative path to the thumbnail
   * image on the SAN e.g. /communication/1/slide1_001.png
   */
  private String previewUrl;

  /**
   * The URL to get the livestate information for this communication. e.g.
   * http://mp01.test01.brighttalk.net/service/livestate/xml
   */
  private String liveUrl;

  /**
   * The list of presenters that are permitted to access this communication. Presenters who can access a communication
   * have their session id's stored in the database (can_access table). We model presenter authorization by checking the
   * session id of the current user against the sessions in this table. If the current users session id matches a
   * session id in the table then the presenter is allowed to access this communication.
   */
  private List<Session> presenters;

  /** This communications resources aka attachments such as white papers, pdfs etc. */
  private List<Resource> resources;

  /** This communications configuration */
  private CommunicationConfiguration configuration;

  /** This communications viewing statistics. */
  private CommunicationStatistics communicationStatistics;

  /** This communications categories - may have none. */
  private List<CommunicationCategory> categories;

  /** The url to this communication on the portal */
  private String url;

  /** The url to this communication in the channel */
  private String channelWebcastUrl;

  /** Url of the conference room to be used when this commuication is live and being presented. */
  private String conferenceRoomUrl;

  /** Any Survey for this communication */
  private Survey survey;

  /** This communications status. */
  public static enum Status {
    /** Communication is live. */
    LIVE,
    /** Communication is recorded. */
    RECORDED,
    /** Communication is cancelled. */
    CANCELLED,
    /** Communication is deleted. */
    DELETED,
    /**
     * Communication is in pending status. Derived status. The communication is upcoming but is 5 minutes before go
     * live.
     */
    PENDING,
    /** Communication is upcoming. */
    UPCOMING,
    /**
     * Communication is processing. E.g its being recorded after a live presentation or renditions are being produced
     * (self service video).
     */
    PROCESSING,
    /**
     * When the encoding of the video has failed (self service video). This will not be set in the db.
     */
    PROCESSINGFAILED,
    /** Communication is in error. */
    ERROR,
    /**
     * Communication is in an unknown status- should never occur. Probably a programming error.
     */
    UNKNOWN
  }

  /** The publish status of this communication. */
  public static enum PublishStatus {
    /** This communication is published and therefore can be viewed. */
    PUBLISHED,
    /** This communication is unpublished and therefore cannot be viewed. */
    UNPUBLISHED
  }

  /** The format of this communication. */
  public static enum Format {
    /** This communication is audio (typically with slides). */
    AUDIO,
    /** This communication is video. */
    VIDEO,
    /** Unknown - should never occur. Probably a programming error. */
    UNKNOWN
  }

  private List<Rendition> renditions;

  /** Date of scheduled publication. Make the publication published. */
  private CommunicationScheduledPublication scheduledPublication;

  /** Indicates if this communication is rescheduled (scheduled date and time, duration, timezone have changed) */
  private boolean isRescheduled;

  /** Indicates is this communications publish status has changed. */
  private boolean publishStatusHasChanged;

  private CommunicationUrlGenerator calendarUrlGenerator;

  private CommunicationPortalUrlGenerator communicationPortalUrlGenerator;

  /**
   * The encoding count of this communication. Indicates how many times this communication has been successfuly encoded
   * by encoding.com. This is used for HD and video communications only.
   */
  private Integer encodingCount = new Integer(0);

  /**
   * the configuration for a mediazone type webcasts.
   */
  private MediazoneConfig mediazoneConfig;

  /** Construct this communication as a new communication. */
  public Communication() {
  }

  /**
   * Construct this communication as an existing communication with an id.
   * 
   * @param id the id of the communication
   */
  public Communication(final Long id) {
    super(id);
  }

  /**
   * Copy constructor. Make a copy of the given communication.
   * 
   * It is currently only used for the sending of reschedule and cancelled emails. So we only set the properties
   * required to be able to send the emails. It is not a complete nor deep copy.
   * 
   * @param communication to copy
   */
  public Communication(final Communication communication) {
    id = communication.getId();
    title = communication.getTitle();
    status = communication.getStatus();
    scheduledDateTime = communication.getScheduledDateTime();
    timeZone = communication.getTimeZone();
    authors = communication.getAuthors();
    channelId = communication.getChannelId();
    provider = communication.getProvider();
    format = communication.getFormat();
  }

  public Long getChannelId() {
    return channelId;
  }

  public void setChannelId(final Long channelId) {
    this.channelId = channelId;
  }

  public Long getMasterChannelId() {
    return masterChannelId;
  }

  public void setMasterChannelId(final Long masterChannelId) {
    this.masterChannelId = masterChannelId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  /**
   * Indicate if this communication has title set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasTitle() {
    return title != null;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  /**
   * Indicate if this communication has description set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasDescription() {
    return description != null;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(final String keywords) {
    this.keywords = keywords;
  }

  /**
   * Indicate if this communication has keywords set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasKeywords() {
    return keywords != null;
  }

  public String getAuthors() {
    return authors;
  }

  public void setAuthors(final String authors) {
    this.authors = authors;
  }

  /**
   * Indicate if this communication has authors set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasAuthors() {
    return authors != null;
  }

  public String getTimeZone() {
    return timeZone;
  }

  public void setTimeZone(final String timeZone) {
    this.timeZone = timeZone;
  }

  /**
   * check if timezone is set.
   * 
   * @return true is timezone is set
   */
  public boolean hasTimeZone() {
    return StringUtils.isNotEmpty(timeZone);
  }

  public Integer getDuration() {
    return duration;
  }

  public void setDuration(final Integer duration) {
    this.duration = duration;
  }

  /**
   * @return true if this communication has a duratin, else false
   */
  public boolean hasDuration() {
    return duration != null;
  }

  public Integer getBookingDuration() {
    return bookingDuration;
  }

  public void setBookingDuration(final Integer bookingDuration) {
    this.bookingDuration = bookingDuration;
  }

  /**
   * Indicate if this communication has an entry time. The entry time is defined as 5 minutes before the scheduled start
   * of this communication (5 mins before go live). By definition only upcoming communications have an entry time.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasEntryTime() {
    return isUpcoming();
  }

  /**
   * Get the entry time for this communication - i.e. the five minutes before go live time.
   * 
   * @return the entry time.
   */
  public Date getEntryTime() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(getScheduledDateTime());
    calendar.add(Calendar.MINUTE, PENDING_OFFSET_MINUTES);
    return calendar.getTime();
  }

  /**
   * Indicate is this communication has a close time. Only relevant for upcoming & live.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasCloseTime() {
    return isUpcoming() || isLive();
  }

  /**
   * Get the close time for this communication - i.e. the five minutes after the scheduled end time.
   * 
   * @return the close time.
   */
  public Date getCloseTime() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(scheduledDateTime);
    calendar.add(Calendar.SECOND, duration);
    if (!isRecorded()) {
      calendar.add(Calendar.MINUTE, ENFORCED_CLOSE_OFFSET_MINUTES);
    }
    return calendar.getTime();
  }

  public Integer getNumberOfRegistrants() {
    return numberOfRegistrants;
  }

  public void setNumberOfRegistrants(final Integer numberOfRegistrants) {
    this.numberOfRegistrants = numberOfRegistrants;
  }

  /**
   * Get the format of this communication. Indicates if this is an audio or video communication
   * 
   * @return the format.
   * 
   * @throws IllegalStateException if this communication has no format set. A communication must always have its format
   * to be valid.
   * 
   * #see isVideoFormat() #see isAudioFormat()
   */
  public Communication.Format getFormat() {
    if (format == null) {
      throw new IllegalStateException("No format set on communication [" + getId() + "]. Probably a programming error.");
    }
    return format;
  }

  public List<Rendition> getRenditions() {
    return renditions;
  }

  public void setRenditions(final List<Rendition> renditions) {
    this.renditions = renditions;
  }

  /**
   * check if the communciation has renditions.
   * 
   * @return boolean.
   */
  public Boolean hasRenditions() {
    return !CollectionUtils.isEmpty(renditions);
  }

  public void setScheduledPublication(final CommunicationScheduledPublication scheduledPublication) {
    this.scheduledPublication = scheduledPublication;
  }

  public CommunicationScheduledPublication getScheduledPublication() {
    return scheduledPublication;
  }

  /**
   * Indicate if this communication has a scheduled publication. .
   * 
   * @return true if the scheduled publication is set.
   */
  public boolean hasScheduledPublication() {
    return scheduledPublication != null;
  }

  /**
   * Indicate if this communication is video.
   * 
   * @return <code>true</code> or <code>false</code>
   * 
   * #see isAudioFormat()
   */
  public boolean isVideoFormat() {
    return getFormat().equals(Format.VIDEO);
  }

  /**
   * Indicate if this communication is audio.
   * 
   * @return <code>true</code> or <code>false</code>
   * 
   * #see isVideoFormat()
   */
  public boolean isAudioFormat() {
    return getFormat().equals(Format.AUDIO);
  }

  public void setFormat(final Communication.Format format) {
    this.format = format;
  }

  /**
   * Get the publish status of this communication. Published indicates this communication can be watched (accessible
   * from view communication) Unpublished indicates this communication cannot be watched (not accessible from view
   * communication)
   * 
   * @return the publish status.
   * 
   * @throws IllegalStateException if this communication as no publish status set. A communication must always have its
   * publish status to be valid.
   * 
   * @see #isPublished()
   * @see #isUnpublished()
   */
  public Communication.PublishStatus getPublishStatus() {
    return publishStatus;
  }

  /**
   * Indicate if this communication is published - can be viewed.
   * 
   * @return <code>true</code> or <code>false</code>
   * 
   * @see #isUnpublished()
   */
  public boolean isPublished() {
    return getPublishStatus().equals(PublishStatus.PUBLISHED);
  }

  /**
   * Indicate if this communication is unpublished - cannot be viewed.
   * 
   * @return <code>true</code> or <code>false</code>
   * 
   * @see #isPublished()
   */
  public boolean isUnpublished() {
    return getPublishStatus().equals(PublishStatus.UNPUBLISHED);
  }

  /**
   * Set the publish status on this communication.
   * 
   * @param publishStatus the publish status to set.
   */
  public void setPublishStatus(final Communication.PublishStatus publishStatus) {
    this.publishStatus = publishStatus;
  }

  /**
   * returns true is publish status is set.
   * 
   * @return true if the communication has a publish status set
   */
  public boolean hasPublishStatus() {
    return publishStatus != null;
  }

  public Integer getRerunCount() {
    return rerunCount;
  }

  public void setRerunCount(final Integer rerunCount) {
    this.rerunCount = rerunCount;
  }

  public String getPinNumber() {
    return pinNumber;
  }

  public void setPinNumber(final String pinNumber) {
    this.pinNumber = pinNumber;
  }

  public String getLivePhoneNumber() {
    return livePhoneNumber;
  }

  public void setLivePhoneNumber(final String livePhoneNumber) {
    this.livePhoneNumber = livePhoneNumber;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public String getChannelWebcastUrl() {
    return channelWebcastUrl;
  }

  public void setChannelWebcastUrl(final String channelWebcastUrl) {
    this.channelWebcastUrl = channelWebcastUrl;
  }

  /**
   * Set the communication url. This will sets the calendar url as well as the commnuication and channel webcast url
   * 
   * @param channel in which the webcast lives. used to check the channel feature.
   */
  public void setCommunicationUrl(final Channel channel) {

    this.setCalendarUrl(calendarUrlGenerator.generate(channel, this));

    String communicationUrl = communicationPortalUrlGenerator.generate(channel, this);
    String webcastUrl = communicationUrl;

    if (this.getConfiguration().hasCustomUrl()) {
      webcastUrl = communicationPortalUrlGenerator.getDefaultPortalChannelUrl(channel, this);
    }

    this.setUrl(communicationUrl);
    this.setChannelWebcastUrl(webcastUrl);
  }

  public String getConferenceRoomUrl() {
    return conferenceRoomUrl;
  }

  public void setConferenceRoomUrl(final String conferenceRoomUrl) {
    this.conferenceRoomUrl = conferenceRoomUrl;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public void setThumbnailUrl(final String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }

  /**
   * Indicate if this communication has thumbnail url set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasThumbnailUrl() {
    return StringUtils.isNotEmpty(thumbnailUrl);
  }

  public String getCalendarUrl() {
    return calendarUrl;
  }

  public void setCalendarUrl(final String value) {
    calendarUrl = value;
  }

  /**
   * Indicate if this communication has calendar url set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasCalendarUrl() {
    return StringUtils.isNotEmpty(calendarUrl);
  }

  public String getPreviewUrl() {
    return previewUrl;
  }

  public void setPreviewUrl(final String previewUrl) {
    this.previewUrl = previewUrl;
  }

  /**
   * Indicate if this communication has preview url set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasPreviewUrl() {
    return StringUtils.isNotEmpty(previewUrl);
  }

  public String getLiveUrl() {
    return liveUrl;
  }

  public void setLiveUrl(final String liveUrl) {
    this.liveUrl = liveUrl;
  }

  /**
   * Get the go-live date and time for this communication.
   * 
   * @return the scheduled go-live date and time.
   */
  public Date getScheduledDateTime() {
    return scheduledDateTime;
  }

  /**
   * Sets the scheduled go-live date and time on this communication.
   * 
   * @param scheduledDateTime the scheduled go-live date and time to set.
   */
  public void setScheduledDateTime(final Date scheduledDateTime) {
    this.scheduledDateTime = scheduledDateTime;
  }

  /**
   * check if the scheduled date/time is set.
   * 
   * @return true if the scheduledDateTime is set
   */
  public boolean hasScheduledDateTime() {
    return scheduledDateTime != null;
  }

  /**
   * Indicate if this communication is the featured communication - the one closest to now.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isFeatured() {
    return featured;
  }

  /**
   * Set this communication as the featured communication - the one closest to now.
   */
  public void setFeatured() {
    featured = true;
  }

  /**
   * Indicate if this communication is upcoming.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isUpcoming() {
    return getStatus().equals(Communication.Status.UPCOMING);
  }

  /**
   * Indicates if the communication is in authoring period, which means more than 15 minutes to start.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isInAuthoringPeriod() {
    Date currentTime = new Date();
    return isUpcoming() && (currentTime.getTime() < scheduledDateTime.getTime() - AUTHORING_OFFSET_MILLI_SECONDS);
  }

  /**
   * Indicates if the communication is pending to start.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isPending() {
    Date currentTime = new Date();
    return isUpcoming() && (currentTime.getTime() >= scheduledDateTime.getTime() - PENDING_OFFSET_MILLI_SECONDS);
  }

  /**
   * Indicate if this communication is live.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isLive() {
    return getStatus().equals(Communication.Status.LIVE);
  }

  /**
   * Indicate if this communication is processing.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isProcessing() {
    return getStatus().equals(Communication.Status.PROCESSING);
  }

  /**
   * Indicate if this communication is processing failed.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isProcessingFailed() {
    return getStatus().equals(Communication.Status.PROCESSINGFAILED);
  }

  /**
   * Indicate if this communication is recorded.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isRecorded() {
    return getStatus().equals(Communication.Status.RECORDED);
  }

  /**
   * Indicate if this communication is cancelled.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isCancelled() {
    return getStatus().equals(Communication.Status.CANCELLED);
  }

  /**
   * Indicate if this communication is deleted.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isDeleted() {
    return getStatus().equals(Communication.Status.DELETED);
  }

  /**
   * Indicate if this communication has errored.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isErrored() {
    return getStatus().equals(Communication.Status.ERROR);
  }

  /**
   * Indicates if the communication is in 15 minute window before go live.
   * 
   * Typically in the 15 minute window before go live the presenter cannot do things like add slides or votes or
   * resources (attachments).
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isIn15MinuteWindow() {
    Date currentTime = new Date();
    long timeDifference = scheduledDateTime.getTime() - currentTime.getTime();
    return timeDifference > 0 && timeDifference < AUTHORING_OFFSET_MILLI_SECONDS;
  }

  /**
   * Return the status of this communication.
   * 
   * @return status of the communication.
   * 
   * @throws IllegalStateException if this communication as no status set. A communication must always have its status
   * to be valid.
   */
  public Communication.Status getStatus() {
    if (status == null) {
      throw new IllegalStateException("No status set on communication [" + getId() + "]. Probably a programming error.");
    }
    return status;
  }

  /**
   * Sets the status on this communication.
   * 
   * @param status to set.
   */
  public void setStatus(final Communication.Status status) {
    this.status = status;
  }

  /**
   * returns true is status is set.
   * 
   * @return true if the communication has a status set
   */
  public boolean hasStatus() {
    return status != null;
  }

  /**
   * Get any Survey for this communication.
   * 
   * @return A Survey Form this Communication - or <code>null</code> if no survey set.
   */
  public Survey getSurvey() {
    if (survey == null && getConfiguration().getSurveyId() != null) {
      survey = new Survey(getConfiguration().getSurveyId(), channelId, getConfiguration().isSurveyActive());
    }
    return survey;
  }

  /**
   * Set a Survey on this communication.
   * 
   * @param survey The Survey to set
   */
  public void setSurvey(final Survey survey) {
    this.survey = survey;
  }

  /**
   * Return the provider of this communication.
   * 
   * @return provider of the communication.
   * 
   * @throws IllegalStateException if this communication has no provider set. A communication must always have a
   * provider to be valid.
   */
  public Provider getProvider() {
    if (provider == null) {
      throw new IllegalStateException("No provider set on communication [" + getId() + "].");
    }
    return provider;
  }

  /**
   * Indicate if this communication has provider set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasProvider() {
    return provider != null;
  }

  /**
   * check if the provider has been changed.
   * 
   * Throw a BadRequest exception if it has
   * 
   * @param communication to check
   */
  public void hasProviderChanged(final Communication communication) {
    if (communication.hasProvider()) {
      String existingProvider = this.getProvider().getName();
      String newProvider = communication.getProvider().getName();
      if (!existingProvider.equals(newProvider)) {
        throw new BadRequestException("Provider cannot be changed. Existing provider was [" + existingProvider
            + "]. New provider was [" + newProvider + "].", ErrorCode.INVALID_WEBCAST_ID.getName(),
            new Object[] { newProvider });
      }
    }
  }

  /**
   * Indicate if this communication has format set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasFormat() {
    return format != null;
  }

  /**
   * Indicate if this communication has configuration set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasConfiguration() {
    return configuration != null;
  }

  /**
   * Indicate if this is a BrightTALK communication (i.e. audio with slides).
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isBrightTALK() {
    return getProvider().getName().equals(Provider.BRIGHTTALK);
  }

  /**
   * Indicate if this is a BrightTALK HD communication (i.e. HD video).
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isBrightTALKHD() {
    return getProvider().getName().equals(Provider.BRIGHTTALK_HD);
  }

  /**
   * Indicate if this is a MediaZone communication.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isMediaZone() {
    return getProvider().getName().equals(Provider.MEDIAZONE);
  }

  /**
   * Indicate if this is a self service video communication.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isVideo() {
    return getProvider().getName().equals(Provider.VIDEO);
  }

  /**
   * Sets the provider on this communication.
   * 
   * @param provider to set.
   */
  public void setProvider(final Provider provider) {
    this.provider = provider;
  }

  /**
   * Return the presenters who have a logged in session to present this communication.
   * 
   * @return collection of presenters session of the communication.
   */
  public List<Session> getPresenters() {
    if (presenters == null) {
      presenters = new ArrayList<Session>();
    }
    return presenters;
  }

  /**
   * Sets the collection of presenters who have a logged in session to present this communication.
   * 
   * @param presenters to set.
   */
  public void setPresenters(final List<Session> presenters) {
    this.presenters = presenters;
  }

  /**
   * Indicates if the given user is a logged in presenter of this communication.
   * 
   * @param user the user to be verified
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isPresenter(final User user) {
    return getPresenters().contains(user.getSession());
  }

  /**
   * Get the resources (attachments) for this communication.
   * 
   * @return collection of resources of the communication or an empty collection if this communication has no resources.
   */
  public List<Resource> getResources() {
    if (resources == null) {
      resources = new ArrayList<Resource>();
    }
    return resources;
  }

  /**
   * Sets the collection of resources (attachments) on this communication.
   * 
   * @param resources to set.
   */
  public void setResources(final List<Resource> resources) {
    this.resources = resources;
  }

  /**
   * Get a resource (attachment) from this communication.
   * 
   * @param resourceId id of the resource to be retrieved
   * 
   * @return the resource
   * 
   * @throws NotFoundException if the resource does not exist.
   */
  public Resource getResource(final Long resourceId) throws NotFoundException {
    for (Resource resource : resources) {
      if (resource.getId().equals(resourceId)) {
        return resource;
      }
    }
    throw new NotFoundException("Resource[" + resourceId + "] does not belong to the communication.");
  }

  /**
   * Indicate if this communication contains the given resource.
   * 
   * @param resourceId the resource id to be verified
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasResource(final Long resourceId) {
    for (Resource resource : resources) {
      if (resource.getId().equals(resourceId)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Get the configuration for this communication.
   * 
   * @return this communications configuration..
   * 
   * @throws IllegalStateException if this communication as no configuration set. A communication must always have its
   * configuration to be valid.
   */
  public CommunicationConfiguration getConfiguration() {
    if (configuration == null) {
      throw new IllegalStateException("No configuration set on communication [" + getId()
          + "].Probably a programming error.");
    }
    return configuration;
  }

  /**
   * Set this communication's configuration.
   * 
   * @param configuration to set.
   */
  public void setConfiguration(final CommunicationConfiguration configuration) {
    this.configuration = configuration;
  }

  /**
   * Get this communications statistics.
   * 
   * @return this communications viewing statistics.
   */
  public CommunicationStatistics getCommunicationStatistics() {
    if (communicationStatistics == null) {
      communicationStatistics = new CommunicationStatistics(getId());
    }
    return communicationStatistics;
  }

  /**
   * Set this configurations statistics.
   * 
   * @param communicationStatistics the communication statistics to set.
   */
  public void setCommunicationStatistics(final CommunicationStatistics communicationStatistics) {
    this.communicationStatistics = communicationStatistics;
  }

  /**
   * Get this communications categories.
   * 
   * @return this communications categories or an empty list if this communication has not categories.
   */
  public List<CommunicationCategory> getCategories() {
    if (categories == null) {
      categories = new ArrayList<CommunicationCategory>();
    }
    return categories;
  }

  public void setCategories(final List<CommunicationCategory> categories) {
    this.categories = categories;
  }

  /**
   * Add a category to this communication.
   * 
   * @param category the category to add.
   */
  public void addCategory(final CommunicationCategory category) {
    getCategories().add(category);
  }

  /**
   * Indicate if this communication has categories set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasCategories() {
    return categories != null && categories.size() > 0;
  }

  public CommunicationUrlGenerator getCalendarUrlGenerator() {
    return calendarUrlGenerator;
  }

  public void setCalendarUrlGenerator(final CommunicationUrlGenerator calendarUrlGenerator) {
    this.calendarUrlGenerator = calendarUrlGenerator;
  }

  public CommunicationPortalUrlGenerator getCommunicationPortalUrlGenerator() {
    return communicationPortalUrlGenerator;
  }

  public void setCommunicationPortalUrlGenerator(final CommunicationPortalUrlGenerator communicationPortalUrlGenerator) {
    this.communicationPortalUrlGenerator = communicationPortalUrlGenerator;
  }

  public Integer getEncodingCount() {
    return encodingCount;
  }

  public void setEncodingCount(final Integer encodingCount) {
    this.encodingCount = encodingCount;
  }

  /**
   * Merge the given communication into the current instance. This will replace the values of the properties in the
   * current instance with the value of the corresponding property from the given instance, but only if set. The
   * properties merged are only the ones required for communication update.
   * 
   * @param communication to be merged
   */
  public void merge(final Communication communication) {
    CommunicationMerger communicationMerger = new CommunicationMerger();
    communicationMerger.mergeCommunication(this, communication);
  }

  /**
   * Merge any syndicated override data into this communication.
   * 
   * @param syndicatedOverrideData the syndicated override data to add.
   */
  public void merge(final CommunicationOverride syndicatedOverrideData) {
    CommunicationMerger communicationMerger = new CommunicationMerger();
    communicationMerger.mergeSyndicatedOverrideData(this, syndicatedOverrideData);
  }

  /**
   * Indicates if a presenter can dial in to present this communication. Presenters are allowed to dial in, in the
   * 15mins window to go live. If they dial before the 15min window, a dialledInTooEarly exception will be thrown.
   * Presenters are allowed to dial in, in the 15mins window to go live. If they dial, after the end time of this
   * communication (start time + duration) a dialledInTooLate exception will be thrown
   * 
   * @throws DialledInTooEarlyException when presenter has dialled in too early
   * @throws DialledInTooLateException when presenter has dialled in too late
   */
  public void canDialIn() {
    if (dialedInTooEarly()) {
      throw new DialledInTooEarlyException("Presenter dialed in too early for communication [" + id + "].", "TooEarly");
    } else if (dialedInTooLate()) {
      throw new DialledInTooLateException("Presenter dialed in too late for communication [" + id + "].", "TooLate");
    }
  }

  /**
   * Validates if a presenter has dialled in too early. Presenters are allowed to dial in, in the 15mins window to go
   * live. If they dial before the 15min window, a dialledInTooEarly exception will be thrown
   * 
   * @return <code>true</code> or <code>false</code>
   */
  private boolean dialedInTooEarly() {
    return isInAuthoringPeriod();
  }

  /**
   * Validates if a presenter has dialled in too late. Presenters are allowed to dial in, in the 15mins window to go
   * live. If they dial, after the start time + duration, a dialledInTooLate exception will be thrown
   * 
   * @return <code>true</code> or <code>false</code>
   */
  private boolean dialedInTooLate() {
    long now = new Date().getTime();
    long startTime = this.getScheduledDateTime().getTime();
    long webcastDuration = this.getDuration() * 1000; // duration in milliseconds
    long communicationEndTime = startTime + webcastDuration;
    return communicationEndTime < now;
  }

  /**
   * Assert if the webcast has been scheduled in the future.
   * 
   * @return true if the webcast is in the future, else false.
   */
  public boolean inTheFuture() {
    Date currentDateAndTime = new Date();
    return this.getScheduledDateTime().getTime() > currentDateAndTime.getTime();
  }

  /**
   * Check that a webcast is not in the future.
   * 
   * @return true if the webcast is not the future, else false.
   */
  public boolean notInTheFuture() {
    return !inTheFuture();
  }

  /**
   * Flag if this communication has been rescheduled. A communication is deemed rescheduled c.g. its previous
   * incarnation, if the webcast is upcoming and if the given scheduled date and time, duration or timezone are
   * different than the ones stored in database we set the property to true else it returns the default value, false
   * 
   * @param communication the communication to check against. Typically hold the values of the communication that is
   * being updated.
   */
  public void setHasBeenRescheduled(final Communication communication) {
    boolean hasScheduledDateTimeChanged =
        !(scheduledDateTime.getTime() == communication.getScheduledDateTime().getTime());
    boolean hasDurationChanged = !duration.equals(communication.getDuration());
    boolean hasTimeZoneChanged = !timeZone.equals(communication.getTimeZone());
    isRescheduled = communication.isUpcoming()
        && (hasScheduledDateTimeChanged || hasDurationChanged || hasTimeZoneChanged);
  }

  /**
   * Indicate if this communication is in a rescheduled state.
   * 
   * @return true if the time has changed else false
   */
  public boolean isRescheduled() {
    return isRescheduled;
  }

  /**
   * returns a boolean to indicate if the publish status has changed.
   * 
   * @return true if the publish status has changed else false
   */
  public boolean hasPublishStatusChanged() {
    return publishStatusHasChanged;
  }

  /**
   * @param publishStatusHasChanged the publishStatusIsDifferent to set
   */
  public void setPublishStatusHasChanged(final boolean publishStatusHasChanged) {
    this.publishStatusHasChanged = publishStatusHasChanged;
  }

  /**
   * @return the mediazoneConfig
   */
  public MediazoneConfig getMediazoneConfig() {
    return mediazoneConfig;
  }

  /**
   * @param mediazoneConfig the mediazoneConfig to set
   */
  public void setMediazoneConfig(final MediazoneConfig mediazoneConfig) {
    this.mediazoneConfig = mediazoneConfig;
  }

  /**
   * verifies if the communication has mediazone config set.
   * 
   * @return boolean true if set.
   */
  public boolean hasMediazoneConfig() {
    return mediazoneConfig != null;
  }

  /**
   * check if the communication is the master communication.
   * 
   * @return boolean
   */
  public boolean isMasterCommunication() {
    return channelId.equals(masterChannelId);
  }

  /**
   * Validates this communication with given validator. Throws various validation exceptions depending on the validator
   * used.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<Communication> validator) {
    validator.validate(this);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder("Communication\n");
    builder.append(super.toString());
    builder.append("[channelId=" + getChannelId() + "]\n");
    builder.append("[masterChannelId=" + getMasterChannelId() + "]\n");
    builder.append("[title=" + getTitle() + "]\n");
    builder.append("[description=" + getDescription() + "]\n");
    builder.append("[keywords=" + getKeywords() + "]\n");
    builder.append("[authors=" + getAuthors() + "]\n");
    builder.append("[featured=" + isFeatured() + "]\n");
    builder.append("[scheduledDateTime (UTC)=" + getScheduledDateTime() + "]\n");
    builder.append("[timezone=" + getTimeZone() + "]\n");
    builder.append("[no of registrants=" + getNumberOfRegistrants() + "]\n");
    builder.append("[status=" + getStatus() + "]\n");
    builder.append("[provider=" + getProvider() + "]\n");
    builder.append("[format=" + getFormat() + "]\n");
    builder.append("[publishStatus=" + getPublishStatus() + "]\n");
    builder.append("[recordedDuration (secs)=" + getDuration() + "]\n");
    builder.append("[bookingDuration (secs)=" + getBookingDuration() + "]\n");
    builder.append("[pin number=" + getPinNumber() + "]\n");
    builder.append("[live phone number=" + getLivePhoneNumber() + "]\n");
    builder.append("[thumbnail URL=" + getThumbnailUrl() + "]\n");
    builder.append("[preview URL=" + getPreviewUrl() + "]\n");
    builder.append("[live URL=" + getLiveUrl() + "]\n");
    builder.append("[encoding count=" + getEncodingCount() + "]\n");
    builder.append(getConfiguration().toString());
    builder.append(getCommunicationStatistics().toString());
    builder.append(getCategories().toString());
    if (this.hasScheduledPublication()) {
      builder.append(getScheduledPublication().toString());
    }
    builder.append("[mediazone config=" + getMediazoneConfig() + "]\n");
    return builder.toString();
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof Communication)) {
      return false;
    }
    Communication communication = (Communication) obj;
    return id.equals(communication.getId());
  }

  /**
   * Returns a hash code value for this communication.
   * 
   * @return the hash code
   */
  @Override
  public int hashCode() {
    return this.getId().hashCode();
  }
}