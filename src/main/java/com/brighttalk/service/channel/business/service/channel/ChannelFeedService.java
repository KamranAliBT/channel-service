/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelFeedService.java 75381 2014-03-18 15:16:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.PaginatedChannelFeed;

/**
 * The Channel Feed Service Business API.
 * <p>
 * The Channel Feed Service is responsible generating a {@link PaginatedChannelFeed paginated feed} of communications
 * for a channel.
 * <p>
 */
public interface ChannelFeedService {

  /**
   * Get a paginated communication feed for a channel.
   * 
   * @param channel the id of the channel
   * @param searchCriteria the search criteria for the feeds. Contains for example page number and page size and filter
   * info.
   * 
   * @return a page worth of communications or an empty list if there are no communications in the feed.
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException if the channel is not found.
   */
  PaginatedChannelFeed getFeed(Channel channel, ChannelFeedSearchCriteria searchCriteria);

}