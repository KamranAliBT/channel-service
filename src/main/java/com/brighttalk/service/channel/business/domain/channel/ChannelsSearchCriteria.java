/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: ChannelsSearchCriteria.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;

/**
 * 
 * Search Criteria for retrieving the paginated channels list.
 * <p>
 * Contains the page number being requested, the size of the page being requested.
 * <p>
 * Flags on how the channels could be expanded are part of this class, too.
 * <p>
 * It can be used for syntactical validation of the request parameters.
 */
public class ChannelsSearchCriteria extends BaseSearchCriteria {

  @NotNull
  @Pattern(regexp = "(\\d+)(,*\\d+)*", message = "{error.invalid.ids.value}")
  private String ids;

  private boolean privateWebcastsIncluded;

  public void setIds(final String ids) {
    this.ids = ids;
  }

  /**
   * Indicates if private webcasts are included in this ChannelsSearchCriteria.
   * 
   * @return true or false
   */
  public boolean includePrivateWebcasts() {
    return privateWebcastsIncluded;
  }

  public void setPrivateWebcastsIncluded(final boolean privateWebcastsIncluded) {
    this.privateWebcastsIncluded = privateWebcastsIncluded;
  }

  /**
   * Converts comma separated list of ids to list of long.
   * 
   * It removes duplicates if they exist in the list.
   * 
   * @return returns collection of channel ids
   */
  public List<Long> getChanelIds() {

    if (StringUtils.isEmpty(ids)) {
      return new ArrayList<Long>();
    }

    Set<Long> channelIds = new LinkedHashSet<Long>();

    if (!ids.contains(",")) {
      channelIds.add(Long.valueOf(ids));
      return new ArrayList<Long>(channelIds);
    }

    String[] channelIdStrings = StringUtils.split(ids, ',');
    for (String idString : channelIdStrings) {
      Long id = Long.valueOf(idString);
      channelIds.add(id);
    }

    return new ArrayList<Long>(channelIds);
  }
}