/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: InvalidChannelException.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that provided Channel is invalid. The exception will be thrown by presentation layer validator. 
 */
@SuppressWarnings("serial")
public class InvalidChannelException extends ApplicationException {

  /**
   * Error code for invalid channel.
   */
  public static final String ERROR_CODE_INVALID_CHANNEL = "InvalidChannel";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_INVALID_CHANNEL}.
   */
  private String errorCode = ERROR_CODE_INVALID_CHANNEL;
  
  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public InvalidChannelException(String message, String errorCode) {
    super(message, ERROR_CODE_INVALID_CHANNEL);
    this.setUserErrorMessage(errorCode);
  }
  
  @Override
  public String getUserErrorCode() {
    return errorCode;
  }
}