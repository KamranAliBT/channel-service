/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationStatistics.java 99258 2015-08-18 10:35:43Z ssarraj $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import java.math.BigDecimal;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.service.channel.business.domain.Entity;

/**
 * The statistics for a communication.
 * <p>
 * Provides the:
 * <ul>
 * <li>
 * Number of viewings - the number of viewings of the webcast.</li>
 * <li>
 * Total viewing duration - the total duration in seconds the webcast was viewed for across all viewings.</li>
 * <li>
 * Number of ratings - the number of people who rated the webcast.</li>
 * <li>
 * Average rating - the average rating for the webcast across all viewers who have rated it.</li>
 * <li>
 * Total registration - the total number of people registered to the communication.</li>
 * </ul>
 */
public class CommunicationStatistics extends Entity {

  private static final long serialVersionUID = 1L;

  /**
   * The total number of people who viewed the webcast.
   */
  private int numberOfViewings;
  /**
   * The total viewing duration for the webcast in seconds.
   */
  private long totalViewingDuration;
  /**
   * The total number of ratings for the webcast i.e. how many people rated it.
   */
  private int numberOfRatings;
  /**
   * The total rating for the webcast i.e. the sum of everyones rating.
   */
  private float averageRating;
  /**
   * The total number of people registered to the webcast.
   */
  private int totalRegistration;

  /**
   * The channel Id of identified communication statistics.
   */
  private long channelId;

  /**
   * Construct and empty viewing statistics object.
   */
  public CommunicationStatistics() {
  }

  /**
   * Construct this viewing statistic with zero statistics.
   * 
   * @param communicationId the id of the communication to create this viewing statistic for.
   */
  public CommunicationStatistics(final Long communicationId) {
    super(communicationId);
  }

  /**
   * Construct this viewing statistics with the given stats for a communication.
   * 
   * @param communicationId the id of the communication to create this viewing statistic for.
   * @param numberOfViewings the number of viewings of the webcast.
   * @param totalViewingDuration the total viewing duration of the webcast.
   * @param numberOfRatings the number of ratings the webcast has had.
   * @param averageRating the average rating for the webcast across all users who have rated it.
   */
  public CommunicationStatistics(final long communicationId, final int numberOfViewings,
      final long totalViewingDuration, final int numberOfRatings, final float averageRating) {
    super(communicationId);
    this.numberOfViewings = numberOfViewings;
    this.totalViewingDuration = totalViewingDuration;
    this.numberOfRatings = numberOfRatings;
    this.averageRating = averageRating;
  }

  /**
   * Get the number of viewings for the webcast.
   * 
   * @return the number of viewings.
   */
  public int getNumberOfViewings() {
    return numberOfViewings;
  }

  public void setNumberOfViewings(final int value) {
    numberOfViewings = value;
  }

  /**
   * Get the total viewing duration for the webcast across all viewers.
   * 
   * @return the total viewing duration.
   */
  public long getTotalViewingDuration() {
    return totalViewingDuration;
  }

  public void setTotalViewingDuration(final long value) {
    totalViewingDuration = value;
  }

  /**
   * Get the average rating for this webcast across all viewers.
   * <p>
   * Derived as <code> (number of ratings * total rating)/number of ratings </code>
   * 
   * @return the average rating.
   */
  public float getAverageRating() {
    return averageRating;
  }

  public void setAverageRating(final float value) {
    averageRating = value;
  }

  /**
   * Get the average rating rounded to one decimal place.
   * 
   * @return formatted rating
   */
  public float getFormattedRating() {

    final int precision = 1;
    final int roundingMode = BigDecimal.ROUND_HALF_UP;

    BigDecimal decimal = new BigDecimal(averageRating);
    BigDecimal rounded = decimal.setScale(precision, roundingMode);

    return rounded.floatValue();
  }

  /**
   * Get the number of ratings for the webcast.
   * 
   * @return the number of ratings.
   */
  public int getNumberOfRatings() {
    return numberOfRatings;
  }

  public void setNumberOfRatings(final int value) {
    numberOfRatings = value;
  }

  /**
   * @return the totalRegistration
   */
  public int getTotalRegistration() {
    return totalRegistration;
  }

  /**
   * @param totalRegistration the totalRegistration to set
   */
  public void setTotalRegistration(int totalRegistration) {
    this.totalRegistration = totalRegistration;
  }

  /**
   * @return the channelId
   */
  public long getChannelId() {
    return channelId;
  }

  /**
   * @param channelId the channelId to set
   */
  public void setChannelId(long channelId) {
    this.channelId = channelId;
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this);

    builder.append(super.toString());
    builder.append("communicationId", id);
    builder.append("channelId", channelId);
    builder.append("numberOfViewings", numberOfViewings);
    builder.append("totalViewingDuration", totalViewingDuration);
    builder.append("numberOfRatings", numberOfRatings);
    builder.append("averageRating", averageRating);
    builder.append("totalRegistration", totalRegistration);

    return builder.toString();
  }
}
