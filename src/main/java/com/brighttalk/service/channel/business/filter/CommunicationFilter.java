/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: CommunicationFilter.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.filter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.integration.communication.dto.CommunicationDto;

/**
 * Helper to filter communications.
 */
@Component
public class CommunicationFilter {

  @Autowired
  private CollectionFilter collectionFilter;

  /**
   * Filter a communication by id.
   * 
   * @param communications the communications to filter.
   * @param searchableCommunication the communication to search for.
   * 
   * @return the filtered communication.
   */
  public CommunicationDto filterByCommunicationId(final List<CommunicationDto> communications,
      final CommunicationDto searchableCommunication) {
    Predicate<CommunicationDto> equalsCommId = new Predicate<CommunicationDto>() {
      @Override
      public boolean apply(final CommunicationDto communication) {
        return communication.getId().equals(searchableCommunication.getId());
      }
    };

    List<CommunicationDto> filteredCommunications = collectionFilter.filter(communications, equalsCommId);

    if (CollectionUtils.isEmpty(filteredCommunications)) {
      throw new CommunicationNotFoundException(searchableCommunication.getId());
    }

    return filteredCommunications.iterator().next();
  }
}
