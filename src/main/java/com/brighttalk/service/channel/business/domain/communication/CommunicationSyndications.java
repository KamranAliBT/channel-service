/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: CommunicationSyndications.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import java.util.ArrayList;
import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * Wrapper class containing all the details of a communication syndication details this include: the
 * {@link Communication} details in the master channel, the communication master {@link Channel} details and all the
 * communication distributed {@link CommunicationSyndication channels syndication details}.
 */
public class CommunicationSyndications {

  private Channel channel;
  private Communication communication;
  private List<CommunicationSyndication> communicationSyndications;

  /**
   * @return the channel
   */
  public Channel getChannel() {
    return channel;
  }

  /**
   * @return the communication
   */
  public Communication getCommunication() {
    return communication;
  }

  /**
   * @return the communicationSyndications
   */
  public List<CommunicationSyndication> getCommunicationSyndications() {
    return communicationSyndications;
  }

  /**
   * @param channel the channel to set
   */
  public void setChannel(Channel channel) {
    this.channel = channel;
  }

  /**
   * @param communication the communication to set
   */
  public void setCommunication(Communication communication) {
    this.communication = communication;
  }

  /**
   * @param communicationSyndications the communicationSyndications to set
   */
  public void setCommunicationSyndications(List<CommunicationSyndication> communicationSyndications) {
    this.communicationSyndications = communicationSyndications;
  }

  /**
   * @param communicationSyndication The communication syndication to add.
   */
  public void addCommunicationSyndications(CommunicationSyndication communicationSyndication) {
    if (this.communicationSyndications == null) {
      this.communicationSyndications = new ArrayList<>();
    }
    this.communicationSyndications.add(communicationSyndication);
  }

}
