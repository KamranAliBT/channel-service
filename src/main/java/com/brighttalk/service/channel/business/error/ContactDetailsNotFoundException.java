/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContactDetailsNotFoundException.java 69660 2013-10-14 11:30:45Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

/**
 * Indicates the contact details fo a given channel have not been found.
 */
@SuppressWarnings("serial")
public class ContactDetailsNotFoundException extends NotFoundException {

  private static final String MESSAGE_NO_CONTACT_DETAILS = "Contact details not found for channel [%s].";

  /**
   * @param channelId see below.
   * @param cause see below.
   * 
   * @see NotFoundException#NotFoundException(String, Throwable)
   */
  public ContactDetailsNotFoundException(final long channelId, final Throwable cause) {
    super(String.format(MESSAGE_NO_CONTACT_DETAILS, channelId), cause);
  }

}
