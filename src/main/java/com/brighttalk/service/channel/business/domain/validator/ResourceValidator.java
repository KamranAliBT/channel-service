/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceValidator.java 47319 2012-04-30 11:25:39Z amajewski $ 
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.error.InvalidResourceException;

/**
 * {@link Validator} that validates that a {@link Resource} can be updated. 
 * 
 * A resource cannot be changed from a file to a link or vice versa (type cannot be changed). 
 * If a resource is a file it cannot be changed from an external hosted file to an internal hosted file (or vice versa). 
 * 
 * Note - its assumed the presentation layer has validated the following: 
 *  - size, mimeType can only be changed for type = file && hosted = external
 *  - url can only be changed for type = link OR hosted != "internal"
 * 
 */
public class ResourceValidator implements Validator<Resource> {

  /**
   * The existing resource. Used to check against the current resource on update.
   */
  private Resource existingResource;

  /**
   * Construct this validator with the existing resource.
   * 
   * @param existingResource the resource to validate against.
   */
  public ResourceValidator(Resource existingResource) {
    this.existingResource = existingResource;
  }

  /**
   * Validate a resource for update against the existing resource . 
   * 
   * Validates the type of resource has not changed (link to file or vice versa). 
   * Validates, if the resource is a file, this its hosted type has not changed (internal to external or vice versa)
   * 
   * @param resource to be validated.
   * 
   * @throws InvalidResourceException if the type has changed. 
   * @throws InvalidResourceException if hosted file type has changed
   */
  @Override
  public void validate(Resource resource) {

    // has the type changed?
    if (resource.hasType() 
            && !this.existingResource.getType().equals(resource.getType())) {
      throw new InvalidResourceException("Type Cannot be changed", "TypeCannotBeChanged");
    }

    // has the hosted changed?
    if (resource.isFile()
            && !this.existingResource.getHosted().equals(resource.getHosted())) {
      throw new InvalidResourceException("Hosted Cannot be changed", "HostedCannotBeChanged");
    }

  }
}
