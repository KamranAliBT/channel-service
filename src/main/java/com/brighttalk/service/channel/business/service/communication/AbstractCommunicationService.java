/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AbstractCommunicationService.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.brighttalk.common.utils.JobIdGenerator;
import com.brighttalk.service.channel.business.domain.ContentPlan;
import com.brighttalk.service.channel.business.domain.Keywords;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationPortalUrlGenerator;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationUrlGenerator;
import com.brighttalk.service.channel.business.error.InvalidCommunicationException;
import com.brighttalk.service.channel.business.error.ProviderNotSupportedException;
import com.brighttalk.service.channel.business.queue.EmailQueuer;
import com.brighttalk.service.channel.business.queue.LesQueuer;
import com.brighttalk.service.channel.business.queue.NotificationQueuer;
import com.brighttalk.service.channel.business.queue.SummitQueuer;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.database.CommunicationCategoryDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationConfigurationDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationOverrideDbDao;
import com.brighttalk.service.channel.integration.database.RenditionDbDao;
import com.brighttalk.service.channel.integration.database.ScheduledPublicationDbDao;
import com.brighttalk.service.channel.integration.database.statistics.CommunicationStatisticsDbDao;
import com.brighttalk.service.channel.integration.database.syndication.SyndicationDbDao;
import com.brighttalk.service.channel.integration.email.EmailServiceDao;
import com.brighttalk.service.channel.integration.les.LiveEventServiceDao;
import com.brighttalk.service.channel.integration.notification.NotificationServiceDao;
import com.brighttalk.service.channel.integration.summit.SummitServiceDao;

/**
 * Abstract implementation used by the Communication Create / Update / Cancel services.
 * 
 */
abstract class AbstractCommunicationService {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(AbstractCommunicationService.class);

  /** Channel finder. */
  @Autowired
  protected ChannelFinder channelFinder;

  /** Communication finder. */
  @Autowired
  protected CommunicationFinder communicationFinder;

  /** Communication db dao. */
  @Autowired
  protected CommunicationDbDao communicationDbDao;

  /** Communication override db dao. */
  @Autowired
  protected CommunicationOverrideDbDao communicationOverrideDbDao;

  /** Configuration db dao. */
  @Autowired
  protected CommunicationConfigurationDbDao configurationDbDao;

  /** Category db dao. */
  @Autowired
  protected CommunicationCategoryDbDao categoryDbDao;

  /** Scheduled publication db dao. */
  @Autowired
  protected ScheduledPublicationDbDao scheduledPublicationDbDao;

  /** Syndication db dao */
  @Autowired
  protected SyndicationDbDao syndicationDbDao;

  /** Communication statistics db dao. */
  @Autowired
  protected CommunicationStatisticsDbDao communicationStatisticsDbDao;

  /** Keywords object. */
  @Autowired
  protected Keywords keywords;

  /** Content plan object. */
  @Autowired
  protected ContentPlan contentPlan;

  /** Email Dao. */
  @Autowired
  protected EmailServiceDao emailServiceDao;

  /** Summit Dao. */
  @Autowired
  protected SummitServiceDao summitServiceDao;

  /** LES Dao. */
  @Autowired
  protected LiveEventServiceDao liveEventServiceDao;

  /** Notification Dao. */
  @Autowired
  protected NotificationServiceDao notificationServiceDao;

  /** Rendition Dao */
  @Autowired
  protected RenditionDbDao renditionDbDao;

  /** Email queuer used by JMS. */
  @Autowired
  protected EmailQueuer emailQueuer;

  /** LES queuer used by JMS. */
  @Autowired
  protected LesQueuer lesQueuer;

  /** Notification queuer used by JMS. */
  @Autowired
  protected NotificationQueuer notificationQueuer;

  /** Summit queuer used by JMS. */
  @Autowired
  protected SummitQueuer summitQueuer;

  @Autowired
  private JobIdGenerator jobIdGenerator;

  /** Calendar url generator object. */
  @Value("#{calendarUrlGenerator}")
  protected CommunicationUrlGenerator calendarUrlGenerator;

  /** Communication url generator object. */
  @Value("#{communicationPortalUrlGenerator}")
  protected CommunicationUrlGenerator communicationUrlGenerator;

  /** Communication portal url generator object. */
  @Autowired
  protected CommunicationPortalUrlGenerator communicationPortalUrlGenerator;

  /** check if notification service is enabled . */
  @Value("${notification.service.enabled}")
  private boolean notificationServiceEnabled;

  /**
   * @return the notificationServiceEnabled
   */
  protected boolean isNotificationServiceEnabled() {
    return notificationServiceEnabled;
  }

  /**
   * @param notificationServiceEnabled the notificationServiceEnabled to set
   */
  public void setNotificationServiceEnabled(final boolean notificationServiceEnabled) {
    this.notificationServiceEnabled = notificationServiceEnabled;
  }

  /**
   * Add quotes to the keywords.
   * 
   * This needs to be done in order to respect, spacing, quotes and format of keywords entered by the user.
   * 
   * @param communication the communication object containing the keywords to add quotes to
   */
  protected void addQuotesToKeywords(final Communication communication) {
    String quotedKeywords = keywords.addQuotes(communication.getKeywords());
    communication.setKeywords(quotedKeywords);
  }

  /**
   * @param communication The communication to validates.
   * @throws ProviderNotSupportedException In case of the communication provider is not "BrightTALK HD".
   */
  protected void validateCommunicationIsBrighTALKHD(final Communication communication) {
    LOGGER.debug("Validate comunication provider is of type [brighttalkhd].");
    if (!communication.isBrightTALKHD()) {
      throw new ProviderNotSupportedException("The supplied communication provider [" + communication.getProvider()
          + "] is not supported.", new Object[] { communication.getProvider().getName() });
    }
    LOGGER.debug("Comunication is of type [brighttalkhd].");
  }

  /**
   * @param communication The communication to validates.
   * @throws ProviderNotSupportedException In case of the communication provider is not "Video".
   */
  protected void validateCommunicationIsVideo(final Communication communication) {
    if (!communication.isVideo()) {
      throw new ProviderNotSupportedException("The supplied communication provider [" + communication.getProvider()
          + "] is not supported.", new Object[] { communication.getProvider().getName() });
    }
  }

  /**
   * Find a communication for the given communication id.
   * 
   * @param webcastId of the communication to load
   * @return the commnuication
   * @throws NotFoundException if the webcast has not been found.
   */
  protected Communication findCommunication(final Long webcastId) {
    LOGGER.debug("Finding communication [" + webcastId + "].");

    return communicationFinder.find(webcastId);
  }

  /**
   * Set the commnunication url.
   * 
   * @param channel used to set the communication url based on the features.
   * @param communication to which the urls need to be set
   */
  protected void setCommunicationUrl(final Channel channel, final Communication communication) {
    communication.setCalendarUrlGenerator(calendarUrlGenerator);
    communication.setCommunicationPortalUrlGenerator(communicationPortalUrlGenerator);
    communication.setCommunicationUrl(channel);
  }

  /**
   * Get the list of syndicated communications.
   * 
   * This will get the list of consumer syndications, set the correct configurations as well as communication urls.
   * 
   * @param webcastId of the master communication
   * @return list of consumer communication including the master one.
   */
  protected List<Communication> getSyndicatedCommunications(final Long webcastId) {
    Communication communication = findCommunication(webcastId);

    List<CommunicationSyndication> consumerSyndications = loadConsumerSyndication(communication);
    List<Communication> communications = new ArrayList<Communication>();

    communications.add(communication);
    for (CommunicationSyndication consumerSyndication : consumerSyndications) {
      Communication copiedCommunication = new Communication(communication);
      copiedCommunication.setChannelId(consumerSyndication.getChannel().getId());

      CommunicationConfiguration communicationConfiguration = configurationDbDao.get(
          copiedCommunication.getChannelId(), copiedCommunication.getId());

      copiedCommunication.setConfiguration(communicationConfiguration);

      Channel channel = channelFinder.find(consumerSyndication.getChannel().getId());
      setCommunicationUrl(channel, copiedCommunication);

      communications.add(copiedCommunication);
    }

    return communications;
  }

  /**
   * Load the list of consumer syndications.
   * 
   * @param communication master
   * @return list of consumer syndication
   */
  protected List<CommunicationSyndication> loadConsumerSyndication(final Communication communication) {
    return syndicationDbDao.findConsumerChannelsSyndication(communication.getId());
  }

  /**
   * Assert the given communication can be updated. A communication cannot be updated if its cancelled or deleted.
   * 
   * @param newCommunication used to check if it can be updated.
   * @throws InvalidCommunicationException if it cannot be updated.
   */
  protected void assertCanBeUpdated(final Communication newCommunication) {
    // verify that the communication can be updated or not
    if (!canBeUpdated(newCommunication)) {
      throw new InvalidCommunicationException("Communication [" + newCommunication.getId() + "] with status ["
          + newCommunication.getStatus() + "] cannot be updated.",
          InvalidCommunicationException.ERROR_CODE_INVALID_COMMUNICATION);
    }
  }

  /**
   * verify that a webcast can be updated.
   * 
   * To be updated, it status must not be cancelled or deleted.
   * 
   * @param communication to check
   * 
   * @return true if the webcast can be updated, else false.
   */
  private boolean canBeUpdated(final Communication communication) {
    return !communication.isCancelled() && !communication.isDeleted();
  }
}