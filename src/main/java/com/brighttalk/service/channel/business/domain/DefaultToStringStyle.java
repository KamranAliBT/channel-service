/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: DefaultToStringStyle.java 83983 2014-09-29 12:51:47Z jbridger $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import org.apache.commons.lang.builder.StandardToStringStyle;

/**
 * This style will use a comma to delimit fields and hide the class name and hash code of the object.
 * 
 * The format will be: [key_1=<value_1>, ..., key_n=<value_n>].
 */
public class DefaultToStringStyle extends StandardToStringStyle {

  private static final long serialVersionUID = 5634074508539326199L;

  private static final String FIELD_SEPARATOR = ", ";

  /**
   * Constructor.
   */
  public DefaultToStringStyle() {
    super.setFieldSeparator(FIELD_SEPARATOR);
    super.setUseClassName(false);
    super.setUseIdentityHashCode(false);
  }

}