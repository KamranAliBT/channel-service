/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * The Communication Update Service Business API.
 * <p>
 * The Communication Update Service is responsible for updating (including rescheduling) communications (aka webcasts)
 * and informing other services of the updating event.
 * <p>
 */
public interface CommunicationUpdateService {

  /**
   * Updates a communication in the identified channel and published the update event to other services that need to
   * know. Performs authorisation and authentication checking. That is validates the given user is authenticated and
   * authorised (channel owner, or BrightTALK manger) to update the given communication.
   * 
   * Intended for external use.
   * 
   * @param user The user performing the create (channel owner or BrightTALK manager).
   * @param channel the channel in which we update the webcast
   * @param communication The communication to be updated.
   * 
   * @return The id of the updated communication.
   * 
   * @throws com.brighttalk.common.user.error.UserAuthorisationException if the user is not authorised to update the
   * communication
   * @throws com.brighttalk.service.channel.business.error.NotFoundException if the channel to update the communication
   * in cannot be found
   * @throws com.brighttalk.service.channel.business.error.ContentPlanException if the update of the communication is
   * outside of the channel content plan rules.
   * @throws com.brighttalk.service.channel.business.error.InvalidCommunicationException if there are business rule
   * violations when updating the communication
   * @throws com.brighttalk.service.channel.business.error.ProviderNotSupportedException In case of the communication
   * provider is not "BrightTALK HD".
   */
  Long update(User user, Channel channel, Communication communication);

  /**
   * Updates a communication in the identified channel and published the update event to other services that need to
   * know. Does not perform any authentication/authorisation checking. Intended for trusted internal services to perform
   * an update.
   * 
   * "@Transactional" means that the method will be executed transactionally. If an Exception is being thrown, all the
   * changes made will be reverted to a previous known state. Propagation.REQUIRED means that the transaction is always
   * required for this method. The Rollback will happen when an exception has been caught.
   * 
   * @param channel the channel in which we update the webcast
   * @param communication The communication to be updated.
   * 
   * @return The id of the updated communication.
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException if the channel to update the communication
   * in cannot be found
   * @throws com.brighttalk.service.channel.business.error.ContentPlanException if the update of the communication is
   * outside of the channel content plan rules.
   * @throws com.brighttalk.service.channel.business.error.InvalidCommunicationException if there are business rule
   * violations when updating the communication
   * @throws com.brighttalk.service.channel.business.error.ProviderNotSupportedException In case of the communication
   * provider is not "BrightTALK HD".
   */
  Long update(Channel channel, Communication communication);

  /**
   * Update the status of a communication.
   * 
   * @param channel master of the communication
   * @param communication to be updated
   */
  void updateStatus(Channel channel, Communication communication);

  /**
   * Update Feature Image on a communication. This will update either the thumbnail or the preview url.
   * 
   * @param communication to be updated.
   */
  void updateFeatureImage(Communication communication);

  /**
   * Callback to handle any asynchronous reschedule webcast messages to be sent to the email service.
   * 
   * @param webcastId id of the rescheduled webcast.
   */
  void emailRescheduleCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous video upload complete messages to be sent to the email service.
   * 
   * @param webcastId id of the sucessfully encoded webcast.
   */
  void emailVideoUploadCompleteCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous video upload failed messages to be sent to the email service.
   * 
   * @param webcastId id of the failed encoded webcast.
   */
  void emailVideoUploadErrorCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous missed you emails.
   * 
   * @param webcastId id of the webcast that just completed.
   */
  void emailMissedYouCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous recording published emails.
   * 
   * @param webcastId id of the webcast that just completed.
   */
  void emailRecordingPublishedCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous reschedule webcast messages to be sent to the Summit Service.
   * 
   * @param webcastId id of the rescheduled webcast.
   */
  void summitRescheduleCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous messages to notification service when a webcast has ended.
   * 
   * @param webcastId id of the ended webcast.
   */
  void notificationInformCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous messages to image converter service.
   * 
   * @param webcastId id of the ended webcast.
   */
  void imageConverterInformCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous reschedule webcast messages to be sent to the Live Event Service (LES).
   * 
   * @param webcastId id of the rescheduled webcast.
   */
  void lesRescheduleCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous essages to be sent to the Live Event Service (LES) when the webcast encoding
   * has been successful and the webcast has been set to recorded.
   * 
   * @param webcastId id of the recorded webcast.
   */
  void lesVideoUploadCompleteCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous essages to be sent to the Live Event Service (LES) when the webcast encoding
   * failed.
   * 
   * @param webcastId id of the recorded webcast.
   */
  void lesVideoUploadErrorCallback(Long webcastId);

}
