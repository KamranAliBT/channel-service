/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelService.java 75382 2014-03-18 15:26:05Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * The Channel Service Business API.
 * <p>
 * Responsible for managing {@link Channel channels}.
 * <p>
 */
public interface ChannelService {

  /**
   * Creates the given channel.
   * <p>
   * Note: This is not used yet by any APIs.
   * 
   * @param user details of the user requesting channel creation.
   * @param channel details of the channel to be created
   * 
   * @return {@link Channel}
   */
  Channel create(User user, Channel channel);

  /**
   * Creates the given channel and store the given contact details.
   * <p>
   * Note: This is not used yet by any APIs.
   * 
   * @param user details of the user requesting channel creation.
   * @param channel details of the channel to be created
   * @param contactDetails contact details of the user creating the channel
   * 
   * @return {@link Channel}
   */
  Channel create(User user, Channel channel, ContactDetails contactDetails);

  /**
   * Callback to handle any asynchronous create channel operations.
   * 
   * @param channelId id of the channel to be created.
   */
  void createCallback(Long channelId);

  /**
   * Updates the given channel if the given user is authorized for an update.
   * 
   * @param user authenticated user
   * @param channel details to be updated
   * 
   * @return updated channel
   */
  Channel update(User user, Channel channel);

  /**
   * Delete the given channel.
   * 
   * @param user the user requesting the delete - who must be authorized to do the delete.
   * @param channelId of the channel to be deleted.
   * 
   * @throws com.brighttalk.common.user.error.UserAuthorisationException if the user is not authorized to perform the
   * delete.
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException if the channel has not been found.
   * 
   * @see #deleteCallback(Long channelId)
   */
  void delete(User user, Long channelId);

  /**
   * Callback to handle any asynchronous delete channel operations.
   * 
   * @param channelId id of the channel to be deleted.
   */
  void deleteCallback(Long channelId);

  /**
   * Unsubscribes a user for a given channel.
   * 
   * @param user the user to unsubcribe
   * @param channelId id of the channel to unsubcribe the user from
   */
  void unsubscribeUserFromChannel(User user, Long channelId);

  /**
   * Unsubscribes a user from all channels.
   * 
   * @param user the user to unsubcribe
   */
  void unsubscribeUserFromChannels(User user);

  /**
   * Gets the contact details for the identified channel.
   * 
   * @param user The user accessing the contact details.
   * @param channelId ID of the channel to get the contact details for.
   * 
   * @throws com.brighttalk.common.user.error.UserAuthorisationException if the user is not authorized to perform the
   * delete.
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException if the channel has not been found.
   * 
   * @return The contact details for the channel.
   */
  ContactDetails getContactDetails(User user, Long channelId);

  /**
   * Updates the contact details for the identified channel.
   * 
   * @param user The user updating the contact details.
   * @param channelId ID of the channel to update the contact details for.
   * @param contactDetails The new contact details to update the channel with.
   * 
   * @throws com.brighttalk.common.user.error.UserAuthorisationException if the user is not authorized to perform the
   * delete.
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException if the channel has not been found.
   * @throws com.brighttalk.common.error.ApplicationException if unable to update the contact details.
   * 
   * @return The contact details for the channel.
   */
  ContactDetails updateContactDetails(User user, Long channelId, ContactDetails contactDetails);
}