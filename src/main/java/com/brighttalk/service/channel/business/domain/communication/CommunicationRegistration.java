/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationRegistration.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import java.util.Date;

/**
 * Represents a communication registration wrapper.
 * 
 * The class contains information about individual communication registration. The identifiers : id, channel id and
 * communication id and also when the registration happened.
 */
public class CommunicationRegistration {

  private Long id;

  private Long channelId;

  private Long communicationId;

  private Date registered;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getChannelId() {
    return channelId;
  }

  public void setChannelId(Long channelId) {
    this.channelId = channelId;
  }

  public Long getCommunicationId() {
    return communicationId;
  }

  public void setCommunicationId(Long communicationId) {
    this.communicationId = communicationId;
  }

  public Date getRegistered() {
    return registered;
  }

  public void setRegistered(Date registered) {
    this.registered = registered;
  }
}