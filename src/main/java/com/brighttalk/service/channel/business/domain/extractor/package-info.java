/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 91635 2015-03-13 16:31:34Z ssarraj $
 * ****************************************************************************
 */
/**
 * This package contains classes for extracting specific elements from the business domain objects in the Channel Service.
 */
package com.brighttalk.service.channel.business.domain.extractor;