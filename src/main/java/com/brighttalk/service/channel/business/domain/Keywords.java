/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationFinder.java 70153 2013-10-23 17:23:51Z acairns $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import java.util.List;

/**
 * The Keywords class manages the channel/communication keywords/tags.
 * <p>
 * Responsibilities:
 * <li>Cleaning or add quotes to comma separated keywords string.</li>
 * <li>Convert comma separated keywords string to list of keywords.</li>
 * <li>Convert List of keywords to comma separated keywords string.</li>
 */
public interface Keywords {

  /**
   * replaces all the spaces not in quotes with commas and removes all the quotes set.
   * 
   * @param keywords the keywords to be cleaned
   * 
   * @return the cleaned set of keywords
   */
  String clean(String keywords);

  /**
   * add set of quotes around the given keyword string.
   * 
   * @param keywords the keywords list to which we need to add quotes
   * 
   * @return the keyword string with quotes
   */
  String addQuotes(String keywords);

  /**
   * Remove quotes around the keywords for the given list.
   * 
   * @param keywords the keywords list to which we need to remove the quotes
   * 
   * @return the keywords list with no quotes
   */
  List<String> removeQuotes(List<String> keywords);

  /**
   * Convert the supplied list of keywords into one String separated each keyword by a comma.
   * 
   * @param keywords List keywords to convert.
   * @return Keywords String.
   */
  String convertListOfKeywordsToKeywordsString(List<String> keywords);

  /**
   * Convert the supplied keywords string into list of keywords.
   * 
   * @param strKeywords The string keywords to convert.
   * @return List of keywords split by comma.
   */
  List<String> convertKeywordsStringToListOfKeywords(String strKeywords);

}