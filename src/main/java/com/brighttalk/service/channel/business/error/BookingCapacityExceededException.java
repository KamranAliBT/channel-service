/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: BookingExceededException.java 62869 2013-04-09 09:17:41Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that when booking or updating a communication, the booking has failed.
 */
@SuppressWarnings("serial")
public class BookingCapacityExceededException extends ApplicationException {

  /**
   * Error code for the booking exception classe.
   */
  public static enum ErrorCode {
    /**
     * Content plan default exception
     */
    BookingCapacityExceeded;
  }

  /** Content plan default error code */
  public static final String ERROR_CODE = ErrorCode.BookingCapacityExceeded.toString();

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE}.
   */
  private String errorCode = ERROR_CODE;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public BookingCapacityExceededException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public BookingCapacityExceededException(final String message, final String errorCode) {
    super(message, errorCode);
    this.setUserErrorCode(errorCode);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public BookingCapacityExceededException(final String message) {
    super(message, ERROR_CODE);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

  /**
   * Sets the errorCode to allow overriding default one.
   * 
   * @param errorCode the new error code
   */
  public void setUserErrorCode(final String errorCode) {
    this.errorCode = errorCode;
  }
}