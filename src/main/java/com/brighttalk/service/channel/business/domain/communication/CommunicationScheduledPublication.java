/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: codetemplates.xml 41756 2012-02-02 11:15:06Z rgreen $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;

import com.brighttalk.service.channel.business.domain.validator.CommunicationUpdateValidator;
import com.brighttalk.service.channel.business.domain.validator.Validator;

/**
 * Class that manages the scheduled publication of a webcast.
 */
public class CommunicationScheduledPublication {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(CommunicationUpdateValidator.class);

  /**
   * Default scheduled publication date. This is used to unset a scheduled publication.
   */
  public static final String DEFAULT_DATE = "2000-01-01 00:00:00";

  /** Date of scheduled publication. Make the communication published. */
  private Date publicationDate;

  /** Date of scheduled unpublication. Make the communication unpublished. */
  private Date unpublicationDate;

  /**
   * Public constructor of the class.
   */
  public CommunicationScheduledPublication() {
  }

  /**
   * Get the publication date.
   * 
   * @return the publication date
   */
  public Date getPublicationDate() {
    return publicationDate;
  }

  /**
   * Set the publication date.
   * 
   * @param publicationDate the publication date
   */
  public void setPublicationDate(final Date publicationDate) {
    this.publicationDate = publicationDate;
  }

  /**
   * Return a boolean to validate if a communication has a publication date.
   * 
   * @return true is a publication date has been set else false
   */
  public boolean hasPublicationDate() {
    return publicationDate != null;
  }

  /**
   * Return a boolean if the publication date is set.
   * 
   * @return boolean if the publication date is set
   */
  public boolean isPublicationDateSet() {
    return publicationDate != null;
  }

  public boolean isDefaultPublicationDate() {
    return publicationDate.equals(this.getDefaultScheduledPublicationDate());
  }

  public Date getDefaultScheduledPublicationDate() {
    return DateTimeFormat.forPattern("yyyy-mm-dd HH:mm:ss").parseDateTime(DEFAULT_DATE).toDate();
  }

  /**
   * Returns the unpublication date.
   * 
   * @return the unpublication date.
   */
  public Date getUnpublicationDate() {
    return unpublicationDate;
  }

  /**
   * Set the unpublication date.
   * 
   * @param unpublicationDate the date of the scheduled unpublication
   */
  public void setUnpublicationDate(final Date unpublicationDate) {
    this.unpublicationDate = unpublicationDate;
  }

  /**
   * Return a boolean to validate if a communication has a publication date.
   * 
   * @return true is a publication date is set else false
   */
  public boolean hasUnpublicationDate() {
    return unpublicationDate != null;
  }

  /**
   * Return a boolean if the unpublication date is set.
   * 
   * @return boolean if the unpublication date is set
   */
  public boolean isUnpublicationDateSet() {
    return unpublicationDate != null;
  }

  public boolean isDefaultUnpublicationDate() {
    return unpublicationDate.equals(this.getDefaultScheduledPublicationDate());
  }

  /**
   * Are either one of the publication dates set for this communication.
   * 
   * @return boolean TRUE if either date is set
   */
  public boolean hasScheduledPublicationDates() {
    return this.isPublicationDateSet() || this.isUnpublicationDateSet();
  }

  /**
   * Validates this communication scheduled publication with given validator. Throws various validation exceptions
   * depending on the validator used.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<CommunicationScheduledPublication> validator) {
    validator.validate(this);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder("CommunicationScheduledPublication\n");
    builder.append(super.toString());
    builder.append("[publicationDate=" + this.getPublicationDate() + "]\n");
    builder.append("[unpublicationDate=" + this.getUnpublicationDate() + "]\n");
    builder.append("[hasPublicationDate=" + this.hasPublicationDate() + "]\n");
    builder.append("[hasUnpublicationDate=" + this.hasUnpublicationDate() + "]\n");
    builder.append("[isPublicationDateSet=" + this.isPublicationDateSet() + "]\n");
    builder.append("[isUnpublicationDateSet=" + this.isUnpublicationDateSet() + "]\n");
    return builder.toString();
  }
}
