/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CreateChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Interface for informing LES of some actions processed on the webcast.
 * 
 * The operations that need to be done asynchronously include tasks that must be performed by other services.
 * 
 * To perform those operations service API has a separate callback method.
 */
public interface LesQueuer {

  /**
   * Perform asynchronous operations on a given communication required when performing a reschedule operation.
   * 
   * @param webcastId id of the webcast to be rescheduled
   */
  void reschedule(Long webcastId);

  /**
   * Perform asynchronous operations on a given communication required when performing a cancel operation.
   * 
   * @param webcastId id of the webcast to be cancelled
   */
  void cancel(Long webcastId);

  /**
   * Perform asynchronous operations on a given communication when video upload has been completed successfully.
   * 
   * @param webcastId id of the webcast for which the video upload has been successful
   */
  void videoUploadComplete(Long webcastId);

  /**
   * Perform asynchronous operations on a given communication when video upload has failed.
   * 
   * @param webcastId id of the webcast for which the video upload failed
   */
  void videoUploadError(Long webcastId);
}
