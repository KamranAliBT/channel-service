/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelPortalUrlGenerator.java 74727 2014-02-26 14:15:00Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import com.brighttalk.service.channel.business.domain.Feature;

/**
 * Implementation of a URL Generator that generates the Channel URL.
 */
public class ChannelPortalUrlGenerator implements ChannelUrlGenerator {

  /**
   * Token used to recognize if the custom channel url is empty.
   */
  protected static final String CHANNEL_URL_EMPTY_VALUE = "noChannelUrl";

  private String channelUriTemplate;

  /**
   * This template is used to create channel url and is set when the bean is instantiated in Spring config.
   * 
   * @return uri template
   */
  public String getChannelUriTemplate() {
    return channelUriTemplate;
  }

  public void setChannelUriTemplate(final String channelUriTemplate) {
    this.channelUriTemplate = channelUriTemplate;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String generate(final Channel channel) {

    final String customUrl = getCustomChannelUrl(channel);

    if (customUrl != null) {
      return customUrl;
    }

    return getDefaultPortalChannelUrl(channel);
  }

  /**
   * Get any custom url for this channel.
   * 
   * @param channel details
   * 
   * @return string|null any custom url - null if feature is not enabled - empty string if no url exsits for this
   * channel
   */
  private String getCustomChannelUrl(final Channel channel) {

    Feature customUrlFeature = channel.getFeature(Feature.Type.CUSTOM_URL);

    if (customUrlFeature.isEnabled() && customUrlFeature.hasValue()) {
      if (customUrlFeature.getValue().equals(CHANNEL_URL_EMPTY_VALUE)) {
        return "";
      }
      return customUrlFeature.getValue();
    }
    return null;
  }

  /**
   * Get the default Url of this channel as it appears on Portal.
   * 
   * @param channel details
   * 
   * @return string The Url
   */
  private String getDefaultPortalChannelUrl(final Channel channel) {
    return channelUriTemplate.replace("{channelId}", channel.getId().toString());
  }

}