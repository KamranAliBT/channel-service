/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: InvalidCommunicationException.java 70246 2013-10-25 14:40:10Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that provided Communication is invalid. The exception will be thrown by presentation layer validator.
 */
@SuppressWarnings("serial")
public class InvalidCommunicationException extends ApplicationException {

  /**
   * Error code for the communication create validator.
   */
  public static enum ErrorCode {
    /**
     * Max duration exceeded.
     * 
     * This is thrown when the communication duration is greater than the channel max duration feature.
     */
    CommunicationTooLong,
    /**
     * Future only.
     * 
     * This is thrown when the scheduled time of the webcast is in the past.
     */
    ScheduledTimeInTheFuture,
    /**
     * Communication overlap.
     * 
     * This is thrown when the communication overlaps with another webcast on the channel.
     */
    OverlappingCommunication,
    /**
     * Reschedule time is invalid.
     * 
     * This is thrown when the communication schedule time is not in the correct authoring period.
     */
    InvalidRescheduleTime,
    /**
     * Content invalid communication error code
     */
    InvalidCommunication;
  }

  /**
   * Error code for invalid communication.
   */
  public static final String ERROR_CODE_INVALID_COMMUNICATION = ErrorCode.InvalidCommunication.toString();

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_INVALID_COMMUNICATION}.
   */
  private String errorCode = ERROR_CODE_INVALID_COMMUNICATION;

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public InvalidCommunicationException(final String message, final String errorCode) {
    super(message, errorCode);
    this.setUserErrorCode(errorCode);
    this.setUserErrorMessage(errorCode);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

  /**
   * Sets the errorCode to allow overriding default one.
   * 
   * @param errorCode the new error code
   */
  public void setUserErrorCode(final String errorCode) {
    this.errorCode = errorCode;
  }
}