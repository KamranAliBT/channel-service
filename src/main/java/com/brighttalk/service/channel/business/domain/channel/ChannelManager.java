/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManager.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.common.user.User;

/**
 * Represents a BrightTALK {@link ChannelManager channel manager}.
 * <p>
 * Users who have been granted access to channels can see them listed in the "Channels I Own" section of the
 * "My BrightTALK" pages.
 * </p>
 * <p>
 * Channel Managers have full access to manage the channel contents.
 * </p>
 */
public class ChannelManager implements Serializable {

  private static final long serialVersionUID = 1L;

  /** Channel manager id */
  private Long id;

  /** BrightTALK user wrapped in this channel manager. */
  private User user;

  /** Channel that the manager has access. */
  private Channel channel;

  /** Flag to inform if the channel manager can receive the channel owner emails. */
  private boolean emailAlerts;

  /**
   * Default constructor.
   */
  public ChannelManager() {

  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(final User user) {
    this.user = user;
  }

  public Channel getChannel() {
    return channel;
  }

  public void setChannel(final Channel channel) {
    this.channel = channel;
  }

  public boolean getEmailAlerts() {
    return emailAlerts;
  }

  public void setEmailAlerts(final boolean emailAlerts) {
    this.emailAlerts = emailAlerts;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if (!(obj instanceof ChannelManager)) {
      return false;
    }

    ChannelManager channelManager = (ChannelManager) obj;

    if (id != null) {
      return id.equals(channelManager.getId());
    }

    return false;
  }

  @Override
  public int hashCode() {
    int result = id == null ? 0 : id.hashCode();
    return result * 37;
  }

  @Override
  public String toString() {

    ToStringBuilder builder = new ToStringBuilder(this);

    builder.append("id", id);
    builder.append("user", user);
    builder.append("channel", channel);
    builder.append("emailAlerts", emailAlerts);

    return super.toString() + builder.toString();
  }

}
