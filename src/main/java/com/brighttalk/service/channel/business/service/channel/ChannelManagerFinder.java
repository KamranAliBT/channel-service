/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerService.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria;

/**
 * The Channel Manager Finder Business API.
 * <p>
 * Responsible for finding {@link ChannelManager channel managers}.
 * <p>
 */
public interface ChannelManagerFinder {

  /**
   * Find channel managers by the given search criteria.
   * 
   * @param user channel owner or brighttalk manager
   * @param channelId Channel id
   * @param searchCriteria criteria
   * 
   * @return paginated list
   */
  PaginatedList<ChannelManager> find(User user, Long channelId, ChannelManagersSearchCriteria searchCriteria);

}