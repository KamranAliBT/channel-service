/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2011.
 * All Rights Reserved.
 * $Id: ImageConverterSettingsGenerator.java 65405 2013-06-10 15:01:47Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Generator Class for generating Image Converter Settings objects.
 */
@Service
public class ImageConverterSettingsGenerator {

  @Value("${feature.image.extension}")
  private String featureImageExtension;

  @Value("${feature.image.format}")
  private String featureImageFormat;

  @Value("${preview.image.width}")
  private Integer previewImageWidth;

  @Value("${preview.image.height}")
  private Integer previewImageHeight;

  @Value("${thumbnail.image.width}")
  private Integer thumbnailImageWidth;

  @Value("${thumbnail.image.height}")
  private Integer thumbnailImageHeight;

  @Value("${feature.image.destination}")
  private String featureImageDestination;

  /**
   * Generate settings object for preview image.
   * 
   * @param communication the communication with the feature image details
   * 
   * @return settings
   */
  public ImageConverterSettings generateForPreviewImage(final Communication communication) {

    final String communicationId = communication.getId().toString();

    String destination = getDestinationUrl(communicationId, ImageConverterSettings.TYPE_PREVIEW);

    ImageConverterSettings settings = new ImageConverterSettings();
    settings.setFormat(featureImageFormat);
    settings.setDestination(destination);
    settings.setSource(communication.getPreviewUrl());
    settings.setWidth(previewImageWidth);
    settings.setHeight(previewImageHeight);
    return settings;
  }

  /**
   * Generate settings object for thumbnail image.
   * 
   * @param communication the communication with the feature image details
   * 
   * @return settings
   */
  public ImageConverterSettings generateForThumbnailImage(final Communication communication) {

    final String communicationId = communication.getId().toString();

    String destination = getDestinationUrl(communicationId, ImageConverterSettings.TYPE_THUMBNAIL);

    ImageConverterSettings settings = new ImageConverterSettings();
    settings.setFormat(featureImageFormat);
    settings.setDestination(destination);
    settings.setSource(communication.getPreviewUrl());
    settings.setWidth(thumbnailImageWidth);
    settings.setHeight(thumbnailImageHeight);
    return settings;
  }

  private String getDestinationUrl(final String communicationId, final String type) {
    Long randomNumber = new DateTime().getMillis();
    String destinationUrl = featureImageDestination.replace("[[communicationId]]", communicationId) + "/" + type
        + randomNumber + "." + featureImageExtension;
    return destinationUrl;
  }

  public String getFeatureImageFormat() {
    return featureImageFormat;
  }

  public void setFeatureImageFormat(final String featureImageFormat) {
    this.featureImageFormat = featureImageFormat;
  }

  public Integer getPreviewImageWidth() {
    return previewImageWidth;
  }

  public void setPreviewImageWidth(final Integer previewImageWidth) {
    this.previewImageWidth = previewImageWidth;
  }

  public Integer getPreviewImageHeight() {
    return previewImageHeight;
  }

  public void setPreviewImageHeight(final Integer previewImageHeight) {
    this.previewImageHeight = previewImageHeight;
  }

  public Integer getThumbnailImageWidth() {
    return thumbnailImageWidth;
  }

  public void setThumbnailImageWidth(final Integer thumbnailImageWidth) {
    this.thumbnailImageWidth = thumbnailImageWidth;
  }

  public Integer getThumbnailImageHeight() {
    return thumbnailImageHeight;
  }

  public void setThumbnailImageHeight(final Integer thumbnailImageHeight) {
    this.thumbnailImageHeight = thumbnailImageHeight;
  }

}