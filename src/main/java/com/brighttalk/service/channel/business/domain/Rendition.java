/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: Rendition.java 85869 2014-11-11 11:17:11Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

/**
 * Represents a BrightTALK rendition.
 * 
 * A rendition is a format of an encoded video. It can be of many types (hls,...)
 * 
 */
public class Rendition {

  /** Rendition id. */
  private Long id;

  /** Is active field of a rendition. */
  private Boolean isActive;

  /** url of a rendition. */
  private String url;

  /** rendition container. */
  private String container;

  /** rendition codecs. */
  private String codecs;

  /** rendition width. */
  private Long width;

  /** rendition bitrate */
  private Long bitrate;

  private String type;

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * @return the isActive
   */
  public Boolean getIsActive() {
    return isActive;
  }

  /**
   * @param isActive the isActive to set
   */
  public void setIsActive(final Boolean isActive) {
    this.isActive = isActive;
  }

  /**
   * @return the url
   */
  public String getUrl() {
    return url;
  }

  /**
   * @param url the url to set
   */
  public void setUrl(final String url) {
    this.url = url;
  }

  /**
   * @return the container
   */
  public String getContainer() {
    return container;
  }

  /**
   * @param container the container to set
   */
  public void setContainer(final String container) {
    this.container = container;
  }

  /**
   * @return the codecs
   */
  public String getCodecs() {
    return codecs;
  }

  /**
   * @param codecs the codecs to set
   */
  public void setCodecs(final String codecs) {
    this.codecs = codecs;
  }

  /**
   * @return the width
   */
  public Long getWidth() {
    return width;
  }

  /**
   * @param width the width to set
   */
  public void setWidth(final Long width) {
    this.width = width;
  }

  /**
   * @return the bitrate
   */
  public Long getBitrate() {
    return bitrate;
  }

  /**
   * @param bitrate the bitrate to set
   */
  public void setBitrate(final Long bitrate) {
    this.bitrate = bitrate;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(final String type) {
    this.type = type;
  }

}
