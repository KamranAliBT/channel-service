/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndications;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.presentation.error.InvalidWebcastException;
import com.brighttalk.service.channel.presentation.error.NotAuthorisedException;

/**
 * Default implementation of the {@link CommunicationUpdateService}.
 * 
 * "@Primary" Indicates that a bean should be given preference when multiple candidates are qualified to autowire a
 * single-valued dependency.
 */
@Service
public class DefaultCommunicationSyndicationService extends AbstractCommunicationService implements CommunicationSyndicationService {
  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(DefaultCommunicationSyndicationService.class);

  /**
   * {@inheritDoc}
   */
  @Override
  public CommunicationSyndications get(Channel channel, Communication communication) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Retrieving the communication [" + communication.getId()
          + "] syndication details belonging to channel [" + channel.getId() + "].");
    }
    CommunicationSyndications communicationSyndications = new CommunicationSyndications();

    // Retrieve the supplied communication syndications.
    List<CommunicationSyndication> consumerSyndications = syndicationDbDao.findConsumerChannelsSyndication(
        communication.getId());

    // Check if the supplied communication is a communication on a master channel.
    if (communication.isMasterCommunication()) {
      // Set the syndication master channel and master channel communication.
      communicationSyndications.setChannel(channel);
      communicationSyndications.setCommunication(communication);

      if (!CollectionUtils.isEmpty(consumerSyndications)) {
        // Extract the syndication consumer channels Ids and find all the channels communication statistics.
        List<Long> channelIds = extractSyndicationConsumerChannelIds(consumerSyndications);
        List<CommunicationStatistics> communicationsStatistics =
            communicationStatisticsDbDao.findCommunicationStatisticsByChannelIds(communication.getId(), channelIds);

        // Set each syndicated communication activities statistics.
        for (CommunicationSyndication communicationSyndication : consumerSyndications) {
          if (communicationSyndication.getChannel() != null) {
            communicationSyndication.setCommunicationStatistics(extractCommunicationStatistics(
                communicationSyndication.getChannel().getId(), communicationsStatistics));
          }
        }

        communicationSyndications.setCommunicationSyndications(consumerSyndications);
      }
    } else {
      // Set the syndication master channel and master channel communication.
      communicationSyndications.setChannel(channelFinder.find(communication.getMasterChannelId()));
      communicationSyndications.setCommunication(communicationDbDao.find(communication.getMasterChannelId(),
          communication.getId()));

      // Retrieve and set the supplied consumer channel syndication details.
      communicationSyndications.setCommunicationSyndications(extractDistributedChannelSyndication(channel.getId(),
          consumerSyndications));
    }

    return communicationSyndications;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Long update(final User user, final Channel channel, final Communication communication) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updating communication [" + communication.getId() + "] syndicated into channel [" + channel.getId()
          + "].");
    }

    validateSyndicatedCommunicationData(communication);
    assertCanOverrideDetails(user, channel);
    updateConfiguration(communication);
    updateCommunicationDetailsOverride(communication);

    return communication.getId();
  }

  /**
   * Validate the syndicated in communication data. Currently only validating that the publish status has not be changed
   * as it can only be altered on the master communication.
   * 
   * @param communication to validate
   * @throws InvalidWebcastException when the publish status is different.
   */
  private void validateSyndicatedCommunicationData(final Communication communication) {
    if (communication.hasPublishStatusChanged()) {
      throw new InvalidWebcastException("Only master channel can update publish status", "InvalidWebcastData");
    }
  }

  /**
   * Assert that the user can override the syndicated webcast details.
   * 
   * only a manager, channel owner are allowed to updated the webcast details. Moreover, in order to be able to update
   * the webcast details, the SYNDICATION_OVERRIDES feature needs to be enabled at channel level.
   * 
   * If the user is a manager, it does not matter if the feature is enabled or not.
   * 
   * @param user performing the update
   * @param channel in which we check the channel feature
   * @throws NotAuthorisedException when the user is not authorised to update the webcast details
   * @throws FeatureNotEnabledException when the feature is disabled.
   */
  private void assertCanOverrideDetails(final User user, final Channel channel) {
    if (user.isManager()) {
      return;
    }

    if (!channel.isChannelOwner(user) && !channel.isChannelManager(user)) {
      throw new NotAuthorisedException(
          "Only channel owners or channel managers are allowed to Override Syndication Details",
          "NotAuthorisedException", new Object[] { channel.getId() });
    }

    Feature syndicationOverridesFeature = channel.getFeature(Type.SYNDICATION_OVERRIDES);
    if (!syndicationOverridesFeature.isEnabled()) {
      throw new FeatureNotEnabledException("Syndication Override Disabled for channel [" + channel.getId() + "]",
          "FeatureNotEnabled");
    }
  }

  /**
   * Update the configuration table.
   * 
   * @param communication to update
   */
  private void updateConfiguration(final Communication communication) {
    configurationDbDao.update(communication.getConfiguration());
  }

  /**
   * Update the communication details override. if the details of the communication are the same as the master
   * communication, we set the communication details override to null.
   * 
   * @param communication to update
   */
  private void updateCommunicationDetailsOverride(final Communication communication) {
    Communication existingCommunication = communicationDbDao.find(communication.getChannelId(), communication.getId());

    if (communication.getTitle().equals(existingCommunication.getTitle())) {
      communication.setTitle(null);
    }

    if (communication.getDescription().equals(existingCommunication.getDescription())) {
      communication.setDescription(null);
    }

    if (communication.getAuthors().equals(existingCommunication.getAuthors())) {
      communication.setAuthors(null);
    }

    if (communication.getKeywords().equals(existingCommunication.getKeywords())) {
      communication.setKeywords(null);
    }

    communicationOverrideDbDao.updateCommunicationDetailsOverride(communication);
  }

  /**
   * Extract the channel ids for all the consumer channels syndication.
   * 
   * @param communicationSyndications The communication syndications details.
   * @return List of channel Ids.
   */
  private List<Long> extractSyndicationConsumerChannelIds(final List<CommunicationSyndication> communicationSyndications) {
    List<Long> channelIds = new ArrayList<>();
    for (CommunicationSyndication communicationSyndication : communicationSyndications) {
      if (communicationSyndication.getChannel() != null) {
        channelIds.add(communicationSyndication.getChannel().getId());
      }
    }
    return channelIds;
  }

  /**
   * Extract communication statistics form the supplied list of statistics for the identified channel.
   * 
   * @param channelId The channel Id.
   * @param communicationsStatistics List of {@link CommunicationStatistics}.
   * @return The extracted {@link CommunicationStatistics}, otherwise Null if the supplied list contain no communication
   * statistics for the supplied channel.
   */
  private CommunicationStatistics extractCommunicationStatistics(final long channelId,
      final List<CommunicationStatistics> communicationsStatistics) {
    for (CommunicationStatistics communicationStatistics : communicationsStatistics) {
      if (communicationStatistics.getChannelId() == channelId) {
        return communicationStatistics;
      }
    }
    return null;
  }

  /**
   * Extract communication syndication form the supplied list of syndications for the identified channel.
   * 
   * @param channelId The channel Id.
   * @param communicationSyndications List of {@link CommunicationSyndication}.
   * @return List of {@link CommunicationSyndication} containing the distributed channel syndication.
   */
  private List<CommunicationSyndication> extractDistributedChannelSyndication(final long channelId,
      final List<CommunicationSyndication> communicationSyndications) {
    List<CommunicationSyndication> distributedSyndication = new ArrayList<>();
    for (CommunicationSyndication communicationSyndication : communicationSyndications) {
      if (communicationSyndication.getChannel() != null && communicationSyndication.getChannel().getId() == channelId) {
        distributedSyndication.add(communicationSyndication);
        break;
      }
    }
    return distributedSyndication;
  }

}