/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CreateChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Interface for deleting a webcast related operations queue for Community.
 * 
 * The operations that need to be done asynchronously include tasks that must be performed by other services.
 * 
 * To perform those operations service API has a separate callback method.
 */
public interface CommunityQueuer {

  /**
   * Perform asynchronous operations on a given communication required when performing a delete operation.
   * 
   * @param webcastId id of the webcast to be deleted
   */
  void delete(Long webcastId);
}
