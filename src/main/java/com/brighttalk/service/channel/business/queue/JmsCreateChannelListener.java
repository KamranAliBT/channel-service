/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsCreateChannelListener.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.brighttalk.service.channel.business.service.channel.ChannelService;

/**
 * JMS Implementation of a Message listener to receive message form the Create Channel queue and use the Service to
 * create channel.
 */
public class JmsCreateChannelListener implements MessageListener {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JmsCreateChannelListener.class);

  @Autowired
  private ChannelService service;

  /** {@inheritDoc} */
  @Override
  public void onMessage(final Message message) {

    if (message instanceof ObjectMessage) {
      try {

        Long channelId = (Long) ((ObjectMessage) message).getObject();
        service.createCallback(channelId);

      } catch (Exception exception) {
        logger.error("Something went wrong", exception);
      }
    } else {
      logger.error("Create channel processing broken, because of illegal argument provided. "
          + "Expected [ObjectMessage] got [" + message + "].");
    }
  }
}