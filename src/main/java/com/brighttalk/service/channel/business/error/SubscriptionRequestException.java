/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionRequestException.java 87022 2014-12-03 13:00:40Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

/**
 * Indicates the request has failed.
 */
@SuppressWarnings("serial")
public class SubscriptionRequestException extends ApplicationException {

  private final ErrorCode errorCode;

  /**
   * @param errorMessage error message
   * @param errorCode error code
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public SubscriptionRequestException(final String errorMessage, final ErrorCode errorCode) {
    super(errorMessage);
    this.errorCode = errorCode;
  }

  public ErrorCode getErrorCode() {
    return errorCode;
  }

}
