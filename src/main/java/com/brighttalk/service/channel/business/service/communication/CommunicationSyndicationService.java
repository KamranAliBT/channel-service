/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndications;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;

/**
 * The Communication Syndication Service.
 * <p>
 * The Communication Syndicate Service is responsible for managing the syndication of communications (aka webcasts).
 * Currently only update is supported.
 * <p>
 */
public interface CommunicationSyndicationService {

  /**
   * Updates a syndicated communication in the identified channel.
   * 
   * @param user The user performing the create (channel owner or BrightTALK manager).
   * @param channel the channel in which we update the webcast
   * @param communication The communication to be updated.
   * 
   * @return The id of the updated communication.
   * 
   * @throws com.brighttalk.common.user.error.UserAuthorisationException if the user is not authorised to update the
   * communication
   * @throws com.brighttalk.service.channel.business.error.NotFoundException if the channel to update the communication
   * in cannot be found
   * @throws com.brighttalk.service.channel.business.error.ContentPlanException if the update of the communication is
   * outside of the channel content plan rules.
   * @throws com.brighttalk.service.channel.business.error.InvalidCommunicationException if there are business rule
   * violations when updating the communication
   * @throws com.brighttalk.service.channel.business.error.ProviderNotSupportedException In case of the communication
   * provider is not "BrightTALK HD".
   */
  Long update(User user, Channel channel, Communication communication);

  /**
   * Retrieves the syndication details for the given communication in a given channel.
   * <p>
   * The {@link CommunicationSyndications} will be returned with the master channel data, the webcast title requested
   * and a list of {@link CommunicationSyndication}.
   * <p>
   * If the request is made from the distributing channel, the list of {@link CommunicationSyndication} will contain
   * each consumer channels in which the webcast has been distributed. The {@link CommunicationStatistics} for each of
   * these distributed webcasts will be returned in their respective channels.
   * <p>
   * However, if the webcast is in the consumer channel, the list of {@link CommunicationSyndication} will only contain
   * the consumer channels in which the webcast has been distributed. The {@link CommunicationStatistics} will not be
   * available.
   * 
   * @param channel The channel where the identified communication belonged to.
   * @param communication The identified communication.
   * @return {@link CommunicationSyndications} containing the syndication details for the identified communication.
   * @throws ChannelNotFoundException in case of the supplied channel does not identify an existing channel.
   * @throws CommunicationNotFoundException in case of the supplied communication does not identify an existing
   * communication.
   * @throws UserAuthorisationException in case of the supplied user not authorised on the supplied channel.
   */
  CommunicationSyndications get(Channel channel, Communication communication);

}
