/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionContext.java 91908 2015-03-18 16:46:30Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Domain Object representing a channel subscription context.
 */
public class SubscriptionContext {

  private Long subscriptionId;
  private SubscriptionLeadType leadType;
  private String leadContext;
  private String engagementScore;

  /** Default Constructor. */
  public SubscriptionContext() {
  }

  /**
   * @param leadType The channel subscriber lead type.
   * @param leadContext The channel subscriber lead context.
   * @param engagementScore The channel subscriber lead engagement score.
   */
  public SubscriptionContext(SubscriptionLeadType leadType, String leadContext, String engagementScore) {
    this.leadType = leadType;
    this.leadContext = leadContext;
    this.engagementScore = engagementScore;
  }

  /**
   * @return the subscriptionId
   */
  public Long getSubscriptionId() {
    return subscriptionId;
  }

  /**
   * @param subscriptionId the subscriptionId to set
   */
  public void setSubscriptionId(Long subscriptionId) {
    this.subscriptionId = subscriptionId;
  }

  /**
   * @return the leadType
   */
  public SubscriptionLeadType getLeadType() {
    return leadType;
  }

  /**
   * @param leadType the leadType to set
   */
  public void setLeadType(SubscriptionLeadType leadType) {
    this.leadType = leadType;
  }

  /**
   * @return the leadContext
   */
  public String getLeadContext() {
    return leadContext;
  }

  /**
   * @param leadContext the leadContext to set
   */
  public void setLeadContext(String leadContext) {
    this.leadContext = leadContext;
  }

  /**
   * @return the engagementScore
   */
  public String getEngagementScore() {
    return engagementScore;
  }

  /**
   * @param engagementScore the engagementScore to set
   */
  public void setEngagementScore(String engagementScore) {
    this.engagementScore = engagementScore;
  }

  /**
   * Checks this subscription context is for summit lead type.
   * 
   * @return <code>true</code> if the lead type is summit, <code>false</code> otherwise.
   */
  public boolean isLeadTypeSummit() {
    return leadType != null && SubscriptionLeadType.isLeadTypeSummit(leadType.getType());
  }

  /**
   * Checks this subscription context is for content lead type.
   * 
   * @return <code>true</code> if the lead type is content, <code>false</code> otherwise.
   */
  public boolean isLeadTypeContent() {
    return leadType != null && SubscriptionLeadType.isLeadTypeContent(leadType.getType());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("subscriptionId", subscriptionId);
    builder.append("leadType", leadType);
    builder.append("leadContext", leadContext);
    builder.append("engagementScore", engagementScore);
    return builder.toString();
  }

}
