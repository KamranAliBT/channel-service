/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: OverlappingContractPeriod.java 101545 2015-10-16 11:26:42Z kali ${FILE_NAME} 123456 2015-10-16 11:45 kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Thrown when a contract period overlaps and existing contract period for the same channel
 */
@SuppressWarnings("serial")
public class OverlappingContractPeriod extends ApplicationException{
  public OverlappingContractPeriod(String message, String errorCode) {
    super(message, errorCode);
  }
}
