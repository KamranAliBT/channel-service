/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelUrlGenerator.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

/**
 * Interface to be implemented by all Channel Url Generators.
 */
public interface ChannelUrlGenerator {

  /**
   * Generate a Url for a channel.
   * 
   * This Method uses the class level uri template variable as a template - This needs to be set when the bean is
   * instantiated in Spring config.
   * 
   * @param channel from which to generate the url
   * 
   * @return The Generated Url
   */
  String generate( Channel channel );
}