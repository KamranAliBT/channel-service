/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerNotFoundException.java 69660 2013-10-14 11:30:45Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

/**
 * Indicates the channel manager has not been found.
 */
@SuppressWarnings("serial")
public class ChannelManagerNotFoundException extends NotFoundException {

  private final Long channelManagerId;

  /**
   * @param channelManagerId see below.
   * 
   * @see ChannelManagerNotFoundException#ChannelManagerNotFoundException(Long, Throwable)
   */
  public ChannelManagerNotFoundException(final Long channelManagerId) {
    this(channelManagerId, null);
  }

  /**
   * @param channelManagerId see below.
   * @param cause see below.
   * 
   * @see NotFoundException#NotFoundException(String, Throwable)
   */
  public ChannelManagerNotFoundException(final Long channelManagerId, final Throwable cause) {
    super("Channel manager [" + channelManagerId + "] not found.", cause);
    this.channelManagerId = channelManagerId;
  }

  public Long getChannelManagerId() {
    return channelManagerId;
  }

}
