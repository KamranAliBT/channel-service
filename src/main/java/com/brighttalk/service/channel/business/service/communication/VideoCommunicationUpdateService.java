/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.InvalidCommunicationException;

/**
 * Implementation of the {@link CommunicationUpdateService} that manages the updating (including rescheduling) of video
 * webcasts.
 * 
 * "@Primary" Indicates that a bean should be given preference when multiple candidates are qualified to autowire a
 * single-valued dependency.
 * 
 * "@Transaction" Indicates that if the DAO methods throw an Exception, the inserted data will be roll backed to the
 * previous state. Propagation.SUPPORTS means that if a transaction exist it'll be supported else the method will be
 * executed as non transactional.
 */
@Service(value = "VideoCommunicationUpdateService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class VideoCommunicationUpdateService extends AbstractCommunicationService implements CommunicationUpdateService {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(VideoCommunicationUpdateService.class);

  /** Communication syndication service. */
  @Autowired
  protected CommunicationSyndicationService communicationSyndicationService;

  /**
   * {@inheritDoc}
   * 
   * "@Transactional" means that the method will be executed transactionally. If an Exception is being thrown, all the
   * changes made will be reverted to a previous known state. Propagation.REQUIRED means that the transaction is always
   * required for this method. The Rollback will happen when an exception has been caught.
   */
  @Override
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
  public Long update(final Channel channel, final Communication communicationToBeUpdated) {
    // validate that the communication is video
    this.validateCommunicationIsVideo(communicationToBeUpdated);

    assertActiveAsset(communicationToBeUpdated);
    assertCanBeUpdated(communicationToBeUpdated);

    if (communicationToBeUpdated.hasStatus() && communicationToBeUpdated.isProcessingFailed()) {
      informOtherServicesOfFailure(communicationToBeUpdated);
      return null;
    }

    Communication updatedCommunication = communicationDbDao.update(communicationToBeUpdated);

    Long communicationId = updatedCommunication.getId();

    if (communicationToBeUpdated.hasRenditions()) {
      // disable the rendition asset
      renditionDbDao.removeRenditionAssets(communicationId);

      // find all existing renditions
      List<Rendition> oldRenditions = renditionDbDao.findByCommunicationId(communicationId);

      // delete all renditions individually
      for (Rendition oldRendition : oldRenditions) {
        renditionDbDao.delete(oldRendition.getId());
      }

      // add all renditions and asset
      List<Rendition> newRenditions = communicationToBeUpdated.getRenditions();
      for (Rendition newRendition : newRenditions) {
        renditionDbDao.create(newRendition);
        renditionDbDao.addRenditionAsset(communicationId, newRendition.getId());
      }
    }

    if (communicationToBeUpdated.hasStatus() && communicationToBeUpdated.isRecorded()) {
      communicationDbDao.updateEncodingCount(updatedCommunication.getId());
      informOtherServicesOfUpdate(updatedCommunication);
    }

    return communicationId;
  }

  /**
   * Inform other services there has been an update to a communication.
   * 
   * @param communication the communication created from the request.
   */
  private void informOtherServicesOfUpdate(final Communication communication) {
    LOGGER.info("Informing services of successful video encoding and status update for communication ["
        + communication.getId() + "].");

    emailQueuer.videoUploadComplete(communication.getId(), communication.getProvider().getName());
  }

  /**
   * Inform other services there has been an update to a communication.
   * 
   * @param communication the communication created from the request.
   */
  private void informOtherServicesOfFailure(final Communication communication) {
    LOGGER.info("Informing services of failed video encoding and status update for communication ["
        + communication.getId() + "].");

    emailQueuer.videoUploadError(communication.getId(), communication.getProvider().getName());
  }

  /**
   * Assert the communication is an active asset of its channel.
   * 
   * @param existingCommunication
   * @throw InvalidCommunicationException if it is not.
   */
  private void assertActiveAsset(final Communication existingCommunication) {
    // verify that the communication is an active channel asset
    if (!communicationDbDao.isActiveAsset(existingCommunication)) {
      throw new InvalidCommunicationException("Communication [" + existingCommunication.getId()
          + "] is not an active asset of channel [" + existingCommunication.getChannelId() + "].",
          InvalidCommunicationException.ERROR_CODE_INVALID_COMMUNICATION);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailVideoUploadCompleteCallback(final Long webcastId) {
    LOGGER.debug("Callback to send video upload complete for webcast [" + webcastId + "] in Email.");

    Communication communication = findCommunication(webcastId);
    emailServiceDao.sendVideoUploadComplete(communication);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailVideoUploadErrorCallback(final Long webcastId) {
    LOGGER.debug("Callback to send video upload failed for webcast [" + webcastId + "] in Email.");

    Communication communication = findCommunication(webcastId);
    emailServiceDao.sendVideoUploadError(communication);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Long update(final User user, final Channel channel, final Communication communication) {
    throw new UnsupportedOperationException("Update method not implemented for Video communication. User id ["
        + user.getId() + "] - Channel id [" + channel.getId() + "] - Webcast id [" + communication.getId() + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void updateStatus(final Channel channel, final Communication communication) {
    throw new UnsupportedOperationException(
        "Update status method not implemented for Video communication. Channel id [" + channel.getId()
            + "] - Webcast id [" + communication.getId() + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void updateFeatureImage(final Communication communication) {
    throw new UnsupportedOperationException(
        "Update feature image method not implemented for Video communication. Webcast id [" + communication.getId()
            + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailRescheduleCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "Email Reschedule Callback method not implemented for Video communication. Webcast id [" + webcastId + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailMissedYouCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "Missed you email Callback method not implemented for Video communication. Webcast id [" + webcastId + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailRecordingPublishedCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "Email recording published email Callback method not implemented for Video communication. Webcast id ["
            + webcastId + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void summitRescheduleCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "Summit Reschedule Callback method not implemented for Video communication. Webcast id [" + webcastId + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void notificationInformCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "Notification Inform Callback method not implemented for Video communication. Webcast id [" + webcastId + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void imageConverterInformCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "Image converter inform Callback method not implemented for Video communication. Webcast id [" + webcastId
            + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void lesRescheduleCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "LES Reschedule Callback method not implemented for Video communication. Webcast id [" + webcastId + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void lesVideoUploadCompleteCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "LES Video Upload Complete Callback method not implemented for Video communication. Webcast id [" + webcastId
            + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void lesVideoUploadErrorCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "LES Video Upload Error Callback method not implemented for Video communication. Webcast id [" + webcastId
            + "]");
  }

}
