/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: Validator.java 74757 2014-02-27 11:18:36Z ikerbib $ 
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

/**
 * Validator to validate generic types of business objects. 
 * 
 * Implementation of the visitor pattern.
 * Note the use of generics to increase type safety. 
 * 
 * @see "http://www.javaranch.com/journal/200601/visitor.html"
 * 
 * @param <T> the type of business object to validate e.g.
 * {@link com.brighttalk.service.channel.business.domain.communication.Communication},
 * {@link com.brighttalk.service.channel.business.domain.Resource}
 */
public interface Validator<T> {

  /**
   * Validate the given business abject.
   * 
   * @param t the business object to validate.
   */
  void validate(T t);

}
