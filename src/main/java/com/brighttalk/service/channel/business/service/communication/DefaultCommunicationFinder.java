/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationFinder.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.Session;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;
import com.brighttalk.service.channel.business.domain.MediazoneConfig;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationScheduledPublication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationCategoryDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationOverrideDbDao;
import com.brighttalk.service.channel.integration.database.MediazoneCommunicationDbDao;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;
import com.brighttalk.service.channel.integration.database.ScheduledPublicationDbDao;
import com.brighttalk.service.channel.integration.database.syndication.SyndicationDbDao;

/**
 * Default implementation of the {@link CommunicationFinder}.
 */
@Service
@Primary
public class DefaultCommunicationFinder extends AbstractCommunicationService implements CommunicationFinder {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(DefaultCommunicationFinder.class);

  /** Channel finder */
  @Autowired
  protected ChannelFinder channelFinder;

  /** DB DAO for resources (aka attachments) */
  @Autowired
  protected ResourceDbDao resourceDbDao;

  /** DB DAO for communications */
  @Autowired
  protected CommunicationDbDao communicationDbDao;

  /** DB DAO for mediazone communication. */
  @Autowired
  protected MediazoneCommunicationDbDao mediazoneCommunicationDbDao;

  /** DB DAO for communications override */
  @Autowired
  protected CommunicationOverrideDbDao communicationOverrideDbDao;

  /** DB DAO for categories */
  @Autowired
  protected CategoryDbDao categoryDbDao;

  /** DB DAO for communication categories */
  @Autowired
  protected CommunicationCategoryDbDao communicationCategoryDbDao;

  /** DB DAO for scheduled publication */
  @Autowired
  protected ScheduledPublicationDbDao scheduledPublicationDbDao;

  /** DB DAO for syndication */
  @Autowired
  protected SyndicationDbDao syndicationDbDao;

  @Value("${conference.room.uri}")
  private String conferenceRoomUrl;

  @Value("${communications.list.defaultPageNumber}")
  private int defaultPageNumber;

  @Value("${communications.list.maxPageSize}")
  private int maxPageSize;

  @Value("${communications.list.defaultPageSize}")
  private int defaultPageSize;

  @Value("${communication.uri.hrefTemplate}")
  private String defaultCommunicationUrl;

  /** {@inheritDoc} */
  @Override
  public Communication find(final Long communicationId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding communication [" + communicationId + "].");
    }

    Communication communication = communicationDbDao.find(communicationId);

    Channel channel = channelFinder.find(communication.getChannelId());
    setCommunicationUrl(channel, communication);
    setCommunicationAssets(communication);

    return communication;
  }

  /** {@inheritDoc} */
  @Override
  public Communication find(final Channel channel, final Long communicationId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding communication [" + communicationId + "] on channel [" + channel.getId() + "].");
    }

    Communication communication = communicationDbDao.find(channel.getId(), communicationId);
    setCommunicationUrl(channel, communication);
    setCommunicationAssets(communication);

    return communication;
  }

  /**
   * Sets the communication assets to the given communication. Assets are: - allowed presenters to present the webcasts
   * - communication statistics - communication resources (attachments) - webcast renditions if type of webcast is video
   * - mediazone configs if type of webcast is mediazone
   * 
   * @param communication the communication to assign the assets to.
   */
  private void setCommunicationAssets(final Communication communication) {

    List<Session> presenters = communicationDbDao.findPresenters(communication);
    communication.setPresenters(presenters);

    List<CommunicationStatistics> viewingStatistics = communicationDbDao.findCommunicationStatistics(communication);
    if (!CollectionUtils.isEmpty(viewingStatistics)) {
      communication.setCommunicationStatistics(viewingStatistics.get(0));
    }

    List<Resource> resources = resourceDbDao.findAll(communication.getId());
    communication.setResources(resources);

    if (communication.isMediaZone()) {
      MediazoneConfig mediazoneConfig = mediazoneCommunicationDbDao.findMediazoneConfig(communication.getId());
      communication.setMediazoneConfig(mediazoneConfig);
    }

    if (communication.isVideo()) {
      List<Rendition> renditions = communicationDbDao.findRenditionAssets(communication.getId());
      communication.setRenditions(renditions);
    }

    setCategories(communication);
    setScheduledPublication(communication);
    setCommunicationSyndications(communication);
  }

  /**
   * sets the communication categories to the communication.
   * 
   * @param communication in which the categories are going to be set
   */
  private void setCategories(final Communication communication) {
    HashMap<Long, CommunicationCategory> hashMapCategories = communicationCategoryDbDao.find(
        communication.getChannelId(), communication.getId());

    List<CommunicationCategory> categories = new ArrayList<CommunicationCategory>(hashMapCategories.values());

    communication.setCategories(categories);
  }

  /**
   * Set the communication scheduled publication dates to the given communication.
   * 
   * @param communication to set the scheduled publication date to.
   */
  private void setScheduledPublication(final Communication communication) {

    CommunicationScheduledPublication communicationScheduledPublication =
        scheduledPublicationDbDao.find(communication.getId());

    communication.setScheduledPublication(communicationScheduledPublication);
  }

  /**
   * Set the communication syndication for the given communication. If the communication is the master webcast, we need
   * to set the consumer syndications. If the communication is the consumer webcast, we need to set the master
   * communication.
   * 
   * @param communication to set the syndications to.
   */
  private void setCommunicationSyndications(final Communication communication) {
    setCommunicationOverrideDetails(communication);
    setSyndications(communication);
  }

  private void setCommunicationOverrideDetails(final Communication communication) {
    communicationOverrideDbDao.setCommunicationDetailsOverride(communication);
  }

  private void setSyndications(final Communication communication) {
    CommunicationSyndication communicationSyndication = communication.getConfiguration().getCommunicationSyndication();
    List<CommunicationSyndication> consumerSyndications =
        syndicationDbDao.findConsumerChannelsSyndication(communication.getId());

    if (CollectionUtils.isEmpty(consumerSyndications)) {
      return;
    }

    if (communication.isMasterCommunication()) {
      communicationSyndication.setConsumerSyndications(consumerSyndications);

    } else {
      CommunicationSyndication masterSyndication = null;

      /**
       * Loop through the consumer syndication and load the syndication that has the given channel id. we then set the
       * master channel id to the syndication. We need to do this because we do not store the date and time or the
       * approval status for the master syndication. We need to get the consumer syndication in order to get the
       * approval status as well as the date of the approval.
       */
      for (CommunicationSyndication syndication : consumerSyndications) {

        if (communication.getChannelId().equals(syndication.getChannel().getId())) {
          syndication.getChannel().setId((communication.getMasterChannelId()));
          masterSyndication = syndication;
        }

      }

      communicationSyndication.setMasterSyndication(masterSyndication);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String find(final String pin, final String provider) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding communication for pin [" + pin + "] and provider [" + provider + "].");
    }

    Communication communication = communicationDbDao.find(pin, provider);
    communication.canDialIn();

    return conferenceRoomUrl.replace("{communicationId}", communication.getId().toString());
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<Communication> find(final CommunicationsSearchCriteria criteria) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding communications within channels [" + criteria.getChanelAndCommunicationsIds() + "].");
    }

    populateSearchCriteriaDefaultValues(criteria);

    PaginatedList<Communication> communications = communicationDbDao.find(criteria);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + communications.size() + "] communications. Page number [" + criteria.getPageNumber()
          + "], page size [" + criteria.getPageSize() + "], has next page? [" + communications.hasNextPage() + "].");
    }

    if (communications.isEmpty()) {
      return communications;
    }

    // get channels
    List<Channel> channels = channelFinder.find(getChannelIds(communications));

    for (Communication communication : communications) {
      for (Channel channel : channels) {
        if (communication.getChannelId().equals(channel.getId())) {

          setCommunicationUrl(channel, communication);

          if (criteria.includeCategories()) {
            categoryDbDao.loadCategories(channel.getId(), communication);
          }
        }
      }
    }

    return communications;
  }

  /**
   * Given a list of communications return a list of their channel ids.
   * 
   * @param communications the communications
   * @return the channels ids
   */
  private List<Long> getChannelIds(final List<Communication> communications) {

    if (CollectionUtils.isEmpty(communications)) {
      return new ArrayList<Long>();
    }

    List<Long> channelIds = new ArrayList<Long>();
    for (Communication communication : communications) {
      channelIds.add(communication.getChannelId());
    }

    return channelIds;
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<Communication> findSubscribedChannelsCommunications(final User user,
    final MyCommunicationsSearchCriteria searchCriteria) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding communications from channels user [" + user.getId() + "] is subscribed to.");
    }

    ensureSearchCriteriaPageNumber(searchCriteria);

    PaginatedList<Communication> communications = communicationDbDao.findPaginatedSubscribedChannelsCommunications(
        user.getId(), searchCriteria);

    removeCommunicationDuplications(communications);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + communications.size() + "] communications from channels user [" + user.getId()
          + "] is subscribed to. Page number [" + searchCriteria.getPageNumber() + "], page size ["
          + searchCriteria.getPageSize() + "], has next page? [" + communications.hasNextPage() + "].");
    }

    return communications;
  }

  /**
   * Remove communications duplicated by syndication keeping the one in master channel.
   * 
   * @param communications paginated list of communications
   */
  private void removeCommunicationDuplications(final PaginatedList<Communication> communications) {
    PaginatedList<Communication> communicationsNoDuplications = new PaginatedList<Communication>();

    for (Communication communication : communications) {
      if (communicationsNoDuplications.contains(communication)) {
        if (communication.getConfiguration().isInMasterChannel()) {
          communicationsNoDuplications.remove(communication);
          communicationsNoDuplications.add(communication);
        }
      } else {
        communicationsNoDuplications.add(communication);
      }
    }

    communications.clear();
    communications.addAll(communicationsNoDuplications);
  }

  /** {@inheritDoc} */
  @Override
  public Map<String, Long> findSubscribedChannelsCommunicationsTotals(final Long userId) {

    Map<String, Long> communicationsTotals = communicationDbDao.findSubscribedChannelsCommunicationsTotals(userId);

    return communicationsTotals;
  }

  /**
   * Ensures that page number is set.
   * 
   * @param searchCriteria the search criteria
   */
  private void ensureSearchCriteriaPageNumber(final BaseSearchCriteria searchCriteria) {

    if (searchCriteria.getPageNumber() == null) {
      searchCriteria.setPageNumber(defaultPageNumber);
    }

  }

  /**
   * Set default search criteria values if none present.
   * 
   * Verify that the pageSize does not exceed maximum value.
   * 
   * @param searchCriteria the search criteria
   */
  private void populateSearchCriteriaDefaultValues(final BaseSearchCriteria searchCriteria) {

    // Set default page number
    ensureSearchCriteriaPageNumber(searchCriteria);

    // Set default Page Size
    if (searchCriteria.getPageSize() == null) {
      searchCriteria.setPageSize(defaultPageSize);
    }

    // Limit Page Size to max allowed
    if (searchCriteria.getPageSize() > maxPageSize) {
      searchCriteria.setPageSize(maxPageSize);
    }
  }

}