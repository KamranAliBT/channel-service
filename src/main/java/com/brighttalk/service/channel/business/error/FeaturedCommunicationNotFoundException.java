/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FeaturedCommunicationNotFoundException.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates a featured communication has not been found on a channel.
 */
@SuppressWarnings("serial")
public class FeaturedCommunicationNotFoundException extends ApplicationException {

  private static final String ERROR_CODE_NOT_FOUND_ERROR = "FeaturedCommunicationNotFound";
  
  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_NOT_FOUND_ERROR}.
   */
  private String errorCode = ERROR_CODE_NOT_FOUND_ERROR;
  
  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public FeaturedCommunicationNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public FeaturedCommunicationNotFoundException(String message, String errorCode) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public FeaturedCommunicationNotFoundException(String message) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }
}
