/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultUserService.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.service.user;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.brighttalk.service.channel.business.service.channel.ChannelManagerService;
import com.brighttalk.service.channel.business.service.channel.DefaultChannelManagerFinder;

/**
 * The User Service Business API.
 */
@Service
@Primary
public class DefaultUserService implements UserService {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(DefaultChannelManagerFinder.class);

  @Autowired
  private ChannelManagerService channelManagerService;

  /** {@inheritDoc} */
  @Override
  public void delete(final Long userId) {

    LOGGER.debug("Deleting user [" + userId + "].");

    deleteFromChannelManagers(userId);

    LOGGER.debug("User [" + userId + "] deleted.");
  }

  private void deleteFromChannelManagers(final Long userId) {

    channelManagerService.delete(userId);
  }

}