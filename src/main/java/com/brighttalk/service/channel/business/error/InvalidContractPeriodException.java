/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: InvalidContractPeriodException.java 100999 2015-10-05 13:53:18Z kali $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that provided ContractPeriod is invalid. The exception will be thrown by presentation layer validator.
 */
@SuppressWarnings("serial")
public class InvalidContractPeriodException extends ApplicationException {
  /**
   * @param message see below.
   * @param errorCode see below.
   *
   * @see ApplicationException#ApplicationException(String, String)
   */
  public InvalidContractPeriodException(String message, String errorCode) {
    super(message, errorCode);
    this.setUserErrorMessage(message);
    this.setErrorCode(errorCode);
  }

}