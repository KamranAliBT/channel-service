/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionError.java 92672 2015-04-01 08:27:25Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

/**
 * Represents a channel subscription in error.
 */
public class SubscriptionError {

  /** Subscription error code. */
  public enum SubscriptionErrorCode {

    /** Indicates the user is already subscribed to the channel. */
    ALREADY_SUBSCRIBED("AlreadySubscribed"),

    /** Indicates the communication does not identify an existing communication. */
    COMMUNICATION_NOT_FOUND("CommunicationNotFound"),

    /** The subscription context lead engagement score is invalid. */
    INVALID_ENGAGEMENT_SCORE("InvalidEngagementScore"),

    /** The subscription context lead context is invalid. */
    INVALID_LEAD_CONTEXT("InvalidLeadContext"),

    /** Indicates the summit does not identify an existing summit. */
    SUMMIT_NOT_FOUND("SummitNotFound"),

    /** Indicates the subscription does not identify an existing subscription. */
    SUBSCRIPTION_NOT_FOUND("SubscriptionNotFound"),

    /** Indicates the subscription is inactive. */
    SUBSCRIPTION_INACTIVE("SubscriptionInactive"),

    /** Indicates the subscription does not identify an existing subscription. */
    CONTEXT_ALREADY_EXIST("ContextAlreadyExist"),

    /** Indicates the user has unsubscribed from the channel. */
    UNSUBSCRIBED("Unsubscribed"),

    /** Indicates the channel owner has blocked the user from accessing the channel. */
    USER_BLOCKED("UserBlocked"),

    /** Indicates the user is not registered at BrightTALK. */
    USER_NOT_FOUND("UserNotFound");

    private final String code;

    private SubscriptionErrorCode(String code) {
      this.code = code;
    }

    public String getCode() {
      return code;
    }

  }

  private String message;

  private Subscription subscription;

  private List<SubscriptionErrorCode> errorCodes;

  /**
   * @return List of subscription error codes.
   */
  public List<SubscriptionErrorCode> getErrorCodes() {
    return errorCodes;
  }

  /**
   * @param errorCodes List of subscription error codes to set.
   */
  public void setErrorCodes(List<SubscriptionErrorCode> errorCodes) {
    this.errorCodes = errorCodes;
  }

  /**
   * Checks if the configured list of error codes set, if not initialise to empty list and add the supplied error code.
   * 
   * @param errorCode The subscription error code to add.
   */
  public void addErrorCode(final SubscriptionErrorCode errorCode) {
    if (this.errorCodes == null) {
      this.errorCodes = new ArrayList<>();
    }

    if (errorCode != null) {
      this.errorCodes.add(errorCode);
    }
  }

  /**
   * Checks if the configured list of error codes set, if not initialise to empty list and append the supplied list of
   * error codes.
   * 
   * @param errorCodes List of subscription error code to add.
   */
  public void addErrorCodes(final List<SubscriptionErrorCode> errorCodes) {
    if (this.errorCodes == null) {
      this.errorCodes = new ArrayList<>();
    }

    if (!CollectionUtils.isEmpty(errorCodes)) {
      this.errorCodes.addAll(errorCodes);
    }
  }

  /**
   * @return The error message.
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message The error message to set.
   */
  public void setMessage(final String message) {
    this.message = message;
  }

  /**
   * @return The subscription error configured subscription.
   */
  public Subscription getSubscription() {
    return subscription;
  }

  /**
   * @param subscription The subscription to set.
   */
  public void setSubscription(final Subscription subscription) {
    this.subscription = subscription;
  }

}
