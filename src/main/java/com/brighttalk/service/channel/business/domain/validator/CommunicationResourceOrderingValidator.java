/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationResourceOrderingValidator.java 74757 2014-02-27 11:18:36Z ikerbib $ 
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Specialization of {@link CommunicationResourceModificationValidator} to extend the standard checking that a communication
 * is in a valid state for resource re-ordering.
 * 
 * Extends the standard validation provided by  {@link CommunicationResourceModificationValidator} 
 * to add in checking that the resources to be re-ordered already exist on the communication. 
 */
public class CommunicationResourceOrderingValidator extends CommunicationResourceModificationValidator {

  /**
   * The resource to be reordered. Used to check against the current resources on the communication.
   */
  private List<Resource> resourcesToBeReordered = new ArrayList<Resource>();

  /**
   * Construct this validator with the resources to be re-ordered.
   * 
   * @param resourcesToBeReordered the resource to be re-ordered
   */
  public CommunicationResourceOrderingValidator(List<Resource> resourcesToBeReordered) {
    this.resourcesToBeReordered = resourcesToBeReordered;
  }

  /**
   * Validate the communication with the resources to be re-ordered.
   * 
   * Checks the normal business rules around communication state for resource modification
   * then also checks that the resources to be re-ordered already exist on the communication
   * 
   * @param communication to be validated.
   */
  public void validate(Communication communication) {
    
    super.validate(communication);//execute the standard biz rules validation
    validateOrder(communication);//execute the specific validation around resource order. 
    
  }

  /**
   * Validate that the resources to be reordered match the resources all ready existing on the communication. If the
   * communication contains extra resources or the resources to be reordered contain extra resources then an exception
   * is thrown as both should always match when we are reordering.
   * 
   * @throws ApplicationException when the resources to be reordered don't match the existing resources on the
   * communication.
   */
  private void validateOrder(Communication communication) throws ApplicationException {

    // get both lists (resources to be reordered and existing resources on the communication)
    // create a temp copy (we want to maintain order in the original lists)
    // sort the temp copies so the are both in the same order
    // then check both lists are equal. They should be. If they are not, throw an exception.
    List<Resource> tempResourcesToBeReordered = new ArrayList<Resource>(this.resourcesToBeReordered);
    List<Resource> tempExistingCommunicationResources = new ArrayList<Resource>(communication.getResources());

    Collections.sort(tempExistingCommunicationResources);
    Collections.sort(tempResourcesToBeReordered);

    if (!tempExistingCommunicationResources.equals(tempResourcesToBeReordered)) {
      throw new ApplicationException("Resource to be reorderd do not match with communication resources.",
          "InvalidList");
    }
  }

}
