/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelFeedService.java 75381 2014-03-18 15:16:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.PaginatedChannelFeed;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationUrlGenerator;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.ChannelFeedDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationOverrideDbDao;
import com.brighttalk.service.channel.integration.database.statistics.CommunicationStatisticsDbDao;
import com.brighttalk.service.channel.presentation.util.ChannelFeedSearchCriteriaBuilder;

/**
 * Default implementation of the {@link ChannelFeedService}.
 */
@Service
@Primary
public class DefaultChannelFeedService implements ChannelFeedService {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(DefaultChannelFeedService.class);

  /** DB DAO for communications */
  @Autowired
  private ChannelFeedDbDao channelFeedDao;

  /** DB DAO for communication categories */
  @Autowired
  private CategoryDbDao categoryDao;

  /** DB DAO for communication statistics */
  @Autowired
  private CommunicationStatisticsDbDao communicationStatisticsDao;

  /** DB DAO for communication overrides */
  @Autowired
  private CommunicationOverrideDbDao communicationOverrideDao;

  @Value("#{calendarUrlGenerator}")
  private CommunicationUrlGenerator calendarUrlGenerator;

  @Value("#{communicationPortalUrlGenerator}")
  private CommunicationUrlGenerator communicationPortalUrlGenerator;

  /** {@inheritDoc} */
  @Override
  public PaginatedChannelFeed getFeed(final Channel channel, final ChannelFeedSearchCriteria searchCriteria) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Getting a feed for channel [" + channel.getId() + "] with search criteria [" + searchCriteria
          + "].");
    }

    // Create a copy as the criteria may be modified in this method
    ChannelFeedSearchCriteria searchCriteriaCopy = new ChannelFeedSearchCriteriaBuilder().build(searchCriteria);

    Long channelId = channel.getId();

    int totalNumberOfCommunicationsInFeed = channelFeedDao.findTotalCommunicationsInFeed(channelId, searchCriteriaCopy);

    if (totalNumberOfCommunicationsInFeed == 0) {
      return new PaginatedChannelFeed(channel, Collections.<Communication>emptyList(),
          totalNumberOfCommunicationsInFeed, searchCriteriaCopy);
    }

    Date featuredDateTime = channelFeedDao.findFeaturedDateTime(channelId, searchCriteriaCopy);

    if (searchCriteriaCopy.isClosestToNowFilter()) {

      int pageNumber = getClosestToNowPageNumber(channelId, featuredDateTime, searchCriteriaCopy);

      searchCriteriaCopy.setPageNumber(pageNumber);
    }

    List<Communication> communications = channelFeedDao.findFeed(channelId, searchCriteriaCopy);

    PaginatedChannelFeed feed = new PaginatedChannelFeed(channel, communications, totalNumberOfCommunicationsInFeed,
        searchCriteriaCopy);

    communicationOverrideDao.loadOverrides(channelId, feed.getAllCommunications());
    categoryDao.loadCategories(channelId, feed.getAllCommunications());

    communicationStatisticsDao.loadStatistics(channelId, feed.getCommunicationsNotSyndicatedOut());
    communicationStatisticsDao.loadStatisticsFromAllChannels(feed.getSyndicatedOutCommunications());

    loadCommunicationUrls(channel, feed.getAllCommunications());
    markFeatured(feed.getAllCommunications(), featuredDateTime);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found channel feed [" + feed + "] " + "using search search criteria [" + searchCriteriaCopy + "].");
    }

    return feed;
  }

  /**
   * Get the page number of the feed containing the closest to now communication.
   * 
   * @param channelId the id of the channel to get the feed for (just used for logging)
   * @param closestToNowScheduledDate
   * @param criteria Search criteria to use.
   * @return Number of pages.
   */
  private int getClosestToNowPageNumber(final Long channelId, final Date closestToNowScheduledDate,
      final ChannelFeedSearchCriteria criteria) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Getting closest to now page number for for channel [" + channelId + "].");
    }

    int numberOfCommunicationsAfterScheduledDate = channelFeedDao.findTotalCommunicationsScheduledAfterDate(channelId,
        closestToNowScheduledDate, criteria);
    int totalNumberOfFuturePages = numberOfCommunicationsAfterScheduledDate / criteria.getPageSize();
    int closestToNowPageNumber = totalNumberOfFuturePages + 1;

    return closestToNowPageNumber;
  }

  private void markFeatured(final List<Communication> communications, final Date featuredDate) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Attempting to set featured communication. " + "featured date is [" + featuredDate + "].");
    }

    for (Communication communication : communications) {

      if (DateUtils.isSameInstant(communication.getScheduledDateTime(), featuredDate)) {
        if (LOGGER.isDebugEnabled()) {
          LOGGER.debug("Found a communication to mark as featured. communication id [" + communication.getId() + "]");
        }
        communication.setFeatured();
      }
    }
  }

  /**
   * Given a list of communications, set all the calendar urls on those communications.
   * 
   * @param communications the list of communications to set the calendar URLs on.
   */
  private void loadCommunicationUrls(final Channel channel, final List<Communication> communications) {

    for (Communication communication : communications) {
      communication.setCalendarUrl(calendarUrlGenerator.generate(channel, communication));

      final String communicationPortalUrl = communicationPortalUrlGenerator.generate(channel, communication);
      communication.setUrl(communicationPortalUrl);
    }
  }
}