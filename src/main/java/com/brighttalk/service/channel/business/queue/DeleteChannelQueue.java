/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DeleteChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Interface for delete channel related operations queues.
 * The operations that need to be done asynchronously include tasks that must be performed by over services.
 * To perform those operations service API has a separate callback method. 
 */
public interface DeleteChannelQueue {

  /**
   * Perform asynchronous operations on a given channel required 
   * when performing a delete channel operation.
   * 
   * @param channelId id of the channel to be deleted
   */
  void deleteChannel( Long channelId );
}