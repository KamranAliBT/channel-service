/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: NotFoundException.java 69660 2013-10-14 11:30:45Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates a resource (e.g. channel or communication) has not been found.
 */
@SuppressWarnings("serial")
public class NotFoundException extends ApplicationException {

  /** Not Found error code */
  public static final String ERROR_CODE_NOT_FOUND_ERROR = "NotFound";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_NOT_FOUND_ERROR}.
   */
  private final String errorCode = ERROR_CODE_NOT_FOUND_ERROR;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public NotFoundException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public NotFoundException(final String message, final String errorCode) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public NotFoundException(final String message) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

}
