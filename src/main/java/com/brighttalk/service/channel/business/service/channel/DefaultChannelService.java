/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelService.java 75381 2014-03-18 15:16:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.business.domain.channel.ChannelUrlGenerator;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.validator.ChannelCreateValidator;
import com.brighttalk.service.channel.business.domain.validator.ChannelUpdateValidator;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.ChannelUpdateAuthorisationException;
import com.brighttalk.service.channel.business.error.ContactDetailsNotFoundException;
import com.brighttalk.service.channel.business.queue.AuditQueue;
import com.brighttalk.service.channel.business.queue.CreateChannelQueue;
import com.brighttalk.service.channel.business.queue.DeleteChannelQueue;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.booking.BookingServiceDao;
import com.brighttalk.service.channel.integration.booking.HDBookingServiceDao;
import com.brighttalk.service.channel.integration.community.CommunityServiceDao;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.database.ChannelTypeDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.ContactDetailsDbDao;
import com.brighttalk.service.channel.integration.database.SubscriptionDbDao;
import com.brighttalk.service.channel.integration.email.EmailServiceDao;
import com.brighttalk.service.channel.integration.enquiry.EnquiryServiceDao;

/**
 * Default implementation of the {@link ChannelService}.
 */
@Service
@Primary
public class DefaultChannelService implements ChannelService {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(DefaultChannelService.class);

  /** Channel finder service */
  @Autowired
  private ChannelFinder channelFinder;

  /** Channel database DAO */
  @Autowired
  private ChannelDbDao channelDbDao;

  @Autowired
  private ChannelTypeDbDao channelTypeDbDao;

  @Autowired
  private SubscriptionDbDao subscriptionDbDao;

  @Autowired
  private CommunicationDbDao communicationDbDao;

  @Autowired
  private CategoryDbDao categoryDbDao;

  @Autowired
  private ContactDetailsDbDao contactDetailsDbDao;

  @Autowired
  private DeleteChannelQueue deleteChannelQueue;

  @Autowired
  private CreateChannelQueue createChannelQueue;

  @Autowired
  private CommunityServiceDao communityServiceDao;

  @Autowired
  private BookingServiceDao bookingServiceDao;

  @Autowired
  private HDBookingServiceDao hdBookingServiceDao;

  @Autowired
  private EmailServiceDao emailServiceDao;

  @Autowired
  private EnquiryServiceDao enquiryServiceDao;

  @Autowired
  private ChannelUpdateValidator channelUpdateValidator;

  @Autowired
  private AuditQueue auditQueue;

  @Autowired
  @Qualifier("channelAuditRecordDtoBuilder")
  private AuditRecordDtoBuilder channelAuditRecordDtoBuilder;

  @Autowired
  @Qualifier("contactDetailsAuditRecordDtoBuilder")
  private AuditRecordDtoBuilder contactDetailsAuditRecordDtoBuilder;

  @Value("#{channelPortalUrlGenerator}")
  private ChannelUrlGenerator channelUrlGenerator;

  @Value("#{channelAtomFeedUrlGenerator}")
  private ChannelUrlGenerator channelAtomFeedUrlGenerator;

  @Value("#{channelRssFeedUrlGenerator}")
  private ChannelUrlGenerator channelRssFeedUrlGenerator;

  /** {@inheritDoc} */
  @Override
  public Channel create(final User user, Channel channel, final ContactDetails contactDetails) {

    channel = create(user, channel);
    contactDetailsDbDao.create(channel.getId(), contactDetails);

    return channel;
  }

  /** {@inheritDoc} */
  @Override
  public Channel create(final User user, final Channel channel) {

    channel.validate(new ChannelCreateValidator(channelDbDao));

    final String requestChannelTypeName = channel.getCreatedType().getName();

    ChannelType channelType = channelTypeDbDao.find(requestChannelTypeName);
    channel.setPendingType(channelType);
    channel.setCreatedType(channelType);

    ChannelType defaultType = channelTypeDbDao.findDefault();
    channel.setType(defaultType);

    channel.setOwnerUserId(user.getId());
    Channel createdChannel = channelDbDao.create(channel);

    queueCreateChannelMessage(createdChannel.getId());

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Channel [" + channel.getId() + "] created by user [" + user.getId() + "].");
    }

    return createdChannel;
  }

  /**
   * Queue a create channel JMS message.
   * 
   * @param channelId id of the channel to be created.
   */
  private void queueCreateChannelMessage(final Long channelId) {

    if (LOGGER.isDebugEnabled()) {

      LOGGER.debug("Placing channel [" + channelId + "] onto a create queue to perform asynchronous processing");
    }
    createChannelQueue.createChannel(channelId);
  }

  /** {@inheritDoc} */
  @Override
  public void createCallback(final Long channelId) {

    Channel channel = channelFinder.find(channelId);

    emailServiceDao.sendChannelCreateConfirmation(channel);
    enquiryServiceDao.sendCreateChannelRequest(channel);
  }

  /** {@inheritDoc} */
  @Override
  public Channel update(final User user, final Channel channel) {

    LOGGER.debug("Updating channel [" + channel.getId() + "].");

    Channel updateChannel = channelFinder.find(channel.getId());
    updateChannel.merge(channel);
    updateChannel.validate(channelUpdateValidator);

    authoriseForUpdate(user, channel, updateChannel);

    Channel updatedChannel = channelDbDao.update(updateChannel);

    // Audit the channel update
    auditChannelUpdate(user, updatedChannel);

    return updatedChannel;
  }

  /**
   * Audit updated channel.
   * 
   * @param user User
   * @param channel Updated channel
   */
  private void auditChannelUpdate(final User user, final Channel channel) {
    AuditRecordDto auditRecordDto = channelAuditRecordDtoBuilder.update(user.getId(), channel.getId(), null, null);
    auditQueue.create(auditRecordDto);
  }

  /**
   * Audit contact details update.
   * 
   * @param user User requesting the update.
   * @param channel Channel's contact details being updated.
   */
  private void auditContactDetailsUpdate(final User user, final Channel channel) {

    AuditRecordDto auditRecordDto =
        contactDetailsAuditRecordDtoBuilder.update(user.getId(), channel.getId(), null, null);
    auditQueue.create(auditRecordDto);
  }

  /**
   * Authorise a user to perform an update on the channel. The user must be a BrightTALK manager to update the channel
   * owner.
   * 
   * @param user User to authorise
   * @param channel Channel details to update
   * @param loadedChannel Merged channel with details to update
   */
  private void authoriseForUpdate(final User user, final Channel channel, final Channel loadedChannel) {
    if (channel.hasOwnerUserId() && !user.isManager()) {
      throw new ChannelUpdateAuthorisationException(user.getId());
    } else {
      loadedChannel.isAuthorised(user);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void delete(final User user, final Long channelId) {

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Received request to delete channel [" + channelId + "] from user [" + user.getId() + "].");
    }

    authoriseForDelete(user, channelId);
    assertChannelExists(channelId);

    channelDbDao.delete(channelId);
    queueDeleteChannelMessage(channelId);

    // Audit the deletion of a channel request
    AuditRecordDto auditRecordDto = getDeleteChannelAuditRecordDto(user.getId(), channelId);
    auditQueue.create(auditRecordDto);

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Channel [" + channelId + "] deleted by user [" + user.getId() + "].");
    }
  }

  /**
   * Queue a delete channel JMS message.
   * 
   * @param channelId id of the channel to be deleted.
   */
  private void queueDeleteChannelMessage(final Long channelId) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Placing channel [" + channelId + "] onto a delete queue. Required processing will "
          + "be done asynchronously.");
    }
    deleteChannelQueue.deleteChannel(channelId);
  }

  /**
   * Assert the given channel exists.
   * 
   * @param channelId the id of the channel to assert.
   * 
   * @throws ChannelNotFoundException if the channel has not been found.
   */
  private void assertChannelExists(final Long channelId) {
    if (!channelDbDao.exists(channelId)) {
      throw new ChannelNotFoundException(channelId);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void deleteCallback(final Long channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Asynchronous processing of delete channel [" + channelId + "] request started.");
    }

    deleteSubscribers(channelId);

    List<Communication> communicationsToBeDeleted = getCommunicationsToBeDeleted(channelId);
    List<Long> communicationIds = getCommunicationIds(communicationsToBeDeleted);

    deleteCommunications(channelId, communicationIds);
    deleteCommunities(channelId, communicationIds);
    deleteBookings(channelId, communicationsToBeDeleted);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Asynchronous processing of delete channel [" + channelId + "] request complete.");
    }
  }

  /** {@inheritDoc} */
  @Override
  public void unsubscribeUserFromChannel(final User user, final Long channelId) {

    subscriptionDbDao.unsubscribeUser(channelId, user.getId());
  }

  /** {@inheritDoc} */
  @Override
  public void unsubscribeUserFromChannels(final User user) {

    subscriptionDbDao.unsubscribeUser(user.getId());
  }

  /**
   * Delete subscribers for the given channel. Typically used in association with deleting a channel.
   * 
   * @param channelId of the channel
   */
  private void deleteSubscribers(final Long channelId) {

    try {
      subscriptionDbDao.unsubscribeUsers(channelId);
    } catch (Exception exception) {
      LOGGER.error("Delete subscribers for channel [" + channelId + "] failed.", exception);
    }
  }

  /**
   * Load all the communications that must be deleted with the given channel. This means all the communications that
   * originated in this channel (aka master communications).
   * 
   * @param channelId the id of the channel
   * 
   * @return collection of communications
   */
  private List<Communication> getCommunicationsToBeDeleted(final Long channelId) {

    List<Communication> communications = null;

    try {
      communications = communicationDbDao.findAll(channelId);
    } catch (Exception exception) {
      LOGGER.error("Finding communications for channel [" + channelId + "] failed.", exception);
      communications = new ArrayList<Communication>();
    }

    return communications;
  }

  /**
   * Delete the given communications. Typically used in association with deleting a channel.
   * 
   * @param channelId the id of the channel
   * @param communicationIds id's of the communications to be deleted.
   */
  private void deleteCommunications(final Long channelId, final List<Long> communicationIds) {

    try {
      communicationDbDao.delete(communicationIds);
    } catch (Exception exception) {
      LOGGER.error("Deleting one or more of [" + communicationIds.size() + "] communications " + "for channel ["
          + channelId + "] failed", exception);
    }

  }

  /**
   * Deleting all the communities assigned to a channel and its communication. Typically used in association with
   * deleting a channel.
   * 
   * @param channelId the id of the channel
   * @param communicationIds list of communications
   */
  private void deleteCommunities(final Long channelId, final List<Long> communicationIds) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleting communities for channel [" + channelId + "].");
    }

    try {
      communityServiceDao.deleteChannel(channelId);
      communityServiceDao.deleteCommunications(channelId, communicationIds);
    } catch (Exception exception) {
      LOGGER.error("Deleting communities for channel [" + channelId + "] failed", exception);
    }
  }

  /**
   * Delete all the channel associated bookings for brighttalk webcasts. Typically used in association with deleting a
   * channel.
   * 
   * @param channelId the id of the channel
   * @param communications list of communications that are being deleted.
   */
  private void deleteBookings(final Long channelId, final List<Communication> communications) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleting bookings for channel [" + channelId + "].");
    }

    List<Long> brighttalkCommunicationIds = new ArrayList<Long>();
    for (Communication communication : communications) {
      if (communication.isBrightTALK()) {
        brighttalkCommunicationIds.add(communication.getId());
      }
    }

    deleteBrightTALKBookings(channelId, brighttalkCommunicationIds);
    deleteBrightTALKHDBookings(channelId);

  }

  /**
   * Delete all the brighttalk (audio and slides) webcasts from the booking service.
   * 
   * @param channelId of the channel to delete the bookings from
   * @param communicationsIds list of brighttalk webcasts ids to be deleted.
   */
  private void deleteBrightTALKBookings(final Long channelId, final List<Long> communicationsIds) {
    try {
      bookingServiceDao.deleteBookings(communicationsIds);
    } catch (Exception exception) {
      LOGGER.error("Deleting bookings for channel [" + channelId + "] failed.", exception);
    }

  }

  /**
   * Delete all the brighttalk HD webcasts from the HD booking service.
   * 
   * @param channelId of the channel to delete the bookings from
   */
  private void deleteBrightTALKHDBookings(final Long channelId) {
    try {
      hdBookingServiceDao.deleteBookings(channelId);
    } catch (Exception exception) {
      LOGGER.error("Deleting HD bookings for channel [" + channelId + "] failed.", exception);
    }

  }

  /**
   * Authorise the given user to delete the given channel.
   * 
   * @param user
   * @param channelId
   */
  private void authoriseForDelete(final User user, final Long channelId) {

    if (!user.isManager()) {
      throw new UserAuthorisationException("User [" + user.getId() + "] is not authorised to delete channel ["
          + channelId + "].");
    }
  }

  /**
   * Given a list of communications, return the corresponding list of communication ids.
   * 
   * @param communications the communications to get the ids from
   * 
   * @return the communication id's
   */
  private List<Long> getCommunicationIds(final List<Communication> communications) {
    List<Long> communicationIds = new ArrayList<Long>();
    for (Communication communication : communications) {
      communicationIds.add(communication.getId());
    }
    return communicationIds;
  }

  /**
   * Creates an instance of the {@link AuditRecordDto} for the channel deletion event.
   * 
   * @param user ID of user that requested the deletion of the channel.
   * @param channel ID of channel to be deleted.
   * 
   * @return Instance of {@link AuditRecordDto}
   */
  private AuditRecordDto getDeleteChannelAuditRecordDto(final long userId, final long channelId) {
    return channelAuditRecordDtoBuilder.delete(userId, channelId, null, "User [" + userId
        + "] requested a deletion of channel [" + channelId + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ContactDetails getContactDetails(final User user, final Long channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(String.format("Getting contact details for channel [%s].", channelId));
    }

    Channel channel = channelFinder.find(channelId);
    channel.isAuthorised(user);

    ContactDetails contactDetails = null;

    try {
      contactDetails = contactDetailsDbDao.get(channelId);
    } catch (ContactDetailsNotFoundException e) {
      contactDetails = new ContactDetails();
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(String.format("Returning contact details [%s] for channel [%s].", contactDetails, channelId));
    }

    return contactDetails;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ContactDetails updateContactDetails(final User user, final Long channelId, final ContactDetails contactDetails) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updating contact details for channel [" + channelId + "].");
    }

    Channel channel = channelFinder.find(channelId);
    channel.isAuthorised(user);

    ContactDetails updatedContactDetails = contactDetailsDbDao.update(channelId, contactDetails);

    auditContactDetailsUpdate(user, channel);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updated contact details for channel [" + channelId + "].");
    }

    return updatedContactDetails;
  }
}