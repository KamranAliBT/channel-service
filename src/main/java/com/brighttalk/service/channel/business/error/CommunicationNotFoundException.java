/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelNotFoundException.java 69660 2013-10-14 11:30:45Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

/**
 * Indicates the communication has not been found.
 */
@SuppressWarnings("serial")
public class CommunicationNotFoundException extends NotFoundException {

  private final Long communicationId;

  /**
   * @param communicationId see below.
   * 
   * @see CommunicationNotFoundException#CommunicationNotFoundException(Long, Throwable)
   */
  public CommunicationNotFoundException(final Long communicationId) {
    this(communicationId, null);
  }

  /**
   * @param communicationId see below.
   * @param cause see below.
   * 
   * @see NotFoundException#NotFoundException(String, Throwable)
   */
  public CommunicationNotFoundException(final Long communicationId, final Throwable cause) {
    super("Communication [" + communicationId + "] not found.", cause);
    this.communicationId = communicationId;
  }

  /**
   * throw communication not found exception.
   * 
   * @param message to set
   * @param cause of the throwing
   */
  public CommunicationNotFoundException(final String message, final Throwable cause) {
    super(message, cause);
    communicationId = null;
  }

  /**
   * throw communication not found exception.
   * 
   * @param message The exception message to set.
   */
  public CommunicationNotFoundException(final String message) {
    super(message);
    communicationId = null;
  }

  public Long getCommunicationId() {
    return communicationId;
  }

}
