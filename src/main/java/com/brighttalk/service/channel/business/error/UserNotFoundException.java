/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: UserNotFoundException.java 69660 2013-10-14 11:30:45Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

/**
 * Indicates the manager has not been found.
 */
@SuppressWarnings("serial")
public class UserNotFoundException extends NotFoundException {

  private Long id;

  private String emailAddress;

  /**
   * @param id see bellow.
   * 
   * @see NotFoundException#NotFoundException(String)
   */
  public UserNotFoundException(final Long id) {
    super("User [" + id + "] not found.");
    this.id = id;
  }

  /**
   * @param emailAddress see bellow.
   * 
   * @see NotFoundException#NotFoundException(String)
   */
  public UserNotFoundException(final String emailAddress) {
    super("User [" + emailAddress + "] not found.");
    this.emailAddress = emailAddress;
  }

  /**
   * @param id see bellow.
   * @param emailAddress see bellow.
   * 
   * @see NotFoundException#NotFoundException(String)
   */
  public UserNotFoundException(final Long id, final String emailAddress) {
    super("User [id: " + id + ", email: " + emailAddress + "] not found.");
    this.id = id;
    this.emailAddress = emailAddress;
  }

  public Long getId() {
    return id;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

}
