/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: BaseSearchCriteria.java 59945 2013-01-29 12:13:58Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import org.apache.commons.lang.StringUtils;

import com.brighttalk.common.error.ApplicationException;

/**
 * Search Criteria for retrieving the paginated list of business objects.
 * <p>
 * Contains the page number being requested, the size of the page being requested and any filter criteria for the feed.
 * <p>
 * Flags on how the channels should be sorted are part of this class, too. The class can be extended to include object
 * specific search values, e.g. sort by enums that will be different for channels and commmunications.
 */
public class BaseSearchCriteria {

  private Integer pageNumber;

  private Integer pageSize;

  private SortOrder sortOrder;

  /**
   * Sort Order types.
   */
  public static enum SortOrder {

    /**
     * Ascending.
     */
    ASC,

    /**
     * Descending.
     */
    DESC;

    /**
     * Converts a string into SortOrder.
     * 
     * If the value is empty - a default value, DESC, is returned.
     * 
     * @param value to be converted
     * 
     * @return SortOrder
     * 
     * @throws ApplicationException if the value is invalid
     */
    public static SortOrder convert(String value) {

      if (StringUtils.isEmpty(value)) {

        return DESC;
      }

      try {

        return SortOrder.valueOf(value.trim().toUpperCase());

      } catch (IllegalArgumentException exception) {

        throw new ApplicationException("Invalid sort order value requested [" + value + "]", exception, "ClientError");
      }
    }
  }

  public Integer getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(Integer value) {
    pageNumber = value;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer value) {
    pageSize = value;
  }

  public SortOrder getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(SortOrder value) {
    sortOrder = value;
  }

  /**
   * Indicates if this base search criteria has page size.
   * 
   * @return true or false
   */
  public boolean hasPageSize() {
    return this.pageSize != null;
  }
  
  /**
   * Indicates if this base search criteria sort order is ascending.
   * 
   * @return true or false
   */
  public boolean isSortOrderAsc() {
    return BaseSearchCriteria.SortOrder.ASC.equals(this.getSortOrder());
  }

  /**
   * Indicates if this base search criteria sort order is descending.
   * 
   * @return true or false
   */
  public boolean isSortOrderDesc() {
    return BaseSearchCriteria.SortOrder.DESC.equals(this.getSortOrder());
  }
}