/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsCreateChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.brighttalk.service.channel.business.domain.Provider;

/**
 * Jms implementation of a Email Queuer.
 * 
 * 
 * The webcast will then be placed onto a JMS queue where some other listener will pick them up and do the actual
 * processing.
 */
public class JmsEmailQueuer implements EmailQueuer {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JmsEmailQueuer.class);

  /**
   * Create webcast destination name.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.emailCreateWebcastDestination}")
  private String createDestinationName;

  /**
   * Reschedule webcast destination name.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.emailRescheduleWebcastDestination}")
  private String rescheduleDestinationName;

  /**
   * Cancel webcast destination name.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.emailCancelWebcastDestination}")
  private String cancelDestinationName;

  /**
   * Missed you webcast destination name.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.emailMissedYouWebcastDestination}")
  private String missedYouDestinationName;

  /**
   * Recording published webcast destination name.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.emailRecordingPublishedWebcastDestination}")
  private String recordingPublishedDestinationName;

  /**
   * Video Upload Complete destination name.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.emailVideoUploadCompleteDestination}")
  private String videoUploadCompleteDestinationName;

  /**
   * Video Upload Error destination name.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.emailVideoUploadErrorDestination}")
  private String videoUploadErrorDestinationName;

  /**
   * Video Upload Complete destination name for HD webcasts.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.emailVideoUploadHDCompleteDestination}")
  private String videoUploadHDCompleteDestinationName;

  /**
   * Video Upload Error destination name for HD webcasts.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.emailVideoUploadHDErrorDestination}")
  private String videoUploadHDErrorDestinationName;

  /**
   * Helper class acting in the role of the JMS {@link javax.jms.MessageProducer} Handles creation and release of JMS
   * resources (Connections, Session) on the sending of the JMS message.
   */
  @Qualifier(value = "generic")
  @Autowired
  private JmsTemplate jmsTemplate;

  /**
   * {@inheritDoc}
   */
  @Override
  public void create(final Long webcastId) {
    LOGGER.info("Queuing email message for creating communication [" + webcastId + "].");

    MessageCreator msgCreator = createMessage(webcastId);

    jmsTemplate.send(createDestinationName, msgCreator);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void reschedule(final Long webcastId) {
    LOGGER.info("Queuing email message for rescheduling communication [" + webcastId + "].");

    MessageCreator msgCreator = createMessage(webcastId);

    jmsTemplate.send(rescheduleDestinationName, msgCreator);
  }

  /** {@inheritDoc} */
  @Override
  public void cancel(final Long webcastId) {
    LOGGER.info("Queuing email message for cancelling communication [" + webcastId + "].");

    MessageCreator msgCreator = createMessage(webcastId);

    jmsTemplate.send(cancelDestinationName, msgCreator);
  }

  /** {@inheritDoc} */
  @Override
  public void missedYou(final Long webcastId) {
    LOGGER.info("Queuing email message for missed you communication [" + webcastId + "].");

    MessageCreator msgCreator = createMessage(webcastId);

    jmsTemplate.send(missedYouDestinationName, msgCreator);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void recordingPublished(final Long webcastId) {
    LOGGER.info("Queuing email message for recording published communication [" + webcastId + "].");

    MessageCreator msgCreator = createMessage(webcastId);

    jmsTemplate.send(recordingPublishedDestinationName, msgCreator);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void videoUploadComplete(final Long webcastId, final String provider) {
    LOGGER.info("Queuing email message for video upload complete for communication [" + webcastId + "] with provider ["
        + provider + "].");

    MessageCreator msgCreator = createMessage(webcastId);

    if (provider.equals(Provider.BRIGHTTALK_HD)) {
      jmsTemplate.send(videoUploadHDCompleteDestinationName, msgCreator);
    } else {
      jmsTemplate.send(videoUploadCompleteDestinationName, msgCreator);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void videoUploadError(final Long webcastId, final String provider) {
    LOGGER.info("Queuing email message for video upload failed for communication [" + webcastId + "] with provider ["
        + provider + "].");

    MessageCreator msgCreator = createMessage(webcastId);

    if (provider.equals(Provider.BRIGHTTALK_HD)) {
      jmsTemplate.send(videoUploadHDErrorDestinationName, msgCreator);
    } else {
      jmsTemplate.send(videoUploadErrorDestinationName, msgCreator);
    }
  }

  private MessageCreator createMessage(final Long webcastId) {
    MessageCreator msgCreator = new MessageCreator() {

      /**
       * {@inheritDoc}
       * <p>
       * Anonymous implementation of Spring JmsTemplate's simplest callback interface - {@link MessageCreator}. Creates
       * a JMS message that is used to asynchronously request the sending of the message.
       * <p>
       * The message body contains a serializable asset to be processed. This is implemented as a JMS
       * {@link ObjectMessage} since the messages are consumed locally/internally.
       */
      @Override
      public Message createMessage(final Session session) throws JMSException {
        ObjectMessage msg = session.createObjectMessage();
        msg.setObject(webcastId);
        return msg;
      }
    };
    return msgCreator;
  }

}