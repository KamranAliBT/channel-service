/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: NotAllowedAtThisTimeException.java 69660 2013-10-14 11:30:45Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates an operation (e.g. adding a resource) is not allowed at this time, typically because a communication is the
 * wrong state (e.g. its live and things can't be edited when its live).
 * 
 */
@SuppressWarnings("serial")
public class NotAllowedAtThisTimeException extends ApplicationException {

  private static final String ERROR_CODE_NOT_ALLOWED_AT_THIS_TIME_ERROR = "NotAllowedAtThisTime";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_NOT_ALLOWED_AT_THIS_TIME_ERROR}.
   */
  private final String errorCode = ERROR_CODE_NOT_ALLOWED_AT_THIS_TIME_ERROR;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public NotAllowedAtThisTimeException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public NotAllowedAtThisTimeException(final String message, final String errorCode) {
    super(message, ERROR_CODE_NOT_ALLOWED_AT_THIS_TIME_ERROR);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public NotAllowedAtThisTimeException(final String message) {
    super(message, ERROR_CODE_NOT_ALLOWED_AT_THIS_TIME_ERROR);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

}
