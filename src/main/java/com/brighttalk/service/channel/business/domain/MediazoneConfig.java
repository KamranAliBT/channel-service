/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: Rendition.java 85869 2014-11-11 11:17:11Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import java.util.Date;

/**
 * Represents a Mediazone configuration object.
 */
public class MediazoneConfig {

  /** static property to define the type of mediazone data parsed. Type config */
  public static final String MEDIAZONE_CONFIG = "config";
  /** static property to define the type of mediazone data parsed. Type webcast */
  public static final String MEDIAZONE_WEBCAST = "webcast";
  /** static property to define the type of a mediazone communication. medizone config */
  public static final String ASSET_MEDIAZONE_CONFIG = "mediazone_config";
  /** static property to define the type of a mediazone communication. medizone webcast */
  public static final String ASSET_MEDIAZONE_WEBCAST = "mediazone_webcast";

  /** the communication id of the webcast. */
  private Long id;
  private String targetMediazoneConfigUrl;
  private String targetMediazoneId;
  private Boolean isActive;
  private Date created;
  private Date lastUpdated;

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * @return the targetMediazoneConfigUrl
   */
  public String getTargetMediazoneConfigUrl() {
    return targetMediazoneConfigUrl;
  }

  /**
   * @param targetMediazoneConfigUrl the targetMediazoneConfigUrl to set
   */
  public void setTargetMediazoneConfigUrl(final String targetMediazoneConfigUrl) {
    this.targetMediazoneConfigUrl = targetMediazoneConfigUrl;
  }

  /**
   * @return the targetMediazoneId
   */
  public String getTargetMediazoneId() {
    return targetMediazoneId;
  }

  /**
   * @param targetMediazoneId the targetMediazoneId to set
   */
  public void setTargetMediazoneId(final String targetMediazoneId) {
    this.targetMediazoneId = targetMediazoneId;
  }

  /**
   * @return the isActive
   */
  public Boolean getIsActive() {
    return isActive;
  }

  /**
   * @param isActive the isActive to set
   */
  public void setIsActive(final Boolean isActive) {
    this.isActive = isActive;
  }

  /**
   * @return the created
   */
  public Date getCreated() {
    return created;
  }

  /**
   * @param created the created to set
   */
  public void setCreated(final Date created) {
    this.created = created;
  }

  /**
   * @return the lastUpdated
   */
  public Date getLastUpdated() {
    return lastUpdated;
  }

  /**
   * @param lastUpdated the lastUpdated to set
   */
  public void setLastUpdated(final Date lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  /**
   * @return the mediazoneConfig
   */
  public static String getMediazoneConfig() {
    return MEDIAZONE_CONFIG;
  }

  /**
   * @return the mediazoneWebcast
   */
  public static String getMediazoneWebcast() {
    return MEDIAZONE_WEBCAST;
  }

  @Override
  public String toString() {
    StringBuilder toString = new StringBuilder();

    toString.append("communication id: [" + id + "]");
    toString.append("targetMediazoneConfigUrl: [" + targetMediazoneConfigUrl + "]");
    toString.append("targetMediazoneId: [" + targetMediazoneId + "]");
    toString.append("is active: [" + isActive + "]");
    toString.append("created: [" + created + "]");
    toString.append("last_updated: [" + lastUpdated + "]");

    return toString.toString();
  }

}
