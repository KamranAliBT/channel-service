/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: Predicate.java 87022 2014-12-03 13:00:40Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.filter;

/**
 * Helper interface to filter a collection.
 * 
 * @param <T> the type to apply the comparison
 */
public interface Predicate<T> {

  /**
   * Interface method to apply the comparison.
   * 
   * @param type the type to apply the comparison
   * 
   * @return true if the comparison maches
   */
  boolean apply(T type);
}
