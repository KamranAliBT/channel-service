/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodDeleteValidator.java 101445 2015-10-13 11:49:51Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.InvalidContractPeriodException;
import org.joda.time.DateTime;

/**
 * Validates a {@link ContractPeriod} for deletion.
 *
 * A Contract Period cannot be deleted if it is currently active (if today's date lies between
 * {@link ContractPeriod#getStart()} and {@link ContractPeriod#getEnd()} inclusive), or if it's end date is in the past.
 */
public class ContractPeriodDeleteValidator implements Validator<ContractPeriod> {

  private static final String CONTRACT_PERIOD_INELIGIBLE_FOR_DELETION = "ContractPeriodIneligibleForDeletion";

  @Override
  public void validate(ContractPeriod contractPeriod) {
    //This contract period's dates
    DateTime endDate = new DateTime(contractPeriod.getEnd().getTime());
    DateTime startDate = new DateTime(contractPeriod.getStart().getTime());

    //is this contract period current?
    DateTime rightNow = DateTime.now();
    if (startDate.isBefore(rightNow) && endDate.isAfter(rightNow)) {
      throw new InvalidContractPeriodException("A current contract period cannot be deleted.",
          CONTRACT_PERIOD_INELIGIBLE_FOR_DELETION);
    }

    //do not allow a contract period to be deleted if it is active today or any time before today
    DateTime midnightTonight = new DateTime().plusDays(1).withTimeAtStartOfDay();
    boolean contractEndsBeforeNow = endDate.isBefore(midnightTonight);
    if (contractEndsBeforeNow) {
      throw new InvalidContractPeriodException("Contract period in the past cannot be deleted.",
          CONTRACT_PERIOD_INELIGIBLE_FOR_DELETION);
    }
  }
}
