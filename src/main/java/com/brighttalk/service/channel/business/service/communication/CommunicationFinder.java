/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationFinder.java 85869 2014-11-11 11:17:11Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.Map;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria;

/**
 * The Communication Finder Business API.
 * <p>
 * Responsible for finding {@link Communication communications}.
 * <p>
 */
public interface CommunicationFinder {

  /**
   * Finds a communication for the given communicationId. This will find the communication from its master channel, no
   * matter if the communication has also been syndicated.
   * 
   * @param communicationId the communication to find
   * 
   * @return Communication
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when communication cannot be found.
   * 
   * @see #find(Channel channel,Long communicationId) to find a communication which has been syndicated into other
   * channels and if you need to find the syndicated version.
   */
  Communication find(Long communicationId);

  /**
   * Finds a communication for the given channel and communicationId. This will find communications that may or may not
   * be syndicated.
   * 
   * @param channel the channel object containing the channel id
   * @param communicationId the communication to find the resource on
   * 
   * @return Communication
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when communication cannot be found.
   * 
   * @see #find(Long communicationId) to find a communication without a channel id (which will return the communication
   * from the master channel.
   */
  Communication find(Channel channel, Long communicationId);

  /**
   * Finds a conference room url for the given pin number and provider.
   * 
   * @param pin the communication pin number
   * @param provider the communication provider. Optional. If null just the pin number is used.
   * 
   * @return The conference room url
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when communication cannot be found.
   * @throws com.brighttalk.service.channel.business.error.DialledInTooEarlyException If the presenter has dialled in
   * too early
   * @throws com.brighttalk.service.channel.business.error.DialledInTooLateException If the presenter has dialled in too
   * late
   */
  String find(String pin, String provider);

  /**
   * Find a paginated collection of public communications that match the search criteria.
   * 
   * @param criteria the search criteria
   * 
   * @return paginated collection of communications
   */
  PaginatedList<Communication> find(CommunicationsSearchCriteria criteria);

  /**
   * Find a paginated collection of communications that exist in channels user is subscribed to.
   * 
   * @param user the user
   * @param searchCriteria the search criteria
   * 
   * @return paginated collection of communications
   */
  PaginatedList<Communication> findSubscribedChannelsCommunications(User user,
    MyCommunicationsSearchCriteria searchCriteria);

  /**
   * Find communications totals from the channels user is subscribed to.
   * 
   * @param userId the User id.
   * 
   * @return communications totals map.
   */
  Map<String, Long> findSubscribedChannelsCommunicationsTotals(Long userId);

}