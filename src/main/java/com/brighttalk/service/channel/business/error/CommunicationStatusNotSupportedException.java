/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationStatusNotSupportedException.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that the requested communication cannot be updated in the current status.
 */
@SuppressWarnings("serial")
public class CommunicationStatusNotSupportedException extends ApplicationException {

  private static final String ERROR_CODE_NOT_FOUND_ERROR = "StatusNotSupported";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_NOT_FOUND_ERROR}.
   */
  private final String errorCode = ERROR_CODE_NOT_FOUND_ERROR;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public CommunicationStatusNotSupportedException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public CommunicationStatusNotSupportedException(final String message, final String errorCode) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public CommunicationStatusNotSupportedException(final String message) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }
}