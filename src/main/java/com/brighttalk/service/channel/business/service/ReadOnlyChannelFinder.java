/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2013.
 * All Rights Reserved.
 * $Id: ReadOnlyChannelFinder.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.brighttalk.service.channel.business.service.channel.DefaultChannelFinder;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;

/**
 * Read only Implementation of the Channel Finder.
 * 
 * This is used to use a different set of Daos that are configured to use different database credentials that have 
 * read only permissions
 */
@Service
@Qualifier(value = "readOnlyChannelFinder")
public class ReadOnlyChannelFinder extends DefaultChannelFinder {

  @Autowired
  public void setChannelDbDao(@Qualifier(value = "readOnlyChannelDbDao") ChannelDbDao channelDbDao) {
    this.channelDbDao = channelDbDao;
  }

  @Autowired
  public void setCategoryDbDao(@Qualifier(value = "readOnlyCategoryDbDao") CategoryDbDao categoryDbDao) {
    this.categoryDbDao = categoryDbDao;
  }

  @Autowired
  public void setCommunicationDbDAO(@Qualifier(value = "readOnlyCommunicationDbDao") CommunicationDbDao communicationDbDAO) {
    this.communicationDbDAO = communicationDbDAO;
  }
}
