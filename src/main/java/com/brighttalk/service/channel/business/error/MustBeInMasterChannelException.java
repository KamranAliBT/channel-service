/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MustBeInMasterChannelException.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates a communication is not in its master channel.
 */
@SuppressWarnings("serial")
public class MustBeInMasterChannelException extends ApplicationException {

  private static final String ERROR_CODE_MAST_BE_MASTER_CHANNEL_ERROR = "MustBeInMasterChannel";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_MAST_BE_MASTER_CHANNEL_ERROR}.
   */
  private String errorCode = ERROR_CODE_MAST_BE_MASTER_CHANNEL_ERROR;

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public MustBeInMasterChannelException(String message, String errorCode) {
    super(message, ERROR_CODE_MAST_BE_MASTER_CHANNEL_ERROR);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public MustBeInMasterChannelException(String message) {
    super(message, ERROR_CODE_MAST_BE_MASTER_CHANNEL_ERROR);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

}
