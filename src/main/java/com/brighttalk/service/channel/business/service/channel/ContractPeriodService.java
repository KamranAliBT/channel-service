/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContractPeriodService.java 101520 2015-10-15 16:29:24Z kali $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.common.validation.ValidationException;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.ContractPeriodNotFoundException;
import com.brighttalk.service.channel.business.error.InvalidContractPeriodException;

import java.util.List;

/**
 * The Contract Period Service Business API.
 * <p>
 * Responsible for managing {@link ContractPeriod}s.
 * <p>
 */
public interface ContractPeriodService {

  /**
   * Creates the given contract period.
   * <p>
   *
   * @param user details of the user requesting Contract Period creation.
   * @param contractPeriod Contract period details of the Contract Period to be created
   *
   * @return {@link ContractPeriod}
   *
   * @throws ValidationException if the given {@link ContractPeriod} is invalid
   * @throws UserAuthorisationException if the current user is not authorised to create a contract period
   */
  ContractPeriod create(final User user, final ContractPeriod contractPeriod);

  /**
   * Updates the given Contract Period if the given user is authorized for an update.
   *
   * @param user the user requesting the update - who must be authorized
   * @param contractPeriod details to be updated
   *
   * @return updated ContractPeriod
   *
   * @throws ValidationException if the given {@link ContractPeriod} is invalid
   * @throws UserAuthorisationException if the current user is not authorised to create a contract period
   */
  ContractPeriod update(final User user, final ContractPeriod contractPeriod);

  /**
   * Delete the given Contract Period.
   *
   * @param user the user requesting the delete - who must be authorized.
   * @param contractPeriodId The ID of the Contract Period to be deleted.
   *
   * @throws InvalidContractPeriodException if the contract period is ineligible for deletion
   * @throws UserAuthorisationException if the user is not authorized to perform the delete.
   */
  void delete(final User user, final Long contractPeriodId);

  /**
   * Find a Contract Period with the given Id.
   *
   * @param user The user who making the request, who must be authorized
   * @param contractPeriodId the ID of the contract period required
   *
   * @return a Contract Period with the given ID
   *
   * @throws ContractPeriodNotFoundException if the Contract Period was not found.
   * @throws UserAuthorisationException if the user is not authorized to perform the delete.
   */
  ContractPeriod find(final User user, final Long contractPeriodId);

  /**
   * Returns a list of all {@link ContractPeriod} assigned to the given {@link Channel}
   *
   * @param user The user who making the request, who must be authorized
   * @param channelId The channel for which all Contract Periods are required
   *
   * @return a list of Contract Periods
   *
   * @throws ChannelNotFoundException if a {@link Channel} with the given id was not found.
   * @throws ContractPeriodNotFoundException if no Contract Periods were found.
   * @throws UserAuthorisationException if the user is not authorized to perform the delete.
   */
  List<ContractPeriod> findByChannelId(final User user, final Long channelId);
}