/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsCreateChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

/**
 * Jms implementation of a COMMUNITY Delete Webcast Queue.
 * 
 * 
 * The webcast will then be placed onto a JMS queue where some other listener will pick them up and do the actual
 * processing.
 */
public class JmsCommunityQueuer implements CommunityQueuer {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JmsCommunityQueuer.class);

  /**
   * Delete webcast destination name.
   * 
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.communityDeleteWebcastDestination}")
  private String deleteDestinationName;

  /**
   * Helper class acting in the role of the JMS {@link javax.jms.MessageProducer} Handles creation and release of JMS
   * resources (Connections, Session) on the sending of the JMS message.
   */
  @Qualifier(value = "generic")
  @Autowired
  private JmsTemplate jmsTemplate;

  /** {@inheritDoc} */
  @Override
  public void delete(final Long webcastId) {
    LOGGER.debug("Queuing message for deleting communication [" + webcastId + "].");

    MessageCreator msgCreator = createMessage(webcastId);

    jmsTemplate.send(deleteDestinationName, msgCreator);
  }

  private MessageCreator createMessage(final Long webcastId) {
    MessageCreator msgCreator = new MessageCreator() {

      /**
       * {@inheritDoc}
       * <p>
       * Anonymous implementation of Spring JmsTemplate's simplest callback interface - {@link MessageCreator}. Creates
       * a JMS message that is used to asynchronously request the sending of the message.
       * <p>
       * The message body contains a serializable asset to be processed. This is implemented as a JMS
       * {@link ObjectMessage} since the messages are consumed locally/internally.
       */
      @Override
      public Message createMessage(final Session session) throws JMSException {
        ObjectMessage msg = session.createObjectMessage();
        msg.setObject(webcastId);
        return msg;
      }
    };
    return msgCreator;
  }

}