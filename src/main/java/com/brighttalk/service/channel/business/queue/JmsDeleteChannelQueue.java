/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsDeleteChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

/**
 * Jms implementation of a Delete Channel Queue.
 * 
 * This can be wired into the the DefaultChannelService instance to receive channels that need to 
 * have a certain set of operations performed on when deleting.
 * 
 * These channels will then be placed onto a JMS queue where some other listener will pick them up and do the 
 * actual processing.
 */
public class JmsDeleteChannelQueue implements DeleteChannelQueue {
 
  /**
   * Name of the destination to which JMS messages are sent. The name must 
   * be resolvable to a configured {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.deleteChannelDestination}")
  private String destinationName;
  
  /** 
  * Helper class acting in the role of the JMS {@link javax.jms.MessageProducer}
  * Handles creation and release of JMS resources (Connections, Session) on 
  * the sending of the JMS message. 
  */
  @Qualifier(value = "generic")
  @Autowired
  private JmsTemplate jmsTemplate;
   
  public String getDestinationName() {
    return destinationName;
  }
  
  public void setDestinationName(final String destinationName) {
    this.destinationName = destinationName;
  }
  
  public JmsTemplate getJmsTemplate() {
    return jmsTemplate;
  }
  
  public void setJmsTemplate(final JmsTemplate jmsTemplate) {
    this.jmsTemplate = jmsTemplate;
  }
   
  @Override
  public void deleteChannel( final Long channelId ) {
    
    MessageCreator msgCreator = new MessageCreator() {

      /**
       * {@inheritDoc}
       * <p>
       * Anonymous implementation of Spring JmsTemplate's simplest callback 
       * interface - {@link MessageCreator}. Creates a JMS message that is 
       * used to asynchronously request the sending of the user email.
       * <p>
       * The message body contains a serializable asset to be processed.
       * This is implemented as a JMS {@link ObjectMessage} since the 
       * messages are consumed locally/internally. 
       */
      public Message createMessage(final Session session) throws JMSException {
        ObjectMessage msg = session.createObjectMessage();
        msg.setObject(channelId );
        return msg;
      }
    };

    this.jmsTemplate.send(this.destinationName, msgCreator);
  }

}
