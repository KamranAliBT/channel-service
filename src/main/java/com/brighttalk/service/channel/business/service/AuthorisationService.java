/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: AuthorisationService.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Interface for Authorisation Service.
 * 
 * This service is a container for all business logic around authorising users for actions on Business objects.
 */
public interface AuthorisationService {

  /**
   * Authorise access to a channel by UserId.
   * 
   * The given userId must be the channel owner.
   * 
   * @param channel the channel to authorise the access to
   * @param user The user to authorise
   */
  void channelByUserId(Channel channel, User user);

  /**
   * Authorise the given user to access given channel and communication.
   * 
   * @param user The current User.
   * @param channel The Channel of the communication.
   * @param communication The Communication to be accessed.
   * 
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the given user is not authorised to access
   * the Communication.
   */
  void authoriseCommunicationAccess(User user, Channel channel, Communication communication);

}
