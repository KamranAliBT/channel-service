/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsCreateChannelListener.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.brighttalk.service.channel.business.service.communication.CommunicationCancelService;

/**
 * JMS Implementation of a Message listener to receive message from the Email queuer on webcast cancel.
 */
public class JmsEmailCancelListener implements MessageListener {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JmsEmailCancelListener.class);

  @Autowired
  private CommunicationCancelService communicationCancelService;

  /**
   * {@inheritDoc}
   */
  @Override
  public void onMessage(final Message message) {

    if (message instanceof ObjectMessage) {
      try {

        Long webcastId = (Long) ((ObjectMessage) message).getObject();
        communicationCancelService.emailCancelCallback(webcastId);

      } catch (Exception exception) {
        logger.error("Something went wrong with sending cancel webcast in Email", exception);
      }
    } else {
      logger.error("Cancel email webcast processing broken, because of illegal argument provided. "
          + "Expected [ObjectMessage] got [" + message + "].");
    }
  }

}