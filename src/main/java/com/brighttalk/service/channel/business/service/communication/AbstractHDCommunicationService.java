/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AbstractCommunicationService.java 85193 2014-10-24 15:54:02Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.queue.ImageConverterQueuer;
import com.brighttalk.service.channel.business.queue.LesQueuer;
import com.brighttalk.service.channel.integration.booking.HDBookingServiceDao;
import com.brighttalk.service.channel.integration.imageconverter.ImageConverterServiceDao;

/**
 * Abstract implementation used by the Communication Create / Update / Cancel services.
 * 
 */
abstract class AbstractHDCommunicationService extends AbstractCommunicationService {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(AbstractHDCommunicationService.class);

  /** Booking service Dao. */
  @Autowired
  protected HDBookingServiceDao hDBookingServiceDao;

  /** Image Converter Dao. */
  @Autowired
  protected ImageConverterServiceDao imageConverterServiceDao;

  /** Image Converter queuer used by JMS. */
  @Autowired
  protected ImageConverterQueuer imageConverterQueuer;

  /** LES queuer used by JMS. */
  @Autowired
  protected LesQueuer lesQueuer;

  /**
   * @param webcastId The webcast Id.
   */
  public void imageConverterInformCallback(final Long webcastId) {
    LOGGER.debug("Callback to inform image converter to process webcast [" + webcastId + "].");

    Communication communication = findCommunication(webcastId);
    imageConverterServiceDao.processThumbnailImage(communication);
    imageConverterServiceDao.processPreviewImage(communication);
  }

}