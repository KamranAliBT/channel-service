/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: UserService.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.service.user;

/**
 * The User Service Business API.
 */
public interface UserService {

  /**
   * Deletes the given user from the channel service.
   * 
   * @param userId user id
   */
  void delete(Long userId);

}