/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CreateChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Interface for interacting with the imageconverter service.
 * 
 * The operations that need to be done asynchronously include tasks that must be performed by other services.
 * 
 * To perform those operations service API has a separate callback method.
 */
public interface ImageConverterQueuer {

  /**
   * Queue a message to process a feature image conversion.
   * 
   * @param webcastId id of the webcast updated
   */
  void inform(Long webcastId);
}
