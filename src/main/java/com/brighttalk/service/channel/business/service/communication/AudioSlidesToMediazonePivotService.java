/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: codetemplates.xml 25175 2011-01-10 14:47:45Z nbrown $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.integration.exception.ServiceConnectionFailedException;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.validator.CommunicationProviderUpdateValidator;
import com.brighttalk.service.channel.business.error.DataCleanUpFailedException;
import com.brighttalk.service.channel.business.error.ProviderNotSupportedException;
import com.brighttalk.service.channel.integration.audience.AudienceServiceDao;
import com.brighttalk.service.channel.integration.booking.BookingServiceDao;
import com.brighttalk.service.channel.integration.livestate.LiveStateServiceDao;
import com.brighttalk.service.channel.integration.slide.SlideServiceDao;

/**
 * Implementation of the {@link CommunicationPivotService} to allow pivoting from Audio and Slides to MediaZone
 * webcasts. You can only pivot from Audio and Slides to MediaZone. Once pivoted you cannot pivot back again.
 */
@Service
@Qualifier(value = "audioslidestomediazonepivotservice")
public class AudioSlidesToMediazonePivotService extends AbstractPivotService implements CommunicationPivotService {

  /** Delete slides error message key */
  public static final String DELETE_SLIDES_ERROR_MESSAGE_KEY = "integration.slides.errorOnDelete";

  /** Delete vptes error message key */
  protected static final String DELETE_VOTES_ERROR_MESSAGE_KEY = "integration.audience.errorOnVoteDelete";

  /** Delete vote assets error message key */
  protected static final String DELETE_VOTE_ASSETS_ERROR_MESSAGE_KEY =
      "integration.communicationAssetDb.errorOnVoteDelete";

  /** Delete communication from livestate error message key */
  public static final String DELETE_LIVESTATE_ERROR_MESSAGE_KEY = "integration.livestate.errorOnDelete";

  /** Delete booking error message key */
  public static final String DELETE_BOOKING_ERROR_MESSAGE_KEY = "integration.booking.errorOnDelete";

  private static final Logger LOGGER = Logger.getLogger(AudioSlidesToMediazonePivotService.class);

  @Autowired
  private BookingServiceDao bookingServiceDao;

  @Autowired
  private LiveStateServiceDao liveStateServiceDao;

  @Autowired
  private AudienceServiceDao audienceServiceDao;

  @Autowired
  private SlideServiceDao slideServiceDao;

  /** {@inheritDoc} */
  @Override
  public Provider pivot(final Communication communication, final Provider provider) {
    LOGGER.info("Pivoting webcast [" + communication.getId() + "] to [" + provider.getName().toString() + "]");

    provider.validate(new CommunicationProviderUpdateValidator(communication, channelFinder, messages));

    // order is important: need to store previous provider before an update to reference it later on delete
    // TODO: communication should NOT be mutable (or a similar fix) - too easy to create a bug when the order matters!
    final Provider previousProvider = communication.getProvider();

    updateCommunicationOnProviderChange(communication, provider);
    deleteProviderRelatedCommunicationData(communication.getId(), previousProvider);
    restoreProviderRelatedCommunicationData(communication.getId(), provider);

    return communication.getProvider();
  }

  /**
   * Delete the provider related communication data based on the webcast provider.
   * 
   * @param communicationId the id of the communication
   * @param provider the webcast provider
   */
  private void deleteProviderRelatedCommunicationData(final Long communicationId, final Provider provider) {

    final List<String> errors = new ArrayList<String>();

    String providerName = provider.getName();

    if (providerName.equals(Provider.BRIGHTTALK)) {
      deleteForBrightTalkProvider(communicationId, errors);
    } else {
      throw new ApplicationException("Provider not recognized [" + providerName + "].");
    }

    if (!CollectionUtils.isEmpty(errors)) {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Data clean up for communication [" + communicationId + "] provider update failed with ["
            + errors.size() + "] errors.");
      }
      throw new DataCleanUpFailedException("Data clean-up on update provider failed.", StringUtils.join(errors,
          MESSAGE_DELIMITER));
    }
  }

  /**
   * Method to perform deletion of webcast assets for BrightTALK type webcasts.
   * 
   * @param communicationId the id of the communication to delete the data
   * @param errors list of errors
   */
  private void deleteForBrightTalkProvider(final Long communicationId, final List<String> errors) {
    deleteBooking(communicationId, errors);
    deleteLiveState(communicationId, errors);
    deleteVotes(communicationId, errors);
    deleteSlides(communicationId, errors);
    deleteResources(communicationId, errors);
  }

  private void restoreProviderRelatedCommunicationData(final Long communicationId, final Provider provider) {

    final List<String> errors = new ArrayList<String>();

    if (provider.getName().equals(Provider.MEDIAZONE)) {
      restoreMediazoneCommunicationId(communicationId, errors);
    }

    if (!CollectionUtils.isEmpty(errors)) {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Restoring data for communication [" + communicationId + "] provider update failed with ["
            + errors.size() + "] errors.");
      }
      throw new DataCleanUpFailedException("Restoring data on update provider failed.", StringUtils.join(errors,
          MESSAGE_DELIMITER));
    }
  }

  private void restoreMediazoneCommunicationId(final Long communicationId, final List<String> errors) {

    try {

      communicationDbDao.restoreMediazoneAssets(communicationId);

    } catch (Exception exception) {

      LOGGER.error("Restoring mediazone assets for communication [" + communicationId + "] failed.", exception);

      final String mediazoneAssetsDeleteErrorMessage = messages.getValue(RESTORE_RENDITIONS_ERROR_MESSAGE_KEY);
      errors.add(mediazoneAssetsDeleteErrorMessage);
    }
  }

  /**
   * Update communication on provider change.
   * 
   * Currently supports only scenario where provider is changed from 'brighttalk' to 'mediazone' hence all the actions
   * like clearing the telephony and thumbnail details.
   * 
   * @param communicationId the id of the communication that is to be updated
   * @param provider the new provider value
   * 
   * @return updated communication
   */
  private Communication updateCommunicationOnProviderChange(final Communication communication,
      final Provider targetProvider) {

    if (!targetProvider.getName().equals(Provider.MEDIAZONE)) {
      // we should not ever get here - guaranteed by validation
      throw new ProviderNotSupportedException("Not allowed to pivot provider from ["
          + communication.getProvider().getName() + "] to [" + targetProvider.getName() + "].");
    }

    communication.setPinNumber(null);
    communication.setLivePhoneNumber(null);
    communication.setLiveUrl(null);

    LOGGER.info("Removing thumbnail and preview urls for communication [" + communication.getId()
        + "]. The values were: thumbnail [" + communication.getThumbnailUrl() + "], preview ["
        + communication.getPreviewUrl() + "].");

    communication.setThumbnailUrl(null);
    communication.setPreviewUrl(null);
    communication.setProvider(targetProvider);

    communicationDbDao.pivot(communication);

    return communication;
  }

  private void deleteBooking(final Long communicationId, final List<String> errors) {

    List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add(communicationId);

    try {

      bookingServiceDao.deleteBookings(communicationIds);

    } catch (Exception exception) {

      LOGGER.error("Deleting booking for communication [" + communicationId + "] failed.", exception);

      final String bookingServiceDeleteErrorMessage = messages.getValue(DELETE_BOOKING_ERROR_MESSAGE_KEY);
      errors.add(bookingServiceDeleteErrorMessage);
    }
  }

  private void deleteLiveState(final Long communicationId, final List<String> errors) {

    try {

      liveStateServiceDao.deleteCommunication(communicationId);

    } catch (ServiceConnectionFailedException connectionException) {

      // this is NOT an error case - communication is no longer in livestate db, hence 404 returned.
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Removing communication [" + communicationId + "] from live state returned no communication.");
      }
    } catch (Exception exception) {

      LOGGER.error("Deleting live state for communication [" + communicationId + "] failed.", exception);

      final String liveStateServiceDeleteErrorMessage = messages.getValue(DELETE_LIVESTATE_ERROR_MESSAGE_KEY);
      errors.add(liveStateServiceDeleteErrorMessage);
    }
  }

  private void deleteVotes(final Long communicationId, final List<String> errors) {

    try {

      communicationDbDao.removeVotes(communicationId);

    } catch (Exception deleteVoteAssetsException) {

      LOGGER.error("Deleting vote assets for communication [" + communicationId + "] failed.",
          deleteVoteAssetsException);

      String deleteAssetsErrorMessage = messages.getValue(DELETE_VOTE_ASSETS_ERROR_MESSAGE_KEY);
      errors.add(deleteAssetsErrorMessage);
    }

    List<Long> voteIds;
    String audienceServiceDeleteErrorMessage;

    try {
      voteIds = audienceServiceDao.findVoteIds(communicationId);
    } catch (Exception findVotesException) {

      LOGGER.error("Retrieving votes for communication [" + communicationId + "] failed.", findVotesException);
      audienceServiceDeleteErrorMessage = messages.getValue(DELETE_VOTES_ERROR_MESSAGE_KEY, new Object[] { "all" });
      errors.add(audienceServiceDeleteErrorMessage);
      return;
    }

    if (CollectionUtils.isEmpty(voteIds)) {
      return;
    }

    for (Long voteId : voteIds) {
      try {

        audienceServiceDao.deleteVote(voteId);

      } catch (Exception deleteAudienceVoteException) {

        LOGGER.error("Deleting vote [" + voteId + "] for communication [" + communicationId + "] failed.",
            deleteAudienceVoteException);

        audienceServiceDeleteErrorMessage = messages.getValue(DELETE_VOTES_ERROR_MESSAGE_KEY, new Object[] { voteId });
        errors.add(audienceServiceDeleteErrorMessage);
      }
    }
  }

  private void deleteSlides(final Long communicationId, final List<String> errors) {

    try {

      slideServiceDao.deleteSlides(communicationId);

    } catch (Exception exception) {

      LOGGER.error("Deleting slides for communication [" + communicationId + "] failed.", exception);

      final String slideServiceDeleteErrorMessage = messages.getValue(DELETE_SLIDES_ERROR_MESSAGE_KEY);
      errors.add(slideServiceDeleteErrorMessage);
    }
  }

  private void deleteResources(final Long communicationId, final List<String> errors) {

    List<Resource> resources = null;
    String resourcesDbDeleteErrorMessage = null;

    try {

      resources = resourceDbDao.findAll(communicationId);

    } catch (Exception findResourcesException) {

      LOGGER.error("Retrieving resources for communication [" + communicationId + "] failed.", findResourcesException);
      resourcesDbDeleteErrorMessage = messages.getValue(resourcesDbDeleteErrorMessage);
      errors.add(resourcesDbDeleteErrorMessage);
      return;
    }

    if (CollectionUtils.isEmpty(resources)) {
      return;
    }

    for (Resource resource : resources) {
      try {

        resourceDbDao.delete(communicationId, resource.getId());

      } catch (Exception exception) {

        LOGGER.error(
            "Deleting resource [" + resource.getId() + "] for communication [" + communicationId + "] failed.",
            exception);

        resourcesDbDeleteErrorMessage = messages.getValue(DELETE_RESOURCES_ERROR_MESSAGE_KEY,
            new Object[] { resource.getId() });
        errors.add(resourcesDbDeleteErrorMessage);
      }
    }
  }

}
