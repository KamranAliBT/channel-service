/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: FeaturesPostProcessor.java 75381 2014-03-18 15:16:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;
import com.brighttalk.service.channel.integration.database.SubscriptionDbDao;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.util.UserSearchCriteriaBuilder;

/**
 * Default implementation of the {@link FeaturesPostProcessor}.
 * 
 * This class is responsible for post processing any extra actions for the updated features.
 */
@Component
public class FeaturesPostProcessor {

  private static final Logger LOGGER = Logger.getLogger(FeaturesPostProcessor.class);

  @Autowired
  @Qualifier("localUserServiceDao")
  private UserServiceDao userServiceDao;

  @Autowired
  private UserSearchCriteriaBuilder userSearchCriteriaBuilder;

  @Autowired
  private SubscriptionDbDao subscriptionDbDao;

  /**
   * Process the list of supplied features on the supplied channel .
   * 
   * @param channel the channel on which the supplied features were updated
   * @param updatedFeatures list of updated features.
   */
  public void process(final Channel channel, final List<Feature> updatedFeatures) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Start post processing features.");
    }

    if (CollectionUtils.isEmpty(updatedFeatures)) {
      return;
    }

    postProcessBlockedUsersFeature(channel, updatedFeatures);
  }

  private void postProcessBlockedUsersFeature(final Channel channel, final List<Feature> featuresToUpdate) {

    Feature blockedUsersFeature = null;

    for (Feature feature : featuresToUpdate) {
      if (feature.getName().equals(Type.BLOCKED_USER)) {
        blockedUsersFeature = feature;
        break;
      }
    }

    if (blockedUsersFeature == null || !blockedUsersFeature.isEnabled()) {
      return;
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Start post processing the 'blockedUsers' feature.");
    }

    String emailAddresses = blockedUsersFeature.getValue();

    if (StringUtils.isBlank(emailAddresses)) {
      return;
    }

    List<User> users = findUsersByEmailAddresses(emailAddresses);

    if (CollectionUtils.isEmpty(users)) {
      return;
    }

    List<Long> userIds = extractUsersIds(users);
    subscriptionDbDao.unsubscribeUsers(channel.getId(), userIds);
  }

  private List<User> findUsersByEmailAddresses(final String emailAddresses) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding users by email addresses [" + emailAddresses + "].");
    }

    UsersSearchCriteria criteria = userSearchCriteriaBuilder.buildWithDefaultValues();
    PaginatedList<User> paginatedUsers;
    List<User> users = new ArrayList<>();

    do {
      paginatedUsers = userServiceDao.findByEmailAddress(emailAddresses, criteria);

      if (CollectionUtils.isEmpty(paginatedUsers)) {
        break;
      }

      users.addAll(paginatedUsers);

      int pageNumber = paginatedUsers.getCurrentPageNumber() + 1;
      criteria.setPageNumber(pageNumber);

    } while (paginatedUsers.hasNextPage());

    return users;
  }

  private List<Long> extractUsersIds(final List<User> users) {
    List<Long> ids = new ArrayList<>();

    for (User user : users) {
      ids.add(user.getId());
    }

    return ids;
  }

}
