/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationOutsideAuthoringPeriodException.java 62869 2013-04-09 09:17:41Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that the requested communication cannot be updated, because the it is outside of the authoring period - at
 * least 15 minutes before scheduled date.
 */
@SuppressWarnings("serial")
public class CommunicationOutsideAuthoringPeriodException extends ApplicationException {

  private static final String ERROR_CODE = "CannotUpdateProviderOutsideAuthoringPeriod";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE}.
   */
  private final String errorCode = ERROR_CODE;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public CommunicationOutsideAuthoringPeriodException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public CommunicationOutsideAuthoringPeriodException(final String message, final String errorCode) {
    super(message, ERROR_CODE);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public CommunicationOutsideAuthoringPeriodException(final String message) {
    super(message, ERROR_CODE);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }
}