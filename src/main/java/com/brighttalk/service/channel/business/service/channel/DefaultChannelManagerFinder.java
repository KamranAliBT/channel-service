/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelManagerService.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.channel.ChannelManagersSearchCriteria;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;
import com.brighttalk.service.channel.integration.database.ChannelManagerDbDao;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.util.PaginatedListBuilder;
import com.brighttalk.service.channel.presentation.util.UserSearchCriteriaBuilder;

/**
 * Default implementation of the {@link ChannelManagerService}.
 */
@Service
@Primary
public class DefaultChannelManagerFinder implements ChannelManagerFinder {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(DefaultChannelManagerFinder.class);

  @Autowired
  private ChannelFinder channelFinder;

  @Autowired
  @Qualifier("localUserServiceDao")
  private UserServiceDao userServiceDao;

  @Autowired
  private ChannelManagerDbDao channelManagerDbDao;

  @Autowired
  private PaginatedListBuilder paginatedListBuilder;

  @Autowired
  private UserSearchCriteriaBuilder userSearchCriteriaBuilder;

  @Autowired
  private ChannelManagerValidator validator;

  /** {@inheritDoc} */
  @Override
  public PaginatedList<ChannelManager> find(final User user, final Long channelId,
      final ChannelManagersSearchCriteria criteria) {

    LOGGER.debug("Fiding channel managers for the criteria [" + criteria + "].");

    Channel channel = channelFinder.find(channelId);

    validator.assertSearch(user, channel);

    List<ChannelManager> channelManagers = channelManagerDbDao.find(channel);

    PaginatedList<ChannelManager> paginatedChannelManagers = getPaginatedChannelManagers(channelManagers, criteria);

    LOGGER.debug("Found [" + paginatedChannelManagers.size() + "] channel managers.");

    return paginatedChannelManagers;
  }

  private PaginatedList<ChannelManager> getPaginatedChannelManagers(final List<ChannelManager> channelManagers,
      final ChannelManagersSearchCriteria criteria) {

    List<ChannelManager> filteredChannelManagers = new ArrayList<ChannelManager>();

    PaginatedList<ChannelManager> paginatedChannelManagers;

    if (channelManagers.isEmpty()) {
      paginatedChannelManagers = paginatedListBuilder.build(filteredChannelManagers, criteria.getPageNumber(),
          criteria.getPageSize());

      return paginatedChannelManagers;
    }

    filteredChannelManagers.addAll(filterChannelManagers(channelManagers, criteria));

    paginatedChannelManagers = paginatedListBuilder.build(filteredChannelManagers, criteria.getPageNumber(),
        criteria.getPageSize());

    return paginatedChannelManagers;
  }

  private List<ChannelManager> filterChannelManagers(final List<ChannelManager> channelManagers,
      final ChannelManagersSearchCriteria criteria) {

    List<ChannelManager> filteredChannelManagers = new ArrayList<ChannelManager>();

    List<User> users = findUsers(channelManagers, criteria);

    Map<Long, ChannelManager> mappedChannelManagers = getMappedChannelManagers(channelManagers);

    ChannelManager channelManager;

    for (User user : users) {
      channelManager = mappedChannelManagers.get(user.getId());
      channelManager.setUser(user);

      filteredChannelManagers.add(channelManager);
    }

    return filteredChannelManagers;
  }

  private List<User> findUsers(final List<ChannelManager> channelManagers,
      final ChannelManagersSearchCriteria channelManagerCriteria) {

    Integer pageNumber = (channelManagerCriteria.getPageNumber() - 1) * channelManagerCriteria.getPageSize();
    Integer pageSize = channelManagerCriteria.getPageSize() + 1;
    String sortOrder = channelManagerCriteria.getSortOrder().name();
    String sortBy = channelManagerCriteria.getSortBy().getName();

    UsersSearchCriteria userCriteria = userSearchCriteriaBuilder.build(pageNumber, pageSize, sortBy, sortOrder);

    List<Long> userIds = getUserIds(channelManagers);

    List<User> users = userServiceDao.find(userIds, userCriteria);

    return users;
  }

  private Map<Long, ChannelManager> getMappedChannelManagers(final List<ChannelManager> channelManagers) {

    Map<Long, ChannelManager> mappedChannelManagers = new HashMap<Long, ChannelManager>();

    for (ChannelManager channelManager : channelManagers) {
      mappedChannelManagers.put(channelManager.getUser().getId(), channelManager);
    }

    return mappedChannelManagers;
  }

  private List<Long> getUserIds(final List<ChannelManager> channelManagers) {

    List<Long> userIds = new ArrayList<Long>();

    for (ChannelManager cm : channelManagers) {
      userIds.add(cm.getUser().getId());
    }

    return userIds;
  }

}