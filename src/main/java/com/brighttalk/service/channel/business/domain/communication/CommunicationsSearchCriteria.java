/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: CommunicationsSearchCriteria.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;

import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;

/**
 * 
 * Search Criteria for retrieving the paginated communications list.
 * <p>
 * Contains the page number being requested, the size of the page being requested.
 * <p>
 * Flags on how the communications could be expanded are part of this class, too.
 * <p>
 * It can be used for syntactical validation of the request parameters.
 */
public class CommunicationsSearchCriteria extends BaseSearchCriteria {

  private static final String EXPAND_CATEGORIES = "categories";

  @NotNull
  @Pattern(regexp = "(\\d+-\\d+)(,\\d+-\\d+)*", message = "{error.invalid.channel.communication.ids.value}")
  private String ids;

  @Pattern(regexp = EXPAND_CATEGORIES, message = "{error.invalid.expand.value}")
  private String expand;

  /**
   * Helper property containing list of pairs channelId-communicationId generated based on provided ids 
   */
  private List<String> chanelAndCommunicationsIds;

  public void setIds(final String ids) {
    this.ids = ids;
  }

  public void setExpand(final String expand) {
    this.expand = expand;
  }

  /**
   * Indicates if Channel search criteria should be expanded by featuredWebcasts.
   * 
   * @return true or false
   */
  public boolean includeCategories() {
    return EXPAND_CATEGORIES.equals(expand);
  }

  /**
   * Converts comma separated list of combinded ids to list of combinded channel-communicaion ids.
   * 
   * It removes duplicates if they exist in the list.
   * 
   * @return returns collection of channel ids 
   */
  public List<String> getChanelAndCommunicationsIds() {

    if (chanelAndCommunicationsIds != null) {
      return chanelAndCommunicationsIds;
    }

    if (StringUtils.isEmpty(ids)) {
      chanelAndCommunicationsIds = new ArrayList<String>();
      return chanelAndCommunicationsIds;
    }

    Set<String> channelAndCommunicationSet = new LinkedHashSet<String>();

    if (!ids.contains(",")) {
      channelAndCommunicationSet.add(ids);

    } else {
      String[] channelIdStrings = StringUtils.split(ids, ',');
      for (String idString : channelIdStrings) {
        channelAndCommunicationSet.add(idString);
      }
    }
    chanelAndCommunicationsIds = new ArrayList<String>(channelAndCommunicationSet);
    return chanelAndCommunicationsIds;
  }
}