/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2011.
 * All Rights Reserved.
 * $Id: ImageConverterSettings.java 41648 2012-01-31 16:16:53Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

/**
 * Represents Image Converter requests settings.
 */
public class ImageConverterSettings {

  /** Processing an image conversion of type preview. */
  public static final String TYPE_PREVIEW = "preview";

  /** Processing an image conversion of type thumbnail. */
  public static final String TYPE_THUMBNAIL = "thumbnail";

  /** source of the image (san, aws...) */
  private String source;

  /** destination of the file once has been completed. */
  private String destination;

  /** url to notify back once the process has been completed. */
  private String notify;

  /** format of the image. */
  private String format;

  /** width of the image. */
  private Integer width;

  /** height of the image. */
  private Integer height;

  /** The status of this task. E.g. Pending, Complete etc. */
  private ImageConverterSettings.Status status;

  /** Different task status available. */
  public static enum Status {
    /** Task has successfully been processed. */
    COMPLETE,
    /** Task is pending to be processed. */
    PENDING,
    /** Task has failed to be processed. */
    FAILED,
    /** Task has been cancelled. */
    CANCELLED
  }

  /** Scale mode of the image. */
  private String scaleMode;

  /**
   * @return the source
   */
  public String getSource() {
    return source;
  }

  /**
   * @param source the source to set
   */
  public void setSource(final String source) {
    this.source = source;
  }

  /**
   * @return the destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * @param destination the destination to set
   */
  public void setDestination(final String destination) {
    this.destination = destination;
  }

  /**
   * @return the notify
   */
  public String getNotify() {
    return notify;
  }

  /**
   * @param notify the notify to set
   */
  public void setNotify(final String notify) {
    this.notify = notify;
  }

  /**
   * @return the format
   */
  public String getFormat() {
    return format;
  }

  /**
   * @param format the format to set
   */
  public void setFormat(final String format) {
    this.format = format;
  }

  /**
   * @return the width
   */
  public Integer getWidth() {
    return width;
  }

  /**
   * @param width the width to set
   */
  public void setWidth(final Integer width) {
    this.width = width;
  }

  /**
   * @return the height
   */
  public Integer getHeight() {
    return height;
  }

  /**
   * @param height the height to set
   */
  public void setHeight(final Integer height) {
    this.height = height;
  }

  /**
   * @return the status
   */
  public ImageConverterSettings.Status getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(final ImageConverterSettings.Status status) {
    this.status = status;
  }

  /**
   * @return the scaleMode
   */
  public String getScaleMode() {
    return scaleMode;
  }

  /**
   * @param scaleMode the scaleMode to set
   */
  public void setScaleMode(final String scaleMode) {
    this.scaleMode = scaleMode;
  }
}
