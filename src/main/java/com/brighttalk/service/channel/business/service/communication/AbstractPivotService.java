/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: codetemplates.xml 25175 2011-01-10 14:47:45Z nbrown $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.brighttalk.common.configuration.Messages;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.RenditionDbDao;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;

/**
 * AbstractPivotService - common functionality across all the different types of pivoting.
 */
@Service
abstract class AbstractPivotService {

  /** LOGGER constant. */
  protected static final Logger LOGGER = Logger.getLogger(AbstractPivotService.class);

  /** Message delimiter constant. */
  protected static final String MESSAGE_DELIMITER = "; ";

  /** Restore mediazone assets error message key */
  protected static final String RESTORE_RENDITIONS_ERROR_MESSAGE_KEY =
      "integration.communicationAssetDb.errorOnMediazoneRestore";

  /** Delete resources error message key */
  protected static final String DELETE_RESOURCES_ERROR_MESSAGE_KEY = "integration.resourcesDb.errorOnDelete";

  /** channel finder. */
  @Autowired
  @Qualifier(value = "readOnlyChannelFinder")
  protected ChannelFinder channelFinder;

  /** communication db Dao. */
  @Autowired
  protected CommunicationDbDao communicationDbDao;

  /** rendition db Dao. */
  @Autowired
  protected RenditionDbDao renditionDbDao;

  /** resource db Dao. */
  @Autowired
  protected ResourceDbDao resourceDbDao;

  /** messages. */
  @Autowired
  protected Messages messages;

}
