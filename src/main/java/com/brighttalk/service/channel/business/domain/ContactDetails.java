/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContactDetails.java 88375 2015-01-15 13:25:34Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * ContactDetails representation. Used typically when creating a channel.
 */
public class ContactDetails {

  private String firstName;

  private String lastName;

  private String email;

  private String telephone;

  private String jobTitle;

  private String company;

  private String address1;

  private String address2;

  private String city;

  private String state;

  private String postcode;

  private String country;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getJobTitle() {
    return jobTitle;
  }

  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }
  
  @Override
  public String toString() {

    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("firstName", getFirstName());
    builder.append("lastName", getLastName());
    builder.append("email", getEmail());
    builder.append("telephome", getTelephone());
    builder.append("jobTitle", getJobTitle());
    builder.append("companyName", getCompany());
    builder.append("address1", getAddress1());
    builder.append("address2", getAddress2());
    builder.append("city", getCity());
    builder.append("state", getState());
    builder.append("postCode", getPostcode());
    builder.append("country", getCountry());

    return builder.toString();
  }
}