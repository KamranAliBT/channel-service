/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationResourceModificationValidator.java 74757 2014-02-27 11:18:36Z ikerbib $ 
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.MustBeInMasterChannelException;
import com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException;
import com.brighttalk.service.channel.business.error.NotAllowedToAccessMediaZoneCommunicationException;

/**
 * {@link Validator} that validates if a communication is in a validate state for resources to be 
 * modified (e.g. added, updated). 
 */
public class CommunicationResourceModificationValidator implements Validator<Communication> {

  /**
   * Validate the given communication is valid for resource modification (create, update, delete).
   * 
   * Biz Rules:
   * - a Mediazone communications cannot have resources.
   * - communication cannot be in the 15 minutes before go-live
   * - communication cannot be in the LIVE state
   * - communication connot be in the PENDING state
   * - communication must be in the master channel
   * 
   * @param communication the communication to validate
   * 
   * @throws NotAllowedAtThisTimeException is the communication is live, pending or in the 15 mins before live
   * @throws MustBeInMasterChannelException if the communication is not in its master channel (i.e. its syndicated).
   * @throws NotAllowedToAccessMediaZoneCommunicationException if the communication provided by MediaZone.
   */
  public void validate(Communication communication) {

    if (communication.isMediaZone()) {
      throw new NotAllowedToAccessMediaZoneCommunicationException("Communication [" + communication.getId()
          + "] is a MediaZone communication. MediaZone communications do not support resources.", "IsMediaZoneCommunication");
    }

    if (communication.isIn15MinuteWindow()) {
      throw new NotAllowedAtThisTimeException("Communication [" + communication.getId()
          + "] is in the 15 min window to go live. Resources cannot be modified during this window.");
    }

    if (communication.isPending()) {
      throw new NotAllowedAtThisTimeException("Communication [" + communication.getId()
          + "] is pending to start. Resources cannot be modified when a communication is pending.");
    }

    if (communication.isLive()) {
      throw new NotAllowedAtThisTimeException("Communication [" + communication.getId()
          + "] is in LIVE state. Resources cannot be modified when a communication is live.");
    }

    if (!communication.getConfiguration().isInMasterChannel()) {
      throw new MustBeInMasterChannelException("Communication [" + communication.getId()
          + "] is not in its master channel. Resources can only be modified for master communications.");
    }
  }

}
