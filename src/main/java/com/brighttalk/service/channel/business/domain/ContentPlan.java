/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationFinder.java 70153 2013-10-23 17:23:51Z acairns $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.ContentPlanException;

/**
 * The Content Plan Checker.
 * <p>
 * Responsible for asserting if a webcast can be created/updated in the content plan.
 * </p>
 */
public interface ContentPlan {

  /**
   * Check if the communication can be created within the channel content plan.
   * 
   * @param user The user creating the communication.
   * @param channel The channel in which the communication is going to be created.
   * @param communication The communication to create.
   * @throws ContentPlanException Thrown if the the channel content plan has been reached.
   */
  void assertForCreate(User user, Channel channel, Communication communication) throws ContentPlanException;

  /**
   * Check if the communication can be updated within the channel content plan.
   * 
   * @param user The user creating the communication.
   * @param channel The channel in which the communication is going to be updated.
   * @param communication The communication to update.
   * @throws ContentPlanException Thrown if the the channel content plan has been reached.
   */
  void assertForUpdate(User user, Channel channel, Communication communication) throws ContentPlanException;

}