/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationPivotService.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Communication (aka webcast) Pivoting Business API.
 * 
 * Pivoting a communication simply means changing its type from one type to another whilst preserving certain data like
 * registrations. This was introduced in response to client demand that they had booked a communication of one type but
 * subsequently wanted to change it to another type whilst not losing all their pre-registrations. For example a client
 * might have booked a mediazone gig but then later wanted it change to a audio and slides gig before it went live.
 * 
 * So for example you can pivot a mediazone communication to an audio and slides communication whilst preserving all the
 * pre-regs for the original communication type.
 * 
 * There are number of biz rules around pivoting, for example you can only pivot a given communication type when its in
 * a certain state.
 * 
 * Pivoting is typically a manual process invoked by operations staff via a pivot form. However for HD gigs, automatic
 * pivoting is also available as part of a MVP.
 * 
 * @see <a href="http://svn.brighttalk.com/wiki/G2Project/API/External/Channel/WebcastProvider">External Webcast
 * Provider</a>
 * @see <a href="http://svn.brighttalk.com/wiki/G2Project/API/Internal/Channel/WebcastProvider">Internal Webcast
 * Provider</a>
 */
public interface CommunicationPivotService {

  /**
   * Pivot the given communication to the given provider.
   * 
   * @param communication the communication to be updated
   * @param provider to update teh communication to.
   * 
   * @return updated provider value
   */
  Provider pivot(Communication communication, Provider provider);

}
