/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsElementExtractor.java 91635 2015-03-13 16:31:34Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.extractor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;
import com.brighttalk.service.channel.integration.communication.CommunicationServiceDao;
import com.brighttalk.service.channel.integration.communication.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.summit.SummitServiceDao;
import com.brighttalk.service.channel.integration.summit.dto.SummitDto;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.util.UserSearchCriteriaBuilder;

/**
 * Utility class used to extract elements from the list of supplied {@link Subscription}.
 */
@Component
public class SubscriptionsElementExtractor {

  @Value("${subscriptions.user.search.pageNumber}")
  private int userSearchPageNumber;

  @Value("${subscriptions.user.search.pageSize}")
  private int userSearchPageSize;

  @Autowired
  private SummitServiceDao summitServiceDao;

  @Autowired
  private CommunicationServiceDao commServiceDao;

  @Autowired
  private UserSearchCriteriaBuilder userSearchCriteriaBuilder;

  @Autowired
  @Qualifier("localUserServiceDao")
  private UserServiceDao userServiceDao;

  /**
   * Extract all the lead context summits.
   * 
   * @param subscriptions the subscriptions.
   * @return List of all lead context summits.
   */
  public List<SummitDto> extractLeadContextSummits(final List<Subscription> subscriptions) {
    List<Long> summitIds = new ArrayList<>();
    for (Subscription subscription : subscriptions) {
      SubscriptionContext context = subscription.getContext();
      if (context != null && context.isLeadTypeSummit()) {
        summitIds.add(Long.parseLong(context.getLeadContext()));
      }
    }

    List<SummitDto> summits = new ArrayList<>();
    if (!summitIds.isEmpty()) {
      summits = this.summitServiceDao.findPublicSummitsByIds(summitIds);
    }
    return summits;
  }

  /**
   * Extract all the lead context communication Ids.
   * 
   * @param subscriptions the subscriptions.
   * @return List of all lead context communications.
   */
  public List<CommunicationDto> extractLeadContextCommunications(final List<Subscription> subscriptions) {
    List<Long> commIds = new ArrayList<>();
    for (Subscription subscription : subscriptions) {
      SubscriptionContext context = subscription.getContext();
      if (context != null && context.isLeadTypeContent()) {
        commIds.add(Long.parseLong(context.getLeadContext()));
      }
    }

    List<CommunicationDto> communications = new ArrayList<>();
    if (!commIds.isEmpty()) {
      communications = this.commServiceDao.findCommunicationsByIds(commIds);
    }
    return communications;
  }

  /**
   * Search users by IDs and email addresses.
   * 
   * @param subscriptions the subscriptions
   * 
   * @return users
   */
  public List<User> extractUsers(final List<Subscription> subscriptions) {
    List<Long> userIds = extractUserIds(subscriptions);
    List<String> userEmails = extractUserEmails(subscriptions);

    if (CollectionUtils.isEmpty(userIds) && CollectionUtils.isEmpty(userEmails)) {
      return new ArrayList<User>();
    }

    UsersSearchCriteria criteria = userSearchCriteriaBuilder.build(userSearchPageNumber, userSearchPageSize);
    return userServiceDao.findByIdAndEmailAddress(userIds, userEmails, criteria);
  }

  /**
   * Extract user IDs from subscriptions.
   * 
   * @param subscriptions the subscriptions
   * 
   * @return the user IDs
   */
  public List<Long> extractUserIds(final List<Subscription> subscriptions) {
    List<Long> userIds = new ArrayList<>();

    for (Subscription subscription : subscriptions) {
      User user = subscription.getUser();
      if (user.getId() != null) {
        userIds.add(user.getId());
      }
    }

    return userIds;
  }

  /**
   * Extract user email addresses from subscriptions.
   * 
   * @param subscriptions the subscriptions
   * 
   * @return the user email addresses
   */
  public List<String> extractUserEmails(final List<Subscription> subscriptions) {
    List<String> userEmails = new ArrayList<String>();

    for (Subscription subscription : subscriptions) {
      User user = subscription.getUser();
      if (StringUtils.isNotBlank(user.getEmail())) {
        userEmails.add(user.getEmail());
      }
    }

    return userEmails;
  }
}
