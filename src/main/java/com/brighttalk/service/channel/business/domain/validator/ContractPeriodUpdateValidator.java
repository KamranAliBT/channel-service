/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodUpdateValidator.java 101545 2015-10-16 11:26:42Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.OverlappingContractPeriod;
import com.brighttalk.service.channel.business.error.InvalidContractPeriodException;
import com.brighttalk.service.channel.integration.database.ContractPeriodDbDao;
import org.joda.time.DateTime;

/**
 * Validates a {@link ContractPeriod} prior to updating it with a new one.
 *
 * A contract period cannot be updated if it does not exist for the given contract or if it has already expired (is
 * older than tody)
 */
public class ContractPeriodUpdateValidator implements Validator<ContractPeriod> {
  /**
   * ContractPeriodDBDao to verify the ContractPeriod
   */
  private final ContractPeriodDbDao contractPeriodDbDao;

  // When the existing contract period is not in the database
  private static final String CONTRACT_PERIOD_NOT_FOUND = "ContractPeriodNotFound";

  //When the start of a contract period is invalid
  private static final String INVALID_CONTRACT_PERIOD_START = "InvalidContractPeriodStart";

  //When the end of a contract period is invalid
  private static final String INVALID_CONTRACT_PERIOD_END = "InvalidContractPeriodEnd";

  //When a contract period is invalid
  private static final String INVALID_CONTRACT_PERIOD = "InvalidContractPeriod";

  //When a contract period overlaps an existing contract period
  private static final String OVERLAPPING_CONTRACT_PERIOD = "OverlappingContractPeriod";

  public ContractPeriodUpdateValidator(ContractPeriodDbDao contractPeriodDbDao) {
    this.contractPeriodDbDao = contractPeriodDbDao;
  }

  @Override
  public void validate(final ContractPeriod contractPeriod) {
    //check the end date
    assertEndDate(contractPeriod);

    //check the contract period as a whole
    assertContractPeriod(contractPeriod);
  }

  /**
   * Assert the contract period as a whole
   *
   * @param contractPeriod to be validated for correctness
   */
  private void assertContractPeriod(final ContractPeriod contractPeriod) {
    DateTime midnightLastNight = new DateTime().withTimeAtStartOfDay();
    DateTime endDate = new DateTime(contractPeriod.getEnd().getTime());
    DateTime startDate = new DateTime(contractPeriod.getStart().getTime());

    //get the existing contract period that will be updated
    ContractPeriod existingContractPeriod = contractPeriodDbDao.find(contractPeriod.getId());

    //has the existing contract period already expired
    DateTime existingEndDate = new DateTime(existingContractPeriod.getEnd().getTime());
    boolean contractEndsBeforeNow = existingEndDate.isBefore(midnightLastNight);
    if (contractEndsBeforeNow) {
      throw new InvalidContractPeriodException("Contract period in the past cannot be updated.",
          INVALID_CONTRACT_PERIOD);
    }

    //verify the channels the same in the existing and the new contract periods
    if (!existingContractPeriod.getChannelId().equals(contractPeriod.getChannelId())) {
      throw new InvalidContractPeriodException("A contract period's channel cannot be modified.",
          INVALID_CONTRACT_PERIOD);
    }

    //if the start date has changed, it should not have been changed to the past
    DateTime existingStartDate = new DateTime(existingContractPeriod.getStart().getTime());
    if (!existingStartDate.equals(startDate) && startDate.isBefore(midnightLastNight)) {
      throw new InvalidContractPeriodException("Contract period start date cannot be modified for an active contract "
          + "period.", INVALID_CONTRACT_PERIOD_START);
    }

    // is the end date before the start date?
    if (endDate.isBefore(startDate)) {
      throw new InvalidContractPeriodException("End date is before the start date.", INVALID_CONTRACT_PERIOD);
    }

    //does the updated contract period overlap an existing contract period?
    boolean contractPeriodOverlaps =
        contractPeriodDbDao.contractPeriodOverlapsExisting(contractPeriod);
    if (contractPeriodOverlaps) {
      throw new OverlappingContractPeriod("Contract period overlaps an existing contract period.",
          OVERLAPPING_CONTRACT_PERIOD);
    }

  }

  /**
   * Assert the end date of this contract period
   *
   * @param contractPeriod to be validated for correct start date
   */
  private void assertEndDate(final ContractPeriod contractPeriod) {
    DateTime midnightLastNight = new DateTime().withTimeAtStartOfDay();
    DateTime endDate = new DateTime(contractPeriod.getEnd().getTime());

    //is the end date before today
    boolean contractBeforeToday = endDate.isBefore(midnightLastNight);
    if (contractBeforeToday) {
      throw new InvalidContractPeriodException("Contract period end date is in the past.",
          INVALID_CONTRACT_PERIOD_END);
    }
  }
}
