/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionProcessException.java 91249 2015-03-06 16:38:40Z ssarraj $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError.SubscriptionErrorCode;

/**
 * Indicates the subscription processing has failed.
 */
@SuppressWarnings("serial")
public class SubscriptionProcessException extends ApplicationException {

  private static final String ERROR_SUBSCRIPTION_MESSAGE = "Error while processing the subscription.";

  private User user;

  private Subscription subscription;

  private final SubscriptionErrorCode errorCode;

  /**
   * @param user user in error
   * @param errorCode the error code
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public SubscriptionProcessException(final User user, final SubscriptionErrorCode errorCode) {
    super(ERROR_SUBSCRIPTION_MESSAGE);
    this.user = user;
    this.errorCode = errorCode;
  }

  /**
   * @param subscription subscription in error
   * @param errorCode the error code
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public SubscriptionProcessException(final Subscription subscription, final SubscriptionErrorCode errorCode) {
    super(ERROR_SUBSCRIPTION_MESSAGE);
    this.subscription = subscription;
    this.errorCode = errorCode;
  }

  /**
   * @param errorCode the error code
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public SubscriptionProcessException(final SubscriptionErrorCode errorCode) {
    super(ERROR_SUBSCRIPTION_MESSAGE);
    this.errorCode = errorCode;
  }

  public User getUser() {
    return user;
  }

  public Subscription getSubscription() {
    return subscription;
  }

  public SubscriptionErrorCode getErrorCode() {
    return errorCode;
  }

}
