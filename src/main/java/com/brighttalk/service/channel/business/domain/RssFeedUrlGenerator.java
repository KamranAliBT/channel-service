/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RssFeedUrlGenerator.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelUrlGenerator;

/**
 * Implementation of a URl Generator that generates The RSS feed URL for a channel.
 */
public class RssFeedUrlGenerator implements ChannelUrlGenerator {

  private String channelFeedRssUriTemplate;
  
  /**
   * This template is used to create channel rss feed url and is set when 
   * the bean is instantiated in Spring config.
   * 
   * @return uri template
   */
  public String getChannelFeedRssUriTemplate() {
    return channelFeedRssUriTemplate;
  }

  public void setChannelFeedRssUriTemplate(String channelFeedRssUriTemplate) {
    this.channelFeedRssUriTemplate = channelFeedRssUriTemplate;
  }

  /** {@inheritDoc} */
  @Override
  public String generate(Channel channel) {
    return channelFeedRssUriTemplate.replace("{channelId}", channel.getId().toString() );
  }

}
