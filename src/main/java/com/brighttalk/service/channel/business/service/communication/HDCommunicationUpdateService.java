/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationScheduledPublication;
import com.brighttalk.service.channel.business.domain.validator.CommunicationScheduledPublicationValidator;
import com.brighttalk.service.channel.business.domain.validator.CommunicationUpdateValidator;
import com.brighttalk.service.channel.business.error.InvalidCommunicationException;

/**
 * Implementation of the {@link CommunicationUpdateService} that manages the updating (including rescheduling) of High
 * Definition (aka Pro) webcasts.
 * 
 * Note: "@Primary" Indicates that a bean should be given preference when multiple candidates are qualified to autowire
 * a single-valued dependency.
 * 
 * "@Transaction" Indicates that if the DAO methods throw an Exception, the inserted data will be roll backed to the
 * previous state. Propagation.SUPPORTS means that if a transaction exist it'll be supported else the method will be
 * executed as non transactional.
 */
@Service(value = "hdcommunicationupdateservice")
@Primary
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class HDCommunicationUpdateService extends AbstractHDCommunicationService implements CommunicationUpdateService {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(HDCommunicationUpdateService.class);

  /** Communication syndication service. */
  @Autowired
  protected CommunicationSyndicationService communicationSyndicationService;

  @Autowired
  @Value("${hd.time.to.resource.allocation}")
  protected Long timeToResourceAllocation;

  @Autowired
  @Value("${hd.reschedule.window}")
  protected Long rescheduleWindow;

  /**
   * {@inheritDoc}
   * 
   * "@Transactional" means that the method will be executed transactionally. If an Exception is being thrown, all the
   * changes made will be reverted to a previous known state. Propagation.REQUIRED means that the transaction is always
   * required for this method. The Rollback will happen when an exception has been caught.
   */
  @Override
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
  public Long update(final User user, final Channel channel, final Communication communicationToBeUpdated) {

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Received request to update HD webcast [" + communicationToBeUpdated.getId() + "] in channel ["
          + communicationToBeUpdated.getChannelId() + "].");
    }

    this.validateCommunicationIsBrighTALKHD(communicationToBeUpdated);

    Communication existingCommunication = communicationFinder.find(channel, communicationToBeUpdated.getId());

    assertActiveAsset(existingCommunication);
    assertCanBeUpdated(existingCommunication);
    assertProviderHasNotChanged(communicationToBeUpdated, existingCommunication);

    // if the communication has date and time
    if (communicationToBeUpdated.hasScheduledDateTime()) {

      // see if they have changed values
      existingCommunication.setHasBeenRescheduled(communicationToBeUpdated);

      // validate if the new scheduled date/time is in the reschedule time frame
      validateRescheduleTimeFrame(existingCommunication, communicationToBeUpdated);

      // set exclude from channel content plan
      // if user is manager use the value from the request else use the value from the loaded communication
      boolean excludeFromChannelContentPlan = getExcludeFromContentPlanSetting(user, communicationToBeUpdated,
          existingCommunication);
      existingCommunication.getConfiguration().setExcludeFromChannelContentPlan(excludeFromChannelContentPlan);
    }

    // set the publish status is different property
    setPublishStatusIsDifferent(existingCommunication, communicationToBeUpdated);

    // Merge the communication from the request and the database
    existingCommunication.merge(communicationToBeUpdated);

    // Assert the supplied communication content plan.
    contentPlan.assertForUpdate(user, channel, existingCommunication);

    Long communicationId = null;

    // do the update
    if (existingCommunication.getConfiguration().getCommunicationSyndication().isSyndicatedIn()) {
      communicationId = updateSyndicatedCommunication(user, channel, existingCommunication);
    } else {
      communicationId = updateMasterCommunication(user, channel, existingCommunication);
    }

    // Update the supplied communication categories.
    updateCategories(communicationToBeUpdated);

    LOGGER.info("Communication [" + communicationId + "] successfully updated.");

    return communicationId;
  }

  private void validateRescheduleTimeFrame(final Communication existingCommunication,
    final Communication communicationToBeUpdated) {
    LOGGER.info("Validating communication [" + communicationToBeUpdated.getId() + "] is in reschedule timeframe.");

    if (!existingCommunication.isRescheduled()) {
      return;
    }
    LOGGER.debug("Checking communication [" + communicationToBeUpdated.getId()
        + "] is in resource allocation window interval.");

    // Get time now in seconds
    Long nowInSecs = new DateTime().getMillis() / 1000;

    // Get the existing scheduled time in seconds
    Long existingScheduledTimeInSecs = existingCommunication.getScheduledDateTime().getTime() / 1000;
    // Get the resource allocation time: scheduled time minus time to resource allocation in seconds
    Long resourceAllocationTime = existingScheduledTimeInSecs - timeToResourceAllocation;

    // check if now is in the interval between the existing schedule time -
    boolean isInTimeToResourceAllocationInterval = new Interval(resourceAllocationTime, existingScheduledTimeInSecs).withEndMillis(
        existingScheduledTimeInSecs + 1).contains(nowInSecs);

    if (isInTimeToResourceAllocationInterval) {
      LOGGER.info("Communication [" + communicationToBeUpdated.getId()
          + "] is in time to resource allocation interval.");

      LOGGER.debug("Checking communication [" + communicationToBeUpdated.getId()
          + "] is in time to reschedule window interval.");

      Date newScheduledDateTime = communicationToBeUpdated.getScheduledDateTime();
      Long newScheduledDateTimeInSeconds = newScheduledDateTime.getTime() / 1000;

      Long nowPlus40Mins = nowInSecs + rescheduleWindow;

      boolean isInRescheduleWindowInterval = new Interval(nowInSecs, nowPlus40Mins).withEndMillis(nowPlus40Mins + 1).contains(
          newScheduledDateTimeInSeconds);

      if (isInRescheduleWindowInterval) {
        throw new InvalidCommunicationException("Communication [" + communicationToBeUpdated.getId()
            + "] has an invalid rescheduled time.",
            InvalidCommunicationException.ErrorCode.InvalidRescheduleTime.toString());
      }
    }
    LOGGER.info("Communication [" + communicationToBeUpdated.getId() + "] is in the correct reschedule timeframe.");
  }

  /**
   * {@inheritDoc}
   * 
   * "@Transactional" means that the method will be executed transactionally. If an Exception is being thrown, all the
   * changes made will be reverted to a previous known state. Propagation.REQUIRED means that the transaction is always
   * required for this method. The Rollback will happen when an exception has been caught.
   */
  @Override
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
  public Long update(final Channel channel, final Communication communicationToBeUpdated) {

    LOGGER.info("Internal update of HD Communication [" + communicationToBeUpdated.getId() + "]");

    // validate that the communication is video
    this.validateCommunicationIsBrighTALKHD(communicationToBeUpdated);

    assertActiveAsset(communicationToBeUpdated);
    assertCanBeUpdated(communicationToBeUpdated);

    if (communicationToBeUpdated.hasStatus() && communicationToBeUpdated.isProcessingFailed()) {
      lesQueuer.videoUploadError(communicationToBeUpdated.getId());
      return null;
    }

    Communication updatedCommunication = communicationDbDao.update(communicationToBeUpdated);

    if (updatedCommunication.hasStatus() && updatedCommunication.isRecorded()) {
      communicationDbDao.updateEncodingCount(updatedCommunication.getId());
    }

    manageRenditions(communicationToBeUpdated);

    manageEmails(communicationToBeUpdated);

    return updatedCommunication.getId();
  }

  /**
   * Manage the communication renditions if needed.
   * 
   * @param communication containing the renditions to be updated
   */
  private void manageRenditions(final Communication communication) {
    if (communication.hasRenditions()) {
      // disable the rendition asset
      renditionDbDao.removeRenditionAssets(communication.getId());

      // find all existing renditions
      List<Rendition> oldRenditions = renditionDbDao.findByCommunicationId(communication.getId());

      // delete all renditions individually
      for (Rendition oldRendition : oldRenditions) {
        renditionDbDao.delete(oldRendition.getId());
      }

      // add all renditions and asset
      List<Rendition> newRenditions = communication.getRenditions();
      for (Rendition newRendition : newRenditions) {
        renditionDbDao.create(newRendition);
        renditionDbDao.addRenditionAsset(communication.getId(), newRendition.getId());
      }
    }
  }

  /**
   * Manage the emails that needs sending at different state.
   * 
   * @param communication for which emails need to be sent.
   */
  private void manageEmails(final Communication communication) {
    if (communication.hasStatus() && communication.isRecorded()) {
      lesQueuer.videoUploadComplete(communication.getId());

      // only send for the first successful encoding, if the encoding count
      // is greater than 0, it has been successfully encoded at least once already
      if (communication.getEncodingCount() < 1) {

        emailQueuer.recordingPublished(communication.getId());

        if (communication.isPublished()) {
          emailQueuer.missedYou(communication.getId());
        }
      }
    }

  }

  @Override
  public void updateStatus(final Channel channel, final Communication communicationToBeUpdated) {
    LOGGER.info("Received request to update status of webcast [" + communicationToBeUpdated.getId() + "] in channel ["
        + communicationToBeUpdated.getChannelId() + "].");

    this.validateCommunicationIsBrighTALKHD(communicationToBeUpdated);

    Communication existingCommunication = communicationFinder.find(channel, communicationToBeUpdated.getId());

    assertActiveAsset(existingCommunication);
    assertCanBeUpdated(existingCommunication);
    assertProviderHasNotChanged(communicationToBeUpdated, existingCommunication);

    LOGGER.info("Updating communication [" + existingCommunication.getId() + "] status from ["
        + existingCommunication.getStatus() + "] to [" + communicationToBeUpdated.getStatus() + "].");

    // Merge the communication from the request and the database
    existingCommunication.merge(communicationToBeUpdated);

    // do the update
    Communication updatedCommunication = communicationDbDao.update(existingCommunication);

    // When the webcast is set to processing, inform the notification service.
    if (updatedCommunication.isProcessing() && isNotificationServiceEnabled()) {
      notificationQueuer.inform(updatedCommunication.getId());
    }

    LOGGER.info("Status of communication [" + updatedCommunication.getId() + "] has been successfully updated to ["
        + updatedCommunication.getStatus() + "].");
  }

  @Override
  public void updateFeatureImage(final Communication communication) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Update the feature image of webcast [" + communication.getId() + "].");
    }

    // do the update
    Communication updatedCommunication = communicationDbDao.update(communication);

    LOGGER.debug("Communication [" + updatedCommunication.getId() + "] successfully updated.");
  }

  /**
   * Set the publishStatusIsDifferent property for the given communication.
   * 
   * if the given publish status is different than the one stored in database we set the property to true else false
   * 
   * @param existingCommunication containing the existing informations
   * @param communicationToBeUpdated containing the new informations
   */
  private void setPublishStatusIsDifferent(final Communication existingCommunication,
    final Communication communicationToBeUpdated) {

    boolean publishStatusIsDifferent = false;

    if (communicationToBeUpdated.hasPublishStatus() && existingCommunication.isRecorded()
        && !existingCommunication.getPublishStatus().name().equals(communicationToBeUpdated.getPublishStatus().name())) {
      publishStatusIsDifferent = true;
    }
    existingCommunication.setPublishStatusHasChanged(publishStatusIsDifferent);
  }

  /**
   * Update the master communication.
   * 
   * @param user performing the update action.
   * @param channel in which the update needs to happen.
   * @param communication to be updated.
   * @return the updated communication id.
   */
  private Long updateMasterCommunication(final User user, final Channel channel, final Communication communication) {

    LOGGER.debug("Updating master communication [" + communication.getId() + "].");

    // do validation
    validateForReschedule(channel, communication);
    assertScheduledPublicationDates(communication);

    // Add quotes to communication keywords.
    addQuotesToKeywords(communication);

    // do the update
    Communication updatedCommunication = communicationDbDao.update(communication);
    updateCommunicationConfiguration(communication);
    updateScheduledPublicationDates(communication);

    // Get the live phone number and update the newly created communication
    if (communication.isRescheduled()) {
      String livePhoneNumber = hDBookingServiceDao.updateBooking(communication);
      communication.setLivePhoneNumber(livePhoneNumber);
      communicationDbDao.updateLivePhoneNumber(communication);
    }

    // let other relevant services know the update has happened
    informOtherServicesOfUpdate(communication);

    return updatedCommunication.getId();
  }

  /**
   * Update the syndicated communication.
   * 
   * @param user performing the update action.
   * @param channel in which the update needs to happen.
   * @param communication to be updated.
   * @return the updated communication id.
   */
  private Long updateSyndicatedCommunication(final User user, final Channel channel, final Communication communication) {

    LOGGER.debug("Updating communication [" + communication.getId() + "] syndicated into channel [" + channel.getId()
        + "].");

    addQuotesToKeywords(communication);

    communicationSyndicationService.update(user, channel, communication);

    return communication.getId();
  }

  /**
   * Inform other services there has been an update to a communication.
   * 
   * @param communication the communication created from the request.
   */
  private void informOtherServicesOfUpdate(final Communication communication) {
    LOGGER.info("Inform other service that communication [" + communication.getId() + "] has been updated.");

    // If a HD communication has been rescheduled then the live event service needs to know so it can update its state
    // and reshedule emails need sent to the audience
    if (communication.isRescheduled()) {
      lesQueuer.reschedule(communication.getId());
      emailQueuer.reschedule(communication.getId());
    }

    // if the communication has been rescheduled or if its publish status has changed
    // the summit service need to know so it can change its duration (reshedule) or show/hide the communciation (publish
    // status)
    if (communication.isRescheduled() || communication.hasPublishStatusChanged()) {
      summitQueuer.reschedule(communication.getId());
    }

    if (communication.hasPreviewUrl()) {
      imageConverterQueuer.inform(communication.getId());
    }

  }

  /**
   * Update the communication configuration in the database.
   * 
   * @param communication the communication created based on the communication from the request.
   */
  private void updateCommunicationConfiguration(final Communication communication) {
    // Update the supplied communication configuration.
    if (communication.hasConfiguration()) {
      CommunicationConfiguration configuration = communication.getConfiguration();
      configuration.setId(communication.getId());
      configurationDbDao.update(communication.getConfiguration());
    }
  }

  /**
   * Assert the scheduled publication dates are in the correct state.
   * 
   * @param communication the communication to assert the dates on.
   * @throws InvalidCommunicationException if the assertion fails.
   */
  private void assertScheduledPublicationDates(final Communication communication) {
    // assert scheduled publication rules
    if (communication.hasScheduledPublication()) {
      communication.getScheduledPublication().validate(new CommunicationScheduledPublicationValidator(communication));
    }
  }

  /**
   * Validate the communication does not break any business rules if has been rescheduled.
   * 
   * @param channel the channel to check.
   * @param communication the communication to check.
   * @throws InvalidCommunicationException if the rules are broken.
   */
  private void validateForReschedule(final Channel channel, final Communication communication) {
    // check if the communication to be updated overlaps with another one in the channel.
    if (communication.isRescheduled()) {
      boolean isCommunicationOverlapping = communicationDbDao.isOverlapping(communication);
      communication.validate(new CommunicationUpdateValidator(channel, isCommunicationOverlapping));
    }
  }

  /**
   * Assert the provider has not changed before we do an update. You cannot change a communications provider except
   * through pivoting it.
   * 
   * @param newCommunication the communication to be updated
   * @param existingCommunication the existing communication.
   * @throws InvalidCommunicationException
   */
  private void assertProviderHasNotChanged(final Communication newCommunication,
    final Communication existingCommunication) {
    existingCommunication.hasProviderChanged(newCommunication);
  }

  /**
   * Work out and return the content plan exclusion setting. If the user is a manager use the value from the request
   * otherwise use the existing setting.
   * 
   * @param user the current user doing the update.
   * @param newCommunication the communication to be updated
   * @param existingCommunication the existing version of the communication
   * @return <code>true</code> if the communication is to be excluded from the content plan or <code>false</code>
   */
  private boolean getExcludeFromContentPlanSetting(final User user, final Communication newCommunication,
    final Communication existingCommunication) {
    boolean excludeFromChannelContentPlan = user.isManager() ? newCommunication.getConfiguration().excludeFromChannelContentPlan()
        : existingCommunication.getConfiguration().excludeFromChannelContentPlan();
    return excludeFromChannelContentPlan;
  }

  /**
   * Assert the communication is an active asset of its channel.
   * 
   * @param existingCommunication
   * @throw InvalidCommunicationException if it is not.
   */
  private void assertActiveAsset(final Communication existingCommunication) {
    // verify that the communication is an active channel asset
    if (!communicationDbDao.isActiveAsset(existingCommunication)) {
      throw new InvalidCommunicationException("Communication [" + existingCommunication.getId()
          + "] is not an active asset of channel [" + existingCommunication.getChannelId() + "].",
          InvalidCommunicationException.ERROR_CODE_INVALID_COMMUNICATION);
    }
  }

  /**
   * Update the communication categories.
   * 
   * This will load the categories from the database for the given channel id and communication id It gets the
   * categories to update, the one from the request Creates a new list of categories to be updated in the database.
   * 
   * @param communication in which the categories need to be updated.
   */
  private void updateCategories(final Communication communication) {

    HashMap<Long, CommunicationCategory> existingCategories = categoryDbDao.find(communication.getChannelId(),
        communication.getId());

    List<CommunicationCategory> categoriesToUpdate = communication.getCategories();

    List<CommunicationCategory> categoriesToBeUpdated = getCommunicationCategoriesToBeUpdated(existingCategories,
        categoriesToUpdate);
    communication.setCategories(categoriesToBeUpdated);

    updateCategoriesWithCommunicationId(communication);

    categoryDbDao.update(communication.getCategories());
  }

  /**
   * Update the scheduled publication dates. If the publish status been updated, then we need to de-activate the
   * previously scheduled publication date set. Otherwise, we need to update the scheduled publicationif they were set.
   * 
   * @param communication containing the scheduled publications.
   */
  private void updateScheduledPublicationDates(final Communication communication) {
    if (communication.hasScheduledPublication()) {
      if (communication.hasPublishStatusChanged()) {
        // If the publication status has changed we deactivate the publication date that would
        // have set the communication to the new status.
        scheduledPublicationDbDao.deactivatePublicationDate(communication);
      } else if (communication.hasScheduledPublication()) {
        CommunicationScheduledPublication communicationScheduledPublication = communication.getScheduledPublication();

        if (!communicationScheduledPublication.hasScheduledPublicationDates()) {
          return;
        }

        if (communicationScheduledPublication.isDefaultPublicationDate()
            && communicationScheduledPublication.isDefaultUnpublicationDate()) {
          scheduledPublicationDbDao.deactivatePublicationDate(communication);
        } else {
          scheduledPublicationDbDao.update(communication);
        }
      }
    }
  }

  /**
   * Get the list of categories to be updated from the existing ones and the new ones from the request.
   * 
   * @param existingCommunicationCategories the existing communication categories
   * @param categoriesToBeUpdated the new categories to be updated
   * @return the updated list of categories.
   */
  private List<CommunicationCategory> getCommunicationCategoriesToBeUpdated(
    final HashMap<Long, CommunicationCategory> existingCommunicationCategories,
    final List<CommunicationCategory> categoriesToBeUpdated) {

    for (Entry<Long, CommunicationCategory> e : existingCommunicationCategories.entrySet()) {
      Long id = e.getKey();
      CommunicationCategory existingCommunicationCategory = e.getValue();

      if (categoriesToBeUpdated.contains(existingCommunicationCategory)) {
        // If category has not change then removing it
        categoriesToBeUpdated.remove(id);
      } else {

        existingCommunicationCategory.setIsActive(false);
        categoriesToBeUpdated.add(existingCommunicationCategory);
      }
    }

    return categoriesToBeUpdated;
  }

  /**
   * Set the categories to be updated to the communication object.
   * 
   * @param communication to add categories to.
   */
  private void updateCategoriesWithCommunicationId(final Communication communication) {
    List<CommunicationCategory> categories = new ArrayList<CommunicationCategory>();

    if (!communication.hasCategories()) {
      return;
    }

    for (CommunicationCategory communicationCategory : communication.getCategories()) {
      communicationCategory.setCommunicationId(communication.getId());
      communicationCategory.setChannelId(communication.getChannelId());
      categories.add(communicationCategory);
    }

    communication.setCategories(categories);
  }

  /** {@inheritDoc} */
  @Override
  public void lesRescheduleCallback(final Long webcastId) {
    LOGGER.info("Callback to reschedule webcast [" + webcastId + "] in LES.");

    Communication communication = findCommunication(webcastId);
    liveEventServiceDao.reschedule(communication);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void summitRescheduleCallback(final Long webcastId) {
    LOGGER.info("Callback to reschedule webcast [" + webcastId + "] in Summit.");

    Communication communication = findCommunication(webcastId);
    summitServiceDao.inform(communication);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailRescheduleCallback(final Long webcastId) {
    LOGGER.info("Callback to send reschedule email for HD webcast [" + webcastId + "].");

    List<Communication> communications = getSyndicatedCommunications(webcastId);
    emailServiceDao.sendCommunicationRescheduleConfirmation(communications);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailMissedYouCallback(final Long webcastId) {
    LOGGER.info("Callback to send missed you email for HD webcast [" + webcastId + "].");

    List<Communication> communications = getSyndicatedCommunications(webcastId);

    emailServiceDao.sendCommunicationMissedYouConfirmation(communications);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailRecordingPublishedCallback(final Long webcastId) {
    LOGGER.info("Callback to send recording published email for HD webcast [" + webcastId + "].");

    Communication communication = findCommunication(webcastId);
    emailServiceDao.sendCommunicationRecordingPublishedConfirmation(communication);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailVideoUploadCompleteCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "Email Video Upload Complete Callback method not implemented for HD communication. Webcast id [" + webcastId
            + "]");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailVideoUploadErrorCallback(final Long webcastId) {
    throw new UnsupportedOperationException(
        "Email Video Upload Error Callback method not implemented for HD communication. Webcast id [" + webcastId + "]");
  }

  /**
   * Callback to handle any asynchronous essages to be sent to the Live Event Service (LES) when the webcast encoding
   * has been successful and the webcast has been set to recorded.
   * 
   * @param webcastId id of the recorded webcast.
   */
  @Override
  public void lesVideoUploadCompleteCallback(final Long webcastId) {
    LOGGER.info("Callback to inform LES that encoding of webcast [" + webcastId + "] has been recorded.");

    liveEventServiceDao.recordedSuccess(webcastId);
  }

  /**
   * Callback to handle any asynchronous essages to be sent to the Live Event Service (LES) when the webcast encoding
   * failed.
   * 
   * @param webcastId id of the recorded webcast.
   */
  @Override
  public void lesVideoUploadErrorCallback(final Long webcastId) {
    LOGGER.info("Callback to inform LES that encoding of webcast [" + webcastId + "] has failed.");

    liveEventServiceDao.recordedError(webcastId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void notificationInformCallback(final Long webcastId) {
    LOGGER.info("Callback to inform notification service that webcast [" + webcastId + "] has ended.");

    Communication communication = findCommunication(webcastId);
    notificationServiceDao.inform(communication);
  }
}
