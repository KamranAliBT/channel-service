/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsService.java 95102 2015-05-15 10:07:51Z nbrown $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionsSummary;

/**
 * The Subscriptions Service Business API.
 * <p>
 * The Subscriptions Service is responsible for managing a {@link Subscription} for a channel.
 * <p>
 */
public interface SubscriptionsService {

  /**
   * Add a list of subscriptions to a channel.
   * <p>
   * Before creating a subscription this method validates if:
   * <ul>
   * <li>The supplied subscriptions user element has only id or email address.</li>
   * <li>The supplied subscriptions user element is not blocked in the identified channel.</li>
   * <li>The supplied subscriptions user element is not already subscribed to the identified channel.</li>
   * <li>The supplied subscriptions user element is not already unsubscribed from the identified channel.</li>
   * <li>The supplied subscriptions context element lead context values identify existing summits and communications.</li>
   * <li>The supplied subscriptions context element engagement score values is between 0 and 100.</li>
   * </ul>
   * 
   * @param channelId The Id of the channel where the subscriptions are add to.
   * @param subscriptions The subscriptions to be added.
   * @param dryRun if it's a dry run call.
   * 
   * @return The subscription summary containing list of the created subscriptions and any errors.
   */
  SubscriptionsSummary create(Long channelId, List<Subscription> subscriptions, Boolean dryRun);

  /**
   * Updates a supplied list of (existing) subscriptions.
   * <p>
   * Supports updating a subscription's referral, and adding a subscription context for the subscription (if the 
   * existing subscription doesn't already have a context).
   * <p>
   * The method validates each supplied subscription before applying any updates. The following must hold true, 
   * otherwise the update of the individual subscription will be rejected - 
   * <ul>
   * <li>The subscription ID must identify an existing, _active_ subscription.</li>
   * <li>The supplied subscription can only include a context if the existing subscription doesn't have one.</li> 
   * <li>A supplied subscription context's lead context values must identify existing summits and communications.</li>
   * <li>A supplied subscription context's engagement score values must be between 0 and 100.</li>
   * </ul>
   * 
   * @param subscriptions The list of subscription to update.
   * @param dryRun if it's a dry run call.
   * @return The subscription summary containing list of the updated subscriptions and any errors.
   */
  SubscriptionsSummary update(List<Subscription> subscriptions, Boolean dryRun);

  /**
   * Finds active Subscription by the supplied Id.
   * <p>
   * Before returning the found subscription this method validate if:
   * <ul>
   * <li>The supplied subscription id identify an existing subscription.</li>
   * <li>The found subscription is active.</li>
   * </ul>
   * 
   * @param id The subscription Id to find.
   * @return The subscription summary containing the found subscription and any errors in case of subscription does not
   * identify an existing subscription or the subscription is inactive.
   */
  SubscriptionsSummary findById(Long id);

}