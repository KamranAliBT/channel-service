/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsDeleteChannelListener.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.brighttalk.service.channel.business.service.channel.ChannelService;

/**
 * JMS Implementation of a Message listener to receive message form the Delete Channel queue and use the Service to 
 * delete channel related bookings and communities.
 */
public class JmsDeleteChannelListener implements MessageListener {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JmsDeleteChannelListener.class);
  
  @Autowired
  private ChannelService service;
  
  public ChannelService getService() {
    return service;
  }

  public void setService(ChannelService service) {
    this.service = service;
  }

  /** {@inheritDoc} */
  @Override
  public void onMessage(Message message) {
   
    if (message instanceof ObjectMessage) {
      try {

        Long channelId = (Long) ((ObjectMessage) message).getObject();
        service.deleteCallback( channelId );

      } catch (Exception exception) {
        logger.error("Something went wrong", exception);
      }
    } else {
      logger.error("Delete channel processing broken, because of illegal argument provided. "
          + "Expected [ObjectMessage] got [" + message + "].");
    }
  }

}
