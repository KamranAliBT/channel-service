/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionsSummary.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a subscriptions summary.
 */
public class SubscriptionsSummary {

  private List<Subscription> subscriptions;

  private List<SubscriptionError> errors;

  public List<Subscription> getSubscriptions() {
    return subscriptions;
  }

  public void setSubscriptions(final List<Subscription> subscriptions) {
    this.subscriptions = subscriptions;
  }

  /**
   * Add a subscription to the subscriptions list.
   * 
   * @param subscription the subscription
   */
  public void addSubscription(final Subscription subscription) {
    if (subscriptions == null) {
      subscriptions = new ArrayList<>();
    }
    subscriptions.add(subscription);
  }

  public List<SubscriptionError> getErrors() {
    return errors;
  }

  /**
   * Add a subscription error to the subscriptions error list.
   * 
   * @param error the subscription error
   */
  public void addErrors(final SubscriptionError error) {
    if (errors == null) {
      errors = new ArrayList<>();
    }
    errors.add(error);
  }

  public void setErrors(final List<SubscriptionError> errors) {
    this.errors = errors;
  }

}
