/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationFinder.java 70153 2013-10-23 17:23:51Z acairns $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

/**
 * Default implementation of the {@link TimestampToolbox}.
 */
@Service
public class DefaultTimestampToolbox implements TimestampToolbox {

  /** constant to get 23 hours */
  private static final int TWENTY_THREE_HOURS = 23;

  /** constant to get 59 minutes */
  private static final int FIFTY_NINE_MINUTES = 59;

  /** constant to get 59 seconds */
  private static final int FIFTY_NINE_SECONDS = 59;

  /** constant to get 28 days */
  private static final int TWENTY_EIGHT_DAYS = 28;

  /** constant to get 31 days */
  private static final int THIRTY_ONE_DAYS = 31;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean inFirstMonth(final Date created, final Date scheduled) {

    Date createdAtMidnight = getDateAtMidnight(created);

    // If this communication is scheduled in the past
    if (scheduled.getTime() < createdAtMidnight.getTime()) {
      return false;
    }

    // If this communication is scheduled for more than 31 days in the future
    Date thirtyOneDays = addDays(createdAtMidnight, THIRTY_ONE_DAYS);
    if (scheduled.getTime() >= thirtyOneDays.getTime()) {
      return false;
    }

    // If this communication is scheduled for less than 28 days in the future
    Date twentyEightDays = addDays(createdAtMidnight, TWENTY_EIGHT_DAYS);
    if (scheduled.getTime() < twentyEightDays.getTime()) {
      return true;
    }

    Date firstMonthBoundary = addMonths(createdAtMidnight, 1);

    return firstMonthBoundary.getTime() >= scheduled.getTime();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public HashMap<String, Date> getPeriod(final Date created, final Date scheduled, final int periodInMonths) {
    Date createdDateAtMidnight = getDateAtMidnight(created);

    if (scheduled.getTime() < createdDateAtMidnight.getTime()) {
      return getPastPeriod(createdDateAtMidnight, scheduled, periodInMonths);
    }

    return getFuturePeriod(createdDateAtMidnight, scheduled, periodInMonths);
  }

  /**
   * This will return the start and end points of the time in future period.
   * 
   * @param created the created timestamp of the channel
   * @param scheduled the scheduled timestamp of the communication
   * @param periodInMonths the number of months a period is
   * @return HashMap<String, Date> The time period details - ('start' => int, 'end' => int)
   */
  private HashMap<String, Date> getFuturePeriod(final Date created, final Date scheduled, final int periodInMonths) {
    HashMap<String, Date> period = new HashMap<String, Date>();

    Date periodStart = addMonths(created, 1);

    Date periodEnd = addMonths(created, periodInMonths + 1);

    while (scheduled.getTime() > periodEnd.getTime()) {
      periodStart = addMonths(periodStart, periodInMonths);
      periodEnd = addMonths(periodEnd, periodInMonths);
    }

    period.put(START_PERIOD, periodStart);
    period.put(END_PERIOD, addSeconds(periodEnd, -1));

    return period;
  }

  /**
   * This will return the start and end points of the time in past period.
   * 
   * @param created the created timestamp of the channel
   * @param scheduled the scheduled timestamp of the communication
   * @param periodInMonths the number of months a period is
   * @return HashMap<String, Date> The time period details - ('start' => int, 'end' => int)
   */
  private HashMap<String, Date> getPastPeriod(final Date created, final Date scheduled, final int periodInMonths) {
    HashMap<String, Date> period = new HashMap<String, Date>();

    Date periodStart = addMonths(created, -periodInMonths);
    Date periodEnd = addSeconds(created, -1);

    while (scheduled.getTime() < periodStart.getTime()) {
      periodStart = addMonths(periodStart, -periodInMonths);
      periodEnd = addMonths(periodEnd, -periodInMonths);
    }

    period.put(START_PERIOD, periodStart);
    period.put(END_PERIOD, periodEnd);

    return period;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public HashMap<String, Date> getFirstMonthPeriod(final Date created) {
    HashMap<String, Date> period = new HashMap<String, Date>();

    Date startPeriod = getDateAtMidnight(created);

    Date endPeriod = new Date(addMonths(startPeriod, 1).getTime() - 1);

    period.put(START_PERIOD, startPeriod);
    period.put(END_PERIOD, endPeriod);

    return period;
  }

  @Override
  public Date addSeconds(final Date date, final int numberOfSeconds) {

    DateTime dateTime = new DateTime(date);

    return dateTime.plusSeconds(numberOfSeconds).toDate();
  }

  @Override
  public Date addDays(final Date date, final int numberOfDays) {

    DateTime dateTime = new DateTime(date);

    return dateTime.plusDays(numberOfDays).toDate();
  }

  @Override
  public Date addMonths(final Date date, final int numberOfMonths) {

    DateTime initialDateTime = new DateTime(date);

    int day = initialDateTime.getDayOfMonth();

    DateTime dateTime = initialDateTime.plusMonths(numberOfMonths);

    // get the number of days in the given month
    int numberOfDaysInMonth = getNumberOfDaysInAMonth(dateTime.getMonthOfYear(), dateTime.getYear());

    if (day > numberOfDaysInMonth) {
      return initialDateTime.plusMonths(numberOfMonths).withTime(TWENTY_THREE_HOURS, FIFTY_NINE_MINUTES,
          FIFTY_NINE_SECONDS, 0).toDate();
    }

    return initialDateTime.plusMonths(numberOfMonths).toDate();
  }

  /**
   * get the number of days in a given month and year
   * 
   * @param initialMonth
   * @param year
   * @return Number of days in one month.
   */
  private int getNumberOfDaysInAMonth(final int initialMonth, final int year) {
    Calendar calendar = Calendar.getInstance();
    int date = 1;
    int month = initialMonth - 1; // month starts at 0 for Jan
    calendar.set(year, month, date);
    int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    return days;
  }

  /**
   * Get midnight from the given date
   * 
   * @param date the date to get at midnight
   * @return the new date at midnight
   */
  private Date getDateAtMidnight(final Date date) {

    DateTime dateTime = new DateTime(date);

    return dateTime.toDateMidnight().toDate();
  }

}