/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultResourceService.java 78460 2014-05-23 10:58:44Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.validator.CommunicationResourceAccessValidator;
import com.brighttalk.service.channel.business.domain.validator.CommunicationResourceModificationValidator;
import com.brighttalk.service.channel.business.domain.validator.CommunicationResourceOrderingValidator;
import com.brighttalk.service.channel.business.domain.validator.ResourceValidator;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.business.service.channel.DefaultChannelService;
import com.brighttalk.service.channel.business.service.communication.CommunicationFinder;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;

/**
 * Default implementation of the {@link ResourceService}.
 */
@Service
@Primary
public class DefaultResourceService implements ResourceService {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(DefaultChannelService.class);

  /** DB DAO for resources (aka attachments) */
  @Autowired
  private ResourceDbDao resourceDbDao;

  /** DB DAO for communications */
  @Autowired
  private CommunicationFinder communicationFinder;

  /** {@inheritDoc} */
  @Override
  public Resource findResource(final User user, final Channel channel, final Long communicationId, final Long resourceId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Find resource [" + resourceId + "] for channel [" + channel.getId() + "] and communication ["
          + communicationId + "]");
    }

    Communication communication = communicationFinder.find(channel, communicationId);

    assertUserAccessForCommunication(user, channel, communication);
    communication.validate(new CommunicationResourceAccessValidator());

    Resource resource = communication.getResource(resourceId);
    return resource;
  }

  /** {@inheritDoc} */
  @Override
  public Resource createResource(final User user, final Channel channel, final Long communicationId,
      final Resource resource) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Create resource for channel [" + channel.getId() + "] and communication [" + communicationId + "]");
    }

    Communication communication = communicationFinder.find(channel, communicationId);

    assertUserAccessForCommunication(user, channel, communication);
    communication.validate(new CommunicationResourceModificationValidator());

    // In order to add a new resource at the end of resource list the precedence is set to the size of resource list.
    resource.setPrecedence(communication.getResources().size());
    Resource createdResource = resourceDbDao.create(communicationId, resource);

    return createdResource;
  }

  /** {@inheritDoc} */
  @Override
  public Resource updateResource(final Resource resource) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Update resource [" + resource.getId() + "].");
    }

    Resource existingResource = resourceDbDao.find(resource.getId());

    resource.validate(new ResourceValidator(existingResource));

    existingResource.merge(resource);

    Resource resourceToUpdate = resourceDbDao.update(existingResource);

    return resourceToUpdate;
  }

  /** {@inheritDoc} */
  @Override
  public Resource updateResource(final User user, final Channel channel, final Long communicationId,
      final Resource resource) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Edit resource [" + resource.getId() + "] for channel [" + channel.getId() + "] and communication ["
          + communicationId + "]");
    }

    Communication communication = communicationFinder.find(channel, communicationId);

    assertUserAccessForCommunication(user, channel, communication);

    communication.validate(new CommunicationResourceModificationValidator());
    Resource resourceToUpdate = communication.getResource(resource.getId());

    resource.validate(new ResourceValidator(resourceToUpdate));

    resourceToUpdate.merge(resource);
    resourceToUpdate = resourceDbDao.update(resourceToUpdate);

    return resourceToUpdate;
  }

  /** {@inheritDoc} */
  @Override
  public void deleteResource(final User user, final Channel channel, final Long communicationId, final Long resourceId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleting resource [" + resourceId + "] forchannel [" + channel.getId() + "] and communication ["
          + communicationId + "].");
    }

    Communication communication = communicationFinder.find(channel, communicationId);
    assertUserAccessForCommunication(user, channel, communication);
    communication.validate(new CommunicationResourceModificationValidator());
    if (communication.hasResource(resourceId)) {
      resourceDbDao.delete(communicationId, resourceId);
    } else {
      throw new NotFoundException("Resource[" + resourceId + "] does not belong to the communication.");
    }
  }

  /** {@inheritDoc} */
  @Override
  public List<Resource> findResources(final User user, final Channel channel, final Long communicationId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Gets resources for channel [" + channel.getId() + "] and communication [" + communicationId + "].");
    }

    Communication communication = communicationFinder.find(channel, communicationId);
    assertUserAccessForCommunication(user, channel, communication);
    communication.validate(new CommunicationResourceAccessValidator());

    return communication.getResources();
  }

  /** {@inheritDoc} */
  @Override
  public List<Resource> updateResourcesOrder(final User user, final Channel channel, final Long communicationId,
      final List<Resource> resources) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Update resources order for channel [" + channel.getId() + "] and communication [" + communicationId
          + "]. Resources [" + resources + "]");
    }

    Communication communication = communicationFinder.find(channel, communicationId);
    assertUserAccessForCommunication(user, channel, communication);

    communication.validate(new CommunicationResourceOrderingValidator(resources));

    resourceDbDao.updateOrder(resources);
    communication = communicationFinder.find(channel, communicationId);

    return communication.getResources();
  }

  /**
   * Assert the given user has permissions to do work on resources.
   * 
   * @param user the user
   * @param channel the channel to be verified
   * @param communication the communication to be verified
   * 
   * @throws UserAuthorisationException if the given user is not authorized to access this channel or communication.
   */
  private void assertUserAccessForCommunication(final User user, final Channel channel,
      final Communication communication) {
    if (user.isManager() || communication.isPresenter(user) || channel.isChannelOwner(user)
        || channel.isChannelManager(user)) {
      return;
    }

    throw new UserAuthorisationException("User [" + user.getId() + "] is not authorized to access "
        + "communication id [" + communication.getId() + "] " + "in channel id [" + channel.getId() + "].");
  }

}