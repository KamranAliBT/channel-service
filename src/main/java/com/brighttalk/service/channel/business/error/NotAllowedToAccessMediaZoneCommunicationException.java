/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: NotAllowedToAccessMediaZoneCommunicationException.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates a resource is trying to be accessed on MediaZone communication.
 * 
 */
@SuppressWarnings("serial")
public class NotAllowedToAccessMediaZoneCommunicationException extends ApplicationException {

  private static final String ERROR_CODE_NOT_ALLOWED_TO_ACCESS_MEDIAZONE_ERROR = "NotAllowedToAccessMediaZoneCommunication";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_NOT_ALLOWED_TO_ACCESS_MEDIAZONE_ERROR}.
   */
  private String errorCode = ERROR_CODE_NOT_ALLOWED_TO_ACCESS_MEDIAZONE_ERROR;

  /**
   * @param userErrorMessage see below.
   * @param message see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public NotAllowedToAccessMediaZoneCommunicationException(String message, String userErrorMessage) {
    super(message, ERROR_CODE_NOT_ALLOWED_TO_ACCESS_MEDIAZONE_ERROR);
    this.setUserErrorMessage(userErrorMessage);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public NotAllowedToAccessMediaZoneCommunicationException(String message) {
    super(message, ERROR_CODE_NOT_ALLOWED_TO_ACCESS_MEDIAZONE_ERROR);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

}
