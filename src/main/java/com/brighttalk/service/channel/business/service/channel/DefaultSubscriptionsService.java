/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: DefaultSubscriptionsService.java 95321 2015-05-19 14:59:11Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.List;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError.SubscriptionErrorCode;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionsSummary;
import com.brighttalk.service.channel.business.domain.extractor.SubscriptionsElementExtractor;
import com.brighttalk.service.channel.business.error.SubscriptionNotFoundException;
import com.brighttalk.service.channel.business.error.SubscriptionProcessException;
import com.brighttalk.service.channel.business.error.UserNotFoundException;
import com.brighttalk.service.channel.business.filter.UserFilter;
import com.brighttalk.service.channel.integration.communication.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.database.SubscriptionContextDbDao;
import com.brighttalk.service.channel.integration.database.SubscriptionDbDao;
import com.brighttalk.service.channel.integration.summit.dto.SummitDto;
import com.brighttalk.service.channel.integration.user.UserServiceDao;

/**
 * Default implementation of the {@link SubscriptionsService}.
 */
@Service
public class DefaultSubscriptionsService implements SubscriptionsService {

  private static final Logger LOGGER = Logger.getLogger(DefaultSubscriptionsService.class);

  @Autowired
  @Qualifier("localUserServiceDao")
  private UserServiceDao userServiceDao;

  @Autowired
  private SubscriptionDbDao subscriptionDbDao;

  @Autowired
  private SubscriptionContextDbDao subscriptionContextDbDao;

  @Autowired
  private UserFilter userFilter;

  @Autowired
  private ChannelFinder channelFinder;

  @Autowired
  private SubscriptionValidator validator;

  @Autowired
  private SubscriptionsElementExtractor extractor;

  /** {@inheritDoc} */
  @Override
  public SubscriptionsSummary create(final Long channelId, final List<Subscription> subscriptions,
      final Boolean dryRun) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Creating a list of subscriptions [" + subscriptions + "] for the channel [" + channelId
          + "] and dry run flag [" + dryRun + "].");
    }
    Channel channel = channelFinder.find(channelId);

    SubscriptionsSummary summary = new SubscriptionsSummary();
    List<User> users = extractor.extractUsers(subscriptions);
    List<SummitDto> summits = extractor.extractLeadContextSummits(subscriptions);
    List<CommunicationDto> communications = extractor.extractLeadContextCommunications(subscriptions);

    for (Subscription subscription : subscriptions) {
      SubscriptionError error = validateSubscriptionForCreate(subscription, channel, users, summits, communications);

      // Create the supplied subscription if there are no errors and it is not a dry run request.
      if (error == null) {
        Subscription createdSubscription = processSubscriptionForCreate(subscription, channel, dryRun);
        summary.addSubscription(createdSubscription);
      } else {
        summary.addErrors(error);
      }
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully created the identified subscriptions [" + subscriptions + "].");
    }
    return summary;
  }

  /** {@inheritDoc} */
  @Override
  public SubscriptionsSummary update(final List<Subscription> subscriptions, final Boolean dryRun) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updating the supplied list of subscriptions [" + subscriptions
          + "] and dry run flag [" + dryRun + "].");
    }

    SubscriptionsSummary summary = new SubscriptionsSummary();
    List<SummitDto> summits = extractor.extractLeadContextSummits(subscriptions);
    List<CommunicationDto> communications = extractor.extractLeadContextCommunications(subscriptions);

    for (Subscription subscription : subscriptions) {
      SubscriptionError error = validateSubscriptionForUpdate(subscription, summits, communications);

      // Update subscription referral and create context if there are no errors and it is not a dry run update.
      if (error == null) {
        processSubscriptionForUpdate(subscription, dryRun);
        summary.addSubscription(subscription);
      } else {
        summary.addErrors(error);
      }
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully Updated the identified subscriptions [" + subscriptions + "].");
    }
    return summary;
  }

  /** {@inheritDoc} */
  @Override
  public SubscriptionsSummary findById(final Long id) {
    Validate.notNull(id, "The supplied subscription id must not be null.");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding subscription by id [" + id + "].");
    }
    SubscriptionsSummary summary = new SubscriptionsSummary();
    SubscriptionError error = new SubscriptionError();
    Subscription foundSubscription = null;

    try {
      foundSubscription = subscriptionDbDao.findById(id);
      summary.addSubscription(foundSubscription);
    } catch (SubscriptionNotFoundException e) {
      error.addErrorCode(SubscriptionErrorCode.SUBSCRIPTION_NOT_FOUND);
      summary.addErrors(error);
    }

    // Check if the found subscription is active.
    if (foundSubscription != null) {
      try {
        validator.assertSubscriptionActive(foundSubscription);
      } catch (SubscriptionProcessException e) {
        error.addErrorCode(e.getErrorCode());
        error.setSubscription(e.getSubscription());
        summary.addErrors(error);
      }
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully found subscription [" + foundSubscription + "].");
    }
    return summary;
  }

  /**
   * Finds the user details of the supplied subscription.
   * 
   * @param subscription the subscription.
   * @return the found user.
   */
  private User findUser(final Subscription subscription) {
    Long userId = subscription.getUser().getId();
    User foundUser = userServiceDao.find(userId);
    subscription.setUser(foundUser);
    return foundUser;
  }

  /**
   * Filter user based on the user ID.
   * 
   * @param users the users to filter from
   * @param user the user to filter by
   * 
   * @return the filtered user
   */
  private User filterUser(final List<User> users, final User user) {

    try {
      return userFilter.filterByUserIdOrEmail(users, user);
    } catch (UserNotFoundException e) {
      throw new SubscriptionProcessException(user, SubscriptionErrorCode.USER_NOT_FOUND);
    }
  }

  /**
   * Creates the supplied subscription if the supplied <code>dryRun</code> set to false, otherwise if
   * <code>dryRun</code> flag set to true, this method will only return empty subscription with user details.
   * 
   * @param subscription the subscription to create.
   * @param channel the channel.
   * @param dryRun if it's a dry run call.
   * 
   * @return The created subscription.
   */
  private Subscription processSubscriptionForCreate(final Subscription subscription, final Channel channel,
      final Boolean dryRun) {

    Subscription newSubscription;

    if (dryRun) {
      newSubscription = new Subscription();
    } else {
      newSubscription = subscriptionDbDao.create(subscription, channel);
    }

    newSubscription.setUser(subscription.getUser());
    newSubscription.setContext(subscription.getContext());

    return newSubscription;
  }

  /**
   * Updates the supplied subscription if the supplied <code>dryRun</code> set to false, otherwise if
   * <code>dryRun</code> flag set to true, the supplied subscription will not be updated.
   * 
   * @param subscription The subscription to update.
   * @param dryRun if it's a dry run call.
   */
  private void processSubscriptionForUpdate(final Subscription subscription, final Boolean dryRun) {
    // Updated the supplied subscription details if it is not dryRun request.
    if (!dryRun) {
      subscriptionDbDao.updateReferral(subscription.getReferral(), subscription.getId());

      SubscriptionContext context = subscription.getContext();
      if (context != null) {
        context.setSubscriptionId(subscription.getId());
        subscriptionContextDbDao.create(context);
      }
    }
  }

  /**
   * Validates the supplied subscription for create and report back {@link SubscriptionError} containing multiple errors
   * or null if the supplied subscription is valid.
   * <p>
   * This method validates the following:
   * <ul>
   * <li>If the supplied subscription user does identify an existing user.</li>
   * <li>If the supplied subscription user is blocked in the channel.</li>
   * <li>If the supplied subscriber already subscribed to the supplied channel.</li>
   * <li>If the supplied subscriber already unsubscribed from the supplied channel.</li>
   * <li>If the supplied subscription context lead context is invalid - does not identify an existing summit or
   * communication.</li>
   * <li>If the supplied subscription context engagement score is invalid - the supplied value is not between 0-100.</li>
   * </ul>
   * 
   * @param subscription The Subscription to validate.
   * @param channel The channel of this subscription
   * @param users List of users used to valid the created subscription user subscriber details.
   * @param summits List of summits used to valid the subscription context lead context in case of summit lead type.
   * @param communications List of communications used to valid the subscription context lead context in case of content
   * lead type.
   * @return {@link SubscriptionError} in case of any validation error, otherwise null.
   */
  private SubscriptionError validateSubscriptionForCreate(final Subscription subscription, final Channel channel,
      final List<User> users, final List<SummitDto> summits, final List<CommunicationDto> communications) {
    SubscriptionError error = new SubscriptionError();

    try {
      User user = filterUser(users, subscription.getUser());
      subscription.setUser(user);
    } catch (SubscriptionProcessException e) {
      error.addErrorCode(e.getErrorCode());
    }

    try {
      validator.assertBlockedUser(subscription, channel);
    } catch (SubscriptionProcessException e) {
      error.addErrorCode(e.getErrorCode());
    }

    try {
      validator.assertSubscribedUser(subscription, channel);
    } catch (SubscriptionProcessException e) {
      error.addErrorCode(e.getErrorCode());
      error.setSubscription(e.getSubscription());
    }
    // Validates the supplied subscription context - if invalid add multiple errors.
    SubscriptionError contextErrors = validateSubscriptionContext(subscription, summits, communications);
    if (contextErrors != null) {
      error.addErrorCodes(contextErrors.getErrorCodes());
    }

    // Check if the created subscription error contain subscription if not create one with only user details.
    if (error.getSubscription() == null && error.getErrorCodes() != null) {
      Subscription subscriptionWithNoContext = new Subscription();
      subscriptionWithNoContext.setUser(subscription.getUser());
      error.setSubscription(subscriptionWithNoContext);
    }

    return error.getErrorCodes() != null ? error : null;
  }

  /**
   * Validates the supplied subscription for update and report back {@link SubscriptionError} containing multiple errors
   * or null if the supplied subscription is valid.
   * <p>
   * This method validates the following:
   * <ul>
   * <li>If the supplied subscription identify an existing subscription.</li>
   * <li>If the supplied subscription is still active.</li>
   * <li>If the supplied subscription already has subscription context.</li>
   * <li>If the supplied subscription context lead context is invalid - does not identify an existing summit or
   * communication.</li>
   * <li>If the supplied subscription context engagement score is invalid - the supplied value is not between 0-100.</li>
   * </ul>
   * 
   * @param subscription The Subscription to validate.
   * @param summits List of summits used to valid the subscription context lead context in case of summit lead type.
   * @param communications List of communications used to valid the subscription context lead context in case of content
   * lead type.
   * @return {@link SubscriptionError} in case of any validation error, otherwise null.
   */
  private SubscriptionError validateSubscriptionForUpdate(final Subscription subscription,
      final List<SummitDto> summits, final List<CommunicationDto> communications) {
    SubscriptionError error = new SubscriptionError();
    Subscription foundSubscription = null;

    // Validate if the supplied subscription exists.
    SubscriptionsSummary foundSubscriptionSummary = findById(subscription.getId());
    if (!CollectionUtils.isEmpty(foundSubscriptionSummary.getSubscriptions())) {
      foundSubscription = foundSubscriptionSummary.getSubscriptions().iterator().next();
      subscription.setUser(findUser(foundSubscription));
      subscription.setLastSubscribed(foundSubscription.getLastSubscribed());
    }

    // Add all errors reported back when finding subscription.
    if (!CollectionUtils.isEmpty(foundSubscriptionSummary.getErrors())) {
      error.addErrorCodes(foundSubscriptionSummary.getErrors().iterator().next().getErrorCodes());
    }

    // Check the found subscription's context doesn't exists and the supplied subscription has valid lead context and
    // engagement score.
    SubscriptionContext context = subscription.getContext();
    if (context != null) {
      if (foundSubscription != null && foundSubscription.getContext() != null) {
        error.addErrorCode(SubscriptionErrorCode.CONTEXT_ALREADY_EXIST);
        error.setSubscription(foundSubscription);
      }
      // Validates the supplied subscription context - if invalid add multiple errors.
      SubscriptionError contextErrors = validateSubscriptionContext(subscription, summits, communications);
      if (contextErrors != null) {
        error.addErrorCodes(contextErrors.getErrorCodes());
      }
    }

    // Check if the created subscription error contain subscription if not assign the supplied subscription.
    if (error.getSubscription() == null && error.getErrorCodes() != null) {
      error.setSubscription(subscription);
    }

    return error.getErrorCodes() != null ? error : null;
  }

  /**
   * Validates the supplied subscription context and report back {@link SubscriptionError} containing multiple errors or
   * null if the supplied subscription context is valid.
   * <p>
   * This method validates the following:
   * <ul>
   * <li>If the supplied subscription context lead context is invalid - does not identify an existing summit or
   * communication.</li>
   * <li>If the supplied subscription context engagement score is invalid - the supplied value is not between 0-100.</li>
   * </ul>
   * 
   * @param subscription The Subscription to validate.
   * @param summits List of summits used to valid the subscription context lead context in case of summit lead type.
   * @param communications List of communications used to valid the subscription context lead context in case of content
   * lead type.
   * @return {@link SubscriptionError} in case of any validation error, otherwise null.
   */
  private SubscriptionError validateSubscriptionContext(final Subscription subscription, final List<SummitDto> summits,
      final List<CommunicationDto> communications) {
    SubscriptionError error = new SubscriptionError();

    try {
      validator.assertLeadContextValue(subscription, summits, communications);
    } catch (SubscriptionProcessException e) {
      error.addErrorCode(e.getErrorCode());
    }

    try {
      validator.assertContextEngagementScore(subscription);
    } catch (SubscriptionProcessException e) {
      error.addErrorCode(e.getErrorCode());
    }

    return error.getErrorCodes() != null ? error : null;
  }

}