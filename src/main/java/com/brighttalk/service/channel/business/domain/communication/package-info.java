/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 42160 2012-02-07 17:38:24Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the business domain classes of the Communication Service.
 */
package com.brighttalk.service.channel.business.domain.communication;