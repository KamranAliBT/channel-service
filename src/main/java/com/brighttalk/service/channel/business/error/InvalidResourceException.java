/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: InvalidResourceException.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates a resource is invalid (missing required fields) for creation or update.
 * 
 */
@SuppressWarnings("serial")
public class InvalidResourceException extends ApplicationException {

  private static final String ERROR_CODE_INVALID_RESOURCE_ERROR = "InvalidResource";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_INVALID_RESOURCE_ERROR}.
   */
  private String errorCode = ERROR_CODE_INVALID_RESOURCE_ERROR;

  /**
   * @param userErrorMessage see below.
   * @param message see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public InvalidResourceException(String message, String userErrorMessage) {
    super(message, ERROR_CODE_INVALID_RESOURCE_ERROR);
    this.setUserErrorMessage(userErrorMessage);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public InvalidResourceException(String message) {
    super(message, ERROR_CODE_INVALID_RESOURCE_ERROR);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

}
