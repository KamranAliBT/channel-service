/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: ReadOnlyCommunicationFinder.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.brighttalk.service.channel.business.service.channel.ChannelFinder;
import com.brighttalk.service.channel.integration.database.CategoryDbDao;
import com.brighttalk.service.channel.integration.database.CommunicationDbDao;
import com.brighttalk.service.channel.integration.database.ResourceDbDao;

/**
 * Read Only implementation of the Communication Finder.
 */
@Service
@Qualifier(value = "readOnlyCommunicationFinder")
public class ReadOnlyCommunicationFinder extends DefaultCommunicationFinder {

  @Autowired
  public void setChannelFinder(@Qualifier(value = "readOnlyChannelFinder") ChannelFinder channelFinder) {
    this.channelFinder = channelFinder;
  }

  @Autowired
  public void setCategoryDbDao(@Qualifier(value = "readOnlyCategoryDbDao") CategoryDbDao categoryDbDao) {
    this.categoryDbDao = categoryDbDao;
  }

  @Autowired
  public void setCommunicationDbDao(@Qualifier(value = "readOnlyCommunicationDbDao") CommunicationDbDao communicationDbDao) {
    this.communicationDbDao = communicationDbDao;
  }

  @Autowired
  public void setResourceDbDao(@Qualifier(value = "readOnlyResourceDbDao") ResourceDbDao resourceDbDao) {
    this.resourceDbDao = resourceDbDao;
  }

  
  
}
