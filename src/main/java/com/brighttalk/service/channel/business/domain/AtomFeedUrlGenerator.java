/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AtomFeedUrlGenerator.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelUrlGenerator;

/**
 * Implementation of a URl Generator that generates the URL for Channel Atom feed.
 */
public class AtomFeedUrlGenerator implements ChannelUrlGenerator {

  private String channelFeedAtomUriTemplate;
  
  /**
   * This template is used to create channel atom feed url and is set when 
   * the bean is instantiated in Spring config.
   * 
   * @return uri template
   */
  public String getChannelFeedAtomUriTemplate() {
    return channelFeedAtomUriTemplate;
  }

  public void setChannelFeedAtomUriTemplate(String channelFeedAtomUriTemplate) {
    this.channelFeedAtomUriTemplate = channelFeedAtomUriTemplate;
  }

  /** {@inheritDoc} */
  @Override
  public String generate(Channel channel) {
    return channelFeedAtomUriTemplate.replace("{channelId}", channel.getId().toString() );
  }

}
