/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: InvalidRequestingUserIdException.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that provided 'X-BrightTALK-Requesting-UserId' header is invalid. 
 */
@SuppressWarnings("serial")
public class InvalidRequestingUserIdException extends ApplicationException {

  /**
   * Error code for invalid header 'X-BrightTALK-Requesting-UserId'. 
   */
  public static final String ERROR_CODE_INVALID_USER_ID = "InvalidRequestingUserId";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_INVALID_USER_ID}.
   */
  private String errorCode = ERROR_CODE_INVALID_USER_ID;

  /**
   * @param logMessage the error message written into the log.
   * @param userErrorMessage the error message visible by client.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public InvalidRequestingUserIdException(String logMessage, String userErrorMessage) {
    super(logMessage, ERROR_CODE_INVALID_USER_ID);
    this.setUserErrorMessage(userErrorMessage);
  }

  /**
   * @param userErrorMessage the error message visible by client.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public InvalidRequestingUserIdException(String userErrorMessage) {
    super(userErrorMessage, ERROR_CODE_INVALID_USER_ID);
    this.setUserErrorMessage(userErrorMessage);
  }

  /**
   * @param userErrorMessage the error message visible by client.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public InvalidRequestingUserIdException(String userErrorMessage, Throwable cause) {
    super(userErrorMessage, cause, ERROR_CODE_INVALID_USER_ID);
    this.setUserErrorMessage(userErrorMessage);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }
}