/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.utils.JobIdGenerator;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;

/**
 * Implementation of the {@link ScheduledPublicationUpdateService} that uses the Spring Scheduler mechanism to update
 * the publication status of communications on a periodic basis. This will find if any webcasts has a scheduled
 * publication job set and perform the publishing or unpublishing of the webcast. Once the job executed, it ll be marked
 * as completed.
 */
@Service
public class CronScheduledPublicationUpdateService extends AbstractCommunicationService implements ScheduledPublicationUpdateService {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(CronScheduledPublicationUpdateService.class);

  @Autowired
  private JobIdGenerator jobIdGenerator;

  /** {@inheritDoc} */
  @Override
  public void performScheduledPublicationStatusUpdate() {

    final String jobId = jobIdGenerator.generateId();
    Map<PublishStatus, List<Long>> communicationsMap = scheduledPublicationDbDao.findScheduledCommunications(jobId);

    List<Long> publishCommunicationIds = communicationsMap.get(PublishStatus.PUBLISHED);
    boolean hasPublished = !CollectionUtils.isEmpty(publishCommunicationIds);

    if (hasPublished) {
      communicationDbDao.updatePublishStatus(publishCommunicationIds, PublishStatus.PUBLISHED);
    }

    List<Long> unpublishCommunicationIds = communicationsMap.get(PublishStatus.UNPUBLISHED);
    boolean hasUnpublished = !CollectionUtils.isEmpty(unpublishCommunicationIds);

    if (hasUnpublished) {
      communicationDbDao.updatePublishStatus(unpublishCommunicationIds, PublishStatus.UNPUBLISHED);
    }

    if (hasPublished || hasUnpublished) {
      scheduledPublicationDbDao.markAsCompleted(jobId);
    }
  }

}