/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelCreateValidator.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import java.util.Date;

import org.apache.log4j.Logger;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationScheduledPublication;
import com.brighttalk.service.channel.business.error.InvalidCommunicationException;

/**
 * Business Layer Validator for a communication scheduled publication.
 */
public class CommunicationScheduledPublicationValidator implements Validator<CommunicationScheduledPublication> {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(CommunicationScheduledPublicationValidator.class);

  /** error code when throwing exception. */
  private static final String INVALID_SCHEDULED_PUBLICATION_ERROR_CODE = "InvalidScheduledPublication";

  /** Time after webcast has been live, in seconds */
  private static final Long TIME_AFTER_LIVE = 300L;

  /** Communication that needs to be validated. */
  private final Communication communication;

  /** Communication scheduled publication that will be validated */
  private final CommunicationScheduledPublication communicationScheduledPublication;

  /**
   * Construct this validator with the communication object.
   * 
   * @param communication to validate the scheduled publication for.
   */
  public CommunicationScheduledPublicationValidator(final Communication communication) {
    this.communication = communication;
    communicationScheduledPublication = communication.getScheduledPublication();
  }

  @Override
  public void validate(final CommunicationScheduledPublication communicationScheduledPublication) {
    LOGGER.debug("Assert communication scheduled publication rules.");

    if (!communicationScheduledPublication.hasScheduledPublicationDates()) {
      return;
    }

    if (!publicationDatesAreInTheFuture()) {
      throw new InvalidCommunicationException("Publication dates must be set in the future.",
          INVALID_SCHEDULED_PUBLICATION_ERROR_CODE);
    }

    if (publicationDateSetForPublishedWebcasts(communication)) {
      throw new InvalidCommunicationException("Publication date cannot be set for published webcast.",
          INVALID_SCHEDULED_PUBLICATION_ERROR_CODE);
    }

    if (unpublicationDateSetForUnpublishedWebcasts(communication)) {
      throw new InvalidCommunicationException("Unpublication date cannot be set for unpublished webcast.",
          INVALID_SCHEDULED_PUBLICATION_ERROR_CODE);
    }

    if (publicationDateIsSmallerThanUnpublicationDate()) {
      throw new InvalidCommunicationException("Unpublication date should be greater than publication date.",
          INVALID_SCHEDULED_PUBLICATION_ERROR_CODE);
    }

    if (publicationDateIsSmallerThanWebcastTimeToLive(communication)) {
      throw new InvalidCommunicationException(
          "Publication date should be greater than webcast scheduled time + duration + " + TIME_AFTER_LIVE + ".",
          INVALID_SCHEDULED_PUBLICATION_ERROR_CODE);
    }
  }

  /**
   * Validate that the scheduled publication dates provided are set in the future.
   * 
   * @return true if in the future else false.
   */
  private boolean publicationDatesAreInTheFuture() {
    Long currentDateAndTime = new Date().getTime() / 1000;
    Long publicationDateTime = communicationScheduledPublication.getPublicationDate().getTime() / 1000;
    Long unpublicationDateTime = communicationScheduledPublication.getUnpublicationDate().getTime() / 1000;

    return !(communicationScheduledPublication.hasPublicationDate()
        && !communicationScheduledPublication.isDefaultPublicationDate() && publicationDateTime <= currentDateAndTime)
        || !(communicationScheduledPublication.hasUnpublicationDate()
            && !communicationScheduledPublication.isDefaultUnpublicationDate() && unpublicationDateTime <= currentDateAndTime);
  }

  /**
   * Checks if publication dates have been set for published webcasts.
   * 
   * @param communication containing the scheduled publication dates.
   * @return true if publication dates are set for published webcast.
   */
  private boolean publicationDateSetForPublishedWebcasts(final Communication communication) {
    return communicationScheduledPublication.isUnpublicationDateSet() && communication.isPublished()
        && communicationScheduledPublication.isPublicationDateSet()
        && !communicationScheduledPublication.isDefaultPublicationDate();
  }

  /**
   * Checks if unpublication dates have been set for unpublished webcasts.
   * 
   * @param communication containing the scheduled publication dates.
   * @return true if unpublication dates are set for unpublished webcast.
   */
  private boolean unpublicationDateSetForUnpublishedWebcasts(final Communication communication) {
    return communicationScheduledPublication.isPublicationDateSet() && communication.isUnpublished()
        && communicationScheduledPublication.isUnpublicationDateSet()
        && !communicationScheduledPublication.isDefaultUnpublicationDate();
  }

  /**
   * Checks that the unpublication date is greater than the publication date if both dates are set.
   * 
   * @return true if the unpublication date greater than the publication date, else false
   */
  private boolean publicationDateIsSmallerThanUnpublicationDate() {
    Long publicationDateTime = communicationScheduledPublication.getPublicationDate().getTime() / 1000;
    Long unpublicationDateTime = communicationScheduledPublication.getUnpublicationDate().getTime() / 1000;

    return communicationScheduledPublication.hasPublicationDate()
        && !communicationScheduledPublication.isDefaultUnpublicationDate()
        && communicationScheduledPublication.hasUnpublicationDate() && (publicationDateTime >= unpublicationDateTime);
  }

  /**
   * Checks that the unpublication date is greater than the webcats time to live. Webcast time to live is the webcast
   * scheduled date + duration + an extra time after the webcast has been live.
   * 
   * @param communication containing the scheduled publication
   * @return true if the date is greater than the webcast time to live
   */
  private boolean publicationDateIsSmallerThanWebcastTimeToLive(final Communication communication) {
    boolean unpublicationDateSmallerThanScheduledTime = communicationScheduledPublication.hasUnpublicationDate()
        && !communicationScheduledPublication.isDefaultUnpublicationDate()
        && (communicationScheduledPublication.getUnpublicationDate().getTime() < (communication.getScheduledDateTime().getTime()
            + communication.getDuration() + TIME_AFTER_LIVE));

    boolean publicationDateSmallerThanScheduledTime = communicationScheduledPublication.hasPublicationDate()
        && !communicationScheduledPublication.isDefaultPublicationDate()
        && (communicationScheduledPublication.getPublicationDate().getTime() < (communication.getScheduledDateTime().getTime()
            + communication.getDuration() + TIME_AFTER_LIVE));

    return unpublicationDateSmallerThanScheduledTime || publicationDateSmallerThanScheduledTime;
  }
}