/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionValidator.java 92672 2015-04-01 08:27:25Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.math.IntRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionError.SubscriptionErrorCode;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;
import com.brighttalk.service.channel.business.error.SubscriptionNotFoundException;
import com.brighttalk.service.channel.business.error.SubscriptionProcessException;
import com.brighttalk.service.channel.business.error.SummitNotFoundException;
import com.brighttalk.service.channel.business.filter.CommunicationFilter;
import com.brighttalk.service.channel.business.filter.SubscriptionFilter;
import com.brighttalk.service.channel.business.filter.SummitFilter;
import com.brighttalk.service.channel.integration.communication.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.database.SubscriptionContextDbDao;
import com.brighttalk.service.channel.integration.database.SubscriptionDbDao;
import com.brighttalk.service.channel.integration.summit.dto.SummitDto;
import com.brighttalk.service.channel.integration.user.UserServiceDao;

/**
 * Validate basic actions in a {@link Subscription}.
 */
@Component
public class SubscriptionValidator {
  private static final int ENGAGEMENT_SCORE_MIN_VALUE = 0;
  private static final int ENGAGEMENT_SCORE_MAX_VALUE = 100;
  private static final String MISSING_SUBSCRIPTION_ERROR_MESSAGE = "The supplied subscription must not be null.";
  private static final String MISSING_USER_ERROR_MESSAGE = "The supplied subscription user must not be null.";
  private static final String MISSING_SUBSCRIPTION_ID_ERROR_MESSAGE = "The supplied subscription id must not be null.";

  @Value("${subscriptions.user.search.pageNumber}")
  private int userSearchPageNumber;

  @Value("${subscriptions.user.search.pageSize}")
  private int userSearchPageSize;

  @Autowired
  @Qualifier("localUserServiceDao")
  private UserServiceDao userServiceDao;

  @Autowired
  private SubscriptionDbDao subscriptionDbDao;

  @Autowired
  private SubscriptionContextDbDao subscriptionContextDbDao;

  @Autowired
  private SummitFilter summitFilter;

  @Autowired
  private CommunicationFilter commFilter;

  @Autowired
  private SubscriptionFilter subscriptionFilter;

  /**
   * Validates the supplied {@link Subscription} if the channel subscriber has been blocked from accessing the
   * identified channel.
   * 
   * @param subscription The subscription to validate.
   * @param channel the channel to check for
   * @throws SubscriptionProcessException in case of the channel subscriber is blocked.
   */
  public void assertBlockedUser(final Subscription subscription, final Channel channel) {
    Validate.notNull(subscription, MISSING_SUBSCRIPTION_ERROR_MESSAGE);
    Validate.notNull(subscription.getUser(), MISSING_USER_ERROR_MESSAGE);
    User user = subscription.getUser();
    Feature feature = channel.getFeature(Type.BLOCKED_USER);

    if (feature.isEnabled() && StringUtils.contains(feature.getValue(), user.getEmail())) {
      throw new SubscriptionProcessException(user, SubscriptionErrorCode.USER_BLOCKED);
    }
  }

  /**
   * Validates the supplied {@link Subscription} if the channel subscriber has been already subscribed or unsubscribed
   * from accessing the identified channel.
   * 
   * @param subscription The subscription to validate.
   * @param channel the channel to check for.
   * @throws SubscriptionProcessException in case of the channel subscriber already subscribed or the subscription is
   * inactive (unsubscribed).
   */
  public void assertSubscribedUser(final Subscription subscription, final Channel channel) {
    Validate.notNull(subscription, MISSING_SUBSCRIPTION_ERROR_MESSAGE);
    Validate.notNull(subscription.getUser(), MISSING_USER_ERROR_MESSAGE);
    User user = subscription.getUser();
    List<Subscription> channelSubscriptions = subscriptionDbDao.findByChannelId(channel.getId());

    Subscription channelSubscription;

    try {
      channelSubscription = subscriptionFilter.filterByUserId(channelSubscriptions, user.getId());
      channelSubscription.setUser(user);
    } catch (SubscriptionNotFoundException e) {
      return;
    }

    SubscriptionErrorCode errorCode;
    SubscriptionProcessException processException;

    if (channelSubscription.isActive()) {
      errorCode = SubscriptionErrorCode.ALREADY_SUBSCRIBED;
      processException = new SubscriptionProcessException(channelSubscription, errorCode);
    } else {
      errorCode = SubscriptionErrorCode.UNSUBSCRIBED;
      processException = new SubscriptionProcessException(channelSubscription.getUser(), errorCode);
    }

    throw processException;
  }

  /**
   * Validates the supplied {@link Subscription} is active.
   * 
   * @param subscription The subscription to validate.
   * @throws SubscriptionProcessException in case of the supplied subscription is inactive.
   */
  public void assertSubscriptionActive(final Subscription subscription) {
    Validate.notNull(subscription, MISSING_SUBSCRIPTION_ERROR_MESSAGE);

    if (!subscription.isActive()) {
      throw new SubscriptionProcessException(subscription, SubscriptionErrorCode.SUBSCRIPTION_INACTIVE);
    }
  }

  /**
   * Validates the supplied {@link Subscription} doesn't have a context.
   * 
   * @param subscription The subscription to validate.
   * @throws SubscriptionProcessException in case of the supplied subscription have a context.
   */
  public void assertContextDoNotExist(final Subscription subscription) {
    Validate.notNull(subscription, MISSING_SUBSCRIPTION_ERROR_MESSAGE);

    if (subscription.getContext() != null) {
      throw new SubscriptionProcessException(subscription, SubscriptionErrorCode.CONTEXT_ALREADY_EXIST);
    }
  }

  /**
   * Validates subscription context does not exists by finding subscription by id and checking if the found
   * subscription's context does not exists.
   * 
   * @param subscriptionId The subscription Id.
   * @throws SubscriptionProcessException in case of subscription context exists.
   */
  public void assertContextDoNotExist(final Long subscriptionId) {
    Validate.notNull(subscriptionId, MISSING_SUBSCRIPTION_ID_ERROR_MESSAGE);

    Subscription foundSubscription = subscriptionDbDao.findById(subscriptionId);

    if (foundSubscription.getContext() != null) {
      throw new SubscriptionProcessException(foundSubscription, SubscriptionErrorCode.CONTEXT_ALREADY_EXIST);
    }

  }

  /**
   * Validates the supplied {@link Subscription} engagement score.
   * <p>
   * Engagement score is optional for subscription context with "content" lead type and if provided it must between 0
   * and 100.
   * 
   * @param subscription The subscription to validate.
   * @throws SubscriptionProcessException in case of invalid subscription lead context.
   */
  public void assertContextEngagementScore(final Subscription subscription) {
    Validate.notNull(subscription, MISSING_SUBSCRIPTION_ERROR_MESSAGE);

    User user = subscription.getUser();
    SubscriptionContext context = subscription.getContext();

    if (context != null) {
      String engagementScore = context.getEngagementScore();
      Integer engagementScoreValue = null;
      if (engagementScore != null) {
        try {
          engagementScoreValue = Integer.parseInt(engagementScore);
        } catch (NumberFormatException nfe) {
          throw new SubscriptionProcessException(user, SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE);
        }

        IntRange range = new IntRange(ENGAGEMENT_SCORE_MIN_VALUE, ENGAGEMENT_SCORE_MAX_VALUE);
        if (!range.containsInteger(engagementScoreValue)) {
          throw new SubscriptionProcessException(user, SubscriptionErrorCode.INVALID_ENGAGEMENT_SCORE);
        }
      }
    }
  }

  /**
   * Validates the supplied {@link Subscription} lead context value does identify an existing summit if the subscription
   * lead type was a summit or an existing communication if the subscription lead type was a content.
   * 
   * @param subscription The subscription to validate.
   * @param summits The list of summit to validate against.
   * @param communications The list of communication to validate against.
   * @throws SubscriptionProcessException In case of the supplied lead context summit Id does not identify an existing
   * summit or the supplied communication Id does not identify an existing communication.
   */
  public void assertLeadContextValue(final Subscription subscription, final List<SummitDto> summits,
      final List<CommunicationDto> communications) {
    Validate.notNull(subscription, MISSING_SUBSCRIPTION_ERROR_MESSAGE);

    SubscriptionContext context = subscription.getContext();
    User user = subscription.getUser();
    if (context != null) {
      if (context.isLeadTypeSummit()) {
        try {
          summitFilter.filterBySummitId(summits, new SummitDto(new Long(context.getLeadContext())));
        } catch (SummitNotFoundException e) {
          throw new SubscriptionProcessException(user, SubscriptionErrorCode.SUMMIT_NOT_FOUND);
        }
      }

      if (context.isLeadTypeContent()) {
        try {
          commFilter.filterByCommunicationId(communications, new CommunicationDto(new Long(context.getLeadContext())));
        } catch (CommunicationNotFoundException e) {
          throw new SubscriptionProcessException(user, SubscriptionErrorCode.COMMUNICATION_NOT_FOUND);
        }
      }
    }
  }

}