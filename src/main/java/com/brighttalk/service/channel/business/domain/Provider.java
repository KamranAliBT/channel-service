/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: Provider.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import com.brighttalk.service.channel.business.domain.validator.Validator;

/**
 * Represents a Provider.
 * 
 * List the different types of providers as well as providing a validate method.
 * 
 */
public class Provider {

  /**
   * Provider of type BrightTALK (audio and slides).
   */
  public static final String BRIGHTTALK = "brighttalk";

  /**
   * Provider of type BrightTALK HD (HD video).
   */
  public static final String BRIGHTTALK_HD = "brighttalkhd";

  /**
   * Provider of type Mediazone.
   */
  public static final String MEDIAZONE = "mediazone";

  /**
   * Provider of type Video (recorded video).
   */
  public static final String VIDEO = "video";

  private final String name;

  /**
   * Constructor class with mandatory name.
   * 
   * @param name of the private
   */
  public Provider(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  /**
   * Validates this provider with given validator. Throws various validation exceptions depending on the validator used.
   * 
   * @param validator the validator implementation
   */
  public void validate(final Validator<Provider> validator) {
    validator.validate(this);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder("Provider\n");
    builder.append("[name=" + getName() + "]\n");
    return builder.toString();
  }
}
