/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelManagerService.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.error.ChannelManagerNotFoundException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.error.MaxChannelManagersFeatureException;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.integration.database.ChannelManagerDbDao;

/**
 * Validate basic actions in a {@link ChannelManager}.
 */
@Service
@Primary
public class ChannelManagerValidator {

  /** Logger for this class. */
  protected static final Logger LOGGER = Logger.getLogger(ChannelManagerValidator.class);

  @Autowired
  private ChannelManagerDbDao channelManagerDbDao;

  /**
   * Assert if the channel owner can create channel managers.
   * 
   * @param user channel owner or brighttalk manager
   * @param channel channel
   * @param channelManagers channel managers
   */
  public void assertCreate(final User user, final Channel channel, final List<ChannelManager> channelManagers) {

    autorize(user, channel);
    assertChannelManagersFeatureEnabled(channel);
    assertChannelManagersFeatureLimit(channel, channelManagers);
  }

  /**
   * Assert if the channel owner can update channel managers.
   * 
   * @param user channel owner or brighttalk manager
   * @param channel channel
   * @param channelManager channel manager
   */
  public void assertUpdate(final User user, final Channel channel, final ChannelManager channelManager) {

    autorize(user, channel);
    assertChannelManagersFeatureEnabled(channel);
    assertChannelManagerExists(channel.getId(), channelManager.getId());
  }

  /**
   * Assert if the channel owner can delete channel managers.
   * 
   * @param user channel owner or brighttalk manager
   * @param channel channel
   * @param channelManagerId channel manager id
   */
  public void assertDelete(final User user, final Channel channel, final Long channelManagerId) {

    autorize(user, channel);
    assertChannelManagersFeatureEnabled(channel);
    assertChannelManagerExists(channel.getId(), channelManagerId);
  }

  /**
   * Assert if the channel owner can search for channel managers.
   * 
   * @param user channel owner or brighttalk manager
   * @param channel channel
   */
  public void assertSearch(final User user, final Channel channel) {

    autorize(user, channel);
    assertChannelManagersFeatureEnabled(channel);
  }

  private void autorize(final User user, final Channel channel) {

    if (!user.isManager() && !channel.getOwnerUserId().equals(user.getId())) {
      throw new UserAuthorisationException("User [" + user + "] is not autorized for channel [" + channel + "].");
    }
  }

  private void assertChannelManagersFeatureEnabled(final Channel channel) {

    Feature channelManagersFeature = channel.getFeature(Feature.Type.CHANNEL_MANAGERS);

    if (!channelManagersFeature.isEnabled()) {
      throw new FeatureNotEnabledException("Channel Managers feature disabled.");
    }
  }

  private void assertChannelManagersFeatureLimit(final Channel channel, final List<ChannelManager> channelManagers) {

    Feature channelManagersFeature = channel.getFeature(Feature.Type.CHANNEL_MANAGERS);
    Integer maxChannelManagers = Integer.valueOf(channelManagersFeature.getValue());
    Integer numberOfExistingChannelManagers = countChannelManagers(channel.getId());
    Integer numberOfNewChannelManagers = channelManagers.size();

    if ((numberOfExistingChannelManagers + numberOfNewChannelManagers) > maxChannelManagers) {
      throw new MaxChannelManagersFeatureException(channel.getId(), maxChannelManagers, numberOfExistingChannelManagers);
    }
  }

  private void assertChannelManagerExists(final Long channelId, final Long channelManagerId) {

    try {
      channelManagerDbDao.findByChannelAndChannelManager(channelId, channelManagerId);
    } catch (NotFoundException e) {
      throw new ChannelManagerNotFoundException(channelManagerId);
    }
  }

  private Integer countChannelManagers(final Long channelId) {

    LOGGER.debug("Counting channel managers for channel [" + channelId + "].");

    Integer numberOfChannelManagers = channelManagerDbDao.count(channelId);

    LOGGER.debug("Found [" + numberOfChannelManagers + "] channel managers for channel [" + channelId + "].");

    return numberOfChannelManagers;
  }

}