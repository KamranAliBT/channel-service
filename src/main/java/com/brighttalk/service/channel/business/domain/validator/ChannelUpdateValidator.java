/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelUpdateValidator.java 91285 2015-03-09 13:05:18Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.error.InvalidChannelException;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.user.UserServiceDao;

/**
 * Business Layer Validator for a channel update.
 */
@Component
public class ChannelUpdateValidator implements Validator<Channel> {

  /**
   * The message error code indicating that channel name already exists.
   */
  protected static final String ERRORCODE_CHANNEL_TITLE_IN_USE = "ChannelTitleInUse";

  @Autowired
  private ChannelDbDao channelDbDao;

  @Autowired
  private UserServiceDao userServiceDao;

  @Override
  public void validate(final Channel channel) {
    assertTitleNotInUse(channel);
    assertUserExists(channel.getOwnerUserId());
  }

  private void assertTitleNotInUse(final Channel channel) {
    if (channel.isTitleChanged() && channelDbDao.titleInUseExcludingChannel(channel.getTitle(), channel.getId())) {
      throw new InvalidChannelException("Channel title [" + channel.getTitle() + "] already in use.",
          ERRORCODE_CHANNEL_TITLE_IN_USE);
    }
  }

  private void assertUserExists(final Long userId) {
    userServiceDao.find(userId);
  }

}