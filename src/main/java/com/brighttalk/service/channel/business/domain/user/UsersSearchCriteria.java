/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: UserSearchCriteria.java 69499 2013-10-09 11:19:13Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.user;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;

/**
 * Search Criteria for retrieving the user list.
 */
public class UsersSearchCriteria extends BaseSearchCriteria {

  private static final String ERROR_SORT_ORDER_CRITERIA = "sortOrder";

  private static final String ERROR_SORT_BY_CRITERIA = "sortBy";

  private static final String CRITERIA_EMAIL = "email";

  private static final String COLUMN_EMAIL = "email";

  private static final String CRITERIA_CREATED = "created";

  private static final String COLUMN_CREATED = "created";

  private static final String CRITERIA_LAST_LOGIN = "lastLogin";

  private static final String COLUMN_LAST_LOGIN = "last_login";

  private static final String CRITERIA_LAST_NAME = "lastName";

  private static final String COLUMN_LAST_NAME = "last_name";

  private SortBy sortBy;

  /**
   * Sort Order types.
   */
  public static enum SortBy {

    /** Email. */
    EMAIL(CRITERIA_EMAIL, COLUMN_EMAIL),

    /** Created date. */
    CREATED(CRITERIA_CREATED, COLUMN_CREATED),

    /** Last login date. */
    LAST_LOGIN(CRITERIA_LAST_LOGIN, COLUMN_LAST_LOGIN),

    /** Last name. */
    LAST_NAME(CRITERIA_LAST_NAME, COLUMN_LAST_NAME);

    private final String name;

    private final String value;

    /**
     * Default constructor.
     * 
     * @param name criteria name
     * @param value column name
     */
    SortBy(final String name, final String value) {
      this.name = name;
      this.value = value;
    }

    public String getName() {
      return name;
    }

    public String getValue() {
      return value;
    }

    /**
     * Default converter.
     * 
     * @param value sort by criteria
     * 
     * @return sort by
     */
    public static SortBy convert(final String value) {

      if (StringUtils.isEmpty(value)) {
        return SortBy.CREATED;
      }

      for (SortBy s : SortBy.values()) {
        if (value.equals(s.name)) {
          return s;
        }
      }

      throw new SearchCriteriaException(ERROR_SORT_BY_CRITERIA, value);
    }
  }

  public SortBy getSortBy() {
    return sortBy;
  }

  public void setSortBy(final SortBy sortBy) {
    this.sortBy = sortBy;
  }

  public void setSortBy(final String sortBy) {
    this.sortBy = SortBy.convert(sortBy);
  }

  /**
   * Set the string criteria as a sort order object.
   * 
   * @param sortOrder sort order criteria
   */
  public void setSortOrder(final String sortOrder) {

    try {

      super.setSortOrder(SortOrder.convert(sortOrder));

    } catch (ApplicationException e) {
      throw new SearchCriteriaException(ERROR_SORT_ORDER_CRITERIA, sortOrder);
    }
  }

  @Override
  public String toString() {

    ToStringBuilder builder = new ToStringBuilder(this);

    builder.append("pageNumber", getPageNumber());
    builder.append("pageSize", getPageSize());
    builder.append("sortBy", getSortBy());
    builder.append("sortOrder", getSortOrder());

    return builder.toString();
  }

}
