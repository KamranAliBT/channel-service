/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultPinManager.java 70141 2013-10-23 15:46:02Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brighttalk.service.channel.integration.database.PinGeneratorDbDao;

/**
 * Default implementation of the {@link PinGenerator}.
 */
@Service
public class RandomNumberPinGenerator implements PinGenerator {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(RandomNumberPinGenerator.class);

  private static final int MAX_PIN = 99999999;

  private static final String PIN_FORMAT = "%08d";

  /** Channel database DAO */
  @Autowired
  private PinGeneratorDbDao pinManagerDbDao;

  /**
   * {@inheritDoc}
   */
  @Override
  public String generatePinNumber() {
    String pinNumber = null;

    do {
      pinNumber = createPinNumber();
    } while (isUsed(pinNumber));

    return pinNumber;
  }

  /**
   * Create a random pin number of 8 digit.
   * 
   * @return the newly generated pin number
   */
  private String createPinNumber() {
    Random random = new Random();
    return String.format(PIN_FORMAT, random.nextInt(MAX_PIN));
  }

  /**
   * check in the database if the pin generated is already in use by a different communication.
   * 
   * @param pinNumber the pin number to check
   * @return true if in used else false
   */
  private boolean isUsed(final String pinNumber) {
    boolean isPinUsed = pinManagerDbDao.isPinUsed(pinNumber);
    if (isPinUsed) {
      LOGGER.debug("Pin [" + pinNumber + "] is already in use.");
    }
    return isPinUsed;
  }
}