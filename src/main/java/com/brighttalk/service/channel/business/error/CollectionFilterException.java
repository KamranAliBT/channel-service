/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: CollectionFilterException.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates the collection filter has failed.
 */
@SuppressWarnings("serial")
public class CollectionFilterException extends ApplicationException {

  /**
   * @param exception see below.
   * 
   */
  public CollectionFilterException(final Throwable exception) {
    super("Error while filtering collection.", exception);
  }

}
