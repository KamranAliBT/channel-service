/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContractPeriodNotFoundException.java 101130 2015-10-07 17:05:51Z kali $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

/**
 * Indicates the ContractPeriod has not been found.
 */
@SuppressWarnings("serial")
public class ContractPeriodNotFoundException extends NotFoundException {

  private final Long contractPeriodId;

  /**
   * @param contractPeriodId see below.
   *
   * @see ContractPeriodNotFoundException#ContractPeriodNotFoundException(Long, Throwable)
   */
  public ContractPeriodNotFoundException(final Long contractPeriodId) {
    this(contractPeriodId, null);
  }

  /**
   * @param contractPeriodId see below.
   * @param cause see below.
   *
   * @see NotFoundException#NotFoundException(String, Throwable)
   */
  public ContractPeriodNotFoundException(final Long contractPeriodId, final Throwable cause) {
    super("ContractPeriod [" + contractPeriodId + "] not found for id [" + contractPeriodId + "].", cause);
    this.contractPeriodId = contractPeriodId;
  }

  public Long getContractPeriodId() {
    return contractPeriodId;
  }

}
