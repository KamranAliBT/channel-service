/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ChannelUpdateAuthorisationException.java 83494 2014-09-17 16:13:00Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.user.error.UserAuthorisationException;

/**
 * Indicates the user is not authorised to update a channel.
 */
@SuppressWarnings("serial")
public class ChannelUpdateAuthorisationException extends UserAuthorisationException {

  private final Long id;

  /**
   * @param id User id
   * 
   * @see NotFoundException#NotFoundException(String)
   */
  public ChannelUpdateAuthorisationException(final Long id) {
    super("User [" + id + "] is not authorised to update this channel.");
    this.id = id;
  }

  public Long getId() {
    return id;
  }

}
