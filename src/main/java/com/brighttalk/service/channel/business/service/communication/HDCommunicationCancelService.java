/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.error.CommunicationStatusNotSupportedException;
import com.brighttalk.service.channel.business.queue.CommunityQueuer;
import com.brighttalk.service.channel.integration.community.CommunityServiceDao;

/**
 * Implementation of the {@link CommunicationCancelService} that manages the cancellation of High Definition (aka Pro)
 * webcasts.
 * 
 * Note: "@Primary" Indicates that a bean should be given preference when multiple candidates are qualified to autowire
 * a single-valued dependency.
 * 
 * "@Transaction" Indicates that if the DAO methods throw an Exception, the inserted data will be roll backed to the
 * previous state. Propagation.SUPPORTS means that if a transaction exist it'll be supported else the method will be
 * executed as non transactional.
 */
@Service
@Primary
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class HDCommunicationCancelService extends AbstractHDCommunicationService implements CommunicationCancelService {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(HDCommunicationCancelService.class);

  @Autowired
  private CommunityServiceDao communityServiceDao;

  @Autowired
  private CommunityQueuer communityQueuer;

  /**
   * {@inheritDoc}
   * 
   * "@Transactional" means that the method will be executed transactionally. If an Exception is being thrown, all the
   * changes made will be reverted to a previous known state. Propagation.REQUIRED means that the transaction is always
   * required for this method. The Rollback will happen when an exception has been caught.
   */
  @Override
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
  public Long cancel(final Communication communication) {
    LOGGER.info("Cancelling communication [" + communication.getId() + "].");

    // validate that the communication can be cancelled.
    canBeCancelled(communication);

    // Update the communication.
    communicationDbDao.cancel(communication.getId());

    // Delete the booking in booking service
    hDBookingServiceDao.deleteBooking(communication);

    // deactivate scheduled publication
    scheduledPublicationDbDao.deactivatePublicationDate(communication);

    // create jms message to cancel webcast in Email
    emailQueuer.cancel(communication.getId());

    // create jms message to cancel webcast in LES
    lesQueuer.cancel(communication.getId());

    // create jms message to delete webcast in Summit
    summitQueuer.delete(communication.getId());

    // create jms message to delete webcast in community
    communityQueuer.delete(communication.getId());

    LOGGER.info("Communication [" + communication.getId() + "] successfully cancelled.");

    return communication.getId();
  }

  /**
   * Validates the existing webcast status before we perform a cancel.
   * 
   * Currently an HD webcast can only be cancelled when the existing webcast is in an upcoming state.
   * 
   * @param communication to validate
   * @throws CommunicationStatusNotSupportedException when the status is different than the allowed one.
   */
  private void canBeCancelled(final Communication communication) {
    LOGGER.debug("Checking if communication [" + communication.getId() + "] can be cancelled.");
    Communication existingCommunication = communicationFinder.find(communication.getId());

    if (!existingCommunication.getStatus().name().equals(Status.UPCOMING.name())) {
      throw new CommunicationStatusNotSupportedException("Communication status ["
          + existingCommunication.getStatus().name() + "] is not supported to cancel a webcast.");
    }

  }

  /** {@inheritDoc} */
  @Override
  public void lesCancelCallback(final Long webcastId) {
    LOGGER.info("Callback to cancel webcast [" + webcastId + "] in LES.");

    Communication communication = findCommunication(webcastId);
    liveEventServiceDao.cancel(communication);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void summitDeleteCallback(final Long webcastId) {
    LOGGER.info("Callback to delete webcast [" + webcastId + "] in Summit.");

    summitServiceDao.delete(webcastId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailCancelCallback(final Long webcastId) {
    LOGGER.info("Callback to send cancel webcast [" + webcastId + "] in Email.");

    List<Communication> communications = getSyndicatedCommunications(webcastId);

    emailServiceDao.sendCommunicationCancelConfirmation(communications);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void communityDeleteCallback(final Long webcastId) {
    LOGGER.info("Callback to delete webcast [" + webcastId + "] in Community.");

    Communication communication = findCommunication(webcastId);
    List<Long> communicationIds = new ArrayList<Long>();
    communicationIds.add(webcastId);
    communityServiceDao.deleteCommunications(communication.getChannelId(), communicationIds);
  }

}
