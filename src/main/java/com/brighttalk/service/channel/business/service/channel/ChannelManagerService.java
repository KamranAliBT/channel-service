/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerService.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.ChannelManager;

/**
 * The Channel Manager Service Business API.
 * <p>
 * Responsible for managing {@link ChannelManager channel managers}.
 * <p>
 */
public interface ChannelManagerService {

  /**
   * Creates the given {@link ChannelManager channel manager}.
   * 
   * @param user channel owner or brighttalk manager
   * @param channelId Channel id
   * @param channelManagers details of the channel managers to be created
   * 
   * @return {@link ChannelManager Channel Managers}
   * 
   * @throws com.brighttalk.service.channel.business.error.InvalidChannelManagerException when the provided channel
   * manager invalid.
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException when the channel has not been found.
   * @throws com.brighttalk.service.channel.business.error.FeatureNotEnabledException when the Channel Manager features
   * disabeld.
   * @throws com.brighttalk.service.channel.business.error.MaxChannelManagersFeatureException when the channel managers
   * limit has reached or will be with the provided channel managers list.
   */
  List<ChannelManager> create(com.brighttalk.common.user.User user, Long channelId, List<ChannelManager> channelManagers);

  /**
   * Updates the given {@link ChannelManager channel manager}.
   * 
   * @param user channel owner or brighttalk manager
   * @param channelId channel id
   * @param channelManager details of the channel manager to be updated
   * 
   * @return {@link ChannelManager Channel Manager}
   * 
   * @throws com.brighttalk.service.channel.business.error.InvalidChannelManagerException when the provided channel
   * manager invalid.
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException when the channel has not been found.
   * @throws com.brighttalk.service.channel.business.error.ChannelManagerNotFoundException when the channel manager has
   * not been found.
   * @throws com.brighttalk.service.channel.business.error.FeatureNotEnabledException when the Channel Manager features
   * disabeld.
   */
  ChannelManager update(com.brighttalk.common.user.User user, Long channelId, ChannelManager channelManager);

  /**
   * Deletes the given {@link ChannelManager channel manager}.
   * 
   * @param user channel owner or brighttalk manager
   * @param channelId channel id
   * @param channelManagerId channel manager id
   * 
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException when the channel has not been found.
   * @throws com.brighttalk.service.channel.business.error.ChannelManagerNotFoundException when the channel manager has
   * not been found.
   * @throws com.brighttalk.service.channel.business.error.FeatureNotEnabledException when the Channel Manager features
   * disabeld.
   */
  void delete(com.brighttalk.common.user.User user, Long channelId, Long channelManagerId);

  /**
   * Deletes the given user id from the channel service.
   * <p>
   * This will perform a delete action (logic) in the channel manager table.
   * </p>
   * 
   * @param userId channel owner id
   */
  void delete(Long userId);

}