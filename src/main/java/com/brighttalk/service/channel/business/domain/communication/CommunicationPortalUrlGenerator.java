/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationPortalUrlGenerator.java 74727 2014-02-26 14:15:00Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * Implementation of a Communication URl Generator that generates communication portal url.
 */
public class CommunicationPortalUrlGenerator implements CommunicationUrlGenerator {

  /**
   * Token used to recognize if the custom channel url is empty.
   */
  protected static final String CHANNEL_URL_EMPTY_VALUE = "noChannelUrl";

  private String communicationPortalUriTemplate;

  /**
   * This template is used to create communication portal url and is set when the bean is instantiated in Spring config.
   * 
   * @return uri template
   */
  public String getCommunicationPortalUriTemplate() {
    return communicationPortalUriTemplate;
  }

  public void setCommunicationPortalUriTemplate(final String communicationPortalUriTemplate) {
    this.communicationPortalUriTemplate = communicationPortalUriTemplate;
  }

  /**
   * {@inheritDoc}
   * 
   * Communication url is set according to a set of business rules.
   * 
   * There are 4 possible versions of the url that can be generated. The method tries to generate url according to these
   * versions' precedence.
   * 
   * If the first version is not generated (is null) the mechanisms tries to generate the next version and so on, until
   * it reaches the default, portal version.
   */
  @Override
  public String generate(final Channel channel, final Communication communication) {

    final String communicationLevelCustomCommunicationUrl = getCommunicationLevelCustomCommunicationUrl(channel,
        communication);

    if (communicationLevelCustomCommunicationUrl != null) {

      return communicationLevelCustomCommunicationUrl;
    }

    final String channelLevelCustomCommunicationUrl = getChannelLevelCustomCommunicationUrl(channel, communication);

    if (channelLevelCustomCommunicationUrl != null) {

      return channelLevelCustomCommunicationUrl;
    }

    final String customChannelUrl = getCustomChannelUrl(channel);

    if (customChannelUrl != null) {

      return customChannelUrl;
    }

    return getDefaultPortalChannelUrl(channel, communication);
  }

  /**
   * Get communication level custom communication url. This url can be set when editing a specific communication
   * settings
   * 
   * @param channel details
   * @param communication details
   * 
   * @return string|null custom communication url - null if feature is not enabled or custom url is empty
   */
  private String getCommunicationLevelCustomCommunicationUrl(final Channel channel, final Communication communication) {

    Feature customUrlFeature = channel.getFeature(Feature.Type.ENABLE_COMMUNICATION_CUSTOM_URL);

    if (customUrlFeature.isEnabled()) {

      String customUrl = communication.getConfiguration().getCustomUrl();

      if (customUrl != null && !customUrl.equals("")) {

        return customUrl;
      }
    }

    return null;
  }

  /**
   * Get channel level custom communication url. This url is set from a template that's applied to all the channel's
   * communications. The template can be defined when editing channel settings
   * 
   * @param channel details
   * @param communication details
   * 
   * @return string|null any custom url - null if feature is not enabled
   */
  private String getChannelLevelCustomCommunicationUrl(final Channel channel, final Communication communication) {

    Feature customUrlFeature = channel.getFeature(Feature.Type.CUSTOM_COMM_URL);

    if (customUrlFeature.isEnabled() && customUrlFeature.hasValue()) {

      String customUrl = customUrlFeature.getValue();
      customUrl = customUrl.replace("${commId}", communication.getId().toString());

      return customUrl;
    }

    return null;
  }

  /**
   * Get any custom url for this channel. Communication url for each communication in the channel can be set to custom
   * channel url.
   * 
   * @param channel details
   * @param communication details
   * 
   * @return string|null any custom url - null if feature is not enabled or url is empty
   */
  private String getCustomChannelUrl(final Channel channel) {

    Feature customUrlFeature = channel.getFeature(Feature.Type.CUSTOM_URL);

    if (customUrlFeature.isEnabled() && customUrlFeature.hasValue()
        && !customUrlFeature.getValue().equals(CHANNEL_URL_EMPTY_VALUE)) {

      return customUrlFeature.getValue();
    }

    return null;
  }

  /**
   * Get the default Url of this communication on this channel as it appears on Portal.
   * 
   * @param channel details
   * @param communication details
   * 
   * @return string The Url
   */
  public String getDefaultPortalChannelUrl(final Channel channel, final Communication communication) {

    String portalUrl = communicationPortalUriTemplate;
    portalUrl = portalUrl.replace("{channelId}", channel.getId().toString());
    portalUrl = portalUrl.replace("{communicationId}", communication.getId().toString());

    return portalUrl;
  }
}