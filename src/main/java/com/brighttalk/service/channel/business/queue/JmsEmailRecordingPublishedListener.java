/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsCreateChannelListener.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.brighttalk.service.channel.business.service.communication.CommunicationUpdateService;

/**
 * JMS Implementation of a Message listener to receive message from the Email queuer on webcast recording published.
 */
public class JmsEmailRecordingPublishedListener implements MessageListener {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JmsEmailRecordingPublishedListener.class);

  @Autowired
  @Qualifier(value = "hdcommunicationupdateservice")
  private CommunicationUpdateService communicationUpdateService;

  /**
   * {@inheritDoc}
   */
  @Override
  public void onMessage(final Message message) {

    if (message instanceof ObjectMessage) {
      try {

        Long webcastId = (Long) ((ObjectMessage) message).getObject();
        communicationUpdateService.emailRecordingPublishedCallback(webcastId);

      } catch (Exception exception) {
        logger.error("Something went wrong with sending recording published email request", exception);
      }
    } else {
      logger.error("Recording published email webcast processing broken, because of illegal argument provided. "
          + "Expected [ObjectMessage] got [" + message + "].");
    }
  }

}