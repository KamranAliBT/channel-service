/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelStatistics.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

/**
 * Represents a BrightTALK channel statistics.
 */
public class ChannelStatistics {

  /** The id of the channel where the statistics belong to. */
  private final Long channelId;

  /**
   * The rating given by users of this channel. This is the average rating across all communications in this channel.
   */
  private float rating;

  /**
   * The amount of time users viewed this channel for. This is the average time across all communications in this
   * channel.
   * 
   */
  private long viewedFor;

  /** The number of live communications for this channel. */
  private long numberOfLiveCommunications;

  /** The number of upcoming communications for this channel. */
  private long numberOfUpcomingCommunications;

  /** The number of recorded communications for this channel. */
  private long numberOfRecordedCommunications;

  /** The number of users subscribed to this channel. */
  private long numberOfSubscribers;

  /**
   * Construct this channel as a new channel.
   */
  public ChannelStatistics() {
    this.channelId = null;
  }

  /**
   * Construct this channel statistics as an existing channel statistics with an channelId.
   * 
   * @param channelId the id of the channel
   */
  public ChannelStatistics(final Long channelId) {
    this.channelId = channelId;
  }

  /**
   * @return the channel id this channel statistics
   */
  public Long getChannelId() {
    return channelId;
  }

  /**
   * @return this channels rating or <code>null</code> if not set
   */
  public float getRating() {
    return rating;
  }

  /**
   * Set this channels rating.
   * 
   * @param rating the rating to set
   */
  public void setRating(float rating) {
    this.rating = rating;
  }

  /**
   * @return this amount of time users viewed this channel for in seconds.
   */
  public long getViewedFor() {
    return viewedFor;
  }

  /**
   * Set the amount of time users viewed this channel for.
   * 
   * @param viewedFor the viewedFor to set
   */
  public void setViewedFor(long viewedFor) {
    this.viewedFor = viewedFor;
  }

  /**
   * @return the number of live communications for this channel
   */
  public long getNumberOfLiveCommunications() {
    return numberOfLiveCommunications;
  }

  /**
   * Set the number of live communications for this channel.
   * 
   * @param numberOfLiveCommunications the number of live communications to set
   */
  public void setNumberOfLiveCommunications(final long numberOfLiveCommunications) {
    this.numberOfLiveCommunications = numberOfLiveCommunications;
  }

  /**
   * @return the number of upcoming communications for this channel
   */
  public long getNumberOfUpcomingCommunications() {
    return numberOfUpcomingCommunications;
  }

  /**
   * Set the number of upcoming communications for this channel.
   * 
   * @param numberOfUpcomingCommunications the number of upcoming communications to set
   */
  public void setNumberOfUpcomingCommunications(final long numberOfUpcomingCommunications) {
    this.numberOfUpcomingCommunications = numberOfUpcomingCommunications;
  }

  /**
   * @return the number of recorded communications for this channel
   */
  public long getNumberOfRecordedCommunications() {
    return numberOfRecordedCommunications;
  }

  /**
   * Set the number of recorded communications for this channel.
   * 
   * @param numberOfRecordedCommunications the amount of recorded communications to set
   */
  public void setNumberOfRecordedCommunications(final long numberOfRecordedCommunications) {
    this.numberOfRecordedCommunications = numberOfRecordedCommunications;
  }

  /**
   * @return the number of users subscribed to this channel.
   */
  public long getNumberOfSubscribers() {
    return numberOfSubscribers;
  }

  /**
   * Set the number of users subscribed to this channel.
   * 
   * @param numberOfSubscribers the amount of users subscribed to set
   */
  public void setNumberOfSubscribers(final long numberOfSubscribers) {
    this.numberOfSubscribers = numberOfSubscribers;
  }

}