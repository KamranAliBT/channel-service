/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: InvalidChannelManagerException.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that the provided channel manager is invalid.
 */
@SuppressWarnings("serial")
public class InvalidChannelManagerException extends ApplicationException {

  /**
   * Default Constructor.
   */
  public InvalidChannelManagerException() {
    super("Invalid channel manager.");
  }

}