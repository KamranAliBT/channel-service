/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsCreateChannelListener.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.brighttalk.service.channel.business.service.communication.CommunicationUpdateService;

/**
 * JMS Implementation of a Message listener to receive message form the notification queuer.
 */
public class JmsNotificationInformListener implements MessageListener {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JmsNotificationInformListener.class);

  @Autowired
  private CommunicationUpdateService communicationUpdateService;

  /**
   * {@inheritDoc}
   */
  @Override
  public void onMessage(final Message message) {
    if (message instanceof ObjectMessage) {

      try {
        Long webcastId = (Long) ((ObjectMessage) message).getObject();
        communicationUpdateService.notificationInformCallback(webcastId);

      } catch (Exception exception) {
        logger.error("Something went wrong with informing notification service that the webcast has ended.", exception);
      }

    } else {
      logger.error("Informing notification service processing broken, because of illegal argument provided. "
          + "Expected [ObjectMessage] got [" + message + "].");
    }
  }

}