/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * The Communication Create Service Business API.
 * <p>
 * The Communication Create Service is responsible for creating (booking) communications (aka webcasts) and informing other
 * services that a communication has been created (booked). 
 * <p>
 */
public interface CommunicationCreateService {

  /**
   * Creates (books) a communication (aka webcast) in the given channel.
   * 
   * @param user The user performing the create (channel owner or BrightTALK manager).
   * @param channel the channel in which we create the new webcast
   * @param communication The communication to be created.
   * 
   * @return The id of the created communication.
   * 
   * @throws com.brighttalk.common.user.error.UserAuthorisationException if the user is not authorised to create the
   * communication
   * @throws com.brighttalk.service.channel.business.error.NotFoundException if the channel to create the communication
   * in cannot be found
   * @throws com.brighttalk.service.channel.business.error.ContentPlanException if the creation of the communication is
   * outside of the channel content plan rules.
   * @throws com.brighttalk.service.channel.business.error.InvalidCommunicationException if there are business rule
   * violations when creating the communication
   * @throws com.brighttalk.service.channel.business.error.ProviderNotSupportedException In case of the communication
   * provider is not "BrightTALK HD".
   */
  Long create(User user, Channel channel, Communication communication);

  /**
   * Callback to handle any asynchronous create webcast messages to be sent to the email service.
   * 
   * @param webcastId id of the newly created (booked) webcast. 
   */
  void emailCreateCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous messages to image converter service. 
   * The image convertor handles the processing of any uploaded feature image made during booking
   * (namely create a thumbnail and preview image).
   * 
   * @param webcastId id of the webcast that needs feature image processing. 
   */
  void imageConverterInformCallback(Long webcastId);
}
