/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CreateChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Interface for create channel related operations queues. The operations that need to be done asynchronously include
 * tasks that must be performed by over services. To perform those operations service API has a separate callback
 * method.
 */
public interface CreateChannelQueue {

  /**
   * Perform asynchronous operations on a given channel required when performing a create channel operation.
   * 
   * @param channelId id of the channel to be created
   */
  void createChannel(Long channelId);
}
