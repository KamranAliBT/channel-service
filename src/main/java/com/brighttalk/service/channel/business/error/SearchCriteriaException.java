/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SearchCriteriaException.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates the search criteria is invalid.
 */
@SuppressWarnings("serial")
public class SearchCriteriaException extends ApplicationException {

  private final String criteria;

  private final String value;

  /**
   * @param criteria see below.
   * @param value see below
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public SearchCriteriaException(final String criteria, final String value) {
    super("Invalid search criteria " + criteria + ", value [" + value + "].");

    this.criteria = criteria;
    this.value = value;
  }

  public String getCriteria() {
    return criteria;
  }

  public String getValue() {
    return value;
  }

}
