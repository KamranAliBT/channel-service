/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelFinderTest.java 70141 2013-10-23 15:46:02Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

/**
 * Default implementation of {@link Keywords}.
 * <p>
 * TODO Rename this class to something like "KeywordsConverter" after changing keywords implementation in Webcast APIs
 * to list of keywords, also this class is better implemented as final utility class with all static methods. The method
 * in this class has some duplicate logic in "addQuotes" and "convertListOfKeywordsToKeywordsString" around adding
 * quotation, left as is for now, will re-factor when Webcast APIs keywords implementation changed to be similar to the
 * Channel APIs.
 */
@Component
public class DefaultKeywords implements Keywords {

  private static final String COMMA_SPACE = ", ";
  private static final char EMPTY_CHAR = '\0';
  private static final String KEYWORDS_SEPARATOR = ",";

  /**
   * {@inheritDoc}
   */
  @Override
  public String clean(final String keywordString) {
    boolean inQuotes = false;
    String cleanString = "";

    char[] keywords = keywordString.toCharArray();

    int i = 1;
    for (char character : keywords) {

      if ((character == ',' || character == ' ') && cleanString.length() == 0) {
        // if the start character ends with a , or a space, set empty character
        character = EMPTY_CHAR;

      } else if ((character == ',' || character == ' ') && keywords.length == i) {
        // if the end character ends with a , or a space, set empty character
        character = EMPTY_CHAR;

      } else if (character == '"') {
        // if the character is a double quote, remove the quote and initialise the inquotes to true
        inQuotes = !inQuotes;
        character = EMPTY_CHAR;

      } else if (character == ' ') {
        // the the character is a space

        // if the character is not in quote
        if (!inQuotes) {

          // check that the new string has data, that the last character is not a comma or a space and that the 2 lasts
          // characters are not a comma and space
          if (cleanString.length() > 0 && !cleanString.substring(cleanString.length() - 1).equals(",")
              && !cleanString.substring(cleanString.length() - 1).equals(" ")
              && !cleanString.substring(cleanString.length() - 2).equals(COMMA_SPACE)) {
            character = ',';
          }

        }
      }
      // if the character is an empty character, do not set it to the new string
      if (character != EMPTY_CHAR) {
        cleanString += character;
      }
      i++;
    }

    return cleanString.replaceAll(",,", COMMA_SPACE);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String addQuotes(final String keywordString) {
    String[] keywords = keywordString.split(",");

    int i = 0;
    for (String keyword : keywords) {
      keyword = keyword.trim();

      if (keyword.indexOf(' ') > 0) {
        keyword = '"' + keyword + '"';
      }

      keywords[i] = keyword;
      i++;
    }

    return StringUtils.join(keywords, COMMA_SPACE);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<String> removeQuotes(final List<String> keywords) {
    List<String> keywordsWithoutQuotes = null;

    if (keywords != null) {
      keywordsWithoutQuotes = new ArrayList<>();

      for (String keyword : keywords) {
        keyword = keyword.replaceAll("\"", "").trim();
        keywordsWithoutQuotes.add(keyword);
      }
    }

    return keywordsWithoutQuotes;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String convertListOfKeywordsToKeywordsString(final List<String> keywords) {
    if (CollectionUtils.isEmpty(keywords)) {
      return StringUtils.EMPTY;
    }

    return StringUtils.join(keywords, KEYWORDS_SEPARATOR);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<String> convertKeywordsStringToListOfKeywords(final String strKeywords) {
    if (StringUtils.isEmpty(strKeywords)) {
      return Collections.emptyList();
    }

    String[] keywordsArray = strKeywords.split(KEYWORDS_SEPARATOR);
    return Arrays.asList(keywordsArray);
  }
}