/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceService.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import java.util.List;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * The Communication Service Business API.
 * <p>
 * The Communication Service is responsible for managing and {@link Resource resource}.
 * <p>
 * The business API defined by this communication service currently supports:
 * <ul>
 * <li>Get a Single Resource from a Communication</li>
 * <li>Delete a Resource in a Communication</li>
 * <li>Create a Resource in a Communication</li>
 * <li>Edit a Resource</li>
 * <li>Edit a Resource in a Communication</li>
 * <li>Get all Resources of a Communication</li>
 * <li>Update the Order of Resources of a communication</li>
 * </ul>
 */
public interface ResourceService {

  /**
   * Finds a resource for given user, channel, communicationId and resourceId.
   * 
   * @param user the user looking for the resource
   * @param channel the channel to find the resource on
   * @param communicationId the communication to find the resource on
   * @param resourceId the id of the resource to find
   * 
   * @return resource
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the resource or communication cannot
   * be found.
   */
  Resource findResource(User user, Channel channel, Long communicationId, Long resourceId);

  /**
   * Finds resources for given user, channel and communicationId.
   * 
   * @param user the user looking for the resources
   * @param channel the channel object
   * @param communicationId the communication to get resources on
   * 
   * @return collection of resources or empty list if the communication does not have any resources.
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the communication or resource cannot
   * be found.
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the given user is not authorized to access
   * the channel or communication.
   */
  List<Resource> findResources(User user, Channel channel, Long communicationId);

  /**
   * Create a resource for the given user, channel and communicationId.
   * 
   * @param user the user creating the resource
   * @param channel the channel to create the resource on
   * @param communicationId the communication to create the resource on
   * @param resource the resource to be created
   * 
   * @return resource
   * 
   * @throws com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException when the communication is LIVE
   * or in 15 minute window to go-live time. A resource cannot be created during this time.
   * @throws com.brighttalk.service.channel.business.error.MustBeInMasterChannelException when the communication is not
   * in a master channel.
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the given user is not authorized to access
   * the channel or communication.
   */
  Resource createResource(User user, Channel channel, Long communicationId, Resource resource);

  /**
   * Updates a resource detail.
   * 
   * @param resource resource to be updated
   * @return resource
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the resource cannot be found
   */
  Resource updateResource(Resource resource);

  /**
   * Updates a resource for given user, channel and communicationId.
   * 
   * @param user the user updating the resource
   * @param channel the channel object
   * @param communicationId the communication to update the resource on
   * @param resource the resource to be updated
   * 
   * @return resource.
   * 
   * @throws com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException when the communication is LIVE
   * or in 15 minute window to go-live time. A resource cannot be updated during this time.
   * @throws com.brighttalk.service.channel.business.error.MustBeInMasterChannelException when the communication is not
   * in a master channel.
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the communication or resource cannot
   * be found.
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the given user is not authorized to access
   * the channel or communication.
   */
  Resource updateResource(User user, Channel channel, Long communicationId, Resource resource);

  /**
   * Updates resources order for given channel and communicationId.
   * 
   * @param user the user updating the resources order
   * @param channel the channel object
   * @param communicationId the communication to update order on
   * @param resources collection of resources with new order
   * 
   * @return collection of resources in new order.
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the communication does not exist.
   * @throws com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException when the communication is LIVE
   * or in 15 minute window to go-live time. The resources cannot be reordered during this time.
   * @throws com.brighttalk.service.channel.business.error.MustBeInMasterChannelException when the communication is not
   * in a master channel.
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the given user is not authorized to access
   * the channel or communication.
   */
  List<Resource> updateResourcesOrder(User user, Channel channel, Long communicationId, List<Resource> resources);

  /**
   * Deletes a resource for given channel, communicationId and resourceId.
   * 
   * @param user the user deleting the resource
   * @param channel the channel object
   * @param communicationId the communication to delete the resource on
   * @param resourceId id of the the resource to be deleted
   * 
   * @throws com.brighttalk.service.channel.business.error.NotAllowedAtThisTimeException when the communication is LIVE
   * or in 15 minute window to go-live time. A resource cannot be deleted during this time.
   * @throws com.brighttalk.service.channel.business.error.MustBeInMasterChannelException when the communication is not
   * in a master channel.
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the communication or resource cannot
   * be found.
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the given user is not authorized to access
   * the channel or communication.
   */
  void deleteResource(User user, Channel channel, Long communicationId, Long resourceId);

}