/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: PresentationCriteria.java 71274 2013-11-26 14:01:47Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import javax.validation.constraints.Pattern;

/**
 * Criteria for retrieving the expand paramitter.
 */
public class PresentationCriteria {

  /** Expand value for Featured Webcasts */
  public static final String EXPAND_FEATURED_WEBCASTS = "featuredWebcasts";

  /** Expand request param */
  @Pattern(regexp = EXPAND_FEATURED_WEBCASTS, message = "{error.invalid.expand.value}")
  private String expand;

  /**
   * Set the expand param. Accepted value: "featuredWebcasts".
   * 
   * @param expand Expand param
   */
  public void setExpand(final String expand) {
    this.expand = expand;
  }

  /**
   * Add Expand Value to this criteria.
   * 
   * @param value The Value to add
   */
  public void addExpand(final String value) {

    if (expand == null) {
      expand = value;
    } else {
      expand = expand + "," + value;
    }
  }

  /**
   * Check if the criteria contain a specified expand value.
   * 
   * @param expandEntity The value to search for
   * @return <code>True</code> if the expand contains the search for value
   */
  public boolean expandIncludes(final String expandEntity) {
    if (expand == null) {
      return false;
    }

    return expand.contains(expandEntity);
  }
}