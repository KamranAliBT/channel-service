/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionLeadType.java 91908 2015-03-18 16:46:30Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import com.brighttalk.service.channel.business.error.SubscriptionRequestException;
import com.brighttalk.service.channel.presentation.error.ErrorCode;

/**
 * List of channel subscription lead type.
 */
public enum SubscriptionLeadType {

  /** Keyword lead type. */
  KEYWORD("keyword"),
  /** Summit lead type. */
  SUMMIT("summit"),
  /** Content lead type. */
  CONTENT("content");

  private static final String ERROR_MESSAGE_INVALID_LEAD_TYPE =
      "The subscription context lead type is invalid for one or more subscription.";

  private String type;

  /**
   * Default constructor.
   * 
   * @param leadTypeValue The lead type.
   */
  private SubscriptionLeadType(String type) {
    this.type = type;
  }

  /**
   * Retrieve the configured lead type.
   * 
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * Retrieve and validate the supplied lead type is supported.
   * 
   * @param type The lead type to validate.
   * @throws SubscriptionRequestException if the the lead type is not supported.
   * 
   * @return The found {@link SubscriptionLeadType}.
   */
  public static SubscriptionLeadType getType(String type) {
    switch (type.toLowerCase().trim()) {
      case "webinar":
        return CONTENT;
      default:
        try {
          return SubscriptionLeadType.valueOf(type.toUpperCase().trim());
        } catch (IllegalArgumentException ex) {
          throw new SubscriptionRequestException(ERROR_MESSAGE_INVALID_LEAD_TYPE, ErrorCode.INVALID_LEAD_TYPE);
        }
    }
  }

  /**
   * Validate the supplied subscription lead type is {@link #KEYWORD}.
   * 
   * @param leadType The lead type to validate.
   * @return <code>true</code> if this subscription context lead type is {@link #KEYWORD}, <code>false</code> otherwise.
   */
  public static boolean isLeadTypeKeyword(String leadType) {
    return KEYWORD.equals(getType(leadType));
  }

  /**
   * Validate the supplied subscription lead type is {@link #SUMMIT}.
   * 
   * @param leadType The lead type to validate.
   * @return <code>true</code> if this subscription lead type is of type {@link #SUMMIT}, <code>false</code> otherwise.
   */
  public static boolean isLeadTypeSummit(String leadType) {
    return SUMMIT.equals(getType(leadType));
  }

  /**
   * Validate the supplied subscription lead type is {@link #CONTENT}.
   * 
   * @param leadType The lead type to validate.
   * @return <code>true</code> if this subscription lead type is of type {@link #CONTENT}, <code>false</code> otherwise.
   */
  public static boolean isLeadTypeContent(String leadType) {
    return CONTENT.equals(getType(leadType));
  }

}
