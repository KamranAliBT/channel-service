/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationRegistrationService.java 95153 2015-05-18 08:20:33Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedListWithTotalPages;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria;

/**
 * The Communication Registration Service Business API.
 * <p>
 * The Communication Registration Service is responsible for managing registrations.
 * <p>
 * The business API defined by this communication registrations service currently supports:
 * <ul>
 * <li>Get a Paginated collection of communication registrations for a user using given search criteria</li>
 * </ul>
 */
public interface CommunicationRegistrationService {

  /**
   * Get a paginated collection of communication registrations for a given user and search criteria.
   * 
   * @param user the user for which the registrations will be searched
   * @param searchCriteria criteria used to findRegistrations the correct pagination of the registrations
   * 
   * @return paginated collection of communication registrations
   */
  PaginatedListWithTotalPages<CommunicationRegistration> findRegistrations(User user,
      CommunicationRegistrationSearchCriteria searchCriteria);
}