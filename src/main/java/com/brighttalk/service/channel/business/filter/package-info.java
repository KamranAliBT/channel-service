/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: package-info.java 87022 2014-12-03 13:00:40Z afernandes $
 * ****************************************************************************
 */
/**
 * This package contains the business filter classes of the Audience Import Service.
 */
package com.brighttalk.service.channel.business.filter;

