/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SummitFilter.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.filter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.error.SummitNotFoundException;
import com.brighttalk.service.channel.integration.summit.dto.SummitDto;

/**
 * Helper to filter summits.
 */
@Component
public class SummitFilter {

  @Autowired
  private CollectionFilter collectionFilter;

  /**
   * Filter a summit by id.
   * 
   * @param summits the summits to filter.
   * @param searchableSummit the summit to search for.
   * 
   * @return the filtered summit.
   */
  public SummitDto filterBySummitId(final List<SummitDto> summits, final SummitDto searchableSummit) {
    Predicate<SummitDto> equalsSummitId = new Predicate<SummitDto>() {
      @Override
      public boolean apply(final SummitDto summit) {
        return summit.getId().equals(searchableSummit.getId());
      }
    };

    List<SummitDto> filteredSummits = collectionFilter.filter(summits, equalsSummitId);

    if (CollectionUtils.isEmpty(filteredSummits)) {
      throw new SummitNotFoundException(searchableSummit.getId());
    }

    return filteredSummits.iterator().next();
  }
}
