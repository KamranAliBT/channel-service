/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: DefaultFeatureService.java 75381 2014-03-18 15:16:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.FeatureAuthorisation;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.queue.AuditQueue;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.database.FeatureDbDao;
import com.brighttalk.service.channel.integration.database.SubscriptionDbDao;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.util.UserSearchCriteriaBuilder;

/**
 * Default implementation of the {@link FeatureService}.
 */
@Service
public class DefaultFeatureService implements FeatureService {

  private static final int FEATURE_VALUE_BEGIN_INDEX = 0;

  private static final int FEATURE_VALUE_LIMIT = 500;

  private static final Logger LOGGER = Logger.getLogger(DefaultFeatureService.class);

  private static final String COMMA_SEPARATOR = ",";

  private static final String PIPE_SEPARATOR = "|";

  private static final String ELLIPSIS_CHAR = "...";

  @Autowired
  private FeatureDbDao featureDbDao;

  @Autowired
  private FeatureAuthorisation featureAuthorisation;

  @Autowired
  @Qualifier("localUserServiceDao")
  private UserServiceDao userServiceDao;

  @Autowired
  private UserSearchCriteriaBuilder userSearchCriteriaBuilder;

  @Autowired
  private SubscriptionDbDao subscriptionDbDao;

  @Autowired
  private FeaturesPostProcessor featuresPostProcessor;

  @Autowired
  private AuditQueue auditQueue;

  @Autowired
  @Qualifier("featureAuditRecordDtoBuilder")
  private AuditRecordDtoBuilder featureAuditRecordDtoBuilder;

  /** {@inheritDoc} */
  @Override
  public List<Feature> findByChannelId(final Long channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding features for channel  [" + channelId + "].");
    }

    List<Feature> features = featureDbDao.findByChannelId(channelId);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + features.size() + "] features.");
    }

    if (CollectionUtils.isEmpty(features)) {
      throw new ChannelNotFoundException(channelId);
    }

    return features;
  }

  /** {@inheritDoc} */
  @Override
  public List<Feature> update(final User user, final Channel channel, final List<Feature> featuresToUpdate) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updating features for channel  [" + channel.getId() + "].");
    }

    featureAuthorisation.authoriseForUpdate(user, channel, featuresToUpdate);

    // Process feature update by comparing with supplied channel default and existing features.
    processFeatureUpdate(channel, featuresToUpdate);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updated [" + featuresToUpdate.size() + "] features.");
    }

    auditUpdate(user, channel, featuresToUpdate);

    featuresPostProcessor.process(channel, featuresToUpdate);

    List<Feature> features = findByChannelId(channel.getId());

    return filterFeaturesByUser(user, features);
  }

  /**
   * This method will compare the new channel features with the existing channel feature and the default channel type
   * features before performing the update.
   * <p>
   * The update of the channel features will only be processed if:
   * <ul>
   * <li>The new channel features are different from the existing channel features</li>
   * <li>The new channel features are identical to the default channel type features in case we delete the channel
   * features (soft delete the feature override value entry).</li>
   * <li>The new features are not equal to the default features, the we perform an upsert of the channel feature
   * override values, including activating the channel feature override value on update.</li>
   * </ul>
   * 
   * @param channel The channel with features to update.
   * @param features List of {@link Feature} to update.
   */
  private void processFeatureUpdate(final Channel channel, final List<Feature> features) {
    List<Feature> defaultFeatures = featureDbDao.findDefaultFeatureByChannelTypeId(channel.getType().getId());
    List<Feature> existingFeatures = findByChannelId(channel.getId());

    List<Feature> updatableOverrideFeatures = new ArrayList<>();
    List<Type> deleteableFeatureNames = new ArrayList<>();
    for (Feature feature : features) {
      // We only need to update the feature value if this is different to what is stored.
      if (!feature.equals(filterFeaturesByName(existingFeatures, feature.getName()))) {

        // We remove override values if they match the default, otherwise update the channel feature.
        if (feature.equals(filterFeaturesByName(defaultFeatures, feature.getName()))) {
          deleteableFeatureNames.add(feature.getName());
        } else {
          updatableOverrideFeatures.add(feature);
        }
      }
    }

    if (!deleteableFeatureNames.isEmpty()) {
      featureDbDao.deleteFeatureOverridesByNames(channel.getId(), deleteableFeatureNames);
    }

    if (!updatableOverrideFeatures.isEmpty()) {
      featureDbDao.update(channel, updatableOverrideFeatures);
    }
  }

  private void auditUpdate(final User user, final Channel channel, final List<Feature> featuresToUpdate) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Auditing updated features for channel  [" + channel.getId() + "].");
    }

    StringBuilder actionDescription = new StringBuilder();

    for (Feature feature : featuresToUpdate) {
      String featureName = feature.getName().getApiName();
      boolean isEnabled = feature.isEnabled();
      String featureValue = feature.hasValue() ? feature.getValue() : StringUtils.EMPTY;

      if (StringUtils.isNotBlank(featureValue)) {
        int endIndex = Math.min(featureValue.length(), FEATURE_VALUE_LIMIT);
        featureValue = featureValue.substring(FEATURE_VALUE_BEGIN_INDEX, endIndex);
        // If trimmed feature value less than the original value append with ellipsis character.
        if (featureValue.length() < feature.getValue().length()) {
          featureValue += ELLIPSIS_CHAR;
        }
      }

      actionDescription.append(featureName).append(COMMA_SEPARATOR);
      actionDescription.append(isEnabled).append(COMMA_SEPARATOR);
      actionDescription.append(featureValue).append(PIPE_SEPARATOR);
    }

    AuditRecordDto auditRecordDto = featureAuditRecordDtoBuilder.update(user.getId(), channel.getId(),
        actionDescription.toString());

    auditQueue.create(auditRecordDto);
  }

  private List<Feature> filterFeaturesByUser(final User user, final List<Feature> features) {
    if (user.isManager()) {
      return features;
    }

    List<Feature> filteredFeatures = new ArrayList<>();

    for (Feature feature : features) {
      if (FeatureAuthorisation.OWNER_MANAGER_VIEW_FEATURES.contains(feature.getName())) {
        filteredFeatures.add(feature);
      }
    }

    return filteredFeatures;
  }

  private Feature filterFeaturesByName(final List<Feature> features, final Type type) {
    for (Feature feature : features) {
      if (feature.getName().equals(type)) {
        return feature;
      }
    }
    return null;
  }

}
