/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodCreateValidator.java 101545 2015-10-16 11:26:42Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.InvalidContractPeriodException;
import com.brighttalk.service.channel.business.error.OverlappingContractPeriod;
import com.brighttalk.service.channel.integration.database.ContractPeriodDbDao;
import com.brighttalk.service.channel.presentation.controller.base.BaseController;
import com.brighttalk.service.channel.presentation.dto.ErrorDto;
import org.joda.time.DateTime;

/**
 * Validates a {@link ContractPeriod} prior to creation.
 * <p/>
 * Unlike the other Validators, this is a fail-fast validator which throws the exception immediately to be handled by
 * a top-level controller (in this case, {@link BaseController}). Fail-fast is preferred here because currently the
 * {@link ErrorDto} does not support returning multiple errors, so we do not want to do potentially expensive validation
 * only to return a single failure reason.
 * <p/>
 * Stateless.
 */
public class ContractPeriodCreateValidator implements Validator<ContractPeriod> {

  /**
   * ContractPeriodDBDao to verify the ContractPeriod
   */
  private final ContractPeriodDbDao contractPeriodDbDao;

  //When the start of a contract period is invalid
  private static final String INVALID_CONTRACT_PERIOD_START = "InvalidContractPeriodStart";

  //When the end of a contract period is invalid
  private static final String INVALID_CONTRACT_PERIOD_END = "InvalidContractPeriodEnd";

  //When a contract period is invalid
  private static final String INVALID_CONTRACT_PERIOD = "InvalidContractPeriod";

  //When a contract period overlaps an existing contract period
  private static final String OVERLAPPING_CONTRACT_PERIOD = "OverlappingContractPeriod";

  public ContractPeriodCreateValidator(final ContractPeriodDbDao contractPeriodDbDao) {
    this.contractPeriodDbDao = contractPeriodDbDao;
  }

  @Override
  public void validate(final ContractPeriod contractPeriod) {
    //check the start date
    assertStartDate(contractPeriod);

    //check the end date
    assertEndDate(contractPeriod);

    //check the contract period as a whole
    assertContractPeriod(contractPeriod);
  }

  /**
   * Validates a {@link ContractPeriod}'s start date.
   *
   * @param contractPeriod to be validated
   *
   * @throws InvalidContractPeriodException if the start date is invalid
   */
  private void assertStartDate(final ContractPeriod contractPeriod) {
    //is the date given?
    if (!contractPeriod.hasStart()) {
      throw new InvalidContractPeriodException("Contract period start date not given.",
          INVALID_CONTRACT_PERIOD_START);
    }

    //is this date before today's date?
    DateTime midnightLastNight = new DateTime().withTimeAtStartOfDay();
    DateTime startDate = new DateTime(contractPeriod.getStart().getTime());
    boolean contractBeforeToday = startDate.isBefore(midnightLastNight);
    if (contractBeforeToday) {
      throw new InvalidContractPeriodException("ContractPeriod start date is in the past.",
          INVALID_CONTRACT_PERIOD_START);
    }
  }

  /**
   * Validates a {@link ContractPeriod}'s end date.
   *
   * @param contractPeriod to be validated
   *
   * @throws InvalidContractPeriodException if the end date is invalid
   */
  private void assertEndDate(final ContractPeriod contractPeriod) {
    //is the date given?
    if (!contractPeriod.hasEnd()) {
      throw new InvalidContractPeriodException("Contract period end date not given.", INVALID_CONTRACT_PERIOD_END);
    }

    //is this date before now?
    DateTime midnightLastNight = new DateTime().withTimeAtStartOfDay();
    DateTime endDate = new DateTime(contractPeriod.getEnd().getTime());
    boolean contractBeforeToday = endDate.isBefore(midnightLastNight);
    if (contractBeforeToday) {
      throw new InvalidContractPeriodException("Contract period end date is in the past.",
          INVALID_CONTRACT_PERIOD_END);
    }
  }

  /**
   * Validates a {@link ContractPeriod} in a generic manner.
   *
   * @param contractPeriod to be validated
   *
   * @throws InvalidContractPeriodException if the {@link ContractPeriod} is invalid
   */
  private void assertContractPeriod(final ContractPeriod contractPeriod) {
    //are the start and end times the same?
    DateTime startDate = new DateTime(contractPeriod.getStart().getTime());
    DateTime endDate = new DateTime(contractPeriod.getEnd().getTime());
    if (startDate.equals(endDate)) {
      throw new InvalidContractPeriodException("Start date is the same as the end date.",
          INVALID_CONTRACT_PERIOD);
    }

    // is the end date before the start date?
    if (endDate.isBefore(startDate)) {
      throw new InvalidContractPeriodException("End date is before the start date.", INVALID_CONTRACT_PERIOD);
    }

    boolean contractPeriodOverlaps =
        contractPeriodDbDao.contractPeriodOverlapsExisting(contractPeriod);
    if (contractPeriodOverlaps) {
      throw new OverlappingContractPeriod("Contract period overlaps an existing contract period.",
          OVERLAPPING_CONTRACT_PERIOD);
    }
  }
}
