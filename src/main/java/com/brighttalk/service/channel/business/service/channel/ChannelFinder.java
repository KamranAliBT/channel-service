/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelFinder.java 75381 2014-03-18 15:16:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.List;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;

/**
 * The Channel Finder Business API.
 * <p>
 * Responsible for finding {@link Channel channels}.
 * <p>
 */
public interface ChannelFinder {

  /**
   * Finds the given channel without authorization checking.
   * 
   * @param channelId the id of the channel to find
   * 
   * @return the channel
   * 
   * @see #find(User, Long) to find a channel with authorisation checking
   */
  Channel find(Long channelId);
  
  /**
   * Finds an active channel by channelId with authorization checking.
   * <p>
   * Authorization checks are as follows:
   * <ul>
   * <li>allow access if user is a manager</li>
   * <li>allow access if user is the channel owner</li>
   * <li>allow access if user is a presenter on any webcasts in the channel</li>
   * </ul>
   * <p>
   * All other users are disallowed access.
   * 
   * @param user the user to perform authorization checks on.
   * @param channelId the id of the channel to find
   * 
   * @return the channel
   * 
   * @throws com.brighttalk.common.user.error.UserAuthorisationException when the user is not allowed access to this
   * channel.
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when the channel cannot be found.
   * 
   * @see #find(Long) to find a channel without authorisation checking. 
   */
  Channel find(User user, Long channelId);
  
  /**
   * Find a paginated list of channels that meet the given search criteria. 
   * 
   * @param criteria the search criteria
   * 
   * @return the paginated list of channels
   */
  PaginatedList<Channel> find(ChannelsSearchCriteria criteria);

  /**
   * Find a list of channels given a list of channel ids.
   * 
   * @param ids the ids of the channels to retrieve
   * 
   * @return list of found channels
   */
  List<Channel> find(List<Long> ids);

 
  /**
   * Find all the channels the given user owns that meet the given search criteria.
   * 
   * @param user the user who owns the channels
   * @param criteria search criteria
   * 
   * @return a paginated list of channels owned by the user
   */
  PaginatedList<Channel> findOwnedChannels(User user, MyChannelsSearchCriteria criteria);
 
  /**
   * Find all the channels the given user is subscribed to that meet the given search criteria.
   * 
   * @param user the user who is subscribed to the channels
   * @param criteria search criteria
   * 
   * @return a paginated list of channels the user is subscribed to
   */
  PaginatedList<Channel> findSubscribedChannels(User user, MyChannelsSearchCriteria criteria);


  /**
   * Finds subscribed channels for the given user.
   * 
   * @param user the user to find the channels for.
   * 
   * @return {@link List list} of channels, or an empty list when no channels are found for the user. We may have no
   * subscribed channels because the user hasn't subscribed to any yet, or because the user does not exist on the
   * system.
   * 
   * @deprecated this was an initial version used for version 1 of the iphone app. We will remove it eventually 
   * once the iphone app stops using the relevant API. 
   * @see #findSubscribedChannels(User,MyChannelsSearchCriteria)
   */
  @Deprecated
  List<Channel> findSubscribedChannels(User user);
  
}