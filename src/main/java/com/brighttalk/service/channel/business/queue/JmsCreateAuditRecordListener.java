/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsCreateAuditRecordListener.java 78594 2014-05-28 15:11:50Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.brighttalk.service.channel.integration.audit.AuditServiceDao;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * JMS Implementation of a Message listener to receive message form the Create Audit Record queue and use the audit
 * service to create the audit record.
 */
public class JmsCreateAuditRecordListener implements MessageListener {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JmsCreateAuditRecordListener.class);

  @Autowired
  private AuditServiceDao auditServiceDao;

  /** {@inheritDoc} */
  @Override
  public void onMessage(final Message message) {

    if (message instanceof ObjectMessage) {
      try {

        AuditRecordDto auditRecordDto = (AuditRecordDto) ((ObjectMessage) message).getObject();
        auditServiceDao.create(auditRecordDto);

      } catch (Exception exception) {
        logger.error("Error unpacking message and calling service", exception);
      }
    } else {
      logger.error("Create channel processing broken, because of illegal argument provided. "
          + "Expected [ObjectMessage] got [" + message + "].");
    }
  }
}