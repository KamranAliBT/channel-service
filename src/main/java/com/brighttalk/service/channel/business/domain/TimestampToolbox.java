/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationFinder.java 70153 2013-10-23 17:23:51Z acairns $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import java.util.Date;
import java.util.HashMap;

/**
 * The Timestamp Tool box used to calculate time related periods.
 * <p>
 * Responsible for managing timestamp calculation. This will be used by the content plan checker.
 * </p>
 */
public interface TimestampToolbox {

  /** Constant to get the start period key */
  String START_PERIOD = "start";

  /** Constant to get the start period key */
  String END_PERIOD = "end";

  /**
   * Check if the given scheduled timestamp is within the first month of the created channel.
   * 
   * @param created the created timestamp of the channel
   * @param scheduled the scheduled timestamp of the communication
   * @return boolean true if in first month
   */
  boolean inFirstMonth(Date created, Date scheduled);

  /**
   * This will return the start and end points of the time period within which the given communication is scheduled.
   * 
   * The periods are defined as 1 year blocks, starting 1 month after the channel was created, repeating each year
   * 
   * @param created the created timestamp of the channel
   * @param scheduled the scheduled timestamp of the communication
   * @param periodInMonths the number of months a period is
   * @return HashMap<String, Date> The time period details - ('start' => int, 'end' => int)
   */
  HashMap<String, Date> getPeriod(Date created, Date scheduled, int periodInMonths);

  /**
   * Get the First Month Time Period for a channel.
   * 
   * @param created the created timestamp of the channel
   * @return HashMap<String, Date> The time period details - ('start' => int, 'end' => int)
   */
  HashMap<String, Date> getFirstMonthPeriod(Date created);

  /**
   * Add a number of seconds to a given date.
   * 
   * @param date the date to add seconds to.
   * @param numberOfSeconds the number of seconds to add to the given date.
   * @return the new date with added number of seconds
   */
  Date addSeconds(Date date, int numberOfSeconds);

  /**
   * Add a number of days to a given date.
   * 
   * @param date the date to add seconds to.
   * @param numberOfDays the number of days to add to the given date.
   * @return the new date with added number of days
   */
  Date addDays(Date date, int numberOfDays);

  /**
   * Add a number of months to a given date.
   * 
   * @param date the date to add months to.
   * @param numberOfMonths the number of months to add to the given date.
   * @return the new date with added months
   */
  Date addMonths(Date date, int numberOfMonths);

}