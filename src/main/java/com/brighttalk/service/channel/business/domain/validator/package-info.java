/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains validator classes for the business domain of the Channel Service.
 */
package com.brighttalk.service.channel.business.domain.validator;