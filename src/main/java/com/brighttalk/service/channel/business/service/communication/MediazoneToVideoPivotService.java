/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: codetemplates.xml 25175 2011-01-10 14:47:45Z nbrown $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Format;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.validator.CommunicationProviderUpdateValidator;
import com.brighttalk.service.channel.business.error.DataCleanUpFailedException;
import com.brighttalk.service.channel.business.error.ProviderNotSupportedException;

/**
 * Implementation of {@link CommunicationPivotService} which pivots from a mediazone communication to a (recorded) Video
 * communication and from a (recorded) video communication to a mediazone communication. That is you can pivot forward
 * and back providing certain biz rules are met.
 * 
 */
@Service
@Qualifier(value = "mediazonetovideopivotservice")
public class MediazoneToVideoPivotService extends AbstractPivotService implements CommunicationPivotService {

  /** Delete rendtions assets error message key */
  public static final String DELETE_RENDITIONS_ERROR_MESSAGE_KEY =
      "integration.communicationAssetDb.errorOnRenditionsDelete";

  /** Delete mediazone assets error message key */
  public static final String DELETE_MEDIAZONE_ASSETS_ERROR_MESSAGE_KEY =
      "integration.communicationAssetDb.errorOnMediazoneDelete";

  private static final Logger LOGGER = Logger.getLogger(MediazoneToVideoPivotService.class);

  /** {@inheritDoc} */
  @Override
  public Provider pivot(final Communication communication, final Provider provider) {
    LOGGER.debug("Pivoting webcast [" + communication.getId() + "] to [" + provider.getName().toString() + "]");

    provider.validate(new CommunicationProviderUpdateValidator(communication, channelFinder, messages));

    // order is important: need to store previous provider before an update to reference it later on delete
    // TODO: communication should NOT be mutable (or a similar fix) - too easy to create a bug when the order matters!
    final Provider previousProvider = communication.getProvider();

    updateCommunicationOnProviderChange(communication, provider);
    deleteProviderRelatedCommunicationData(communication.getId(), previousProvider);
    restoreProviderRelatedCommunicationData(communication.getId(), provider);

    return communication.getProvider();
  }

  /**
   * Delete the provider related communication data based on the webcast provider.
   * 
   * @param communicationId the id of the communication
   * @param provider the webcast provider
   */
  private void deleteProviderRelatedCommunicationData(final Long communicationId, final Provider provider) {

    final List<String> errors = new ArrayList<String>();

    String providerName = provider.getName();

    if (providerName.equals(Provider.MEDIAZONE)) {
      deleteForMediazoneProvider(communicationId, errors);
    } else if (providerName.equals(Provider.VIDEO)) {
      deleteForVideoProvider(communicationId, errors);
    } else {
      throw new ApplicationException("Provider not recognized [" + providerName + "].");
    }

    if (!CollectionUtils.isEmpty(errors)) {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Data clean up for communication [" + communicationId + "] provider update failed with ["
            + errors.size() + "] errors.");
      }
      throw new DataCleanUpFailedException("Data clean-up on update provider failed.", StringUtils.join(errors,
          MESSAGE_DELIMITER));
    }
  }

  /**
   * Method to perform deletion of webcast assets for Mediazone type webcasts.
   * 
   * @param communicationId the id of the communication to delete the data
   * @param errors list of errors
   */
  private void deleteForMediazoneProvider(final Long communicationId, final List<String> errors) {
    deleteMediazoneProviderAssets(communicationId, errors);
  }

  /**
   * Method to perform deletion of webcast assets for Video type webcasts.
   * 
   * @param communicationId the id of the communication to delete the data
   * @param errors list of errors
   */
  private void deleteForVideoProvider(final Long communicationId, final List<String> errors) {
    deleteRenditionAssets(communicationId, errors);
    deleteResources(communicationId, errors);
  }

  private void restoreProviderRelatedCommunicationData(final Long communicationId, final Provider provider) {

    final List<String> errors = new ArrayList<String>();

    if (provider.getName().equals(Provider.MEDIAZONE)) {
      restoreMediazoneCommunicationId(communicationId, errors);
    }

    if (!CollectionUtils.isEmpty(errors)) {

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Restoring data for communication [" + communicationId + "] provider update failed with ["
            + errors.size() + "] errors.");
      }
      throw new DataCleanUpFailedException("Restoring data on update provider failed.", StringUtils.join(errors,
          MESSAGE_DELIMITER));
    }
  }

  private void restoreMediazoneCommunicationId(final Long communicationId, final List<String> errors) {

    try {

      communicationDbDao.restoreMediazoneAssets(communicationId);

    } catch (Exception exception) {

      LOGGER.error("Restoring mediazone assets for communication [" + communicationId + "] failed.", exception);

      final String mediazoneAssetsDeleteErrorMessage = messages.getValue(RESTORE_RENDITIONS_ERROR_MESSAGE_KEY);
      errors.add(mediazoneAssetsDeleteErrorMessage);
    }
  }

  /**
   * Update communication on provider change.
   * 
   * Currently supports only scenario where provider is changed from 'brighttalk' to 'mediazone' hence all the actions
   * like clearing the telephony and thumbnail details.
   * 
   * @param communicationId the id of the communication that is to be updated
   * @param provider the new provider value
   * 
   * @return updated communication
   */
  private Communication updateCommunicationOnProviderChange(final Communication communication,
      final Provider targetProvider) {

    final String currentProvider = communication.getProvider().getName();

    if (targetProvider.getName().equals(Provider.MEDIAZONE)) {

      if (currentProvider.equals(Provider.VIDEO)) {
        updateProviderFromVideoToMediazone(communication);
      }

    } else if (targetProvider.getName().equals(Provider.VIDEO)) {
      communication.setFormat(Format.VIDEO);

    } else {
      // we should not ever get here - guaranteed by validation
      throw new ProviderNotSupportedException("Not allowed to pivot provider from ["
          + communication.getProvider().getName() + "] to [" + targetProvider.getName() + "].");
    }

    LOGGER.info("Removing thumbnail and preview urls for communication [" + communication.getId()
        + "]. The values were: thumbnail [" + communication.getThumbnailUrl() + "], preview ["
        + communication.getPreviewUrl() + "].");

    communication.setThumbnailUrl(null);
    communication.setPreviewUrl(null);
    communication.setProvider(targetProvider);

    communicationDbDao.pivot(communication);

    return communication;
  }

  /**
   * Set communication details on update from Video to Mediazone type webcast.
   * 
   * @param communication the communication to be updated
   */
  private void updateProviderFromVideoToMediazone(final Communication communication) {
    communication.setStatus(Status.PROCESSING);
  }

  private void deleteResources(final Long communicationId, final List<String> errors) {

    List<Resource> resources = null;
    String resourcesDbDeleteErrorMessage = null;

    try {

      resources = resourceDbDao.findAll(communicationId);

    } catch (Exception findResourcesException) {

      LOGGER.error("Retrieving resources for communication [" + communicationId + "] failed.", findResourcesException);
      resourcesDbDeleteErrorMessage = messages.getValue(resourcesDbDeleteErrorMessage);
      errors.add(resourcesDbDeleteErrorMessage);
      return;
    }

    if (CollectionUtils.isEmpty(resources)) {
      return;
    }

    for (Resource resource : resources) {
      try {

        resourceDbDao.delete(communicationId, resource.getId());

      } catch (Exception exception) {

        LOGGER.error(
            "Deleting resource [" + resource.getId() + "] for communication [" + communicationId + "] failed.",
            exception);

        resourcesDbDeleteErrorMessage = messages.getValue(DELETE_RESOURCES_ERROR_MESSAGE_KEY,
            new Object[] { resource.getId() });
        errors.add(resourcesDbDeleteErrorMessage);
      }
    }
  }

  private void deleteMediazoneProviderAssets(final Long communicationId, final List<String> errors) {

    try {

      communicationDbDao.removeMediazoneAssets(communicationId);

    } catch (Exception exception) {

      LOGGER.error("Deleting mediazone assets for communication [" + communicationId + "] failed.", exception);

      final String mediazoneAssetsDeleteErrorMessage = messages.getValue(DELETE_MEDIAZONE_ASSETS_ERROR_MESSAGE_KEY);
      errors.add(mediazoneAssetsDeleteErrorMessage);
    }
  }

  private void deleteRenditionAssets(final Long communicationId, final List<String> errors) {

    try {

      renditionDbDao.removeRenditionAssets(communicationId);

    } catch (Exception exception) {

      LOGGER.error("Deleting renditions for communication [" + communicationId + "] failed.", exception);

      final String mediazoneAssetsDeleteErrorMessage = messages.getValue(DELETE_RENDITIONS_ERROR_MESSAGE_KEY);
      errors.add(mediazoneAssetsDeleteErrorMessage);
    }
  }

}
