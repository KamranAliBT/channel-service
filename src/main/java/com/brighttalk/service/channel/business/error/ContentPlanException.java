/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContentPlanExceededException.java 62869 2013-04-09 09:17:41Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that when booking or updating a communication, the content plan has been reached.
 */
@SuppressWarnings("serial")
public class ContentPlanException extends ApplicationException {

  /**
   * Error code for the content plan exception classe.
   */
  public static enum ErrorCode {
    /**
     * Booking not allowed error code.
     * 
     * This is thrown when the content plan has been set to 0.
     */
    BookingNotAllowed,
    /**
     * First month reached error code.
     * 
     * This is thrown when the content plan has been reached for the first month.
     */
    FirstMonthLimitReached,
    /**
     * Period reached error code
     * 
     * This is thrown when the content plan has been reach for the given period.
     */
    PeriodLimitReached,
    /**
     * Content plan default exception
     */
    ContentPlanException;
  }

  /** Content plan default error code */
  public static final String ERROR_CODE = ErrorCode.ContentPlanException.toString();

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE}.
   */
  private String errorCode = ERROR_CODE;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public ContentPlanException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public ContentPlanException(final String message, final String errorCode) {
    super(message, errorCode);
    this.setUserErrorCode(errorCode);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public ContentPlanException(final String message) {
    super(message, ERROR_CODE);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

  /**
   * Sets the errorCode to allow overriding default one.
   * 
   * @param errorCode the new error code
   */
  public void setUserErrorCode(final String errorCode) {
    this.errorCode = errorCode;
  }
}