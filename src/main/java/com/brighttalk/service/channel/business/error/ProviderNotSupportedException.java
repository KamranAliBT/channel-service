/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ProviderNotSupportedException.java 71274 2013-11-26 14:01:47Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that the requested communication provider update cannot be updated to the given provider value.
 */
@SuppressWarnings("serial")
public class ProviderNotSupportedException extends ApplicationException {

  private static final String ERROR_CODE_NOT_FOUND_ERROR = "ProviderNotSupported";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_NOT_FOUND_ERROR}.
   */
  private final String errorCode = ERROR_CODE_NOT_FOUND_ERROR;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public ProviderNotSupportedException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public ProviderNotSupportedException(final String message, final String errorCode) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public ProviderNotSupportedException(final String message) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
  }
  
  /**
   * @param message the detail message, for internal consumption.
   * 
   * @param errorArgs An array of objects to be used to replace placeholders/tokens in the user error message 
   * which is resolved using the {@code userErrorCode}.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public ProviderNotSupportedException(final String message, Object[] errorArgs) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR, errorArgs);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }
}