/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelCreateValidator.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import java.util.ArrayList;
import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.error.InvalidChannelException;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;

/**
 * Business Layer Validator for a channel creation.
 */
public class ChannelCreateValidator implements Validator<Channel> {

  /**
   * The message error code indicating that channel has id set. Channel to be created should not have id.
   */
  protected static final String ERRORCODE_REDUNDANT_CHANNEL_ID_PROVIDED = "RedundantChannelIdProvided";

  /**
   * The message error code indicating that channel name already exists.
   */
  protected static final String ERRORCODE_DUPLICATE_CHANNEL_TITLE = "DuplicateChannelTitle";

  private ChannelDbDao channelDbDao;

  private List<String> errors = new ArrayList<String>();
  
  /**
   * Construct this validator with the channel database dao.
   * 
   * @param channelDbDao dao used to perform validation
   */
  public ChannelCreateValidator(ChannelDbDao channelDbDao) {

    this.channelDbDao = channelDbDao;
  }

  @Override
  public void validate(Channel channel) {

    assertNoChannelId(channel);
    assertTitleNotDuplicate(channel);

    throwErrors();
  }

  private void assertNoChannelId(Channel channel) {

    if (channel.hasId()) {

      errors.add(ERRORCODE_REDUNDANT_CHANNEL_ID_PROVIDED);
    }
  }

  private void assertTitleNotDuplicate(Channel channel) {

    if (channelDbDao.titleInUse(channel.getTitle())) {

      errors.add(ERRORCODE_DUPLICATE_CHANNEL_TITLE);
    }
  }

  private void throwErrors() {

    if (!errors.isEmpty()) {

      throw new InvalidChannelException("Invalid channel " + errors, errors.toString());
    }
  }
}