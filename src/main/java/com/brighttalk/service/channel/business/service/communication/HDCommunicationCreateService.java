/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PinGenerator;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.validator.CommunicationCreateValidator;

/**
 * Implementation of the {@link CommunicationCreateService} that manages the creation (booking) of High Definition (aka
 * Pro) webcasts.
 * 
 * 
 * Note: "@Primary" Indicates that a bean should be given preference when multiple candidates are qualified to autowire
 * a single-valued dependency.
 * 
 * "@Transaction" Indicates that if the DAO methods throw an Exception, the inserted data will be roll backed to the
 * previous state. Propagation.SUPPORTS means that if a transaction exist it'll be supported else the method will be
 * executed as non-tranasctional.
 */
@Service
@Primary
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class HDCommunicationCreateService extends AbstractHDCommunicationService implements CommunicationCreateService {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(HDCommunicationCreateService.class);

  @Autowired
  private PinGenerator pinGenerator;

  @Value("${live.url}")
  private String liveUrl;

  /**
   * {@inheritDoc}
   * 
   * "@Transactional" means that the method will be executed transactionally. If an Exception is being thrown, all the
   * changes made will be reverted to a previous known state. Propagation.REQUIRED means that the transaction is always
   * required for this method. The Rollback will happen when an exception has been caught.
   */
  @Override
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
  public Long create(final User user, final Channel channel, final Communication communication) {
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Booking an HD webcast in channel [" + communication.getChannelId() + "].");
    }

    // The user must be a channel owner or a manger to create a webcast.
    channel.isAuthorised(user);

    // Validate the supplied communication provider.
    // Assert the supplied communication content plan.
    // Assert Business Rules:
    // check if the communicaiton to be created overlapps with another one in the channel.
    // Validate the communicaiton business rules.

    // do business rule validation.
    this.validateCommunicationIsBrighTALKHD(communication); // only brighttalkHD is supported for now.
    contentPlan.assertForCreate(user, channel, communication); // make sure content plan rules are not violated
    boolean isCommunicationOverlapping = communicationDbDao.isOverlapping(communication);
    communication.validate(new CommunicationCreateValidator(channel, isCommunicationOverlapping));// validate the
                                                                                                  // business rules
                                                                                                  // around create.

    String pinNumber = pinGenerator.generatePinNumber();

    communication.setStatus(Status.UPCOMING);
    communication.setPinNumber(pinNumber);
    communication.setLiveUrl(liveUrl);

    // Add quotes to keywords
    addQuotesToKeywords(communication);

    // Creates the communication in the DB.
    Communication createdCommunication = communicationDbDao.create(communication);

    // Get the live phone number and update the newly created communication
    String livePhoneNumber = hDBookingServiceDao.createBooking(createdCommunication);
    createdCommunication.setLivePhoneNumber(livePhoneNumber);
    communicationDbDao.updateLivePhoneNumber(createdCommunication);

    // set the communication as an asset of the channel
    communicationDbDao.createChannelAsset(createdCommunication);

    if (communication.hasConfiguration()) {
      CommunicationConfiguration configuration = communication.getConfiguration();
      configuration.setId(createdCommunication.getId());
      configurationDbDao.create(communication.getConfiguration());
    }

    if (communication.hasCategories()) {
      List<CommunicationCategory> categories = communication.getCategories();
      for (CommunicationCategory category : categories) {
        category.setCommunicationId(createdCommunication.getId());
        categoryDbDao.create(category);
      }
    }

    // let other relevant services know the communication has been created
    informOtherServicesOfCreate(communication);

    LOGGER.info("Communication [" + createdCommunication.getId() + "] has been successfully created.");

    return createdCommunication.getId();
  }

  /**
   * Inform other services the communication has been created.
   * 
   * @param communication the communication created from the request.
   */
  private void informOtherServicesOfCreate(final Communication communication) {
    LOGGER.debug("Inform other services that communication [" + communication.getId()
        + "] has been successfully created.");

    // send the communication scheduled email to JMS queue
    emailQueuer.create(communication.getId());

    // if we have a feature image, tell the image processing service to process it (produce thumbnail and feature
    // image).
    if (communication.hasPreviewUrl()) {
      imageConverterQueuer.inform(communication.getId());
    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void emailCreateCallback(final Long webcastId) {
    LOGGER.info("Callback to send create webcast [" + webcastId + "] in Email.");

    Communication communication = findCommunication(webcastId);
    emailServiceDao.sendCommunicationCreateConfirmation(communication);
  }

}
