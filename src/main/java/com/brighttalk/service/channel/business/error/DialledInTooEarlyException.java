/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DialledInTooEarlyException.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates a presenter had dialled in too early to present a webcast.
 */
@SuppressWarnings("serial")
public class DialledInTooEarlyException extends ApplicationException {

  /** Too early error code */
  public static final String ERROR_CODE_DIALLED_IN_TOO_EARLY_ERROR = "TooEarly";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_DIALLED_IN_TOO_EARLY_ERROR}.
   */
  private final String errorCode = ERROR_CODE_DIALLED_IN_TOO_EARLY_ERROR;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public DialledInTooEarlyException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public DialledInTooEarlyException(final String message, final String errorCode) {
    super(message, ERROR_CODE_DIALLED_IN_TOO_EARLY_ERROR);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public DialledInTooEarlyException(final String message) {
    super(message, ERROR_CODE_DIALLED_IN_TOO_EARLY_ERROR);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

}
