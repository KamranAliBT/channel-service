/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: PaginatedChannelFeed.java 83974 2014-09-29 11:38:04Z jbridger $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria.CommunicationStatusFilter;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;

/**
 * A (Paginated) Channel Feed.
 * <p>
 * Represents a paginated list of communication on a channel. Used by the presentation later to generate the various
 * paginated channel feeds required by clients, e.g. the ATOM feed for a channel and the RSS feed for a channel.
 * <p>
 * If the feed is empty, behavior of the pagination methods is currently undefined.
 * <p>
 * Not thread safe. Must be used in a thread safe context.
 */
public class PaginatedChannelFeed {

  private static final int NOT_SET = -1;

  /**
   * The channel this feed relates to.
   */
  private Channel channel;

  /**
   * A page of communications in this feed (up to the page size limit).
   */
  private List<Communication> allCcommunications = new ArrayList<Communication>();

  /**
   * The subset of all communications that are not syndicated.
   */
  private List<Communication> nonSyndicatedCommunications;

  /**
   * The subset of all communications that are not syndicated out.
   * 
   * Note - Includes communications that are syndicated in.
   */
  private List<Communication> notSyndicatedOutCommunications;

  /**
   * The subset of all communications that are syndicated out.
   * <p>
   * This is needed because some of the feed values for syndicated out communications are calculately different for non
   * syndication out communications (yes its true). For example viewing, ratings and registrations are calculated across
   * all channels a communication is syndicated out to. Wheras for a non syndicated out communication they are
   * caclulcated only for the master channel for that communication.
   */
  private List<Communication> syndicatedOutCommunications;

  /**
   * The total number of communications that meet the criteria for being in the feed.
   */
  private int totalNumberOfCommunications;

  /**
   * Search criteria associated with the channel feed results.
   */
  private ChannelFeedSearchCriteria searchCriteria;

  /**
   * The total number of pages in this feed (derived from totalNumberOfCommunications and pageSize).
   */
  private int numberOfPages = NOT_SET;

  private boolean empty = true;

  /**
   * Construct this empty feed (no channel, no communications).
   */
  public PaginatedChannelFeed() {
    empty = true;
  }

  /**
   * Construct feed with just a channel. All the other class members are just set to default values. These values are 0
   * for all the counters and the all communications collection is instantiated to avoid null pointer exceptions.
   * 
   * @param channel the channel this feed is for.
   */
  public PaginatedChannelFeed(final Channel channel) {
    super();

    if (channel == null) {
      throw new IllegalStateException("Cannot construct feed with a null channel.");
    }
    this.channel = channel;
    empty = false;
  }

  /**
   * Construct this feed with a channel, communications and paginated numbers.
   * 
   * @param channel the channels this feed is for.
   * @param communications a page of communications.
   * @param totalNumberOfCommunications the total number of communications in the DB (needed for pagination
   * calculcations).
   * @param searchCriteria the search criteria used to get this feed result.
   */
  public PaginatedChannelFeed(final Channel channel, final List<Communication> communications,
      final int totalNumberOfCommunications, ChannelFeedSearchCriteria searchCriteria) {
    super();

    if (channel == null) {
      throw new IllegalStateException("Cannot construct a feed with a null channel.");
    }
    this.channel = channel;

    if (communications == null || communications.isEmpty()) {
      allCcommunications = new ArrayList<Communication>();
    } else {
      allCcommunications = communications;
    }
    this.totalNumberOfCommunications = totalNumberOfCommunications;
    empty = false;

    this.searchCriteria = searchCriteria;
  }

  /**
   * Get the channel this feed is for.
   * 
   * @return the channel
   */
  public Channel getChannel() {

    if (channel == null) {
      channel = new Channel();
    }
    return channel;
  }

  /**
   * Get all the communications in this feed.
   * 
   * @return the communications or an empty list if the feed is empty.
   */
  public List<Communication> getAllCommunications() {
    return allCcommunications;
  }

  public void setAllCommunications(final List<Communication> value) {

    allCcommunications = value;
  }

  /**
   * Get the non syndicated communications from this feed.
   * 
   * @return the non syndicated communications or an empty list if the feed is empty.
   */
  public List<Communication> getNonSyndicatedCommunications() {
    if (nonSyndicatedCommunications == null) {
      buildNonSyndicatedCommunicationsList();
    }
    return nonSyndicatedCommunications;
  }

  /**
   * Get a list of the communications which are not syndicated out.
   * 
   * This will include communications that are not syndicated and those that are syndicated in to this channel.
   * 
   * @return the list of communications
   */
  public List<Communication> getCommunicationsNotSyndicatedOut() {

    if (notSyndicatedOutCommunications != null) {
      return notSyndicatedOutCommunications;
    }

    notSyndicatedOutCommunications = new ArrayList<Communication>();

    for (Communication communication : allCcommunications) {
      if (communication.getConfiguration().getCommunicationSyndication().isNotSyndicated()
          || communication.getConfiguration().getCommunicationSyndication().isSyndicatedIn()) {
        notSyndicatedOutCommunications.add(communication);
      }
    }
    return notSyndicatedOutCommunications;
  }

  /**
   * Get the syndicated out communications from this feed.
   * 
   * @return the syndicated out communications or an empty list if the feed is empty.
   */
  public List<Communication> getSyndicatedOutCommunications() {
    if (syndicatedOutCommunications == null) {
      buildSyndicatedOutCommunicationsList();
    }
    return syndicatedOutCommunications;
  }

  /**
   * Get the total number of communications this feed represents.
   * 
   * @return the total number of communications (as in the DB)
   */
  public int getTotalNumberOfCommunications() {
    return totalNumberOfCommunications;
  }

  /**
   * Get the page size for this feed.
   * 
   * @return the page size.
   */
  public int getPageSize() {
    return searchCriteria == null ? 0 : searchCriteria.getPageSize();
  }

  /**
   * Get the current page number for this feed.
   * 
   * @return the current page number.
   */
  public int getCurrentPageNumber() {
    return searchCriteria == null ? 0 : searchCriteria.getPageNumber();
  }

  /**
   * Get the next page number for this feed.
   * 
   * @return the next page number or the last page number if this feed is at the last page.
   */
  public int getNextPageNumber() {
    if (isLastPage()) {
      return getCurrentPageNumber();
    } else {
      return getCurrentPageNumber() + 1;
    }
  }

  /**
   * Get the previous page number for this feed.
   * 
   * @return the previous page number or the first page number if this feed is at the first page.
   */
  public int getPreviousPageNumber() {
    if (isFirstPage()) {
      return getFirstPageNumber();
    } else {
      return getCurrentPageNumber() - 1;
    }
  }

  /**
   * Get the first page number for this feed.
   * 
   * @return 1 - always returns 1
   */
  public int getFirstPageNumber() {
    return 1;
  }

  /**
   * Get the last page number for this feed.
   * 
   * @return the last page number.
   */
  public int getLastPageNumber() {
    return getNumberOfPages();
  }

  /**
   * Indicate if this feed is empty - contains no channel and no communications.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isEmpty() {
    return empty;
  }

  /**
   * Indicate if this feed is on the first page.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isFirstPage() {
    return getCurrentPageNumber() == 1;
  }

  /**
   * Indicate if this feed on the last page.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean isLastPage() {
    return getCurrentPageNumber() == getLastPageNumber();
  }

  /**
   * Get the number of pages in this feed.
   * 
   * @return the number of pages or zero if this feed does not have a page size.
   */
  public int getNumberOfPages() {

    if (getPageSize() == 0) {
      return 0;
    }

    if (numberOfPages == NOT_SET) {
      // see http://www.tonymarston.net/php-mysql/pagination.html
      // need to make a float quotient helper variable, otherwise dividing int/int results in int.
      float quotient = (float) totalNumberOfCommunications / getPageSize();
      numberOfPages = (int) Math.ceil(quotient);
    }

    return numberOfPages;
  }

  /**
   * Helper to build the list of syndicated out communications.
   */
  private void buildSyndicatedOutCommunicationsList() {

    syndicatedOutCommunications = new ArrayList<Communication>();

    for (Communication communication : getAllCommunications()) {
      if (communication.getConfiguration().getCommunicationSyndication().isSyndicatedOut()) {
        syndicatedOutCommunications.add(communication);
      }
    }
  }

  /**
   * Helper to build the list of non syndicated communications.
   */
  private void buildNonSyndicatedCommunicationsList() {

    nonSyndicatedCommunications = new ArrayList<Communication>();

    for (Communication communication : getAllCommunications()) {
      CommunicationConfiguration communicationConfiguration = communication.getConfiguration();
      CommunicationSyndication communicationSyndication = communicationConfiguration.getCommunicationSyndication();

      if (communicationSyndication.isNotSyndicated()) {
        nonSyndicatedCommunications.add(communication);
      }
    }
  }

  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder("PaginatedChannelFeed\n");

    builder.append("[channelId=" + getChannel().getId() + "]\n");
    builder.append("[currentPageNumber=" + getCurrentPageNumber() + "]\n");
    builder.append("[pageSize=" + getPageSize() + "]\n");
    builder.append("[numberOfPage=" + getNumberOfPages() + "]\n");
    builder.append("[totalNumberOfCommunications=" + getTotalNumberOfCommunications() + "]\n");

    return builder.toString();
  }

  /**
   * Gets the communication status filters associated with this feed result.
   * 
   * @return Communication status filters.
   */
  public List<CommunicationStatusFilter> getCommunicationStatusFilter() {

    return searchCriteria == null ? Collections.<CommunicationStatusFilter>emptyList()
        : searchCriteria.getCommunicationStatusFilter();
  }
}
