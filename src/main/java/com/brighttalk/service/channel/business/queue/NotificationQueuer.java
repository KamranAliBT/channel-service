/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CreateChannelQueue.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

/**
 * Interface for informing NOTIFICATION service of a webcast status change.
 * 
 * The operations that need to be done asynchronously include tasks that must be performed by other services.
 * 
 * To perform those operations service API has a separate callback method.
 */
public interface NotificationQueuer {

  /**
   * Informing asynchronously notification service of a webcast status change.
   * 
   * @param webcastId of the webcast that has ended
   */
  void inform(Long webcastId);
}
