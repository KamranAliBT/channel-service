/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultSurveyService.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.brighttalk.common.form.domain.Form;
import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.error.SurveyNotFoundException;
import com.brighttalk.service.channel.integration.database.SurveyDbDao;
import com.brighttalk.service.channel.integration.survey.SurveyServiceDao;

/**
 * Default implementation of the {@link SurveyService}.
 */
@Service
@Primary
public class DefaultSurveyService implements SurveyService {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(DefaultSurveyService.class);

  @Autowired
  private SurveyDbDao surveyDbDao;

  @Autowired
  private SurveyServiceDao surveyServiceDao;

  /** {@inheritDoc} */
  @Override
  public Survey findSurvey(Long surveyId) {
    Survey survey = this.surveyDbDao.findChannelSurvey(surveyId);

    if (survey == null) {
      survey = this.surveyDbDao.findCommunicationSurvey(surveyId);
    }

    if (survey == null) {
      throw new SurveyNotFoundException("No channel found for survey [" + surveyId + "].", "Survey not found ["
          + surveyId + "]");
    }
    return survey;
  }

  /** {@inheritDoc} */
  @Override
  public Survey loadSurvey(Survey survey) {
    Form form = this.surveyServiceDao.getSurveyForm(survey.getId());

    if (form == null) {
      throw new SurveyNotFoundException("Survey Id [" + survey.getId() + "] could not be found");
    }
    survey.setForm(form);

    return survey;
  }

  public void setSurveyDbDao(SurveyDbDao surveyDbDao) {
    this.surveyDbDao = surveyDbDao;
  }

  public void setSurveyServiceDao(SurveyServiceDao surveyServiceDao) {
    this.surveyServiceDao = surveyServiceDao;
  }

}
