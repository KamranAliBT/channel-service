/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultChannelManagerService.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;
import com.brighttalk.service.channel.business.queue.AuditQueue;
import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;
import com.brighttalk.service.channel.integration.audit.dto.builder.AuditRecordDtoBuilder;
import com.brighttalk.service.channel.integration.database.ChannelManagerDbDao;
import com.brighttalk.service.channel.integration.user.UserServiceDao;
import com.brighttalk.service.channel.presentation.util.UserSearchCriteriaBuilder;

/**
 * Default implementation of the {@link ChannelManagerService}.
 */
@Service
@Primary
public class DefaultChannelManagerService implements ChannelManagerService {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(DefaultChannelManagerService.class);

  @Autowired
  private ChannelFinder channelFinder;

  @Autowired
  @Qualifier("localUserServiceDao")
  private UserServiceDao userServiceDao;

  @Autowired
  private ChannelManagerDbDao channelManagerDbDao;

  @Autowired
  private UserSearchCriteriaBuilder userSearchCriteriaBuilder;

  @Autowired
  private ChannelManagerValidator validator;

  @Autowired
  @Qualifier("channelManagerAuditRecordDtoBuilder")
  private AuditRecordDtoBuilder auditRecordDtoBuilder;

  @Autowired
  private AuditQueue auditQueue;

  /** {@inheritDoc} */
  @Override
  public List<ChannelManager> create(final User user, final Long channelId,
      final List<ChannelManager> channelManagers) {

    LOGGER.debug("Creating channel managers [" + channelManagers + "] for channel [" + channelId + "].");

    Channel channel = channelFinder.find(channelId);

    validator.assertCreate(user, channel, channelManagers);

    List<ChannelManager> createdChannelManagers = create(channel, channelManagers);

    // Audit the created channel manager event.
    if (!CollectionUtils.isEmpty(createdChannelManagers)) {
      AuditRecordDto auditRecordDto =
          auditRecordDtoBuilder.create(user.getId(), channelId, null,
              "User ids: " + extractChannelManagerUserIds(createdChannelManagers) + " created as channel managers.");
      auditQueue.create(auditRecordDto);
    }

    LOGGER.debug("Channel managers [" + createdChannelManagers + "] created.");

    return createdChannelManagers;
  }

  private List<ChannelManager> create(final Channel channel, final List<ChannelManager> channelManagers) {

    List<String> emailAddresses = getEmailAddresses(channelManagers);
    List<User> users = findUsersByEmailAddress(emailAddresses);

    ChannelManager channelManager;
    ChannelManager createdChannelManager;
    List<ChannelManager> createdChannelManagers = new ArrayList<ChannelManager>();

    for (User user : users) {
      channelManager = new ChannelManager();
      channelManager.setUser(user);

      createdChannelManager = create(channel, channelManager);
      createdChannelManager.setUser(user);

      createdChannelManagers.add(createdChannelManager);
    }

    return createdChannelManagers;
  }

  private ChannelManager create(final Channel channel, final ChannelManager channelManager) {

    LOGGER.debug("Creating channel manager [" + channelManager + "] for channel [" + channel + "].");

    ChannelManager createdChannelManager = channelManagerDbDao.create(channel, channelManager);

    LOGGER.debug("Channel manager [" + channelManager + "] created.");

    return createdChannelManager;
  }

  private List<String> getEmailAddresses(final List<ChannelManager> channelManagers) {

    List<String> emailAddresses = new ArrayList<String>();

    for (ChannelManager channelManager : channelManagers) {
      emailAddresses.add(channelManager.getUser().getEmail());
    }

    return emailAddresses;
  }

  /** {@inheritDoc} */
  @Override
  public ChannelManager update(final User user, final Long channelId,
      final ChannelManager channelManager) {

    LOGGER.debug("Updating channel manager [" + channelManager.getId() + "].");

    Channel channel = channelFinder.find(channelId);

    validator.assertUpdate(user, channel, channelManager);

    ChannelManager updatedChannelManager = channelManagerDbDao.update(channelManager);

    loadChannelManagerDetails(updatedChannelManager);

    // Audit the update channel manager event.
    AuditRecordDto auditRecordDto = auditRecordDtoBuilder.update(user.getId(), channelId, null,
        "Updated user id [" + updatedChannelManager.getUser().getId() + "].");
    auditQueue.create(auditRecordDto);

    LOGGER.debug("Channel manager [" + channelManager.getId() + "] updated.");

    return updatedChannelManager;
  }

  /** {@inheritDoc} */
  @Override
  public void delete(final User user, final Long channelId, final Long channelManagerId) {

    LOGGER.debug("Deleting channel manager [" + channelManagerId + "].");

    Channel channel = channelFinder.find(channelId);

    validator.assertDelete(user, channel, channelManagerId);

    // Added part of AP-209 - Retrieving channel manager before deleting to audit the channel manager user Id.
    ChannelManager channelManager = channelManagerDbDao.find(channelManagerId);

    // Delete the supplied channel manager.
    channelManagerDbDao.delete(channelManagerId);

    // Audit the delete channel manager event.
    AuditRecordDto auditRecordDto = auditRecordDtoBuilder.delete(user.getId(), channel.getId(), null,
        "Deleted channel manager with user id [" + channelManager.getUser().getId() + "].");
    auditQueue.create(auditRecordDto);

    LOGGER.debug("Channel manager [" + channelManagerId + "] deleted.");
  }

  /** {@inheritDoc} */
  @Override
  public void delete(final Long userId) {

    LOGGER.debug("Deleting user [" + userId + "] from channel managers.");

    channelManagerDbDao.deleteByUser(userId);

    LOGGER.debug("User [" + userId + "] deleted from channel managers.");
  }

  private List<User> findUsersByEmailAddress(final List<String> emailAddresses) {

    UsersSearchCriteria criteria = userSearchCriteriaBuilder.buildWithDefaultValues();

    List<User> users = userServiceDao.findByEmailAddress(emailAddresses, criteria);

    return users;
  }

  private void loadChannelManagerDetails(final ChannelManager channelManager) {

    User user = userServiceDao.find(channelManager.getUser().getId());
    channelManager.setUser(user);
  }

  private Set<Long> extractChannelManagerUserIds(List<ChannelManager> channelManagers) {
    Set<Long> userIds = new HashSet<>();
    for (ChannelManager channelManager : channelManagers) {
      userIds.add(channelManager.getUser().getId());
    }
    return userIds;
  }

}