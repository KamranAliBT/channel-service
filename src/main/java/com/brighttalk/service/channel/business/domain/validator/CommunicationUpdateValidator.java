/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationUpdateValidator.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import org.apache.log4j.Logger;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.InvalidCommunicationException;

/**
 * Business Layer Validator for a communication update.
 */
public class CommunicationUpdateValidator implements Validator<Communication> {

  /** This class logger. */
  protected static final Logger LOGGER = Logger.getLogger(CommunicationUpdateValidator.class);

  /** Channel in which the communication is being updated. */
  private final Channel channel;

  /** is communication overlapping status */
  private final boolean isCommunicationOverlapping;

  /**
   * Construct this validator with the communication database dao.
   * 
   * @param channel the channel in which the communication is going to be updated.
   * @param isCommunicationOverlapping used to validate if a communication is overlapping with anothe rone on the
   * channel.
   */
  public CommunicationUpdateValidator(final Channel channel, final boolean isCommunicationOverlapping) {
    this.channel = channel;
    this.isCommunicationOverlapping = isCommunicationOverlapping;
  }

  @Override
  public void validate(final Communication communication) {
    LOGGER.debug("Validating communication business rules for update.");

    assertCommunicationDuration(communication, channel);
    assertFutureOnlyRule(communication);
    assertDoesNotOverlap();
  }

  /**
   * Assert that the communication duration does not exceed the max communication duration set at the channel level.
   * Note: If the value of the channel feature is set to 0, we treat it as unlimited, which is valid.
   * 
   * @param communication the communication to assert
   * @param channel the channel to assert the communication against
   */
  private void assertCommunicationDuration(final Communication communication, final Channel channel) {
    LOGGER.debug("Assert communication duration does not exceed max communication duration channel feature.");

    if (!channel.isCommunicationDurationValid(communication)) {
      throw new InvalidCommunicationException("Invalid max duration",
          InvalidCommunicationException.ErrorCode.CommunicationTooLong.toString());
    }
  }

  /**
   * Assert that the communication scheduled date and time is not in the past.
   * 
   * @param communication the communication to assert.
   */
  private void assertFutureOnlyRule(final Communication communication) {
    LOGGER.debug("Assert communication scheduled date and time is not in the past.");

    if (communication.notInTheFuture()) {
      throw new InvalidCommunicationException("Invalid scheduled time",
          InvalidCommunicationException.ErrorCode.ScheduledTimeInTheFuture.toString());
    }
  }

  /**
   * Assert that the communication updated does not overlap with an existing communication in the channel.
   */
  private void assertDoesNotOverlap() {
    LOGGER.debug("Assert communication does not overlap with another communication in the channel.");

    if (isCommunicationOverlapping) {
      throw new InvalidCommunicationException("Communication overlapps",
          InvalidCommunicationException.ErrorCode.OverlappingCommunication.toString());
    }
  }
}