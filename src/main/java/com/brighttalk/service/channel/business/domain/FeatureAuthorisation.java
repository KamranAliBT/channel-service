/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: FeatureAuthorisation.java 100001 2015-09-08 08:53:13Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * Authorise an user to perform actions on channel's features.
 */
@Component
public class FeatureAuthorisation {

  /**
   * This list contains features that a Channel Owner/Manager can update if the "mayDisableEmails" feature is enable.
   */
  @SuppressWarnings("serial")
  private static final List<Feature.Type> MAY_DISABLE_EMAILS_FEATURES = new ArrayList<Feature.Type>() {
    {
      add(Type.EMAIL_CHANNEL_SUBSCRIPTION);
      add(Type.EMAIL_COMM_24HR_REMINDER);
      add(Type.EMAIL_COMM_CANCELLED);
      add(Type.EMAIL_COMM_MISSED_YOU);
      add(Type.EMAIL_COMM_REGISTRATION_CONFIRMATION);
      add(Type.EMAIL_COMM_RERUN);
      add(Type.EMAIL_COMM_RESCHEDULED);
      add(Type.EMAIL_COMM_STARTING_NOW);
      add(Type.EMAIL_COMM_VIEWER_FOLLOWUP);
      add(Type.EMAIL_WEEKLY_EMAIL);
    }
  };

  /**
   * This list contains features that a Channel Owner/Manager can update.
   */
  @SuppressWarnings("serial")
  public static final List<Feature.Type> OWNER_MANAGER_UPDATE_FEATURES = new ArrayList<Feature.Type>() {
    {
      addAll(MAY_DISABLE_EMAILS_FEATURES);
    }
  };

  /**
   * This list contains features that a Channel Owner/Manager can view.
   */
  @SuppressWarnings("serial")
  public static final List<Feature.Type> OWNER_MANAGER_VIEW_FEATURES = new ArrayList<Feature.Type>() {
    {
      addAll(OWNER_MANAGER_UPDATE_FEATURES);
    }
  };

  /**
   * Authorise the user for update the given features.
   * 
   * @param user the user requesting the changes
   * @param channel the channel
   * @param featuresToUpdate features to update
   * 
   * @throws com.brighttalk.common.user.error.UserAuthorisationException
   */
  public void authoriseForUpdate(final User user, final Channel channel, final List<Feature> featuresToUpdate) {

    channel.isAuthorised(user);

    if (user.isManager()) {
      return;
    }

    authoriseUserForFeatures(user, channel, featuresToUpdate, OWNER_MANAGER_UPDATE_FEATURES);
    authoriseUserForMayDisableEmailsFeature(user, channel, featuresToUpdate);
  }

  private void authoriseUserForFeatures(final User user, final Channel channel, final List<Feature> featuresToUpdate,
      final List<Feature.Type> ownerManagerFeatures) {

    for (Feature feature : featuresToUpdate) {
      if (!ownerManagerFeatures.contains(feature.getName())) {
        throw new UserAuthorisationException("User [" + user.getId() + "] not authorised to update "
            + "the channel [" + channel.getId() + "] feature [" + feature.getName().getApiName() + "].");
      }
    }
  }

  private void authoriseUserForMayDisableEmailsFeature(final User user, final Channel channel,
      final List<Feature> featuresToUpdate) {

    Feature mayDisableEmailsFeature = channel.getFeature(Feature.Type.MAY_DISABLE_EMAILS);

    if (mayDisableEmailsFeature.isEnabled()) {
      return;
    }

    for (Feature feature : featuresToUpdate) {
      if (MAY_DISABLE_EMAILS_FEATURES.contains(feature.getName())) {
        throw new UserAuthorisationException("User [" + user.getId() + "] not authorised to update "
            + "the channel [" + channel.getId() + "] email features.");
      }
    }
  }

}
