/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: UserFilter.java 87022 2014-12-03 13:00:40Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.filter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.error.UserNotFoundException;

/**
 * Helper to filter subscriptions.
 */
@Component
public class UserFilter {

  @Autowired
  private CollectionFilter collectionFilter;

  /**
   * Filter an user by id.
   * 
   * @param users the users to filter
   * @param searchableUser the user to search for
   * 
   * @return the filtered user
   */
  public User filterByUserIdOrEmail(final List<User> users, final User searchableUser) {
    Predicate<User> equalsUserId = new Predicate<User>() {
      @Override
      public boolean apply(final User user) {
        return user.getId().equals(searchableUser.getId())
            || user.getEmail().equalsIgnoreCase(searchableUser.getEmail());
      }
    };

    List<User> filteredUsers = collectionFilter.filter(users, equalsUserId);

    if (CollectionUtils.isEmpty(filteredUsers)) {
      throw new UserNotFoundException(searchableUser.getId(), searchableUser.getEmail());
    }

    return filteredUsers.iterator().next();
  }

}
