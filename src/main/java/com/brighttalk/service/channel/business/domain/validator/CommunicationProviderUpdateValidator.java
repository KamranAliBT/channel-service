/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationProviderUpdateValidator.java 96319 2015-06-09 12:25:26Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.validator;

import org.apache.log4j.Logger;

import com.brighttalk.common.configuration.Messages;
import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.error.CannotUpdateCurrentProviderException;
import com.brighttalk.service.channel.business.error.CommunicationOutsideAuthoringPeriodException;
import com.brighttalk.service.channel.business.error.CommunicationStatusNotSupportedException;
import com.brighttalk.service.channel.business.error.FeatureNotEnabledException;
import com.brighttalk.service.channel.business.service.channel.ChannelFinder;

/**
 * {@link Validator} that validates whether a communication can be updated using the given target provider.
 */
public class CommunicationProviderUpdateValidator implements Validator<Provider> {

  /**
   * Import media feature not enabled message key.
   */
  protected static final String IMPORT_MEDIA_FEATURE_NOT_ENABLED_MESSAGE_KEY =
      "validation.business.provider.importMediaFeatureNotEnabled";

  /**
   * Video upload feature not enabled message key.
   */
  protected static final String VIDEO_UPLOAD_FEATURE_NOT_ENABLED_MESSAGE_KEY =
      "validation.business.provider.videoUploadFeatureNotEnabled";

  /**
   * Cannot update provider message key.
   */
  protected static final String CANNOT_UPDATE_PROVIDER_MESSAGE_KEY = "validation.business.provider.cannotUpdateCurrent";

  /**
   * Outside of authoring period message key.
   */
  protected static final String OUTSIDE_OF_AUTHORING_PERIOD_MESSAGE_KEY =
      "validation.business.provider.cannotUpdateIn15MinutesWindow";

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(CommunicationProviderUpdateValidator.class);

  private final Communication communication;

  private final Messages messages;

  private final ChannelFinder channelFinder;

  /**
   * Construct this validator with the existing communication.
   * 
   * @param communication the value of an existing communication that will be updated.
   * @param channelFinder channel finder
   * @param messages util to retrieve customized messages from message source
   */
  public CommunicationProviderUpdateValidator(final Communication communication,
      final ChannelFinder channelFinder, final Messages messages) {

    this.communication = communication;
    this.channelFinder = channelFinder;
    this.messages = messages;
  }

  /**
   * Validates if a provider can be set on an existing communication.
   * 
   * Depending on current communication provider's value different business rules apply.
   * 
   * @param provider the target provider
   */
  @Override
  public void validate(final Provider provider) {

    final String currentProvider = communication.getProvider().getName();
    final String targetProvider = provider.getName();

    if (targetProvider.equals(Provider.MEDIAZONE)) {

      validateWhenUpdatingToMediazone(currentProvider);

    } else if (targetProvider.equals(Provider.VIDEO)) {

      validateWhenUpdatingToVideo(currentProvider);

    } else {

      throwCannotUpdateCurrentProvider(targetProvider);
    }
  }

  /**
   * Validate provider when target provider is mediazone.
   * 
   * @param currentProvider the current provider
   * @param targetProvider the provider to update to
   */
  private void validateWhenUpdatingToMediazone(final String currentProvider) {

    if (currentProvider.equals(Provider.BRIGHTTALK)) {

      validateBrighttalkToMediazoneChangeScenario();

    } else if (currentProvider.equals(Provider.VIDEO)) {

      validateVideoToMediazoneChangeScenario();

    } else {

      throwCannotUpdateCurrentProvider(Provider.MEDIAZONE);
    }
  }

  /**
   * Validate provider when target provider is video.
   * 
   * @param currentProvider the current provider
   * @param targetProvider the provider to update to
   */
  private void validateWhenUpdatingToVideo(final String currentProvider) {

    if (currentProvider.equals(Provider.MEDIAZONE) || currentProvider.equals(Provider.BRIGHTTALK_HD)) {

      validateUpdateToVideoChangeScenario();

    } else {

      throwCannotUpdateCurrentProvider(Provider.VIDEO);
    }
  }

  private void validateBrighttalkToMediazoneChangeScenario() {

    assertCurrentStatus(Status.UPCOMING);
    assertCommunicationIsInAuthoringPeriod();

    assertFeatureIsEnabled(Feature.Type.MEDIA_ZONES);
  }

  private void validateUpdateToVideoChangeScenario() {

    assertCurrentStatus(Status.PROCESSING);
    assertFeatureIsEnabled(Feature.Type.ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS);
  }

  private void validateVideoToMediazoneChangeScenario() {

    assertCurrentStatus(Status.RECORDED);

    assertFeatureIsEnabled(Feature.Type.MEDIA_ZONES);
  }

  private void assertCurrentStatus(final Status requiredStatus) {

    if (!requiredStatus.equals(communication.getStatus())) {

      final String currentStatus = communication.getStatus().toString().toLowerCase();
      final String errorMessage = messages.getValue("validation.business.provider.statusNotSupported",
          new Object[] { currentStatus });

      throw new CommunicationStatusNotSupportedException("Cannot update provider for communication status ["
          + currentStatus + "].", errorMessage);
    }
  }

  private void assertCommunicationIsInAuthoringPeriod() {

    if (!communication.isInAuthoringPeriod()) {

      final String errorMessage = messages.getValue(OUTSIDE_OF_AUTHORING_PERIOD_MESSAGE_KEY);
      throw new CommunicationOutsideAuthoringPeriodException("Cannot update communication when in 15 minutes window",
          errorMessage);
    }
  }

  private void assertFeatureIsEnabled(final Feature.Type featureType) {

    final Long channelId = communication.getMasterChannelId();
    Channel channel = channelFinder.find(channelId);

    Feature feature = null;

    try {
      feature = channel.getFeature(featureType);
    } catch (Exception exception) {
      throwFeatureIsNotEnabled(featureType, channelId);
    }

    if (feature == null || !feature.isEnabled()) {
      throwFeatureIsNotEnabled(featureType, channelId);
    }
  }

  private void throwFeatureIsNotEnabled(final Feature.Type featureType, final Long channelId) {

    String errorMessage;

    switch (featureType) {

      case MEDIA_ZONES:
        errorMessage = messages.getValue(IMPORT_MEDIA_FEATURE_NOT_ENABLED_MESSAGE_KEY);
        break;

      case ENABLE_SELF_SERVICE_VIDEO_COMMUNICATIONS:

        errorMessage = messages.getValue(VIDEO_UPLOAD_FEATURE_NOT_ENABLED_MESSAGE_KEY);
        break;

      default:
        throw new ApplicationException("Requested feature [" + featureType
            + "]  is not supported. Most likely a programatic error");
    }

    throw new FeatureNotEnabledException("Required feature for provider update is not enabled on the channel ["
        + channelId + "]", errorMessage);
  }

  private void throwCannotUpdateCurrentProvider(final String targetProvider) {

    final String currentProvider = communication.getProvider().getName();
    final Object[] params = new Object[] { currentProvider, targetProvider };
    final String errorMessage = messages.getValue(CANNOT_UPDATE_PROVIDER_MESSAGE_KEY, params);

    throw new CannotUpdateCurrentProviderException("Cannot update communication provider from [" + currentProvider
        + "] to [" + targetProvider + "]", errorMessage);
  }
}