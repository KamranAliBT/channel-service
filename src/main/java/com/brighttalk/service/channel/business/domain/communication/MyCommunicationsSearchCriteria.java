/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MyCommunicationsSearchCriteria.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;

/**
 * Search Criteria for retrieving the paginated communications list related to my subscribed webcast.
 * <p>
 * Contains the page number being requested, the size of the page being requested and any filter criteria for the feed.
 * <p>
 * Flags on how the channels should be sorted are part of this class, too.
 */
public class MyCommunicationsSearchCriteria extends BaseSearchCriteria {

  private static final String CLIENT_ERROR_FLAG = "ClientError";

  private SortBy sortBy;

  private Boolean registered;

  private List<CommunicationStatusFilterType> communicationsStatusFilter;

  /**
   * 'Sort by' types.
   */
  public static enum SortBy {

    /**
     * Sort by scheduled date.
     */
    SCHEDULEDDATE,

    /**
     * .Sort by average rating.
     */
    AVERAGERATING,

    /**
     * .Sort by number of viewings.
     */
    TOTALVIEWINGS;

    /**
     * Converts a string into a ChannelSortBy.
     * 
     * An empty value means sort by default value SCHEDULEDDATE.
     * 
     * @param value to be converted
     * 
     * @return ChannelSortBy
     * 
     * @throws ApplicationException if the value is invalid 
     */
    public static SortBy convert(String value) {

      if (StringUtils.isEmpty(value)) {
        return SCHEDULEDDATE;
      }

      try {

        return SortBy.valueOf(value.trim().toUpperCase());

      } catch (IllegalArgumentException exception) {

        throw new ApplicationException("Invalid sort by value requested [" + value + "]", exception, CLIENT_ERROR_FLAG);
      }
    }
  }

  /**
   * Supported communication status filters.
   */
  public static enum CommunicationStatusFilterType {

    /**
     * Filter upcoming communication status.
     */
    UPCOMING,

    /**
     * Filter live communication status.
     */
    LIVE,

    /**
     * Filter recorded communication status.
     */
    RECORDED;

    /**
     * Converts a string into a CommunicationStatusFilterType.
     * 
     * An empty value means that no filtering is requested.
     * 
     * @param value to be converted.
     *
     * @return {@link CommunicationStatusFilterType}
     * 
     * @throws ApplicationException if the value is invalid 
     */
    public static CommunicationStatusFilterType convert(String value) {

      if (StringUtils.isEmpty(value)) {
        return null;
      }

      try {

        return CommunicationStatusFilterType.valueOf(value.trim().toUpperCase());

      } catch (IllegalArgumentException exception) {

        throw new ApplicationException("Invalid communication status filter requested [" + value + "]", exception,
            CLIENT_ERROR_FLAG);
      }
    }
  }

  public SortBy getSortBy() {
    return sortBy;
  }

  public void setSortBy(SortBy value) {
    sortBy = value;
  }

  /**
   * Indicates if this search criteria has sort by set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasSortBy() {
    return sortBy != null;
  }

  /**
   * Indicates if this criteria has communication status filter defined.
   * 
   * Default value "all" indicates that there is no filtering on communication status
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasCommunicationsStatusFilter() {
    return !CollectionUtils.isEmpty(communicationsStatusFilter);
  }

  public List<CommunicationStatusFilterType> getCommunicationStatusFilter() {
    return communicationsStatusFilter;
  }

  public void setCommunicationsStatusFilter(List<CommunicationStatusFilterType> value) {
    communicationsStatusFilter = value;
  }

  public Boolean getRegistered() {
    return registered;
  }

  public void setRegistered(Boolean value) {
    registered = value;
  }

  /**
   * Indicated if this search criteria has registered flag set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasRegistered() {
    return registered != null;
  }
}