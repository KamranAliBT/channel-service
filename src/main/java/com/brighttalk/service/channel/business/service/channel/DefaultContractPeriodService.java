/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: DefaultContractPeriodService.java 101521 2015-10-15 16:45:21Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.domain.validator.ContractPeriodCreateValidator;
import com.brighttalk.service.channel.business.domain.validator.ContractPeriodDeleteValidator;
import com.brighttalk.service.channel.business.domain.validator.ContractPeriodUpdateValidator;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.ContractPeriodNotFoundException;
import com.brighttalk.service.channel.business.error.InvalidContractPeriodException;
import com.brighttalk.service.channel.integration.database.ChannelDbDao;
import com.brighttalk.service.channel.integration.database.ContractPeriodDbDao;
import com.brighttalk.service.channel.integration.error.ContractPeriodAlreadyExistsForChannel;
import com.brighttalk.service.channel.presentation.error.ErrorCode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Default implementation of the {@link ContractPeriodService} - the Contract Period Service Business API.
 * <p>
 * Responsible for managing {@link ContractPeriod}s.
 * <p>
 */
@Service
public class DefaultContractPeriodService implements ContractPeriodService {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(DefaultContractPeriodService.class);

  /** Contract Period DAO */
  private ContractPeriodDbDao contractPeriodDbDao;

  /** Channel DAO */
  private ChannelDbDao channelDbDao;

  @Autowired
  public DefaultContractPeriodService(
      ContractPeriodDbDao contractPeriodDbDao,
      ChannelDbDao channelDbDao) {
    this.contractPeriodDbDao = contractPeriodDbDao;
    this.channelDbDao = channelDbDao;
  }

  /** {@inheritDoc} */
  @Override
  public ContractPeriod create(final User user, final ContractPeriod contractPeriod) {
    authorise(user);
    verifyChannelExists(contractPeriod.getChannelId());
    contractPeriod.validate(new ContractPeriodCreateValidator(contractPeriodDbDao));
    ContractPeriod createdContractPeriod = null;
    try {
      createdContractPeriod = contractPeriodDbDao.create(contractPeriod);
    } catch (ContractPeriodAlreadyExistsForChannel e) {
      throw new InvalidContractPeriodException(e.getMessage(), e.getUserErrorCode());
    }
    return createdContractPeriod;
  }

  /** {@inheritDoc} */
  @Override
  public ContractPeriod update(final User user, final ContractPeriod contractPeriod) {
    authorise(user);
    verifyChannelExists(contractPeriod.getChannelId());
    contractPeriod.validate(new ContractPeriodUpdateValidator(contractPeriodDbDao));
    ContractPeriod updated = contractPeriodDbDao.update(contractPeriod);
    return updated;
  }

  /** {@inheritDoc} */
  @Override
  public void delete(final User user, final Long contractPeriodId) {
    authorise(user);
    try {
      ContractPeriod contractPeriodFromDatabase = getContractPeriodFromDatabase(contractPeriodId);
      contractPeriodFromDatabase.validate(new ContractPeriodDeleteValidator());
      contractPeriodDbDao.delete(contractPeriodId);
    } catch (ContractPeriodNotFoundException e) {
      //maintain idempotence, if it's not found then just return a success
    }
  }

  /** {@inheritDoc} */
  @Override
  public ContractPeriod find(final User user, final Long contractPeriodId) {
    authorise(user);
    ContractPeriod contractPeriod = contractPeriodDbDao.find(contractPeriodId);
    return contractPeriod;
  }

  /** {@inheritDoc} */
  @Override
  public List<ContractPeriod> findByChannelId(final User user, final Long channelId) {
    authorise(user);
    verifyChannelExists(channelId);
    List<ContractPeriod> contractPeriodsByChannel = contractPeriodDbDao.findAllByChannelId(channelId);
    return contractPeriodsByChannel;
  }

  /**
   * Verify if the channel exists
   *
   * @param channelId to be checked
   */
  private void verifyChannelExists(long channelId) {
    boolean channelExists = channelDbDao.exists(channelId);
    if (!channelExists) {
      throw new ChannelNotFoundException(channelId);
    }
  }

  /**
   * Return a contract period if it exists
   *
   * @param contractPeriodId the contract period to be checked
   */
  private ContractPeriod getContractPeriodFromDatabase(long contractPeriodId) {
    try {
      return contractPeriodDbDao.find(contractPeriodId);
    } catch (ContractPeriodNotFoundException e) {
      throw new ContractPeriodNotFoundException(contractPeriodId);
    }
  }

  /**
   * The {@link User} must be a Manager to be able to create/update/delete a {@link ContractPeriod}.
   *
   * @param user to be verified for
   */
  private void authorise(final User user) {
    if (!user.isManager()) {
      throw new UserAuthorisationException("User [" + user.getId() + "] does not have the MANAGER role.",
          ErrorCode.NOT_AUTHORISED.getName());
    }
  }
}
