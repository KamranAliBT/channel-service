/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: CollectionFilter.java 87022 2014-12-03 13:00:40Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.filter;

import java.util.Collection;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.error.CollectionFilterException;

/**
 * Helper collection filter.
 */
@Component
public class CollectionFilter {

  /**
   * Helper method to filter a collection of objects.
   * 
   * @param target the collection to filter
   * @param predicate the expected comparison
   * @param <T> the Collection type
   * @param <E> the Collection element
   * 
   * @return the filtered collection
   */
  @SuppressWarnings("unchecked")
  public <E, T extends Collection<E>> T filter(final T target, final Predicate<E> predicate) {

    T result;

    try {
      result = (T) target.getClass().newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      throw new CollectionFilterException(e);
    }

    for (E element : target) {
      if (predicate.apply(element)) {
        result.add(element);
      }
    }
    return result;
  }

}
