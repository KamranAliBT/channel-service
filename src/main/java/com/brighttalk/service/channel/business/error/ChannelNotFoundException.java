/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelNotFoundException.java 69660 2013-10-14 11:30:45Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

/**
 * Indicates the channel has not been found.
 */
@SuppressWarnings("serial")
public class ChannelNotFoundException extends NotFoundException {

  private final Long channelId;

  /**
   * @param channelId see below.
   * 
   * @see ChannelNotFoundException#ChannelNotFoundException(Long, Throwable)
   */
  public ChannelNotFoundException(final Long channelId) {
    this(channelId, null);
  }

  /**
   * @param channelId see below.
   * @param cause see below.
   * 
   * @see NotFoundException#NotFoundException(String, Throwable)
   */
  public ChannelNotFoundException(final Long channelId, final Throwable cause) {
    super("Channel [" + channelId + "] not found.", cause);
    this.channelId = channelId;
  }

  public Long getChannelId() {
    return channelId;
  }

}
