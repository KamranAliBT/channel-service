/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationFinder.java 70153 2013-10-23 17:23:51Z acairns $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain;

/**
 * Pin Manager class that handles the generation of unique pin numbers. The pin number will be assigned to a
 * communication before its creation.
 */
public interface PinGenerator {

  /**
   * Generates a unique 8 digit pin number used by the communication. The pin number is cast as a string so if it starts
   * with a 0, the 0 is not being ignored.
   * 
   * @return unique pin number
   */
  String generatePinNumber();

}