/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: AuditQueue.java 78594 2014-05-28 15:11:50Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * Interface for queues containing audit record operations. This allows these operations to be carried out
 * asynchronously.
 */
public interface AuditQueue {

  /**
   * Creates an audit record asynchronously.
   * 
   * @param auditRecordDto Audit Record DTO to create audit record for.
   */
  void create(AuditRecordDto auditRecordDto);
}
