/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationUrlGenerator.java 74717 2014-02-26 11:48:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.communication;

import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * Interface to be implemented by all Communication Url Generators.
 */
public interface CommunicationUrlGenerator {

  /**
   * Generate url for communication on a channel.
   * 
   * This Method uses the class level uri template variable as a template - This needs to be set when the bean is
   * instantiated in Spring config.
   * 
   * @param channel details.
   * @param communication details
   * 
   * @return the generated url
   */
  String generate( Channel channel, Communication communication );
}
