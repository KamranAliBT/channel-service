/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationService.java 72767 2014-01-20 17:54:34Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.communication;

import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * The Communication Cancel Service Business API.
 * <p>
 * The Communication Cancel API is responsible for cancelling communications (aka webcasts) and
 * informing other services of the cancel event.
 * <p>
 */
public interface CommunicationCancelService {

  /**
   * Cancels the communication and publishes the cancel event to other services that need to know.
   * 
   * @param communication to be cancelled
   * @return the id of the cancelled communication
   */
  Long cancel(Communication communication);

  /**
   * Callback to handle any asynchronous cancel webcast messages to be sent to the email service.
   * 
   * @param webcastId id of the cancelled webcast.
   */
  void emailCancelCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous cancel webcast operations to HD Live Event Service. 
   * The HD Live Event service needs to know a webcast has been cancelled so it can discard it from its state. 
   * 
   * @param webcastId id of the webcast to be cancelled.
   */
  void lesCancelCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous cancel webcast messages to be sent to the Summit Service. 
   * The Summit service needs to know a webcast has been cancelled so it can discard it from any relevant summits. 
   * 
   * @param webcastId id of the webcast to be cancelled.
   */
  void summitDeleteCallback(Long webcastId);

  /**
   * Callback to handle any asynchronous cancel webcast messages to be sent to the Community Service. 
   * The Community service needs to know a webcast has been cancelled so it can discard it from any relevant communities. 
   * 
   * @param webcastId id of the webcast to be cancelled.
   */
  void communityDeleteCallback(Long webcastId);
}
