/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SurveyNotFoundException.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates a survey has not been found.
 */
@SuppressWarnings("serial")
public class SurveyNotFoundException extends ApplicationException {

  private static final String ERROR_CODE_SURVEY_NOT_FOUND_ERROR = "SurveyNotFound";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_SURVEY_NOT_FOUND_ERROR}.
   */
  private String errorCode = ERROR_CODE_SURVEY_NOT_FOUND_ERROR;

  /**
   * @param logMessage the message provided in the log file.
   * @param userMessage the message provided in client response.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public SurveyNotFoundException(String logMessage, String userMessage) {
    super(logMessage, ERROR_CODE_SURVEY_NOT_FOUND_ERROR);
    this.setUserErrorMessage(userMessage);
  }
  
  /**
   * @param logMessage the message provided in the log file.
   * @param cause The {@link Throwable} which is the source of the original exception, which the created exception
   * should wrap.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public SurveyNotFoundException(String logMessage, Throwable cause) {
    super(logMessage, cause);
  }
  
  /**
   * @param userMessage message provided in client response.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public SurveyNotFoundException(String userMessage) {
    super(userMessage, ERROR_CODE_SURVEY_NOT_FOUND_ERROR);
    this.setUserErrorMessage(userMessage);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }

}
