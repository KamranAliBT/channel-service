/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2012.
 * All Rights Reserved.
 * $Id: DefaultAuthorisationService.java 78460 2014-05-23 10:58:44Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.error.UserAuthorisationException;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Default Implementation of the {@link AuthorisationService}.
 */
@Service
@Primary
public class DefaultAuthorisationService implements AuthorisationService {

  /**
   * {@inheritDoc}
   */
  @Override
  public void channelByUserId(final Channel channel, final User user) {
    if (!channel.getOwnerUserId().equals(user.getId())) {
      throw new UserAuthorisationException("User Cannot be Authorised in Channel, " + "User Id [" + user.getId() + "] "
          + "Channel Id [" + channel.getId() + "], " + "Channel owner Id [" + channel.getOwnerUserId() + "]");
    }
  }

  /** {@inheritDoc} */
  @Override
  public void authoriseCommunicationAccess(final User user, final Channel channel, final Communication communication) {
    if (user.isManager() || communication.isPresenter(user) || channel.isChannelOwner(user)
        || channel.isChannelManager(user)) {
      return;
    }

    throw new UserAuthorisationException("User [" + user.getId() + "] is not authorized to access "
        + "communication id [" + communication.getId() + "] " + "in channel id [" + channel.getId() + "].");
  }

}
