/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: FeatureService.java 75381 2014-03-18 15:16:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.service.channel;

import java.util.List;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * The Feature Business API.
 */
public interface FeatureService {

  /**
   * Finds the channel's features.
   * 
   * @param channelId the channel id
   * 
   * @return a list of features
   * 
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException
   */
  List<Feature> findByChannelId(Long channelId);

  /**
   * Updates one or more channel's features.
   * <p>
   * Channel features are only updated by comparing the supplied new features with the channel default and existing
   * features. The update only happen if: The supplied feature is different from the exsiting feature, and:
   * <ul>
   * <li>The supplied feature equal to the default channel feature, then this method removes the channel feature
   * override values if exsits (soft delete the feature override value entry).</li>
   * <li>Otherwise if the supplied features not equal to default feature value, upsert the channel feature override
   * values, including activating the channel feature override value on update.</li>
   * </ul>
   * 
   * @param user the user questing the update
   * @param channel the channel
   * @param features the features to update
   * 
   * @return a list of features
   * 
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException
   * @throws com.brighttalk.common.user.error.UserAuthorisationException
   * 
   */
  List<Feature> update(User user, Channel channel, List<Feature> features);

}