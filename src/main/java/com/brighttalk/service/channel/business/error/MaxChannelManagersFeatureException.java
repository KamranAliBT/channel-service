/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: MaxChannelManagersFeatureException.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates the max channel managers feature has been reached.
 */
@SuppressWarnings("serial")
public class MaxChannelManagersFeatureException extends ApplicationException {

  private final Long channelId;

  private final Integer limit;

  private final Integer actual;

  /**
   * @param channelId see below.
   * @param limit channel feature limit
   * @param actual number fo channel managers
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public MaxChannelManagersFeatureException(final Long channelId, final Integer limit, final Integer actual) {
    super("The channel [" + channelId + "] has reached the max number of channel managers [" + limit + "].");

    this.channelId = channelId;
    this.limit = limit;
    this.actual = actual;
  }

  public Long getChannelId() {
    return channelId;
  }

  public Integer getLimit() {
    return limit;
  }

  public Integer getActual() {
    return actual;
  }

}
