/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SubscriptionNotFoundException.java 87022 2014-12-03 13:00:40Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.NotFoundException;

/**
 * Indicates the subscription has not been found.
 */
@SuppressWarnings("serial")
public class SubscriptionNotFoundException extends NotFoundException {

  /**
   * @see NotFoundException#NotFoundException(String)
   */
  public SubscriptionNotFoundException() {
    super("Subscription not found.");
  }

}
