/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelFeedSearchCriteria.java 83974 2014-09-29 11:38:04Z jbridger $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.business.domain.channel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;
import com.brighttalk.service.channel.business.error.SearchCriteriaException;

/**
 * Search Criteria for retrieving the paginated channel feed.
 * <p>
 * Contains the page number being requested, the size of the page being requested, what webcasts to include by status,
 * and any filter criteria for the feed.
 * <p>
 * Currently only closest to now filtering is supported. If the filter is not supplied it will default to no filtering.
 */
public class ChannelFeedSearchCriteria {

  /**
   * Supported filter types.
   */
  public static enum FilterType {

    /** Find the page with the closest to now (featured) communication in it */
    CLOSESTTONOW
  }

  /**
   * The requested page number.
   */
  private Integer pageNumber;

  /**
   * The requested page size.
   */
  private Integer pageSize;

  /**
   * The requested filter type.
   */
  private FilterType filterType;

  private List<CommunicationStatusFilter> communicationStatusFilter;

  /**
   * Construct an empty search criteria.
   */
  public ChannelFeedSearchCriteria() {
  }

  /**
   * Construct this search criteria.
   * 
   * @param pageNumber the requested page number. Optional. Can be <code>null</code> in which case the default will
   * apply.
   * @param pageSize the requested page size. Optional. Can be <code>null</code> in which case the default will apply.
   * @param filterType the requested filter. Optional. Can be <code>null</code> in which case the default (no filtering)
   * will apply.
   */
  public ChannelFeedSearchCriteria(final Integer pageNumber, final Integer pageSize, final String filterType) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.filterType = convertFilterType(filterType);
  }

  /**
   * Indicate if this criteria is request a closest to now page.
   * 
   * @return <code>true</code> or <code>false</code>.
   */
  public boolean isClosestToNowFilter() {
    return FilterType.CLOSESTTONOW.equals(filterType);
  }

  /**
   * Get the page size requested.
   * 
   * @return the page size or the default page size.
   */
  public Integer getPageSize() {
    return pageSize;
  }

  /**
   * Set the page size on this criteria.
   * 
   * @param pageSize the Page size.
   */
  public void setPageSize(final Integer pageSize) {
    this.pageSize = pageSize;
  }

  /**
   * Get the page number requested.
   * 
   * @return the page number or the default page number.
   */
  public Integer getPageNumber() {
    return pageNumber;
  }

  /**
   * Set the page number on this criteria.
   * 
   * @param pageNumber the page number.
   */
  public void setPageNumber(final Integer pageNumber) {
    this.pageNumber = pageNumber;
  }

  /**
   * Get the Filter Type.
   * 
   * @return The Filter Type of this search
   */
  public FilterType getFilterType() {
    return filterType;
  }

  /**
   * Set the filter type on this criteria.
   * 
   * @param filterType the filter type.
   */
  public void setFilterType(final FilterType filterType) {
    this.filterType = filterType;
  }

  /**
   * Set the filter type on this criteria from a String.
   * 
   * @param filterType the filter type string.
   */
  public void setFilterType(final String filterType) {
    this.filterType = convertFilterType(filterType);
  }

  /**
   * Helper method to convert a String to a Filter Type.
   * 
   * If an invalid type is given a Filter type of NONE will be returned.
   * 
   * @param filterType The String to convert
   * 
   * @return The converted filter Type
   */
  private FilterType convertFilterType(final String filterType) {

    if (filterType == null) {
      return null;
    }

    String formattedString = filterType.trim().toUpperCase();

    try {
      return FilterType.valueOf(formattedString);
    } catch (IllegalArgumentException e) {

      // Excepteion swallowed as we don;t need to log this.

      return null;
    }
  }

  /**
   * Set the communication status filter.
   * 
   * @param communicationStatusFilter List of communication statuses to filter by.
   */
  public void setCommunicationStatusFilter(final List<CommunicationStatusFilter> communicationStatusFilter) {

    this.communicationStatusFilter = communicationStatusFilter;
  }

  /**
   * Gets the communication status filter.
   * 
   * @return List of communication statuses to filter by.
   */
  public List<CommunicationStatusFilter> getCommunicationStatusFilter() {
    return communicationStatusFilter;
  }

  /**
   * Checks whether the communication status filter list has been set.
   * 
   * @return True if empty, false otherwise.
   */
  public boolean hasCommunicationStatusFilter() {

    return !CollectionUtils.isEmpty(getCommunicationStatusFilter());
  }

  /**
   * Supported communication status filters.
   */
  public static enum CommunicationStatusFilter {

    /**
     * Filter upcoming communication status.
     */
    UPCOMING,

    /**
     * Filter live communication status.
     */
    LIVE,

    /**
     * Filter processing communication status.
     */
    PROCESSING,

    /**
     * Filter recorded communication status.
     */
    RECORDED;

    /**
     * Converts a string into a {@link CommunicationStatusFilter}.
     * 
     * An empty value means that no filtering is requested.
     * 
     * @param value to be converted.
     * 
     * @return {@link CommunicationStatusFilter}
     */
    public static CommunicationStatusFilter convert(final String value) {

      for (CommunicationStatusFilter statusFilter : CommunicationStatusFilter.values()) {
        if (statusFilter.name().equalsIgnoreCase(value)) {
          return statusFilter;
        }
      }

      throw new SearchCriteriaException("webcastStatus", value);
    }
  }

  /**
   * Gets the status filter as a list of string.
   * 
   * @return List of string equivalents of the statuses. Empty list if there are no statuses set.
   */
  public List<String> getStatusFilterAsStringList() {

    List<String> statusFilterStringList = new ArrayList<String>();

    if (!CollectionUtils.isEmpty(communicationStatusFilter)) {

      for (CommunicationStatusFilter status : communicationStatusFilter) {

        statusFilterStringList.add(status.toString().toLowerCase());
      }
    }

    return statusFilterStringList;
  }

  @Override
  public String toString() {

    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("pageNumber", getPageNumber());
    builder.append("pageSize", getPageSize());
    builder.append("filterType", getFilterType());
    builder.append("communicationStatusFilter", getCommunicationStatusFilter());

    return builder.toString();
  }

}
