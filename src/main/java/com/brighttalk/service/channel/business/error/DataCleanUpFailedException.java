/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DataCleanUpFailedException.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Indicates that the requested communication provider update encountered problems when cleaning up all the data
 * asssociated with the old provider type.
 */
@SuppressWarnings("serial")
public class DataCleanUpFailedException extends ApplicationException {

  private static final String ERROR_CODE_NOT_FOUND_ERROR = "DataCleanUpFailed";

  /**
   * The application-specific code identifying the error which caused this Exception, that will be reported back to end
   * user/client if this exception is left unhandled. Defaults to {@link #ERROR_CODE_NOT_FOUND_ERROR}.
   */
  private final String errorCode = ERROR_CODE_NOT_FOUND_ERROR;

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public DataCleanUpFailedException(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public DataCleanUpFailedException(final String message, final String errorCode) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   * 
   * @see ApplicationException#ApplicationException(String)
   */
  public DataCleanUpFailedException(final String message) {
    super(message, ERROR_CODE_NOT_FOUND_ERROR);
  }

  @Override
  public String getUserErrorCode() {
    return errorCode;
  }
}