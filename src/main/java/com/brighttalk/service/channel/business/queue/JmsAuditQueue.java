/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JmsAuditQueue.java 78594 2014-05-28 15:11:50Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.business.queue;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * JMS implementation of a Audit Record Queue.
 * 
 * The audit records will be placed on to a JMS queue where some other listener will pick them up and do the actual
 * processing.
 */
public class JmsAuditQueue implements AuditQueue {

  /**
   * Logger for this class
   */
  protected static final Logger logger = Logger.getLogger(JmsAuditQueue.class);

  /**
   * Name of the destination to which JMS messages are sent. The name must be resolvable to a configured
   * {@link javax.jms.Destination JMS Destination}.
   */
  @Value(value = "${jms.createAuditRecordDestination}")
  private String destinationName;

  /**
   * Helper class acting in the role of the JMS {@link javax.jms.MessageProducer}. Handles creation and release of JMS
   * resources (Connections, Session) on the sending of the JMS message.
   */
  @Qualifier(value = "generic")
  @Autowired
  private JmsTemplate jmsTemplate;

  /**
   * {@inheritDoc}
   */
  @Override
  public void create(final AuditRecordDto auditRecordDto) {

    MessageCreator msgCreator = new MessageCreator() {

      /**
       * {@inheritDoc}
       * <p>
       * Anonymous implementation of Spring JmsTemplate's simplest callback interface - {@link MessageCreator}. Creates
       * a JMS message that is used to asynchronously request the sending of the user email.
       * <p>
       * The message body contains a serializable asset to be processed. This is implemented as a JMS
       * {@link ObjectMessage} since the messages are consumed locally/internally.
       */
      @Override
      public Message createMessage(final Session session) throws JMSException {
        ObjectMessage msg = session.createObjectMessage();
        msg.setObject(auditRecordDto);
        return msg;
      }
    };

    this.jmsTemplate.send(this.destinationName, msgCreator);
  }

}
