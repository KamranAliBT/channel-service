/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationsDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.community.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * DTO Class used to send details of communications 
 * community settings to the community service.
 */
@XmlRootElement(name = "communications")
public class CommunicationsDto {

  private List<CommunicationDto> communications;

  public List<CommunicationDto> getCommunications() {
    return communications;
  }
  
  @XmlElement(name = "communication")
  public void setCommunications(List<CommunicationDto> communications) {
    this.communications = communications;
  }
  
  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  public String toString() {
    
    StringBuilder builder = new StringBuilder();
    builder.append("communications: [").append( communications ).append("] ");

    return builder.toString();
  }
}
