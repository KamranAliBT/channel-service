/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: JdbcContractPeriodDbDao.java 101521 2015-10-15 16:45:21Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.ContractPeriodNotFoundException;
import com.brighttalk.service.channel.integration.error.ContractPeriodAlreadyExistsForChannel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class JdbcContractPeriodDbDao extends JdbcDao implements ContractPeriodDbDao {

  /** Set of ContractPeriod database field names */
  private static final String ID_FIELD = "id";
  private static final String IS_ACTIVE_FIELD = "is_active";
  private static final String LAST_UPDATED_FIELD = "last_updated";
  private static final String CREATED_FIELD = "created";
  private static final String CHANNEL_ID_FIELD = "channel_id";
  private static final String START_DATE_FIELD = "start_date";
  private static final String END_DATE_FIELD = "end_date";

  /** Static queries used */
  /* @formatter:off */
    //CHECKSTYLE:OFF
  private static final String CONTRACT_PERIOD_FIELDS =
    " cp.id as id, " +
    " cp.channel_id as channel_id, " +
    " cp.start_date as start_date, " +
    " cp.end_date as end_date, " +
    " cp.is_active as is_active, " +
    " cp.created as created, " +
    " cp.last_updated as last_updated ";

  private static final String CONTRACT_PERIOD_UPDATE_QUERY =
    "UPDATE " +
    " contract_period " +
    " SET " +
    "   start_date = :start_date, " +
    "   end_date = :end_date, " +
    "   last_updated = NOW()  " +
    " WHERE " +
    "   id = :id " +
    "  AND " +
    "   is_active = 1 ";

  private static final String CONTRACT_PERIOD_SELECT_BY_ID_QUERY =
    " SELECT " +
      CONTRACT_PERIOD_FIELDS +
    " FROM " +
    "  contract_period cp " +
    " INNER JOIN " +
    "  channel c on c.id = cp.channel_id " +
    " WHERE " +
    "  cp.id = :contractPeriodId " +
    " AND cp.is_active = 1 " +
    " AND c.is_active = 1 ";

  private static final String CONTRACT_PERIODS_BY_CHANNEL_ID_SELECT_QUERY =
    " SELECT " +
       CONTRACT_PERIOD_FIELDS +
    " FROM " +
    "  contract_period cp " +
    " INNER JOIN " +
    "  channel c on c.id = cp.channel_id " +
    " WHERE " +
    "  cp.channel_id = :channelId " +
    " AND cp.is_active = 1 " +
    " AND c.is_active = 1 " +
    " ORDER BY " +
    "  cp.start_date desc ";

  private static final String CONTRACT_PERIOD_OVERLAP_QUERY =
    " SELECT count(channel_id) " +
    " FROM contract_period cp " +
    " WHERE cp.channel_id = :channelId " +
    " AND cp.is_active = 1 " +
    " AND  (   " +
    "    (:startDate BETWEEN cp.start_date AND cp.end_date) " + // Does this CP start within another
    "    OR    (:endDate BETWEEN cp.start_date AND cp.end_date) " + //Does this CP end within another
    "    OR    (cp.start_date BETWEEN :startDate AND :endDate) " +  //Does this completely encapsulate another?
                                                                    //-We only need to check the start date to confirm
    " ) ";
  
  private static final String CONTRACT_PERIOD_EXISTS_QUERY =
    " SELECT count(cp.id) " +
    " FROM contract_period cp " +
    " INNER JOIN channel c on c.id = cp.channel_id " +
    " WHERE " +
    "   cp.id = :contractPeriodId " +
    "   AND cp.channel_id = :channelId " +
    "   AND cp.is_active = 1 " +
    "   AND c.is_active = 1 " +
    " LIMIT 1 ";
  
  private static final String CONTRACT_PERIOD_DELETE_QUERY =
  " UPDATE " +
  "   contract_period " +
  " SET " +
  "   is_active = 0, " +
  "   last_updated = NOW() " +
  " WHERE " +
  "   id = :contractPeriodId ";
  // CHECKSTYLE:ON
    /* @formatter:on */

  /** Logger for this class */
  private static final Logger logger = Logger.getLogger(JdbcContractPeriodDbDao.class);

  /** JDBC template used to access the database. */
  protected NamedParameterJdbcTemplate jdbcTemplate;

  private SimpleJdbcInsert simpleJdbcInsertContractPeriod;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   *
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    simpleJdbcInsertContractPeriod = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertContractPeriod.withTableName("contract_period");
    simpleJdbcInsertContractPeriod.usingGeneratedKeyColumns("id");
    simpleJdbcInsertContractPeriod.usingColumns(CHANNEL_ID_FIELD, START_DATE_FIELD, END_DATE_FIELD, IS_ACTIVE_FIELD,
        CREATED_FIELD, LAST_UPDATED_FIELD);
  }

  /** {@inheritDoc} */
  @Override
  public ContractPeriod create(final ContractPeriod contractPeriod) {
    MapSqlParameterSource params = new MapSqlParameterSource();

    //data fields
    params.addValue(CHANNEL_ID_FIELD, contractPeriod.getChannelId());
    params.addValue(START_DATE_FIELD, contractPeriod.getStart());
    params.addValue(END_DATE_FIELD, contractPeriod.getEnd());

    //audit/maintenance
    params.addValue(IS_ACTIVE_FIELD, 1);
    params.addValue(LAST_UPDATED_FIELD, new Date());
    params.addValue(CREATED_FIELD, new Date());

    //insert this ContractPeriod in to the database and return it's primary key
    Number primaryKey;
    try {
      primaryKey = simpleJdbcInsertContractPeriod.executeAndReturnKey(params);
    } catch (DuplicateKeyException e) {//This contract period already exists for this channel
      throw new ContractPeriodAlreadyExistsForChannel("This start/end time is already stored for an existing contract "
          + "period on this channel.");
    }

    //fetch and return the newly created ContractPeriod
    Long contractPeriodId = primaryKey.longValue();
    contractPeriod.setId(contractPeriodId);
    contractPeriod.setActive(true);
    return contractPeriod;
  }

  /** {@inheritDoc} */
  @Override
  public ContractPeriod find(Long contractPeriodId) {
    if (logger.isDebugEnabled()) {
      logger.debug("Finding contract period [" + contractPeriodId + "].");
    }

    ContractPeriod contractPeriod = null;
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("contractPeriodId", contractPeriodId);

    try {
      contractPeriod =
          jdbcTemplate.queryForObject(CONTRACT_PERIOD_SELECT_BY_ID_QUERY, params, new ContractPeriodRowMapper());
    } catch (EmptyResultDataAccessException e) {
      if (logger.isDebugEnabled()) {
        logger.debug("contract period [" + contractPeriodId + "] not found.");
      }
      throw new ContractPeriodNotFoundException(contractPeriodId, e);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Found contract period [" + contractPeriodId + "].");
    }
    return contractPeriod;
  }

  /** {@inheritDoc} */
  @Override
  public boolean exists(Long contractPeriodId, Long channelId) {

    if (logger.isDebugEnabled()) {
      logger.debug("Finding contract period [" + contractPeriodId + "] for channel [" + channelId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource("contractPeriodId", contractPeriodId);
    params.addValue("channelId", channelId);

    int noOfRows = jdbcTemplate.queryForObject(CONTRACT_PERIOD_EXISTS_QUERY, params, Integer.class);
    if (logger.isDebugEnabled()) {
      logger.debug("Found [" + noOfRows + "] contract periods [" + contractPeriodId
          + "] for channel [" + channelId + "].");
    }
    return (noOfRows > 0) ? true : false;
  }

  /** {@inheritDoc} */
  @Override
  public List<ContractPeriod> findAllByChannelId(Long channelId) {
    if (logger.isDebugEnabled()) {
      logger.debug("Finding contract periods with channel id [" + channelId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channelId", channelId);

    List<ContractPeriod> contractPeriods = jdbcTemplate.query(CONTRACT_PERIODS_BY_CHANNEL_ID_SELECT_QUERY, params,
        new ContractPeriodRowMapper());

    if (logger.isDebugEnabled()) {
      logger.debug("Found [" + contractPeriods.size() + "] contract periods with channel id [" + channelId + "].");
    }
    return contractPeriods;
  }

  /** {@inheritDoc} */
  @Override
  public ContractPeriod update(ContractPeriod contractPeriod) {
    if (logger.isDebugEnabled()) {
      logger.debug("Updating contract period [" + contractPeriod.getId() + "].");
    }
    MapSqlParameterSource params = new MapSqlParameterSource();

    params.addValue(ID_FIELD, contractPeriod.getId());
    params.addValue(START_DATE_FIELD, contractPeriod.getStart());
    params.addValue(END_DATE_FIELD, contractPeriod.getEnd());

    int noOfRows = 0;
    try {
      noOfRows = jdbcTemplate.update(CONTRACT_PERIOD_UPDATE_QUERY, params);
    } catch (DuplicateKeyException e) {//This contract period already exists for this channel
      throw new ContractPeriodAlreadyExistsForChannel("This start/end time is already stored for an existing contract "
          + "period on this channel.");
    }

    if (noOfRows != 1) {
      throw new ApplicationException("Could not update contract period [" + contractPeriod.getId() + "]");
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Updated contract period [" + contractPeriod.getId() + "].");
    }
    return contractPeriod;
  }

  /** {@inheritDoc} */
  @Override
  public void delete(Long contractPeriodId) {
    //Check if this contract period exists - it might already be deleted or it's channel might be inactive
    find(contractPeriodId);

    if (logger.isDebugEnabled()) {
      logger.debug("Deleting contract period [" + contractPeriodId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource("contractPeriodId", contractPeriodId);

    int noOfRows = jdbcTemplate.update(CONTRACT_PERIOD_DELETE_QUERY, params);
    if (noOfRows != 1) {
      throw new ApplicationException("Could not delete contract period [" + contractPeriodId + "]");
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Deleted contract period [" + contractPeriodId + "].");
    }
  }

  /** {@inheritDoc} */
  @Override
  public boolean contractPeriodOverlapsExisting(ContractPeriod contractPeriod) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channelId", contractPeriod.getChannelId());
    params.addValue("startDate", contractPeriod.getStart());
    params.addValue("endDate", contractPeriod.getEnd());

    String contractPeriodOverlapQuery = CONTRACT_PERIOD_OVERLAP_QUERY;
    if (contractPeriod.getId() != null) {
      params.addValue("contractPeriodId", contractPeriod.getId());
      contractPeriodOverlapQuery += " AND cp.id != :contractPeriodId ";
    }

    int noOfRows = jdbcTemplate.queryForObject(contractPeriodOverlapQuery, params, Integer.class);
    return noOfRows != 0;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps a {@link ResultSet} to a {@link ContractPeriod}.
   */
  private class ContractPeriodRowMapper implements RowMapper<ContractPeriod> {

    @Override
    public ContractPeriod mapRow(ResultSet rs, int rowNum) throws SQLException {
      ContractPeriod contractPeriod = new ContractPeriod(rs.getLong(ID_FIELD));
      contractPeriod.setChannelId(rs.getLong(CHANNEL_ID_FIELD));
      contractPeriod.setStart(rs.getTimestamp(START_DATE_FIELD));
      contractPeriod.setEnd(rs.getTimestamp(END_DATE_FIELD));

      contractPeriod.setActive(rs.getBoolean(IS_ACTIVE_FIELD));
      contractPeriod.setCreated(rs.getTimestamp(CREATED_FIELD));
      contractPeriod.setLastUpdated(rs.getTimestamp(LAST_UPDATED_FIELD));
      return contractPeriod;
    }
  }
}
