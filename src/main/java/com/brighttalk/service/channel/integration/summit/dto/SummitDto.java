/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SummitDto.java 98710 2015-08-03 09:52:40Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.summit.dto;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.common.presentation.dto.LinkDto;
import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Domain Object representing a summit. TODO - move this to the Java common project.
 */
public class SummitDto {

  private Long id;

  private String title;

  private String alias;

  private String description;

  private String keywords;

  private String visibility;

  private Boolean enableSponsorASummit;

  private List<LinkDto> links;

  private String scheduled;

  private String entryTime;

  private String closeTime;

  private List<SponsorDto> sponsors;

  private List<WebcastRefDto> webcastRefs;

  /**
   * Default constrcutor.
   */
  public SummitDto() {
  }

  /**
   * Default constructor with summit id.
   * 
   * @param id of the summit to set
   */
  public SummitDto(final Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(final String alias) {
    this.alias = alias;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(final String keywords) {
    this.keywords = keywords;
  }

  public String getEntryTime() {
    return entryTime;
  }

  public void setEntryTime(final String entryTime) {
    this.entryTime = entryTime;
  }

  public String getCloseTime() {
    return closeTime;
  }

  public void setCloseTime(final String closeTime) {
    this.closeTime = closeTime;
  }

  public String getScheduled() {
    return scheduled;
  }

  public void setScheduled(final String scheduled) {
    this.scheduled = scheduled;
  }

  public String getVisibility() {
    return visibility;
  }

  public void setVisibility(final String visibility) {
    this.visibility = visibility;
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  public void setLinks(final List<LinkDto> links) {
    this.links = links;
  }

  public List<SponsorDto> getSponsors() {
    return sponsors;
  }

  public void setSponsors(final List<SponsorDto> sponsors) {
    this.sponsors = sponsors;
  }

  public List<WebcastRefDto> getWebcastRefs() {
    return webcastRefs;
  }

  public void setWebcastRefs(final List<WebcastRefDto> webcastRefs) {
    this.webcastRefs = webcastRefs;
  }

  public Boolean getEnableSponsorASummit() {
    return enableSponsorASummit;
  }

  public void setEnableSponsorASummit(final Boolean enableSponsorASummit) {
    this.enableSponsorASummit = enableSponsorASummit;
  }

  /**
   * Indicates if this summit has visibility set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasVisibility() {
    return !StringUtils.isEmpty(visibility);
  }

  /**
   * Indicates if this summit has scheduled date set.
   * 
   * @return <code>true</code> or <code>false</code>
   */
  public boolean hasScheduled() {
    return !StringUtils.isEmpty(scheduled);
  }

  /** {@inheritDoc} */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("id", id);
    builder.append("title", title);
    builder.append("alias", alias);
    builder.append("description", description);
    builder.append("keywords", keywords);
    builder.append("visibility", visibility);
    builder.append("enableSponsorASummit", enableSponsorASummit);
    builder.append("links", links);
    builder.append("scheduled", scheduled);
    builder.append("entryTime", entryTime);
    builder.append("closeTime", closeTime);
    builder.append("sponsors", sponsors);
    builder.append("webcastRefs", webcastRefs);
    return builder.toString();
  }
}