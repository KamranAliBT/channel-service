/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SortDto.java 89869 2015-02-12 17:26:56Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.communication.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Sort search DTO.
 */
@XmlRootElement(name = "sort")
@XmlAccessorType(XmlAccessType.FIELD)
public class SortDto {

  @XmlAttribute
  private String order;
  @XmlValue
  private String sortValue;

  /**
   * Default constructor.
   */
  public SortDto() {
  }

  /**
   * @param order The sort order
   * @param sortValue The sore value
   */
  public SortDto(String order, String sortValue) {
    this.order = order;
    this.sortValue = sortValue;
  }

  /**
   * @return the order
   */
  public String getOrder() {
    return order;
  }

  /**
   * @return the sortValue
   */
  public String getSortValue() {
    return sortValue;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("order", order);
    builder.append("sortValue", sortValue);
    return builder.toString();
  }

}
