/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultTaskConverter.java 65488 2013-06-12 13:30:16Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.imageconverter.dto.converter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.ImageConverterSettings;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.imageconverter.dto.TaskDto;

/**
 * Default implementation of an CommunicationConverter.
 * 
 * This takes an Asset and Converts it to the relevant Dto Object used when making a request to the channel service to
 * publish content.
 */
@Component
public class DefaultTaskConverter implements TaskConverter {

  /** Show all scale mode. */
  protected static final String SCALE_MODE_SHOW_ALL = "showAll";

  /** No border scale mode. */
  protected static final String SCALE_MODE_NO_BORDER = "noBorder";

  @Autowired
  @Value("${feature.image.callback.url}")
  private String callbackUrl;

  @Autowired
  @Value("${site.url}")
  private String domain;

  /** {@inheritDoc} */
  @Override
  public TaskDto convertForThumbnail(final Communication communication, final ImageConverterSettings settings) {

    String notifyUrl = callbackUrl.replace("[[communicationId]]", communication.getId().toString()) + "thumbnail";

    return convert(notifyUrl, settings, SCALE_MODE_NO_BORDER);
  }

  /** {@inheritDoc} */
  @Override
  public TaskDto convertForPreview(final Communication communication, final ImageConverterSettings settings) {

    String notifyUrl = callbackUrl.replace("[[communicationId]]", communication.getId().toString()) + "preview";

    return convert(notifyUrl, settings, SCALE_MODE_SHOW_ALL);
  }

  private TaskDto convert(final String notifyUrl, final ImageConverterSettings settings, final String scaleMode) {

    TaskDto dto = new TaskDto();
    dto.setNotify(notifyUrl);
    dto.setSource(stripDomain(settings.getSource()));
    dto.setDestination(settings.getDestination());
    dto.setFormat(settings.getFormat());
    dto.setWidth(settings.getWidth());
    dto.setHeight(settings.getHeight());
    dto.setScaleMode(scaleMode);

    return dto;
  }

  /**
   * Strips the domain from the given url if set.
   * 
   * @param url to strip the domain from
   * @return the stripped url
   */
  private String stripDomain(String url) {
    if (StringUtils.isNotEmpty(url) && url.contains(domain)) {
      url = url.replace(domain, "");

    }
    return url;
  }

}