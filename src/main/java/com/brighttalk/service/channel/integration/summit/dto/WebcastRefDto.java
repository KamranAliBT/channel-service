/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: WebcastRefDto.java 91239 2015-03-06 14:16:20Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.summit.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * WebcastRef dto class.
 */
@XmlRootElement(name = "webcastRef")
public class WebcastRefDto {

  private String id;

  private Boolean featured;

  /**
   * Returns the webcast ref dto id. Format is channelId-webcastId
   * 
   * @return the formatted id of the webcast ref dto
   */
  @XmlAttribute
  public String getId() {
    return id;
  }

  /**
   * Set the webcast ref dto id. The id is a combination of channel id and webcast id.
   * 
   * @param id id of the webcast refs
   */
  public void setId(final String id) {
    this.id = id;
  }

  /**
   * @return if the webcast is featured
   */
  public Boolean getFeatured() {
    return featured;
  }

  /**
   * @param featured the isFeatured to set
   */
  @XmlAttribute(name = "featured")
  public void setFeatured(final Boolean featured) {
    this.featured = featured;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("id", id);
    builder.append("featured", featured);
    return builder.toString();
  }

}