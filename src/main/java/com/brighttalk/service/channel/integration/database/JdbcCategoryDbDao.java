/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2010.
 * All Rights Reserved.
 * $Id: JdbcCategoryDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.Category;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;

/**
 * Spring JDBC Database DAO implementation of {@link CategoryDbDao} which uses JDBC via Spring's SimpleJDBCTemplate to
 * access the Database.
 */
@Repository
@Primary
public class JdbcCategoryDbDao extends JdbcDao implements CategoryDbDao {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JdbcCategoryDbDao.class);

  // CHECKSTYLE:OFF
  // @formatter:off
  private static final String CHANNELS_SELECT_CATEGORIES_QUERY = "SELECT "
     + "  id AS id, "
     + "  channel_id AS channel_id, "
     + "  category AS name, "
     + "  created AS created,"
     + "  last_updated AS last_updated "
     + "FROM "
     + "  category "
     + "WHERE "
     + "  channel_id IN (:channelIds) "
     + "  AND is_active = 1 "
     + "ORDER BY "
     + "  category ASC ";
  
   private static final String COMMUNICATIONS_SELECT_CATEGORIES_QUERY = "SELECT "
     + " c.id as category_id,"
     + " c.channel_id as channel_id,"
     + " c.category as category,"
     + " cc.communication_id as communication_id,"
     + " cc.is_active as is_active "
     + "FROM "
     + "  category c "
     + "  LEFT JOIN communication_category cc ON (c.id = cc.category_id) "
     + "WHERE "
     + "  cc.communication_id = :communicationsId "
     + "  AND cc.channel_id = :channelId"
     + "  AND c.is_active = 1"
     + "  AND cc.is_active = 1 "
     + "ORDER BY "
     + "  c.category ASC";

        // @formatter:on  
  // CHECKSTYLE:ON

  /** JDBC template used to access the database. */
  protected NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Set the datasource on this DAO.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public void loadCategories(Channel channel) {

    if (logger.isDebugEnabled()) {
      logger.debug("Loading categories for channel [" + channel.getId() + "].");
    }

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  id AS id, ");
    sql.append("  channel_id AS channel_id, ");
    sql.append("  category AS name, ");
    sql.append("  created AS created,");
    sql.append("  last_updated AS last_updated ");
    sql.append("FROM ");
    sql.append("  category ");
    sql.append("WHERE ");
    sql.append("  channel_id = :channelId ");
    sql.append("  AND is_active = 1 ");
    sql.append("  ORDER BY category ASC ");

    // CHECKSTYLE:ON

    // CHECKSTYLE:OFF
    params.addValue("channelId", channel.getId());
    // CHECKSTYLE:ON

    List<Category> categories = this.jdbcTemplate.query(sql.toString(), params, new ChannelCategoryRowMapper());

    channel.setCategories(categories);

    if (logger.isDebugEnabled()) {
      logger.debug("Categories loaded for channel [" + channel.getId() + "]. Number of found categories ["
              + categories.size() + "].");
    }

  }

  @Override
  public void loadCategories(List<Channel> channels) {

    Map<Long, Channel> mappedChannels = getMappedChannels(channels);
    Set<Long> channelIds = mappedChannels.keySet();

    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    params.addValue("channelIds", channelIds);
    // CHECKSTYLE:ON

    List<Category> categories = this.jdbcTemplate.query(CHANNELS_SELECT_CATEGORIES_QUERY, params,
        new ChannelCategoryRowMapper());

    for (Category category : categories) {

      mappedChannels.get(category.getChannelId()).addCategory(category);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Categories loaded for channels [" + channelIds + "]. Number of found categories ["
              + categories.size() + "].");
    }
  }

  /** {@inheritDoc} */
  @Override
  public void loadCategories(Long channelId, Communication communication) {

    // if we have no channel id, or no communication, just returns before hitting the DB.
    if (channelId == null || communication == null) {
      return;
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Adding communication categories for channelId [" + channelId + "] and communication ids ["
              + communication.getId() + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    params.addValue("channelId", channelId);
    params.addValue("communicationsId", communication.getId());
    // CHECKSTYLE:ON

    List<CommunicationCategory> communicationCategories =
            jdbcTemplate.query(COMMUNICATIONS_SELECT_CATEGORIES_QUERY, params, new CommunicationCategoryRowMapper());

    if (logger.isDebugEnabled()) {
      logger.debug("Categories for channelId [" + channelId + "]. Categories [" + communicationCategories + "].");
    }

    communication.setCategories(communicationCategories);
  }

  /** {@inheritDoc} */
  @Override
  public void loadCategories(Long channelId, List<Communication> communications) {

    // if we have no channel id, or no communications, just returns before hitting the DB.
    if (channelId == null || communications == null || communications.size() == 0) {
      return;
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Adding communication categories for channelId [" + channelId + "].");
    }

    List<Long> communicationIds = super.getCommunicationIds(communications);

    if (logger.isDebugEnabled()) {
      logger.debug("Adding communication categories for channelId [" + channelId + "] and communication ids ["
              + communicationIds + "].");
    }

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(" c.id as category_id,");
    sql.append(" c.channel_id as channel_id,");
    sql.append(" c.category as category,");
    sql.append(" cc.communication_id as communication_id,");
    sql.append(" cc.is_active as is_active ");
    sql.append("FROM ");
    sql.append("  category c ");
    sql.append("  LEFT JOIN communication_category cc ON (c.id = cc.category_id) ");
    sql.append("WHERE ");
    sql.append("  cc.communication_id IN ( :ids ) ");
    sql.append("  AND cc.channel_id = :channelId");
    sql.append("  AND c.is_active = 1");
    sql.append("  AND cc.is_active = 1 ");
    sql.append("ORDER BY ");
    sql.append("  c.category ASC");

    params.addValue("channelId", channelId);
    params.addValue("ids", communicationIds);
    // CHECKSTYLE:ON

    List<CommunicationCategory> communicationCategories =
            jdbcTemplate.query(sql.toString(), params, new CommunicationCategoryRowMapper());

    if (logger.isDebugEnabled()) {
      logger.debug("Categories for channelId [" + channelId + "]. Categories [" + communicationCategories + "].");
    }

    loadCategories(communications, communicationCategories);
  }

  /**
   * Load the given categories to the given communications.
   * 
   * @param communications the communications to add the categories to.
   * @param categories the categories to add.
   */
  private void loadCategories(List<Communication> communications, List<CommunicationCategory> categories) {

    for (Communication communication : communications) {
      for (CommunicationCategory category : categories) {

        if (communication.getId().equals(category.getCommunicationId())) {
          communication.addCategory(category);
          if (logger.isDebugEnabled()) {
            logger.debug("Added category [" + category + "] to communication [" + communication.getId() + "]");
          }
        } // if

      } // categories
    } // communications

    if (logger.isDebugEnabled()) {
      logger.debug("Categories added");
    }
  }

  /**
   * Helper method to map channels by id.
   * 
   * @param channels The list of channels
   * 
   * @return The Map of channels keyed by Id
   */
  private Map<Long, Channel> getMappedChannels(List<Channel> channels) {

    Map<Long, Channel> mappedChannels = new HashMap<Long, Channel>();

    for (Channel channel : channels) {
      mappedChannels.put(channel.getId(), channel);
    }
    return mappedChannels;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Category} for channel categories.
   */
  private class ChannelCategoryRowMapper implements RowMapper<Category> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Category mapRow(ResultSet resultSet, int rowNum) throws SQLException {

      Long id = resultSet.getLong("id");
      Long channelId = resultSet.getLong("channel_id");
      String name = resultSet.getString("name");

      Category category = new Category(id, channelId, name);
      category.setCreated(resultSet.getTimestamp("created"));
      category.setLastUpdated(resultSet.getTimestamp("last_updated"));

      return category;
    }
  } // ChannelCategoryRowMapper

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link CommunicationCategory}.
   */
  private static class CommunicationCategoryRowMapper implements RowMapper<CommunicationCategory> {

    /**
     * {@inheritDoc}
     */
    @Override
    public CommunicationCategory mapRow(ResultSet resultSet, int rowNum) throws SQLException {

      return new CommunicationCategory(resultSet.getLong("category_id"),
              resultSet.getLong("channel_id"),
              resultSet.getLong("communication_id"),
              resultSet.getString("category"),
              resultSet.getBoolean("is_active"));
    }
  } // ComunicationCategoryRowMapper
} // DAO
