/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the Converters used to convert communication business object to a summit request DTO.
 */
package com.brighttalk.service.channel.integration.summit.dto.converter;