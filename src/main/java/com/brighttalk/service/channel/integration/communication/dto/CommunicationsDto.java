/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: CommunicationsDto.java 89869 2015-02-12 17:26:56Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.communication.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Represents Communications DTO.
 */
@XmlRootElement(name = "communications")
@XmlAccessorType(XmlAccessType.FIELD)
public class CommunicationsDto {

  @XmlAttribute
  private String quantity;
  @XmlElementRef
  private List<CommunicationDto> communications;

  /**
   * @return the quantity
   */
  public String getQuantity() {
    return quantity;
  }

  /**
   * @param quantity the quantity to set
   */
  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  /**
   * @return the communications
   */
  public List<CommunicationDto> getCommunications() {
    return communications;
  }

  /**
   * @param communications the communications to set
   */
  public void setCommunications(List<CommunicationDto> communications) {
    this.communications = communications;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("quantity", quantity);
    builder.append("communications", communications);
    return builder.toString();
  }
}
