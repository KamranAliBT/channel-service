/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: VotesDto.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audience.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * VotesDto.
 */
@XmlRootElement(name = "votes")
public class VotesDto {

  private List<VoteDto> votes;

  public List<VoteDto> getVotes() {
    return votes;
  }

  @XmlElement(name = "vote")
  public void setVotes(final List<VoteDto> votes) {
    this.votes = votes;
  }
}