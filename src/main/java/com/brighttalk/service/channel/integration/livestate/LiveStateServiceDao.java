/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: LiveStateServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.livestate;

/**
 * Interface to define the required methods for the interactions with the LiveState Service.
 */
public interface LiveStateServiceDao {

  /**
   * Delete communication from live state by given id.
   * 
   * @param communicationId the id of the communication
   */
  void deleteCommunication(Long communicationId);
}