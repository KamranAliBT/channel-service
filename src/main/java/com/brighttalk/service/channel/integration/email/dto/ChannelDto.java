/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelDto.java 71274 2013-11-26 14:01:47Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.email.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * ChannelDto.
 */
public class ChannelDto {

  private Long id;

  private String title;

  private String type;

  private Long ownerUserID;

  /** Default constructor. */
  public ChannelDto() {
  }

  /**
   * @param id The id of the channel.
   */
  public ChannelDto(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Long getOwnerUserID() {
    return ownerUserID;
  }

  public void setOwnerUserID(Long ownerUserID) {
    this.ownerUserID = ownerUserID;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("id: [").append(id).append("] ");
    builder.append("title: [").append(title).append("] ");
    builder.append("type: [").append(type).append("] ");
    builder.append("ownerUserID: [").append(ownerUserID).append("] ");

    return builder.toString();
  }
}