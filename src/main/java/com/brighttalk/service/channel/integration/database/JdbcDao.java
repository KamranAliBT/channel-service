/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcDao.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.brighttalk.service.channel.business.domain.Entity;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Base class for all JdbcDao's.
 * 
 * TODO - should extend Sring's JDBCDaoSupport, but could not get it to work. Need to revisit.
 */
public abstract class JdbcDao {

  /**
   * Checks if the provided value is set to {@link Entity#DELETE_INDICATOR}. If it is, returns a new value of null
   * (which is what the corresponding DB field is to be set to) Otherwise, just returns the value.
   * 
   * @param value to be checked
   * @return the value or null
   */
  protected String convertDeleteIndicator(final String value) {
    return Entity.DELETE_INDICATOR.equals(value) ? null : value;
  }

  /**
   * Given a list of communications return a list of their ids.
   * 
   * @param communications the communications
   * @return the ids
   */
  public List<Long> getCommunicationIds(final List<Communication> communications) {
    List<Long> communicationIds = new ArrayList<Long>();
    for (Communication communication : communications) {
      communicationIds.add(communication.getId());
    }
    return communicationIds;
  }

  /**
   * This is a helper method to replace the deprecated queryForInt method for JdbcTemplate. This will return 0 if the
   * data return from the db query is null. We check that the data is not null otherwise a nullpointerexception will be
   * thrown as the null data will not be converted into a primitive.
   * 
   * see: http://stackoverflow.com/questions/15661313/jdbctemplate-queryforint-long-is-deprecated-in-spring-3-2-2-what-
   * should-it-be-r see:
   * http://docs.spring.io/autorepo/docs/spring-framework/3.2.x/javadoc-api/org/springframework/jdbc/
   * core/simple/SimpleJdbcOperations.html
   * 
   * @param jdbcTemplate used for db access/query
   * @param sql to query the db
   * @param args used for the query
   * @return integer value or 0 if null is return from the db
   * @throws DataAccessException if data access fail
   */
  protected int queryForInt(final JdbcTemplate jdbcTemplate, final String sql, final Object... args)
      throws DataAccessException {
    Number number = jdbcTemplate.queryForObject(sql, args, Integer.class);
    return (number != null ? number.intValue() : 0);
  }

  /**
   * This is a helper method to replace the deprecated queryForInt method for NamedParameterJdbcTemplate. This will
   * return 0 if the data return from the db query is null. We check that the data is not null otherwise a
   * nullpointerexception will be thrown as the null data will not be converted into a primitive.
   * 
   * see: http://stackoverflow.com/questions/15661313/jdbctemplate-queryforint-long-is-deprecated-in-spring-3-2-2-what-
   * should-it-be-r see:
   * http://docs.spring.io/autorepo/docs/spring-framework/3.2.x/javadoc-api/org/springframework/jdbc/
   * core/simple/SimpleJdbcOperations.html
   * 
   * @param jdbcTemplate used for db access/query
   * @param sql to query the db
   * @param params used for the query
   * @return integer value or 0 if null is return from the db
   * @throws DataAccessException if data access fail
   */
  protected int queryForInt(final NamedParameterJdbcTemplate jdbcTemplate, final String sql,
      final SqlParameterSource params) throws DataAccessException {
    Number number = jdbcTemplate.queryForObject(sql, params, Integer.class);
    return (number != null ? number.intValue() : 0);
  }

  /**
   * This is a helper method to replace the deprecated queryForLong method for JdbcTemplate. This will return 0 if the
   * data return from the db query is null. We check that the data is not null otherwise a nullpointerexception will be
   * thrown as the null data will not be converted into a primitive.
   * 
   * see: http://stackoverflow.com/questions/15661313/jdbctemplate-queryforint-long-is-deprecated-in-spring-3-2-2-what-
   * should-it-be-r see:
   * http://docs.spring.io/autorepo/docs/spring-framework/3.2.x/javadoc-api/org/springframework/jdbc/
   * core/simple/SimpleJdbcOperations.html
   * 
   * @param jdbcTemplate used for db access/query
   * @param sql to query the db
   * @param args used for the query
   * @return integer value or 0 if null is return from the db
   * @throws DataAccessException if data access fail
   */
  protected long queryForLong(final JdbcTemplate jdbcTemplate, final String sql, final Object... args)
      throws DataAccessException {
    Number number = jdbcTemplate.queryForObject(sql, args, Long.class);
    return (number != null ? number.longValue() : 0L);
  }

  /**
   * This is a helper method to replace the deprecated queryForLong method for JdbcTemplate. This will return 0 if the
   * data return from the db query is null. We check that the data is not null otherwise a nullpointerexception will be
   * thrown as the null data will not be converted into a primitive.
   * 
   * see: http://stackoverflow.com/questions/15661313/jdbctemplate-queryforint-long-is-deprecated-in-spring-3-2-2-what-
   * should-it-be-r see:
   * http://docs.spring.io/autorepo/docs/spring-framework/3.2.x/javadoc-api/org/springframework/jdbc/
   * core/simple/SimpleJdbcOperations.html
   * 
   * @param jdbcTemplate used for db access/query
   * @param sql to query the db
   * @param params used for the query
   * @return integer value or 0 if null is return from the db
   * @throws DataAccessException if data access fail
   */
  protected long queryForLong(final NamedParameterJdbcTemplate jdbcTemplate, final String sql,
      final SqlParameterSource params) throws DataAccessException {
    Number number = jdbcTemplate.queryForObject(sql, params, Long.class);
    return (number != null ? number.longValue() : 0L);
  }

}
