/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2011.
 * All Rights Reserved.
 * $Id: package-info.java 41526 2012-01-30 10:43:06Z aaugustyn $
 * ****************************************************************************
 */
/**
 * This package contains the Converters used to convert assets to an image converter's TaskDto
 */
package com.brighttalk.service.channel.integration.imageconverter.dto.converter;