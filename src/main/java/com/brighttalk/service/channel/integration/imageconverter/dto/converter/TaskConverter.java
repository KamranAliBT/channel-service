/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2011.
 * All Rights Reserved.
 * $Id: TaskConverter.java 65442 2013-06-11 12:31:45Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.imageconverter.dto.converter;

import com.brighttalk.service.channel.business.domain.ImageConverterSettings;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.imageconverter.dto.TaskDto;

/**
 * Interface for TaskConverter Converters - used to convert Asset Objects to image conversion TaskDto.
 */
public interface TaskConverter {

  /**
   * Convert the given asset into dto when requesting a thumbnail image.
   * 
   * @param communication object for which we details
   * @param settings details
   * 
   * @return dto
   */
  TaskDto convertForThumbnail(Communication communication, ImageConverterSettings settings);

  /**
   * Convert the given asset into dto when requesting a preview image.
   * 
   * @param communication details
   * @param settings details
   * 
   * @return dto
   */
  TaskDto convertForPreview(Communication communication, ImageConverterSettings settings);

}