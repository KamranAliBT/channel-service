/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: JdbcClientCredentialsDbDao.java 57924 2012-11-26 13:53:01Z apannone $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Spring JDBC Database DAO implementation of {@link ClientCredentialsDbDao}.
 */
@Repository
public class JdbcClientCredentialsDbDao extends JdbcDao implements ClientCredentialsDbDao {

  private static final Logger LOGGER = Logger.getLogger(JdbcClientCredentialsDbDao.class);

  /* @formatter:off */
  //CHECKSTYLE:OFF

  private static final String CLIENT_COUNT_SELECT_QUERY = ""
      + "SELECT "
      + "  COUNT(*) AS count "
      + "FROM "
      + "  api_client_credentials "
      + "WHERE "
      + "  api_key = :apiKey "
      + "  AND api_secret = :apiSecret";

  // CHECKSTYLE:ON
  /* @formatter:on */

  /** JDBC template used to access the database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public boolean exists(final String apiKey, final String apiSecret) {

    LOGGER.debug("Authenticating client [" + apiKey + "] with secret [" + apiSecret + "].");

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("apiKey", apiKey);
    params.addValue("apiSecret", apiSecret);

    int foundClient = queryForInt(jdbcTemplate, CLIENT_COUNT_SELECT_QUERY, params);

    return foundClient > 0;
  }
}
