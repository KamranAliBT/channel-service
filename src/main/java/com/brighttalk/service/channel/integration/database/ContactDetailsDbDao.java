/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ContactDetailsDbDao.java 98330 2015-07-23 09:51:44Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import com.brighttalk.service.channel.business.domain.ContactDetails;

/**
 * DAO for database operations on a {@link ContactDetails} instance.
 */
public interface ContactDetailsDbDao {

  /**
   * Creates a given Contact details object.
   * 
   * @param channelId id of the channel the contact details are associated with
   * @param contactDetails the details to be created
   */
  void create(Long channelId, ContactDetails contactDetails);

  /**
   * Gets the contact details for the identified channel.
   * 
   * @param channelId ID of the channel to get the contact details for.
   * 
   * @return The contact details for the channel. Empty contact details are returned if none are found.
   * 
   * @throws com.brighttalk.service.channel.business.error.ContactDetailsNotFoundException if the contact details are
   * not found for the given channel ID.
   */
  ContactDetails get(Long channelId);

  /**
   * Updates the contact details for the identified channel. If the updates is not successful, we try to create the
   * contact details with data given.
   * 
   * @param channelId ID of the channel to update the contact details for.
   * @param contactDetails The new contact details.
   * 
   * @throws com.brighttalk.common.error.ApplicationException if unable to update.
   * 
   * @return The new contact details for the channel.
   */
  ContactDetails update(Long channelId, ContactDetails contactDetails);
}
