/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: BookingRequestDto.java 83838 2014-09-24 11:10:48Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * BookingRequestDto.
 */
@XmlRootElement(name = "request")
public class BookingRequestDto {

  private List<CommunicationDto> communications;

  public List<CommunicationDto> getCommunications() {
    return communications;
  }

  @XmlElement(name = "communication")
  public void setCommunications(final List<CommunicationDto> communications) {
    this.communications = communications;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("communications: [").append(communications).append("] ");

    return builder.toString();
  }

}