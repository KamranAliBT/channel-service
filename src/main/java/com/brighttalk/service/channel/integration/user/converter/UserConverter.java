/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: UserConverter.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.user.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.integration.dto.UserDto;

/**
 * User Converter used to converte DTO for user.
 */
@Component
public class UserConverter {

  /**
   * Convert the given {@link UserDto} into {@link User}.
   * 
   * @param userDto to be converted
   * 
   * @return DTO object
   */
  public User convert(final UserDto userDto) {

    User user = new User();
    user.setId(userDto.getId());
    user.setExternalId(userDto.getExternalId());
    user.setIsComplete(userDto.getIsComplete());
    user.setIsActive(userDto.getIsActive());
    user.setRealmUserId(userDto.getRealmUserId());
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setEmail(userDto.getEmail());
    user.setTimeZone(userDto.getTimeZone());
    user.setApiKey(userDto.getApiKey());
    user.setCreated(userDto.getCreated());
    user.setLastLogin(userDto.getLastLogin());
    user.setTelephone(userDto.getTelephone());
    user.setJobTitle(userDto.getJobTitle());
    user.setLevel(userDto.getLevel());
    user.setCompanyName(userDto.getCompanyName());
    user.setIndustry(userDto.getIndustry());
    user.setCompanySize(userDto.getCompanySize());
    user.setStateRegion(userDto.getStateRegion());
    user.setCountry(userDto.getCountry());

    return user;
  }

  /**
   * Convert the given {@link UserDto users dto} into {@link User users}.
   * 
   * @param usersDto to be converted
   * 
   * @return DTO object
   */
  public List<User> convert(final List<UserDto> usersDto) {

    List<User> users = new ArrayList<User>();

    if (usersDto == null) {
      return users;
    }

    for (UserDto userDto : usersDto) {
      users.add(convert(userDto));
    }

    return users;
  }

}