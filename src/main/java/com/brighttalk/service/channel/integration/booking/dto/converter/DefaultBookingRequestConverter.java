/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultBookingRequestConverter.java 83838 2014-09-24 11:10:48Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking.dto.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.brighttalk.common.time.Iso8601DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.booking.dto.HDBookingDto;
import com.brighttalk.service.channel.integration.booking.dto.BookingRequestDto;
import com.brighttalk.service.channel.integration.booking.dto.CommunicationDto;

/**
 * Default implementation of an BookingRequestConverter.
 * 
 * This takes a collection of communications and converts it to the relevant DTO object. Used when making a request to
 * the Booking Service to delete one or more BrightTALK communications' bookings.
 */
@Component
public class DefaultBookingRequestConverter implements BookingRequestConverter {

  /** {@inheritDoc} */
  @Override
  public BookingRequestDto convert(final List<Long> communicationIds) {

    BookingRequestDto requestDto = new BookingRequestDto();
    List<CommunicationDto> communicationDtos = new ArrayList<CommunicationDto>();

    for (Long communicationId : communicationIds) {

      CommunicationDto communicationDto = new CommunicationDto();
      communicationDto.setId(communicationId);

      communicationDtos.add(communicationDto);
    }

    requestDto.setCommunications(communicationDtos);

    return requestDto;
  }

  /** {@inheritDoc} */
  @Override
  public HDBookingDto convertForCreateAndUpdate(final Communication communication) {
    HDBookingDto bookingDto = new HDBookingDto();

    bookingDto.setStart(format(communication.getScheduledDateTime()));
    bookingDto.setDuration(communication.getDuration());
    bookingDto.setTimeZone(communication.getTimeZone());

    return bookingDto;
  }

  /**
   * Format the scheduled date to be Iso860.
   * 
   * @param date to be formatted
   * @return the formatted date as string.
   */
  private String format(final Date date) {
    Iso8601DateTimeFormatter iso8601FormattedDate = new Iso8601DateTimeFormatter();
    return iso8601FormattedDate.convert(date);
  }

}