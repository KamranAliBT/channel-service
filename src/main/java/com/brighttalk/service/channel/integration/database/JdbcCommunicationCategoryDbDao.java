/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationCategoryDbDao.java 91799 2015-03-17 13:08:17Z nbrown $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;

/**
 * Spring JDBC Database DAO implementation of {@link CommunicationCategoryDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database.
 */
@Repository
public class JdbcCommunicationCategoryDbDao implements CommunicationCategoryDbDao {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcCommunicationCategoryDbDao.class);

  private static final String CHANNEL_ID_SQL_PARAMETER = "channelId";
  private static final String COMM_ID_SQL_PARAMETER = "commId";
  private static final String CATEGORY_ID_SQL_PARAMETER = "categoryId";
  private static final String CREATED_SQL_PARAMETER = "created";
  private static final String LAST_UPDATED_SQL_PARAMETER = "lastUpdated";

  /* @formatter:off */
  /**
   * Named fields for communication category query.
   */
  private static final String COMMUNICATION_CATEGORY_FIELDS_TO_SET = ""
    + "channel_id = :channelId, "
    + "communication_id = :commId, "
    + "category_id = :categoryId, "
    + "created = :created, "     
    + "last_updated = :lastUpdated ";
  
  
  /**
   * Select all the categories for the given channel and communication ids.
   */
  private static final String FIND_CATEGORIES_BY_IDS = "SELECT" +
      " ch_c.id, " +
      " ch_c.channel_id, " +
      " ch_c.category AS category, " +
      " ch_c.is_active, " +
      " co_c.communication_id as communicationId " +
      "FROM " +
      " communication_category co_c " +
      "JOIN category ch_c ON (co_c.category_id = ch_c.id) " +
      "WHERE " +
      " co_c.communication_id = :communicationId " +
      "AND " +
      " co_c.channel_id = :channelId " +
      "AND " +
      " ch_c.is_active = 1 " +
      "AND " +
      " co_c.is_active = 1 " +
      "ORDER BY " +
      " ch_c.category ASC";
  
  
  /**
   * Insert or update categories.
   */
  // CHECKSTYLE:OFF
  // @formatter:off
  private static final String INSERT_OR_UPDATE_CATEGORIES = "INSERT INTO" +
      " communication_category" +
      " (communication_id, channel_id, category_id, is_active, created, last_updated)" +
      " VALUES" +
      " (:communicationId, :channelId, :categoryId, :isActive, NOW(), NOW())" +
      " ON DUPLICATE KEY UPDATE" +
      " is_active = VALUES(is_active)," +
      " last_updated = VALUES(last_updated)";
  // @formatter:on  
  // CHECKSTYLE:ON

  /** JDBC template used to access the database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public HashMap<Long, CommunicationCategory> find(final Long channelId, final Long communicationId) {
    LOGGER.debug("Finding catgeories for channel [" + channelId + "] and communication [" + communicationId + "].");

    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    params.addValue("channelId", channelId);
    params.addValue("communicationId", communicationId);
    // CHECKSTYLE:ON

    List<CommunicationCategory> categories = jdbcTemplate.query(FIND_CATEGORIES_BY_IDS, params,
        new CommunicationCategoryRowMapper());

    HashMap<Long, CommunicationCategory> keyyedCategories = new HashMap<Long, CommunicationCategory>();

    for (CommunicationCategory communicationCategory : categories) {
      keyyedCategories.put(communicationCategory.getId(), communicationCategory);
    }

    return keyyedCategories;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void create(final CommunicationCategory category) {
    LOGGER.debug("Creating communication category.");

    String sql = "INSERT INTO `communication_category` SET " + COMMUNICATION_CATEGORY_FIELDS_TO_SET;

    jdbcTemplate.update(sql, getCommunicationCategoryParamsMap(category));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void update(final List<CommunicationCategory> categories) {
    if (CollectionUtils.isEmpty(categories)) {
      return;
    }

    LOGGER.debug("Updating categories");

    for (CommunicationCategory communicationCategory : categories) {
      updateCategory(communicationCategory);
    }

  }

  /**
   * Insert or update individual communication category.
   * 
   * @param communicationCategory the communication category to insert or update.
   */
  private void updateCategory(final CommunicationCategory communicationCategory) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("channelId", communicationCategory.getChannelId());
    params.addValue("communicationId", communicationCategory.getCommunicationId());
    params.addValue("categoryId", communicationCategory.getId());
    params.addValue("isActive", communicationCategory.isActive());
    // CHECKSTYLE:ON

    jdbcTemplate.update(INSERT_OR_UPDATE_CATEGORIES, params);

    LOGGER.debug("Insert or Update category [" + communicationCategory.getId() + "] successfull.");
  }

  /**
   * Retrieves a populated parameter map for creating a communication category.
   * 
   * @param category the communication to populate the map with
   * @return the populated map.
   */
  private MapSqlParameterSource getCommunicationCategoryParamsMap(final CommunicationCategory category) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_SQL_PARAMETER, category.getChannelId());
    params.addValue(COMM_ID_SQL_PARAMETER, category.getCommunicationId());
    params.addValue(CATEGORY_ID_SQL_PARAMETER, category.getId());
    params.addValue(CREATED_SQL_PARAMETER, new Date());
    params.addValue(LAST_UPDATED_SQL_PARAMETER, new Date());
    return params;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link CommunicationCategory}.
   */
  protected static class CommunicationCategoryRowMapper implements RowMapper<CommunicationCategory> {

    /**
     * {@inheritDoc}
     */
    @Override
    public CommunicationCategory mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Long categoryId = resultSet.getLong("id");
      Long channelId = resultSet.getLong("channel_id");
      Long communicationId = resultSet.getLong("communicationId");
      String description = resultSet.getString("category");
      boolean isActive = resultSet.getBoolean("is_active");

      CommunicationCategory communicationCategory = new CommunicationCategory(categoryId, channelId, communicationId,
          description, isActive);

      return communicationCategory;
    }
  }

}
