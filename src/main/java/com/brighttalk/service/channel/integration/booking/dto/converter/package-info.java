/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the Converters used to convert communications to a collection of communication dtos.
 */
package com.brighttalk.service.channel.integration.booking.dto.converter;