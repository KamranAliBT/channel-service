/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ResourceDbDao.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;
import java.util.Map;

import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * DAO for database operations on a {@link Resource} instance.
 */
public interface ResourceDbDao {

  /**
   * Gets resource by given id.
   * 
   * @param resourceId the resource id
   * 
   * @return Resource
   * 
   * @throws NotFoundException when resource for given id does not exist.
   */
  Resource find(Long resourceId) throws NotFoundException;

  /**
   * Gets all resource for given communication id.
   * 
   * @param communicationId the communication id.
   * 
   * @return collection of Resource
   */
  List<Resource> findAll(Long communicationId);

  /**
   * Creates a new resource.
   * 
   * @param communicationId the communication to create the resource on.
   * @param resource the resource to be added without specified id.
   * 
   * @return Resource with updated id.
   */
  Resource create(Long communicationId, Resource resource);

  /**
   * Updates resource.
   * 
   * @param resource the resource to be updated.
   * 
   * @return Resource
   */
  Resource update(Resource resource);

  /**
   * Updates resources order.
   * 
   * @param resources collection of resources in new order.
   */
  void updateOrder(List<Resource> resources);

  /**
   * Deletes resource. Soft delete is done.
   * 
   * @param communicationId the communication to delete the resource on.
   * @param resourceId the id of resource to be deleted.
   */
  void delete(Long communicationId, Long resourceId);

  /**
   * Find the Channel and Communication Ids that a given Resource was created in.
   * 
   * This returns a Map with "channelId" and "CommunicationId" entries.
   * 
   * @param resourceId The Id of the resource to look for
   * @return Map containing channelId and communicationId Entries
   * 
   * @throws NotFoundException when resource for given id does not exist.
   */
  Map<String, Long> findChannelIdAndCommunicationId(Long resourceId);
}
