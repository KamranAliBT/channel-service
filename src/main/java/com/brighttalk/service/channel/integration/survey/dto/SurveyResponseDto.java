/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SurveyResponseDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.survey.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.brighttalk.common.error.dto.ResponseErrorDto;
import com.brighttalk.common.form.dto.FormDto;


/**
 * DTO object representing get survey response.
 */
@XmlRootElement(name = "response")
public class SurveyResponseDto {
  /** 
   * Set of valid states that can be reported in a response. 
   */
  public enum State {

    // CHECKSTYLE:OFF
    received,
    processed,
    failed,
    // CHECKSTYLE:ON

    /**
     * Value used when this service is acting as an API client and the API server returns an unrecognised (invalid)
     * value
     */
    unknown;

    /**
     * @return <code>true</code> if this State can be assumed to mean that the associated request was accepted by the
     * API server.
     */
    public boolean isSuccess() {
      return this.equals(processed) || this.equals(received);
    }

    /**
     * @return <code>true</code> if this State can be assumed to mean that the associated request failed (was rejected
     * by the API server).
     */
    public boolean isFailed() {
      return !this.isSuccess();
    }
  }

  /**
   * The value which the {@link #state} property is initialised to on construction if not specified - "processed". This
   * value is appropriate (convenient) when the user service is acting as an API server.
   */
  private static final State DEFAULT_STATE = State.processed;

  /** The published state of this response. */
  private State state = DEFAULT_STATE;

   /** The time this response measured in seconds since midnight, January 1, 1970 UTC. */
  @XmlAttribute( name = "timestamp" )
  private Long timestamp;

  /** The error details for this response, if the request failed */
  @XmlElement( name = "error" )
  private ResponseErrorDto error;

  private FormDto form;
  
  /**
   * @return the timestamp
   */
  public final Long getTimestamp() {
    return this.timestamp;
  }

  /**
   * @return the state
   */
  public final State getState() {
    return this.state;
  }

  /**
   * @param state the state to set
   */
  @XmlAttribute
  public final void setState(State state) {
    this.state = state;
  }

  /**
   * @return the error
   */
  public final ResponseErrorDto getError() {
    return this.error;
  }

  /**
   * Sets the error code to be included in this response payload and additionally sets the payload's state to.
   * 
   * @param errorCode The string representation of the error code.
   */
  public final void setErrorCode(String errorCode) {
    this.setError(new ResponseErrorDto(errorCode));
  }

  /**
   * Sets the error information to be included in this response payload and additionally sets the payload's state to
   * {@link ResponsePayloadDto.State#failed}.
   * 
   * @param error The error information to set in the form of a {@link ResponseErrorDto}.
   */
  private void setError(ResponseErrorDto error) {
    this.error = error;
    this.setState(State.failed);
  }


  public FormDto getForm() {
    return form;
  }

  public void setForm(FormDto form) {
    this.form = form;
  }

  @Override
  public String toString() {
    return "SurveyResponseDto [form=" + form + "]";
  }
 
  
}
