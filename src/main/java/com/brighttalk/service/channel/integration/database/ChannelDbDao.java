/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelDbDao.java 96319 2015-06-09 12:25:26Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;

/**
 * DAO for database operations on a {@link Channel} instance.
 */
public interface ChannelDbDao {

  /**
   * Check if the given channel exists.
   * 
   * @param channelId to be checked.
   * 
   * @return <code>true</code> if the channel exists else <code>false</code> if the channel does not exist or is
   * inactive.
   */
  boolean exists(Long channelId);

  /**
   * Check if the given channel title is already in use.
   * 
   * @param channelTitle to be checked.
   * 
   * @return true or false
   */
  boolean titleInUse(String channelTitle);

  /**
   * Check if the given channel title is already in use, excluding the given channel id in the search.
   * 
   * @param channelTitle to be checked
   * @param channelId to exclude
   * 
   * @return true or false
   */
  boolean titleInUseExcludingChannel(String channelTitle, Long channelId);

  /**
   * Finds the active channel for given channel id.
   * 
   * @param channelId id of the channel to return
   * 
   * @return channel
   * 
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException if the channel has not been found.
   */
  Channel find(Long channelId);

  /**
   * Finds the active channel for channel ids.
   * 
   * @param channelIds ids of the channel to be return
   * 
   * @return channels
   */
  List<Channel> find(List<Long> channelIds);

  /**
   * Find channels that meet the given search criteria.
   * 
   * @param criteria the search criteria
   * 
   * @return paginated list of Channels
   */
  PaginatedList<Channel> find(ChannelsSearchCriteria criteria);

  /**
   * @deprecated Finds the active subscribed channels for given user id.
   * 
   * @param userId id of the user to return the subscribed channels for
   * 
   * @return {@link List list} of channels, or an empty list when no channels are found for the user.
   */
  @Deprecated
  List<Channel> findSubscribedChannels(Long userId);

  /**
   * Find channels the user is subscribed to that meet the given search criteria.
   * 
   * @param userId id of the user to return the subscribed channels for
   * @param criteria the search criteria
   * 
   * @return a result object
   */
  PaginatedList<Channel> findPaginatedChannelsSubscribed(Long userId, MyChannelsSearchCriteria criteria);

  /**
   * Find channels owned by the user that meet the given search criteria.
   * 
   * @param userId id of the user to return the owned channels for
   * @param criteria the search criteria
   * 
   * @return result object
   */
  PaginatedList<Channel> findPaginatedChannelsOwned(Long userId, MyChannelsSearchCriteria criteria);

  /**
   * Creates a given channel.
   * 
   * @param channel object to be created.
   * 
   * @return created channel.
   */
  Channel create(Channel channel);

  /**
   * Updates a given channel.
   * 
   * @param channel object to be updated
   * 
   * @return updated channel
   */
  Channel update(Channel channel);

  /**
   * Delete the given channel.
   * 
   * @param channelId id of the channel to be deleted
   * 
   * @throws com.brighttalk.service.channel.business.error.ChannelNotFoundException if the channel has not been found.
   * @throws com.brighttalk.common.error.ApplicationException if unable to delete
   */
  void delete(Long channelId);

  /**
   * Check if a given user is a presenter on any communication of a channel.
   * 
   * @param user The user to check
   * @param channel The channel to check for
   * 
   * @return <code>true</code> if the user is a presenter else <code>false</code>.
   */
  boolean isPresenter(User user, Channel channel);

  /**
   * Loads the channels statistics: number of recorded communications and number of upcoming communications.
   * 
   * @param channel channel to load the statistics for.
   */
  void loadStatistics(Channel channel);

  /**
   * Loads the channels statistics: number of recorded communications and number of upcoming communications.
   * 
   * @param channels Channels to load statistics for.
   */
  void loadStatistics(List<Channel> channels);

  /**
   * Loads the channel survey.
   * 
   * @param channel channel to load the survey for.
   */
  void loadSurvey(Channel channel);

  /**
   * Loads the channel survey for multiple channels.
   * 
   * @param channels channels to load the survey for.
   */
  void loadSurvey(List<Channel> channels);

}
