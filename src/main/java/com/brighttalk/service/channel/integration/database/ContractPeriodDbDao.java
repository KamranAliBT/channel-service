/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodDbDao.java 101521 2015-10-15 16:45:21Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ContractPeriod;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;
import com.brighttalk.service.channel.business.error.ContractPeriodNotFoundException;

import java.util.List;

/**
 * DAO for the database operations related to a {@link ContractPeriod} instance.
 */
public interface ContractPeriodDbDao {

  /**
   * Creates a given ContractPeriod.
   *
   * @param contractPeriod object to be created.
   *
   * @return created ContractPeriod.
   */
  ContractPeriod create(final ContractPeriod contractPeriod);

  /**
   * Finds the active ContractPeriod for given ContractPeriod id.
   *
   * @param contractPeriodId id of the ContractPeriod to return
   *
   * @return ContractPeriod with the given ID
   *
   * @throws ContractPeriodNotFoundException if the ContractPeriod was not been found.
   */
  ContractPeriod find(final Long contractPeriodId);

  /**
   * Checks if a ContractPeriod with the given ID exists for the given channel ID.
   *
   * @param contractPeriodId id of the ContractPeriod to check
   * @param channelId id of the channel for which the ContractPeriod is being checked
   *
   * @return true if a contract period exists for the given channel with the given id
   */
  boolean exists(final Long contractPeriodId, final Long channelId);

  /**
   * Finds all the active {@link ContractPeriod} for given {@link Channel} id.
   *
   * @param channelId id of the channel for which the ContractPeriods are required.
   *
   * @return {@link List} of active {@link ContractPeriod}s assigned to this channel, sorted by the start date (so the
   * earliest date will be first). An empty list is returned if there are no matches.
   */
  List<ContractPeriod> findAllByChannelId(final Long channelId);

  /**
   * Updates a given {@link ContractPeriod}.
   *
   * @param contractPeriod object to be updated
   *
   * @return updated {@link ContractPeriod}
   */
  ContractPeriod update(final ContractPeriod contractPeriod);

  /**
   * Delete the given {@link ContractPeriod}.
   *
   * @param contractPeriodId id of the ContractPeriod to be deleted
   *
   * @throws ApplicationException if unable to delete
   */
  void delete(Long contractPeriodId);

  /**
   * Checks if a {@link ContractPeriod} already exists for its {@link Channel} at the dates it provides. If the
   * given Contract Period has an ID already, then only those overlapping contract periods with a different ID will
   * cause true to be returned. This is to prevent a contract period from appearing to overlap itself.
   *
   * @param contractPeriod the contract period which we want to check for any existing overlapping contract periods
   *
   * @return true if an existing contract period exists which covers (atleast some) portion of the time covered by
   * this contract period.
   *
   * @throws ChannelNotFoundException if a Channel has not been found for this contract period's channel ID.
   */
  boolean contractPeriodOverlapsExisting(ContractPeriod contractPeriod);
}
