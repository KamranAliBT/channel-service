/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 70804 2013-11-12 14:03:50Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.les.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Communication DTO used when informing LES of a cancelled event.
 */
@XmlRootElement(name = "webcast")
@XmlAccessorType(XmlAccessType.FIELD)
public class CancelCommunicationDto extends BaseCommunicationDto {

}
