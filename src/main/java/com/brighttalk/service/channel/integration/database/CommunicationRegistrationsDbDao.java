/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationRegistrationsDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria;

/**
 * DAO for database operations on communication registrations.
 * <p>
 * Before viewing a webcast, a user might register to view that webcast, unless its anonymous. Every upcoming
 * communication therefore has a number of registrants.
 */
public interface CommunicationRegistrationsDbDao {

  /**
   * Get the total number of communication registrations that meet the given criteria. 
   * 
   * @param userId the id of the user whose registrations are searched for
   * @param criteria the search criteria 
   * 
   * @return total number of communication registrations the met the given criteria
   */
  int findTotalRegistrations(Long userId, CommunicationRegistrationSearchCriteria criteria);

  /**
   * Load a collection of {@link CommunicationRegistration registrations} for a given user and search criteria.
   * 
   * @param userId the id of the user whose registrations are searched for
   * @param criteria the search criteria 
   * 
   * @return a collection of registrations that met the search criteria
   */
  List<CommunicationRegistration> findRegistrations(Long userId, CommunicationRegistrationSearchCriteria criteria);

  /**
   * Load the number of registrants to the given communications in the given channel.
   * 
   * @param channelId the id of the channel to get the registrants for.
   * @param communications the communications to add the registrants to. Should not contain any syndicated out
   * communications as the stats for these are generated separately across all channels.
   * 
   */
  void loadRegistrations(Long channelId, List<Communication> communications);

  /**
   * Load the number of registrants to the given list of syndicated out communications aggregated across all channels
   * the communications are syndicated out to.
   * 
   * @param syndicatedOutCommunications the communications to add the registrants to.
   */
  void loadRegistrations(List<Communication> syndicatedOutCommunications);
}