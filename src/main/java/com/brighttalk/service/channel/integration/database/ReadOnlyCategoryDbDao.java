/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2013.
 * All Rights Reserved.
 * $Id: ReadOnlyCategoryDbDao.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * Read Only implementation of the CategoryDbDao.
 * 
 * This uses a different data source with read only credentials to allow read only requests to be made to a 
 * different database server etc.
 */
@Repository(value = "readOnlyCategoryDbDao")
public class ReadOnlyCategoryDbDao extends JdbcCategoryDbDao {

  /**
   * Set the Datasource - Autowired to be the read only data.
   * 
   * @param dataSource The Read only data source
   */
  @Override
  @Autowired
  public void setDataSource(@Qualifier("channelReadOnlyDataSource") DataSource dataSource) {
    super.setDataSource(dataSource);
  }
}
