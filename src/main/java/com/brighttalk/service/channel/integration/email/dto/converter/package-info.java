/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the Converters used to convert channel business object to a email request DTO.
 */
package com.brighttalk.service.channel.integration.email.dto.converter;