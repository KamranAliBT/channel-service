/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RenditionDbDao.java 47319 2012-04-30 11:25:39Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * DAO for database operations on a {@link Rendition} instance.
 */
public interface RenditionDbDao {

  /**
   * Gets rendition by given id.
   * 
   * @param renditionId the rendition id
   * 
   * @return Rendition
   * 
   * @throws NotFoundException when rendition for given id does not exist.
   */
  Rendition find(Long renditionId) throws NotFoundException;

  /**
   * Gets all rendition for given communication id.
   * 
   * @param communicationId the communication id.
   * 
   * @return collection of Rendition
   */
  List<Rendition> findByCommunicationId(Long communicationId);

  /**
   * Gets all rendition for given list of communication ids.
   * 
   * @param communicationIds list of communication id.
   * 
   * @return collection of Rendition
   */
  List<Rendition> findByCommunicationIds(List<Long> communicationIds);

  /**
   * Creates a new rendition.
   * 
   * @param rendition the rendition to be added without specified id.
   * 
   * @return Rendition with updated id.
   */
  Rendition create(Rendition rendition);

  /**
   * Add the rendition asset.
   * 
   * @param communicationId the communication id.
   * @param renditionId the rendition to add as an asset.
   * 
   */
  void addRenditionAsset(Long communicationId, Long renditionId);

  /**
   * Removes video rendition assets for a given communication id.
   * 
   * @param communicationId the id of the communication the assets are to be removed
   */
  void removeRenditionAssets(Long communicationId);

  /**
   * Deletes rendition. Soft delete is done.
   * 
   * @param renditionId the id of rendition to be deleted.
   */
  void delete(Long renditionId);
}
