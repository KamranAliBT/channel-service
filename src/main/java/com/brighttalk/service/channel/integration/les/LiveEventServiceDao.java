/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: LiveStateServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.les;

import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Interface to define the required methods for the interactions with the Live Event Service.
 */
public interface LiveEventServiceDao {

  /**
   * Inform LES that a webcast has been rescheduled.
   * 
   * @param communication the rescheduled communication
   */
  void reschedule(Communication communication);

  /**
   * Inform LES that a webcast has been cancelled.
   * 
   * @param communication the cancelled communication
   */
  void cancel(Communication communication);

  /**
   * Inform LES that a webcast has been recorded, meaning the transcoding of the video has been successfull.
   * 
   * @param communicationId of the recorded communication
   */
  void recordedSuccess(Long communicationId);

  /**
   * Inform LES that an error occured when the transcoding failed.
   * 
   * @param communicationId of the communication that failed transcoding.
   */
  void recordedError(Long communicationId);
}