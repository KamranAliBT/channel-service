/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: VoteDto.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking.dto;

/**
 * LivePhoneNumberDto.
 */
public class LivePhoneNumberDto {

  private String livePhoneNumber;

  /**
   * @return the livePhoneNumber
   */
  public String getLivePhoneNumber() {
    return livePhoneNumber;
  }

  /**
   * @param livePhoneNumber the livePhoneNumber to set
   */
  public void setLivePhoneNumber(final String livePhoneNumber) {
    this.livePhoneNumber = livePhoneNumber;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("livePhoneNumber: [").append(livePhoneNumber).append("] ");

    return builder.toString();
  }
}