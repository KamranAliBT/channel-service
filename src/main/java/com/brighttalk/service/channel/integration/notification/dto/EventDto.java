/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RequestDto.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.notification.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * RequestDto.
 */
@XmlRootElement(name = "event")
public class EventDto {

  /** TYPE LIVE END constant. */
  public static final String TYPE_LIVE_END = "communication.live.event.end";

  private String type;
  private Long communicationId;

  /**
   * @return the type
   */
  @XmlAttribute
  public String getType() {
    if (type == null) {
      type = TYPE_LIVE_END;
    }
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(final String type) {
    this.type = type;
  }

  /**
   * @return the communicationId
   */
  public Long getCommunicationId() {
    return communicationId;
  }

  /**
   * @param communicationId the communicationId to set
   */
  public void setCommunicationId(final Long communicationId) {
    this.communicationId = communicationId;
  }

}