/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RequestDto.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.slide.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * RequestDto.
 */
@XmlRootElement(name = "request")
public class RequestDto {

  private CommunicationDto communication;

  public CommunicationDto getCommunication() {
    return communication;
  }

  public void setCommunication(final CommunicationDto communication) {
    this.communication = communication;
  }

  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("communication: [").append(communication).append("] ");

    return builder.toString();
  }
}