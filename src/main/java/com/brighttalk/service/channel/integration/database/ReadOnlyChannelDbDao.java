/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2013.
 * All Rights Reserved.
 * $Id: ReadOnlyChannelDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * Read Only implementation of the ChannelDbDao.
 * 
 * This uses a different data source with read only credentials to allow read only requests to be made to a 
 * different database server etc.
 */
@Repository(value = "readOnlyChannelDbDao")
public class ReadOnlyChannelDbDao extends JdbcChannelDbDao {

  /**
   * Set the Datasource - Autowired to be the read only data.
   * 
   * @param dataSource The Read only data source
   */
  @Override
  @Autowired
  public void setDataSource(@Qualifier("channelReadOnlyDataSource") DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * Channels cannot be created with a read only DbDao.
   * 
   * @param channel The Channel
   * @return The created Channel
   * @throws UnsupportedOperationException Thrown as this method cannot be used.
   */
  @Override
  public Channel create(Channel channel) {
    throw new UnsupportedOperationException("Cannot Create A Channel with a Readonly Connection");
  }

  /**
   * Channels cannot be updated with a read only DbDao.
   * 
   * @param channel The Channel
   * @return The created Channel
   * @throws UnsupportedOperationException Thrown as this method cannot be used.
   */
  @Override
  public Channel update(Channel channel) {
    throw new UnsupportedOperationException("Cannot Update A Channel with a Readonly Connection");
  }

  /**
   * Channels cannot be deleted with a read only DbDao.
   * 
   * @param channelId The Id of the channel
   * @throws UnsupportedOperationException Thrown as this method cannot be used.
   */
  @Override
  public void delete(Long channelId) {
    throw new UnsupportedOperationException("Cannot Delete A Channel with a Readonly Connection");
  }
}
