/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 55023 2012-10-01 10:22:47Z afernandes $
 * ****************************************************************************
 */
/**
 * This package contains the Converters used to convert user business object.
 */
package com.brighttalk.service.channel.integration.user.converter;