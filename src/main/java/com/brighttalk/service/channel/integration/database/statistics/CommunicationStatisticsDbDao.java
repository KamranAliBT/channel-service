/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationStatisticsDbDao.java 98706 2015-07-31 13:38:59Z ssarraj $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database.statistics;

import java.util.List;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;

/**
 * DAO for database operations on communication statistics.
 * <p>
 * Communication Statistics are things like total rating for a webcast, average rating, total viewing duration etc.
 */
public interface CommunicationStatisticsDbDao {

  /**
   * Load communication statistics to the given list of communications in the given channel.
   * 
   * @param channelId the id of the channel to get the statistics for.
   * @param communications the communications to add the statistics to. Should not contain any syndicated out
   * communications as the stats for these are generated separately across all channels.
   */
  void loadStatistics(Long channelId, List<Communication> communications);

  /**
   * Add communication statistics to the given list of syndicated out communications aggregated across all channels the
   * communications are syndicated out to.
   * 
   * @param syndicatedOutCommunications the communications to add the statistics to.
   */
  void loadStatisticsFromAllChannels(List<Communication> syndicatedOutCommunications);

  /**
   * Find all communication activities statistics (viewings, pre-registration and rating totals) for the supplied
   * communication in all the supplied channels.
   * 
   * @param commId The communication id.
   * @param channelIds The list of channel ids.
   * @return List of {@link CommunicationStatistics} of the identified communication statistics in each of the supplied
   * channels.
   */
  List<CommunicationStatistics> findCommunicationStatisticsByChannelIds(Long commId, List<Long> channelIds);

}
