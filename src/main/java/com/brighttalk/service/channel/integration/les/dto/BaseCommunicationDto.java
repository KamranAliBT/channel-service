/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 70804 2013-11-12 14:03:50Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.les.dto;

/**
 * Base Communication DTO extended by the rescheduled, cancelled dtos.
 */
public abstract class BaseCommunicationDto {

  /** id of the communication */
  protected Long id;

  /**
   * @return the communicationId
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id of the communication to set
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("communication id: [").append(id).append("] ");

    return builder.toString();
  }
}
