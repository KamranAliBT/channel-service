/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ImageConverterServiceDao.java 65431 2013-06-11 10:46:49Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.imageconverter;

import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Interface to the image converter service Dao.
 * 
 * This lists what methods are available to interact with the image converter service
 */
public interface ImageConverterServiceDao {

  /**
   * Send request to process feature image and create thumbnail.
   * 
   * @param communication details
   */
  void processThumbnailImage(Communication communication);

  /**
   * Send request to process feature image and create preview.
   * 
   * @param communication details
   */
  void processPreviewImage(Communication communication);
}