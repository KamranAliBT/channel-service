/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CategoryDbDao.java 63781 2013-04-29 10:40:35Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

/**
 * DAO for database operations on communication pin numbers.
 */
public interface PinGeneratorDbDao {

  /**
   * Check if the pin has already been used by a different communication in the system.
   * 
   * @param pinNumber the pin number to check.
   * 
   * @return boolean, true if the pin has been used before.
   */
  boolean isPinUsed(String pinNumber);
}
