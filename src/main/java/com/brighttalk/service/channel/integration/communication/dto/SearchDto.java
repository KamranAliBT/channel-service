/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: SearchDto.java 89869 2015-02-12 17:26:56Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.communication.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Search communication DTO.
 */
@XmlRootElement(name = "search")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchDto {

  private Integer offset;
  private Integer limit;
  private SortDto sort;
  @XmlElementWrapper(name = "restrictions")
  private List<CommunicationDto> communications;

  /**
   * @return the offset
   */
  public Integer getOffset() {
    return offset;
  }

  /**
   * @return the limit
   */
  public Integer getLimit() {
    return limit;
  }

  /**
   * @return the sort
   */
  public SortDto getSort() {
    return sort;
  }

  /**
   * @return the communications
   */
  public List<CommunicationDto> getCommunications() {
    return communications;
  }

  /**
   * @param offset the offset to set
   */
  public void setOffset(Integer offset) {
    this.offset = offset;
  }

  /**
   * @param limit the limit to set
   */
  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  /**
   * @param sort the sort to set
   */
  public void setSort(SortDto sort) {
    this.sort = sort;
  }

  /**
   * @param communications the communications to set
   */
  public void setCommunications(List<CommunicationDto> communications) {
    this.communications = communications;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("offset", offset);
    builder.append("limit", limit);
    builder.append("sort", sort);
    builder.append("communications", communications);
    return builder.toString();
  }

}
