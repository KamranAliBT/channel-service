/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AudienceServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audience;

import java.util.List;

/**
 * Interface to define the required methods for the interactions with the Audience Service.
 */
public interface AudienceServiceDao {

  /**
   * Find all the votes in a communication.
   * 
   * @param communicationId the id of the communication we want to load votes for.
   * 
   * @return collection of votes
   */
  List<Long> findVoteIds(Long communicationId);

  /**
   * Delete a given vote by id.
   * 
   * @param voteId the id of the vote
   */
  void deleteVote(Long voteId);
}