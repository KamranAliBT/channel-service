/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpBookingServiceDao.java 83838 2014-09-24 11:10:48Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.booking.dto.BookingRequestDto;
import com.brighttalk.service.channel.integration.booking.dto.converter.BookingRequestConverter;

/**
 * Implementation of the Dao to interact with the Booking Service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
public class XmlHttpBookingServiceDao implements BookingServiceDao {

  private static final Logger LOGGER = Logger.getLogger(XmlHttpBookingServiceDao.class);

  /**
   * Delete bookings request path.
   */
  protected static final String DELETE_REQUEST_PATH = "/internal/xml/delete";

  @Autowired
  private BookingRequestConverter converter;

  private RestTemplate restTemplate;

  private String serviceBaseUrl;

  public BookingRequestConverter getConverter() {
    return converter;
  }

  public void setConverter(final BookingRequestConverter converter) {
    this.converter = converter;
  }

  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public String getServiceBaseUrl() {
    return serviceBaseUrl;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  /** {@inheritDoc} */
  @Override
  public void deleteBookings(final List<Long> communicationIds) {

    if (communicationIds == null || communicationIds.size() == 0) {
      return;
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Sending request to delete bookings for [" + communicationIds.size() + "] communications.");
    }

    BookingRequestDto request = converter.convert(communicationIds);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Sending delete booking request [" + request + "].");
    }

    String url = serviceBaseUrl + DELETE_REQUEST_PATH;

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleting bookings using URL [" + url + "].");
    }

    restTemplate.postForLocation(url, request);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleted bookings for communications [" + StringUtils.join(communicationIds, ", ") + "].");
    }
  }

}