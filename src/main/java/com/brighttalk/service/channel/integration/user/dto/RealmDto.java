/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: RealmDto.java 55023 2012-10-01 10:22:47Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.user.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * User Realm Dto.
 */
@XmlRootElement(name = "realm")
public class RealmDto {

  private Integer id;

  private String name;

  public Integer getId() {
    return id;
  }

  @XmlAttribute
  public void setId(final Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  @XmlAttribute
  public void setName(final String name) {
    this.name = name;
  }

}
