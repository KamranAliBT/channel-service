/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpAudienceServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audience;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.audience.dto.VoteDto;
import com.brighttalk.service.channel.integration.audience.dto.VotesDto;

/**
 * Implementation of the Dao to interact with the Audience service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
public class XmlHttpAudienceServiceDao implements AudienceServiceDao {

  /**
   * Path used when calling audience service to retrieve all votes in a communication.
   */
  protected static final String VOTES_REQUEST_PATH = "/internal/webcast/{id}/votes";

  /**
   * Path used when calling audience service to retrieve all votes in a communication.
   */
  protected static final String DELETE_VOTE_REQUEST_PATH = "/internal/vote/{id}";

  private static final Logger LOGGER = Logger.getLogger(XmlHttpAudienceServiceDao.class);

  private RestTemplate restTemplate;

  private String serviceBaseUrl;

  /** {@inheritDoc} */
  @Override
  public List<Long> findVoteIds(final Long communicationId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Retrieving votes from Audience Service for communication [" + communicationId + "].");
    }

    final String url = serviceBaseUrl + VOTES_REQUEST_PATH;
    VotesDto votesDto = restTemplate.getForObject(url, VotesDto.class, communicationId);

    List<Long> voteIds = new ArrayList<Long>();

    if (votesDto.getVotes() != null) {
      for (VoteDto voteDto : votesDto.getVotes()) {
        voteIds.add(voteDto.getId());
      }
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + voteIds.size() + "] votes for communication [" + communicationId + "].");
    }

    return voteIds;
  }

  /** {@inheritDoc} */
  @Override
  public void deleteVote(final Long voteId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Calling Audience Service to delete vote [" + voteId + "].");
    }

    String url = serviceBaseUrl + DELETE_VOTE_REQUEST_PATH;
    restTemplate.delete(url, voteId);
  }

  public String getServiceBaseUrl() {
    return serviceBaseUrl;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }
}