/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcScheduledPublicationDbDao.java 91799 2015-03-17 13:08:17Z nbrown $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.business.domain.communication.CommunicationScheduledPublication;

/**
 * Spring JDBC Database DAO implementation of {@link ScheduledPublicationDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database retrieve information about scheduled publication tasks.
 */
@Repository
@Primary
public class JdbcScheduledPublicationDbDao implements ScheduledPublicationDbDao {

  private static final String JOB_ID_FIELD = "job_id";
  private static final String COMMUNICATION_ID_FIELD = "communicationId";

  private static final Logger LOGGER = Logger.getLogger(JdbcScheduledPublicationDbDao.class);

  /* @formatter:off */
  //CHECKSTYLE:OFF
  private static final String MARK_AS_COMPLETED_QUERY = "UPDATE"
      + "  scheduled_publication "
      + "SET "
      + "  completed = 1, "
      + "  last_updated = NOW() "
      + "WHERE "
      + "  job_id = job_id";
  // CHECKSTYLE:ON
  /* @formatter:on */

  /* @formatter:off */
  //CHECKSTYLE:OFF
  private static final String UPDATE_QUERY_FOR_PRESELECT_OF_SCHEDULED_COMMUNICATIONS = "UPDATE"
      + "  scheduled_publication "
      + "SET "
      + " job_id = :job_id "
      + "WHERE "
      + "  scheduled <= NOW() "
      + "  AND completed = 0 "
      + "  AND is_active = 1 "
      + "  AND job_id IS NULL ";
  // CHECKSTYLE:ON
  /* @formatter:on */

  /* @formatter:off */
  // CHECKSTYLE:OFF
  private static final String SELECT_QUERY_FOR_SCHEDULED_COMMUNICATIONS = "SELECT"
      + " communication_id AS id, "
      + " publish_status AS publishStatus "
      + "FROM"
      + "  scheduled_publication "
      + "WHERE "
      + "  job_id = :job_id ";
  // CHECKSTYLE:ON
  /* @formatter:on */

  /* @formatter:off */
  //CHECKSTYLE:OFF
  private static final String DEACTIVATE_SCHEDULED_PUBLICATION = "UPDATE "
      + "  scheduled_publication "
      + "SET "
      + "  is_active = 0, "
      + "  last_updated = NOW() "
      + "WHERE "
      + "  communication_id = :communicationId " 
      + "AND "
      + " is_active = 1";
  // CHECKSTYLE:ON
  /* @formatter:on */

  /* @formatter:off */
  //CHECKSTYLE:OFF
  private static final String UPDATE_SCHEDULED_PUBLICATION = "INSERT INTO " +
      " scheduled_publication " +
      "SET " +
      " communication_id = :communicationId, " +
      " scheduled = FROM_UNIXTIME(:scheduledPublicationDate) , " +
      " publish_status = :publishStatus, " +
      " is_active = 1, " +
      " created = NOW(), " +
      " last_updated = NOW() " +
      "ON DUPLICATE KEY UPDATE " +
      " job_id = null, " +
      " scheduled = VALUES(scheduled), " +
      " publish_status = VALUES(publish_status), " +
      " completed = 0, " +
      " is_active = VALUES(is_active), " +
      " last_updated = NOW();";
  // CHECKSTYLE:ON
  /* @formatter:on */

  /* @formatter:off */
  //CHECKSTYLE:OFF
  private static final String FIND_SCHEDULED_PUBLICATION = "SELECT " +
      " scheduled, " +
      " publish_status " +
      "FROM " +
      " scheduled_publication " +
      "WHERE " +
      " communication_id = :communicationId " +
      "AND " +
      " is_active = 1 " +
      "AND " +
      "completed = 0;";
  // CHECKSTYLE:ON
  /* @formatter:on */

  private NamedParameterJdbcTemplate jdbcTemplate;

  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  @Override
  public Map<PublishStatus, List<Long>> findScheduledCommunications(final String jobId) {

    if (preselect(jobId) == 0) {

      Map<PublishStatus, List<Long>> communications = new HashMap<PublishStatus, List<Long>>();
      communications.put(PublishStatus.PUBLISHED, new ArrayList<Long>());
      communications.put(PublishStatus.UNPUBLISHED, new ArrayList<Long>());
      return communications;
    }

    return findTasks(jobId);
  }

  /**
   * Preselect the scheduled tasks to load - Optimistic Locking.
   * 
   * @param jobId the job id to be assigned for the preselected tasks.
   * 
   * @return a number of preselected communications
   */
  private int preselect(final String jobId) {

    final MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(JOB_ID_FIELD, jobId);

    int noOfRows = jdbcTemplate.update(UPDATE_QUERY_FOR_PRESELECT_OF_SCHEDULED_COMMUNICATIONS, params);

    LOGGER.debug("Updated [" + noOfRows + "] scheduled communications with [" + jobId + "].");

    return noOfRows;
  }

  /**
   * Find scheduled communication tasks for a given job id.
   * 
   * @param jobId the job id used to find publication tasks
   */
  private Map<PublishStatus, List<Long>> findTasks(final String jobId) {

    final MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(JOB_ID_FIELD, jobId);

    List<Map<String, Object>> scheduledTasks = jdbcTemplate.queryForList(SELECT_QUERY_FOR_SCHEDULED_COMMUNICATIONS,
        params);

    List<Long> publishedCommunications = new ArrayList<Long>();
    List<Long> unpublishedCommunications = new ArrayList<Long>();
    for (Map<String, Object> task : scheduledTasks) {

      Long commId = Long.valueOf(task.get("id").toString());
      PublishStatus status = PublishStatus.valueOf(task.get("publishStatus").toString().toUpperCase());

      List<Long> targetCollection = PublishStatus.PUBLISHED.equals(status) ? publishedCommunications
          : unpublishedCommunications;
      targetCollection.add(commId);
    }

    final Map<PublishStatus, List<Long>> communications = new HashMap<PublishStatus, List<Long>>();
    communications.put(PublishStatus.PUBLISHED, publishedCommunications);
    communications.put(PublishStatus.UNPUBLISHED, unpublishedCommunications);

    return communications;
  }

  @Override
  public void markAsCompleted(final String jobId) {

    LOGGER.debug("Marking scheduled publications tasks [" + jobId + "] as completed.");

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(JOB_ID_FIELD, jobId);

    int noOfRows = jdbcTemplate.update(MARK_AS_COMPLETED_QUERY, params);

    LOGGER.debug("Updated [" + noOfRows + "] scheduled publication tasks.");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void update(final Communication communication) {
    LOGGER.info("Updating scheduled publication for commnuication [" + communication.getId() + "]");

    CommunicationScheduledPublication communicationScheduledPublication = communication.getScheduledPublication();

    String publishStatus = null;
    Date scheduledPublicationDate = communicationScheduledPublication.getDefaultScheduledPublicationDate();

    if (communicationScheduledPublication.hasPublicationDate()
        && !communicationScheduledPublication.isDefaultPublicationDate()) {

      publishStatus = Communication.PublishStatus.PUBLISHED.toString().toLowerCase();
      scheduledPublicationDate = communicationScheduledPublication.getPublicationDate();

    } else if (communicationScheduledPublication.hasUnpublicationDate()
        && !communicationScheduledPublication.isDefaultUnpublicationDate()) {

      publishStatus = Communication.PublishStatus.UNPUBLISHED.toString().toLowerCase();
      scheduledPublicationDate = communicationScheduledPublication.getUnpublicationDate();

    }

    final MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COMMUNICATION_ID_FIELD, communication.getId());
    params.addValue("publishStatus", publishStatus);
    params.addValue("scheduledPublicationDate", scheduledPublicationDate.getTime() / 1000);

    jdbcTemplate.update(UPDATE_SCHEDULED_PUBLICATION, params);

    LOGGER.info("Scheduled publication updated for communication [" + communication.getId() + "].");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deactivatePublicationDate(final Communication communication) {
    LOGGER.info("Deactivating scheduled publication for communication [" + communication.getId() + "]");

    final MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COMMUNICATION_ID_FIELD, communication.getId());

    String sql = DEACTIVATE_SCHEDULED_PUBLICATION;

    if (communication.hasPublishStatus()) {
      sql.concat(" AND publish_status = :publishStatus ");
    }

    jdbcTemplate.update(sql, params);

    LOGGER.debug("Scheduled publication deactivated for communication [" + communication.getId() + "].");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CommunicationScheduledPublication find(final Long communicationId) {
    LOGGER.debug("Find all the active scheduled publication dates for communication [" + communicationId + "]");

    final MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COMMUNICATION_ID_FIELD, communicationId);

    List<Map<String, Object>> scheduledPublications = jdbcTemplate.queryForList(FIND_SCHEDULED_PUBLICATION, params);

    CommunicationScheduledPublication communicationScheduledPublication = new CommunicationScheduledPublication();

    for (Map<String, Object> scheduledPublication : scheduledPublications) {

      Date scheduled = (Date) scheduledPublication.get("scheduled");
      PublishStatus publishStatus =
          PublishStatus.valueOf(scheduledPublication.get("publish_status").toString().toUpperCase());

      if (publishStatus.equals(PublishStatus.PUBLISHED)) {
        communicationScheduledPublication.setPublicationDate(scheduled);
      } else if (publishStatus.equals(PublishStatus.UNPUBLISHED)) {
        communicationScheduledPublication.setUnpublicationDate(scheduled);
      }

    }
    return communicationScheduledPublication;
  }
}