/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FormDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.enquiry.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * FormDto.
 */
public class FormDto {

  private String id;
  
  private List<FormAnswerDto> formAnswers;

  public String getId() {
    return id;
  }

  @XmlAttribute
  public void setId(String id) {
    this.id = id;
  }

  public List<FormAnswerDto> getFormAnswers() {
    return formAnswers;
  }

  @XmlElement(name = "formAnswer")
  public void setFormAnswers(List<FormAnswerDto> formAnswers) {
    this.formAnswers = formAnswers;
  }
}