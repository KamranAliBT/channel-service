/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the Converters used to convert communications to a collection of 
 * {@link com.brighttalk.service.channel.integration.community.dto.CommunicationDto} dtos.
 */
package com.brighttalk.service.channel.integration.community.dto.converter;