/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpSlideServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.slide;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.slide.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.slide.dto.RequestDto;
import com.brighttalk.service.channel.integration.slide.dto.SlideDto;

/**
 * Implementation of the Dao to interact with the Slide service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
public class XmlHttpSlideServiceDao implements SlideServiceDao {

  /**
   * Path used when calling slide service to delete all slide in a communication.
   */
  protected static final String DELETE_REQUEST_PATH = "/internal/xml/update";

  private static final Logger LOGGER = Logger.getLogger(XmlHttpSlideServiceDao.class);

  private RestTemplate restTemplate;

  private String serviceBaseUrl;

  public String getServiceBaseUrl() {
    return serviceBaseUrl;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Override
  public void deleteSlides(final Long communicationId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Removing communication [" + communicationId + "] slides by calling Slide Service.");
    }

    RequestDto requestDto = createRequestDto(communicationId);

    String url = serviceBaseUrl + DELETE_REQUEST_PATH;

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Removing communication [" + communicationId + "] slides. Url [" + url + "] and request ["
          + requestDto + "].");
    }

    restTemplate.postForLocation(url, requestDto);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleted communication [" + communicationId + "] slides.");
    }
  }

  private RequestDto createRequestDto(final Long communicationId) {

    final RequestDto dto = new RequestDto();

    final CommunicationDto communicationDto = new CommunicationDto();
    communicationDto.setId(communicationId);
    communicationDto.setSlides(new ArrayList<SlideDto>());

    dto.setCommunication(communicationDto);

    return dto;
  }
}