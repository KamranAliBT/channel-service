/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpSurveyServiceDao.java 83927 2014-09-26 09:14:38Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.survey;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.form.converter.FormConverter;
import com.brighttalk.common.form.domain.Form;
import com.brighttalk.common.form.dto.FormDto;
import com.brighttalk.service.channel.integration.survey.dto.SurveyResponseDto;
import com.brighttalk.service.channel.integration.survey.validator.FormCreateValidator;

/**
 * Implementation of the Dao to interact with the Survey service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
@Component
public class XmlHttpSurveyServiceDao implements SurveyServiceDao {

  private static final Logger logger = Logger.getLogger(XmlHttpSurveyServiceDao.class);

  private static final String ACCEPT_HEADER = "Accept";

  private static final String APPLICATION_XML = "application/xml";

  /**
   * Get Survey Form request path.
   */
  protected static final String GET_SURVEY_PATH = "/internal/xml/get/";

  @Autowired
  @Qualifier(value = "surveyServiceRestTemplate")
  private RestTemplate restTemplate;

  @Value(value = "${service.survey.service.base.url}")
  private String serviceBaseUrl;

  @Autowired
  private FormConverter formConverter;

  /** {@inheritDoc} */
  @Override
  public Form getSurveyForm(Long surveyId) {

    String url = serviceBaseUrl + GET_SURVEY_PATH + surveyId;

    MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
    headers.add(ACCEPT_HEADER, APPLICATION_XML);

    HttpEntity<?> entity = new HttpEntity<Object>(headers);

    if (logger.isDebugEnabled()) {
      logger.debug("Get Survey from Survey service using [" + url + "]. Headers [" + entity.getHeaders() + "].");
    }

    ResponseEntity<SurveyResponseDto> response = restTemplate.exchange(url, HttpMethod.GET, entity,
        SurveyResponseDto.class);

    if (logger.isDebugEnabled()) {
      logger.debug("Response from Get Survey. Status: [" + response.getStatusCode() + "], " + "Headers: ["
          + response.getHeaders() + "] and survey form [" + response.getBody().getForm() + "].");
    }

    if (response.getBody().getState().isFailed()) {
      logger.error("Error recieved when requesting survey service." + response.getBody().getError());

      throw new ApplicationException(response.getBody().getError().getCode());
    }

    // validate the formDto results before converting the form business objects.
    FormDto formDto = response.getBody().getForm();
    formDto.validate(new FormCreateValidator());

    // convert successful response to business object
    Form form = this.formConverter.convert(response.getBody().getForm());
    return form;
  }

  public void setRestTemplate(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public void setServiceBaseUrl(String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public void setFormConverter(FormConverter formConverter) {
    this.formConverter = formConverter;
  }
}