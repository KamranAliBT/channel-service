/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EmailRequestConverter.java 71415 2013-11-29 12:05:10Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.les.dto.converter;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.brighttalk.common.time.Iso8601DateTimeFormatter;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.les.dto.CancelCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.RescheduleCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.VideoUploadCompleteCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.VideoUploadErrorCommunicationDto;

/**
 * Live Event Request Converter used to create DTO for communicating with the Live event service.
 */
@Component
public class LiveEventRequestConverter {

  /**
   * Convert the given communication into reschedule communication dto.
   * 
   * @param communication to be converted
   * 
   * @return DTO object
   */
  public RescheduleCommunicationDto convertForReschedule(final Communication communication) {
    RescheduleCommunicationDto communicationDto = new RescheduleCommunicationDto();
    communicationDto.setId(communication.getId());
    communicationDto.setScheduled(format(communication.getScheduledDateTime()));
    communicationDto.setDuration(communication.getDuration());
    communicationDto.setTimezone(communication.getTimeZone());

    return communicationDto;
  }

  /**
   * Convert the given communication into cancel communication dto.
   * 
   * @param communication to be converted
   * 
   * @return DTO object
   */
  public CancelCommunicationDto convertForCancel(final Communication communication) {
    CancelCommunicationDto communicationDto = new CancelCommunicationDto();
    communicationDto.setId(communication.getId());

    return communicationDto;
  }

  /**
   * Convert the given communication into recorded communication dto.
   * 
   * @param communicationId of the webcast that has been recorded
   * 
   * @return DTO object
   */
  public VideoUploadCompleteCommunicationDto convertForVideoUploadComplete(final Long communicationId) {
    VideoUploadCompleteCommunicationDto communicationDto = new VideoUploadCompleteCommunicationDto();
    communicationDto.setId(communicationId);

    return communicationDto;
  }

  /**
   * Convert the given communication into error communication dto.
   * 
   * @param communicationId of the webcast that has failed transcoding
   * 
   * @return DTO object
   */
  public VideoUploadErrorCommunicationDto convertForVideoUploadError(final Long communicationId) {
    VideoUploadErrorCommunicationDto communicationDto = new VideoUploadErrorCommunicationDto();
    communicationDto.setId(communicationId);

    return communicationDto;
  }

  /**
   * Format the scheduled date to be Iso860.
   * 
   * @param date to be formatted
   * @return the formatted date as string.
   */
  private String format(final Date date) {
    Iso8601DateTimeFormatter iso8601FormattedDate = new Iso8601DateTimeFormatter();
    return iso8601FormattedDate.convert(date);
  }
}