/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: UsersDto.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.user.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.brighttalk.common.user.integration.dto.UserDto;

/**
 * Represents a users search of BrightTALK.
 */
@XmlRootElement(name = "users")
@XmlType(name = "usersInternalResponse", namespace = "usersInternalResponse")
public class UsersDto {

  private Integer quantity;

  private List<UserDto> users;

  /**
   * Default constructor.
   */
  public UsersDto() {

  }

  public Integer getQuantity() {
    return quantity;
  }

  @XmlAttribute
  public void setQuantity(final Integer quantity) {
    this.quantity = quantity;
  }

  public List<UserDto> getUsers() {
    return users;
  }

  @XmlElement(name = "user", type = UserDto.class)
  public void setUsers(final List<UserDto> users) {
    this.users = users;
  }

}
