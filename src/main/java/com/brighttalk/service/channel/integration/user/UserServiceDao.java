/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: UserServiceDao.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.user;

import java.util.List;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;

/**
 * Interface to define the required methods for the interactions with the User Service.
 */
public interface UserServiceDao {

  /**
   * Find <em>active</em> users using their id.
   * 
   * @param id The id to search on.
   * 
   * @return {@link User}
   */
  User find(Long id);

  /**
   * Find <em>active</em> users using their ids.
   * 
   * @param ids The ids to search on.
   * @param criteria The search criteria.
   * 
   * @return {@link User users}
   */
  List<User> find(List<Long> ids, UsersSearchCriteria criteria);

  /**
   * Find <em>active</em> users using their email address.
   * 
   * @param emailAddresses The email addresses to search on.
   * @param criteria The search criteria.
   * 
   * @return {@link User}
   */
  List<User> findByEmailAddress(List<String> emailAddresses, UsersSearchCriteria criteria);

  /**
   * Find <em>active</em> users using their email address.
   * 
   * @param emailAddresses The email addresses to search on.
   * @param criteria The search criteria.
   * 
   * @return {@link PaginatedList} of users
   */
  PaginatedList<User> findByEmailAddress(String emailAddresses, UsersSearchCriteria criteria);

  /**
   * Find <em>active</em> users using their IDs and email addresses.
   * 
   * @param ids The IDs to search on.
   * @param emailAddresses The email addresses to search on.
   * @param criteria The search criteria.
   * 
   * @return {@link User users}
   */
  List<User> findByIdAndEmailAddress(List<Long> ids, List<String> emailAddresses, UsersSearchCriteria criteria);

}