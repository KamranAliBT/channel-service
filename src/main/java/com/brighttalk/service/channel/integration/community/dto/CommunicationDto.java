/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.community.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * CommunicationDto.
 */
public class CommunicationDto {

  private String href;

  public String getHref() {
    return href;
  }

  @XmlAttribute
  public void setHref(String href) {
    this.href = href;
  }
  
  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  public String toString() {
    
    StringBuilder builder = new StringBuilder();
    builder.append("href: [").append( href ).append("] ");

    return builder.toString();
  }
}
