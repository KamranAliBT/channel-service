/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcChannelDbDao.java 98771 2015-08-04 16:31:59Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.User;
import com.brighttalk.common.user.User.Role;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;
import com.brighttalk.service.channel.business.domain.Keywords;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.Survey;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Channel.Searchable;
import com.brighttalk.service.channel.business.domain.channel.ChannelStatistics;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.business.domain.channel.ChannelsSearchCriteria;
import com.brighttalk.service.channel.business.domain.channel.MyChannelsSearchCriteria;
import com.brighttalk.service.channel.business.error.ChannelNotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link ChannelDbDao} which uses JDBC via Spring's SimpleJDBCTemplate to
 * access the Database.
 */
@Repository
@Primary
public class JdbcChannelDbDao extends JdbcDao implements ChannelDbDao {

  /** Set of channel database field names. */
  private static final String ID_FIELD = "id";

  private static final String IS_ACTIVE = "is_active";

  private static final String LAST_UPDATED_FIELD = "last_updated";

  private static final String CREATED_FIELD = "created";

  private static final String OWNER_USER_ID_FIELD = "owner_user_id";

  private static final String STRAPLINE_FIELD = "strapline";

  private static final String ORGANISATION_FIELD = "organisation";

  private static final String KEYWORDS_FIELD = "keywords";

  private static final String DESCRIPTION_FIELD = "description";

  private static final String TITLE_FIELD = "title";

  private static final String VIEWED_FOR_FIELD = "viewed_for";

  private static final String RATING_FIELD = "rating";

  private static final String CHANNEL_ID_FIELD = "channel_id";

  private static final String CHANNEL_TITLE_FIELD = "title";

  private static final String IS_SEARCHABLE_FIELD = "is_searchable";

  private static final String PROMOTE_ON_HOMEPAGE_FIELD = "promote_on_homepage";

  private static final String CHANNEL_TYPE_FIELD = "type";

  private static final String CHANNEL_TYPE_ID_FIELD = "channel_type_id";

  private static final String CREATED_CHANNEL_TYPE_ID_FIELD = "created_channel_type_id";

  private static final String PENDING_CHANNEL_TYPE_ID_FIELD = "pending_channel_type_id";

  private static final String CHANNEL_TYPE_CREATED_FIELD = "created_type";

  private static final String CHANNEL_TYPE_PENDING_FIELD = "pending_type";

  private static final String UPCOMING_COUNT_FIELD = "upcoming_count";

  private static final String RECORDED_COUNT_FIELD = "recorded_count";

  private static final String LIVE_COUNT_FIELD = "live_count";

  private static final String SUBSCRIBER_COUNT_FIELD = "subscriber_count";

  private static final String OWNER_USER_ID = "owner_user_id";

  private static final String SPACE = " ";

  /**
   * Keywords utility class used to convert {@ink Channel} keywords from/to comma separated string to list of string
   * keywords.
   */
  @Autowired
  private Keywords keywords;

  /* @formatter:off */
  //CHECKSTYLE:OFF

  private static final String CHANNEL_FIELDS = ""
      + "c.id AS id, "
      + "c.title AS title, "
      + "c.keywords AS keywords, "
      + "c.organisation AS organisation, "
      + "c.strapline AS strapline, "
      + "c.description AS description, "
      + "c.owner_user_id AS owner_user_id, "
      + "c.is_searchable AS is_searchable, "
      + "c.is_active AS is_active, "
      + "c.promote_on_homepage AS promote_on_homepage, "
      + "c.owner_user_id AS owner_user_id, "
      + "c.channel_type_id AS channel_type_id, "
      + "c.created_channel_type_id AS created_channel_type_id, "
      + "c.pending_channel_type_id AS pending_channel_type_id, "
      + "c.created AS created,"
      + "c.last_updated AS last_updated, "
      + "c.rating AS rating, "
      + "c.viewed_for AS viewed_for ";

  private static final String CHANNEL_TYPE_FIELDS = ""
      + "type.id AS type_id, "
      +" type.name AS type_name, "
      + "type.is_enabled AS type_is_enabled, "
      + "type.is_default AS type_is_default, "
      + "type.required_role AS type_required_role, "
      + "type.created AS type_created, "
      + "type.last_modified AS type_last_modified, "
      + "type.email_group AS type_email_group ";

  private static final String CHANNEL_CREATED_TYPE_FIELDS = ""
      + "created_type.id AS created_type_id, "
      +" created_type.name AS created_type_name, "
      + "created_type.is_enabled AS created_type_is_enabled, "
      + "created_type.is_default AS created_type_is_default, "
      + "created_type.required_role AS created_type_required_role, "
      + "created_type.created AS created_type_created, "
      + "created_type.last_modified AS created_type_last_modified, "
      + "created_type.email_group AS created_type_email_group ";

  private static final String CHANNEL_PENDING_TYPE_FIELDS = ""
      + "pending_type.id AS pending_type_id, "
      +" pending_type.name AS pending_type_name, "
      + "pending_type.is_enabled AS pending_type_is_enabled, "
      + "pending_type.is_default AS pending_type_is_default, "
      + "pending_type.required_role AS pending_type_required_role, "
      + "pending_type.created AS pending_type_created, "
      + "pending_type.last_modified AS pending_type_last_modified, "
      + "pending_type.email_group AS pending_type_email_group ";

  /**
   * SQL query to select data from channel statistics.
   */
  private static final String CHANNEL_STATISTICS_SELECT_QUERY = "SELECT "
      + "  channel_id, "
      + "  upcoming_count, "
      + "  live_count, "
      + "  recorded_count, "
      + "  subscriber_count  "
      + "FROM "
      + "  channel_statistics "
      + "WHERE "
      + "  channel_id = :channelId";

  /**
   * SQL query to select data from channel statistics for multiple channels.
   */
  private static final String CHANNELS_SELECT_QUERY = "SELECT "
      + CHANNEL_FIELDS + ", "
      + CHANNEL_TYPE_FIELDS + ", "
      + CHANNEL_CREATED_TYPE_FIELDS + ", " 
      + CHANNEL_PENDING_TYPE_FIELDS
      + "FROM "
      + "  channel c "
      + "  JOIN channel_type type ON (c.channel_type_id = type.id) "
      + "  LEFT JOIN channel_type created_type ON (c.created_channel_type_id = created_type.id) "
      + "  LEFT JOIN channel_type pending_type ON (c.pending_channel_type_id = pending_type.id) "
      + "WHERE "
      + "  c.id IN (:channelIds) "
      + "  AND c.is_active = 1 "
      + "ORDER BY FIELD(c.id, :channelIds)"
      + "LIMIT"
      + "  :offset, :size";

  /**
   * SQL query to select data from channel statistics for multiple channels.
   */
  private static final String CHANNEL_SELECT_BY_ID_QUERY = ""
      + "SELECT "
      + CHANNEL_FIELDS + ", "
      + CHANNEL_TYPE_FIELDS + ", "
      + CHANNEL_CREATED_TYPE_FIELDS + ", " 
      + CHANNEL_PENDING_TYPE_FIELDS
      + "FROM "
      + "  channel c "
      + "  JOIN channel_type type ON (c.channel_type_id = type.id) "
      + "  LEFT JOIN channel_type created_type ON (c.created_channel_type_id = created_type.id) "
      + "  LEFT JOIN channel_type pending_type ON (c.pending_channel_type_id = pending_type.id) "
      + "WHERE "
      + "  c.id = :channelId "
      + "  AND c.is_active = 1 ";

  /**
   * SQL query to select data from channel statistics for multiple channels.
   */
  private static final String CHANNELS_BY_IDS_SELECT_QUERY = "SELECT "
      + CHANNEL_FIELDS + ", "
      + CHANNEL_TYPE_FIELDS + ", "
      + CHANNEL_CREATED_TYPE_FIELDS + ", " 
      + CHANNEL_PENDING_TYPE_FIELDS
      + "FROM "
      + "  channel c "
      + "  JOIN channel_type type ON (c.channel_type_id = type.id) "
      + "  LEFT JOIN channel_type created_type ON (c.created_channel_type_id = created_type.id) "
      + "  LEFT JOIN channel_type pending_type ON (c.pending_channel_type_id = pending_type.id) "
      + "WHERE "
      + "  c.id IN (:channelIds) "
      + "  AND c.is_active = 1 "
      + "ORDER BY FIELD(c.id, :channelIds)";

  /**
   * SQL query to select data from channel statistics for multiple channels.
   */
  private static final String CHANNELS_STATISTICS_SELECT_QUERY = "SELECT "
      + "  channel_id, "
      + "  upcoming_count, "
      + "  live_count, "
      + "  recorded_count, "
      + "  subscriber_count  "
      + "FROM "
      + "  channel_statistics "
      + "WHERE "
      + "  channel_id IN (:channelIds)";

  private static final String CHANNELS_SURVEY_SELECT_QUERY = "SELECT "
      + "  target_id AS id, "
      + "  channel_id AS channel_id, "
      + "  is_active AS is_active, "
      + "  created AS created,"
      + "  last_updated AS last_updated "
      + "FROM "
      + "  asset "
      + "WHERE "
      + "  channel_id IN (:channelIds) "
      + "  AND type = 'survey' "
      + "ORDER BY "
      + "  created ASC ";

  private static final String CHANNELS_SUMMARY_FIELDS_FOR_SELECT = ""
      + "c.id AS id, "
      + "c.title AS title, "
      + "c.description AS description, "
      + "c.strapline AS strapline, "
      + "c.keywords AS keywords, "
      + "c.organisation AS organisation, "
      + "c.is_searchable AS is_searchable, "
      + "c.viewed_for AS viewed_for, "
      + "c.rating as rating, "
      + "c.created as created, "
      + "cs.upcoming_count AS upcoming_count, "
      + "cs.live_count AS live_count, "
      + "cs.recorded_count AS recorded_count, "
      + "cs.subscriber_count AS subscriber_count ";

  private static final String CHANNELS_OWNED_SELECT_QUERY = ""
      + "SELECT "
      +     CHANNELS_SUMMARY_FIELDS_FOR_SELECT
      + "FROM "
      + "  channel c "
      + "  LEFT JOIN channel_statistics cs ON (c.id = cs.channel_id) "
      + "  LEFT JOIN channel_manager cm ON (c.id = cm.channel_id) "
      + "WHERE "
      + "  (c.owner_user_id = :userId OR (cm.user_id = :userId AND cm.is_active = 1)) "
      + "  AND c.is_active = 1 "
      + "GROUP BY "
      + "  c.id "
      + "ORDER BY "
      + "  :sortBy :sortOrder "
      + "LIMIT "
      + "  :offset, :size";

  private static final String CHANNEL_UPDATE_QUERY = ""
      + "UPDATE "
      + "  channel "
      + "SET "
      + "  title = :title, "
      + "  description = :description, "
      + "  keywords = :keywords, "
      + "  organisation = :organisation, "
      + "  strapline = :strapline, "
      + "  last_updated = :last_updated, "
      + "  owner_user_id = :owner_user_id "
      + "WHERE "
      + "  id = :id";

  // CHECKSTYLE:ON
  /* @formatter:on */

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JdbcChannelDbDao.class);

  /** JDBC template used to access the database. */
  protected NamedParameterJdbcTemplate jdbcTemplate;

  private SimpleJdbcInsert simpleJdbcInsertChannel;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    simpleJdbcInsertChannel = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertChannel.withTableName("channel");
    simpleJdbcInsertChannel.usingGeneratedKeyColumns("id");
    simpleJdbcInsertChannel.usingColumns(TITLE_FIELD, DESCRIPTION_FIELD, KEYWORDS_FIELD, ORGANISATION_FIELD,
        STRAPLINE_FIELD, CHANNEL_TYPE_ID_FIELD, CREATED_CHANNEL_TYPE_ID_FIELD, PENDING_CHANNEL_TYPE_ID_FIELD,
        OWNER_USER_ID_FIELD, LAST_UPDATED_FIELD, CREATED_FIELD, IS_SEARCHABLE_FIELD, PROMOTE_ON_HOMEPAGE_FIELD,
        IS_ACTIVE, RATING_FIELD, VIEWED_FOR_FIELD);
  }

  @Override
  public boolean exists(final Long channelId) {

    if (logger.isDebugEnabled()) {
      logger.debug("Checking if channel [" + channelId + "] exists.");
    }

    // CHECKSTYLE:OFF
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT ");
    sql.append("  COUNT(*) ");
    sql.append("FROM ");
    sql.append("  channel c ");
    sql.append("WHERE ");
    sql.append("  c.id = :channelId ");
    sql.append("  AND c.is_active = 1 ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("channelId", channelId);
    // CHECKSTYLE:ON

    int numberOfChannels = queryForInt(jdbcTemplate, sql.toString(), params);
    boolean channelExists = numberOfChannels > 0;

    if (logger.isDebugEnabled()) {
      logger.debug("Channel [" + channelId + "] exists check. Result [" + channelExists + "].");
    }

    return channelExists;
  }

  /** {@inheritDoc} */
  @Override
  public boolean titleInUse(final String channelTitle) {

    if (logger.isDebugEnabled()) {
      logger.debug("Checking if channel title [" + channelTitle + "] is already in use.");
    }

    // CHECKSTYLE:OFF
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT ");
    sql.append("  COUNT(*) ");
    sql.append("FROM ");
    sql.append("  channel c ");
    sql.append("WHERE ");
    sql.append("  c.title = :channelTitle ");
    sql.append("  AND c.is_active = 1 ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channelTitle", channelTitle);

    int numberOfChannels = queryForInt(jdbcTemplate, sql.toString(), params);
    boolean channelTitleInUse = numberOfChannels > 0;

    if (logger.isDebugEnabled()) {
      logger.debug("Channel title [" + channelTitle + "] is in use check. Result [" + channelTitleInUse + "].");
    }

    return channelTitleInUse;
  }

  /** {@inheritDoc} */
  @Override
  public boolean titleInUseExcludingChannel(final String channelTitle, final Long channelId) {

    if (logger.isDebugEnabled()) {
      logger.debug("Checking if channel title [" + channelTitle + "] is already in use.");
    }

    // CHECKSTYLE:OFF
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT ");
    sql.append("  COUNT(*) ");
    sql.append("FROM ");
    sql.append("  channel c ");
    sql.append("WHERE ");
    sql.append("  c.title = :title ");
    sql.append("  AND c.is_active = 1 ");
    sql.append("  AND c.id <> :channel_id ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_FIELD, channelId);
    params.addValue(CHANNEL_TITLE_FIELD, channelTitle);

    int numberOfChannels = queryForInt(jdbcTemplate, sql.toString(), params);
    boolean channelTitleInUse = numberOfChannels > 0;

    if (logger.isDebugEnabled()) {
      logger.debug("Channel title [" + channelTitle + "] is in use check. Result [" + channelTitleInUse + "].");
    }

    return channelTitleInUse;
  }

  /** {@inheritDoc} */
  @Override
  public Channel find(final Long channelId) {

    if (logger.isDebugEnabled()) {
      logger.debug("Finding channel [" + channelId + "].");
    }

    Channel channel = null;
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channelId", channelId);

    try {
      channel = jdbcTemplate.queryForObject(CHANNEL_SELECT_BY_ID_QUERY, params, new ChannelRowMapper());
    } catch (EmptyResultDataAccessException e) {
      if (logger.isDebugEnabled()) {
        logger.debug("Channel [" + channelId + "] not found.");
      }
      throw new ChannelNotFoundException(channelId, e);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Found channel [" + channel + "].");
    }
    return channel;
  }

  @Override
  public List<Channel> find(final List<Long> channelIds) {

    if (logger.isDebugEnabled()) {
      logger.debug("Finding channel [" + channelIds + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("channelIds", channelIds);
    // CHECKSTYLE:ON

    List<Channel> channels = jdbcTemplate.query(CHANNELS_BY_IDS_SELECT_QUERY, params, new ChannelRowMapper());

    if (logger.isDebugEnabled()) {
      logger.debug("Found [" + channels.size() + "] channels.");
    }

    return channels;

  }

  @Override
  public PaginatedList<Channel> find(final ChannelsSearchCriteria criteria) {

    List<Long> channelIds = criteria.getChanelIds();

    if (logger.isDebugEnabled()) {
      logger.debug("Finding channel [" + channelIds + "].");
    }

    final int offset = (criteria.getPageNumber() - 1) * criteria.getPageSize();

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("channelIds", channelIds);

    params.addValue("offset", offset);
    // add 1 to page size to perform peek check
    params.addValue("size", criteria.getPageSize() + 1);
    // CHECKSTYLE:ON

    List<Channel> channels = jdbcTemplate.query(CHANNELS_SELECT_QUERY, params, new ChannelRowMapper());

    if (logger.isDebugEnabled()) {
      logger.debug("Found [" + channels.size() + "] channels.");
    }

    return buildPaginatedList(channels, criteria);

  }

  /** {@inheritDoc} */
  @Override
  public Channel create(Channel channel) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(TITLE_FIELD, channel.getTitle());
    params.addValue(DESCRIPTION_FIELD, channel.getDescription());
    params.addValue(KEYWORDS_FIELD, keywords.convertListOfKeywordsToKeywordsString(channel.getKeywords()));
    params.addValue(ORGANISATION_FIELD, channel.getOrganisation());
    params.addValue(STRAPLINE_FIELD, channel.getStrapline());
    params.addValue(CHANNEL_TYPE_ID_FIELD, channel.getType().getId());
    params.addValue(CREATED_CHANNEL_TYPE_ID_FIELD, channel.getCreatedType().getId());
    params.addValue(PENDING_CHANNEL_TYPE_ID_FIELD, channel.getPendingType().getId());
    params.addValue(OWNER_USER_ID_FIELD, channel.getOwnerUserId());
    params.addValue(LAST_UPDATED_FIELD, new Date());
    params.addValue(CREATED_FIELD, new Date());
    // use default db values for required db fields
    params.addValue(IS_SEARCHABLE_FIELD, Channel.Searchable.RULE_BASED);
    params.addValue(PROMOTE_ON_HOMEPAGE_FIELD, 0);
    params.addValue(IS_ACTIVE, 1);
    params.addValue(RATING_FIELD, 0);
    params.addValue(VIEWED_FOR_FIELD, 0);

    Number primaryKey = simpleJdbcInsertChannel.executeAndReturnKey(params);

    Long channelId = primaryKey.longValue();
    channel = find(channelId);

    return channel;
  }

  /** {@inheritDoc} */
  @Override
  public Channel update(final Channel channel) {

    if (logger.isDebugEnabled()) {
      logger.debug("Updating channel [" + channel.getId() + "] details.");
    }
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(ID_FIELD, channel.getId());
    params.addValue(TITLE_FIELD, channel.getTitle());
    params.addValue(DESCRIPTION_FIELD, channel.getDescription());
    params.addValue(KEYWORDS_FIELD, keywords.convertListOfKeywordsToKeywordsString(channel.getKeywords()));
    params.addValue(ORGANISATION_FIELD, channel.getOrganisation());
    params.addValue(STRAPLINE_FIELD, channel.getStrapline());
    params.addValue(OWNER_USER_ID, channel.getOwnerUserId());
    params.addValue(LAST_UPDATED_FIELD, new Date());

    int noOfRows = jdbcTemplate.update(CHANNEL_UPDATE_QUERY, params);

    if (noOfRows != 1) {
      throw new ApplicationException("Could not update channel [" + channel.getId() + "]");
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Updated channel [" + channel.getId() + "].");
    }

    return channel;
  }

  /** {@inheritDoc} */
  @Override
  public void delete(final Long channelId) {

    if (logger.isDebugEnabled()) {
      logger.debug("Deleting channel [" + channelId + "].");
    }

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  channel ");
    sql.append("SET ");
    sql.append("  is_active = 0, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  id = :channelId ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channelId", channelId);

    int noOfRows = jdbcTemplate.update(sql.toString(), params);

    if (noOfRows != 1) {
      throw new ApplicationException("Could not delete channel [" + channelId + "]");
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Deleted channel [" + channelId + "].");
    }
  }

  /** {@inheritDoc} */
  @Override
  public void loadStatistics(final Channel channel) {

    if (logger.isDebugEnabled()) {
      logger.debug("Loading statistics for channel [" + channel.getId() + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();

    params.addValue("channelId", channel.getId());

    ChannelStatistics channelStatistics;
    try {
      channelStatistics = jdbcTemplate.queryForObject(CHANNEL_STATISTICS_SELECT_QUERY, params,
          new ChannelStatisticsRowMapper());
    } catch (EmptyResultDataAccessException emptyResultException) {
      if (logger.isDebugEnabled()) {
        logger.debug("No Statistics found for Channel [" + channel.getId() + "].");
      }
      return;
    }
    if (channel.hasStatistics()) {
      channel.getStatistics().setNumberOfUpcomingCommunications(channelStatistics.getNumberOfUpcomingCommunications());
      channel.getStatistics().setNumberOfLiveCommunications(channelStatistics.getNumberOfLiveCommunications());
      channel.getStatistics().setNumberOfRecordedCommunications(channelStatistics.getNumberOfRecordedCommunications());
      channel.getStatistics().setNumberOfSubscribers(channelStatistics.getNumberOfSubscribers());
    } else {
      channel.setStatistics(channelStatistics);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Statistics loaded for channel [" + channel.getId() + "].");
    }

  }

  /** {@inheritDoc} */
  @Override
  public void loadStatistics(final List<Channel> channels) {

    List<Long> channelIds = getChannelIds(channels);

    if (logger.isDebugEnabled()) {
      logger.debug("Loading statistics for channels [" + channelIds + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();

    params.addValue("channelIds", channelIds);

    List<ChannelStatistics> allChannelStatistics = jdbcTemplate.query(CHANNELS_STATISTICS_SELECT_QUERY, params,
        new ChannelStatisticsRowMapper());

    Map<Long, ChannelStatistics> mappedStats = new HashMap<Long, ChannelStatistics>();
    for (ChannelStatistics stats : allChannelStatistics) {
      mappedStats.put(stats.getChannelId(), stats);
    }

    for (Channel channel : channels) {

      ChannelStatistics stats = mappedStats.get(channel.getId());

      if (stats != null) {
        if (channel.hasStatistics()) {
          channel.getStatistics().setNumberOfUpcomingCommunications(stats.getNumberOfUpcomingCommunications());
          channel.getStatistics().setNumberOfLiveCommunications(stats.getNumberOfLiveCommunications());
          channel.getStatistics().setNumberOfRecordedCommunications(stats.getNumberOfRecordedCommunications());
          channel.getStatistics().setNumberOfSubscribers(stats.getNumberOfSubscribers());
        } else {
          channel.setStatistics(stats);
        }
      }
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Statistics loaded for channels [" + channelIds + "].");
    }

  }

  /** {@inheritDoc} */
  @Override
  public void loadSurvey(final Channel channel) {

    if (logger.isDebugEnabled()) {
      logger.debug("Loading survey for channel [" + channel.getId() + "].");
    }

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  target_id AS id, ");
    sql.append("  channel_id AS channel_id, ");
    sql.append("  is_active AS is_active, ");
    sql.append("  created AS created,");
    sql.append("  last_updated AS last_updated ");
    sql.append("FROM ");
    sql.append("  asset ");
    sql.append("WHERE ");
    sql.append("  channel_id = :channelId ");
    sql.append("  AND type = 'survey' ");
    sql.append("  ORDER BY created ASC ");
    // CHECKSTYLE:ON

    // CHECKSTYLE:OFF
    params.addValue("channelId", channel.getId());
    // CHECKSTYLE:ON
    try {
      Survey survey = jdbcTemplate.queryForObject(sql.toString(), params, new SurveyRowMapper());
      channel.setSurvey(survey);

    } catch (EmptyResultDataAccessException e) {

      if (logger.isDebugEnabled()) {
        logger.debug("No Survey found for Channel [" + channel.getId() + "].");
      }
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Survey loaded for channel [" + channel.getId() + "]. Survey [" + channel.getSurvey() + "]");
    }

  }

  @Override
  public void loadSurvey(final List<Channel> channels) {

    Map<Long, Channel> mappedChannels = getMappedChannels(channels);
    Set<Long> channelIds = mappedChannels.keySet();

    if (logger.isDebugEnabled()) {
      logger.debug("Loading survey for channels [" + channelIds + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    params.addValue("channelIds", channelIds);
    // CHECKSTYLE:ON

    List<Survey> surveys = jdbcTemplate.query(CHANNELS_SURVEY_SELECT_QUERY, params, new SurveyRowMapper());

    for (Survey survey : surveys) {
      mappedChannels.get(survey.getChannelId()).setSurvey(survey);
    }
    if (logger.isDebugEnabled()) {
      logger.debug("Survey loaded for channels [" + channelIds + "].  [" + surveys.size() + "] surveys.");
    }
  }

  /** {@inheritDoc} */
  @Override
  public List<Channel> findSubscribedChannels(final Long userId) {

    if (logger.isDebugEnabled()) {
      logger.debug("Finding subscribed channels for user [" + userId + "].");
    }

    StringBuilder sql = new StringBuilder();
    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  c.id AS id, ");
    sql.append("  c.title AS title, ");
    sql.append("  c.keywords AS keywords, ");
    sql.append("  c.organisation AS organisation, ");
    sql.append("  c.strapline AS strapline, ");
    sql.append("  c.owner_user_id AS owner_user_id ");
    sql.append("FROM ");
    sql.append("  channel c ");
    sql.append("  LEFT JOIN subscription s ON (c.id = s.channel_id) ");
    sql.append("WHERE ");
    sql.append("  s.user_id = :userId ");
    sql.append(" AND c.is_active = 1");
    sql.append(" AND s.is_active = 1");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    params.addValue("userId", userId);
    // CHECKSTYLE:ON

    List<Channel> subscribedChannels = jdbcTemplate.query(sql.toString(), params, new ChannelsSubscribedRowMapper());

    if (logger.isDebugEnabled()) {
      logger.debug("Found [ " + subscribedChannels.size() + "] subscribed channels for user [" + userId + "].");
    }

    return subscribedChannels;
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<Channel> findPaginatedChannelsSubscribed(final Long userId,
    final MyChannelsSearchCriteria criteria) {

    if (logger.isDebugEnabled()) {
      logger.debug("Finding subscribed channels for user [" + userId + "].");
    }

    List<Channel> subscribedChannels = loadSubscribedChannelsSummaries(userId, criteria);

    return buildPaginatedList(subscribedChannels, criteria);
  }

  /**
   * Load a collection of subscribed channels' summaries for a given user using given search criteria.
   * 
   * @param userId the user id
   * @param criteria the search criteria
   * 
   * @return a collection of channels
   */
  private List<Channel> loadSubscribedChannelsSummaries(final Long userId, final MyChannelsSearchCriteria criteria) {

    StringBuilder sql = new StringBuilder();
    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(CHANNELS_SUMMARY_FIELDS_FOR_SELECT);
    sql.append("FROM ");
    sql.append("  subscription s ");
    sql.append("  JOIN channel c ON (c.id = s.channel_id AND c.is_active = 1) ");
    sql.append("  LEFT JOIN channel_statistics cs ON (c.id = cs.channel_id)");
    sql.append("WHERE ");
    sql.append(" s.user_id = :userId");
    sql.append(" AND s.is_active = 1 ");
    sql.append("GROUP BY");
    sql.append(" c.id ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("userId", userId);

    addOrderAndLimitClauses(sql, criteria, params);

    List<Channel> subscribedChannels = jdbcTemplate.query(sql.toString(), params, new ChannelSummaryRowMapper());

    return subscribedChannels;
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<Channel> findPaginatedChannelsOwned(final Long userId,
    final MyChannelsSearchCriteria searchCriteria) {

    if (logger.isDebugEnabled()) {
      logger.debug("Finding channels owned by user [" + userId + "].");
    }

    List<Channel> ownedChannels = loadOwnedChannelsSummaries(userId, searchCriteria);

    return buildPaginatedList(ownedChannels, searchCriteria);
  }

  /**
   * Load owned channels summaries for a given user and search criteria.
   * 
   * @param userId user id
   * @param criteria search criteria
   * 
   * @return a collection of channels
   */
  private List<Channel> loadOwnedChannelsSummaries(final Long userId, final MyChannelsSearchCriteria criteria) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("userId", userId);
    params.addValue("offset", (criteria.getPageNumber() - 1) * criteria.getPageSize());
    params.addValue("size", criteria.getPageSize() + 1);

    String sql = CHANNELS_OWNED_SELECT_QUERY;
    sql = sql.replace(":sortBy", criteria.getSortBy().getValue());
    sql = sql.replace(":sortOrder", criteria.getSortOrder().name());

    List<Channel> ownedChannels = jdbcTemplate.query(sql, params, new ChannelSummaryRowMapper());

    checkForEmptyEntityCase(ownedChannels);

    return ownedChannels;
  }

  /**
   * Add order and limit clauses to sql builder and sql params for given search criteria.
   * 
   * @param sql sql builder
   * @param criteria search criteria
   * @param params sql params
   */
  private void addOrderAndLimitClauses(final StringBuilder sql, final MyChannelsSearchCriteria criteria,
    final MapSqlParameterSource params) {

    if (criteria.hasSortBy()) {
      addOrderByClause(sql, criteria, params);
    }

    sql.append("LIMIT ");
    sql.append("  :offset, :size");

    final int offset = (criteria.getPageNumber() - 1) * criteria.getPageSize();
    params.addValue("offset", offset);
    // add 1 to page size to perform peek check
    params.addValue("size", criteria.getPageSize() + 1);
  }

  /**
   * Add order by clause depending on sort by setting of search criteria.
   * 
   * @param sql sql builder
   * @param criteria saerch criteria
   * @param params sql params
   */
  private void addOrderByClause(final StringBuilder sql, final MyChannelsSearchCriteria criteria,
    final MapSqlParameterSource params) {

    String orderByValue = null;

    switch (criteria.getSortBy()) {

      case CREATEDDATE:
        orderByValue = CREATED_FIELD;
        break;

      case AVERAGERATING:
        orderByValue = RATING_FIELD;
        break;

      case UPCOMINGWEBCASTS:
        orderByValue = UPCOMING_COUNT_FIELD;
        break;

      case RECORDEDWEBCASTS:
        orderByValue = RECORDED_COUNT_FIELD;
        break;

      case SUBSCRIBERS:
        orderByValue = SUBSCRIBER_COUNT_FIELD;
        break;

      default:
        throw new ApplicationException("Failed to add order by clause. Value [" + criteria.getSortBy()
            + "] not supported.");
    }
    sql.append("ORDER BY ");
    sql.append(orderByValue);
    sql.append(SPACE).append(criteria.getSortOrder().toString());
    sql.append(SPACE);
  }

  /**
   * This is a work-around that allows to keep production tested sql query the processing of which worked slightly
   * differently when in used to be handled by PHP.
   * 
   * Because of the complicated nature of the find owned and subscribed queries, in the case of <b>no results</b> found,
   * the query still returns one row with null values. Php didn't have automatic row mapping, hence it was manually
   * covering this scenario.
   * 
   * This method handles that by removing this extra row, in this one, special scenario.
   * 
   * TODO: A correct fix would be to revisit the query so that it does not return that redundant row.
   * 
   * @param channels a collection to be verified
   */
  private void checkForEmptyEntityCase(final List<Channel> channels) {

    if (!CollectionUtils.isEmpty(channels) && channels.size() == 1) {

      Channel channel = channels.get(0);
      if (channel.getId() == 0) {
        channels.remove(channel);
      }
    }
  }

  /**
   * A helper method to build a channel search result wrapper.
   * 
   * Does a check on the peek result.
   * 
   * @param channels a collection of channels
   * @param criteria search criteria
   * 
   * @return result object
   */
  private PaginatedList<Channel> buildPaginatedList(final List<Channel> channels, final BaseSearchCriteria criteria) {

    boolean isLastPage = true;

    if (!CollectionUtils.isEmpty(channels) && channels.size() > criteria.getPageSize()) {
      isLastPage = false;

      // Remove extra channel added to support peeking for next page
      channels.remove(channels.size() - 1);
    }

    PaginatedList<Channel> result = new PaginatedList<Channel>();

    result.addAll(channels);
    result.setIsFinalPage(isLastPage);
    result.setCurrentPageNumber(criteria.getPageNumber());
    result.setPageSize(criteria.getPageSize());

    return result;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isPresenter(final User user, final Channel channel) {

    if (logger.isDebugEnabled()) {
      logger.debug("Checking if user [" + user.getId() + "] is a presenter on channel [" + channel.getId() + "]");
    }

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  COUNT(*) ");
    sql.append("FROM ");
    sql.append("  asset a ");
    sql.append("  LEFT JOIN can_present cp ON (");
    sql.append("    cp.communication_id = a.target_id ");
    sql.append("    AND a.type = 'communication' ");
    sql.append("    AND a.is_active = 1 ");
    sql.append("    AND cp.is_active = 1) ");
    sql.append("WHERE ");
    sql.append("  a.channel_id = :channelId ");
    sql.append("  AND cp.session_id = :sessionId ");
    // CHECKSTYLE:ON

    // CHECKSTYLE:OFF
    params.addValue("channelId", channel.getId());
    params.addValue("sessionId", user.getSession().getId());
    // CHECKSTYLE:ON

    Integer presenterCount = queryForInt(jdbcTemplate, sql.toString(), params);

    boolean userCanPresent = presenterCount > 0;

    if (logger.isDebugEnabled()) {
      logger.debug("User [" + user.getId() + "] is a presenter on channel [" + channel.getId() + "]. Result ["
          + userCanPresent + "].");
    }

    return userCanPresent;
  }

  /**
   * Helper method to extract channel ids form a list of channels.
   * 
   * @param channels The list of channels
   * 
   * @return The list of channel ids
   */
  private List<Long> getChannelIds(final List<Channel> channels) {

    List<Long> channelIds = new ArrayList<Long>();

    for (Channel channel : channels) {
      channelIds.add(channel.getId());
    }
    return channelIds;
  }

  /**
   * Helper method to map channels by id.
   * 
   * @param channels The list of channels
   * 
   * @return The Map of channels keyed by Id
   */
  private Map<Long, Channel> getMappedChannels(final List<Channel> channels) {

    Map<Long, Channel> mappedChannels = new HashMap<Long, Channel>();

    for (Channel channel : channels) {
      mappedChannels.put(channel.getId(), channel);
    }
    return mappedChannels;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Channel}.
   */
  private class ChannelRowMapper implements RowMapper<Channel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Channel mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Channel channel = new Channel(resultSet.getLong(ID_FIELD));
      channel.setTitle(resultSet.getString(TITLE_FIELD));
      channel.setKeywords(keywords.convertKeywordsStringToListOfKeywords(resultSet.getString(KEYWORDS_FIELD)));
      channel.setOrganisation(resultSet.getString(ORGANISATION_FIELD));
      channel.setDescription(resultSet.getString(DESCRIPTION_FIELD));
      channel.setStrapline(resultSet.getString(STRAPLINE_FIELD));
      channel.setOwnerUserId(resultSet.getLong(OWNER_USER_ID_FIELD));

      ChannelStatistics statistics = new ChannelStatistics(channel.getId());

      statistics.setRating(resultSet.getFloat(RATING_FIELD));
      statistics.setViewedFor(resultSet.getLong(VIEWED_FOR_FIELD));

      channel.setStatistics(statistics);

      channel.setActive(resultSet.getBoolean(IS_ACTIVE));
      channel.setSearchable(Searchable.valueOf(resultSet.getString(IS_SEARCHABLE_FIELD).toUpperCase()));
      channel.setPromotedOnHomepage(resultSet.getBoolean(PROMOTE_ON_HOMEPAGE_FIELD));

      channel.setType(mapChannelType(resultSet, CHANNEL_TYPE_FIELD));
      channel.setCreatedType(mapChannelType(resultSet, CHANNEL_TYPE_CREATED_FIELD));
      channel.setPendingType(mapChannelType(resultSet, CHANNEL_TYPE_PENDING_FIELD));

      channel.setCreated(resultSet.getTimestamp(CREATED_FIELD));
      channel.setLastUpdated(resultSet.getTimestamp(LAST_UPDATED_FIELD));
      return channel;
    }

    private ChannelType mapChannelType(final ResultSet resultSet, final String key) throws SQLException {

      ChannelType type = null;
      String typeId = resultSet.getString(key + "_id");

      if (typeId != null) {

        type = new ChannelType(resultSet.getLong(key + "_id"));
        type.setName(resultSet.getString(key + "_name"));
        type.setIsEnabled(resultSet.getBoolean(key + "_is_enabled"));
        type.setIsDefault(resultSet.getBoolean(key + "_is_default"));
        type.setRequiredRole(Role.valueOf(resultSet.getString(key + "_required_role").toUpperCase()));
        type.setEmailGroup(resultSet.getString(key + "_email_group"));

        // note that last modified in database os mapped to last modified in business object
        type.setCreated(resultSet.getTimestamp(key + "_created"));
        type.setLastUpdated(resultSet.getTimestamp(key + "_last_modified"));
      }

      return type;
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Channel} including channel's summary
   * information.
   */
  private class ChannelSummaryRowMapper implements RowMapper<Channel> {

    @Override
    public Channel mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Channel channel = new Channel(resultSet.getLong(ID_FIELD));
      channel.setTitle(resultSet.getString(TITLE_FIELD));
      channel.setKeywords(keywords.convertKeywordsStringToListOfKeywords(resultSet.getString(KEYWORDS_FIELD)));
      channel.setDescription(resultSet.getString(DESCRIPTION_FIELD));
      channel.setStrapline(resultSet.getString(STRAPLINE_FIELD));
      channel.setOrganisation(resultSet.getString(ORGANISATION_FIELD));
      channel.setSearchable(Searchable.valueOf(resultSet.getString(IS_SEARCHABLE_FIELD).toUpperCase()));

      ChannelStatistics statistics = new ChannelStatistics(channel.getId());

      statistics.setViewedFor(resultSet.getLong(VIEWED_FOR_FIELD));
      statistics.setRating(resultSet.getFloat(RATING_FIELD));
      statistics.setNumberOfUpcomingCommunications(resultSet.getLong(UPCOMING_COUNT_FIELD));
      statistics.setNumberOfRecordedCommunications(resultSet.getLong(RECORDED_COUNT_FIELD));
      statistics.setNumberOfLiveCommunications(resultSet.getLong(LIVE_COUNT_FIELD));
      statistics.setNumberOfSubscribers(resultSet.getLong(SUBSCRIBER_COUNT_FIELD));

      channel.setStatistics(statistics);

      return channel;
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Channel} for subscribed channels.
   */
  private class ChannelsSubscribedRowMapper implements RowMapper<Channel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Channel mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Channel channel = new Channel(resultSet.getLong(ID_FIELD));
      channel.setTitle(resultSet.getString(TITLE_FIELD));
      channel.setKeywords(keywords.convertKeywordsStringToListOfKeywords(resultSet.getString(KEYWORDS_FIELD)));
      channel.setOrganisation(resultSet.getString(ORGANISATION_FIELD));
      channel.setStrapline(resultSet.getString(STRAPLINE_FIELD));
      channel.setOwnerUserId(resultSet.getLong(OWNER_USER_ID_FIELD));

      return channel;
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Survey} for channel survey.
   */
  private static class SurveyRowMapper implements RowMapper<Survey> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Survey mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Survey survey = new Survey(resultSet.getLong(ID_FIELD), resultSet.getLong(CHANNEL_ID_FIELD),
          resultSet.getBoolean(IS_ACTIVE));
      survey.setCreated(resultSet.getTimestamp(CREATED_FIELD));
      survey.setLastUpdated(resultSet.getTimestamp(LAST_UPDATED_FIELD));

      return survey;
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link ChannelStatistics} for channel
   * statistics.
   */
  private static class ChannelStatisticsRowMapper implements RowMapper<ChannelStatistics> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ChannelStatistics mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      ChannelStatistics channelStatistics = new ChannelStatistics(resultSet.getLong(CHANNEL_ID_FIELD));
      channelStatistics.setNumberOfUpcomingCommunications(resultSet.getLong(UPCOMING_COUNT_FIELD));
      channelStatistics.setNumberOfLiveCommunications(resultSet.getLong(LIVE_COUNT_FIELD));
      channelStatistics.setNumberOfRecordedCommunications(resultSet.getLong(RECORDED_COUNT_FIELD));
      channelStatistics.setNumberOfSubscribers(resultSet.getLong(SUBSCRIBER_COUNT_FIELD));

      return channelStatistics;
    }
  }

}