/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SurveyServiceDao.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.survey;

import com.brighttalk.common.form.domain.Form;

/**
 * Interface to define the required methods for the interactions with the Survey Service.
 */
public interface SurveyServiceDao {

  /**
   * Gets survey form from survey service.
   * 
   * @param surveyId the id of the survey to be retrieved.
   * 
   * @return {@link Form}
   */
  Form getSurveyForm(Long surveyId);
}