/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: HDBookingDto.java 83838 2014-09-24 11:10:48Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking.dto;

/**
 * BookingDto.
 */
public class HDBookingDto {

  private String start;
  private Integer duration;
  private String timeZone;

  /**
   * @return the start
   */
  public String getStart() {
    return start;
  }

  /**
   * @param start the start to set
   */
  public void setStart(final String start) {
    this.start = start;
  }

  /**
   * @return the duration
   */
  public Integer getDuration() {
    return duration;
  }

  /**
   * @param duration the duration to set
   */
  public void setDuration(final Integer duration) {
    this.duration = duration;
  }

  /**
   * @return the timeZone
   */
  public String getTimeZone() {
    return timeZone;
  }

  /**
   * @param timeZone the timeZone to set
   */
  public void setTimeZone(final String timeZone) {
    this.timeZone = timeZone;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("start: [").append(start).append("] ");
    builder.append("duration: [").append(duration).append("] ");
    builder.append("timeZone: [").append(timeZone).append("] ");

    return builder.toString();
  }
}
