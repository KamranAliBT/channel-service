/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationConfigurationDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;

/**
 * DAO for database operations on a {@link CommunicationConfiguration} instance.
 */
public interface CommunicationConfigurationDbDao {

  /**
   * Creates a communication configuration.
   * 
   * @param config The communication configuration to be created.
   */
  void create(CommunicationConfiguration config);

  /**
   * Updates a communication configuration.
   * 
   * @param config The communication configuration to be updated.
   */
  void update(CommunicationConfiguration config);

  /**
   * Get the communication configuration for the given channelId and commnuicationId.
   * 
   * @param channelId of the channel in which the communication lives.
   * @param communicationId of the communication.
   * @return the communicationConfiguration object
   */
  CommunicationConfiguration get(Long channelId, Long communicationId);
}
