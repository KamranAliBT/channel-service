/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: package-info.java 97400 2015-07-01 08:36:42Z ssarraj $
 * ****************************************************************************
 */
/**
 * Containing list of AuditRecordDto Builders.
 */
package com.brighttalk.service.channel.integration.audit.dto.builder;