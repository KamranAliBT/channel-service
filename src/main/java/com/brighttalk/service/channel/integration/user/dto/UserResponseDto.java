/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: UserResponseDto.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.user.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.brighttalk.common.user.integration.dto.UserDto;

/**
 * Represents a user search of BrightTALK.
 */
@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserResponseDto {

  private UserDto user;

  @XmlAttribute
  private String state;

  /**
   * Default constructor.
   */
  public UserResponseDto() {

  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(final UserDto user) {
    this.user = user;
  }

  /**
   * Returns the response status from the API call.
   * 
   * @return <code>processed</code> if success or <code>failed</code> otherwise.
   */
  public String getState() {
    return state;
  }

  /**
   * Set the response status from the API call.
   * 
   * @param state Response status.
   */
  public void setState(final String state) {
    this.state = state;
  }

}
