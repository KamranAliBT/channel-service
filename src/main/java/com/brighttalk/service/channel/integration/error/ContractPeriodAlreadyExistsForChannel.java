/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ContractPeriodAlreadyExistsForChannel.java 101449 2015-10-13 12:37:44Z kali $$
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.error;

import com.brighttalk.common.error.ApplicationException;

/**
 * Thrown when an attempt is made to insert a duplicate contract period for a channel
 */
public class ContractPeriodAlreadyExistsForChannel extends ApplicationException {

  /** Invalid contract period error code */
  private static final String ERROR_CODE_INVALID_CONTRACT_PERIOD = "InvalidContractPeriod";

  /**
   * @param message see below.
   * @param cause see below.
   * @see ApplicationException#ApplicationException(String, Throwable)
   */
  public ContractPeriodAlreadyExistsForChannel(final String message, final Throwable cause) {
    super(message, cause);
  }

  /**
   * @param message see below.
   * @param errorCode see below.
   * @see ApplicationException#ApplicationException(String, String)
   */
  public ContractPeriodAlreadyExistsForChannel(final String message, final String errorCode) {
    super(message, ERROR_CODE_INVALID_CONTRACT_PERIOD);
    this.setUserErrorMessage(errorCode);
  }

  /**
   * @param message the detail message, for internal consumption.
   *
   * @see ApplicationException#ApplicationException(String)
   */
  public ContractPeriodAlreadyExistsForChannel(final String message) {
    super(message, ERROR_CODE_INVALID_CONTRACT_PERIOD);
  }

  @Override
  public String getUserErrorCode() {
    return ERROR_CODE_INVALID_CONTRACT_PERIOD;
  }

}
