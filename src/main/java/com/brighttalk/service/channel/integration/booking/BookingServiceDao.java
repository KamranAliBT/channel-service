/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: BookingServiceDao.java 83838 2014-09-24 11:10:48Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking;

import java.util.List;

/**
 * Interface to define the required methods for the interactions with the Booking Service. This is not used yet because
 * we did not complete the migration of the create channel code from php to java. The delete bookings method would be
 * used when a channel is deleted.
 */
public interface BookingServiceDao {

  /**
   * Delete Multiple Bookings.
   * 
   * @param communicationIds a collection of communication bookings of which are to be deleted.
   */
  void deleteBookings(List<Long> communicationIds);

}