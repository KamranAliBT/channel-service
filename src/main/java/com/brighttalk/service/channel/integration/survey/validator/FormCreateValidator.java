/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: FormCreateValidator.java 62868 2013-04-09 09:13:50Z aaugustyn $ 
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.survey.validator;

import org.springframework.stereotype.Component;

import com.brighttalk.common.form.dto.FormDto;
import com.brighttalk.common.validator.Validator;
import com.brighttalk.service.channel.business.error.SurveyNotFoundException;

/**
 * Syntatical Integration Layer Validator to validate a Form for creation.
 */
@Component
public class FormCreateValidator implements Validator<FormDto> {

  /**
   * Validate a form dto before for creation of form business object.
   * 
   * formItems are mandatory.
   * 
   * @param formDto to be validated.
   * 
   * @throws SurveyNotFoundException if the form does not contain any items.
   */
  @Override
  public void validate(final FormDto formDto) {

    if (!formDto.hasFormItems() || formDto.getFormItems().isEmpty()) {
      throw new SurveyNotFoundException("Survey [" + formDto.getId() + "] could not be found.");
    }
  }

}
