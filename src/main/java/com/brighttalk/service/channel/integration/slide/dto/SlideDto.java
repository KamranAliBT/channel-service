/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SlideDto.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.slide.dto;

/**
 * SlideDto. Empty as it is just a place holder that is currently not being used, but needs to be created so that the
 * wrapper element is inserted into the xml.
 */
public class SlideDto {
}