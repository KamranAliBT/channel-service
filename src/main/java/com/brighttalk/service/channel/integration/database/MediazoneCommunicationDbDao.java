/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDbDao.java 88525 2015-01-20 12:15:30Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import com.brighttalk.service.channel.business.domain.MediazoneConfig;

/**
 * DAO for database operations on a {@link MediazoneConfig} instance.
 */
public interface MediazoneCommunicationDbDao {

  /**
   * Load the mediazone configuration a communication.
   * 
   * @param communicationId The communication to load the config for.
   * @return the mediazone config.
   */
  MediazoneConfig findMediazoneConfig(Long communicationId);
}
