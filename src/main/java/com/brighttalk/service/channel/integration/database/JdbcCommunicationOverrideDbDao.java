/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationOverrideDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationOverride;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link CommunicationOverrideDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database.
 */
@Repository
@Primary
public class JdbcCommunicationOverrideDbDao extends JdbcDao implements CommunicationOverrideDbDao {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JdbcCommunicationOverrideDbDao.class);

  private static final String CHANNEL_ID_SQL_PARAMETER = "channelId";
  private static final String COMMUNICATION_ID_SQL_PARAMETER = "id";
  private static final String COMMUNICATION_IDS_SQL_PARAMETER = "ids";
  private static final String TITLE_SQL_PARAMETER = "title";
  private static final String DESCRIPTION_SQL_PARAMETER = "description";
  private static final String KEYWORDS_SQL_PARAMETER = "keywords";
  private static final String PRESENTER_SQL_PARAMETER = "presenter";
  private static final String CREATED_SQL_PARAMETER = "created";
  private static final String LAST_UPDATED_SQL_PARAMETER = "lastUpdated";

  // CHECKSTYLE:OFF
  // @formatter:off
  /**
   * select the communication details override query.
   */
  private static final String COMMUNICATION_DETAILS_OVERRIDE_SELECT_QUERY = ""
    + "SELECT " 
    + " communication_id, "
    + " title, "
    + " description, "
    + " presenter as author, "
    + " keywords "
    + "FROM "
    + " communication_details_override "
    + "WHERE "
    + " communication_id = :id "
    + "AND "
    + " channel_id = :channelId"; 
  // @formatter:on  
  // CHECKSTYLE:ON

  // CHECKSTYLE:OFF
  // @formatter:off
  /**
   * insert or update communication details override query.
   */
  private static final String COMMUNICATION_DETAILS_OVERRIDE_INSERT_OR_UPDATE_QUERY = ""
    + "INSERT INTO communication_details_override SET " 
    + " channel_id = :channelId, "
    + " communication_id = :id, "
    + " title = :title, "
    + " description = :description, "
    + " presenter = :presenter, "
    + " keywords = :keywords, "
    + " created = :created, "
    + " last_updated = :lastUpdated "
    + "ON DUPLICATE KEY UPDATE "
    + " title = VALUES(title),"
    + " description = VALUES (description), "
    + " presenter = VALUES (presenter),"
    + " keywords = VALUES (keywords), "
    + " last_updated = :lastUpdated"; 
  // @formatter:on  
  // CHECKSTYLE:ON

  /** JDBC template used to access the database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Set the datasource on this DAO.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  @Override
  public void loadOverrides(final Long channelId, final List<Communication> syndicatedCommunications) {

    // if we have no channel id, or no communications, just returns before hitting the DB.
    if (channelId == null || syndicatedCommunications == null || syndicatedCommunications.size() == 0) {
      return;
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Adding communication overrides for channelId [" + channelId + "].");
    }

    List<Long> communicationIds = super.getCommunicationIds(syndicatedCommunications);

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(" co.communication_id as communication_id,");
    sql.append(" co.title as title,");
    sql.append(" co.presenter as author,");
    sql.append(" co.description as description,");
    sql.append(" co.keywords as keywords ");
    sql.append("FROM ");
    sql.append("  communication_details_override co ");
    sql.append("WHERE ");
    sql.append("  channel_id = :channelId ");
    sql.append("  AND communication_id IN ( :ids )");

    params.addValue(CHANNEL_ID_SQL_PARAMETER, channelId);
    params.addValue(COMMUNICATION_IDS_SQL_PARAMETER, communicationIds);
    // CHECKSTYLE:ON

    List<CommunicationOverride> communicationOverrides = null;

    // TODO - query for list
    try {
      communicationOverrides = jdbcTemplate.query(sql.toString(), params, new CommunicationOverrideRowMapper());
    } catch (EmptyResultDataAccessException e) {
      throw new NotFoundException("Problems getting communication overrides for channel " + "[" + channelId
          + "] and communications [" + communicationIds + "]", e);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Found communication overrides for channelId [" + channelId + "]. Overrides ["
          + communicationOverrides + "].");
    }

    addOverrides(syndicatedCommunications, communicationOverrides);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void updateCommunicationDetailsOverride(final Communication communication) {
    logger.debug("Updating communication details override.");

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue(CHANNEL_ID_SQL_PARAMETER, communication.getChannelId());
    params.addValue(COMMUNICATION_ID_SQL_PARAMETER, communication.getId());
    params.addValue(TITLE_SQL_PARAMETER, communication.getTitle());
    params.addValue(DESCRIPTION_SQL_PARAMETER, communication.getDescription());
    params.addValue(PRESENTER_SQL_PARAMETER, communication.getAuthors());
    params.addValue(KEYWORDS_SQL_PARAMETER, communication.getKeywords());
    params.addValue(CREATED_SQL_PARAMETER, new Date());
    params.addValue(LAST_UPDATED_SQL_PARAMETER, new Date());
    // CHECKSTYLE:ON

    jdbcTemplate.update(COMMUNICATION_DETAILS_OVERRIDE_INSERT_OR_UPDATE_QUERY, params);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setCommunicationDetailsOverride(final Communication communication) {
    logger.debug("Loading the communication details override.");

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue(CHANNEL_ID_SQL_PARAMETER, communication.getChannelId());
    params.addValue(COMMUNICATION_ID_SQL_PARAMETER, communication.getId());
    // CHECKSTYLE:ON

    try {
      CommunicationOverride overridenCommunication = jdbcTemplate.queryForObject(
          COMMUNICATION_DETAILS_OVERRIDE_SELECT_QUERY, params, new CommunicationOverrideRowMapper());

      if (overridenCommunication.hasTitle()) {
        communication.setTitle(overridenCommunication.getTitle());
      }
      if (overridenCommunication.hasDescription()) {
        communication.setDescription(overridenCommunication.getDescription());
      }
      if (overridenCommunication.hasAuthors()) {
        communication.setAuthors(overridenCommunication.getAuthors());
      }
      if (overridenCommunication.hasKeywords()) {
        communication.setKeywords(overridenCommunication.getKeywords());
      }
    } catch (EmptyResultDataAccessException erdae) {
      return;
    }

  }

  /**
   * Add the given categories to the given communications.
   * 
   * @param communications the communications to add the overrides to.
   * @param communicationOverrides the overrides to add.
   */
  private void addOverrides(final List<Communication> communications,
    final List<CommunicationOverride> communicationOverrides) {

    for (CommunicationOverride override : communicationOverrides) {
      for (Communication communication : communications) {
        if (communication.getId().equals(override.getId())) {
          communication.merge(override);
        }
      }
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Overrides added");
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link CommunicationOverride}.
   */
  private static class CommunicationOverrideRowMapper implements RowMapper<CommunicationOverride> {

    /**
     * {@inheritDoc}
     */
    @Override
    public CommunicationOverride mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      return new CommunicationOverride(resultSet.getLong("communication_id"), resultSet.getString("title"),
          resultSet.getString("description"), resultSet.getString("author"), resultSet.getString("keywords"));
    }
  } // ComunicationOverrideRowMapper
} // Dao
