/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the integration Dto classes to communicate with the Survey service
 */
package com.brighttalk.service.channel.integration.survey.dto;