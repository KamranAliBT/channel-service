/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: package-info.java 101522 2015-10-15 16:58:46Z kali $
 * ****************************************************************************
 */
/**
 * This package contains the exceptions to be thrown from the integration layer.
 */
package com.brighttalk.service.channel.integration.error;