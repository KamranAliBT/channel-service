/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DialInNumberConverter.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking.dto.converter;

import com.brighttalk.service.channel.integration.booking.dto.LivePhoneNumberDto;

/**
 * Interface for Live Phone Number converters - used to convert collections of D Objects to Booking Request Dto objects.
 */
public interface LivePhoneNumberConverter {

  /**
   * Convert the given livePhoneNumberDto to a string.
   * 
   * @param livePhoneNumberDto dto to be converted into a string containing the live phone number
   * 
   * @return DTO object
   */
  String convert(LivePhoneNumberDto livePhoneNumberDto);
}