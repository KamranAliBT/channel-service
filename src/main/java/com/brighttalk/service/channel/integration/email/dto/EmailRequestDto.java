/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EmailRequestDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.email.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * EmailRequestDto as per API definition.
 */
@XmlRootElement(name = "request")
public class EmailRequestDto {

  private EmailDto email;

  public EmailDto getEmail() {
    return email;
  }

  public void setEmail(EmailDto email) {
    this.email = email;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("email: [").append(email).append("] ");

    return builder.toString();
  }
}