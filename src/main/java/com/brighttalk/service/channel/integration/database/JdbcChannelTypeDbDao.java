/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcChannelTypeDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.channel.ChannelType;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link ChannelTypeDbDao} which uses JDBC via Spring's SimpleJDBCTemplate
 * to access the Database.
 */
@Repository
@Primary
public class JdbcChannelTypeDbDao implements ChannelTypeDbDao {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JdbcChannelTypeDbDao.class);

  private static final String ID_FIELD = "id";

  private static final String NAME_FIELD = "name";

  private static final String EMAIL_GROUP_FIELD = "email_group";

  private static final Integer DEFAUL_IS_ENABLED_VALUE = 1;

  private static final Integer DEFAUL_IS_ACTIVE_VALUE = 1;

  private static final Integer IS_DEFAUL_VALUE = 1;

  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public ChannelType findDefault() {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  id AS id, ");
    sql.append("  name AS name, ");
    sql.append("  email_group AS email_group ");
    sql.append("FROM ");
    sql.append("  channel_type ");
    sql.append("WHERE ");
    sql.append("  is_active = :is_active ");
    sql.append("  AND is_enabled = :is_enabled ");
    sql.append("  AND is_default = :is_default ");
    sql.append("LIMIT 1");

    // CHECKSTYLE:ON

    ChannelType channelType = null;
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("is_active", DEFAUL_IS_ACTIVE_VALUE);
    params.addValue("is_enabled", DEFAUL_IS_ENABLED_VALUE);
    params.addValue("is_default", IS_DEFAUL_VALUE);

    try {

      channelType = jdbcTemplate.queryForObject(sql.toString(), params, new ChannelTypeRowMapper());

    } catch (EmptyResultDataAccessException exception) {

      final String errorMessage = "Unable to find a default channel type.";
      logger.error(errorMessage);
      throw new ApplicationException(errorMessage);
    }

    return channelType;
  }

  /** {@inheritDoc} */
  @Override
  public ChannelType find(String name) {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  id AS id, ");
    sql.append("  name AS name, ");
    sql.append("  email_group AS email_group ");
    sql.append("FROM ");
    sql.append("  channel_type ");
    sql.append("WHERE ");
    sql.append("  name = :name ");
    sql.append("  AND is_active = 1 ");
    sql.append("  AND is_enabled = 1 ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(NAME_FIELD, name);

    ChannelType channelType = null;
    try {

      channelType = jdbcTemplate.queryForObject(sql.toString(), params, new ChannelTypeRowMapper());

    } catch (EmptyResultDataAccessException exception) {

      throw new NotFoundException("Channel type [" + name + "] not found. ", exception);
    }

    return channelType;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Channel}.
   */
  private class ChannelTypeRowMapper implements RowMapper<ChannelType> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ChannelType mapRow(ResultSet resultSet, int rowNum) throws SQLException {

      ChannelType channelType = new ChannelType(resultSet.getLong(ID_FIELD));
      channelType.setName(resultSet.getString(NAME_FIELD));
      channelType.setEmailGroup(resultSet.getString(EMAIL_GROUP_FIELD));

      return channelType;
    }
  }
}