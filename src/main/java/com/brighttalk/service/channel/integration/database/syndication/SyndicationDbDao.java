/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ScheduledPublicationDbDao.java 71752 2013-12-11 10:39:42Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database.syndication;

import java.util.List;

import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;

/**
 * DAO for database operations on syndication tasks.
 */
public interface SyndicationDbDao {

  /**
   * Find all the consumer channels syndication details for the given communication.
   * 
   * @param communicationId The communication Id.
   * @return List of {@link CommunicationSyndication} for all the communication consumer channels.
   */
  List<CommunicationSyndication> findConsumerChannelsSyndication(long communicationId);

}