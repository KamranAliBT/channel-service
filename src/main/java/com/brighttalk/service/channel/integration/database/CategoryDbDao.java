/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CategoryDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * DAO for database operations on channel and communication categories.
 */
public interface CategoryDbDao {

  /**
   * Loads all the channel categories onto the given channel.
   * 
   * @param channel channel to load the categories for. 
   */
  void loadCategories(Channel channel);

  /**
   * Loads all the channel categories onto the given list of channels.
   * 
   * @param channels channels to load the categories for. 
   */
  void loadCategories(List<Channel> channels);

  /**
   * Load the communication categories (if any) for the given list of communications in the given channel. 
   * 
   * @param channelId the channel id to get the categories for
   * @param communications the communications to populate with their categories
   * 
   */
  void loadCategories(Long channelId, List<Communication> communications);

  /**
   * Load the communication categories (if any) for the given communications in the given channel. 
   * 
   * @param channelId the channel id to get the categories for
   * @param communication the communication to populate with their categories
   * 
   */
  void loadCategories(Long channelId, Communication communication);

}
