/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDbDao.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;
import java.util.Map;

import com.brighttalk.common.user.Session;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;

/**
 * DAO for database operations on a {@link Communication} instance.
 */
public interface CommunicationDbDao {

  /**
   * Creates a communication.
   * 
   * @param communication The communication to be created.
   * @return the newly created communication.
   */
  Communication create(Communication communication);

  /**
   * Finds the communication for the given communication id .
   * 
   * @param communicationId id of the communication
   * 
   * @return {@link Communication}
   * 
   * @throws CommunicationNotFoundException if communication has not been found in db
   */
  Communication find(Long communicationId) throws CommunicationNotFoundException;

  /**
   * Finds the communication for the given channel id and communication id.
   * 
   * @param channelId id of channel to which the communication belongs
   * @param communicationId id of the communication
   * 
   * @return {@link Communication}
   * 
   * @throws CommunicationNotFoundException if communication has not been found in db
   */
  Communication find(Long channelId, Long communicationId) throws CommunicationNotFoundException;

  /**
   * Finds the communication for the given pin number and provider.
   * 
   * @param pin the pin number of the communication
   * @param provider provider of the communication
   * 
   * @return {@link Communication}
   * 
   * @throws CommunicationNotFoundException if communication has not been found in db
   */
  Communication find(String pin, String provider) throws CommunicationNotFoundException;

  /**
   * Find all communications for a given channel, filtering those that have been syndicated in to the channel.
   * 
   * @param channelId if of the channel to find the communication for
   * 
   * @return the communications.
   */
  List<Communication> findAll(Long channelId);

  /**
   * Find featured communications for a given collection of channel ids.
   * 
   * @param channelIds ids of the channels to find the communications for
   * @param includePrivate a flag to indicate whether to include private communications in the search
   * 
   * @return the featured communications
   */
  List<Communication> findFeatured(List<Long> channelIds, boolean includePrivate);

  /**
   * Find a paginated collection of communications that exist in channels user is subscribed to.
   * 
   * @param userId the user id
   * @param serachCriteria the search criteria
   * 
   * @return collection of communications
   */
  PaginatedList<Communication> findPaginatedSubscribedChannelsCommunications(Long userId,
    MyCommunicationsSearchCriteria serachCriteria);

  /**
   * Find a paginated collection of public communications that for given communication search criteria.
   * 
   * @param criteria the search criteria
   * 
   * @return collection of communications
   */
  PaginatedList<Communication> find(CommunicationsSearchCriteria criteria);

  /**
   * Delete all the given communications. Typically used in association with deleting a channel.
   * 
   * @param communicationIds a collection of communication ids to be deleted.
   * 
   * @throws com.brighttalk.common.error.ApplicationException if unable to delete
   */
  void delete(List<Long> communicationIds);

  /**
   * Retrieves the counts for user subscibed channels communications grouped by status.
   * 
   * @param userId the user id.
   * 
   * @return a map where key is communication status and the value is the count of communications of that status
   */
  Map<String, Long> findSubscribedChannelsCommunicationsTotals(Long userId);

  /**
   * Updates a given communication.
   * 
   * @param communication object to be updated
   * 
   * @return updated communication
   */
  Communication update(Communication communication);

  /**
   * Updates the live phone number of a communciation.
   * 
   * @param communication object to be updated
   */
  void updateLivePhoneNumber(Communication communication);

  /**
   * Cancels a given communication.
   * 
   * @param communicationId id of the webcast to be cancelled
   */
  void cancel(Long communicationId);

  /**
   * Updates a given communication when it has been pivoted fron one type to another.
   * 
   * @param communication object to be updated
   * 
   * @return updated communication
   */
  Communication pivot(Communication communication);

  /**
   * Update multiple communications to a given publish status.
   * 
   * @param communicationIds ids of the communications to be updated
   * @param publishStatus the publish status
   */
  void updatePublishStatus(List<Long> communicationIds, PublishStatus publishStatus);

  /**
   * Removes vote assets for a given communication.
   * 
   * @param communicationId the id of communication the votes are to be removed.
   */
  void removeVotes(Long communicationId);

  /**
   * Removes mediazone related assets for a given communication.
   * 
   * @param communicationId the id of the communication the assets are to be removed
   */
  void removeMediazoneAssets(Long communicationId);

  /**
   * Restore mediazone related assets for a given communication id.
   * 
   * @param communicationId the id of the communication the assets are to be restored
   */
  void restoreMediazoneAssets(Long communicationId);

  /**
   * Find all the communication renditions for a given communication.
   * 
   * @param communicationId the id of the communication to load the renditions for
   * 
   * @return the renditions.
   */
  List<Rendition> findRenditionAssets(Long communicationId);

  /**
   * Load the list of presenters of the communication. These are not the authors of the presenters but the presenters
   * allowed to present the webcast (data used from can_present table).
   * 
   * @param communication the communication to load the presenters to
   * 
   * @return the list of presenters for the given communication.
   */
  List<Session> findPresenters(Communication communication);

  /**
   * Load the communication statistics to the given communication.
   * 
   * @param communication the communication to load the statistics to.
   * 
   * @return the list of viewing statistics for the given communication.
   */
  List<CommunicationStatistics> findCommunicationStatistics(Communication communication);

  /**
   * Checks if the communication is overlapping.
   * 
   * @param communication the communication to check
   * @return true if the communication overlaps with an existing one in the channel.
   */
  boolean isOverlapping(Communication communication);

  /**
   * Create the communication as an asset of the channel.
   * 
   * @param communication the communication that is a channel asset
   */
  void createChannelAsset(Communication communication);

  /**
   * Check if the communication is a valid asset on the channel.
   * 
   * @param communication to check
   * @return true if the communication is an active asset of the channel else false
   */
  boolean isActiveAsset(Communication communication);

  /**
   * Update the encoding count each time a video/hd webcast has been successfully encoded by encoding.com.
   * 
   * @param communicationId id of the communication to update the encoding count for.
   */
  void updateEncodingCount(Long communicationId);
}
