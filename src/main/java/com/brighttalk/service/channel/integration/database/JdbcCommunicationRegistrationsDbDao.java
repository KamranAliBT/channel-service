/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationRegistrationsDbDao.java 98771 2015-08-04 16:31:59Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.CommunicationRegistrationSearchCriteria.CommunicationStatusFilterType;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link CommunicationRegistrationsDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database.
 */
@Repository
@Primary
public class JdbcCommunicationRegistrationsDbDao extends JdbcDao implements CommunicationRegistrationsDbDao {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JdbcCommunicationRegistrationsDbDao.class);

  private static final String USER_ID_PARAM = "userId";

  private static final String ID_FIELD = "id";

  private static final String CHANNEL_ID_FIELD = "channel_id";

  private static final String COMMUNICATION_ID_FIELD = "communication_id";

  private static final String CREATED_FIELD = "created";

  private static final String REGISTRATION_FIELDS_FOR_SELECT = " cr.id AS id, " + " cr.channel_id AS channel_id, "
      + " cr.communication_id AS communication_id, " + " cr.created AS created ";

  /** JDBC template used to access the database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Set the datasource on this DAO.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  @Override
  public int findTotalRegistrations(final Long userId, final CommunicationRegistrationSearchCriteria criteria) {

    if (logger.isDebugEnabled()) {
      logger.debug("Getting total number of registered communications for user [" + userId + "].");
    }

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  COUNT(*) ");
    sql.append("FROM ");
    sql.append("  communication_registration cr ");
    sql.append("  JOIN communication c ON (c.id = cr.communication_id) ");
    sql.append("WHERE ");
    sql.append("  cr.user_id = :userId ");
    sql.append("  AND cr.is_active = 1 ");
    // CHECKSTYLE:ON

    params.addValue(USER_ID_PARAM, userId);

    addSearchCriteriaClauses(criteria, sql, params);

    int totalNumberOfRegistrations = 0;

    try {

      totalNumberOfRegistrations = queryForInt(jdbcTemplate, sql.toString(), params);

    } catch (EmptyResultDataAccessException exception) {

      return totalNumberOfRegistrations;
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Found [" + totalNumberOfRegistrations + "] total number of registered communications "
          + "for user [" + userId + "] and specified search criteria.");
    }

    return totalNumberOfRegistrations;
  }

  @Override
  public List<CommunicationRegistration> findRegistrations(final Long userId,
    final CommunicationRegistrationSearchCriteria criteria) {

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(REGISTRATION_FIELDS_FOR_SELECT);
    sql.append("FROM ");
    sql.append("  communication_registration cr ");
    sql.append("  JOIN communication c ON (c.id = cr.communication_id) ");
    sql.append("WHERE ");
    sql.append("  cr.user_id = :userId ");
    sql.append("  AND cr.is_active = 1 ");
    // CHECKSTYLE:ON

    params.addValue(USER_ID_PARAM, userId);

    addSearchCriteriaClauses(criteria, sql, params);
    addOrderAndLimitClauses(criteria, sql, params);

    List<CommunicationRegistration> registrations = jdbcTemplate.query(sql.toString(), params,
        new CommunicationRegistrationsRowMapper());

    return registrations;
  }

  private void addSearchCriteriaClauses(final CommunicationRegistrationSearchCriteria criteria,
    final StringBuilder sql, final MapSqlParameterSource params) {

    if (criteria.hasChannelIds()) {
      sql.append(" AND cr.channel_id IN ( :channelIds ) ");
      params.addValue("channelIds", criteria.getChannelIds());
    }

    if (criteria.hasCommunicationStatusFilter()) {
      sql.append("  AND c.status = :communicationStatus ");
      String communicationStatus = getConvertedCommunicationStatusFilter(criteria.getCommunicationStatusFilter());
      params.addValue("communicationStatus", communicationStatus);
    }
  }

  private void addOrderAndLimitClauses(final CommunicationRegistrationSearchCriteria criteria, final StringBuilder sql,
    final MapSqlParameterSource params) {

    sql.append("ORDER BY ");
    sql.append("  cr.created DESC ");
    sql.append("LIMIT ");
    sql.append("  :offset, :size");

    final int offset = (criteria.getPageNumber() - 1) * criteria.getPageSize();
    params.addValue("offset", offset);
    params.addValue("size", criteria.getPageSize());
  }

  private String getConvertedCommunicationStatusFilter(final CommunicationStatusFilterType filterType) {

    if (filterType == null) {
      return "";
    }

    return filterType.toString().toLowerCase();
  }

  @Override
  public void loadRegistrations(final Long channelId, final List<Communication> communications) {

    // just a double check. if we don't have any channel id or communications, then make sure
    // we don't hit the database to do pointless queries.
    if (channelId == null || communications == null || communications.size() == 0) {
      return;
    }

    List<Long> communicationIds = super.getCommunicationIds(communications);

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(" a.target_id as communication_id,");
    sql.append(" COUNT(cr.communication_id) AS total_number_of_registrants ");
    sql.append("FROM ");
    sql.append("  asset a ");
    sql.append("  JOIN communication c ON (c.id = a.target_id AND a.type = 'communication') ");
    sql.append("  JOIN communication_registration cr ON ( cr.communication_id = a.target_id AND cr.channel_id = a.channel_id AND cr.is_active = 1) ");
    sql.append("WHERE ");
    sql.append("  a.target_id IN ( :communicationIds )");
    sql.append("  AND cr.channel_id = :channelId ");
    sql.append("  AND c.status != 'deleted'");
    sql.append("  AND a.is_active = 1 ");
    sql.append("GROUP BY ");
    sql.append("  a.target_id ");

    params.addValue("channelId", channelId);
    params.addValue("communicationIds", communicationIds);
    // CHECKSTYLE:ON

    // Key of the map is the communcation id and value of the map is the number of registrants.
    List<Map<Long, Integer>> communicationRegistrations = null;

    try {
      communicationRegistrations = jdbcTemplate.query(sql.toString(), params, new CommunicationRegistrantsRowMapper());

    } catch (EmptyResultDataAccessException e) {
      throw new NotFoundException("Problems getting communication registrations for channel [" + channelId
          + "] and communications [" + communicationIds + "]", e);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Found communication registrations for channelId [" + channelId + "]. Registrations ["
          + communicationRegistrations + "].");
    }

    this.addRegistrationStatistics(communications, communicationRegistrations);
  }

  @Override
  public void loadRegistrations(final List<Communication> syndicatedOutCommunications) {

    // just a double check. if we don't have any channel id or communications, then make sure
    // we don't hit the database to do pointless queries.
    if (syndicatedOutCommunications == null || syndicatedOutCommunications.size() == 0) {
      return;
    }

    List<Long> communicationIds = super.getCommunicationIds(syndicatedOutCommunications);

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(" a.target_id as communication_id,");
    sql.append(" COUNT(cr.communication_id) AS total_number_of_registrants ");
    sql.append("FROM ");
    sql.append("  asset a ");
    sql.append("  JOIN communication c ON (c.id = a.target_id AND a.type = 'communication') ");
    sql.append("  JOIN communication_registration cr ON ( cr.communication_id = a.target_id AND cr.is_active = 1) ");
    sql.append("WHERE ");
    sql.append("  a.target_id IN ( :communicationIds )");
    sql.append("  AND c.status != 'deleted'");
    sql.append("  AND a.is_active = 1 ");
    sql.append("GROUP BY ");
    sql.append("  a.target_id ");

    params.addValue("communicationIds", communicationIds);
    // CHECKSTYLE:ON

    // Key of the map is the communcation id and value of the map is the number of registrants.
    List<Map<Long, Integer>> syndicatedCommunicationRegistrations = null;

    try {
      syndicatedCommunicationRegistrations = jdbcTemplate.query(sql.toString(), params,
          new CommunicationRegistrantsRowMapper());
    } catch (EmptyResultDataAccessException e) {
      throw new NotFoundException("Problems getting communication registrations for syndicated communications ["
          + communicationIds + "]", e);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Found communication registrations for syndicagted communications. Registrations ["
          + syndicatedCommunicationRegistrations + "].");
    }

    this.addRegistrationStatistics(syndicatedOutCommunications, syndicatedCommunicationRegistrations);

  }

  /**
   * Add the registrants to the given communications.
   * 
   * @param communications the communications to add the registrants to.
   * @param registrations the number of registrants to add to each communication. Key of the map is the communcation id
   * and value of the map is the number of registrants.
   */
  private void addRegistrationStatistics(final List<Communication> communications,
    final List<Map<Long, Integer>> registrations) {

    for (Communication communication : communications) {
      for (Map<Long, Integer> registrationData : registrations) {
        Long communicationId = communication.getId();
        if (registrationData.containsKey(communicationId)) {
          communication.setNumberOfRegistrants(registrationData.get(communicationId));
        }
      }
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to a {@link CommunicationRegistration}.
   */
  private static class CommunicationRegistrationsRowMapper implements RowMapper<CommunicationRegistration> {

    /**
     * {@inheritDoc}
     */
    @Override
    public CommunicationRegistration mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      CommunicationRegistration registration = new CommunicationRegistration();
      registration.setId(resultSet.getLong(ID_FIELD));
      registration.setChannelId(resultSet.getLong(CHANNEL_ID_FIELD));
      registration.setCommunicationId(resultSet.getLong(COMMUNICATION_ID_FIELD));
      registration.setRegistered(resultSet.getTimestamp(CREATED_FIELD));

      return registration;
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to a map of communication id and number of
   * registrants.
   */
  private static class CommunicationRegistrantsRowMapper implements RowMapper<Map<Long, Integer>> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Long, Integer> mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Map<Long, Integer> registrationData = new HashMap<Long, Integer>();
      registrationData.put(resultSet.getLong("communication_id"), resultSet.getInt("total_number_of_registrants"));
      return registrationData;

    }
  }
}