/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the DTO Objects used when interacting with the Audience Service.
 */
package com.brighttalk.service.channel.integration.audience.dto;