/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelTypeDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import com.brighttalk.service.channel.business.domain.channel.ChannelType;
/**
 * DAO for database operations on a {@link ChannelType} instance.
 */
public interface ChannelTypeDbDao {

  /**
   * Finds the default channel type.
   * 
   * @return {@link ChannelType}
   */
  ChannelType findDefault();
  
  /**
   * Finds the active and enabled channel type for the given channel id.
   * 
   * @param name the name of the channel type
   * 
   * @return {@link ChannelType}
   * 
   * @throws com.brighttalk.service.channel.business.error.NotFoundException when channel type is not found
   */
  ChannelType find(String name);
}