/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationCategoryDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.HashMap;
import java.util.List;

import com.brighttalk.service.channel.business.domain.communication.CommunicationCategory;

/**
 * DAO for database operations on a {@link CommunicationCategory} instance.
 */
public interface CommunicationCategoryDbDao {

  /**
   * Returns a list of communication categories.
   * 
   * @param channelId the id of the channel
   * @param communicationId the id of the communication
   * @return list of commnunication categories
   */
  HashMap<Long, CommunicationCategory> find(Long channelId, Long communicationId);

  /**
   * Creates a communication category.
   * 
   * @param category The communication category to be created.
   */
  void create(CommunicationCategory category);

  /**
   * Updates communication categories.
   * 
   * @param categories The communication categories to be updated.
   */
  void update(List<CommunicationCategory> categories);
}
