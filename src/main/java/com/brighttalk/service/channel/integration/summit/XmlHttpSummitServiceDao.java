/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpSummitServiceDao.java 70804 2013-11-12 14:03:50Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.summit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.summit.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.summit.dto.SummitDto;
import com.brighttalk.service.channel.integration.summit.dto.SummitsPublicDto;
import com.brighttalk.service.channel.integration.summit.dto.converter.SummitRequestConverter;

/**
 * Implementation of the Dao to interact with the Summit service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
@Component
public class XmlHttpSummitServiceDao implements SummitServiceDao {

  private static final String ERROR_MESSAGE_MAX_CONSECUTIVE_REQUESTS =
      "The max consecutive requests for the Get Summits API was reached.";

  private static final Logger LOGGER = Logger.getLogger(XmlHttpSummitServiceDao.class);

  /**
   * Send email request path.
   */
  protected static final String REQUEST_PATH = "/internal/webcast/";

  private static final String FIND_PUBLIC_SUMMITS_API_URI_TEMPLATE = "/internal/summits";

  private static final int FIND_SUMMITS_BY_ID_MAX_REQUESTS = 100;

  @Autowired
  @Qualifier(value = "summitService")
  private RestTemplate restTemplate;

  @Value("${service.summitServiceUrl}")
  private String serviceBaseUrl;

  @Autowired
  private SummitRequestConverter converter;

  /** {@inheritDoc} */
  @Override
  public void inform(final Communication communication) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Inform summit of change for communication [" + communication.getId() + "].");
    }

    CommunicationDto request = converter.convert(communication);
    String url = serviceBaseUrl + REQUEST_PATH + communication.getId();

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Sending request [" + request + "] to summit service using URL [" + url + "].");
    }

    restTemplate.put(url, request);
  }

  /** {@inheritDoc} */
  @Override
  public List<SummitDto> findPublicSummitsByIds(final List<Long> summitIds) {
    Validate.notEmpty(summitIds, "List of supplied summit Ids must be supplied and not empty.");

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Received request to find the public summit details for summits [" + summitIds + "].");
    }

    HttpHeaders requestHeaders = new HttpHeaders();
    requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    requestHeaders.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<SummitDto> requestEntity = new HttpEntity<SummitDto>(null, requestHeaders);

    String url = buildFindPublicSummitsApiURL(summitIds);

    ResponseEntity<SummitsPublicDto> responseEntity;
    List<SummitDto> summits = new ArrayList<>();
    SummitsPublicDto responseBody;
    int requests = 0;

    do {
      responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, SummitsPublicDto.class);
      responseBody = responseEntity.getBody();

      if (CollectionUtils.isEmpty(responseBody.getSummits())) {
        break;
      }

      summits.addAll(responseBody.getSummits());

      if (!responseBody.hasNextLink()) {
        break;
      }

      if (++requests >= FIND_SUMMITS_BY_ID_MAX_REQUESTS) {
        throw new ApplicationException(ERROR_MESSAGE_MAX_CONSECUTIVE_REQUESTS);
      }

      url = responseBody.getNextLink().getHref();
    } while (true);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully found the details of summits [" + summits + "].");
    }

    return summits;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public void setConverter(final SummitRequestConverter converter) {
    this.converter = converter;
  }

  /**
   * Builds the Find Public Summits API URL.
   * 
   * @param summitIds List of summits.
   * 
   * @return The find public summits API URL.
   */
  private String buildFindPublicSummitsApiURL(final List<Long> summitIds) {
    String commaSeparatedIds = StringUtils.join(summitIds, ",");
    return UriComponentsBuilder.fromUriString(serviceBaseUrl)
        .path(FIND_PUBLIC_SUMMITS_API_URI_TEMPLATE)
        .queryParam("ids", commaSeparatedIds)
        .build()
        .toUriString();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void delete(final Long communicationId) {
    // @todo
  }

}