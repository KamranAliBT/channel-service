/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpCommunityServiceDao.java 98710 2015-08-03 09:52:40Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.community;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.community.dto.CommunicationsDto;
import com.brighttalk.service.channel.integration.community.dto.converter.CommunicationsConverter;

/**
 * Implementation of the Dao to interact with the Community service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
public class XmlHttpCommunityServiceDao implements CommunityServiceDao {

  /**
   * Path used when calling community service to delete channel communities.
   */
  protected static final String CHANNEL_REQUEST_PATH = "/internal/channel/{id}";

  /**
   * Path used when calling community service to delete communiciation communities.
   */
  protected static final String COMMUNICATIONS_REQUEST_PATH = "/internal/communications/delete";

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(XmlHttpCommunityServiceDao.class);

  @Autowired
  private CommunicationsConverter converter;

  private RestTemplate restTemplate;

  private String serviceBaseUrl;

  public CommunicationsConverter getConverter() {
    return converter;
  }

  public void setConverter(final CommunicationsConverter converter) {
    this.converter = converter;
  }

  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public String getServiceBaseUrl() {
    return serviceBaseUrl;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  /** {@inheritDoc} */
  @Override
  public void deleteChannel(final Long channelId) {

    HttpHeaders requestHeaders = getXAuthorisedHeader();
    HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);

    Map<String, String> vars = Collections.singletonMap("id", channelId.toString());
    String url = serviceBaseUrl + CHANNEL_REQUEST_PATH;

    if (logger.isDebugEnabled()) {
      logger.debug("Deleting channel [" + channelId + "] community settings using URL [" + url + "].");
    }

    // Set to Void.class as we do not expect any response type for this call
    restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, Void.class, vars);
  }

  /** {@inheritDoc} */
  @Override
  public void deleteCommunications(final Long channelId, final List<Long> communicationIds) {

    if (logger.isDebugEnabled()) {
      logger.debug("Deleting a collection of [" + communicationIds.size() + "] communications' "
          + "community settings for channel [" + channelId + "].");
    }

    CommunicationsDto requestDto = converter.convert(channelId, communicationIds);

    HttpHeaders requestHeaders = getXAuthorisedHeader();
    HttpEntity<?> httpEntity = new HttpEntity<Object>(requestDto, requestHeaders);

    String url = serviceBaseUrl + COMMUNICATIONS_REQUEST_PATH;

    if (logger.isDebugEnabled()) {
      logger.debug("Deleting channel [" + channelId + "] community communications settings using URL [" + url + "].");
    }
    restTemplate.postForLocation(url, httpEntity);
  }

  private HttpHeaders getXAuthorisedHeader() {
    HttpHeaders requestHeaders = new HttpHeaders();

    // CHECKSTYLE:OFF
    requestHeaders.set("X-Pre-Authorized", "true");
    // CHECKSTYLE:ON
    return requestHeaders;
  }
}