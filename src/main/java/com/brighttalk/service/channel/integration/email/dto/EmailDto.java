/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EmailDto.java 71274 2013-11-26 14:01:47Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.email.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * EmailDto.
 */
public class EmailDto {

  private String type;

  private ChannelDto channel;
  
  private CommunicationDto communication;

  public String getType() {
    return type;
  }

  @XmlAttribute
  public void setType(String type) {
    this.type = type;
  }

  public ChannelDto getChannel() {
    return channel;
  }

  public void setChannel(ChannelDto channel) {
    this.channel = channel;
  }

  /**
   * @return the communication
   */
  public CommunicationDto getCommunication() {
    return communication;
  }

  /**
   * @param communication the communication to set
   */
  public void setCommunication(CommunicationDto communication) {
    this.communication = communication;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("channel: [").append(channel).append("] ");
    builder.append("communication: [").append(communication).append("] ");

    return builder.toString();
  }
}