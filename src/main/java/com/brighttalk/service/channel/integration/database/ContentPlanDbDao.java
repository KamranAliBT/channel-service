/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CategoryDbDao.java 63781 2013-04-29 10:40:35Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.Date;

/**
 * DAO for database operations on channel and communication regarding the content plan.
 */
public interface ContentPlanDbDao {

  /**
   * This returns a count of the number of communications scheduled in a channel in a given period of time
   * 
   * Syndicated in communications can be excluded if necessary
   * 
   * A specified communication can also be exclueded form the count - useful when checking capacity
   * 
   * Various communications can be excl
   * 
   * method allows verifying if the count of communication created in the channel exceed the limit of communication to
   * be created for the given channel for a given period.
   * 
   * @param channelId the id of the channel to count the number of communication in the given period
   * @param startPeriod the start of period of the communication count
   * @param endPeriod the end of period of the communication count
   * @param includeSyndicatedCommunications include Syndicated Communications in the count
   * @param communicationId the id of the communication to ignore in the count (used for update)
   * @return the count of communications in the given period
   */
  Long countCommunicationsInPeriod(Long channelId, Date startPeriod, Date endPeriod,
    boolean includeSyndicatedCommunications, Long communicationId);

}
