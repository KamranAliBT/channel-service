/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationConfigurationDbDao.java 89901 2015-02-13 15:59:18Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link CommunicationConfigurationDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database.
 */
@Repository
public class JdbcCommunicationConfigurationDbDao extends AbstractJdbcCommunicationDbDao implements CommunicationConfigurationDbDao {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcCommunicationConfigurationDbDao.class);

  private static final String CHANNEL_ID_SQL_PARAMETER = "channelId";
  private static final String COMM_ID_SQL_PARAMETER = "communicationId";
  private static final String VISIBILITY_SQL_PARAMETER = "visibility";
  private static final String IS_MASTER_SQL_PARAMETER = "isMaster";
  private static final String ALLOW_ANON_VIEWINGS_SQL_PARAMETER = "allowAnonViewings";
  private static final String SHOW_CHANNEL_SURVEY_SQL_PARAMETER = "showChannelSurvey";
  private static final String CUSTOM_URL_SQL_PARAMETER = "customUrl";
  private static final String EXCLUDE_FROM_CHANNEL_CAPACITY_SQL_PARAMETER = "excludeFromChannelCapacity";
  private static final String CLIENT_BOOKING_REF_SQL_PARAMETER = "clientBookingRef";
  private static final String CREATED_SQL_PARAMETER = "created";
  private static final String LAST_UPDATED_SQL_PARAMETER = "lastUpdated";

  /* @formatter:off */
  /**
   * Named fields to create communication configuration query.
   */
  private static final String COMMUNICATION_CONFIGURATION_SELECT_FIELDS = ""
    + " c.master_channel_id, " 
    + " channel_id, " 
    + " is_master AS in_master_channel, " 
    + " visibility, " 
    + " allow_anon_viewings," 
    + " survey_id," 
    + " survey_active," 
    + " show_channel_survey," 
    + " custom_url," 
    + " syndication_type," 
    + " syndication_auth_master," 
    + " UNIX_TIMESTAMP(syndication_auth_master_timestamp) AS syndication_auth_master_timestamp," 
    + " syndication_auth_consumer," 
    + " UNIX_TIMESTAMP(syndication_auth_consumer_timestamp) AS syndication_auth_consumer_timestamp," 
    + " exclude_from_channel_capacity," 
    + " client_booking_reference,"  
    + " cc.created AS cc_created,"
    + " cc.last_updated AS cc_last_updated";
  
  /**
   * Named fields to create communication configuration query.
   */
  private static final String COMMUNICATION_CONFIGURATION_CREATE_FIELDS = ""
    + "channel_id = :channelId, "
    + "communication_id = :communicationId, "
    + "visibility = :visibility, "
    + "is_master = :isMaster, "
    + "allow_anon_viewings = :allowAnonViewings, "
    + "show_channel_survey = :showChannelSurvey, "
    + "custom_url = :customUrl, "
    + "exclude_from_channel_capacity = :excludeFromChannelCapacity, "
    + "client_booking_reference = :clientBookingRef, "
    + "created = :created, "     
    + "last_updated = :lastUpdated ";  

  /**
   * Named fields to update communication configuration query.
   */
  private static final String COMMUNICATION_CONFIGURATION_UPDATE_FIELDS = ""
    + "visibility = :visibility, "
    + "allow_anon_viewings = :allowAnonViewings, "
    + "show_channel_survey = :showChannelSurvey, "
    + "custom_url = :customUrl, "
    + "exclude_from_channel_capacity = :excludeFromChannelCapacity, "
    + "client_booking_reference = :clientBookingRef, "
    + "last_updated = :lastUpdated ";  
/* @formatter:on */

  /** JDBC template used to access the database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public void create(final CommunicationConfiguration config) {
    LOGGER.debug("Creating communication configuration.");

    String sql = "INSERT INTO " + " `communication_configuration` " + "SET "
        + COMMUNICATION_CONFIGURATION_CREATE_FIELDS;

    jdbcTemplate.update(sql, getCommunicationConfigParamsMapForCreate(config));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void update(final CommunicationConfiguration config) {
    LOGGER.debug("Updating communication configuration in DB.");

    String sql = "UPDATE " + " `communication_configuration` " + "SET " + COMMUNICATION_CONFIGURATION_UPDATE_FIELDS
        + " WHERE channel_id = :channelId AND communication_id = :communicationId";

    jdbcTemplate.update(sql, getCommunicationConfigParamsMapForUpdate(config));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CommunicationConfiguration get(final Long channelId, final Long communicationId) {
    LOGGER.debug("Loading communication configuration.");

    String sql = "SELECT " + COMMUNICATION_CONFIGURATION_SELECT_FIELDS + " FROM " + " communication_configuration cc "
        + "JOIN communication c ON c.id = cc.communication_id " + "WHERE " + " channel_id = :channelId " + "AND "
        + " communication_id = :communicationId";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COMM_ID_SQL_PARAMETER, communicationId);
    params.addValue(CHANNEL_ID_SQL_PARAMETER, channelId);

    CommunicationConfiguration communicationConfiguration = null;
    try {
      communicationConfiguration = jdbcTemplate.queryForObject(sql, params, new CommunicationConfigurationRowMapper(
          communicationId));
    } catch (EmptyResultDataAccessException erdae) {
      throw new NotFoundException("No communication configuration found for webcast [" + communicationId
          + "] in channel [" + channelId + "]. sql: [" + sql + "]", erdae);
    }

    return communicationConfiguration;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link CommunicationConfiguration}.
   */
  protected class CommunicationConfigurationRowMapper implements RowMapper<CommunicationConfiguration> {

    /** id of the communciation to load. */
    protected Long communicationId;

    /**
     * Constructor.
     * 
     * @param communicationId id of the communication to set.
     */
    public CommunicationConfigurationRowMapper(final Long communicationId) {
      this.communicationId = communicationId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommunicationConfiguration mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
      // map the communications configuration
      CommunicationConfiguration communicationConfiguration = mapCommunicationConfiguration(communicationId, resultSet);
      return communicationConfiguration;
    }
  }

  /**
   * Retrieves a populated parameter map for creating a communication configuration.
   * 
   * @param config the communication configuration to populate the map with
   * @return the populated map.
   */
  private MapSqlParameterSource getCommunicationConfigParamsMapForCreate(final CommunicationConfiguration config) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    if (config != null) {
      params.addValue(CHANNEL_ID_SQL_PARAMETER, config.getChannelId());
      params.addValue(COMM_ID_SQL_PARAMETER, config.getId());
      params.addValue(CUSTOM_URL_SQL_PARAMETER, config.getCustomUrl());
      params.addValue(VISIBILITY_SQL_PARAMETER, config.getVisibility().name());
      params.addValue(IS_MASTER_SQL_PARAMETER, 1);
      params.addValue(ALLOW_ANON_VIEWINGS_SQL_PARAMETER, config.allowAnonymousViewings() ? 1 : 0);
      params.addValue(SHOW_CHANNEL_SURVEY_SQL_PARAMETER, config.showChannelSurvey() ? 1 : 0);
      params.addValue(EXCLUDE_FROM_CHANNEL_CAPACITY_SQL_PARAMETER, config.excludeFromChannelContentPlan() ? 1 : 0);
      params.addValue(CLIENT_BOOKING_REF_SQL_PARAMETER, config.getClientBookingReference());
      params.addValue(CREATED_SQL_PARAMETER, new Date());
      params.addValue(LAST_UPDATED_SQL_PARAMETER, new Date());
    }
    return params;
  }

  /**
   * Retrieves a populated parameter map for creating a communication configuration.
   * 
   * @param config the communication configuration to populate the map with
   * @return the populated map.
   */
  private MapSqlParameterSource getCommunicationConfigParamsMapForUpdate(final CommunicationConfiguration config) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    if (config != null) {
      params.addValue(COMM_ID_SQL_PARAMETER, config.getId());
      params.addValue(CHANNEL_ID_SQL_PARAMETER, config.getChannelId());
      params.addValue(CUSTOM_URL_SQL_PARAMETER, config.getCustomUrl());
      params.addValue(VISIBILITY_SQL_PARAMETER, config.getVisibility().name());
      params.addValue(ALLOW_ANON_VIEWINGS_SQL_PARAMETER, config.allowAnonymousViewings() ? 1 : 0);
      params.addValue(SHOW_CHANNEL_SURVEY_SQL_PARAMETER, config.showChannelSurvey() ? 1 : 0);
      params.addValue(EXCLUDE_FROM_CHANNEL_CAPACITY_SQL_PARAMETER, config.excludeFromChannelContentPlan() ? 1 : 0);
      params.addValue(CLIENT_BOOKING_REF_SQL_PARAMETER, config.getClientBookingReference());
      params.addValue(LAST_UPDATED_SQL_PARAMETER, new Date());
    }
    return params;
  }

}
