/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationsConverter.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.community.dto.converter;

import java.util.List;

import com.brighttalk.service.channel.integration.community.dto.CommunicationsDto;

/**
 * Interface for Communication Communities Converters - used to convert collections of communication Objects 
 * to Communication Dto request objects.
 */
public interface CommunicationsConverter {

  /**
   * Convert the given collection of communications into a DTO.  
   * 
   * @param channelId the id.
   * @param communicationIds a collection of communications
   * 
   * @return dto object
   */
  CommunicationsDto convert( Long channelId, List<Long> communicationIds );
}
