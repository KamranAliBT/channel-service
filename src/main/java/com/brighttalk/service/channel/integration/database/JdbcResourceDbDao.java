/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcResourceDbDao.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Resource;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link ResourceDbDao} which uses JDBC via Spring's SimpleJDBCTemplate to
 * access the Database and SimpleJdbcInsert to insert resource data to database.
 */
@Repository
@Primary
public class JdbcResourceDbDao extends JdbcDao implements ResourceDbDao {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JdbcResourceDbDao.class);

  private static final String COLUMN_COMMUNICATION_ID = "communication_id";

  private static final String COLUMN_CREATED = "created";

  private static final String COLUMN_DESCRIPTION = "description";

  private static final String COLUMN_HOSTED = "hosted";

  private static final String COLUMN_IS_ACTIVE = "is_active";

  private static final String COLUMN_LAST_UPDATED = "last_updated";

  private static final String COLUMN_MIME_TYPE = "mime_type";

  private static final String COLUMN_PRECEDENCE = "precedence";

  private static final String COLUMN_SIZE = "size";

  private static final String COLUMN_TARGET_ID = "target_id";

  private static final String COLUMN_TITLE = "title";

  private static final String COLUMN_TYPE = "type";

  private static final String COLUMN_URL = "url";

  private static final String COLUMN_ID = "id";

  private static final String COLUMN_RESOURCE_ID = "resourceId";

  private static final String TABLE_COMMUNICATION_ASSET = "communication_asset";

  private static final String TABLE_RESOURCE = "resource";

  /** JDBC template used to access the channel database. */
  protected NamedParameterJdbcTemplate jdbcTemplate;

  /** JDBC Insert to insert details of resource into resource table. */
  private SimpleJdbcInsert insertResource;

  /** JDBC Insert to insert details of communication asset into communication_asset table. */
  private SimpleJdbcInsert insertCommunicationAsset;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") DataSource dataSource) {

    this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    this.insertResource = new SimpleJdbcInsert(dataSource);
    this.insertResource.withTableName(TABLE_RESOURCE);
    this.insertResource.usingGeneratedKeyColumns(COLUMN_ID);
    this.insertResource.usingColumns(
            COLUMN_TITLE,
            COLUMN_URL,
            COLUMN_TYPE,
            COLUMN_HOSTED,
            COLUMN_SIZE,
            COLUMN_MIME_TYPE,
            COLUMN_PRECEDENCE,
            COLUMN_CREATED,
            COLUMN_LAST_UPDATED,
            COLUMN_IS_ACTIVE);

    this.insertCommunicationAsset = new SimpleJdbcInsert(dataSource);
    this.insertCommunicationAsset.withTableName(TABLE_COMMUNICATION_ASSET);
    this.insertCommunicationAsset.usingColumns(
            COLUMN_COMMUNICATION_ID,
            COLUMN_TARGET_ID,
            COLUMN_TYPE,
            COLUMN_CREATED,
            COLUMN_LAST_UPDATED,
            COLUMN_IS_ACTIVE);
  }

  /** {@inheritDoc} */
  @Override
  public Resource find(Long resourceId) throws NotFoundException {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  r.id AS id, ");
    sql.append("  r.title AS title, ");
    sql.append("  r.type AS type, ");
    sql.append("  r.hosted AS hosted, ");
    sql.append("  r.url AS url, ");
    sql.append("  r.size AS size, ");
    sql.append("  r.mime_type AS mime_type, ");
    sql.append("  r.created AS created, ");
    sql.append("  r.last_updated AS last_updated, ");
    sql.append("  d.description AS description ");
    sql.append("FROM ");
    sql.append("  resource r ");
    sql.append("  LEFT JOIN resource_description d ON (r.id = d.id) ");
    sql.append("WHERE ");
    sql.append("  r.is_active=1 AND ");
    sql.append("  r.id = :resourceId ");
    // CHECKSTYLE:ON

    Resource resource = null;
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_RESOURCE_ID, resourceId);

    try {
      resource = jdbcTemplate.queryForObject(sql.toString(), params, new ResourceRowMapper());
    } catch (EmptyResultDataAccessException e) {
      throw new NotFoundException("Resource [" + resourceId + "] not found.", e);
    }
    if (logger.isDebugEnabled()) {
      logger.debug("Found resource [" + resource + "]");
    }

    return resource;
  }

  /** {@inheritDoc} */
  @Override
  public List<Resource> findAll(Long communicationId) {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  r.id AS id, ");
    sql.append("  r.title AS title, ");
    sql.append("  r.type AS type, ");
    sql.append("  r.hosted AS hosted, ");
    sql.append("  r.url AS url, ");
    sql.append("  r.size AS size, ");
    sql.append("  r.mime_type AS mime_type, ");
    sql.append("  r.created AS created, ");
    sql.append("  r.last_updated AS last_updated, ");
    sql.append("  d.description AS description ");
    sql.append("FROM ");
    sql.append("  resource r ");
    sql.append("  LEFT JOIN resource_description d ON (r.id = d.id) ");
    sql.append("  LEFT JOIN communication_asset a ON (r.id = a.target_id AND a.type = 'resource') ");
    sql.append("WHERE ");
    sql.append("  r.is_active=1 AND ");
    sql.append("  a.is_active=1 AND ");
    sql.append("  a.communication_id = :communicationId ");
    sql.append("ORDER BY ");
    sql.append("  r.precedence ASC ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communicationId", communicationId);

    List<Resource> resources = jdbcTemplate.query(sql.toString(), params, new ResourceRowMapper());

    if (logger.isDebugEnabled()) {
      logger.debug("Found [" + resources.size() + "] resources for communication [" + communicationId + "].");
    }

    return resources;
  }

  /** {@inheritDoc} */
  @Override
  public Map<String, Long> findChannelIdAndCommunicationId(Long resourceId) {

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  c.id AS 'communicationId', ");
    sql.append("  c.master_channel_id AS 'channelId' ");
    sql.append("FROM ");
    sql.append("  communication_asset ca ");
    sql.append("  JOIN communication c ON ( ca.communication_id = c.id) ");
    sql.append("WHERE  ");
    sql.append("  ca.type = 'resource' ");
    sql.append("  AND ca.target_id = :resourceId");
    sql.append("  AND ca.is_active = 1 ");
    sql.append("ORDER BY ");
    sql.append("  ca.created ASC ");
    sql.append("LIMIT 1 ");
    // CHECKSTYLE:ON

    // CHECKSTYLE:OFF
    params.addValue(COLUMN_RESOURCE_ID, resourceId);
    // CHECKSTYLE:ON

    Map<String, Object> row;
    try {
      row = this.jdbcTemplate.queryForMap(sql.toString(), params);
    } catch (EmptyResultDataAccessException e) {
      throw new NotFoundException("Resource [" + resourceId + "] not found.", e);
    }

    Map<String, Long> result = new HashMap<String, Long>();

    for (Map.Entry<String, Object> entry : row.entrySet()) {
      result.put(entry.getKey(), (Long) entry.getValue());
    }

    return result;
  }

  /** {@inheritDoc} */
  @Override
  public Resource create(Long communicationId, Resource resource) {

    // update the resource create time and lust update time with current timestamp
    resource.setCreated(new Date());
    resource.setLastUpdated(new Date());

    // CHECKSTYLE:OFF
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_TITLE, resource.getTitle());
    params.addValue(COLUMN_URL, resource.getUrl());
    params.addValue(COLUMN_TYPE, resource.getType().name().toLowerCase());
    if (resource.hasHosted()) {
      params.addValue(COLUMN_HOSTED, resource.getHosted().name().toLowerCase());
    }
    params.addValue(COLUMN_SIZE, resource.getSize());
    params.addValue(COLUMN_MIME_TYPE, resource.getMimeType());
    params.addValue(COLUMN_PRECEDENCE, resource.getPrecedence());
    params.addValue(COLUMN_CREATED, resource.getCreated());
    params.addValue(COLUMN_LAST_UPDATED, resource.getLastUpdated());
    params.addValue(COLUMN_IS_ACTIVE, 1);
    // CHECKSTYLE:ON

    try {

      Number primaryKey = insertResource.executeAndReturnKey(params);
      Long resourceId = primaryKey.longValue();
      resource.setId(resourceId);
      if (resource.hasDescription()) {
        this.insertOrUpdateDescription(resourceId, resource.getDescription());
      }

    } catch (DataIntegrityViolationException dive) {
      throw new ApplicationException("Could not create resource [" + resource.getId() + "]", dive);
    }

    createCommunicationResourceAsset(communicationId, resource.getId());

    if (logger.isDebugEnabled()) {
      logger.debug("Created resource [" + resource + "].");
    }
    return resource;
  }

  /**
   * Creates communication asset of type resource for the given communication id, target id.
   * 
   * @param communicationId the communication to create the communication asset on
   * @param targetId the id of an asset
   */
  private void createCommunicationResourceAsset(Long communicationId, Long targetId) {

    MapSqlParameterSource params = new MapSqlParameterSource();

    params.addValue(COLUMN_COMMUNICATION_ID, communicationId);
    params.addValue(COLUMN_TARGET_ID, targetId);
    params.addValue(COLUMN_TYPE, "resource");
    params.addValue(COLUMN_CREATED, new Date());
    params.addValue(COLUMN_LAST_UPDATED, new Date());
    params.addValue(COLUMN_IS_ACTIVE, 1);

    try {
      int noOfRows = insertCommunicationAsset.execute(params);

      if (noOfRows != 1) {
        throw new ApplicationException("Could not create communication asset target id [" + targetId
                + "]  communication id [" + communicationId + "]");
      }
    } catch (DataIntegrityViolationException dive) {
      throw new ApplicationException("Could not create communication asset target id [" + targetId
              + "]  communication id [" + communicationId + "]", dive);

    }

    if (logger.isDebugEnabled()) {
      logger.debug("Created communication asset target id [" + targetId + "]  communication id [" + communicationId
              + "]");
    }
  }

  /** {@inheritDoc} */
  @Override
  public Resource update(Resource resource) {

    // resource fields update from Delete indicator to NULL's
    resource.setTitle(convertDeleteIndicator(resource.getTitle()));
    resource.setUrl(convertDeleteIndicator(resource.getUrl()));
    resource.setMimeType(convertDeleteIndicator(resource.getMimeType()));

    // update last updated time with current time
    resource.setLastUpdated(new Date());

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  resource ");
    sql.append("SET ");
    sql.append("  url = :url, ");
    sql.append("  title = :title, ");
    sql.append("  size = :size, ");
    sql.append("  mime_type = :mime_type, ");
    sql.append("  last_updated = :last_updated ");
    sql.append("WHERE ");
    sql.append("  is_active = 1 ");
    sql.append("  AND id = :id ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_ID, resource.getId());
    params.addValue(COLUMN_TITLE, resource.getTitle());
    params.addValue(COLUMN_URL, resource.getUrl());
    params.addValue(COLUMN_SIZE, resource.getSize());
    params.addValue(COLUMN_MIME_TYPE, resource.getMimeType());
    params.addValue(COLUMN_LAST_UPDATED, resource.getLastUpdated());

    int noOfRows = jdbcTemplate.update(sql.toString(), params);

    if (noOfRows != 1) {
      throw new ApplicationException("Could not update resource [" + resource.getId() + "]");
    }

    // makes sure that resource description is not overwritten with null when description was not specified
    if (resource.hasDescription()) {
      this.insertOrUpdateDescription(resource.getId(), resource.getDescription());

      // set description to null if required.
      resource.setDescription(convertDeleteIndicator(resource.getDescription()));
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Updated resource [" + resource.getId() + "].");
    }

    return resource;
  }

  /** {@inheritDoc} */
  @Override
  public void updateOrder(List<Resource> resources) {

    int position = 0;
    for (Resource resource : resources) {
      this.updateResourcePosition(resource.getId(), position++);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Updated resources order.");
    }
  }

  /**
   * Updates a resource precedence. It is used for updating order of communication resources.
   * 
   * @param resourceId the id of a resource to be updated
   * @param precedence new order index
   */
  private void updateResourcePosition(Long resourceId, Integer precedence) {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  resource ");
    sql.append("SET ");
    sql.append("  precedence = :precedence ");
    sql.append("WHERE ");
    sql.append("  id = :id ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_ID, resourceId);
    params.addValue(COLUMN_PRECEDENCE, precedence);

    int noOfRows = jdbcTemplate.update(sql.toString(), params);

    if (noOfRows != 1) {
      throw new ApplicationException("Could not update resource precedence [" + resourceId + "]");
    }

  }

  /** {@inheritDoc} */
  @Override
  public void delete(Long communicationId, Long resourceId) {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  resource ");
    sql.append("SET ");
    sql.append("  is_active = 0, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  is_active = 1 ");
    sql.append("  AND id = :id ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_ID, resourceId);

    int noOfRows = jdbcTemplate.update(sql.toString(), params);

    if (noOfRows != 1) {
      throw new ApplicationException("Could not delete resource [" + resourceId + "]");
    }

    deleteCommunicationResourceAsset(communicationId, resourceId);

    if (logger.isDebugEnabled()) {
      logger.debug("Deleted resource [" + resourceId + "].");
    }
  }

  /**
   * Deletes communication asset resource for the given communication id and target id.
   * 
   * @param communicationId the communication to delete the communication asset on.
   * @param targetId the id of an asset
   */
  private void deleteCommunicationResourceAsset(Long communicationId, Long targetId) {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  communication_asset ");
    sql.append("SET ");
    sql.append("  is_active = 0, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  is_active = 1 ");
    sql.append("  AND target_id = :target_id ");
    sql.append("  AND communication_id = :communication_id ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_COMMUNICATION_ID, communicationId);
    params.addValue(COLUMN_TARGET_ID, targetId);

    int noOfRows = jdbcTemplate.update(sql.toString(), params);

    if (noOfRows != 1) {
      throw new ApplicationException("Could not delete ccommunication - " + "asset target id [" + targetId + "]  "
              + "communication id [" + communicationId + "]");
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Deleted communication - " + "asset target Id[" + targetId + "]  " + "communication id ["
              + communicationId + "]");
    }
  }

  /**
   * Inserts or Updates resource description.
   * 
   * @param resourceId the id of the resource being updated
   * @param description the description to be updated
   */
  private void insertOrUpdateDescription(Long resourceId, String description) {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("INSERT INTO ");
    sql.append("  resource_description ");
    sql.append("  (id, description) ");
    sql.append("VALUES ( ");
    sql.append("  :id, ");
    sql.append("  :description ");
    sql.append(") ON DUPLICATE KEY UPDATE ");
    sql.append("  description = VALUES(description) ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_ID, resourceId);
    params.addValue(COLUMN_DESCRIPTION, convertDeleteIndicator(description));

    try {
      jdbcTemplate.update(sql.toString(), params);
    } catch (DataAccessException dae) {
      throw new ApplicationException("Could not update resource description [" + resourceId + "]", dae);
    }

  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Resource}.
   */
  private class ResourceRowMapper implements RowMapper<Resource> {

    /**
     * {@inheritDoc}
     * 
     * @see "http://docs.oracle.com/javase/1.4.2/docs/api/java/sql/ResultSet.html#getLong%28java.lang.String%29"
     */
    @Override
    public Resource mapRow(ResultSet resultSet, int rowNum) throws SQLException {

      Resource resource = new Resource(resultSet.getLong(COLUMN_ID));
      resource.setTitle(resultSet.getString(COLUMN_TITLE));
      resource.setDescription(resultSet.getString(COLUMN_DESCRIPTION));
      resource.setUrl(resultSet.getString(COLUMN_URL));
      resource.setCreated(resultSet.getTimestamp(COLUMN_CREATED));
      resource.setLastUpdated(resultSet.getTimestamp(COLUMN_LAST_UPDATED));

      if (resultSet.getString(COLUMN_TYPE).toUpperCase().equals(Resource.Type.FILE.name())) {
        resource.setType(Resource.Type.FILE);

        // when the size in SQL eq NULL the getLong returns 0,
        Object size = resultSet.getString(COLUMN_SIZE);
        if (size != null) {
          resource.setSize(resultSet.getLong(COLUMN_SIZE));
        }

        resource.setMimeType(resultSet.getString(COLUMN_MIME_TYPE));
        resource.setHosted(Resource.Hosted.valueOf(resultSet.getString(COLUMN_HOSTED).toUpperCase()));
      } else {
        resource.setType(Resource.Type.LINK);
      }

      return resource;
    }
  }
}
