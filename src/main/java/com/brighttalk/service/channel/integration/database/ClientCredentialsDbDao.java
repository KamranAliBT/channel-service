/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: ClientCredentialsDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

/**
 * DAO for database operations on client credentials.
 */
public interface ClientCredentialsDbDao {

  /**
   * Authenticate the given client.
   * 
   * @param apiKey the client api key.
   * @param apiSecret the client api secret.
   * 
   * @return true if the client exists otherwise false
   */
  boolean exists(String apiKey, String apiSecret);

}
