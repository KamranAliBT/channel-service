/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 88651 2015-01-22 11:55:16Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.email.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Communication DTO used when sending communication related email.
 */
public class CommunicationDto {

  private Long id;
  private String title;
  private String status;
  private Long scheduled;
  private Integer duration;
  private String timezone;
  private String presenter;
  private ChannelDto channel;
  private String customUrl;

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  @XmlAttribute
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(final String title) {
    this.title = title;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(final String status) {
    this.status = status;
  }

  /**
   * @return the scheduled
   */
  public Long getScheduled() {
    return scheduled;
  }

  /**
   * @param scheduled the scheduled to set
   */
  public void setScheduled(final Long scheduled) {
    this.scheduled = scheduled;
  }

  /**
   * @return the duration
   */
  public Integer getDuration() {
    return duration;
  }

  /**
   * @param integer the duration to set
   */
  public void setDuration(final Integer integer) {
    duration = integer;
  }

  /**
   * @return the timezone
   */
  public String getTimezone() {
    return timezone;
  }

  /**
   * @param timezone the timezone to set
   */
  public void setTimezone(final String timezone) {
    this.timezone = timezone;
  }

  /**
   * @return the presenter
   */
  public String getPresenter() {
    return presenter;
  }

  /**
   * @param presenter the presenter to set
   */
  public void setPresenter(final String presenter) {
    this.presenter = presenter;
  }

  /**
   * @return the channel
   */
  public ChannelDto getChannel() {
    return channel;
  }

  /**
   * @param channel the channel to set
   */
  public void setChannel(final ChannelDto channel) {
    this.channel = channel;
  }

  /**
   * @return the customUrl
   */
  public String getCustomUrl() {
    return customUrl;
  }

  /**
   * @param customUrl the customUrl to set
   */
  public void setCustomUrl(final String customUrl) {
    this.customUrl = customUrl;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("id: [").append(id).append("] ");
    builder.append("title: [").append(title).append("] ");
    builder.append("status: [").append(status).append("] ");
    builder.append("scheduled: [").append(scheduled).append("] ");
    builder.append("timezone: [").append(timezone).append("] ");
    builder.append("presenter: [").append(presenter).append("] ");
    builder.append("channelId: [").append(channel.getId()).append("] ");
    builder.append("customUrl: [").append(customUrl).append("] ");

    return builder.toString();
  }
}
