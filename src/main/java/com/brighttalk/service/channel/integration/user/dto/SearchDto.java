/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: SearchDto.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.user.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.user.integration.dto.RealmDto;

/**
 * Represents a user search of BrightTALK.
 */
@XmlRootElement(name = "search")
public class SearchDto {

  /**
   * The offset value from which to return a result
   */
  private Integer offset;

  /**
   * The limit for the number of maximum values to be returned
   */
  private Integer limit;

  /**
   * The ${@link SortDto} with which to sort by
   */
  private SortDto sort;

  /**
   * User ids to search for
   */
  private String userIds;

  /**
   * User emails to search for
   */
  private String email;

  /**
   * The {@link RealmDto} to search for
   */
  private RealmDto realm;

  /**
   * Default constructor.
   */
  public SearchDto() {

  }

  /**
   * Returns the offset
   * 
   * @return the offset value
   */
  public Integer getOffset() {
    return offset;
  }

  /**
   * Sets the offset value
   * 
   * @param offset the offset value to set
   */
  public void setOffset(final Integer offset) {
    this.offset = offset;
  }

  /**
   * Returns the limit
   * 
   * @return the limit value
   */
  public Integer getLimit() {
    return limit;
  }

  /**
   * Sets the limit
   * 
   * @param limit the limit value to set
   */
  public void setLimit(final Integer limit) {
    this.limit = limit;
  }

  /**
   * Returns the {@link SortDto}
   * 
   * @return the {@link SortDto} value
   */
  public SortDto getSort() {
    return sort;
  }

  /**
   * Sets the {@link SortDto}
   * 
   * @param sort the {@link SortDto} to set
   */
  public void setSort(final SortDto sort) {
    this.sort = sort;
  }

  /**
   * Returns the email
   * 
   * @return the email value
   */
  public String getEmail() {
    return email;
  }

  /**
   * Sets the email
   * 
   * @param email the email to set
   */
  public void setEmail(final String email) {
    this.email = buildCsvFromNonEmptyElements(Arrays.asList(email.split(",")));
  }

  /**
   * Set a list of user emails from a list. If the list is empty, then the value returned by
   * {@link SearchDto#getEmail()} ()} will be null.
   * 
   * @see SearchDto#setEmail(List)
   * @see SearchDto#setEmail(String)
   * 
   * @param emails emails
   */
  public void setEmail(final List<String> emails) {
      this.email = buildCsvFromNonEmptyElements(emails);
  }

  /**
   * Returns the user Ids. This can be a comma separated list.
   * 
   * @see SearchDto#setUserIds(List)
   * @see SearchDto#setUserIds(String)
   * @return List of comma separated user Ids.
   */
  public String getUserIds() {
    return userIds;
  }

  /**
   * Sets the user Ids. This can be a comma separated list.
   * 
   * @param userIds the user ids to set
   * @see SearchDto#setUserIds(List)
   * @see SearchDto#setUserIds(String)
   */
  public void setUserIds(final String userIds) {
    this.userIds = buildCsvFromNonEmptyElements(Arrays.asList(userIds.split(",")));
  }

  /**
   * Set a {@link List} of user ids from a list. If the list is empty, then the value returned by
   * {@link SearchDto#getUserIds()} will be null.
   * 
   * @param ids user ids
   */
  public void setUserIds(final List<Long> ids) {
    if (CollectionUtils.isEmpty(ids)) {
      return;
    }

    //convert this Long list to a String List
    List<String> idsAsString = new ArrayList<>(ids.size());
    for (Long id : ids) {
      if (id != null) {
        idsAsString.add(id.toString());
      }
    }
    userIds = buildCsvFromNonEmptyElements(idsAsString);
  }

  /**
   * Returns the {@link RealmDto}
   * 
   * @return the {@link RealmDto} value
   */
  public RealmDto getRealm() {
    return realm;
  }

  /**
   * Sets the {@link RealmDto}
   * 
   * @param realm the {@link RealmDto} to set
   */
  public void setRealm(final RealmDto realm) {
    this.realm = realm;
  }

  /**
   * Returns true if {@link SearchDto#getEmail()} does not return null.
   * @return true if {@link SearchDto#getEmail()} does not return null.
   */
  public boolean hasEmail(){
    return getEmail() != null;
  }

  /**
   * Returns true if {@link SearchDto#getUserIds()} ()} does not return null.
   * @return true if {@link SearchDto#getUserIds()} ()} does not return null.
   */
  public boolean hasUserIds(){
    return getUserIds() != null;
  }

  /**
   * From a given {@link List} of {@link String}s, builds a comma-separated list of its elements that are not blank.
   * The blank criteria is assessed by calling {@link StringUtils#isNotBlank(String)}.
   *
   * @param values a {@link List} of {@link String}s. Can be null or contain null elements.
   * @return a comma-separated list of values, or null if there are no blank values found
   * @see StringUtils#isNotBlank(String)
   */
  private String buildCsvFromNonEmptyElements(List<String> values) {
    if (CollectionUtils.isEmpty(values)) {
      return null;
    }

    StringBuilder stringBuilder = new StringBuilder(values.size() * 2);
    for (String value : values) {
      if (StringUtils.isNotBlank(value)) {
        value = value.trim();

        //does this list item contain a comma? recursively process it to build a csv from that
        if (value.contains(",")) {
          value = buildCsvFromNonEmptyElements(Arrays.asList(value.split(",")));
          if (value == null) {//the value was just a lone comma so there was no CSV to be built from it
            continue;
          } else {
            stringBuilder.append(value);//append this without a comma because recursively it would have gotten a comma
          }
        } else {//this is a normal value, append it with a comma at the end
          stringBuilder.append(value).append(",");
        }
      }
    }

    String returnValue = stringBuilder.toString();
    return StringUtils.isNotBlank(returnValue) ? returnValue : null;
  }

}