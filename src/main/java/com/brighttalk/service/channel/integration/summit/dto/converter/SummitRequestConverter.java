/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SummitRequestConverter.java 70837 2013-11-13 11:28:23Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.summit.dto.converter;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.summit.dto.CommunicationDto;

/**
 * Summit Request Converter used to create DTO for communicating with the email service.
 */
@Component
public class SummitRequestConverter {

  /**
   * Convert the given {@link Communication} into summit request dto.
   * 
   * @param communication to be converted
   * 
   * @return DTO object
   */
  public CommunicationDto convert(final Communication communication) {

    CommunicationDto communicationDto = new CommunicationDto();
    communicationDto.setId(communication.getId());
    communicationDto.setScheduled(communication.getScheduledDateTime());
    communicationDto.setDuration(communication.getDuration());
    communicationDto.setPublishStatus(communication.getPublishStatus().toString().toLowerCase());

    return communicationDto;
  }

}