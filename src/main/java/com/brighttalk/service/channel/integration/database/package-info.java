/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 41851 2012-02-02 17:06:45Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the database integration classes of the Channel Service.
 */
package com.brighttalk.service.channel.integration.database;