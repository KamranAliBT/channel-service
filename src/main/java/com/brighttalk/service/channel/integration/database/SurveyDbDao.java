/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SurveyDbDao.java 55023 2012-10-01 10:22:47Z amajewski $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import com.brighttalk.service.channel.business.domain.Survey;

/**
 * DAO for database operations on channel and communication surveys.
 */
public interface SurveyDbDao {

  /**
   * Finds channel id of given survey assuming that given survey is a channel survey.
   * 
   * @param surveyId the id of the survey
   * 
   * @return {@link Survey} channel
   */
  Survey findChannelSurvey(Long surveyId);

  /**
   * Finds channel id of given survey assuming that given survey is a webcast survey.
   * 
   * @param surveyId the id of the survey
   * 
   * @return {@link Survey} communication
   */
  Survey findCommunicationSurvey(Long surveyId);
}
