/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultCommunicationsConverter.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.community.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.integration.community.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.community.dto.CommunicationsDto;

/**
 * Default implementation of an CommunicationsConverter. 
 * 
 * This takes a collection of communications and using given path template and channel details converts it to the relevant DTO object,
 * Used when making a request to the Community Service to delete all communications' community settings.
 */
@Component
public class DefaultCommunicationsConverter implements CommunicationsConverter {

  @Value("#{communicationHrefTemplate}")
  private String hrefTemplate;
  
  public String getHrefTemplate() {
    return hrefTemplate;
  }

  public void setHrefTemplate(String hrefPathTemplate) {
    this.hrefTemplate = hrefPathTemplate;
  }

  /** {@inheritDoc} */
  @Override
  public CommunicationsDto convert( Long channelId, List<Long> communicationIds ) {
    
    List<CommunicationDto> communicationsDtos = new ArrayList<CommunicationDto>();
    
    for ( Long communicationId : communicationIds ) {
      
      CommunicationDto communicationDto = new CommunicationDto();
      String href = hrefTemplate;
      href = href.replace("{id}", communicationId.toString() );
      href = href.replace("{channelId}", channelId.toString() );
      
      communicationDto.setHref( href );
      
      communicationsDtos.add( communicationDto );
    }
    
    CommunicationsDto communicationsDto = new CommunicationsDto();
    communicationsDto.setCommunications( communicationsDtos );
    
    return communicationsDto;
  }
}