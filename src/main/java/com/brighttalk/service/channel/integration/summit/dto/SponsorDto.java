/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SponsorDto.java 94242 2015-04-28 12:07:44Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.summit.dto;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.common.presentation.dto.LinkDto;
import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Sponsor DTO class.
 */
public class SponsorDto {

  private Long id;

  private String name;

  private List<LinkDto> links;

  private Long weight;

  private String type;

  private String url;

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  public void setLinks(final List<LinkDto> links) {
    this.links = links;
  }

  public Long getWeight() {
    return weight;
  }

  public void setWeight(final Long weight) {
    this.weight = weight;
  }

  public String getType() {
    return type;
  }

  public void setType(final String type) {
    this.type = type;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("id", id);
    builder.append("name", name);
    builder.append("links", links);
    builder.append("weight", weight);
    builder.append("type", type);
    builder.append("url", url);
    return builder.toString();
  }
}