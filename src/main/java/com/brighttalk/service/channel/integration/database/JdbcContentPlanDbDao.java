/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcResourceDbDao.java 57924 2012-11-26 13:53:01Z apannone $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Spring JDBC Database DAO implementation of {@link ContentPlanDbDao} which uses JDBC via Spring's SimpleJDBCTemplate
 * to access the Database.
 */
@Repository
public class JdbcContentPlanDbDao extends JdbcDao implements ContentPlanDbDao {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcContentPlanDbDao.class);

  /* @formatter:off */
  //CHECKSTYLE:OFF
  private static final String COMMUNICATIONS_COUNT_SELECT_QUERY = "SELECT "
      + "  COUNT(*) AS count "
      + "FROM "
      + "  channel c "
      + "  LEFT JOIN asset a ON (c.id = a.channel_id AND a.is_active = 1) "
      + "  LEFT JOIN communication_configuration config ON " 
      + "   (a.type = 'communication' AND a.target_id = config.communication_id AND a.channel_id = config.channel_id) "
      + "  LEFT JOIN communication cs ON (a.target_id = cs.id) "
      + "WHERE "
      + "  c.id IN (:channelId) "
      + "  AND c.is_active = 1 "
      + "  AND config.visibility != 'private' "
      + "  AND config.exclude_from_channel_capacity = 0 "
      + "  AND cs.status NOT IN ('cancelled','deleted') "
      + "  AND cs.scheduled BETWEEN FROM_UNIXTIME(:startPeriod) AND FROM_UNIXTIME(:endPeriod) ";
  // CHECKSTYLE:ON
  /* @formatter:on */

  /** JDBC template used to access the database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {

    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Long countCommunicationsInPeriod(final Long channelId, final Date startPeriod, final Date endPeriod,
    final boolean includeSyndicatedCommunications, final Long communicationId) {

    LOGGER.debug("Getting count of coummunications for the period [" + startPeriod + " - " + endPeriod + "].");

    String sql = COMMUNICATIONS_COUNT_SELECT_QUERY;

    if (!includeSyndicatedCommunications) {
      sql = sql.concat(" AND (config.syndication_type IS NULL OR syndication_type = 'OUT') ");
    }

    if (communicationId != null) {
      sql = sql.concat(" AND a.target_id != :communicationId ");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("channelId", channelId);
    params.addValue("communicationId", communicationId);
    params.addValue("startPeriod", startPeriod.getTime() / 1000);
    params.addValue("endPeriod", endPeriod.getTime() / 1000);
    // CHECKSTYLE:ON

    long communicationsCount = 0L;

    try {
      communicationsCount = queryForLong(jdbcTemplate, sql.toString(), params);
    } catch (EmptyResultDataAccessException e) {
      return communicationsCount;
    }

    LOGGER.debug("Found [" + communicationsCount + "] communications.");

    return communicationsCount;
  }
}
