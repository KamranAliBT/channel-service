/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SlideServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.slide;

/**
 * Interface to define the required methods for the interactions with the Slide Service.
 */
public interface SlideServiceDao {

  /**
   * Remove all the slides that belong to the given communication.
   * 
   * @param communicationId the id of the communication
   */
  void deleteSlides(Long communicationId);
}