/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 76503 2014-04-16 12:25:46Z jbridger $
 * ****************************************************************************
 */
/**
 * This package contains the DTO Objects used when interacting with the Audit Service.
 */
package com.brighttalk.service.channel.integration.audit.dto;