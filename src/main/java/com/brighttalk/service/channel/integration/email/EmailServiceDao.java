/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EmailServiceDao.java 88525 2015-01-20 12:15:30Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.email;

import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Interface to define the required methods for the interactions with the Email Service.
 */
public interface EmailServiceDao {

  /**
   * Send Channel Creation Confirmation Email.
   * 
   * @param channel details of the channel that was created.
   */
  void sendChannelCreateConfirmation(Channel channel);

  /**
   * Send Communication Creation Confirmation Email.
   * 
   * @param communication The created communication.
   */
  void sendCommunicationCreateConfirmation(Communication communication);

  /**
   * Send Communication reschedule Confirmation Email.
   * 
   * @param communications The list of rescheduled communication.
   */
  void sendCommunicationRescheduleConfirmation(List<Communication> communications);

  /**
   * Send Communication cancel Confirmation Email.
   * 
   * @param communications The list of cancelled communication.
   */
  void sendCommunicationCancelConfirmation(List<Communication> communications);

  /**
   * Send Communication Missed You Confirmation Email.
   * 
   * @param communications The list of cancelled communication.
   */
  void sendCommunicationMissedYouConfirmation(List<Communication> communications);

  /**
   * Send Communication Recording Published Confirmation Email.
   * 
   * @param communication The communication for which the email need sending.
   */
  void sendCommunicationRecordingPublishedConfirmation(final Communication communication);

  /**
   * Send video upload error Email.
   * 
   * @param communication of the failed encoded webcast.
   */
  void sendVideoUploadError(Communication communication);

  /**
   * Send video upload completed Email.
   * 
   * @param communication of the successfully encoded webcast.
   */
  void sendVideoUploadComplete(Communication communication);
}
