/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: AuditRecordDtoBuilder.java 98710 2015-08-03 09:52:40Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit.dto.builder;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * Helper class for creating {@link AuditRecordDto} instances for specified audit events.
 */
public abstract class AuditRecordDtoBuilder {

  /**
   * Enum containing audit actions.
   */
  protected enum AuditAction {
    /** Create audit action. */
    CREATE("create"),
    /** Cancel audit action. */
    CANCEL("cancel"),
    /** Delete audit action. */
    DELETE("delete"),
    /** Update audit action. */
    UPDATE("update");

    private final String action;

    /** @param action The audit action. */
    private AuditAction(final String action) {
      this.action = action;
    }

    /** @return the audit action. */
    public String getAction() {
      return action;
    }
  }

  /**
   * Enum containing audit entities.
   */
  protected enum AuditEntity {
    /** Channel audit entity. */
    CHANNEL("channel"),
    /** Channel features audit entity. */
    CHANNEL_FEATURES("channel_features"),
    /** Webcast audit entity. */
    WEBCAST("webcast"),
    /** Contact Details audit entity. */
    CONTACT_DETAILS("contact_details"),
    /** Channel Manager audit entity. */
    CHANNEL_MANAGER("channel_manager");

    private final String entity;

    /** @param entity The audit entity. */
    private AuditEntity(final String entity) {
      this.entity = entity;
    }

    /** @return the audit entity. */
    public String getEntity() {
      return entity;
    }

  }

  /**
   * @param userId User ID of user creating the resource.
   * @param channelId Channel ID of the created resource.
   * @param actionDescription Optional description for audit event.
   * @return an instance of {@link AuditRecordDto}.
   * @see #create(Long, Long, Long, String)
   */
  public AuditRecordDto create(final Long userId, final Long channelId, final String actionDescription) {
    return create(userId, channelId, null, actionDescription);
  }

  /**
   * Creates a {@link AuditRecordDto} for create resource event.
   * 
   * @param userId User ID of user creating the resource.
   * @param channelId Channel ID of the created resource.
   * @param communicationId webcast ID of the created resource, optional only provided if auditing webcast resource.
   * @param actionDescription Optional description for audit event.
   * @return an instance of {@link AuditRecordDto}.
   * @throws UnsupportedOperationException if this method called directly, this method should be called through one of
   * {@link AuditRecordDtoBuilder} subclasses with resource specific implementation.
   */
  public AuditRecordDto create(final Long userId, final Long channelId, final Long communicationId,
    final String actionDescription) {
    throw new UnsupportedOperationException();
  }

  /**
   * @param userId User ID of user updating the resource.
   * @param channelId Channel ID of the updated resource.
   * @param actionDescription Optional description for audit event.
   * @return an instance of {@link AuditRecordDto}.
   * @see #update(Long, Long, Long, String)
   */
  public AuditRecordDto update(final Long userId, final Long channelId, final String actionDescription) {
    return update(userId, channelId, null, actionDescription);
  }

  /**
   * Creates a {@link AuditRecordDto} for update resource event.
   * 
   * @param userId User ID of user updating the resource.
   * @param channelId Channel ID of the updated resource.
   * @param communicationId webcast ID of the update resource, optional only provided if auditing webcast resource.
   * @param actionDescription Optional description for audit event.
   * @return an instance of {@link AuditRecordDto}.
   * @throws UnsupportedOperationException if this method called directly, this method should be called through one of
   * {@link AuditRecordDtoBuilder} subclasses with resource specific implementation.
   */
  public AuditRecordDto update(final Long userId, final Long channelId, final Long communicationId,
    final String actionDescription) {
    throw new UnsupportedOperationException();
  }

  /**
   * @param userId User ID of user deleting the resource.
   * @param channelId Channel ID of the deleted resource.
   * @param actionDescription Optional description for audit event.
   * @return an instance of {@link AuditRecordDto}.
   * @see #delete(Long, Long, Long, String)
   */
  public AuditRecordDto delete(final Long userId, final Long channelId, final String actionDescription) {
    return delete(userId, channelId, null, actionDescription);
  }

  /**
   * Creates a {@link AuditRecordDto} for delete resource event.
   * 
   * @param userId User ID of user deleting the resource.
   * @param channelId Channel ID of the deleted resource.
   * @param communicationId webcast ID of the deleted resource, optional only provided if auditing webcast resource.
   * @param actionDescription Optional description for audit event.
   * @return an instance of {@link AuditRecordDto}.
   * @throws UnsupportedOperationException if this method called directly, this method should be called through one of
   * {@link AuditRecordDtoBuilder} subclasses with resource specific implementation.
   */
  public AuditRecordDto delete(final Long userId, final Long channelId, final Long communicationId,
    final String actionDescription) {
    throw new UnsupportedOperationException();
  }

  /**
   * @param userId User ID of user cancelling the resource.
   * @param channelId Channel ID of the cancelled resource.
   * @param actionDescription Optional description for audit event.
   * @return an instance of {@link AuditRecordDto}.
   * @see #cancel(Long, Long, Long, String)
   */
  public AuditRecordDto cancel(final Long userId, final Long channelId, final String actionDescription) {
    return cancel(userId, channelId, null, actionDescription);
  }

  /**
   * Creates a {@link AuditRecordDto} for cancel resource event.
   * 
   * @param userId User ID of user cancelling the resource.
   * @param channelId Channel ID of the cancelled resource.
   * @param communicationId webcast ID of the cancelled resource, optional only provided if auditing webcast resource.
   * @param actionDescription Optional description for audit event.
   * @return an instance of {@link AuditRecordDto}.
   * @throws UnsupportedOperationException if this method called directly, this method should be called through one of
   * {@link AuditRecordDtoBuilder} subclasses with resource specific implementation.
   */
  public AuditRecordDto cancel(final Long userId, final Long channelId, final Long communicationId,
    final String actionDescription) {
    throw new UnsupportedOperationException();
  }

  /**
   * Creates a {@link AuditRecordDto}.
   * 
   * @param userId User ID of user creating webcast.
   * @param channelId Channel ID that webcast has been created in.
   * @param communicationId ID of webcast.
   * @param auditEntity The audit entity.
   * @param auditAction The audit action.
   * @param actionDescription Optional description for audit event.
   * 
   * @return an instance of {@link AuditRecordDto}.
   */
  protected AuditRecordDto createAuditRecordDto(final Long userId, final Long channelId, final Long communicationId,
    final AuditEntity auditEntity, final AuditAction auditAction, final String actionDescription) {
    AuditRecordDto auditRecordDto = new AuditRecordDto();
    auditRecordDto.setUserId(userId);
    auditRecordDto.setChannelId(channelId);
    auditRecordDto.setCommunicationId(communicationId);
    auditRecordDto.setAction(auditAction.getAction());
    auditRecordDto.setEntity(auditEntity.getEntity());
    auditRecordDto.setActionDescription(actionDescription);
    return auditRecordDto;
  }
}
