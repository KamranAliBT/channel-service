/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpUserServiceDao.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.user;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.common.user.User;
import com.brighttalk.common.user.integration.dto.RealmDto;
import com.brighttalk.common.user.integration.dto.UserDto;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria.SortOrder;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria;
import com.brighttalk.service.channel.business.domain.user.UsersSearchCriteria.SortBy;
import com.brighttalk.service.channel.business.error.UserNotFoundException;
import com.brighttalk.service.channel.integration.user.converter.UserConverter;
import com.brighttalk.service.channel.integration.user.dto.SearchDto;
import com.brighttalk.service.channel.integration.user.dto.SortDto;
import com.brighttalk.service.channel.integration.user.dto.UserResponseDto;
import com.brighttalk.service.channel.integration.user.dto.UsersDto;
import com.brighttalk.service.channel.presentation.util.PaginatedListBuilder;

/**
 * Implementation of the Dao to interact with the user service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
@Component("localUserServiceDao")
public class XmlHttpUserServiceDao implements UserServiceDao {

  private static final Logger LOGGER = Logger.getLogger(XmlHttpUserServiceDao.class);

  /** Path used to retrieve a list of users. */
  protected static final String GET_USER_SEARCH = "/internal/users/search";

  /** Path used to retrieve user details. */
  protected static final String GET_USER_DETAILS = "/internal/xml/getdetails/{id}";

  /** Default realm ID when searching for users. */
  private static final Integer BRIGHTALK_REALM_ID = 1;

  /** API processed state. */
  private static final String STATE_PROCESSED = "processed";

  @Autowired
  @Qualifier("customUserServiceTemplate")
  private RestTemplate restTemplate;

  @Value("${service.userServiceUrl}")
  private String serviceBaseUrl;

  @Autowired
  private UserConverter converter;

  @Autowired
  private PaginatedListBuilder paginatedListBuilder;

  /** {@inheritDoc} */
  @Override
  public User find(final Long id) {

    LOGGER.debug("Finding user [" + id + "].");

    String url = serviceBaseUrl + GET_USER_DETAILS;

    UserResponseDto userResponseDto = restTemplate.getForObject(url, UserResponseDto.class, id);

    if (!STATE_PROCESSED.equals(userResponseDto.getState())) {
      throw new UserNotFoundException(id);
    }

    User user = converter.convert(userResponseDto.getUser());

    LOGGER.debug("User [" + user + "] found.");

    return user;
  }

  /** {@inheritDoc} */
  @Override
  public List<User> find(final List<Long> ids, final UsersSearchCriteria criteria) {

    LOGGER.debug("Finding users [" + ids + "] for criteria [" + criteria + "].");

    SearchDto search = buildDefaultSearch(criteria);
    search.setUserIds(ids);

    List<User> users = search(search);

    return users;
  }

  /** {@inheritDoc} */
  @Override
  public List<User> findByEmailAddress(final List<String> emailAddresses, final UsersSearchCriteria criteria) {

    LOGGER.debug("Finding users by email [" + emailAddresses + "].");

    SearchDto search = buildDefaultSearch(criteria);
    search.setEmail(emailAddresses);

    List<User> users = search(search);

    return users;
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<User> findByEmailAddress(final String emailAddresses, final UsersSearchCriteria criteria) {

    LOGGER.debug("Finding users by email [" + emailAddresses + "].");

    SearchDto search = buildDefaultSearch(criteria);
    search.setEmail(emailAddresses);

    PaginatedList<User> users = search(search);

    return users;
  }

  /** {@inheritDoc} */
  @Override
  public List<User> findByIdAndEmailAddress(final List<Long> ids, final List<String> emailAddresses,
      final UsersSearchCriteria criteria) {

    LOGGER.debug("Finding users by id [" + ids + "] email [" + emailAddresses + "].");

    SearchDto search = buildDefaultSearch(criteria);
    search.setUserIds(ids);
    search.setEmail(emailAddresses);

    List<User> users = search(search);

    return users;
  }

  /**
   * Makes a RESTful call to the user service with the parameterised {@link SearchDto}, returning a
   * {@link PaginatedList} of matching items.
   * If the given {@link SearchDto} has no email (determined by {@link SearchDto#hasEmail()}) or user id  (determined by
   * {@link SearchDto#hasUserIds()}), then the user service will not be called and an empty {@link PaginatedList} will
   * be returned.
   *
   * @param search the search criteria for the {@link User}s required
   *
   * @return A {@link PaginatedList} of users according to the parameterised {@link SearchDto}. If there is no user id
   * or email given, then an {@link PaginatedList} will be returned..
   */
  private PaginatedList<User> search(final SearchDto search) {
    if( !search.hasEmail() && !search.hasUserIds()) {
      LOGGER.debug("No email or user id set in the SearchDto [" + search + "], returning an empty list.");
      return new PaginatedList<>();
    }

    String url = serviceBaseUrl + GET_USER_SEARCH;

    HttpHeaders requestHeaders = new HttpHeaders();
    requestHeaders.setContentType(MediaType.APPLICATION_XML);

    HttpEntity<SearchDto> entity = new HttpEntity<>(search, requestHeaders);

    ResponseEntity<UsersDto> response = restTemplate.exchange(url, HttpMethod.POST, entity, UsersDto.class);

    List<UserDto> usersDto = response.getBody().getUsers();

    List<User> users = converter.convert(usersDto);

    int pageSize = search.getLimit();
    int pageNumber = search.getOffset() / pageSize + 1;

    PaginatedList<User> paginatedUsers = paginatedListBuilder.build(users, pageNumber, pageSize);

    LOGGER.debug("Found [" + paginatedUsers.size() + "] users.");

    return paginatedUsers;
  }

  /**
   * Builds a default {@link SearchDto} from the parameterised {@link UsersSearchCriteria}.
   *
   * @param criteria the {@link UsersSearchCriteria} for which a {@link SearchDto} is required
   *
   * @return A {@link SearchDto} built from the parameterised {@link UsersSearchCriteria}
   */
  private SearchDto buildDefaultSearch(final UsersSearchCriteria criteria) {

    RealmDto realm = new RealmDto();
    realm.setId(BRIGHTALK_REALM_ID);

    SearchDto search = new SearchDto();
    search.setOffset(criteria.getPageNumber());
    search.setLimit(criteria.getPageSize());
    search.setRealm(realm);

    SortOrder sortOrder = criteria.getSortOrder();
    SortBy sortBy = criteria.getSortBy();

    if (sortOrder != null && sortBy != null) {
      SortDto sort = new SortDto();
      sort.setOrder(criteria.getSortOrder().name().toLowerCase());
      sort.setType(criteria.getSortBy().getName());

      search.setSort(sort);
    }

    return search;
  }
}
