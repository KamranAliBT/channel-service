/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2013.
 * All Rights Reserved.
 * $Id: ReadOnlyCommunicationDbDao.java 85869 2014-11-11 11:17:11Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Read Only implementation of the CommunicationDbDao.
 * 
 * This uses a different data source with read only credentials to allow read only requests to be made to a different
 * database server etc.
 */
@Repository(value = "readOnlyCommunicationDbDao")
public class ReadOnlyCommunicationDbDao extends JdbcCommunicationDbDao {

  /**
   * Set the Datasource - Autowired to be the read only data.
   * 
   * @param dataSource The Read only data source
   */
  @Override
  @Autowired
  public void setDataSource(@Qualifier("channelReadOnlyDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * Communications cannot be updated with a read only Dao.
   * 
   * @param communication The Communication
   * @return The Communication
   * @throws UnsupportedOperationException Thrown as this method cannot be used.
   */
  @Override
  public Communication pivot(final Communication communication) {
    throw new UnsupportedOperationException("Cannot update a Communication with a Readonly Connection");
  }

  /**
   * Communications cannot be altered with a read only Dao.
   * 
   * @param communicationIds The communication Ids
   * @param publishStatus The publish statuses
   * @throws UnsupportedOperationException Thrown as this method cannot be used.
   */
  @Override
  public void updatePublishStatus(final List<Long> communicationIds, final Communication.PublishStatus publishStatus) {
    throw new UnsupportedOperationException("Cannot update publish status of Communications with a Readonly Connection");
  }

  /**
   * Votes cannot be removed from a Communications with a read only Dao.
   * 
   * @param communicationId The Id of the Communication
   * @throws UnsupportedOperationException Thrown as this method cannot be used.
   */
  @Override
  public void removeVotes(final Long communicationId) {
    throw new UnsupportedOperationException("Cannot remove votes with a Readonly Connection");
  }

  /**
   * Mediazone Assets cannot be removed from a Communications with a read only Dao.
   * 
   * @param communicationId The Id of the Communication
   * @throws UnsupportedOperationException Thrown as this method cannot be used.
   */
  @Override
  public void removeMediazoneAssets(final Long communicationId) {
    throw new UnsupportedOperationException("Cannot remove MZ Assets with a Readonly Connection");
  }

  /**
   * Mediazone Assets cannot be restored from a Communications with a read only Dao.
   * 
   * @param communicationId The Id of the Communication
   * @throws UnsupportedOperationException Thrown as this method cannot be used.
   */
  @Override
  public void restoreMediazoneAssets(final Long communicationId) {
    throw new UnsupportedOperationException("Cannot restore MZ Assets with a Readonly Connection");
  }
}
