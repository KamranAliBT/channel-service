/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcChannelFeedDbDao.java 98771 2015-08-04 16:31:59Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.FeaturedCommunicationNotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link ChannelFeedDbDao} which uses JDBC via Spring's SimpleJDBCTemplate
 * to access the Database.
 */
@Repository
@Primary
public class JdbcChannelFeedDbDao extends AbstractJdbcCommunicationDbDao implements ChannelFeedDbDao {

  private static final String JDBC_CHANNEL_ID_PARAM_NAME = "id";

  private static final String APPROVED_COMMUNICATIONS_WHERE_CLAUSE = "    AND (" + "     cc.syndication_type IS NULL "
      + "     OR cc.syndication_type = 'out' "
      + "     OR (cc.syndication_auth_master = 'approved' AND cc.syndication_auth_consumer = 'approved')" + "    )";

  /**
   * Logger for this class.
   */
  protected static final Logger LOGGER = Logger.getLogger(JdbcChannelFeedDbDao.class);

  /* @formatter:off */
  //CHECKSTYLE:OFF

  private static final String STATUS_LIST_JOIN_CLAUSE = "    AND (:has_status_list = false OR c.status IN (:statuses)) ";

  private static final String FIND_TOTAL_COMMUNICATIONS_IN_FEED_QUERY = "" 
      + "SELECT "
      + "  COUNT(*) AS total "
      + "FROM "
      + "  channel ch "
      + "  JOIN asset a ON ( "
      + "    a.channel_id = ch.id "
      + "    AND a.is_active = 1 "
      + "    AND a.type = 'communication') "
      + "  JOIN communication c ON ( "
      + "    c.id = a.target_id "
      + "    AND c.status NOT IN ('cancelled','deleted' ) "
      + STATUS_LIST_JOIN_CLAUSE
      + "    AND c.publish_status = 'published'  ) "
      + "  JOIN communication_configuration cc ON ("
      + "    cc.communication_id = a.target_id "
      + "    AND cc.channel_id = a.channel_id "
      + "    AND cc.visibility != 'private' ) "
      + "  WHERE "
      + "    ch.id = :id "
      + "    AND ch.is_active = 1 "
      + APPROVED_COMMUNICATIONS_WHERE_CLAUSE;

  private static final String FIND_FEATURED_COMMUNICATION_DATE_TIME_QUERY = ""
      + "SELECT "
      + "  c.scheduled as scheduledDate "
      + "FROM "
      + "  asset a "
      + "  JOIN communication c ON ( "
      + "    c.id = a.target_id "
      + "    AND c.status NOT IN ('cancelled','deleted' ) "
      + STATUS_LIST_JOIN_CLAUSE
      + "    AND c.publish_status = 'published'  ) "
      + "  JOIN communication_configuration cc ON ("
      + "    cc.communication_id = a.target_id "
      + "    AND cc.channel_id = a.channel_id "
      + "    AND cc.visibility != 'private' ) "
      + "  WHERE "
      + "    a.channel_id = :id "
      + "  AND a.is_active = 1 "
      + APPROVED_COMMUNICATIONS_WHERE_CLAUSE
      + "  ORDER BY "
      + "    ABS(UNIX_TIMESTAMP(c.scheduled) - UNIX_TIMESTAMP()) ASC "
      + "  LIMIT 1";

  private static final String FIND_FEED_QUERY = ""
      + "SELECT "
      + COMMUNICATION_FIELDS_FOR_SELECT
      + ","
      + COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT
      + "FROM "
      + "  channel ch "
      + "  JOIN asset a ON ( "
      + "    a.channel_id = ch.id "
      + "    AND a.is_active = 1 "
      + "    AND a.type = 'communication') "
      + "  JOIN communication c ON ( "
      + "    c.id = a.target_id "
      + "    AND c.status NOT IN ('cancelled','deleted' ) "
      + STATUS_LIST_JOIN_CLAUSE
      + "    AND c.publish_status = 'published'  ) "
      + "  JOIN communication_configuration cc ON ("
      + "    cc.communication_id = a.target_id "
      + "    AND cc.channel_id = a.channel_id "
      + "    AND cc.visibility != 'private' ) "
      + "  WHERE "
      + "    ch.id = :id "
      + "    AND ch.is_active = 1 "
      + APPROVED_COMMUNICATIONS_WHERE_CLAUSE
      + "  ORDER BY "
      + "    c.scheduled DESC "
      + "  LIMIT "
      + "    :offset, :size";

  private static final String FIND_TOTAL_COMMUNICATIONS_SCHEDULED_AFTER_DATE = ""
      + "SELECT "
      + "  COUNT(c.id) AS total "
      + "FROM "
      + "  channel ch "
      + "  JOIN asset a ON ( "
      + "    a.channel_id = ch.id "
      + "    AND a.is_active = 1 "
      + "    AND a.type = 'communication') "
      + "  JOIN communication c ON ( "
      + "    c.id = a.target_id "
      + "    AND c.status NOT IN ('cancelled','deleted' ) "
      + STATUS_LIST_JOIN_CLAUSE
      + "    AND c.publish_status = 'published'  ) "
      + "  JOIN communication_configuration cc ON ("
      + "    cc.communication_id = a.target_id "
      + "    AND cc.channel_id = a.channel_id "
      + "    AND cc.visibility != 'private' ) "
      + "  WHERE "
      + "    ch.id = :id "
      + "    AND ch.is_active = 1 "
      + APPROVED_COMMUNICATIONS_WHERE_CLAUSE
      + "    AND c.scheduled > :comparisonDate";

  /* @formatter:on */
  // CHECKSTYLE:ON

  /**
   * JDBC template used to access the database.
   */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int findTotalCommunicationsInFeed(final Long channelId, final ChannelFeedSearchCriteria criteria) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Getting total number of communications in feed, channel id [" + channelId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(JDBC_CHANNEL_ID_PARAM_NAME, channelId);

    addStatusFilterToMapSqlParameterSource(criteria, params);

    int totalNumberOfCommunications = 0;

    try {
      totalNumberOfCommunications = queryForInt(jdbcTemplate, FIND_TOTAL_COMMUNICATIONS_IN_FEED_QUERY, params);
    } catch (EmptyResultDataAccessException e) {
      // Exception swallowed as this is a valid case
      LOGGER.debug("No communications found for feed, channelId [" + channelId + "].");
      return totalNumberOfCommunications;
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + totalNumberOfCommunications + "] total number of communications for feed, "
          + "channelId [" + channelId + "].");
    }

    return totalNumberOfCommunications;
  }

  /** {@inheritDoc} */
  @Override
  public List<Communication> findFeed(final Long channelId, final ChannelFeedSearchCriteria criteria) {

    int page = criteria.getPageNumber();
    int size = criteria.getPageSize();

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Find feed for channelId [" + channelId + "]. " + "Page number [" + page + "]. " + "Size [" + size
          + "].");
    }

    int offset = (page - 1) * size;

    // Get all communications (and their config) for an active channel which are:
    // - published and not private
    // -not cancelled or deleted (i.e. upcoming, live, pending, recorded)
    // - are not syndicated or syndicated out from the target channel or approved to be syndicated in to the target
    // channel

    // Notes:
    // 1. a.is_active means an active communication

    MapSqlParameterSource params = new MapSqlParameterSource();

    params.addValue(JDBC_CHANNEL_ID_PARAM_NAME, channelId);
    params.addValue("offset", offset);
    params.addValue("size", size);

    addStatusFilterToMapSqlParameterSource(criteria, params);

    List<Communication> communications = jdbcTemplate.query(FIND_FEED_QUERY, params, new CommunicationRowMapper(domain));

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found communication feed " + "for channelId [" + channelId + "]. "
          + "Number of communications found [" + communications.size() + "].");
    }

    return communications;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Date findFeaturedDateTime(final Long channelId, final ChannelFeedSearchCriteria criteria) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding featured communication for channelId [" + channelId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(JDBC_CHANNEL_ID_PARAM_NAME, channelId);

    addStatusFilterToMapSqlParameterSource(criteria, params);

    // use a map so we can return comm id and scheduled date.
    // comm id is just used for debug logging, thats all.
    Map<String, Object> result = null;

    try {
      // closestToNowScheduledDate = jdbcTemplate.queryForObject(sql.toString(), java.util.Date.class, params);
      result = jdbcTemplate.queryForMap(FIND_FEATURED_COMMUNICATION_DATE_TIME_QUERY, params);
    } catch (EmptyResultDataAccessException e) {
      throw new FeaturedCommunicationNotFoundException(
          "Problems getting featured communication for the feed for channel [" + channelId + "].", e);
    }

    Date scheduledDate = (Date) result.get("scheduledDate");

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found featured communication for feed for channelId [" + channelId + "]. " + "Date is ["
          + scheduledDate + "].");
    }

    return scheduledDate;
  }

  /** {@inheritDoc} */
  @Override
  public int findTotalCommunicationsScheduledAfterDate(final Long channelId, final Date comparisonDate,
    final ChannelFeedSearchCriteria criteria) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("get number of communications scheduled " + "after date [" + comparisonDate + "] "
          + "for channelId [" + channelId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();

    params.addValue(JDBC_CHANNEL_ID_PARAM_NAME, channelId);
    params.addValue("comparisonDate", comparisonDate);

    addStatusFilterToMapSqlParameterSource(criteria, params);

    int totalNumberOfCommunications = 0;

    try {
      totalNumberOfCommunications = queryForInt(jdbcTemplate, FIND_TOTAL_COMMUNICATIONS_SCHEDULED_AFTER_DATE, params);
    } catch (EmptyResultDataAccessException e) {
      return 0;
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + totalNumberOfCommunications + "] communications scheduled " + "after date ["
          + comparisonDate + "].");
    }

    return totalNumberOfCommunications;
  }

  private void addStatusFilterToMapSqlParameterSource(final ChannelFeedSearchCriteria criteria,
    final MapSqlParameterSource params) {

    boolean hasStatusList = !criteria.getStatusFilterAsStringList().isEmpty();
    params.addValue("has_status_list", hasStatusList);
    params.addValue("statuses", hasStatusList ? criteria.getStatusFilterAsStringList() : null);
  }
}
