/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpEnquiryServiceDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.enquiry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.integration.enquiry.dto.EnquiryRequestDto;
import com.brighttalk.service.channel.integration.enquiry.dto.converter.EnquiryRequestConverter;

/**
 * Implementation of the Dao to interact with the Enquiry service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
@Component
public class XmlHttpEnquiryServiceDao implements EnquiryServiceDao {

  /**
   * Send enquiry request path.
   */
  protected static final String REQUEST_PATH = "/internal/xml/submit";

  private static final Logger logger = Logger.getLogger(XmlHttpEnquiryServiceDao.class);

  @Qualifier(value = "enquiryServiceRestTemplate")
  private RestTemplate restTemplate;

  @Value("${service.enquiryServiceUrl}")
  private String serviceBaseUrl;

  @Autowired
  private EnquiryRequestConverter converter;

  /** {@inheritDoc} */
  @Override
  public void sendCreateChannelRequest(Channel channel) {

    if (logger.isDebugEnabled()) {
      logger.debug("Sending request to send channel confirmation email for channel [" + channel.getId() + "].");
    }

    EnquiryRequestDto request = converter.convert(channel);
    String url = serviceBaseUrl + REQUEST_PATH;

    if (logger.isDebugEnabled()) {
      logger.debug("Sending channel creation email request [" + request + "] using URL [" + url + "].");
    }

    restTemplate.postForLocation(url, request);
  }

  public void setRestTemplate(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public void setServiceBaseUrl(String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public void setConverter(EnquiryRequestConverter converter) {
    this.converter = converter;
  }
}