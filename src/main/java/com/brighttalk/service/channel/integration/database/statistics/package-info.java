/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: package-info.java 98614 2015-07-29 16:18:32Z ssarraj $
 * ****************************************************************************
 */
/**
 * Package containing DAOs for retrieving communication statistics details.
 */
package com.brighttalk.service.channel.integration.database.statistics;