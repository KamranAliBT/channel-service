/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EnquiryServiceDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.enquiry;

import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * Interface to define the required methods for the interactions with the Enquiry Service.
 */
public interface EnquiryServiceDao {

  /**
   * Send Channel create request to enquiry service.
   * 
   * @param channel details
   */
  void sendCreateChannelRequest(Channel channel);
}