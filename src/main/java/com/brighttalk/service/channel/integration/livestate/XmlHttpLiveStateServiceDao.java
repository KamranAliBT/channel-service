/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpLiveStateServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.livestate;

import org.apache.log4j.Logger;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.livestate.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.livestate.dto.RequestDto;

/**
 * Implementation of the Dao to interact with the LiveState service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
public class XmlHttpLiveStateServiceDao implements LiveStateServiceDao {

  private static final Logger LOGGER = Logger.getLogger(XmlHttpLiveStateServiceDao.class);

  /**
   * Path used when calling live state service to delete communication from live state.
   */
  protected static final String DELETE_REQUEST_PATH = "/internal/xml/unloadcommunication";

  private RestTemplate restTemplate;

  private String serviceBaseUrl;

  public String getServiceBaseUrl() {
    return serviceBaseUrl;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Override
  public void deleteCommunication(final Long communicationId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Removing communication [" + communicationId + "] from Live State Service");
    }

    RequestDto request = createRequestDto(communicationId);

    String url = serviceBaseUrl + DELETE_REQUEST_PATH;
    restTemplate.postForLocation(url, request);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleted communication [" + communicationId + "] from Live State.");
    }
  }

  private RequestDto createRequestDto(final Long communicationId) {

    final RequestDto dto = new RequestDto();

    final CommunicationDto communicationDto = new CommunicationDto();
    communicationDto.setId(communicationId);
    dto.setCommunication(communicationDto);

    return dto;
  }
}