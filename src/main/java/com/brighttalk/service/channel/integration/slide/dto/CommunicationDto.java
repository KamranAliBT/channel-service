/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.slide.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 * CommunicationDto.
 */
public class CommunicationDto {

  private Long id;

  private List<SlideDto> slides;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(final Long id) {
    this.id = id;
  }

  public List<SlideDto> getSlides() {
    return slides;
  }

  @XmlElementWrapper(name = "slides")
  @XmlElement(name = "slide")
  public void setSlides(final List<SlideDto> slides) {
    this.slides = slides;
  }

  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("id: [").append(id).append("] ");
    builder.append("slides: [").append(slides).append("] ");

    return builder.toString();
  }
}