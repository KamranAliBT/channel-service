/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 70804 2013-11-12 14:03:50Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.les.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Communication DTO used when informing LES of a rescheduled event.
 */
@XmlRootElement(name = "webcast")
@XmlAccessorType(XmlAccessType.FIELD)
public class RescheduleCommunicationDto extends BaseCommunicationDto {

  /** id of the communication */
  private String scheduled;
  private Integer duration;
  private String timezone;

  /**
   * @return the scheduled
   */
  public String getScheduled() {
    return scheduled;
  }

  /**
   * @param scheduled the scheduled to set
   */

  public void setScheduled(final String scheduled) {
    this.scheduled = scheduled;
  }

  /**
   * @return the duration
   */

  public Integer getDuration() {
    return duration;
  }

  /**
   * @param duration the duration to set
   */

  public void setDuration(final Integer duration) {
    this.duration = duration;
  }

  /**
   * @return the timezone
   */

  public String getTimezone() {
    return timezone;
  }

  /**
   * @param timezone the timezone to set
   */

  public void setTimezone(final String timezone) {
    this.timezone = timezone;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */

  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append(super.toString());
    builder.append("scheduled: [").append(scheduled).append("] ");
    builder.append("duration: [").append(duration).append("] ");
    builder.append("timezone: [").append(timezone).append("] ");

    return builder.toString();
  }

}
