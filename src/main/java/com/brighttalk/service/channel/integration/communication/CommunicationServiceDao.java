/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationServiceDao.java 91249 2015-03-06 16:38:40Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.communication;

import java.util.List;

import com.brighttalk.service.channel.integration.communication.dto.CommunicationDto;

/**
 * Interface to define the required methods for the interactions with the Communication Service.
 */
public interface CommunicationServiceDao {

  /**
   * Finds the communication details of the list of supplied communication Ids.
   * 
   * @param commIds List of communication Ids to find.
   * @return List of found communications.
   */
  List<CommunicationDto> findCommunicationsByIds(List<Long> commIds);
}
