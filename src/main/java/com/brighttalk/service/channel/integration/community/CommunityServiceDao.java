/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunityServiceDao.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.community;

import java.util.List;

/**
 * Interface to define the required methods for the interactions with the Community Service.
 */
public interface CommunityServiceDao {

  /**
   * Delete the given channels community settings in the community service.
   * 
   * @param channelId the id of the channel to be deleted
   */
  void deleteChannel( Long channelId );
  
  /**
   * Delete the given communications community settings in the Community Service.
   * 
   * @param channelId the id of the channel to be deleted
   * @param communicationIds the ids of the communications to be deleted
   */
  void deleteCommunications( Long channelId, List<Long> communicationIds );
}