/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelManagerDbDao.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;

/**
 * DAO for database operations on a {@link ChannelManager} instance.
 */
public interface ChannelManagerDbDao {

  /**
   * Creates a given channel manager.
   * 
   * @param channel channel that will receive the new manager.
   * @param channelManager new channel manager.
   * 
   * @return updated channel manager.
   */
  ChannelManager create(Channel channel, ChannelManager channelManager);

  /**
   * Update a given channel manager.
   * 
   * @param channelManager channel manager
   * 
   * @return updated channel manager.
   */
  ChannelManager update(ChannelManager channelManager);

  /**
   * Delete a given channel manager.
   * 
   * @param channelManagerId channel manager id
   */
  void delete(Long channelManagerId);

  /**
   * Delete a given user.
   * 
   * @param userId user id
   */
  void deleteByUser(Long userId);

  /**
   * Finds the active channel manager for given channel id and chanel manager id.
   * 
   * @param channelManagerId id of the channel manager
   * 
   * @return channel manager
   */
  ChannelManager find(Long channelManagerId);

  /**
   * Finds the active channel manager for given channel id and chanel manager id.
   * 
   * @param channelId channel id
   * @param channelManagerId channel manager id
   * 
   * @return channel manager
   */
  ChannelManager findByChannelAndChannelManager(Long channelId, Long channelManagerId);

  /**
   * Finds the active channel manager for given channel id and chanel manager id.
   * 
   * @param channelId the channel id to search for channel managers
   * @param userId id of the BrightTALK user
   * 
   * @return channel manager
   */
  ChannelManager findByChannelAndUser(Long channelId, Long userId);

  /**
   * Find channel managers by the given search criteria.
   * 
   * @param channel the channel to search for channel managers
   * 
   * @return chanel managers
   */
  List<ChannelManager> find(Channel channel);

  /**
   * Load channel managers for the given channel.
   * 
   * @param channel the channel
   */
  void load(Channel channel);

  /**
   * Counts the active channel managers for given channel id.
   * 
   * @param channelId id of the channel
   * 
   * @return number of channel managers
   */
  Integer count(Long channelId);

}
