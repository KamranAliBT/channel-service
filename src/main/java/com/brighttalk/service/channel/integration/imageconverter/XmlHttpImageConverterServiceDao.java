/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2011.
 * All Rights Reserved.
 * $Id: XmlHttpImageConverterServiceDao.java 65528 2013-06-12 16:32:48Z aaugustyn $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.imageconverter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.business.domain.ImageConverterSettings;
import com.brighttalk.service.channel.business.domain.ImageConverterSettingsGenerator;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.imageconverter.dto.TaskDto;
import com.brighttalk.service.channel.integration.imageconverter.dto.converter.TaskConverter;

/**
 * Implementation of a image service Dao that uses RestTempalte to communicate with the image converter Service using
 * XML over Http.
 */
@Component
public class XmlHttpImageConverterServiceDao implements ImageConverterServiceDao {

  private static final Logger LOGGER = Logger.getLogger(XmlHttpImageConverterServiceDao.class);

  /**
   * Image converter request path.
   */
  protected static final String REQUEST_PATH = "/internal/task";

  @Autowired
  @Qualifier(value = "imageConverterService")
  private RestTemplate restTemplate;

  @Value("${service.imageConverterServiceUrl}")
  private String serviceBaseUrl;

  @Autowired
  private TaskConverter taskConverter;

  /**
   * ImageConverterSettingsGenerator.
   */
  @Autowired
  protected ImageConverterSettingsGenerator imageConverterSettingsGenerator;

  /** {@inheritDoc} */
  @Override
  public void processThumbnailImage(final Communication communication) {
    LOGGER.debug("Sending Thumbnail Image conversion request for webcast [" + communication.getId() + "]");

    ImageConverterSettings settings = imageConverterSettingsGenerator.generateForThumbnailImage(communication);

    TaskDto request = taskConverter.convertForThumbnail(communication, settings);
    String requestUrl = serviceBaseUrl + REQUEST_PATH;

    LOGGER.debug("Sending create thumbnail image task details :  " + request);

    restTemplate.postForLocation(requestUrl, request);
  }

  /** {@inheritDoc} */
  @Override
  public void processPreviewImage(final Communication communication) {
    LOGGER.debug("Sending Preview Image conversion request for webcast [" + communication.getId() + "]");

    ImageConverterSettings settings = imageConverterSettingsGenerator.generateForPreviewImage(communication);

    TaskDto request = taskConverter.convertForPreview(communication, settings);
    String requestUrl = serviceBaseUrl + REQUEST_PATH;

    LOGGER.debug("Sending convert summit image task details :  " + request);

    restTemplate.postForLocation(requestUrl, request);
  }

  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public String getServiceBaseUrl() {
    return serviceBaseUrl;
  }

  public void setServiceBaseUrl(final String channelServiceBaseUrl) {
    serviceBaseUrl = channelServiceBaseUrl;
  }

  public TaskConverter getTaskConverter() {
    return taskConverter;
  }

  public void setTaskConverter(final TaskConverter taskConverter) {
    this.taskConverter = taskConverter;
  }
}