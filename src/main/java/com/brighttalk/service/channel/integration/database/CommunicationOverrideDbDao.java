/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationOverrideDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * DAO for database operations on communication overrides.
 */
public interface CommunicationOverrideDbDao {

  /**
   * Load any the communication overrides (if any) for the given list of communications in the given channel.
   * <p>
   * When a communication is syndicated its title, description, authors and keywords (or any combination) can be changed
   * in the channel it has been syndicated into. This method will update the given communications with any overrides
   * they have.
   * <p>
   * If there are no overrides the communications are not affected.
   * <p>
   * If there are overrides the relevant fiels in the communication will be changed (e.g. the communications title will
   * be change if its overridden).
   * 
   * @param channelId the channel id to get the communication overrides for
   * @param syndicatedCommunications the communications to populate with their communication overrides (if any).
   * 
   */
  void loadOverrides(Long channelId, List<Communication> syndicatedCommunications);

  /**
   * Update the communication details override. This is used when updating a syndicated webcast.
   * 
   * @param communication to be updated.
   */
  void updateCommunicationDetailsOverride(Communication communication);

  /**
   * Set the communication override details for a syndicated webcast.
   * 
   * @param communication to load the override details to
   */
  void setCommunicationDetailsOverride(Communication communication);
}
