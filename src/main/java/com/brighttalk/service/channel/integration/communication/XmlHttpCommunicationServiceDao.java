/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpCommunicationServiceDao.java 89882 2015-02-13 09:24:11Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.communication;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.communication.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.communication.dto.CommunicationsDto;
import com.brighttalk.service.channel.integration.communication.dto.SearchDto;
import com.brighttalk.service.channel.integration.communication.dto.SortDto;

/**
 * Implementation of the Dao to interact with the Communication service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
@Component
public class XmlHttpCommunicationServiceDao implements CommunicationServiceDao {

  private static final Logger LOGGER = Logger.getLogger(XmlHttpCommunicationServiceDao.class);

  private static final String FIND_COMMUNICATIONS_API_URI_TEMPLATE = "/internal/communications";
  private static final int FIND_COMMUNICATIONS_OFFSET = 0;
  private static final int FIND_COMMUNICATIONS_LIMIT = 100;
  private static final String FIND_COMMUNICATIONS_SORT_TYPE = "desc";
  private static final String FIND_COMMUNICATIONS_SORT_VALUE = "title";

  @Autowired
  @Qualifier(value = "communicationService")
  private RestTemplate restTemplate;

  @Value("${service.communicationServiceUrl}")
  private String serviceBaseUrl;

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  /** {@inheritDoc}. */
  @Override
  public List<CommunicationDto> findCommunicationsByIds(List<Long> commIds) {
    Validate.notEmpty(commIds, "List of supplied communication Ids must be supplied and not empty.");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Received request to find the details of the following communications [" + commIds + "].");
    }
    String url = serviceBaseUrl + FIND_COMMUNICATIONS_API_URI_TEMPLATE;

    HttpHeaders requestHeaders = new HttpHeaders();
    requestHeaders.setContentType(MediaType.APPLICATION_XML);
    SearchDto searchDto = createCommunicationsSearchRequest(commIds);
    HttpEntity<SearchDto> requestEntity = new HttpEntity<SearchDto>(searchDto, requestHeaders);

    ResponseEntity<CommunicationsDto> responseEntity =
        restTemplate.exchange(url, HttpMethod.POST, requestEntity, CommunicationsDto.class);

    List<CommunicationDto> communications = responseEntity.getBody().getCommunications();
    // Check the retrieved communication response if none reported assign empty communication list.
    if (communications == null) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("All supplied communication Ids didn't identify any existing communications, returning empty list.");
      }
      return new ArrayList<>();
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully found the details of communications [" + communications + "].");
    }
    return communications;
  }

  /**
   * Creates Search communication request.
   * 
   * @param commIds The communication Ids.
   * @return Communication search request.
   */
  private SearchDto createCommunicationsSearchRequest(List<Long> commIds) {
    SearchDto searchDto = new SearchDto();
    searchDto.setOffset(FIND_COMMUNICATIONS_OFFSET);
    searchDto.setLimit(FIND_COMMUNICATIONS_LIMIT);
    searchDto.setSort(new SortDto(FIND_COMMUNICATIONS_SORT_TYPE, FIND_COMMUNICATIONS_SORT_VALUE));
    List<CommunicationDto> communications = new ArrayList<>();
    for (Long commId : commIds) {
      communications.add(new CommunicationDto(commId));
    }
    searchDto.setCommunications(communications);
    return searchDto;
  }
}