/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: DefaultDialInNumberConverter.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking.dto.converter;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.integration.booking.dto.LivePhoneNumberDto;

/**
 * Default implementation of an LivePhoneNumberConverter.
 */
@Component
public class DefaultLivePhoneNumberConverter implements LivePhoneNumberConverter {

  /** {@inheritDoc} */
  @Override
  public String convert(final LivePhoneNumberDto livePhoneNumberDto) {

    return livePhoneNumberDto.getLivePhoneNumber().toString();
  }

}
