/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EnquiryRequestConverter.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.enquiry.dto.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.integration.enquiry.dto.EnquiryRequestDto;
import com.brighttalk.service.channel.integration.enquiry.dto.FormAnswerDto;
import com.brighttalk.service.channel.integration.enquiry.dto.FormDto;

/**
 * Enquiry Request Converter used to create DTO for communicating with the enquiry service.
 */
@Component
public class EnquiryRequestConverter {

  /**
   * A request id required by the enquiry service to identify the request.
   */
  protected static final String ENQUIRY_CONTACT_ID = "create_channel";

  /**
   * Converts the given channel into enquiry request dto.
   * 
   * @param channel to be converted.
   * 
   * @return DTO object.
   */
  public EnquiryRequestDto convert(Channel channel) {

    FormDto formDto = new FormDto();
    formDto.setId(ENQUIRY_CONTACT_ID);
    formDto.setFormAnswers(createFormAnswers(channel));
    
    EnquiryRequestDto requestDto = new EnquiryRequestDto();
    requestDto.setForm(formDto);
    return requestDto;
  }
  
  private List<FormAnswerDto> createFormAnswers(Channel channel) {
    
    //form answer ids need to start from 1 and increment
    Long currentNodeId = 1L;
    List<FormAnswerDto> formAnswers = new ArrayList<FormAnswerDto>();

    FormAnswerDto channelIdNode = new FormAnswerDto();
    channelIdNode.setId(currentNodeId++);
    channelIdNode.setValue(channel.getId().toString());
    formAnswers.add(channelIdNode);

    FormAnswerDto channelTitleNode = new FormAnswerDto();
    channelTitleNode.setId(currentNodeId++);
    channelTitleNode.setValue(channel.getTitle());
    formAnswers.add(channelTitleNode);

    FormAnswerDto channelCreatedTypeIdNode = new FormAnswerDto();
    channelCreatedTypeIdNode.setId(currentNodeId++);
    channelCreatedTypeIdNode.setValue(channel.getCreatedType().getId().toString());
    formAnswers.add(channelCreatedTypeIdNode);
    
    FormAnswerDto channelPendingTypeIdNode = new FormAnswerDto();
    channelPendingTypeIdNode.setId(currentNodeId++);
    channelPendingTypeIdNode.setValue(channel.getPendingType().getId().toString());
    formAnswers.add(channelPendingTypeIdNode);
    
    return formAnswers;
  }
}