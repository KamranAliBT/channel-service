/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcSubscriptionDbDao.java 97779 2015-07-09 13:11:27Z build $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.Subscription.Referral;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;
import com.brighttalk.service.channel.business.error.SubscriptionNotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link SubscriptionDbDao} which uses JDBC via Spring's SimpleJDBCTemplate
 * to access the Database.
 */
@Repository
@Primary
public class JdbcSubscriptionDbDao implements SubscriptionDbDao {

  private static final String ID_COLUMN = "id";

  private static final String CHANNEL_ID_COLUMN = "channel_id";

  private static final String USER_ID_COLUMN = "user_id";

  private static final String IS_ACTIVE_COLUMN = "is_active";

  private static final String LAST_SUBSCRIBED_COLUMN = "last_subscribed";

  private static final String REFERAL_COLUMN = "referal";

  private static final String LEAD_TYPEL_COLUMN = "lead_type";

  private static final String LEAD_CONTEXT_COLUMN = "lead_context";

  private static final String ENGAGEMENT_SCORE_COLUMN = "engagement_score";

  private static final String LAST_UPDATED = "last_updated";

  @Autowired
  private SubscriptionContextDbDao contextDbDao;

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcSubscriptionDbDao.class);

  /* @formatter:off */
  //CHECKSTYLE:OFF
  private static final String SUBSCRIPTION_SELECT_FIELD = ""
      + "  s.id, "
      + "  s.user_id, "
      + "  s.is_active, "
      + "  s.last_subscribed, "
      + "  s.referal, "
      + "  sc.lead_type, "
      + "  sc.lead_context, "
      + "  sc.engagement_score ";

  private static final String SUBSCRIPTION_FROM_CLAUSE = ""
      + "FROM "
      + "  subscription s "
      + "  LEFT JOIN subscription_context sc ON s.id = sc.subscription_id ";

  private static final String MARK_AS_UNSUBSCRIBED_QUERY = ""
      + "UPDATE "
      + "  subscription "
      + "SET "
      + "  is_active = 0, "
      + "  last_updated = NOW() "
      + "WHERE "
      + "  user_id = :" + USER_ID_COLUMN;

  private static final String MARK_USERS_AS_UNSUBSCRIBED_FOR_CHANNEL_QUERY = ""
      + "UPDATE "
      + "  subscription "
      + "SET "
      + "  is_active = 0, "
      + "  last_updated = NOW() "
      + "WHERE "
      + "  channel_id = :" + CHANNEL_ID_COLUMN
      + "  AND user_id IN(:" + USER_ID_COLUMN + ")";

  private static final String SUBSCRIPTIONS_SELECT_BY_CHANNEL_QUERY = ""
      + "SELECT "
      + SUBSCRIPTION_SELECT_FIELD
      + SUBSCRIPTION_FROM_CLAUSE
      + "WHERE "
      + "  s.channel_id = :" + CHANNEL_ID_COLUMN;

  private static final String SUBSCRIPTIONS_SELECT_BY_ID = ""
      + "SELECT "
      + SUBSCRIPTION_SELECT_FIELD
      + SUBSCRIPTION_FROM_CLAUSE
      + "WHERE "
      + "  s.id = :" + ID_COLUMN;

  private static final String SUBSCRIPTION_SELECT_ACTIVE_BY_CHANNEL_AND_USER_QUERY = ""
      + "SELECT "
      + SUBSCRIPTION_SELECT_FIELD
      + SUBSCRIPTION_FROM_CLAUSE
      + "WHERE "
      + "  s.channel_id = :channel_id "
      + "  AND s.user_id = :user_id "
      + "  AND s.is_active = 1";

  private static final String CHANNEL_SUBSCRIPTION_INSERT_QUERY = ""
      + "INSERT INTO subscription ( "
      + "  channel_id, "
      + "  user_id, "
      + "  last_subscribed, "
      + "  referal, "
      + "  created, "
      + "  last_updated"
      + ") "
      + "VALUES ("
      + "  :channel_id, "
      + "  :user_id, "
      + "  NOW(), "
      + "  :referal, "
      + "  NOW(), "
      + "  NOW() "
      + ")";

  private static final String UPDATE_SUBSCRIPTION_REFERRAL_QUERY = ""
      + "UPDATE "
      + " subscription "
      + " SET referal = :" + REFERAL_COLUMN + ","
      + " last_updated = :" + LAST_UPDATED
      + " WHERE id = :" + ID_COLUMN;
  // CHECKSTYLE:ON
  /* @formatter:on */

  /** JDBC template used to access the subscription database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {

    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public void unsubscribeUsers(final Long channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Unsubscribing users from channel [" + channelId + "].");
    }

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  subscription ");
    sql.append("SET ");
    sql.append("  is_active = 0, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  channel_id = :channel_id ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_COLUMN, channelId);

    int noOfUsersUnsubscribed = jdbcTemplate.update(sql.toString(), params);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Unsubscribed " + noOfUsersUnsubscribed + " users from channel [" + channelId + "].");
    }
  }

  /** {@inheritDoc} */
  @Override
  public void unsubscribeUser(final Long channelId, final Long userId) {
    unsubscribeUsers(channelId, Collections.singletonList(userId));
  }

  /** {@inheritDoc} */
  @Override
  public void unsubscribeUsers(final Long channelId, final List<Long> userIds) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Unsubscribing user " + userIds + " from channel [" + channelId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_COLUMN, channelId);
    params.addValue(USER_ID_COLUMN, userIds);

    int noOfunsubscribed = jdbcTemplate.update(MARK_USERS_AS_UNSUBSCRIBED_FOR_CHANNEL_QUERY, params);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Unsubscribed user [" + userIds + "] from channel [" + channelId + "]. "
          + "Number of unsubscribed rows [" + noOfunsubscribed + "]");
    }
  }

  /** {@inheritDoc} */
  @Override
  public void unsubscribeUser(final Long userId) {

    LOGGER.debug("Unsubscribing user [" + userId + "] from all channels.");

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("user_id", userId);

    int noOfunsubscribed = jdbcTemplate.update(MARK_AS_UNSUBSCRIBED_QUERY, params);

    LOGGER.debug("Unsubscribed user [" + userId + "] from all channels. Number of unsubscribed rows ["
        + noOfunsubscribed + "]");
  }

  /** {@inheritDoc} */
  @Override
  public Subscription create(final Subscription subscription, final Channel channel) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Creating subscription [" + subscription + "] in channel [" + channel + "].");
    }

    User user = subscription.getUser();
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_COLUMN, channel.getId());
    params.addValue(USER_ID_COLUMN, user.getId());
    params.addValue(REFERAL_COLUMN, subscription.getReferral().name());

    jdbcTemplate.update(CHANNEL_SUBSCRIPTION_INSERT_QUERY, params);

    Subscription createdSubscription = findByChannelIdAndUserId(channel.getId(), user.getId());

    // Check the subscription context exists and if it does create the subscription context.
    SubscriptionContext subscriptionContext = subscription.getContext();
    if (subscriptionContext != null) {
      subscriptionContext.setSubscriptionId(createdSubscription.getId());
      contextDbDao.create(subscriptionContext);
      createdSubscription.setContext(subscriptionContext);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully created subscription [" + createdSubscription + "].");
    }
    return createdSubscription;
  }

  /** {@inheritDoc} */
  @Override
  public Subscription findByChannelIdAndUserId(final Long channelId, final Long userId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding subscription for channel [" + channelId + "] and user [" + userId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_COLUMN, channelId);
    params.addValue(USER_ID_COLUMN, userId);

    Subscription subscription = null;

    try {
      subscription = jdbcTemplate.queryForObject(SUBSCRIPTION_SELECT_ACTIVE_BY_CHANNEL_AND_USER_QUERY, params,
          new SubscriptionRowMapper());
    } catch (EmptyResultDataAccessException e) {
      throw new SubscriptionNotFoundException();
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found subscription [" + subscription + "].");
    }

    return subscription;
  }

  /** {@inheritDoc} */
  @Override
  public List<Subscription> findByChannelId(final Long channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding subscriptions for channel [" + channelId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_COLUMN, channelId);

    List<Subscription> subscriptions = jdbcTemplate.query(SUBSCRIPTIONS_SELECT_BY_CHANNEL_QUERY, params,
        new SubscriptionRowMapper());

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + subscriptions.size() + "] subscriptions in channel [" + channelId + "].");
    }

    return subscriptions;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Subscription findById(final Long id) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding subscription by Id [" + id + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(ID_COLUMN, id);

    Subscription subscription;
    try {
      subscription = jdbcTemplate.queryForObject(SUBSCRIPTIONS_SELECT_BY_ID, params,
          new SubscriptionRowMapper());
    } catch (EmptyResultDataAccessException e) {
      throw new SubscriptionNotFoundException();
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully found subscription [" + subscription + "].");
    }
    return subscription;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void updateReferral(final Referral referral, final Long subscriptionId) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updating the referral [" + referral + "] of the identified subscription [" + subscriptionId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(REFERAL_COLUMN, referral.name());
    params.addValue(ID_COLUMN, subscriptionId);
    params.addValue(LAST_UPDATED, new Date());

    int updateCount = jdbcTemplate.update(UPDATE_SUBSCRIPTION_REFERRAL_QUERY, params);

    if (updateCount == 1) {
      LOGGER.debug("Successfully updated the referral of the identified subscription.");
    }
  }

  /**
   * Default implementation for the subscription row mapper.
   */
  private final class SubscriptionRowMapper implements RowMapper<Subscription> {

    /** {@inheritDoc} */
    @Override
    public Subscription mapRow(final ResultSet rs, final int rowNum) throws SQLException {

      Subscription subscription = getSubscription(rs);
      subscription.setUser(getUser(rs));
      subscription.setContext(getContext(rs));

      return subscription;
    }

    private Subscription getSubscription(final ResultSet rs) throws SQLException {
      Subscription subscription = new Subscription();
      subscription.setId(rs.getLong(ID_COLUMN));
      subscription.setReferral(Referral.valueOf(rs.getString(REFERAL_COLUMN)));
      subscription.setLastSubscribed(rs.getTimestamp(LAST_SUBSCRIBED_COLUMN));
      subscription.setActive(rs.getBoolean(IS_ACTIVE_COLUMN));
      return subscription;
    }

    private User getUser(final ResultSet rs) throws SQLException {
      User user = new User();
      user.setId(rs.getLong(USER_ID_COLUMN));
      return user;
    }

    private SubscriptionContext getContext(final ResultSet rs) throws SQLException {
      String leadType = rs.getString(LEAD_TYPEL_COLUMN);
      String leadContext = rs.getString(LEAD_CONTEXT_COLUMN);
      String engagementScore = rs.getString(ENGAGEMENT_SCORE_COLUMN);

      SubscriptionContext context = null;
      if (StringUtils.isNotBlank(leadType)) {
        context = new SubscriptionContext();
        context.setLeadType(SubscriptionLeadType.getType(leadType));
        context.setLeadContext(leadContext);
        context.setEngagementScore(engagementScore);
      }

      return context;
    }
  }
}