/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 85869 2014-11-11 11:17:11Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * CommunicationDto.
 */
public class CommunicationDto {

  private Long id;

  /**
   * @return the id
   */
  @XmlAttribute
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("id: [").append(id).append("] ");

    return builder.toString();
  }
}
