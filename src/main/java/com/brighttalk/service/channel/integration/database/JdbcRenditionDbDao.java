/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationDbDao.java 83838 2014-09-24 11:10:48Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link RenditionDbDao} which uses JDBC via Spring's SimpleJDBCTemplate to
 * access the Database.
 */
@Repository
@Primary
public class JdbcRenditionDbDao implements RenditionDbDao {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcRenditionDbDao.class);

  private static final String COLUMN_ID = "id";

  private static final String COLUMN_IS_ACTIVE = "is_active";

  private static final String COLUMN_URL = "url";

  private static final String COLUMN_CONTAINER = "container";

  private static final String COLUMN_CODECS = "codecs";

  private static final String COLUMN_WIDTH = "width";

  private static final String COLUMN_BITRATE = "bitrate";

  private static final String COLUMN_COMMUNICATION_ID = "communicationId";

  private static final String COLUMN_RENDITION_ID = "renditionId";

  /* @formatter:off */
  /**
   * Create rendition query.
   */
  private static final String CREATE_RENDITION = "INSERT INTO " +
      " `communication_rendition_asset` " +
      "SET " +
      " url = :url, " + 
      " container = :container, " + 
      " codecs = :codecs, " + 
      " width = :width, " + 
      " bitrate = :bitrate, " + 
      " is_active = 1, " + 
      " created = NOW(), " + 
      " last_updated = NOW() ";
  
  /**
   * Create rendition asset query.
   */
  private static final String CREATE_RENDITION_ASSET = "INSERT INTO " +
      " `communication_asset` " +
      "SET " +
      " communication_id = :communicationId, " + 
      " target_id = :renditionId, " + 
      " type = 'video', " + 
      " is_active = 1, " + 
      " created = NOW(), " + 
      " last_updated = NOW() ";
  
  /**
   * Create rendition asset query.
   */
  private static final String DELETE_RENDITION_ASSET = "UPDATE " +
      " `communication_asset` " +
      "SET " +
      " is_active = 0, " + 
      " last_updated = NOW() " + 
      "WHERE " +
      " communication_id = :communicationId " +
      " AND type = 'video'";
  
  /**
   * Delete rendition query.
   */
  private static final String DELETE_RENDITION = "UPDATE " +
      " `communication_rendition_asset` " +
      "SET " +
      " is_active = 0 " +
      "WHERE " +
      " id = :renditionId;";

  /**
   * Find single rendition.
   */
  private static final String FIND_RENDITION_BY_ID = "SELECT " +
      " id, " +
      " url, " +
      " container, " +
      " codecs, " +
      " width, " +
      " bitrate, " +
      " is_active, " +
      " created, " +
      " last_updated " +
      "FROM  " +
      " `communication_rendition_asset` r  " +
      "WHERE " +
      " r.id = :renditionId";
  
  /**
   * Find renditions by communication id.
   */
  private static final String FIND_RENDITION_BY_COMMUNICATION_ID = "SELECT " +
      " cra.id, " +
      " ca.type, " +
      " cra.is_active, " +
      " cra.url, " +
      " cra.container, " +
      " cra.codecs, " +
      " cra.width, " +
      " cra.bitrate, " +
      " cra.created, " +
      " cra.last_updated " +
      "FROM  " +
      " `communication_asset` ca " +
      "JOIN `communication_rendition_asset` cra ON  " +
      "   (ca.target_id = cra.id) " +
      "WHERE  " +
      " ca.communication_id = :communicationId " +
      "AND " +
      " ca.type = 'video' " +
      "AND " +
      " ca.is_active = 1  " +
      "AND " +
      " cra.is_active = 1";
  
  /**
   * Find renditions by communication id.
   */
  private static final String FIND_RENDITION_BY_COMMUNICATION_IDS = "SELECT " +
      " cra.id, " +
      " ca.type, " +
      " cra.is_active, " +
      " cra.url, " +
      " cra.container, " +
      " cra.codecs, " +
      " cra.width, " +
      " cra.bitrate, " +
      " cra.created, " +
      " cra.last_updated " +
      "FROM  " +
      " `communication_asset` ca " +
      "JOIN `communication_rendition_asset` cra ON  " +
      "   (ca.target_id = cra.id) " +
      "WHERE  " +
      " ca.communication_id IN (:communicationIds) " +
      "AND " +
      " ca.type = 'video' " +
      "AND " +
      " ca.is_active = 1  " +
      "AND " +
      " cra.is_active = 1";
  
  /* @formatter:on */

  private NamedParameterJdbcTemplate jdbcTemplate;

  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Rendition find(final Long renditionId) throws NotFoundException {
    LOGGER.debug("Finding single rendition by renditionId.");

    final MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_RENDITION_ID, renditionId);

    Rendition rendition = jdbcTemplate.queryForObject(FIND_RENDITION_BY_ID, params, new RenditionRowMapper());

    return rendition;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Rendition> findByCommunicationId(final Long communicationId) {
    LOGGER.debug("Finding renditions by communicationId.");

    final MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_COMMUNICATION_ID, communicationId);

    List<Rendition> renditions = jdbcTemplate.query(FIND_RENDITION_BY_COMMUNICATION_ID, params,
        new RenditionRowMapper());

    return renditions;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Rendition> findByCommunicationIds(final List<Long> communicationIds) {
    LOGGER.debug("Finding renditions by communicationIds.");

    final MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("communicationIds", communicationIds);

    List<Rendition> renditions = jdbcTemplate.query(FIND_RENDITION_BY_COMMUNICATION_IDS, params,
        new RenditionRowMapper());

    return renditions;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Rendition create(final Rendition rendition) {
    LOGGER.debug("Creating rendition.");

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("url", rendition.getUrl());
    params.addValue("container", rendition.getContainer());
    params.addValue("codecs", rendition.getCodecs());
    params.addValue("width", rendition.getWidth());
    params.addValue("bitrate", rendition.getBitrate());

    final KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(CREATE_RENDITION, params, generatedKeyHolder);

    Long renditionId = generatedKeyHolder.getKey().longValue();
    rendition.setId(renditionId);

    LOGGER.debug("Rendition successfully created with id [" + renditionId + "].");
    return rendition;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void delete(final Long renditionId) {
    LOGGER.debug("Deleting rendition [" + renditionId + "] .");

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_RENDITION_ID, renditionId);

    int noOfRows = jdbcTemplate.update(DELETE_RENDITION, params);

    if (noOfRows != 1) {
      throw new ApplicationException("Could not delete rendition [" + renditionId + "]");
    }

    LOGGER.debug("Rendition [" + renditionId + "] successfully deleted.");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void addRenditionAsset(final Long communicationId, final Long renditionId) {
    LOGGER.debug("Creating rendition asset for communication [" + communicationId + "] and rendition [" + renditionId
        + "].");

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_COMMUNICATION_ID, communicationId);
    params.addValue(COLUMN_RENDITION_ID, renditionId);

    jdbcTemplate.update(CREATE_RENDITION_ASSET, params);

    LOGGER.debug("Rendition asset successfully created for communication [" + communicationId + "] and rendition ["
        + renditionId + "].");
  }

  /** {@inheritDoc} */
  @Override
  public void removeRenditionAssets(final Long communicationId) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COLUMN_COMMUNICATION_ID, communicationId);

    jdbcTemplate.update(DELETE_RENDITION_ASSET, params);
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Rendition}.
   */
  private class RenditionRowMapper implements RowMapper<Rendition> {

    /**
     * {@inheritDoc}
     * 
     * @see "http://docs.oracle.com/javase/1.4.2/docs/api/java/sql/ResultSet.html#getLong%28java.lang.String%29"
     */
    @Override
    public Rendition mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Rendition rendition = new Rendition();
      rendition.setId(resultSet.getLong(COLUMN_ID));
      rendition.setIsActive(resultSet.getBoolean(COLUMN_IS_ACTIVE));
      rendition.setUrl(resultSet.getString(COLUMN_URL));
      rendition.setContainer(resultSet.getString(COLUMN_CONTAINER));
      rendition.setCodecs(resultSet.getString(COLUMN_CODECS));
      rendition.setWidth(resultSet.getLong(COLUMN_WIDTH));
      rendition.setBitrate(resultSet.getLong(COLUMN_BITRATE));
      return rendition;
    }
  }

}