/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpEmailServiceDao.java 88525 2015-01-20 12:15:30Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.email;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.email.dto.EmailRequestDto;
import com.brighttalk.service.channel.integration.email.dto.converter.EmailRequestConverter;

/**
 * Implementation of the Dao to interact with the Email service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
@Component
public class XmlHttpEmailServiceDao implements EmailServiceDao {

  private static final Logger logger = Logger.getLogger(XmlHttpEmailServiceDao.class);

  /**
   * Send email request path.
   */
  protected static final String REQUEST_PATH = "/internal/xml/send";

  @Autowired
  @Qualifier(value = "emailService")
  private RestTemplate restTemplate;

  @Value("${service.emailServiceUrl}")
  private String serviceBaseUrl;

  @Autowired
  private EmailRequestConverter converter;

  /** {@inheritDoc} */
  @Override
  public void sendChannelCreateConfirmation(final Channel channel) {

    if (logger.isDebugEnabled()) {
      logger.debug("Sending request to send channel confirmation email for channel [" + channel.getId() + "].");
    }

    EmailRequestDto request = converter.convert(channel);
    String url = serviceBaseUrl + REQUEST_PATH;

    if (logger.isDebugEnabled()) {
      logger.debug("Sending channel creation email request [" + request + "] using URL [" + url + "].");
    }

    restTemplate.postForLocation(url, request);
  }

  /** {@inheritDoc} */
  @Override
  public void sendCommunicationCreateConfirmation(final Communication communication) {
    if (logger.isDebugEnabled()) {
      logger.debug("Sending communication create confirmation email for communication [" + communication.getId() + "].");
    }

    EmailRequestDto request = converter.convertForCreate(communication);
    String url = serviceBaseUrl + REQUEST_PATH;

    if (logger.isDebugEnabled()) {
      logger.debug("Sending communication creation email request [" + request + "] using URL [" + url + "].");
    }

    restTemplate.postForLocation(url, request);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendCommunicationRescheduleConfirmation(final List<Communication> communications) {
    if (logger.isDebugEnabled()) {
      logger.debug("Sending communication reschedule confirmation email.");
    }

    for (Communication communication : communications) {
      EmailRequestDto request = converter.convertForUpdate(communication);
      String url = serviceBaseUrl + REQUEST_PATH;

      if (logger.isDebugEnabled()) {
        logger.debug("Sending communication reschedule email request [" + request + "] using URL [" + url + "].");
      }

      restTemplate.postForLocation(url, request);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendCommunicationCancelConfirmation(final List<Communication> communications) {
    if (logger.isDebugEnabled()) {
      logger.debug("Sending communication cancel confirmation email.");
    }

    for (Communication communication : communications) {
      EmailRequestDto request = converter.convertForCancel(communication);
      String url = serviceBaseUrl + REQUEST_PATH;

      if (logger.isDebugEnabled()) {
        logger.debug("Sending communication cancelled email request [" + request + "] using URL [" + url + "].");
      }

      restTemplate.postForLocation(url, request);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendCommunicationMissedYouConfirmation(final List<Communication> communications) {
    if (logger.isDebugEnabled()) {
      logger.debug("Sending communication missed you confirmation email.");
    }

    for (Communication communication : communications) {
      EmailRequestDto request = converter.convertForMissedYou(communication);
      String url = serviceBaseUrl + REQUEST_PATH;

      if (logger.isDebugEnabled()) {
        logger.debug("Sending communication missed you email request [" + request + "] using URL [" + url + "].");
      }

      restTemplate.postForLocation(url, request);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendCommunicationRecordingPublishedConfirmation(final Communication communication) {
    if (logger.isDebugEnabled()) {
      logger.debug("Sending communication recording published confirmation email.");
    }

    EmailRequestDto request = converter.convertForRecordingPublished(communication);
    String url = serviceBaseUrl + REQUEST_PATH;

    if (logger.isDebugEnabled()) {
      logger.debug("Sending communication recording published email request [" + request + "] using URL [" + url + "].");
    }

    restTemplate.postForLocation(url, request);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendVideoUploadError(final Communication communication) {
    if (logger.isDebugEnabled()) {
      logger.debug("Sending video upload error email for communication [" + communication.getId() + "].");
    }

    EmailRequestDto request = converter.convertForVideoUploadError(communication);
    String url = serviceBaseUrl + REQUEST_PATH;

    if (logger.isDebugEnabled()) {
      logger.debug("Sending video upload error email request [" + request + "] using URL [" + url + "].");
    }

    restTemplate.postForLocation(url, request);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendVideoUploadComplete(final Communication communication) {
    if (logger.isDebugEnabled()) {
      logger.debug("Sending video upload complete confirmation email for communication [" + communication.getId()
          + "].");
    }

    EmailRequestDto request = converter.convertForVideoUploadComplete(communication);
    String url = serviceBaseUrl + REQUEST_PATH;

    if (logger.isDebugEnabled()) {
      logger.debug("Sending video upload complete email request [" + request + "] using URL [" + url + "].");
    }

    restTemplate.postForLocation(url, request);
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public void setConverter(final EmailRequestConverter converter) {
    this.converter = converter;
  }

}
