/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SubscriptionDbDao.java 97779 2015-07-09 13:11:27Z build $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.Subscription;
import com.brighttalk.service.channel.business.domain.channel.Subscription.Referral;

/**
 * DAO for database operations related with subscription data.
 */
public interface SubscriptionDbDao {

  /**
   * Unsubscribe all Users from a given Channel.
   * 
   * @param channelId id of the channel to unsubcribed all the users from.
   */
  void unsubscribeUsers(Long channelId);

  /**
   * Unsubscribes a User from a given Channel.
   * 
   * @param channelId id of the channel to unsubcribed the user from
   * @param userId id of the user to unsubcribed
   */
  void unsubscribeUser(Long channelId, Long userId);

  /**
   * Unsubscribes users from a given Channel.
   * 
   * @param channelId id of the channel to unsubcribed the user from
   * @param userIds id of the users to unsubcribed
   */
  void unsubscribeUsers(Long channelId, List<Long> userIds);

  /**
   * Unsubscribes a Users from all Channels.
   * 
   * @param userId id of the user to unsubcribed
   */
  void unsubscribeUser(Long userId);

  /**
   * Finds the subscriptions associated with the identified channel.
   * 
   * @param channelId Channel to find subscriptions for.
   * 
   * @return Subscriptions the identified channel.
   */
  List<Subscription> findByChannelId(Long channelId);

  /**
   * Finds the subscription by channel and user IDs.
   * 
   * @param channelId the channel ID to find by
   * @param userId the user ID to find by
   * 
   * @return the subscription
   */
  Subscription findByChannelIdAndUserId(Long channelId, Long userId);

  /**
   * Find Subscription Id.
   * 
   * @param id The subscription Id.
   * @return The found {@link Subscription}.
   */
  Subscription findById(Long id);

  /**
   * Creates the subscription.
   * 
   * @param subscription the subscription details
   * @param channel the channel to subscribe to
   * 
   * @return the create subscription
   */
  Subscription create(Subscription subscription, Channel channel);

  /**
   * Updates the subscription referral.
   * 
   * @param referral The subscription referral to update.
   * @param subscriptionId The id of the subscription in which the referral is updated.
   */
  void updateReferral(Referral referral, Long subscriptionId);

}