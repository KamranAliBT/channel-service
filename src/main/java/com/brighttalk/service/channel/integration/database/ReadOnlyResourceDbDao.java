/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2009-2013.
 * All Rights Reserved.
 * $Id: ReadOnlyResourceDbDao.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import com.brighttalk.service.channel.business.domain.Resource;

/**
 * Read only implementation of the ResourceDbDao.
 */
@Repository(value = "readOnlyResourceDbDao")
public class ReadOnlyResourceDbDao extends JdbcResourceDbDao {

  /**
   * Set the Datasource - Autowired to be the read only data source.
   * 
   * @param dataSource The Read only data source
   */
  @Override
  @Autowired
  public void setDataSource(@Qualifier("channelReadOnlyDataSource") DataSource dataSource) {
    this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * Create a Resource.
   * 
   * @param communicationId The id of the communication that
   * @param resource The Resource to create
   * @return The updated resource
   * @throws UnsupportedOperationException Thrown as create cannot be done with a read only DAO
   */
  @Override
  public Resource create(Long communicationId, Resource resource) {
    throw new UnsupportedOperationException("Cannot create a Resource with a Readonly Connection");
  }

  /**
   * Update a Resource.
   * 
   * @param resource The Resource to Updated
   * @return The Updated Resource
   * @throws UnsupportedOperationException Thrown as update cannot be done with a read only DAO
   */
  @Override
  public Resource update(Resource resource) {
    throw new UnsupportedOperationException("Cannot update a Resource with a Readonly Connection");
  }

  /**
   * Update the Order of Resources.
   * 
   * @param resources The Resources to update - in order.
   * @throws UnsupportedOperationException Thrown as update cannot be done with a read only DAO
   */
  @Override
  public void updateOrder(List<Resource> resources) {
    throw new UnsupportedOperationException("Cannot update the order of a Resource with a Readonly Connection");
  }

  /**
   * Delete a Resource form a communication.
   * 
   * @param communicationId The Id of the communication.
   * @param resourceId The Id of the rsource to delete
   * @throws UnsupportedOperationException Thrown as delete cannot be done with a read only DAO
   */
  @Override
  public void delete(Long communicationId, Long resourceId) {
    throw new UnsupportedOperationException("Cannot delete a Resource with a Readonly Connection");
  }
}