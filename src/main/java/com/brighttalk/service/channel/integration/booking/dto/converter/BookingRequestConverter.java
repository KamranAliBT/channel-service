/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: BookingRequestConverter.java 83838 2014-09-24 11:10:48Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking.dto.converter;

import java.util.List;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.booking.dto.HDBookingDto;
import com.brighttalk.service.channel.integration.booking.dto.BookingRequestDto;

/**
 * Interface for Booking Request converters - used to convert collections of communication Objects to Booking Request
 * Dto objects.
 */
public interface BookingRequestConverter {

  /**
   * Convert the given collection of communications into a DTO.
   * 
   * @param communicationIds a collection of communication ids
   * 
   * @return DTO object
   */
  BookingRequestDto convert(List<Long> communicationIds);

  /**
   * Convert the given communication into a DTO.
   * 
   * @param communication the communication to be converted
   * 
   * @return DTO object
   */
  HDBookingDto convertForCreateAndUpdate(Communication communication);

}