/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcSurveyDbDao.java 70933 2013-11-15 10:33:42Z msmith $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.Survey;

/**
 * Spring JDBC Database DAO implementation of {@link SurveyDbDao} which uses JDBC via Spring's SimpleJDBCTemplate to
 * access the Database.
 */
@Repository
@Primary
public class JdbcSurveyDbDao extends JdbcDao implements SurveyDbDao {

  /** Logger for this class */
  protected static final Logger logger = Logger.getLogger(JdbcSurveyDbDao.class);

  /** JDBC template used to access the database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Set the datasource on this DAO.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public Survey findChannelSurvey(Long surveyId) {

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  a.channel_id AS channelId, ");
    sql.append("  a.target_id AS surveyId, ");
    sql.append("  a.is_active AS isActive ");
    sql.append("FROM ");
    sql.append("  asset a ");
    sql.append("WHERE ");
    sql.append("  a.target_id = :targetId ");
    sql.append("  AND a.type = 'survey' ");
    // CHECKSTYLE:ON

    params.addValue("targetId", surveyId);

    Survey survey = null;
    try {
      survey = jdbcTemplate.queryForObject(sql.toString(), params,new SurveyRowMapper());
    } catch (EmptyResultDataAccessException e) {
      return null;
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Found channel suvey [" + survey + "].");
    }

    return survey;
  }

  @Override
  public Survey findCommunicationSurvey(Long surveyId) {
    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  cc.channel_id AS channelId, ");
    sql.append("  cc.survey_id AS surveyId, ");
    sql.append("  cc.survey_active AS isActive ");
    sql.append("FROM ");
    sql.append("  communication_configuration cc ");
    sql.append("WHERE ");
    sql.append("  cc.survey_id = :surveyId ");
    // CHECKSTYLE:ON

    params.addValue("surveyId", surveyId);

    Survey survey = null;
    try {
      survey = jdbcTemplate.queryForObject(sql.toString(), params, new SurveyRowMapper());
    } catch (EmptyResultDataAccessException e) {
      return null;
    }

    if (logger.isDebugEnabled()) {
      logger.debug("Found communication survey [" + survey + "].");
    }

    return survey;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Survey}.
   */
  private class SurveyRowMapper implements RowMapper<Survey> {

    /**
     * {@inheritDoc}
     * 
     * @see "http://docs.oracle.com/javase/1.4.2/docs/api/java/sql/ResultSet.html#getLong%28java.lang.String%29"
     */
    @Override
    public Survey mapRow(ResultSet resultSet, int rowNum) throws SQLException {

      Long surveyId = resultSet.getLong("surveyId");
      Long channelId = resultSet.getLong("channelId");
      Boolean isActive = resultSet.getBoolean("isActive");
      Survey survey = new Survey(surveyId, channelId, isActive);

      return survey;
    }
  }
}
