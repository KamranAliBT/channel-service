/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcChannelManagerDbDao.java 55023 2012-10-01 10:22:47Z afernandes $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.common.user.User;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.channel.ChannelManager;
import com.brighttalk.service.channel.business.error.ChannelManagerNotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link ChannelManagerDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database.
 */
@Repository
@Primary
public class JdbcChannelManagerDbDao extends JdbcDao implements ChannelManagerDbDao {

  private static final String ID_FIELD = "id";

  private static final String CHANNEL_ID_FIELD = "channel_id";

  private static final String USER_ID_FIELD = "user_id";

  private static final String EMAIL_ALERTS_FIELD = "email_alerts";

  /* @formatter:off */
  //CHECKSTYLE:OFF
  private static final String CHANNEL_MANAGER_INSERT_QUERY = ""
      + "INSERT INTO channel_manager ( "
      + "  channel_id, "
      + "  user_id, "
      + "  email_alerts, "
      + "  is_active, "
      + "  created, "
      + "  last_updated "
      + ") "
      + "VALUES ( "
      + "  :channel_id, "
      + "  :user_id, "
      + "  :email_alerts, "
      + "  1, "
      + "  NOW(), "
      + "  NOW() "
      + ") "
      + "ON DUPLICATE KEY UPDATE "
      + "  email_alerts = VALUES(email_alerts), "
      + "  is_active = VALUES(is_active), "
      + "  last_updated = VALUES(last_updated)";

  private static final String CHANNEL_MANAGER_UPDATE_QUERY = ""
      + "UPDATE "
      + "  channel_manager "
      + "SET "
      + "  email_alerts = :email_alerts, "
      + "  last_updated = NOW() "
      + "WHERE "
      + "  id = :id ";

  private static final String CHANNEL_MANAGER_DELETE_QUERY = ""
      + "UPDATE "
      + "  channel_manager "
      + "SET "
      + "  is_active = 0, "
      + "  last_updated = NOW() "
      + "WHERE "
      + "  id = :id ";

  private static final String CHANNEL_MANAGER_DELETE_BY_USER_QUERY = ""
      + "UPDATE "
      + "  channel_manager "
      + "SET "
      + "  is_active = 0, "
      + "  last_updated = NOW() "
      + "WHERE "
      + "  user_id = :user_id ";

  private static final String CHANNEL_MANAGER_SELECT_BY_ID_QUERY = ""
      + "SELECT "
      + "  id, "
      + "  channel_id, "
      + "  user_id, "
      + "  email_alerts "
      + "FROM "
      + "  channel_manager "
      + "WHERE "
      + "  id = :id "
      + "  AND is_active = 1";

  private static final String CHANNEL_MANAGER_SELECT_BY_CHANNEL_AND_USER_QUERY = ""
      + "SELECT "
      + "  id, "
      + "  channel_id, "
      + "  user_id, "
      + "  email_alerts "
      + "FROM "
      + "  channel_manager "
      + "WHERE "
      + "  channel_id = :channel_id "
      + "  AND user_id = :user_id "
      + "  AND is_active = 1";

  private static final String CHANNEL_MANAGER_SELECT_BY_CHANNEL_AND_CHANNEL_MANAGER_QUERY = ""
      + "SELECT "
      + "  id, "
      + "  channel_id, "
      + "  user_id, "
      + "  email_alerts "
      + "FROM "
      + "  channel_manager "
      + "WHERE "
      + "  channel_id = :channel_id "
      + "  AND id = :id "
      + "  AND is_active = 1";

  private static final String CHANNEL_MANAGERS_SELECT_QUERY = ""
      + "SELECT "
      + "  id, "
      + "  channel_id, "
      + "  user_id, "
      + "  email_alerts "
      + "FROM "
      + "  channel_manager "
      + "WHERE "
      + "  channel_id = :channel_id "
      + "  AND is_active = 1 ";

  private static final String CHANNEL_MANAGER_COUNT_QUERY = ""
      + "SELECT "
      + "  COUNT(*) "
      + "FROM "
      + "  channel_manager "
      + "WHERE "
      + "  channel_id = :channel_id "
      + "  AND is_active = 1";
  // CHECKSTYLE:ON
  /* @formatter:on */

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcChannelManagerDbDao.class);

  /** Spring JDBC Template */
  protected NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public ChannelManager create(final Channel channel, final ChannelManager channelManager) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_FIELD, channel.getId());
    params.addValue(USER_ID_FIELD, channelManager.getUser().getId());
    params.addValue(EMAIL_ALERTS_FIELD, channelManager.getEmailAlerts());

    jdbcTemplate.update(CHANNEL_MANAGER_INSERT_QUERY, params);

    return findByChannelAndUser(channel.getId(), channelManager.getUser().getId());
  }

  /** {@inheritDoc} */
  @Override
  public ChannelManager update(final ChannelManager channelManager) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(ID_FIELD, channelManager.getId());
    params.addValue(EMAIL_ALERTS_FIELD, channelManager.getEmailAlerts());

    jdbcTemplate.update(CHANNEL_MANAGER_UPDATE_QUERY, params);

    return find(channelManager.getId());
  }

  /** {@inheritDoc} */
  @Override
  public void delete(final Long channelManagerId) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(ID_FIELD, channelManagerId);

    jdbcTemplate.update(CHANNEL_MANAGER_DELETE_QUERY, params);
  }

  /** {@inheritDoc} */
  @Override
  public void deleteByUser(final Long userId) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(USER_ID_FIELD, userId);

    jdbcTemplate.update(CHANNEL_MANAGER_DELETE_BY_USER_QUERY, params);
  }

  /** {@inheritDoc} */
  @Override
  public ChannelManager find(final Long channelManagerId) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(ID_FIELD, channelManagerId);

    ChannelManager channelManager = null;

    try {

      channelManager = jdbcTemplate.queryForObject(CHANNEL_MANAGER_SELECT_BY_ID_QUERY, params,
          new ChannelManagerRowMapper());

    } catch (EmptyResultDataAccessException e) {
      LOGGER.debug("Channel manager [" + channelManagerId + "] not found.");

      throw new ChannelManagerNotFoundException(channelManagerId, e);
    }

    return channelManager;
  }

  /** {@inheritDoc} */
  @Override
  public ChannelManager findByChannelAndUser(final Long channelId, final Long userId) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_FIELD, channelId);
    params.addValue(USER_ID_FIELD, userId);

    ChannelManager channelManager = null;

    try {

      channelManager = jdbcTemplate.queryForObject(CHANNEL_MANAGER_SELECT_BY_CHANNEL_AND_USER_QUERY, params,
          new ChannelManagerRowMapper());

    } catch (EmptyResultDataAccessException e) {
      LOGGER.debug("Channel manager not found for channel [" + channelId + "] and user [" + userId + "].");

      throw new ChannelManagerNotFoundException(userId, e);
    }

    return channelManager;
  }

  /** {@inheritDoc} */
  @Override
  public ChannelManager findByChannelAndChannelManager(final Long channelId, final Long channelManagerId) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_FIELD, channelId);
    params.addValue(ID_FIELD, channelManagerId);

    ChannelManager channelManager = null;

    try {

      channelManager = jdbcTemplate.queryForObject(CHANNEL_MANAGER_SELECT_BY_CHANNEL_AND_CHANNEL_MANAGER_QUERY, params,
          new ChannelManagerRowMapper());

    } catch (EmptyResultDataAccessException e) {
      LOGGER.debug("Channel manager not found for channel [" + channelId + "] and user [" + channelManagerId + "].");

      throw new ChannelManagerNotFoundException(channelManagerId, e);
    }

    return channelManager;
  }

  /** {@inheritDoc} */
  @Override
  public List<ChannelManager> find(final Channel channel) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_FIELD, channel.getId());

    List<ChannelManager> channelManagers = jdbcTemplate.query(CHANNEL_MANAGERS_SELECT_QUERY, params,
        new ChannelManagerRowMapper());

    return channelManagers;
  }

  /** {@inheritDoc} */
  @Override
  public void load(final Channel channel) {

    List<ChannelManager> channelManagers = find(channel);

    channel.setChannelManagers(channelManagers);
  }

  /** {@inheritDoc} */
  @Override
  public Integer count(final Long channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Counting channel managers for channel [" + channelId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_FIELD, channelId);

    Integer numberOfChannelManagers;

    try {
      numberOfChannelManagers = queryForInt(jdbcTemplate, CHANNEL_MANAGER_COUNT_QUERY, params);
    } catch (EmptyResultDataAccessException e) {
      numberOfChannelManagers = 0;
    }

    return numberOfChannelManagers;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link ChannelManager}.
   */
  private class ChannelManagerRowMapper implements RowMapper<ChannelManager> {

    /** {@inheritDoc} */
    @Override
    public ChannelManager mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      User user = new User();
      user.setId(resultSet.getLong(USER_ID_FIELD));

      Channel channel = new Channel();
      channel.setId(resultSet.getLong(CHANNEL_ID_FIELD));

      ChannelManager channelManager = new ChannelManager();
      channelManager.setId(resultSet.getLong(ID_FIELD));
      channelManager.setUser(user);
      channelManager.setChannel(channel);
      channelManager.setEmailAlerts(resultSet.getBoolean(EMAIL_ALERTS_FIELD));

      return channelManager;
    }
  }

}
