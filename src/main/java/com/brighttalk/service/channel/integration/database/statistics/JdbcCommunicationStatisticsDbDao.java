/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationStatisticsDbDao.java 98706 2015-07-31 13:38:59Z ssarraj $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database.statistics;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.error.NotFoundException;
import com.brighttalk.service.channel.integration.database.JdbcDao;

/**
 * Spring JDBC Database DAO implementation of {@link CommunicationStatisticsDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database.
 */
@Repository
@Primary
public class JdbcCommunicationStatisticsDbDao extends JdbcDao implements CommunicationStatisticsDbDao {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcCommunicationStatisticsDbDao.class);
  private static final String COMMUNICATION_ID_COLUMN_NAME = "communication_id";
  private static final String CHANNEL_ID_COLUMN_NAME = "channel_id";
  private static final String CHANNEL_IDS_PARAM_NAME = "channelIds";
  private static final String COMMUNICATION_ID_PARAM_NAME = "communicationId";
  private static final String COMMUNICATION_IDs_PARAM_NAME = "communicationIds";
  private static final String CHANNEL_ID_PARAM_NAME = "channelId";
  private static final String AVERAGE_RATING_PARAM_NAME = "averageRating";
  private static final String NUMBER_OF_RATINGS_PARAM_NAME = "numberOfRatings";
  private static final String NUMBER_OF_VIEWS_PARAM_NAME = "numberOfViews";
  private static final String TOTAL_VIEWING_DURATION_PARAM_NAME = "totalViewingDuration";
  private static final String REGISTRATION_PARAM_NAME = "registrations";
  private static final String COMMA_SEPARATOR = ", ";
  private static final String SQL_SELECT = "SELECT ";

  /* @formatter:off */
  /** Communication viewings statistics select fields. */
  private static final String COMMUNICATION_STATISTICS_VIEWINGS_FIELDS = ""
    + " cs.communication_id AS " + COMMUNICATION_ID_PARAM_NAME + COMMA_SEPARATOR
    + " cs.channel_id AS " + CHANNEL_ID_PARAM_NAME + COMMA_SEPARATOR
    + " cs.rating AS " + AVERAGE_RATING_PARAM_NAME + COMMA_SEPARATOR
    + " cs.num_ratings AS " + NUMBER_OF_RATINGS_PARAM_NAME + COMMA_SEPARATOR
    + " cs.num_views AS " + NUMBER_OF_VIEWS_PARAM_NAME + COMMA_SEPARATOR
    + " cs.viewed_for AS " + TOTAL_VIEWING_DURATION_PARAM_NAME;

  /** Communication registrations total select fields. */
  private static final String COMMUNICATION_STATISTICS_REGISTRATIONS_FIELDS = ""
    + "  COUNT(cr.communication_id) AS " + REGISTRATION_PARAM_NAME;

  private static final String FROM_COMM_STAT_REG_CLAUSE = ""
    + " FROM "
    + "   communication_statistics cs"
    + " LEFT JOIN communication_registration cr ON (cr.communication_id = cs.communication_id "
    + "   AND cr.channel_id = cs.channel_id "
    + "   AND cr.is_active = 1) ";

  private static final String FROM_ASSET_COMM_COMM_STAT_CLAUSE = ""
    + " FROM "
    + " asset a "
    + " LEFT JOIN communication c ON (a.type = 'communication' AND c.id = a.target_id)  "
    + " LEFT JOIN communication_statistics cs ON ( cs.communication_id = a.target_id AND cs.channel_id = a.channel_id ) ";

  /* @formatter:on */

  /** JDBC template used to access the database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Set the datasource on this DAO.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<CommunicationStatistics> findCommunicationStatisticsByChannelIds(Long commId, List<Long> channelIds) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding all the identified communication [" + commId + "] statistics in the supplied channels [" +
          channelIds + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    StringBuilder sql = new StringBuilder();

    sql.append(SQL_SELECT);
    sql.append(COMMUNICATION_STATISTICS_VIEWINGS_FIELDS + COMMA_SEPARATOR);
    sql.append(COMMUNICATION_STATISTICS_REGISTRATIONS_FIELDS);
    sql.append(FROM_COMM_STAT_REG_CLAUSE);
    sql.append(" WHERE cs." + COMMUNICATION_ID_COLUMN_NAME + " =:" + COMMUNICATION_ID_PARAM_NAME);
    sql.append(" AND cs." + CHANNEL_ID_COLUMN_NAME + " IN (:" + CHANNEL_IDS_PARAM_NAME + ") ");
    sql.append(" GROUP BY cs." + CHANNEL_ID_COLUMN_NAME);

    params.addValue(COMMUNICATION_ID_PARAM_NAME, commId);
    params.addValue(CHANNEL_IDS_PARAM_NAME, channelIds);

    List<CommunicationStatistics> communicationStatistics = jdbcTemplate.query(sql.toString(), params,
        new CommunicationStatisticsRowMapper(true));

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully found communication statistics [" + communicationStatistics + "].");
    }
    return communicationStatistics;
  }

  @Override
  public void loadStatistics(Long channelId, List<Communication> communications) {

    // just a double check. if we don't have any channel id or communications, then make sure
    // we don't hit the database to do pointless queries.
    if (channelId == null || communications == null || communications.isEmpty()) {
      return;
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Adding communication viewing stats for channelId [" + channelId
          + "]. Non syndicated communications.");
    }

    List<Long> communicationIds = super.getCommunicationIds(communications);

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    sql.append(SQL_SELECT);
    sql.append(COMMUNICATION_STATISTICS_VIEWINGS_FIELDS);
    sql.append(FROM_ASSET_COMM_COMM_STAT_CLAUSE);
    sql.append("WHERE ");
    sql.append("  a.target_id IN ( :" + COMMUNICATION_IDs_PARAM_NAME + ")");
    sql.append("  AND a.channel_id = :" + CHANNEL_ID_PARAM_NAME);
    sql.append("  AND c.status != 'deleted'");
    sql.append("  AND a.is_active = 1 ");
    sql.append("GROUP BY a.target_id, a.channel_id ");

    params.addValue(CHANNEL_ID_PARAM_NAME, channelId);
    params.addValue(COMMUNICATION_IDs_PARAM_NAME, communicationIds);

    List<CommunicationStatistics> communicationViewingStatistics = null;

    try {
      communicationViewingStatistics = jdbcTemplate.query(sql.toString(), params,
          new CommunicationStatisticsRowMapper(false));
    } catch (EmptyResultDataAccessException e) {
      throw new NotFoundException("Problems getting communication stats "
          + "for channel [" + channelId + "] "
          + "and communications [" + communicationIds + "]", e);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found communication viewing statistics "
          + "for channelId [" + channelId + "]. "
          + "Statistics [" + communicationViewingStatistics + "].");
    }

    this.setViewingStatistics(communications, communicationViewingStatistics);

  }

  @Override
  public void loadStatisticsFromAllChannels(List<Communication> syndicatedOutCommunications) {

    // just a double check. if we don't have any channel id or communications, then make sure
    // we don't hit the database to do pointless queries.
    if (syndicatedOutCommunications == null || syndicatedOutCommunications.isEmpty()) {
      return;
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Adding communication viewing stats. Syndicated out communications.");
    }

    List<Long> communicationIds = super.getCommunicationIds(syndicatedOutCommunications);

    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    sql.append(SQL_SELECT);
    sql.append(" a.target_id AS " + COMMUNICATION_ID_PARAM_NAME + COMMA_SEPARATOR);
    sql.append(" cs.channel_id AS " + CHANNEL_ID_PARAM_NAME + COMMA_SEPARATOR);
    sql.append(" SUM(cs.num_views) AS " + NUMBER_OF_VIEWS_PARAM_NAME + COMMA_SEPARATOR);
    sql.append(" SUM(cs.viewed_for) AS " + TOTAL_VIEWING_DURATION_PARAM_NAME + COMMA_SEPARATOR);
    sql.append(" SUM(cs.num_ratings) AS " + NUMBER_OF_RATINGS_PARAM_NAME + COMMA_SEPARATOR);
    sql.append(" SUM(cs.rating * cs.num_ratings) / SUM(cs.num_ratings) AS " + AVERAGE_RATING_PARAM_NAME);
    sql.append(FROM_ASSET_COMM_COMM_STAT_CLAUSE);
    sql.append("WHERE ");
    sql.append("  a.target_id IN ( :" + COMMUNICATION_IDs_PARAM_NAME + ")");
    sql.append("  AND c.status != 'deleted'");
    sql.append("  AND a.is_active = 1 ");
    sql.append("GROUP BY ");
    sql.append("  a.target_id");

    params.addValue(COMMUNICATION_IDs_PARAM_NAME, communicationIds);

    List<CommunicationStatistics> communicationViewingStatistics = null;

    try {
      communicationViewingStatistics = jdbcTemplate.query(sql.toString(), params,
          new CommunicationStatisticsRowMapper(false));
    } catch (EmptyResultDataAccessException e) {
      throw new NotFoundException("Problems getting communication overrides for stats "
          + "for syndicated communications [" + communicationIds + "]", e);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found communication viewing statistics for syndicated communications. "
          + "Statistics [" + communicationViewingStatistics + "].");
    }

    this.setViewingStatistics(syndicatedOutCommunications, communicationViewingStatistics);
  }

  /**
   * Set the viewing statistics on the relevant communications.
   * 
   * @param communications the communications to add the statistics to.
   * @param communicationViewingStatistics the statistics to add.
   */
  private void setViewingStatistics(List<Communication> communications,
      List<CommunicationStatistics> communicationViewingStatistics) {

    for (Communication communication : communications) {
      for (CommunicationStatistics viewingStatistics : communicationViewingStatistics) {
        if (communication.getId().equals(viewingStatistics.getId())) {
          communication.setCommunicationStatistics(viewingStatistics);
        }
      }
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Viewing stats added.");
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link CommunicationStatistics}.
   */
  private static class CommunicationStatisticsRowMapper implements RowMapper<CommunicationStatistics> {

    private final boolean includeRegistration;

    /**
     * @param includeRegistration Flag indicates if communication registration included in the communication statistics.
     */
    public CommunicationStatisticsRowMapper(boolean includeRegistration) {
      this.includeRegistration = includeRegistration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommunicationStatistics mapRow(ResultSet resultSet, int rowNum) throws SQLException {

      CommunicationStatistics communicationStatistics = new CommunicationStatistics(
          resultSet.getLong(COMMUNICATION_ID_PARAM_NAME),
          resultSet.getInt(NUMBER_OF_VIEWS_PARAM_NAME),
          resultSet.getLong(TOTAL_VIEWING_DURATION_PARAM_NAME),
          resultSet.getInt(NUMBER_OF_RATINGS_PARAM_NAME),
          resultSet.getFloat(AVERAGE_RATING_PARAM_NAME));
      communicationStatistics.setChannelId(resultSet.getLong(CHANNEL_ID_PARAM_NAME));
      if (includeRegistration) {
        communicationStatistics.setTotalRegistration(resultSet.getInt(REGISTRATION_PARAM_NAME));
      }

      return communicationStatistics;
    }
  }

}
