/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: FeatureDbDao.java 75411 2014-03-19 10:55:15Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;

import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;

/**
 * DAO for database operations on a {@link Feature} instance.
 */
public interface FeatureDbDao {

  /**
   * Loads the channel features.
   * 
   * @param channel channel to load the features for.
   */
  void loadFeatures(Channel channel);

  /**
   * Loads the channel features for multiple channels.
   * 
   * @param channels channels to load the features for.
   */
  void loadFeatures(List<Channel> channels);

  /**
   * Finds the features by channel id.
   * 
   * @param channelId the channel id to find by
   * 
   * @return {@link Feature features}
   */
  List<Feature> findByChannelId(Long channelId);

  /**
   * Updates the features by channel id.
   * 
   * @param channel the channel
   * @param features the features to update
   */
  void update(Channel channel, List<Feature> features);

  /**
   * Finds all the default channel features for the supplied channel type id.
   * 
   * @param channelTypeId The channel type id.
   * @return List of default channel {@link Feature featues}.
   */
  List<Feature> findDefaultFeatureByChannelTypeId(Long channelTypeId);

  /**
   * Delete channel feature overrides by names.
   * 
   * @param channelId The channel id for the features will be soft deleted.
   * @param names List of channel features {@link Type} names.
   */
  void deleteFeatureOverridesByNames(Long channelId, List<Type> names);

}
