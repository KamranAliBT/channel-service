/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ScheduledPublicationDbDao.java 74757 2014-02-27 11:18:36Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.List;
import java.util.Map;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationScheduledPublication;

/**
 * DAO for database operations on scheduled communication tasks.
 */
public interface ScheduledPublicationDbDao {

  /**
   * Find all the communications that were scheduled to be published/unpublished for current time.
   * 
   * @param jobId a temporary id used to lock all the tasks to be processed
   * 
   * @return a map where comm publish status is a key and an 'action' indicator and the value is a collection of found
   * communications
   */
  Map<Communication.PublishStatus, List<Long>> findScheduledCommunications(String jobId);

  /**
   * Find all the scheduled publication for the given communicationId.
   * 
   * @param communicationId of the scheduled publication to load
   * 
   * @return a map where comm publish status is a key and an 'action' indicator and the value is a collection of found
   * communications
   */
  CommunicationScheduledPublication find(Long communicationId);

  /**
   * Mark all scheduled tasks as completed based on a given job id.
   * 
   * @param jobId the job id to be selected as completed
   */
  void markAsCompleted(String jobId);

  /**
   * Update the communication scheduled publication date in the database.
   * 
   * @param communication that contains the scheduled publication to set.
   */
  void update(Communication communication);

  /**
   * Deactivate a scheduled publication. This happens when a publish status is changed or when the element is set but
   * empty in the request.
   * 
   * @param communication to unset the scheduled publication from
   */
  void deactivatePublicationDate(Communication communication);
}