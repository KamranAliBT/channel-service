/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: AuditRecordDto.java 78346 2014-05-21 10:55:35Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * DTO object representing resource.
 */
public class AuditRecordDto implements Serializable {

  private static final long serialVersionUID = 4713298241686335271L;

  private Long userId;

  private Long channelId;

  private Long communicationId;

  private String action;

  private String entity;

  private String actionDescription;

  /**
   * @return the userId
   */
  public Long getUserId() {
    return userId;
  }

  /**
   * @param userId the userId to set
   */
  public void setUserId(final Long userId) {
    this.userId = userId;
  }

  /**
   * @return the channelId
   */
  public Long getChannelId() {
    return channelId;
  }

  /**
   * @param channelId the channelId to set
   */
  public void setChannelId(final Long channelId) {
    this.channelId = channelId;
  }

  /**
   * @return the communicationId
   */
  public Long getCommunicationId() {
    return communicationId;
  }

  /**
   * @param communicationId the communicationId to set
   */
  public void setCommunicationId(final Long communicationId) {
    this.communicationId = communicationId;
  }

  /**
   * @return the action
   */
  public String getAction() {
    return action;
  }

  /**
   * @param action the action to set
   */
  public void setAction(final String action) {
    this.action = action;
  }

  /**
   * @return the entity
   */
  public String getEntity() {
    return entity;
  }

  /**
   * @param entity the entity to set
   */
  public void setEntity(final String entity) {
    this.entity = entity;
  }

  /**
   * @return the actionDescription
   */
  public String getActionDescription() {
    return actionDescription;
  }

  /**
   * @param actionDescription the actionDescription to set
   */
  public void setActionDescription(final String actionDescription) {
    this.actionDescription = actionDescription;
  }

  @Override
  public String toString() {

    ToStringBuilder builder = new ToStringBuilder(this);
    builder.append("userId", userId);
    builder.append("channelId", channelId);
    builder.append("communicationId", communicationId);
    builder.append("action", action);
    builder.append("entity", entity);
    builder.append("actionDescription", actionDescription);

    return builder.toString();
  }
}
