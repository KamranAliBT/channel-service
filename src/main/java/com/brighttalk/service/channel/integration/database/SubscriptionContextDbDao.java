/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SubscriptionContextDbDao.java 91249 2015-03-06 16:38:40Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;

/**
 * DAO for database operation on {@link SubscriptionContext} domain object.
 */
public interface SubscriptionContextDbDao {

  /**
   * Creates a subscription context.
   * 
   * @param context The subscription context to create.
   */
  void create(SubscriptionContext context);

  /**
   * Find subscription context by subscription id.
   * 
   * @param subscriptionId The subscription Id.
   * @return The found subscription context, otherwise <code>null</code> if the supplied subscription Id does not
   * identify an existing subscription context.
   */
  SubscriptionContext findBySubscriptionId(Long subscriptionId);
}
