/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ChannelAuditRecordDtoBuilder.java 97401 2015-07-01 08:38:36Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit.dto.builder;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * Helper class for creating {@link AuditRecordDto} instances for channel audit events.
 */
@Component
public class ChannelAuditRecordDtoBuilder extends AuditRecordDtoBuilder {

  /**
   * {@inheritDoc}
   */
  @Override
  public AuditRecordDto delete(final Long userId, final Long channelId,
      final Long communicationId, final String actionDescription) {
    return createAuditRecordDto(userId, channelId, communicationId, AuditEntity.CHANNEL, AuditAction.DELETE,
        actionDescription);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AuditRecordDto update(final Long userId, final Long channelId, final Long communicationId,
      final String actionDescription) {
    return createAuditRecordDto(userId, channelId, communicationId, AuditEntity.CHANNEL, AuditAction.UPDATE,
        actionDescription);
  }

}
