/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcContactDetailsDbDao.java 98330 2015-07-23 09:51:44Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.ContactDetails;
import com.brighttalk.service.channel.business.error.ContactDetailsNotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link ContactDetailsDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database.
 */
@Repository
@Primary
public class JdbcContactDetailsDbDao implements ContactDetailsDbDao {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcContactDetailsDbDao.class);

  private static final String CHANNEL_ID_FIELD = "channel_id";
  private static final String FIRST_NAME_FIELD = "first_name";
  private static final String LAST_NAME_FIELD = "last_name";
  private static final String EMAIL_FIELD = "email";
  private static final String TELEPHONE_FIELD = "telephone";
  private static final String JOB_TITLE_FIELD = "job_title";
  private static final String COMPANY_NAME_FIELD = "company_name";
  private static final String ADDRESS1_FIELD = "address1";
  private static final String ADDRESS2_FIELD = "address2";
  private static final String CITY_FIELD = "city";
  private static final String STATE_REGION_FIELD = "state_region";
  private static final String POSTCODE_FIELD = "postcode";
  private static final String COUNTRY_FIELD = "country";
  private static final String CREATED_FIELD = "created";
  private static final String LAST_UPDATED_FIELD = "last_updated";

  /* @formatter:off */
  private static final String SELECT_CONTACT_DETAILS_FOR_CHANNEL_QUERY = ""
      + "SELECT"
      + "  first_name,"
      + "  last_name,"
      + "  email,"
      + "  telephone,"
      + "  job_title,"
      + "  company_name,"
      + "  address1,"
      + "  address2,"
      + "  city,"
      + "  state_region,"
      + "  postcode,"
      + "  country "
      + "FROM"
      + "  contact_details  "
      + "WHERE"
      + "  channel_id = :channel_id"
      + " LIMIT 1";

  private static final String UPDATE_CONTACT_DETAILS_FOR_CHANNEL_QUERY = ""
      + "UPDATE "
      + "  contact_details "
      + "SET "
      + "  first_name = :first_name,"
      + "  last_name = :last_name,"
      + "  email = :email,"
      + "  telephone = :telephone,"
      + "  job_title = :job_title,"
      + "  company_name = :company_name,"
      + "  address1 = :address1,"
      + "  address2 = :address2,"
      + "  city = :city,"
      + "  state_region = :state_region,"
      + "  postcode = :postcode,"
      + "  country = :country, "
      + "  last_updated = :last_updated "
      + "WHERE "
      + "  channel_id = :channel_id";
  /* @formatter:on */

  private SimpleJdbcInsert simpleJdbcInsertContactDetails;
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    simpleJdbcInsertContactDetails = new SimpleJdbcInsert(dataSource);
    simpleJdbcInsertContactDetails.withTableName("contact_details");
    simpleJdbcInsertContactDetails.usingColumns(CHANNEL_ID_FIELD, FIRST_NAME_FIELD, LAST_NAME_FIELD, EMAIL_FIELD,
        TELEPHONE_FIELD, JOB_TITLE_FIELD, COMPANY_NAME_FIELD, ADDRESS1_FIELD, ADDRESS2_FIELD, CITY_FIELD,
        STATE_REGION_FIELD, POSTCODE_FIELD, COUNTRY_FIELD, LAST_UPDATED_FIELD, CREATED_FIELD);

    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public void create(final Long channelId, final ContactDetails contactDetails) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(String.format("Creating contact details for channel [%s].", channelId));
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_FIELD, channelId);
    params.addValue(FIRST_NAME_FIELD, contactDetails.getFirstName());
    params.addValue(LAST_NAME_FIELD, contactDetails.getLastName());
    params.addValue(EMAIL_FIELD, contactDetails.getEmail());
    params.addValue(TELEPHONE_FIELD, contactDetails.getTelephone());
    params.addValue(JOB_TITLE_FIELD, contactDetails.getJobTitle());
    params.addValue(COMPANY_NAME_FIELD, contactDetails.getCompany());
    params.addValue(ADDRESS1_FIELD, contactDetails.getAddress1());
    params.addValue(ADDRESS2_FIELD, contactDetails.getAddress2());
    params.addValue(CITY_FIELD, contactDetails.getCity());
    params.addValue(STATE_REGION_FIELD, contactDetails.getState());
    params.addValue(POSTCODE_FIELD, contactDetails.getPostcode());
    params.addValue(COUNTRY_FIELD, contactDetails.getCountry());

    params.addValue(LAST_UPDATED_FIELD, new Date());
    params.addValue(CREATED_FIELD, new Date());

    simpleJdbcInsertContactDetails.execute(params);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(String.format("Contact details successfully created for channel [%s].", channelId));
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ContactDetails get(final Long channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(String.format("Getting contact details for channel [%s].", channelId));
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_FIELD, channelId);

    ContactDetails contactDetails = null;

    try {
      contactDetails = jdbcTemplate.queryForObject(SELECT_CONTACT_DETAILS_FOR_CHANNEL_QUERY, params,
          new ContactDetailsRowMapper());
    } catch (EmptyResultDataAccessException e) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(String.format("Contact details not found for channel [%s].", channelId));
      }
      throw new ContactDetailsNotFoundException(channelId, e);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(String.format("Found contact details [%s].", contactDetails));
    }

    return contactDetails;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link ContactDetails}.
   */
  private class ContactDetailsRowMapper implements RowMapper<ContactDetails> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ContactDetails mapRow(final ResultSet rs, final int rowNum) throws SQLException {

      ContactDetails contactDetails = new ContactDetails();

      contactDetails.setFirstName(rs.getString(FIRST_NAME_FIELD));
      contactDetails.setLastName(rs.getString(LAST_NAME_FIELD));
      contactDetails.setEmail(rs.getString(EMAIL_FIELD));
      contactDetails.setTelephone(rs.getString(TELEPHONE_FIELD));
      contactDetails.setJobTitle(rs.getString(JOB_TITLE_FIELD));
      contactDetails.setCompany(rs.getString(COMPANY_NAME_FIELD));
      contactDetails.setAddress1(rs.getString(ADDRESS1_FIELD));
      contactDetails.setAddress2(rs.getString(ADDRESS2_FIELD));
      contactDetails.setCity(rs.getString(CITY_FIELD));
      contactDetails.setState(rs.getString(STATE_REGION_FIELD));
      contactDetails.setPostcode(rs.getString(POSTCODE_FIELD));
      contactDetails.setCountry(rs.getString(COUNTRY_FIELD));

      return contactDetails;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ContactDetails update(final Long channelId, final ContactDetails contactDetails) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updating contact details for channel [" + channelId + "].");
    }
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_FIELD, channelId);
    params.addValue(FIRST_NAME_FIELD, contactDetails.getFirstName());
    params.addValue(LAST_NAME_FIELD, contactDetails.getLastName());
    params.addValue(EMAIL_FIELD, contactDetails.getEmail());
    params.addValue(TELEPHONE_FIELD, contactDetails.getTelephone());
    params.addValue(JOB_TITLE_FIELD, contactDetails.getJobTitle());
    params.addValue(COMPANY_NAME_FIELD, contactDetails.getCompany());
    params.addValue(ADDRESS1_FIELD, contactDetails.getAddress1());
    params.addValue(ADDRESS2_FIELD, contactDetails.getAddress2());
    params.addValue(CITY_FIELD, contactDetails.getCity());
    params.addValue(STATE_REGION_FIELD, contactDetails.getState());
    params.addValue(POSTCODE_FIELD, contactDetails.getPostcode());
    params.addValue(COUNTRY_FIELD, contactDetails.getCountry());
    params.addValue(LAST_UPDATED_FIELD, new Date());

    int noOfRows = jdbcTemplate.update(UPDATE_CONTACT_DETAILS_FOR_CHANNEL_QUERY, params);

    if (noOfRows != 1) {
      LOGGER.error("Failed to update contact details for channel [" + channelId + "].");
      // AP-989: if the update returns 0, we create an entry with the data given.
      create(channelId, contactDetails);
    } else {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Contact details successfully updated for channel [" + channelId + "].");
      }
    }

    return contactDetails;
  }
}
