/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2011.
 * All Rights Reserved.
 * $Id: SortDto.java 29625 2011-09-16 11:29:59Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.user.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * Data transfer object for a search user sort.
 */
@XmlRootElement(name = "sort")
public class SortDto {

  private String order;

  private String type;

  /**
   * Default constructor.
   */
  public SortDto() {

  }

  public final String getOrder() {
    return order;
  }

  @XmlAttribute
  public final void setOrder(final String order) {
    this.order = order;
  }

  public final String getType() {
    return type;
  }

  @XmlValue
  public final void setType(final String type) {
    this.type = type;
  }

}