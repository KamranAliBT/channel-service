/**
 * *****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AbstractJdbcCommunicationDbDao.java 99258 2015-08-18 10:35:43Z ssarraj $
 * *****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;

import com.brighttalk.service.channel.business.domain.Provider;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationConfiguration;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;

/**
 * Abstract Base class for all JDBC queries around {@link Communication communications}.
 * <p>
 * Contains common JDBC code for the processing of communications in the DB.
 */
public abstract class AbstractJdbcCommunicationDbDao extends JdbcDao {

  private static final String CHANNEL_ID_COLUMN_NAME = "channel_id";
  private static final String MASTER_CHANNEL_ID_COLUMN_NAME = "master_channel_id";

  /**
   * Communication database table - fields used in selecting a communication.
   */
  protected static final String COMMUNICATION_FIELDS_FOR_SELECT = " c.id AS id, "
      + " c.master_channel_id AS master_channel_id, " + " c.title AS title, " + " c.description AS description, "
      + " c.keywords AS keywords, " + " c.presenter AS authors, " + " c.duration AS duration, "
      + " c.scheduled AS scheduled, " + " c.timezone AS timezone, " + " c.status AS status, "
      + " c.publish_status AS publish_status, " + " c.rerun_count AS rerun_count, "
      + " c.master_channel_id AS master_channel_id, " + " c.thumbnail AS thumbnail_url, "
      + " c.preview_url AS preview_url, " + " c.pin_number AS pin_number, "
      + " c.live_phone_number AS live_phone_number, " + " c.live_url AS live_url, " + " c.format AS format, "
      + " c.provider AS provider, " + " c.created AS created, " + " c.last_updated AS last_updated, "
      + " c.booking_duration AS booking_duration, c.encoding_count AS encoding_count ";

  /**
   * Communication configuration database table - fields used in selecting a communication.
   */
  protected static final String COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT = " cc.channel_id AS channel_id, "
      + " cc.visibility AS visibility, " + " cc.allow_anon_viewings AS allow_anon_viewings, "
      + " cc.is_master AS in_master_channel, " + " cc.survey_id AS survey_id, "
      + " cc.survey_active AS survey_active, " + " cc.show_channel_survey AS show_channel_survey, "
      + " cc.custom_url AS custom_url, " + " cc.syndication_type AS syndication_type, "
      + " cc.syndication_auth_master AS syndication_auth_master, "
      + " cc.syndication_auth_master_timestamp AS syndication_auth_master_timestamp, "
      + " cc.syndication_auth_consumer AS syndication_auth_consumer, "
      + " cc.syndication_auth_consumer_timestamp AS syndication_auth_consumer_timestamp, "
      + " cc.exclude_from_channel_capacity AS exclude_from_channel_capacity, "
      + " cc.client_booking_reference AS client_booking_reference, " + " cc.created AS cc_created, "
      + " cc.last_updated AS cc_last_updated ";

  /**
   * Site domain.
   */
  @Value("#{domain}")
  protected String domain;

  public String getDomain() {

    return domain;
  }

  public void setDomain(final String value) {

    domain = value;
  }

  /**
   * Return a clause that will restrict the communications returned to only communications that are :- - Not syndicated
   * - OR syndicated out from the target channel - OR Approved to be syndicated into the target channel
   * 
   * Note - this clause expects the communication_configuration table to be aliased as 'cc'.
   * 
   * @return StringBuilder the clause
   */
  protected static StringBuilder getApprovedCommunicationsClause() {

    // Query is as follows to restrict selection to not syndication communications OR syndicated out communications
    // OR syndicated in communications that are approved
    // "cc.syndication_type IS NULL " +
    // "OR cc.syndication_type = out" +
    // "OR ( cc.syndication_auth_master = approved AND cc.syndication_auth_consumer = approved)" ;

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("(cc.syndication_type IS NULL ");
    sql.append("OR cc.syndication_type = 'out' ");
    sql.append("OR ( cc.syndication_auth_master = 'approved' AND cc.syndication_auth_consumer = 'approved'))");
    // CHECKSTYLE:ON

    return sql;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Communication}.
   */
  protected static class CommunicationRowMapper extends AbstractJdbcCommunicationDbDao implements RowMapper<Communication> {

    /**
     * Domain domain that can be used to generate urls.
     */
    protected String domain;

    /**
     * Constructor.
     * 
     * @param domain that can be used to generate urls
     */
    public CommunicationRowMapper(final String domain) {

      this.domain = domain;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Communication mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Long communicationId = resultSet.getLong("id");

      // map the communication
      Communication communication = new Communication(communicationId);
      communication.setMasterChannelId(resultSet.getLong(MASTER_CHANNEL_ID_COLUMN_NAME));
      communication.setTitle(resultSet.getString("title"));
      communication.setDescription(resultSet.getString("description"));
      communication.setKeywords(resultSet.getString("keywords"));
      communication.setAuthors(resultSet.getString("authors"));
      communication.setDuration(resultSet.getInt("duration"));
      communication.setScheduledDateTime(resultSet.getTimestamp("scheduled"));
      communication.setTimeZone(resultSet.getString("timezone"));
      communication.setStatus(Communication.Status.valueOf(resultSet.getString("status").toUpperCase()));
      communication.setPublishStatus(Communication.PublishStatus.valueOf(resultSet.getString("publish_status").toUpperCase()));
      communication.setRerunCount(resultSet.getInt("rerun_count"));
      communication.setThumbnailUrl(convertUrl(resultSet.getString("thumbnail_url")));
      communication.setPreviewUrl(convertUrl(resultSet.getString("preview_url")));
      communication.setPinNumber(resultSet.getString("pin_number"));
      communication.setLivePhoneNumber(resultSet.getString("live_phone_number"));
      communication.setLiveUrl(resultSet.getString("live_url"));
      communication.setFormat(Communication.Format.valueOf(resultSet.getString("format").toUpperCase()));
      communication.setProvider(new Provider(resultSet.getString("provider")));
      communication.setCreated(resultSet.getTimestamp("created"));
      communication.setLastUpdated(resultSet.getTimestamp("last_updated"));
      communication.setBookingDuration(resultSet.getInt("booking_duration"));
      communication.setEncodingCount(resultSet.getInt("encoding_count"));

      // map the communications configuration

      CommunicationConfiguration communicationConfiguration = super.mapCommunicationConfiguration(communicationId,
          resultSet);
      communication.setConfiguration(communicationConfiguration);
      communication.setChannelId(resultSet.getLong(CHANNEL_ID_COLUMN_NAME));

      return communication;
    }

    /**
     * Convert a URL to its full form by adding in the domain.
     * <p>
     * The preview and thumbnail URLs for a communication are optional and only created when e.g. slides are upload or
     * feature images generated. Only the path to them is stored. We need to pre-pend the env specific domain if there
     * is a url.
     * 
     * @param url url to convert. Can be <code>null</code> or empty string if there is no url to convert
     * @return url with domain pre-pended or null if no url given.
     */
    private String convertUrl(final String url) {
      // no url, no need to add the domain
      if (StringUtils.isEmpty(url)) {
        return null;
      }
      return domain + url;
    }
  }

  /**
   * Communication configuration mapper.
   * 
   * @param communicationId of the communication to map
   * @param resultSet the result set from the db
   * @return the CommunicationConfiguration object
   * @throws SQLException if an error occurs
   */
  protected CommunicationConfiguration mapCommunicationConfiguration(final Long communicationId,
      final ResultSet resultSet) throws SQLException {
    // Make sure that survey id is converted to null or numeric value
    String surveyId = resultSet.getString("survey_id");

    CommunicationConfiguration configuration = new CommunicationConfiguration(communicationId);
    configuration.setVisibility(CommunicationConfiguration.Visibility.valueOf(resultSet.getString("visibility").toUpperCase()));
    configuration.setAllowAnonymousViewings(resultSet.getBoolean("allow_anon_viewings"));
    if (surveyId != null) {
      configuration.setSurveyId(Long.valueOf(surveyId));
    }
    configuration.setChannelId(resultSet.getLong(CHANNEL_ID_COLUMN_NAME));
    configuration.setSurveyActive(resultSet.getBoolean("survey_active"));
    configuration.setShowChannelSurvey(resultSet.getBoolean("show_channel_survey"));
    configuration.setCustomUrl(resultSet.getString("custom_url"));
    configuration.setClientBookingReference(resultSet.getString("client_booking_reference"));
    configuration.setInMasterChannel(resultSet.getBoolean("in_master_channel"));
    configuration.setExcludeFromChannelContentPlan(resultSet.getBoolean("exclude_from_channel_capacity"));
    configuration.setCreated(resultSet.getTimestamp("cc_created"));
    configuration.setLastUpdated(resultSet.getTimestamp("cc_last_updated"));

    CommunicationSyndication communicationSyndication = new CommunicationSyndication();

    communicationSyndication.setChannel(new Channel(resultSet.getLong(MASTER_CHANNEL_ID_COLUMN_NAME)));
    communicationSyndication.setSyndicationType(resultSet.getString("syndication_type"));

    communicationSyndication.setMasterChannelSyndicationAuthorisationStatus(resultSet.getString("syndication_auth_master"));
    communicationSyndication.setMasterChannelSyndicationAuthorisationTimestamp(resultSet.getTimestamp("syndication_auth_master_timestamp"));

    communicationSyndication.setConsumerChannelSyndicationAuthorisationStatus(resultSet.getString("syndication_auth_consumer"));
    communicationSyndication.setConsumerChannelSyndicationAuthorisationTimestamp(resultSet.getTimestamp("syndication_auth_consumer_timestamp"));

    configuration.setCommunicationSyndication(communicationSyndication);

    return configuration;
  }

  /**
   * Strips the domain from the given url if set.
   * 
   * @param url to strip the domain from
   * @return the stripped url
   */
  protected String stripUrl(String url) {
    if (StringUtils.isNotEmpty(url) && url.contains(domain)) {
      url = url.replace(domain, "");

    }
    return url;
  }
}
