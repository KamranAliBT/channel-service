/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: BookingServiceDao.java 71274 2013-11-26 14:01:47Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking;

import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Interface to define the required methods for the interactions with the Booking HD Service.
 */
public interface HDBookingServiceDao {

  /**
   * Book an HD webcast.
   * 
   * @param communication to be booked
   * @return the phone number of the booking if successfully.
   * @throws BookingCapacityExceeded Exception if the booking fails because of capacity exceeded or a system error.
   */
  String createBooking(final Communication communication);

  /**
   * Update the booking of an HD webcast.
   * 
   * @param communication to be updated
   * @return the phone number of the booking if successfully.
   * @throws BookingCapacityExceeded Exception if the booking fails because of capacity exceeded or a system error.
   */
  String updateBooking(final Communication communication);

  /**
   * Delete the booking of an HD webcast.
   * 
   * @param communication to be updated delete
   * @throws BookingCapacityExceeded Exception if the booking fails because of capacity exceeded or a system error.
   */
  void deleteBooking(final Communication communication);

  /**
   * Delete all the HD bookings from booking service for the given channel id.
   * 
   * @param channelId id of the channel to delete the boookings.
   * @throws BookingCapacityExceeded Exception if the booking fails because of capacity exceeded or a system error.
   */
  void deleteBookings(final Long channelId);
}