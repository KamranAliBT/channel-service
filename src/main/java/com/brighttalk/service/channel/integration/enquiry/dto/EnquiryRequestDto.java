/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EnquiryRequestDto.java 55023 2012-10-01 10:22:47Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.enquiry.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * EnquiryRequestDto as per API definition.
 */
@XmlRootElement(name = "request")
public class EnquiryRequestDto {

  private FormDto form;

  public FormDto getForm() {
    return form;
  }

  public void setForm(FormDto form) {
    this.form = form;
  }
}