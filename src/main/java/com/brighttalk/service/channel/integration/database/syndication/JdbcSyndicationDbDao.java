/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcScheduledPublicationDbDao.java 72512 2014-01-13 15:27:48Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database.syndication;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;

/**
 * Spring JDBC Database DAO implementation of {@link JdbcSyndicationDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database retrieve information about scheduled publication tasks.
 */
@Repository
@Primary
public class JdbcSyndicationDbDao implements SyndicationDbDao {

  private static final Logger LOGGER = Logger.getLogger(JdbcSyndicationDbDao.class);

  private static final String CHANNEL_ID_COLUMN_NAME = "channel_id";
  private static final String CHANNEL_TITLE_COLUMN_NAME = "title";
  private static final String CHANNEL_ORGANISATION_COLUMN_NAME = "organisation";
  private static final String SYNDICATION_AUTH_MASTER_COLUMN_NAME = "syndication_auth_master";
  private static final String SYNDICATION_AUTH_MASTER_TIMESTAMP_COLUMN_NAME = "syndication_auth_master_timestamp";
  private static final String SYNDICATION_AUTH_CONSUMER_COLUMN_NAME = "syndication_auth_consumer";
  private static final String SYNDICATION_AUTH_CONSUMER_TIMESTAMP_COLUMN_NAME = "syndication_auth_consumer_timestamp";
  private static final String SYNDICATION_TYPE_COLUMN_NAME = "syndication_type";
  private static final String COMMUNICATION_ID_PARAM_NAME = "communicationId";

  /* @formatter:off */
  /** Select query to retrieve list of consumer channels syndication details for a given communication. */
  private static final String SELECT_CONSUMER_SYNDICATIONS_QUERY = ""
    + "SELECT " 
    + "  cc.channel_id, "
    + "  c.title, "
    + "  c.organisation, "
    + "  cc.syndication_type, "
    + "  cc.syndication_auth_master, "
    + "  cc.syndication_auth_master_timestamp, "
    + "  cc.syndication_auth_consumer, "
    + "  cc.syndication_auth_consumer_timestamp "
    + "FROM "
    + "  communication_configuration cc "
    + "INNER JOIN channel c ON (c.id = cc.channel_id AND c.is_active = 1) "
    + "INNER JOIN asset ON ( " 
    + "  asset.target_id = cc.communication_id " 
    + "  AND asset.channel_id = cc.channel_id " 
    + "  AND asset.type =  'communication' " 
    + "  AND asset.is_active = 1) "
    + "WHERE "
    + "  cc.communication_id = :" + COMMUNICATION_ID_PARAM_NAME
    + "  AND cc.syndication_type = 'in' "
    + "ORDER BY cc.channel_id ASC";
  /* @formatter:on */

  private NamedParameterJdbcTemplate jdbcTemplate;

  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<CommunicationSyndication> findConsumerChannelsSyndication(final long communicationId) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding all consumer channels syndication for communication [" + communicationId + "]");
    }

    final MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COMMUNICATION_ID_PARAM_NAME, communicationId);

    List<CommunicationSyndication> communicationSyndications = jdbcTemplate.query(SELECT_CONSUMER_SYNDICATIONS_QUERY,
        params, new CommunicationSyndicationRowMapper());

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully found the syndications details [" + communicationSyndications + "].");
    }

    return communicationSyndications;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link CommunicationSyndication}.
   */
  private class CommunicationSyndicationRowMapper implements RowMapper<CommunicationSyndication> {

    /**
     * {@inheritDoc}
     */
    @Override
    public CommunicationSyndication mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
      long channelId = resultSet.getLong(CHANNEL_ID_COLUMN_NAME);
      String channelTitle = resultSet.getString(CHANNEL_TITLE_COLUMN_NAME);
      String channelOrganisation = resultSet.getString(CHANNEL_ORGANISATION_COLUMN_NAME);
      String syndicationType = resultSet.getString(SYNDICATION_TYPE_COLUMN_NAME);
      String syndicationAuthMaterStatus = resultSet.getString(SYNDICATION_AUTH_MASTER_COLUMN_NAME);
      Date syndicationAuthMaterTimeStamp = resultSet.getTimestamp(SYNDICATION_AUTH_MASTER_TIMESTAMP_COLUMN_NAME);
      String syndicationAuthConsumerStatus = resultSet.getString(SYNDICATION_AUTH_CONSUMER_COLUMN_NAME);
      Date syndicationAuthConsumerTimeStamp = resultSet.getTimestamp(SYNDICATION_AUTH_CONSUMER_TIMESTAMP_COLUMN_NAME);

      CommunicationSyndication commSyndication = new CommunicationSyndication();
      Channel channel = new Channel(channelId);
      channel.setTitle(channelTitle);
      channel.setOrganisation(channelOrganisation);
      commSyndication.setChannel(channel);
      commSyndication.setSyndicationType(syndicationType);
      commSyndication.setMasterChannelSyndicationAuthorisationStatus(syndicationAuthMaterStatus);
      commSyndication.setMasterChannelSyndicationAuthorisationTimestamp(syndicationAuthMaterTimeStamp);
      commSyndication.setConsumerChannelSyndicationAuthorisationStatus(syndicationAuthConsumerStatus);
      commSyndication.setConsumerChannelSyndicationAuthorisationTimestamp(syndicationAuthConsumerTimeStamp);

      return commSyndication;
    }
  }

}