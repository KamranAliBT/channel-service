/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: VoteDto.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audience.dto;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * VoteDto.
 */
public class VoteDto {

  private Long id;

  public Long getId() {
    return id;
  }

  @XmlAttribute
  public void setId(final Long id) {
    this.id = id;
  }
}