/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpLiveEventServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.notification;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.notification.dto.EventDto;
import com.brighttalk.service.channel.integration.notification.dto.converter.NotificationRequestConverter;

/**
 * Implementation of the Dao to interact with the Notification service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
@Component
public class XmlHttpNotificationServiceDao implements NotificationServiceDao {

  private static final Logger LOGGER = Logger.getLogger(XmlHttpNotificationServiceDao.class);

  /**
   * Path used to call notification service when a communication has been ended.
   */
  protected static final String EVENT_URL = "/rest/event";

  @Autowired
  @Qualifier(value = "notificationService")
  private RestTemplate restTemplate;

  @Value("${service.notificationServiceUrl}")
  private String serviceBaseUrl;

  @Autowired
  private NotificationRequestConverter converter;

  /**
   * @return the converter
   */
  public NotificationRequestConverter getConverter() {
    return converter;
  }

  /**
   * @param converter the converter to set
   */
  public void setConverter(final NotificationRequestConverter converter) {
    this.converter = converter;
  }

  public String getServiceBaseUrl() {
    return serviceBaseUrl;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void inform(final Communication communication) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Inform Notification that communication [" + communication.getId() + "] has ended.");
    }

    EventDto request = converter.convert(communication);
    String url = serviceBaseUrl + EVENT_URL;

    restTemplate.postForLocation(url, request);
  }

}