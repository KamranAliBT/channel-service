/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EmailRequestConverter.java 71415 2013-11-29 12:05:10Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.notification.dto.converter;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.notification.dto.EventDto;

/**
 * Notification Request Converter used to create DTO for communicating with the Notification service.
 */
@Component
public class NotificationRequestConverter {

  /**
   * Convert the given communication into cancel communication dto.
   * 
   * @param communication to be converted
   * 
   * @return DTO object
   */
  public EventDto convert(final Communication communication) {
    EventDto eventDto = new EventDto();
    eventDto.setCommunicationId(communication.getId());
    return eventDto;
  }

}