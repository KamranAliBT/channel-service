/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 70804 2013-11-12 14:03:50Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.summit.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Communication DTO used when informing summit of a change.
 */
@XmlRootElement(name = "webcast")
@XmlAccessorType(XmlAccessType.FIELD)
public class CommunicationDto {

  @XmlAttribute
  private Long id;
  private Date scheduled;
  private int duration;
  private String publishStatus;

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * @return the scheduled
   */
  public Date getScheduled() {
    return scheduled;
  }

  /**
   * @param scheduled the scheduled to set
   */
  public void setScheduled(final Date scheduled) {
    this.scheduled = scheduled;
  }

  /**
   * @return the duration
   */
  public int getDuration() {
    return duration;
  }

  /**
   * @param duration the duration to set
   */
  public void setDuration(final int duration) {
    this.duration = duration;
  }

  /**
   * @return the publishStatus
   */
  public String getPublishStatus() {
    return publishStatus;
  }

  /**
   * @param publishStatus the publishStatus to set
   */
  public void setPublishStatus(final String publishStatus) {
    this.publishStatus = publishStatus;
  }

  /**
   * Create a String Representation of this object.
   * 
   * @return The String Representation
   */
  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();
    builder.append("id: [").append(id).append("] ");
    builder.append("scheduled: [").append(scheduled).append("] ");
    builder.append("duration: [").append(duration).append("] ");
    builder.append("publishStatus: [").append(publishStatus).append("] ");
    return builder.toString();
  }
}
