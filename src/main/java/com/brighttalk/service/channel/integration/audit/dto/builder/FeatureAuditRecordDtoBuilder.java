/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: FeatureAuditRecordDtoBuilder.java 97400 2015-07-01 08:36:42Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit.dto.builder;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * Helper class for creating {@link AuditRecordDto} instances for channel features audit events.
 */
@Component
public class FeatureAuditRecordDtoBuilder extends AuditRecordDtoBuilder {

  /**
   * {@inheritDoc}
   */
  @Override
  public AuditRecordDto update(final Long userId, final Long channelId, final Long communicationId,
      final String actionDescription) {
    return createAuditRecordDto(userId, channelId, communicationId, AuditEntity.CHANNEL_FEATURES, AuditAction.UPDATE,
        actionDescription);
  }

}
