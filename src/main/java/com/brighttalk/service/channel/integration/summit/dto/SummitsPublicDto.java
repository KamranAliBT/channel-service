/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SummitsPublicDto.java 98710 2015-08-03 09:52:40Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.summit.dto;

import java.util.List;

import com.brighttalk.common.presentation.dto.LinkDto;
import com.brighttalk.service.channel.business.error.NotFoundException;

/**
 * The summit public dto class.
 */
public class SummitsPublicDto {

  private static final String PREVIOUS = "previous";

  private static final String NEXT = "next";

  private List<SummitDto> summits;

  private List<LinkDto> links;

  public List<SummitDto> getSummits() {
    return summits;
  }

  public void setSummits(final List<SummitDto> summits) {
    this.summits = summits;
  }

  public List<LinkDto> getLinks() {
    return links;
  }

  public void setLinks(final List<LinkDto> links) {
    this.links = links;
  }

  /**
   * Check if links are set.
   * 
   * @return boolean true or false
   */
  public boolean hasLinks() {
    return links != null;
  }

  /**
   * Check if the summit has next link.
   * 
   * @return boolean true or false
   */
  public boolean hasNextLink() {
    return hasLink(NEXT);
  }

  /**
   * Check if the summit has previous link.
   * 
   * @return boolean true or false
   */
  public boolean hasPreviousLink() {
    return hasLink(PREVIOUS);
  }

  private boolean hasLink(final String rel) {
    if (!hasLinks()) {
      return false;
    }

    for (LinkDto link : links) {
      if (rel.equals(link.getRel())) {
        return true;
      }
    }

    return false;
  }

  public LinkDto getNextLink() {
    return getLink(NEXT);
  }

  public LinkDto getPreviousLink() {
    return getLink(PREVIOUS);
  }

  private LinkDto getLink(final String rel) {
    if (hasLinks()) {
      for (LinkDto link : links) {
        if (rel.equals(link.getRel())) {
          return link;
        }
      }
    }

    throw new NotFoundException("Link [" + rel + "] not found.");
  }

}