/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JsonHttpAuditServiceDao.java 97420 2015-07-01 10:37:21Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * Implementation of the DAO to interact with the Audit Service.
 * 
 * Objects will be converted ot JSON and transmitted over HTTP.
 */
@Repository
public class JsonHttpAuditServiceDao implements AuditServiceDao {

  @Value("#{auditServiceRestTemplate}")
  private RestTemplate auditServiceRestTemplate;

  @Value("${service.auditServiceUrl}")
  private String auditServiceBaseUrl;

  private static final Logger LOGGER = Logger.getLogger(JsonHttpAuditServiceDao.class);

  /**
   * Request path for creating an audit record
   */
  protected static final String CREATE_REQUEST_PATH = "/internal/action";

  /**
   * {@inheritDoc}
   */
  @Override
  public void create(final AuditRecordDto auditRecordDto) {

    if (auditRecordDto == null) {
      // Log error
      LOGGER.error("Supplied audit record is null");
    } else {
      String url = getCreateAuditRecordRequestUrl();

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Audit record to be created: " + auditRecordDto);
        LOGGER.debug("Audit service URL: [" + url + "]");
      }

      try {
        auditServiceRestTemplate.postForLocation(url, auditRecordDto);
      } catch (Exception ex) {
        LOGGER.error("Failed to call audit service", ex);
      }

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Audit record request sent");
      }
    }
  }

  /**
   * Gets the URL for creating an audit record.
   * 
   * @return URL for creating an audit record.
   */
  private String getCreateAuditRecordRequestUrl() {

    return auditServiceBaseUrl + CREATE_REQUEST_PATH;
  }
}
