/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpLiveEventServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.les;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.les.dto.CancelCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.RescheduleCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.VideoUploadCompleteCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.VideoUploadErrorCommunicationDto;
import com.brighttalk.service.channel.integration.les.dto.converter.LiveEventRequestConverter;

/**
 * Implementation of the Dao to interact with the Live Event service.
 * 
 * Objects will be converted to xml and transmitted over HTTP
 */
@Component
public class XmlHttpLiveEventServiceDao implements LiveEventServiceDao {

  private static final Logger LOGGER = Logger.getLogger(XmlHttpLiveEventServiceDao.class);

  /**
   * Path used to call live event service when a communication has been rescheduled.
   */
  protected static final String LIVE_EVENT_URL_RESCHEDULE = "/event/reschedule";

  /**
   * Path used to call live event service when a communication has been cancelled.
   */
  protected static final String LIVE_EVENT_URL_CANCEL = "/event/cancel";

  /**
   * Path used to call live event service when a communication has been recorded.
   */
  protected static final String LIVE_EVENT_URL_RECORDED = "/event/recorded";

  /**
   * Path used to call live event service when a communication has failed processing.
   */
  protected static final String LIVE_EVENT_URL_ERROR = "/event/error";

  @Autowired
  @Qualifier(value = "liveEventService")
  private RestTemplate restTemplate;

  @Value("${service.liveEventServiceUrl}")
  private String serviceBaseUrl;

  @Autowired
  private LiveEventRequestConverter converter;

  private MappingJacksonHttpMessageConverter messageConverter;

  /**
   * @return the converter
   */
  public LiveEventRequestConverter getConverter() {
    return converter;
  }

  /**
   * @param converter the converter to set
   */
  public void setConverter(final LiveEventRequestConverter converter) {
    this.converter = converter;
  }

  public String getServiceBaseUrl() {
    return serviceBaseUrl;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  /**
   * @return the messageConverter
   */
  public MappingJacksonHttpMessageConverter getMessageConverter() {
    if (messageConverter == null) {
      messageConverter = new MappingJacksonHttpMessageConverter();
    }
    return messageConverter;
  }

  /**
   * @param messageConverter the messageConverter to set
   */
  public void setMessageConverter(final MappingJacksonHttpMessageConverter messageConverter) {
    this.messageConverter = messageConverter;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void reschedule(final Communication communication) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Inform LES that communication [" + communication.getId() + "] has been rescheduled.");
    }

    RescheduleCommunicationDto request = converter.convertForReschedule(communication);
    String url = serviceBaseUrl + LIVE_EVENT_URL_RESCHEDULE;

    setRequestAsJson();
    restTemplate.postForLocation(url, request);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void cancel(final Communication communication) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Inform LES that communication [" + communication.getId() + "] has been cancelled.");
    }

    CancelCommunicationDto request = converter.convertForCancel(communication);
    String url = serviceBaseUrl + LIVE_EVENT_URL_CANCEL;

    setRequestAsJson();
    restTemplate.postForLocation(url, request);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void recordedSuccess(final Long communicationId) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Inform LES that communication [" + communicationId + "] has been recorded.");
    }

    VideoUploadCompleteCommunicationDto request = converter.convertForVideoUploadComplete(communicationId);
    String url = serviceBaseUrl + LIVE_EVENT_URL_RECORDED;

    setRequestAsJson();
    restTemplate.postForLocation(url, request);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void recordedError(final Long communicationId) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Inform LES that communication [" + communicationId + "] has failed transcoding.");
    }

    VideoUploadErrorCommunicationDto request = converter.convertForVideoUploadError(communicationId);
    String url = serviceBaseUrl + LIVE_EVENT_URL_ERROR;

    setRequestAsJson();
    restTemplate.postForLocation(url, request);
  }

  /**
   * Set the request to be sent as Json.
   */
  private void setRequestAsJson() {
    List<HttpMessageConverter<?>> list = new ArrayList<HttpMessageConverter<?>>();
    list.add(getMessageConverter());
    restTemplate.setMessageConverters(list);
  }
}