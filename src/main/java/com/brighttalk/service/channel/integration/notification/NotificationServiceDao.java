/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: LiveStateServiceDao.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.notification;

import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * Interface to define the required methods for the interactions with the Notification Service.
 */
public interface NotificationServiceDao {

  /**
   * Inform Notification service that a webcast has been set to processing.
   * 
   * @param communication that has been set to processing
   */
  void inform(Communication communication);

}