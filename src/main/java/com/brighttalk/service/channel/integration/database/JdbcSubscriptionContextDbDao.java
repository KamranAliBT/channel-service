/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: JdbcSubscriptionContextDbDao.java 95321 2015-05-19 14:59:11Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.channel.SubscriptionContext;
import com.brighttalk.service.channel.business.domain.channel.SubscriptionLeadType;

/**
 * Spring JDBC Database DAO implementation of {@link SubscriptionContextDbDao} which uses JDBC via Spring's
 * SimpleJDBCTemplate to access the Database.
 */
@Repository
public class JdbcSubscriptionContextDbDao implements SubscriptionContextDbDao {

  private static final Logger LOGGER = Logger.getLogger(JdbcSubscriptionContextDbDao.class);
  private static final String SUBSCRIPTION_ID_COLUMN_NAME = "subscription_id";
  private static final String ID_COLUMN_NAME = "id";
  private static final String LEAD_TYPE_COLUMN_NAME = "lead_type";
  private static final String LEAD_CONTEXT_COLUMN_NAME = "lead_context";
  private static final String ENGAGEMENT_SCORE_COLUMN_NAME = "engagement_score";
  private static final String CREATED_COLUMN_NAME = "created";
  private static final String LAST_UPDATED_COLUMN_NAME = "last_updated";
  private static final String SUBSCRIPTION_ID_PARAM_NAME = "subscriptionId";
  private static final String LAST_UPDATED_PARAM_NAME = "lastUpdated";

  private static final String SELECT_CONTEXT_SQL_QUERY = ""
      + "SELECT "
      + SUBSCRIPTION_ID_COLUMN_NAME + ", "
      + LEAD_TYPE_COLUMN_NAME + ", "
      + LEAD_CONTEXT_COLUMN_NAME + ", "
      + ENGAGEMENT_SCORE_COLUMN_NAME + ", "
      + CREATED_COLUMN_NAME + ", "
      + LAST_UPDATED_COLUMN_NAME
      + " FROM channel.subscription_context "
      + "WHERE " + SUBSCRIPTION_ID_COLUMN_NAME + " = :" + SUBSCRIPTION_ID_PARAM_NAME;

  private static final String UPDATE_SUBSCRIPTION_QUERY = ""
      + "UPDATE "
      + " subscription "
      + " SET " + LAST_UPDATED_COLUMN_NAME + " = :" + LAST_UPDATED_PARAM_NAME
      + " WHERE " + ID_COLUMN_NAME + " = :" + SUBSCRIPTION_ID_PARAM_NAME;

  /**
   * Thread-safe object that simplifies inserting new subscription context records by removing the need to supply column
   * names.
   * 
   * @see SimpleJdbcInsert
   */
  private SimpleJdbcInsert insertSubscriptionContext;
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Set this DAO's data source.
   * <p>
   * Follows Spring best-practice approach of instantiating the instances of Spring DAO template objects used by this
   * class from the supplied data-source.
   * 
   * @param dataSource the data source to set
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    insertSubscriptionContext = new SimpleJdbcInsert(dataSource).withTableName("subscription_context");
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * {@inheritDoc}
   * <p>
   * This implementation updates the last updated date/time of the context’s associated subscription record on creation
   * of the context. This is a requirement to ensure Channel Owner Reporting API Get Channel Subscribers accurately
   * report channel subscriptions with subscription context when only subscription context created without updating the
   * subscription record, for more details see (https://brighttalktech.jira.com/browse/AP-907).
   */
  @Override
  public void create(SubscriptionContext context) {
    Validate.notNull(context, "The subscription context must be provided.");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Creating database record for subscription context [" + context + "]");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(SUBSCRIPTION_ID_COLUMN_NAME, context.getSubscriptionId());
    params.addValue(LEAD_TYPE_COLUMN_NAME, context.getLeadType().getType());
    params.addValue(LEAD_CONTEXT_COLUMN_NAME, context.getLeadContext());
    params.addValue(ENGAGEMENT_SCORE_COLUMN_NAME, context.getEngagementScore());
    params.addValue(CREATED_COLUMN_NAME, new Date());
    params.addValue(LAST_UPDATED_COLUMN_NAME, new Date());

    insertSubscriptionContext.execute(params);

    // Update the subscription context's subscription last updated date to current data/time, this is a pragmatic
    // solution required to ensure Channel Owner Reporting API Get Channel Subscribers accurately report channel
    // subscriptions with subscription context when only subscription context created without updating the channel
    // subscriber subscription record, for more details see (https://brighttalktech.jira.com/browse/AP-907).
    updateSubscriptionLastUpdated(context.getSubscriptionId());
  }

  /** {@inheritDoc} */
  @Override
  public SubscriptionContext findBySubscriptionId(Long subscriptionId) {
    Validate.notNull(subscriptionId, "The subscription Id must be provided.");
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding a subscription context by the supplied subscription Id [" + subscriptionId + "]");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(SUBSCRIPTION_ID_PARAM_NAME, subscriptionId);

    SubscriptionContext context;
    try {
      context = jdbcTemplate.queryForObject(SELECT_CONTEXT_SQL_QUERY, params, new ContextMapper());
    } catch (EmptyResultDataAccessException e) {
      return null;
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully found a subscription context [" + context + "]");
    }
    return context;
  }

  /**
   * Updates the supplied subscription last updated date/time.
   * 
   * @param subscriptionId The Id of the subscription to update.
   */
  private void updateSubscriptionLastUpdated(Long subscriptionId) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Updating the last updated date of the supplied subscription [" + subscriptionId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(SUBSCRIPTION_ID_PARAM_NAME, subscriptionId);
    params.addValue(LAST_UPDATED_PARAM_NAME, new Date());

    jdbcTemplate.update(UPDATE_SUBSCRIPTION_QUERY, params);
  }

  /**
   * Default implementation for the subscription context row mapper.
   */
  private class ContextMapper implements RowMapper<SubscriptionContext> {
    /** {@inheritDoc} */
    @Override
    public SubscriptionContext mapRow(ResultSet rs, int rowNum) throws SQLException {
      Long subscriptionId = rs.getLong(SUBSCRIPTION_ID_COLUMN_NAME);
      String leadType = rs.getString(LEAD_TYPE_COLUMN_NAME);
      String leadContext = rs.getString(LEAD_CONTEXT_COLUMN_NAME);
      String engagementScore = rs.getString(ENGAGEMENT_SCORE_COLUMN_NAME);
      SubscriptionContext context = new SubscriptionContext(SubscriptionLeadType.getType(leadType), leadContext,
          engagementScore);
      context.setSubscriptionId(subscriptionId);
      return context;
    }

  }
}
