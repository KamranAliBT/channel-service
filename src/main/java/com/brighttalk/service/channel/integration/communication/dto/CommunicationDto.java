/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: CommunicationDto.java 89869 2015-02-12 17:26:56Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.communication.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.brighttalk.service.channel.business.domain.DefaultToStringStyle;

/**
 * Represents communication DTO.
 */
@XmlRootElement(name = "communication")
@XmlAccessorType(XmlAccessType.FIELD)
public class CommunicationDto {

  @XmlAttribute
  private Long id;
  private String title;
  private String description;

  /** Default constructor. */
  public CommunicationDto() {
  }

  /**
   * @param id The communication Id.
   */
  public CommunicationDto(Long id) {
    this.id = id;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    ToStringBuilder builder = new ToStringBuilder(this, new DefaultToStringStyle());
    builder.append("id", id);
    builder.append("title", title);
    builder.append("description", description);
    return builder.toString();
  }
}
