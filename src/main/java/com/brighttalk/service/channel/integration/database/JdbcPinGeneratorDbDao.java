/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcResourceDbDao.java 57924 2012-11-26 13:53:01Z apannone $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Spring JDBC Database DAO implementation of PinManager which uses JDBC via Spring's SimpleJDBCTemplate to access the
 * Database.
 */
@Repository
public class JdbcPinGeneratorDbDao extends JdbcDao implements PinGeneratorDbDao {

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcPinGeneratorDbDao.class);

  /* @formatter:off */
  //CHECKSTYLE:OFF
  private static final String PIN_NUMBER_COUNT_SELECT_QUERY = "SELECT "
      + "  COUNT(*) AS count "
      + "FROM "
      + "  communication c "
      + "WHERE "
      + "  c.pin_number = :pinNumber ";
  // CHECKSTYLE:ON
  /* @formatter:on */

  /** JDBC template used to access the database. */
  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {

    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isPinUsed(final String pinNumber) {

    LOGGER.debug("Checking if pin [" + pinNumber + "] has already been used.");

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("pinNumber", pinNumber);
    // CHECKSTYLE:ON

    long communicationsCount = 0L;

    try {
      communicationsCount = queryForLong(jdbcTemplate, PIN_NUMBER_COUNT_SELECT_QUERY, params);
    } catch (EmptyResultDataAccessException e) {
      return true;
    }

    LOGGER.debug("Found [" + communicationsCount + "] communications with pin [" + pinNumber + "].");

    return communicationsCount > 0;
  }
}
