/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ChannelFeedDbDao.java 83974 2014-09-29 11:38:04Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.util.Date;
import java.util.List;

import com.brighttalk.service.channel.business.domain.channel.ChannelFeedSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.Communication;

/**
 * DAO for database operations around retrieving a feed of {@link Communication communications}.
 */
public interface ChannelFeedDbDao {

  /**
   * Get the total number of communications that will be in the channel feed.
   * 
   * @param channelId the id of the channel to get the number of communications for
   * @param criteria Search criteria to use.
   * 
   * @return the total number of communications in the feed.
   */
  int findTotalCommunicationsInFeed(Long channelId, ChannelFeedSearchCriteria criteria);

  /**
   * Find a paginated list of communications for the channel feed, ordered by latest scheduled date first.
   * 
   * @param channelId the id of the channel
   * @param criteria Search criteria to use.
   * 
   * @return a page worth of communications or an empty list if there are no communications in the feed.
   */
  List<Communication> findFeed(Long channelId, ChannelFeedSearchCriteria criteria);

  /**
   * Find the Date and time of any features webcasts.
   * 
   * There can be multiple featured webcasts that are closest to now
   *
   * @param channelId the id of the channel.
   * @param criteria Search criteria to use.
   * 
   * @return a map containing the scheduled date and id of the featured communication. Keys are 'scheduledDate' and
   * 'communicationId'.
   */
  Date findFeaturedDateTime(Long channelId, ChannelFeedSearchCriteria criteria);

  /**
   * Get the number of communications booked after scheduled date.
   * 
   * @param channelId the id of the channel to get the number of communication for.
   * @param scheduledDate the date to find communications afer.
   * @param criteria Search criteria to use.
   * 
   * @return the number of communications.
   */
  int findTotalCommunicationsScheduledAfterDate(Long channelId, Date scheduledDate, ChannelFeedSearchCriteria criteria);
}
