/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: XmlHttpBookingServiceDao.java 71274 2013-11-26 14:01:47Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.booking;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.error.BookingCapacityExceededException;
import com.brighttalk.service.channel.integration.booking.dto.HDBookingDto;
import com.brighttalk.service.channel.integration.booking.dto.LivePhoneNumberDto;
import com.brighttalk.service.channel.integration.booking.dto.converter.BookingRequestConverter;
import com.brighttalk.service.channel.integration.booking.dto.converter.LivePhoneNumberConverter;

/**
 * Implementation of the Dao to interact with the Booking HD Service.
 * 
 * Objects will be converted to json and transmitted over HTTP
 */
public class XmlHttpHDBookingServiceDao implements HDBookingServiceDao {

  private static final Logger LOGGER = Logger.getLogger(XmlHttpHDBookingServiceDao.class);

  /** add / update /delete single booking request path. */
  protected static final String BOOK_REQUEST_PATH = "/booking";

  /** delete all bookings request path. */
  protected static final String DELETE_ALL_REQUEST_PATH = "/bookings";

  /** Booking type for create */
  protected static final String BOOKING_TYPE_CREATE = "create";

  /** Booking type for update */
  protected static final String BOOKING_TYPE_UPDATE = "update";

  /** Capacity exceeded error code */
  protected static final String CAPACITY_EXCEEDED_ERROR_CODE = "CapacityExceeded";

  /** System error error code */
  protected static final String SYSTEM_ERROR_CODE = "SystemError";

  @Autowired
  private BookingRequestConverter converter;

  @Autowired
  private LivePhoneNumberConverter livePhoneNumberConverter;

  private RestTemplate restTemplate;

  private String serviceBaseUrl;

  public BookingRequestConverter getConverter() {
    return converter;
  }

  public void setConverter(final BookingRequestConverter converter) {
    this.converter = converter;
  }

  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  public void setRestTemplate(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public String getServiceBaseUrl() {
    return serviceBaseUrl;
  }

  public void setServiceBaseUrl(final String serviceBaseUrl) {
    this.serviceBaseUrl = serviceBaseUrl;
  }

  public void setLivePhoneNumberConverter(final LivePhoneNumberConverter livePhoneNumberConverter) {
    this.livePhoneNumberConverter = livePhoneNumberConverter;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String createBooking(final Communication communication) {
    LOGGER.info("Create booking of HD communication [" + communication.getId() + "] in channel ["
        + communication.getMasterChannelId() + "] in MPHD booking service.");

    return processBooking(communication, BOOKING_TYPE_CREATE);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String updateBooking(final Communication communication) {
    LOGGER.info("Updating booking for communication [" + communication.getId() + "] in channel ["
        + communication.getMasterChannelId() + "] in MPHD booking service.");

    return processBooking(communication, BOOKING_TYPE_UPDATE);
  }

  private String processBooking(final Communication communication, final String bookingType) {

    String url = getRequestUrl(communication);
    HDBookingDto request = converter.convertForCreateAndUpdate(communication);

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

    HttpEntity<HDBookingDto> entity = new HttpEntity<HDBookingDto>(request, headers);

    HttpMethod httpMethod = HttpMethod.POST;
    if (bookingType.equals(BOOKING_TYPE_UPDATE)) {
      httpMethod = HttpMethod.PUT;
    }

    try {
      ResponseEntity<LivePhoneNumberDto> response = restTemplate.exchange(url, httpMethod, entity,
          LivePhoneNumberDto.class);

      LOGGER.debug("[" + bookingType + "] of booking successfull for communication [" + communication.getId() + "]");
      return livePhoneNumberConverter.convert(response.getBody());
    } catch (HttpStatusCodeException hsce) {

      if (hsce.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
        throw new BookingCapacityExceededException(CAPACITY_EXCEEDED_ERROR_CODE);
      }
      LOGGER.error("Failure to [" + bookingType + "] booking for HD communication [" + communication.getId() + "].");
      throw new ApplicationException(SYSTEM_ERROR_CODE);

    } catch (Exception e) {
      LOGGER.error("Failure to [" + bookingType + "] booking for HD communication [" + communication.getId() + "].");
      throw new ApplicationException(SYSTEM_ERROR_CODE);
    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteBooking(final Communication communication) {
    LOGGER.info("Cancelling booking of communication [" + communication.getId() + "] ");

    String url = getRequestUrl(communication);

    try {
      restTemplate.delete(url);

    } catch (HttpStatusCodeException hsce) {

      if (hsce.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
        throw new BookingCapacityExceededException(CAPACITY_EXCEEDED_ERROR_CODE);
      }
      LOGGER.error("Failure to delete HD communication [" + communication.getId() + "].");
      throw new ApplicationException(SYSTEM_ERROR_CODE);

    } catch (Exception e) {
      LOGGER.error("Failure to delete HD communication [" + communication.getId() + "].");
      throw new ApplicationException(SYSTEM_ERROR_CODE);
    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteBookings(final Long channelId) {
    LOGGER.debug("Deleting all the HD bookings for channel [" + channelId + "] ");

    String requestUrl = serviceBaseUrl + "/channel/" + channelId + DELETE_ALL_REQUEST_PATH;

    restTemplate.delete(requestUrl);
  }

  /**
   * Build the request url .../channel/{channelId}/webcast/{webcastId}/booking
   * 
   * @param communication to get the ids from
   * @return the request url with the ids
   */
  private String getRequestUrl(final Communication communication) {
    String requestUrl = serviceBaseUrl + "/channel/" + communication.getChannelId() + "/webcast/"
        + communication.getId() + BOOK_REQUEST_PATH;

    LOGGER.debug("Request url: " + requestUrl);
    return requestUrl;

  }
}