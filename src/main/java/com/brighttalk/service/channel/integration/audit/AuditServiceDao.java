/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: AuditServiceDao.java 76503 2014-04-16 12:25:46Z jbridger $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * Interface to define the required methods for the interactions with the Audit Service.
 */
public interface AuditServiceDao {

  /**
   * Create an audit record.
   * 
   * @param auditRecordDto Audit record to create.
   */
  void create(AuditRecordDto auditRecordDto);
}