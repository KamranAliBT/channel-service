/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2015.
 * All Rights Reserved.
 * $Id: JdbcFeatureDbDao.java 95397 2015-05-21 11:06:50Z afernandes $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.Feature;
import com.brighttalk.service.channel.business.domain.Feature.Type;
import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.presentation.error.UnsupportedFeatureException;

/**
 * Spring JDBC Database DAO implementation of {@link FeatureDbDao} which uses JDBC via Spring's SimpleJDBCTemplate to
 * access the Database.
 */
@Repository
@Primary
public class JdbcFeatureDbDao implements FeatureDbDao {

  private static final String CHANNEL_ID_FIELD = "channel_id";
  private static final String CHANNEL_ID_PARAM_NAME = "channelId";
  private static final String CHANNEL_TYPE_ID_PARAM_NAME = "channelTypeId";
  private static final String NAME_FIELD = "name";
  private static final String NAME_PARAM_NAME = "names";
  private static final String OVERRIDE_VALUE_FILED = "override_value";
  private static final String OVERRIDE_ENABLED_FIELD = "override_enabled";
  private static final String DEFAULT_VALUE_FIELD = "default_value";
  private static final String DEFAULT_ENABLED_FIELD = "default_enabled";
  private static final String USE_OVERRIDE_FIELD = "use_override";

  /* @formatter:off */
  //CHECKSTYLE:OFF
  
  private static final String CHANNEL_DEFAULT_FEATURES_SELECT_QUERY = ""
      + "SELECT  "
      + "  name, "
      + "  default_enabled, "
      + "  default_value "
      + "FROM "
      + "  feature_type "
      + "WHERE "
      + "  channel_type_id = :" + CHANNEL_TYPE_ID_PARAM_NAME;
  
  private static final String CHANNEL_FEATURE_OVERRIDE_DELETE_QUERY = ""
      + "UPDATE "
      + "  feature_override "
      + "SET "
      + "  is_active = 0 "
      + "WHERE "
      + "  channel_id = :" + CHANNEL_ID_PARAM_NAME
      + "  AND name IN (:" + NAME_PARAM_NAME + ")";

  private static final String CHANNELS_FEATURES_SELECT_QUERY = ""
      + "SELECT "
      + "  c.id AS channel_id, "
      + "  ft.name AS name, "
      + "  ft.default_enabled AS default_enabled, "
      + "  ft.default_value AS default_value, "
      + "  fo.is_active AS use_override, "
      + "  fo.override_enabled AS override_enabled, "
      + "  fo.override_value AS override_value "
      + "FROM "
      + "  channel c "
      + "  JOIN feature_type ft ON (ft.channel_type_id = c.channel_type_id) "
      + "  LEFT JOIN feature_override fo ON ( "
      + "    fo.channel_id = c.id "
      + "    AND fo.name = ft.name "
      + "    AND fo.is_active = 1) "
      + "WHERE "
      + "  c.id IN (:channelIds) ";

  private static final String CHANNEL_FEATURE_INSERT_QUERY = ""
      + "INSERT INTO feature_override ( "
      + "   channel_id, "
      + "   name, "
      + "   override_enabled, "
      + "   override_value "
      + ") "
      + "VALUES ( "
      + "   :channelId, "
      + "   :name, "
      + "   :overrideEnabled, "
      + "   :overrideValue "
      + ") "
      + "ON DUPLICATE KEY UPDATE "
      + "  is_active = 1, "
      + "  override_enabled = VALUES(override_enabled), "
      + "  override_value = VALUES(override_value)";

  // CHECKSTYLE:ON
  /* @formatter:on */

  private static final Logger LOGGER = Logger.getLogger(JdbcFeatureDbDao.class);

  private NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Feature> findDefaultFeatureByChannelTypeId(final Long channelTypeId) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding all the channel default features for supplied channel type [" + channelTypeId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_TYPE_ID_PARAM_NAME, channelTypeId);

    List<Feature> defaultFeatures = jdbcTemplate.query(CHANNEL_DEFAULT_FEATURES_SELECT_QUERY, params,
        new FeatureTypeRowMapper());

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully found [" + defaultFeatures + "] for channel type id [" + channelTypeId + "].");
    }

    return defaultFeatures;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteFeatureOverridesByNames(final Long channelId, final List<Type> names) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleting all the supplied channel  [" + channelId + "] features by names [" + names + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(NAME_PARAM_NAME, getFeaturesDbNames(names));
    params.addValue(CHANNEL_ID_PARAM_NAME, channelId);

    int deletedRows = jdbcTemplate.update(CHANNEL_FEATURE_OVERRIDE_DELETE_QUERY, params);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Successfully soft deleted [" + deletedRows + "] features from the supplied channel  [" + channelId
          + "].");
    }
  }

  /** {@inheritDoc} */
  @Override
  public void loadFeatures(final Channel channel) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Loading features for channel [" + channel.getId() + "].");
    }

    channel.setFeatures(findByChannelIds(channel.getId()));

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + channel.getFeatures().size() + "] features.");
    }
  }

  /** {@inheritDoc} */
  @Override
  public void loadFeatures(final List<Channel> channels) {

    Map<Long, Channel> mappedChannels = getMappedChannels(channels);
    List<Long> channelIds = new ArrayList<>(mappedChannels.keySet());

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Loading features for channels [" + channelIds + "].");
    }

    List<Feature> features = findByChannelIds(channelIds.toArray(new Long[channelIds.size()]));

    for (Feature feature : features) {
      mappedChannels.get(feature.getChannelId()).addFeature(feature);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + features.size() + "] features.");
    }
  }

  /** {@inheritDoc} */
  @Override
  public List<Feature> findByChannelId(final Long channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding features for channel [" + channelId + "].");
    }

    List<Feature> features = findByChannelIds(channelId);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + features.size() + "] features.");
    }

    return features;
  }

  private List<Feature> findByChannelIds(final Long... channelIds) {

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("channelIds", Arrays.asList(channelIds));

    return jdbcTemplate.query(CHANNELS_FEATURES_SELECT_QUERY, params, new FeatureRowMapper());
  }

  /** {@inheritDoc} */
  @Override
  public void update(final Channel channel, final List<Feature> features) {

    List<SqlParameterSource> params = new ArrayList<>();

    for (Feature feature : features) {
      MapSqlParameterSource param = new MapSqlParameterSource();
      param.addValue("channelId", channel.getId());
      param.addValue("name", feature.getName().getDbName());
      param.addValue("overrideEnabled", feature.isEnabled());
      param.addValue("overrideValue", feature.getValue());
      params.add(param);
    }

    jdbcTemplate.batchUpdate(CHANNEL_FEATURE_INSERT_QUERY, params.toArray(new SqlParameterSource[params.size()]));
  }

  /**
   * Feature row mappper.
   */
  private class FeatureRowMapper implements RowMapper<Feature> {

    /** {@inheritDoc} */
    @Override
    public Feature mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      Feature.Type type;

      try {
        type = Feature.Type.convertDbName(resultSet.getString(NAME_FIELD));
      } catch (UnsupportedFeatureException e) {
        type = Type.UNKNOWN;
      }

      Boolean useOverride = resultSet.getBoolean(USE_OVERRIDE_FIELD);

      Feature feature = new Feature(type);
      feature.setChannelId(resultSet.getLong(CHANNEL_ID_FIELD));
      feature.setIsEnabled(resultSet.getBoolean(useOverride ? OVERRIDE_ENABLED_FIELD : DEFAULT_ENABLED_FIELD));
      feature.setValue(feature.isEnabled() ? resultSet.getString(useOverride ? OVERRIDE_VALUE_FILED
          : DEFAULT_VALUE_FIELD) : null);

      return feature;
    }
  }

  /**
   * {@link RowMapper} that build a default {@link Feature} type.
   */
  private class FeatureTypeRowMapper implements RowMapper<Feature> {

    @Override
    public Feature mapRow(final ResultSet rs, final int rowNum) throws SQLException {
      Feature.Type type;
      try {
        type = Feature.Type.convertDbName(rs.getString(NAME_FIELD));
      } catch (UnsupportedFeatureException e) {
        type = Type.UNKNOWN;
      }

      Feature feature = new Feature(type);
      feature.setIsEnabled(rs.getBoolean(DEFAULT_ENABLED_FIELD));
      feature.setValue(rs.getString(DEFAULT_VALUE_FIELD));
      return feature;
    }

  }

  private Map<Long, Channel> getMappedChannels(final List<Channel> channels) {

    Map<Long, Channel> mappedChannels = new HashMap<Long, Channel>();

    for (Channel channel : channels) {
      mappedChannels.put(channel.getId(), channel);
    }

    return mappedChannels;
  }

  private List<String> getFeaturesDbNames(final List<Type> types) {
    List<String> dbNames = new ArrayList<>();
    for (Type type : types) {
      dbNames.add(type.getDbName());
    }
    return dbNames;
  }

}