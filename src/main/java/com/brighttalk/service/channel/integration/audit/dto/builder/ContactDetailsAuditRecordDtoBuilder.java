/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2014.
 * All Rights Reserved.
 * $Id: ContactDetailsAuditRecordDtoBuilder.java 97400 2015-07-01 08:36:42Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.audit.dto.builder;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.integration.audit.dto.AuditRecordDto;

/**
 * Helper class for creating {@link AuditRecordDto} instances for contact details audit events.
 */
@Component
public class ContactDetailsAuditRecordDtoBuilder extends AuditRecordDtoBuilder {

  /**
   * {@inheritDoc}
   */
  @Override
  public AuditRecordDto create(final Long userId, final Long channelId, final Long communicationId,
      final String actionDescription) {
    return createAuditRecordDto(userId, channelId, communicationId, AuditEntity.CONTACT_DETAILS, AuditAction.CREATE,
        actionDescription);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AuditRecordDto update(final Long userId, final Long channelId, final Long communicationId,
      final String actionDescription) {
    return createAuditRecordDto(userId, channelId, communicationId, AuditEntity.CONTACT_DETAILS, AuditAction.UPDATE,
        actionDescription);
  }
}
