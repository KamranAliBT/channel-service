/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationDbDao.java 99258 2015-08-18 10:35:43Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.brighttalk.common.error.ApplicationException;
import com.brighttalk.common.user.Session;
import com.brighttalk.service.channel.business.domain.BaseSearchCriteria;
import com.brighttalk.service.channel.business.domain.PaginatedList;
import com.brighttalk.service.channel.business.domain.Rendition;
import com.brighttalk.service.channel.business.domain.channel.ChannelStatistics;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.business.domain.communication.Communication.PublishStatus;
import com.brighttalk.service.channel.business.domain.communication.Communication.Status;
import com.brighttalk.service.channel.business.domain.communication.CommunicationStatistics;
import com.brighttalk.service.channel.business.domain.communication.CommunicationSyndication;
import com.brighttalk.service.channel.business.domain.communication.CommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria;
import com.brighttalk.service.channel.business.domain.communication.MyCommunicationsSearchCriteria.CommunicationStatusFilterType;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link CommunicationDbDao} which uses JDBC via Spring's SimpleJDBCTemplate
 * to access the Database.
 */
@Repository
@Primary
public class JdbcCommunicationDbDao extends AbstractJdbcCommunicationDbDao implements CommunicationDbDao {

  private static final String SIZE_VALUE = "size";
  private static final String OFFSET_VALUE = "offset";
  private static final String TARGET_ID_SQL_PARAMETER = "targetId";
  private static final String TYPE_SQL_PARAMETER = "type";
  private static final String CHANNEL_ID_SQL_PARAMETER = "channelId";
  private static final String COMMUNICATION_ID_SQL_PARAMETER = "id";
  private static final String TITLE_SQL_PARAMETER = "title";
  private static final String DESCRIPTION_SQL_PARAMETER = "description";
  private static final String KEYWORDS_SQL_PARAMETER = "keywords";
  private static final String PRESENTER_SQL_PARAMETER = "presenter";
  private static final String DURATION_SQL_PARAMETER = "duration";
  private static final String SCHEDULED_SQL_PARAMETER = "scheduled";
  private static final String TIMEZONE_SQL_PARAMETER = "timezone";
  private static final String STATUS_SQL_PARAMETER = "status";
  private static final String PUBLISH_STATUS_SQL_PARAMETER = "publishStatus";
  private static final String MASTER_CHANNEL_ID_SQL_PARAMETER = "masterChannelId";
  private static final String PROVIDER_SQL_PARAMETER = "provider";
  private static final String FORMAT_SQL_PARAMETER = "format";
  private static final String BOOKING_DURATION_SQL_PARAMETER = "booking_duration";
  private static final String PIN_NUMBER_SQL_PARAMETER = "pin_number";
  private static final String LIVE_URL_SQL_PARAMETER = "live_url";
  private static final String LIVE_PHONE_NUMBER_SQL_PARAMETER = "live_phone_number";
  private static final String THUMBNAIL_URL_SQL_PARAMETER = "thumbnail";
  private static final String PREVIEW_URL_SQL_PARAMETER = "preview_url";
  private static final String CREATED_SQL_PARAMETER = "created";
  private static final String LAST_UPDATED_SQL_PARAMETER = "lastUpdated";

  /* @formatter:off */
  /**
   * Named fields for communication create query.
   */
  private static final String COMMUNICATION_CREATE_FIELDS_TO_SET = ""
    + "title = :title, "
    + "description = :description, "    
    + "keywords = :keywords, "     
    + "presenter = :presenter, "     
    + "duration = :duration, "     
    + "scheduled = :scheduled, "     
    + "timezone = :timezone, "     
    + "status = :status, "     
    + "publish_status = :publishStatus, "     
    + "created = :created, "     
    + "last_updated = :lastUpdated, "     
    + "master_channel_id = :masterChannelId, "     
    + "provider = :provider, "
    + "format = :format, "
    + "booking_duration = :booking_duration, "
    + "thumbnail = :thumbnail, "     
    + "preview_url = :preview_url, "     
    + "pin_number = :pin_number, "
    + "live_url = :live_url, "
    + "live_phone_number = :live_phone_number ";
  
  /**
   * Named fields for communication update query.
   */
  private static final String COMMUNICATION_UPDATE_FIELDS_TO_SET = ""
    + "title = :title, "
    + "description = :description, "    
    + "keywords = :keywords, "     
    + "presenter = :presenter, "     
    + "duration = :duration, "     
    + "scheduled = :scheduled, "     
    + "timezone = :timezone, "     
    + "status = :status, "     
    + "publish_status = :publishStatus, "     
    + "last_updated = :lastUpdated, "     
    + "thumbnail = :thumbnail, "     
    + "preview_url = :preview_url, "     
    + "booking_duration = :booking_duration ";
  
 /**
  * Named fields for encoding count update query.
  */
  private static final String ENCODING_COUNT_UPDATE_FIELDS_TO_SET = ""
     + "last_updated = :lastUpdated, "     
     + "encoding_count = encoding_count + 1";
 
  /**
   * Named fields for channel asset query.
   */
  private static final String CHANNEL_ASSET_FILEDS_TO_SET = ""
    + "channel_id = :channelId, "
    + "target_id = :targetId, "
    + "type = :type, "
    + "created = :created, "     
    + "last_updated = :lastUpdated ";
  /* @formatter:on */

  /**
   * Communication database table - fields used in selecting a communication with overrides. The loading of the
   * overrides is required to get values for syndicated in content. That's what the IFNULL is used for - if there is a
   * value in the overrides table it is used, otherwise the query defaults to communication table.
   */
  protected static final String COMMUNICATION_FIELDS_WITH_OVERRIDES_FOR_SELECT = " c.id AS id, "
      + " c.master_channel_id AS master_channel_id, " + " IFNULL(cdo.title, c.title) AS title, "
      + " IFNULL(cdo.description, c.description) AS description, " + " IFNULL(cdo.keywords, c.keywords) AS keywords, "
      + " IFNULL(cdo.presenter, c.presenter) AS authors, " + " c.duration AS duration, "
      + " c.scheduled AS scheduled, " + " c.timezone AS timezone, " + " c.status AS status, "
      + " c.publish_status AS publish_status, " + " c.rerun_count AS rerun_count, "
      + " c.master_channel_id AS master_channel_id, " + " c.thumbnail AS thumbnail_url, "
      + " c.preview_url AS preview_url, " + " c.pin_number AS pin_number, "
      + " c.live_phone_number AS live_phone_number, " + " c.live_url AS live_url, " + " c.format AS format, "
      + " c.provider AS provider, " + " c.created AS created, " + " c.last_updated AS last_updated, "
      + "c.booking_duration AS booking_duration, c.encoding_count AS encoding_count ";

  // CHECKSTYLE:OFF
  // @formatter:off
   private static final String COMMUNICATIONS_SELECT_QUERY =
     "SELECT "
     + COMMUNICATION_FIELDS_WITH_OVERRIDES_FOR_SELECT
     + ","
     + COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT
     + ","
     + "  cs.rating AS cs_rating, "
     + "  cs.num_views AS cs_viewings "
     + "FROM "
     + "  asset a   "
     + "  JOIN communication c ON (a.target_id  = c.id ) "
     + "  JOIN channel ch ON (ch.id = a.channel_id) "
     + "  JOIN communication_configuration cc ON (cc.channel_id = a.channel_id AND cc.communication_id = a.target_id) "
     + "  LEFT JOIN communication_statistics cs ON (cs.channel_id = cc.channel_id AND cs.communication_id = c.id) "
     + "  LEFT JOIN communication_details_override cdo ON (cdo.channel_id = a.channel_id AND cdo.communication_id = a.target_id) "
     + "WHERE "
     + " CONCAT(a.channel_id, '-', a.target_id) IN (:channelAndCommunicationIds) "
     + " AND a.type = 'communication' "
     + " AND a.is_active = 1"
     + " AND ch.is_active = 1"
     + " AND c.publish_status = 'published' "
     + " AND cc.visibility = 'public' "
     + " AND c.status NOT IN ('deleted', 'cancelled') "
     + " AND "
     + getApprovedCommunicationsClause()
     +  "ORDER BY FIELD(CONCAT(a.channel_id, '-', a.target_id),:channelAndCommunicationIds)"
     + "LIMIT"
     + "  :offset, :size";
   // @formatter:on  
  // CHECKSTYLE:ON

  // CHECKSTYLE:OFF
  // @formatter:off
  protected final String WEBCAST_TOTALS_FOR_SELECT =""
  + "SELECT "
  + "  SUM(cs.upcoming_count) AS upcoming_count, "
  + "  SUM(cs.live_count) AS live_count, "
  + "  SUM(cs.recorded_count) AS recorded_count "
  + "FROM "
  + "  subscription sub "
  + "  JOIN channel ch ON (ch.id = sub.channel_id) "
  + "  LEFT JOIN channel_statistics cs ON (cs.channel_id = ch.id) "
  + "WHERE "
  + " sub.user_id = :userId "
  + " AND ch.is_active = 1 "
  + " AND sub.is_active = 1";
  // @formatter:on  
  // CHECKSTYLE:ON

  // CHECKSTYLE:OFF
  // @formatter:off
  private static final String PUBLISH_SCHEDULED_QUERY ="UPDATE"
      + "  communication "
      + "SET "
      + "  publish_status = :publish_status, "
      + "  last_updated = NOW() "
      + "WHERE "
      + "  id IN (:ids) ";
  // @formatter:on  
  // CHECKSTYLE:ON

  // CHECKSTYLE:OFF
  // @formatter:off
  private static final String LOAD_COMMUNICATION_RENDITIONS ="SELECT"
      + "  cra.id, " 
      + "  cra.is_active, " 
      + "  cra.url, " 
      + "  cra.container, " 
      + "  cra.codecs, " 
      + "  cra.width, " 
      + "  cra.bitrate "
      + "FROM "
      + "  communication_rendition_asset cra "
      + "  JOIN communication_asset ca ON (ca.target_id = cra.id AND ca.type = 'video' AND ca.is_active = 1) "
      + "WHERE "
      + "  ca.communication_id = :id ";
  // @formatter:on  
  // CHECKSTYLE:ON

  // CHECKSTYLE:OFF
  // @formatter:off
  private static final String LOAD_COMMUNICATION_STATISTICS ="SELECT"
      + "  rating AS cs_rating, " 
      + "  num_ratings, " 
      + "  num_views AS cs_viewings, " 
      + "  viewed_for " 
      + "FROM "
      + "  communication_statistics "
      + "WHERE "
      + " communication_id = :communicationId "
      + "AND "
      + " channel_id = :channelId";
  // @formatter:on  
  // CHECKSTYLE:ON

  // CHECKSTYLE:OFF
  // @formatter:off
  private static final String IS_OVERLAPPING_COMMUNICATION ="SELECT "
      + "  COUNT(*) AS count "
      + "FROM "
      + "  communication "
      + "WHERE "
      + "  (UNIX_TIMESTAMP(scheduled) <= :endTime) "
      + "AND "
      +"   ((UNIX_TIMESTAMP(scheduled) + duration) >= :startTime) "
      + "AND "
      + "  master_channel_id = :masterChannelId "
      + "AND "
      + "  status NOT IN ('cancelled','deleted') "
      + "AND "
      + "  provider IN ('brighttalk','brighttalkhd')";
  // @formatter:on  
  // CHECKSTYLE:ON

  // CHECKSTYLE:OFF
  // @formatter:off
  private static final String IS_COMMUNICATION_ACTIVE_ASSET ="SELECT "
      + "  COUNT(target_id) AS count "
      + "FROM "
      + "  asset "
      + "WHERE "
      + "  channel_id = :channelId "
      + "AND "
      +"   target_id = :communicationId "
      + "AND "
      + "  type = 'communication' "
      + "AND "
      + "  is_active = 1 ";
  // @formatter:on  
  // CHECKSTYLE:ON

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcCommunicationDbDao.class);

  /** JDBC template used to access the database. */
  protected NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public Communication find(final Long communicationId) throws CommunicationNotFoundException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding communication [" + communicationId + "]");
    }

    StringBuilder sql = new StringBuilder();
    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(COMMUNICATION_FIELDS_FOR_SELECT);
    sql.append(",");
    sql.append(COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT);
    sql.append("FROM ");
    sql.append("  communication c ");
    sql.append("  JOIN `communication_configuration` cc ON (c.id = cc.communication_id AND cc.channel_id = c.master_channel_id) ");
    sql.append("WHERE ");
    sql.append("  c.id = :id ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("id", communicationId);
    // CHECKSTYLE:ON

    Communication communication = null;

    try {
      communication = jdbcTemplate.queryForObject(sql.toString(), params, new CommunicationRowMapper(domain));
    } catch (EmptyResultDataAccessException e) {
      throw new CommunicationNotFoundException(communicationId, e);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found communication [" + communication + "]");
    }
    return communication;
  }

  /** {@inheritDoc} */
  @Override
  public Communication find(final Long channelId, final Long communicationId) throws CommunicationNotFoundException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding communication [" + communicationId + "] in channel [" + channelId + "].");
    }

    StringBuilder sql = new StringBuilder();
    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(COMMUNICATION_FIELDS_FOR_SELECT);
    sql.append(",");
    sql.append(COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT);
    sql.append("FROM ");
    sql.append("  communication c ");
    sql.append("  INNER JOIN asset a ON (a.target_id = c.id AND a.channel_id = :channelId AND a.is_active = 1) ");
    sql.append("  LEFT JOIN `communication_configuration` cc ON (c.id = cc.communication_id) ");
    sql.append("WHERE ");
    sql.append("  c.id = :id ");
    sql.append("  AND cc.channel_id = :channelId ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("id", communicationId);
    params.addValue("channelId", channelId);
    // CHECKSTYLE:ON

    Communication communication = null;

    try {
      communication = jdbcTemplate.queryForObject(sql.toString(), params, new CommunicationRowMapper(domain));
    } catch (EmptyResultDataAccessException e) {
      throw new CommunicationNotFoundException(communicationId, e);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found communication [" + communication + "]");
    }
    return communication;
  }

  /** {@inheritDoc} */
  @Override
  public Communication find(final String pin, final String provider) throws CommunicationNotFoundException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding communication for pin [" + pin + "] and provider [" + provider + "].");
    }

    StringBuilder sql = new StringBuilder();
    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(COMMUNICATION_FIELDS_FOR_SELECT);
    sql.append(",");
    sql.append(COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT);
    sql.append("FROM ");
    sql.append("  communication c ");
    sql.append("  LEFT JOIN `communication_configuration` cc ON (c.id = cc.communication_id AND c.master_channel_id = cc.channel_id) ");
    sql.append("WHERE ");
    sql.append("  c.pin_number = :pin ");
    sql.append("AND ");
    sql.append("cc.is_master = 1 ");

    if (provider != null) {
      sql.append("AND c.provider = :provider ");
    }
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("pin", pin);
    if (provider != null) {
      params.addValue("provider", provider);
    }
    // CHECKSTYLE:ON

    Communication communication = null;

    try {
      communication = jdbcTemplate.queryForObject(sql.toString(), params, new CommunicationRowMapper(domain));
    } catch (EmptyResultDataAccessException e) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Communication not found for pin [" + pin + "] and provider [" + provider + "].");
      }
      throw new CommunicationNotFoundException("Communication not found for pin [" + pin + "] and provider ["
          + provider + "].", e);
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found communication [" + communication + "]");
    }
    return communication;
  }

  /**
   * Loads presenters for given communication.
   * 
   * @param communication the communication to find the presenters for
   * @return communication with updated presenters list
   */
  @Override
  public List<Session> findPresenters(final Communication communication) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Loading presenters for communication [" + communication.getId() + "].");
    }

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  c.session_id AS session_id ");
    sql.append("FROM ");
    sql.append("  can_present c ");
    sql.append("WHERE ");
    sql.append("  c.communication_id = :communicationId ");
    sql.append("  AND c.is_active = 1 ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("communicationId", communication.getId());
    // CHECKSTYLE:ON

    List<Session> presenters = jdbcTemplate.query(sql.toString(), params, new PresenterRowMapper());

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + presenters.size() + "] for communication [" + communication.getId() + "].");
    }
    return presenters;
  }

  /**
   * Loads the statistics for given communication.
   * 
   * @param communication the communication to find the presenters for
   * @return communication with updated statistics
   */
  @Override
  public List<CommunicationStatistics> findCommunicationStatistics(final Communication communication) {

    LOGGER.debug("Loading statistics for communication [" + communication.getId() + "].");

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("communicationId", communication.getId());
    params.addValue("channelId", communication.getChannelId());
    // CHECKSTYLE:ON

    List<CommunicationStatistics> viewingStatistics = jdbcTemplate.query(LOAD_COMMUNICATION_STATISTICS, params,
        new CommunicationViewingStatisticsRowMapper());

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Loaded statistics for communication [" + communication.getId() + "].");
    }
    return viewingStatistics;
  }

  /** {@inheritDoc} */
  @Override
  public List<Communication> findAll(final Long channelId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding all communications for channel [" + channelId + "].");
    }

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(COMMUNICATION_FIELDS_FOR_SELECT);
    sql.append(",");
    sql.append(COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT);
    sql.append("FROM ");
    sql.append("  asset a ");
    sql.append("  JOIN `communication_configuration` cc ON (cc.channel_id = a.channel_id AND cc.communication_id = a.target_id) ");
    sql.append("  JOIN `communication` c ON (cc.communication_id = c.id) ");
    sql.append("WHERE ");
    sql.append("  a.channel_id = :channelId");
    sql.append("  AND (cc.syndication_type = :syndicationType");
    sql.append("       OR cc.syndication_type IS NULL)");
    sql.append("  AND a.type = 'communication'");
    sql.append("  AND a.is_active = 1");
    // CHECKSTYLE:ON

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("channelId", channelId);
    params.put("syndicationType", CommunicationSyndication.SyndicationType.OUT.toString().toLowerCase());

    List<Communication> communications = jdbcTemplate.query(sql.toString(), params, new CommunicationRowMapper(domain));

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found [" + communications.size() + "] communications for channel [" + channelId + "].");
    }

    return communications;
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<Communication> findPaginatedSubscribedChannelsCommunications(final Long userId,
    final MyCommunicationsSearchCriteria searchCriteria) {

    List<Communication> communications;

    switch (searchCriteria.getSortBy()) {

      case SCHEDULEDDATE:
        communications = findSubscribedChannelsCommunicationsSortedByScheduled(userId, searchCriteria);
        break;

      case AVERAGERATING:

      case TOTALVIEWINGS:
        communications = findSubscribedChannelsCommunicationsSortedByRatingOrViewings(userId, searchCriteria);
        break;

      default:
        throw new ApplicationException("Failed to add order by clause. Value [" + searchCriteria.getSortBy()
            + "] not supported.");
    }
    return buildPaginatedList(communications, searchCriteria);
  }

  /**
   * Find a collection of communications that exist in channels user is subscribed to sorted by communication schedule
   * date.
   * <p>
   * Note that the sql query was optimized during performance test and should not be amended.
   * 
   * @param userId the user id
   * @param serachCriteria the search criteria
   * 
   * @return collection of sorted communications communications
   */
  private List<Communication> findSubscribedChannelsCommunicationsSortedByScheduled(final Long userId,
    final MyCommunicationsSearchCriteria searchCriteria) {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(COMMUNICATION_FIELDS_WITH_OVERRIDES_FOR_SELECT);
    sql.append(",");
    sql.append(COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT);
    sql.append(",");
    sql.append("  cs.rating AS cs_rating, ");
    sql.append("  cs.num_views AS cs_viewings ");
    sql.append("FROM ");
    sql.append("  communication c USE INDEX (scheduled) ");
    sql.append("  JOIN asset a ON (c.id = a.target_id AND a.type = 'communication' AND a.is_active = 1) ");
    sql.append("  JOIN channel ch ON (ch.id = a.channel_id) ");

    sql.append("  JOIN subscription s ON (s.user_id = :userId AND s.channel_id = ch.id) ");
    sql.append("  JOIN communication_configuration cc ON (cc.channel_id = a.channel_id AND cc.communication_id = a.target_id) ");
    sql.append("  LEFT JOIN communication_statistics cs ON (cs.channel_id = cc.channel_id AND cs.communication_id = c.id) ");
    sql.append("  LEFT JOIN communication_details_override cdo ON (cdo.channel_id = a.channel_id AND cdo.communication_id = a.target_id) ");

    // other scenarios not supported as per requirements.
    if (searchCriteria.hasRegistered() && searchCriteria.getRegistered()) {
      sql.append("  JOIN communication_registration cr ON (cr.channel_id = a.channel_id "
          + "AND cr.communication_id = a.target_id AND cr.user_id = :userId AND cr.is_active = 1) ");
    }
    sql.append("WHERE ");
    sql.append("  s.is_active = 1 ");
    sql.append("  AND ch.is_active = 1");
    sql.append("  AND c.publish_status = 'published' ");
    sql.append("  AND cc.visibility = 'public' ");
    sql.append("  AND ").append(getApprovedCommunicationsClause());

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("userId", userId);

    addCommunicationStatusFilterClause(sql, searchCriteria, params);
    sql.append(" ORDER BY ");
    sql.append(" scheduled ");
    // CHECKSTYLE:ON
    addSortOrder(sql, searchCriteria);
    addLimitClause(sql, searchCriteria, params);

    List<Communication> communications = jdbcTemplate.query(sql.toString(), params,
        new CommunicationWithStatisticsRowMapper(domain));

    return communications;
  }

  /**
   * Find a collection of communications that exist in channels user is subscribed to, sorted by communication average
   * rating or number of viewings.
   * <p>
   * Note that the sql query was optimized during performance test and should not be amended.
   * 
   * @param userId the user id
   * @param serachCriteria the search criteria
   * 
   * @return collection of sorted communications communications
   */
  private List<Communication> findSubscribedChannelsCommunicationsSortedByRatingOrViewings(final Long userId,
    final MyCommunicationsSearchCriteria searchCriteria) {

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append(COMMUNICATION_FIELDS_WITH_OVERRIDES_FOR_SELECT);
    sql.append(",");
    sql.append(COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT);
    sql.append(",");
    sql.append("  cs.rating AS cs_rating, ");
    sql.append("  cs.num_views AS cs_viewings ");
    sql.append("FROM ");
    sql.append("  subscription s ");
    sql.append("  JOIN channel ch ON (ch.id = s.channel_id) ");
    sql.append("  JOIN asset a ON (a.channel_id = ch.id AND a.type = 'communication' AND a.is_active = 1) ");
    sql.append("  JOIN communication c ON (c.id = a.target_id) ");
    sql.append("  JOIN communication_configuration cc ON (cc.channel_id = a.channel_id AND cc.communication_id = a.target_id) ");
    sql.append("  LEFT JOIN communication_statistics cs ON (cs.channel_id = cc.channel_id AND cs.communication_id = c.id) ");
    sql.append("  LEFT JOIN communication_details_override cdo ON (cdo.channel_id = a.channel_id AND cdo.communication_id = a.target_id) ");

    // other scenarios not supported as per requirements.
    if (searchCriteria.hasRegistered() && searchCriteria.getRegistered()) {
      sql.append("  JOIN communication_registration cr ON (cr.channel_id = a.channel_id "
          + "AND cr.communication_id = a.target_id AND cr.user_id = :userId AND cr.is_active = 1) ");
    }
    sql.append("WHERE ");
    sql.append("  s.user_id = :userId ");
    sql.append("  AND s.is_active = 1 ");
    sql.append("  AND ch.is_active = 1");
    sql.append("  AND c.publish_status = 'published' ");
    sql.append("  AND cc.visibility = 'public' ");
    sql.append("  AND ").append(getApprovedCommunicationsClause());

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("userId", userId);
    // CHECKSTYLE:ON

    addCommunicationStatusFilterClause(sql, searchCriteria, params);

    // set order by to average rating or total viewings. There is always sortBy value passed to this method.
    sql.append(" ORDER BY ");
    switch (searchCriteria.getSortBy()) {
      case AVERAGERATING:
        sql.append(" cs_rating ");
        break;
      case TOTALVIEWINGS:
        sql.append(" cs_viewings ");
        break;
      default:
        throw new ApplicationException("Failed to add order by clause.");
    }

    addSortOrder(sql, searchCriteria);
    addLimitClause(sql, searchCriteria, params);

    List<Communication> communications = jdbcTemplate.query(sql.toString(), params,
        new CommunicationWithStatisticsRowMapper(domain));

    return communications;
  }

  /**
   * Add communication status clause to sql builder and sql params for given search criteria.
   * 
   * If the communication status filter has been provided it is set. Otherwise just the exclusion of deleted and
   * cancelled communications is added.
   * 
   * @param sql sql builder
   * @param criteria search criteria
   * @param params sql params
   */
  private void addCommunicationStatusFilterClause(final StringBuilder sql,
    final MyCommunicationsSearchCriteria criteria, final MapSqlParameterSource params) {

    if (criteria.hasCommunicationsStatusFilter()) {
      List<String> statuses = getConvertedCommunicationStatusFilter(criteria.getCommunicationStatusFilter());

      sql.append("  AND c.status IN (:statuses)");
      params.addValue("statuses", statuses);
    } else {
      sql.append("  AND c.status NOT IN ('deleted', 'cancelled')");
    }
  }

  private List<String> getConvertedCommunicationStatusFilter(final List<CommunicationStatusFilterType> statusFilter) {
    List<String> statuses = new ArrayList<String>();
    for (CommunicationStatusFilterType filterType : statusFilter) {
      statuses.add(filterType.toString().toLowerCase());
    }
    return statuses;
  }

  /**
   * Add sort order by clause depending on setting of search criteria.
   * 
   * @param sql sql builder
   * @param criteria search criteria
   */
  private void addSortOrder(final StringBuilder sql, final MyCommunicationsSearchCriteria criteria) {
    // CHECKSTYLE:OFF
    sql.append(" ").append(criteria.getSortOrder().toString());
    sql.append(" ");
    // CHECKSTYLE:ON
  }

  /**
   * Add limit clause to sql builder and sql params for given search criteria.
   * <p>
   * Note that limit is only added if the page size was provided in the query.
   * 
   * @param sql sql builder
   * @param criteria search criteria
   * @param params sql params
   */
  private void addLimitClause(final StringBuilder sql, final MyCommunicationsSearchCriteria criteria,
    final MapSqlParameterSource params) {

    if (criteria.hasPageSize()) {
      sql.append("LIMIT ");
      sql.append("  :offset, :size");

      final int offset = (criteria.getPageNumber() - 1) * criteria.getPageSize();
      params.addValue(OFFSET_VALUE, offset);
      // add 1 to page size to perform peek check
      params.addValue(SIZE_VALUE, criteria.getPageSize() + 1);
    }
  }

  /**
   * A helper method to build a paginated collection of communications.
   * 
   * Does a check on the peek result.
   * 
   * @param communications a collection of communications
   * @param criteria search criteria
   * 
   * @return result object
   */
  private PaginatedList<Communication> buildPaginatedList(final List<Communication> communications,
    final BaseSearchCriteria criteria) {

    boolean isLastPage = true;

    if (!CollectionUtils.isEmpty(communications) && criteria.hasPageSize()
        && communications.size() > criteria.getPageSize()) {
      isLastPage = false;

      // Remove extra communication added to support peeking for next page
      communications.remove(communications.size() - 1);
    }

    PaginatedList<Communication> result = new PaginatedList<Communication>();
    result.addAll(communications);
    result.setIsFinalPage(isLastPage);
    result.setCurrentPageNumber(criteria.getPageNumber());

    if (criteria.hasPageSize()) {
      result.setPageSize(criteria.getPageSize());
    } else {
      result.setPageSize(communications.size());
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found paginated [" + result.size() + "] communications.");
    }
    return result;
  }

  /** {@inheritDoc} */
  @Override
  public List<Communication> findFeatured(final List<Long> channelIds, final boolean includePrivate) {

    if (CollectionUtils.isEmpty(channelIds)) {
      return new ArrayList<Communication>();
    }

    List<Communication> featuredCommunications = new ArrayList<Communication>();

    for (Long channelId : channelIds) {
      Date scheduledDate = loadFeaturedDate(channelId, includePrivate);

      if (scheduledDate == null) {
        continue;
      }

      StringBuilder sql = new StringBuilder();
      // CHECKSTYLE:OFF
      sql.append("SELECT ");
      sql.append(COMMUNICATION_FIELDS_WITH_OVERRIDES_FOR_SELECT);
      sql.append(",");
      sql.append(COMMUNICATION_CONFIGURATION_FIELDS_FOR_SELECT);
      sql.append("FROM ");
      sql.append("  asset a ");
      sql.append("  JOIN communication c ON ( ");
      sql.append("    c.id = a.target_id ");
      sql.append("    AND c.status NOT IN ('cancelled','deleted') ) ");
      sql.append("  LEFT JOIN `communication_configuration` cc ON (c.id = cc.communication_id AND cc.channel_id = a.channel_id) ");
      sql.append("  LEFT JOIN communication_details_override cdo ON (cdo.channel_id = a.channel_id AND cdo.communication_id = a.target_id) ");
      sql.append("WHERE ");
      sql.append("  a.channel_id = :channelId ");
      sql.append("  AND c.scheduled = :scheduledDate ");

      if (!includePrivate) {
        sql.append(" AND c.publish_status = 'published' ");
        sql.append(" AND cc.visibility != 'private' ");
      }
      sql.append("  AND ").append(getApprovedCommunicationsClause());

      MapSqlParameterSource params = new MapSqlParameterSource();
      params.addValue("channelId", channelId);
      params.addValue("scheduledDate", scheduledDate);
      // CHECKSTYLE:ON

      List<Communication> communications = jdbcTemplate.query(sql.toString(), params,
          new CommunicationRowMapper(domain));

      for (Communication communication : communications) {
        featuredCommunications.add(communication);
      }
    }
    return featuredCommunications;
  }

  private Date loadFeaturedDate(final Long channelId, final boolean includePrivate) {
    StringBuilder sql = new StringBuilder();
    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    sql.append("SELECT ");
    sql.append("  a.channel_id as channelId, ");
    sql.append("  c.scheduled as scheduledDate ");
    sql.append("FROM ");
    sql.append("  asset a ");
    sql.append("  JOIN communication c ON ( ");
    sql.append("    c.id = a.target_id ");
    sql.append("    AND c.status NOT IN ('cancelled','deleted') ) ");
    sql.append("  JOIN communication_configuration cc ON (");
    sql.append("    cc.communication_id = a.target_id ");
    sql.append("    AND cc.channel_id = a.channel_id ) ");
    sql.append("WHERE ");
    sql.append("  a.channel_id = :channelId ");
    sql.append("  AND a.is_active = 1 ");

    if (!includePrivate) {
      sql.append(" AND c.publish_status = 'published' ");
      sql.append(" AND cc.visibility != 'private' ");
    }

    sql.append("  AND ").append(getApprovedCommunicationsClause());
    sql.append("ORDER BY ");
    sql.append("  ABS(UNIX_TIMESTAMP(c.scheduled) - UNIX_TIMESTAMP()) ASC ");
    sql.append("LIMIT 1");

    params.addValue("channelId", channelId);
    // CHECKSTYLE:ON

    Map<String, Object> result = null;

    try {
      result = jdbcTemplate.queryForMap(sql.toString(), params);
    } catch (EmptyResultDataAccessException exception) {

      return null;
    }

    Date scheduledDate = (Date) result.get("scheduledDate");
    return scheduledDate;
  }

  /** {@inheritDoc} */
  @Override
  public void delete(final List<Long> communicationIds) {
    if (communicationIds == null || communicationIds.size() == 0) {
      return;
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleting communications");
    }

    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  communication ");
    sql.append("SET ");
    sql.append("  status = :deleteStatus, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  id IN ( :ids )");
    // CHECKSTYLE:ON

    Map<String, Object> params = new HashMap<String, Object>();
    // CHECKSTYLE:OFF
    params.put("ids", communicationIds);
    // CHECKSTYLE:ON
    params.put("deleteStatus", Status.DELETED.toString().toLowerCase());

    int noOfRows = jdbcTemplate.update(sql.toString(), params);

    if (noOfRows != communicationIds.size()) {
      throw new ApplicationException("Could not delete one or more of [" + communicationIds.size()
          + "] communications.");
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Deleted [" + communicationIds.size() + "] communications");
    }
  }

  /** {@inheritDoc} */
  @Override
  public PaginatedList<Communication> find(final CommunicationsSearchCriteria criteria) {

    List<String> communicationAndChannelIds = criteria.getChanelAndCommunicationsIds();

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding communications [" + communicationAndChannelIds + "].");
    }

    // We add 1 to the size to be able to peak ahead
    final int size = criteria.getPageSize() + 1;
    final int offset = (criteria.getPageNumber() - 1) * criteria.getPageSize();

    MapSqlParameterSource params = new MapSqlParameterSource();

    // CHECKSTYLE:OFF
    params.addValue("channelAndCommunicationIds", communicationAndChannelIds);
    params.addValue(OFFSET_VALUE, offset);
    params.addValue(SIZE_VALUE, size);
    // CHECKSTYLE:ON

    List<Communication> communications = jdbcTemplate.query(COMMUNICATIONS_SELECT_QUERY, params,
        new CommunicationWithStatisticsRowMapper(domain));

    return buildPaginatedList(communications, criteria);
  }

  /** {@inheritDoc} */
  @Override
  public Map<String, Long> findSubscribedChannelsCommunicationsTotals(final Long userId) {

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding subscribed channels communications totals for user [" + userId + "].");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("userId", userId);
    // CHECKSTYLE:ON

    ChannelStatistics queryResult = jdbcTemplate.queryForObject(WEBCAST_TOTALS_FOR_SELECT, params,
        new ChannelStatisticsRowMapper());

    Map<String, Long> communicationsTotals = new HashMap<String, Long>();

    communicationsTotals.put("upcoming", queryResult.getNumberOfUpcomingCommunications());
    communicationsTotals.put("live", queryResult.getNumberOfLiveCommunications());
    communicationsTotals.put("recorded", queryResult.getNumberOfRecordedCommunications());

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found subscribed channels communications totals for user [" + userId + "]: " + communicationsTotals);
    }
    return communicationsTotals;
  }

  @Override
  public Communication update(final Communication communication) {
    LOGGER.debug("Updating communication [" + communication.getId() + "] in DB");

    String sql = "UPDATE " + " `communication` SET " + COMMUNICATION_UPDATE_FIELDS_TO_SET + " WHERE id = :id";

    MapSqlParameterSource params = getCommunicationParamsMapForUpdate(communication);

    int noOfRows = jdbcTemplate.update(sql, params);
    if (noOfRows != 1) {
      throw new ApplicationException("Could not update communication [" + communication.getId() + "]");
    }

    LOGGER.debug("Communication [" + communication.getId() + "] updated.");
    return communication;
  }

  @Override
  public void updateEncodingCount(final Long communicationId) {
    LOGGER.debug("Updating encoding count for communication [" + communicationId + "]");

    String sql = "UPDATE " + " `communication` SET " + ENCODING_COUNT_UPDATE_FIELDS_TO_SET + " WHERE id = :id";

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COMMUNICATION_ID_SQL_PARAMETER, communicationId);
    params.addValue(LAST_UPDATED_SQL_PARAMETER, new Date());
    int noOfRows = jdbcTemplate.update(sql, params);

    if (noOfRows != 1) {
      throw new ApplicationException("Could not update encoding count for communication [" + communicationId + "]");
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Encoding count updated for communication [" + communicationId + "].");
    }
  }

  @Override
  public void updateLivePhoneNumber(final Communication communication) {
    LOGGER.debug("Updating communication [" + communication.getId() + "] with live phone number ["
        + communication.getLivePhoneNumber() + "].");

    String sql = "UPDATE `communication` SET live_phone_number = :livePhoneNumber, last_updated = NOW() WHERE id = :id";

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("id", communication.getId());
    params.addValue("livePhoneNumber", communication.getLivePhoneNumber());
    // CHECKSTYLE:ON

    int noOfRows = jdbcTemplate.update(sql, params);
    if (noOfRows != 1) {
      throw new ApplicationException("Could not update communication [" + communication.getId() + "] live phone number");
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Communication [" + communication.getId() + "] live phone number updated.");
    }
  }

  @Override
  public void cancel(final Long communicationId) {
    LOGGER.debug("Cancelling communication [" + communicationId + "] in DB.");

    String sql = "UPDATE `communication` SET status = 'cancelled', last_updated = NOW() WHERE id = :id";

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("id", communicationId);
    // CHECKSTYLE:ON

    int noOfRows = jdbcTemplate.update(sql, params);
    if (noOfRows != 1) {
      throw new ApplicationException("Could not cancel communication [" + communicationId + "]");
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Communication [" + communicationId + "] cancelled.");
    }
  }

  /** {@inheritDoc} */
  @Override
  public Communication pivot(final Communication communication) {
    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  communication ");
    sql.append("SET ");
    sql.append("  title = :title, ");
    sql.append("  description = :description, ");
    sql.append("  keywords = :keywords, ");
    sql.append("  provider = :provider, ");
    sql.append("  format = :format, ");
    sql.append("  status = :status, ");
    sql.append("  presenter = :presenter, ");
    sql.append("  live_phone_number = :livePhoneNumber, ");
    sql.append("  pin_number = :pinNumber, ");
    sql.append("  live_url = :liveUrl, ");
    sql.append("  thumbnail = :thumbnail, ");
    sql.append("  preview_url = :previewUrl, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  id = :communicationId ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("communicationId", communication.getId());
    params.addValue("title", communication.getTitle());
    params.addValue("description", communication.getDescription());
    params.addValue("keywords", communication.getKeywords());
    params.addValue("presenter", communication.getAuthors());
    params.addValue("livePhoneNumber", communication.getLivePhoneNumber());
    params.addValue("pinNumber", communication.getPinNumber());
    params.addValue("liveUrl", communication.getLiveUrl());
    params.addValue("provider", communication.getProvider().getName());
    params.addValue("format", communication.getFormat().toString().toLowerCase());
    params.addValue("status", communication.getStatus().toString().toLowerCase());
    params.addValue("thumbnail", communication.getThumbnailUrl());
    params.addValue("previewUrl", communication.getPreviewUrl());
    // CHECKSTYLE:ON

    int noOfRows = jdbcTemplate.update(sql.toString(), params);
    if (noOfRows != 1) {
      throw new ApplicationException("Could not update communication [" + communication.getId() + "]");
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Communication [" + communication.getId() + "] updated.");
    }

    return communication;
  }

  /** {@inheritDoc} */
  @Override
  public void removeVotes(final Long communicationId) {
    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  communication_asset ");
    sql.append("SET ");
    sql.append("  is_active = 0, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  communication_id = :communicationId ");
    sql.append("  AND type = 'vote' ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("communicationId", communicationId);
    // CHECKSTYLE:ON

    jdbcTemplate.update(sql.toString(), params);
  }

  /** {@inheritDoc} */
  @Override
  public void removeMediazoneAssets(final Long communicationId) {
    StringBuilder sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  communication_asset ");
    sql.append("SET ");
    sql.append("  is_active = 0, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  communication_id = :communicationId ");
    sql.append("  AND is_active = 1 ");
    sql.append("  AND (type = 'mediazone_webcast' ");
    sql.append("  OR type = 'mediazone_config') ");
    // CHECKSTYLE:ON

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("communicationId", communicationId);
    // CHECKSTYLE:ON

    jdbcTemplate.update(sql.toString(), params);
  }

  @Override
  public void restoreMediazoneAssets(final Long communicationId) {
    StringBuilder sql = new StringBuilder();

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("communicationId", communicationId);
    // CHECKSTYLE:ON

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  communication_asset ");
    sql.append("SET ");
    sql.append("  is_active = 1, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  communication_id = :communicationId ");
    sql.append("  AND type = 'mediazone_webcast' ");
    sql.append(" ORDER BY ");
    sql.append(" last_updated DESC ");
    sql.append("LIMIT 1");
    // CHECKSTYLE:ON
    jdbcTemplate.update(sql.toString(), params);

    sql = new StringBuilder();

    // CHECKSTYLE:OFF
    sql.append("UPDATE ");
    sql.append("  communication_asset ");
    sql.append("SET ");
    sql.append("  is_active = 1, ");
    sql.append("  last_updated = NOW() ");
    sql.append("WHERE ");
    sql.append("  communication_id = :communicationId ");
    sql.append("  AND type = 'mediazone_config' ");
    sql.append(" ORDER BY ");
    sql.append(" last_updated DESC ");
    sql.append("LIMIT 1");
    // CHECKSTYLE:ON

    jdbcTemplate.update(sql.toString(), params);
  }

  @Override
  public void updatePublishStatus(final List<Long> communicationIds, final PublishStatus publishStatus) {
    LOGGER.debug("Updating publishing status of [" + communicationIds.size() + "] communications. New value ["
        + publishStatus + "]");

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue("ids", communicationIds);
    params.addValue("publish_status", publishStatus.toString().toLowerCase());

    int noOfRows = jdbcTemplate.update(PUBLISH_SCHEDULED_QUERY, params);
    LOGGER.debug("[" + noOfRows + "] communications were updated to [" + publishStatus + "].");
  }

  @Override
  public List<Rendition> findRenditionAssets(final Long communicationId) {
    LOGGER.debug("Loading renditions for communication [" + communicationId + "].");

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("id", communicationId);
    // CHECKSTYLE:ON

    List<Rendition> renditions = jdbcTemplate.query(LOAD_COMMUNICATION_RENDITIONS, params, new RenditionRowMapper());

    LOGGER.debug("Found [" + renditions.size() + "] for communication [" + communicationId + "].");
    return renditions;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Rendition}.
   */
  protected static class RenditionRowMapper implements RowMapper<Rendition> {
    /**
     * {@inheritDoc}
     */
    @Override
    public Rendition mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
      Rendition rendition = new Rendition();
      rendition.setId(resultSet.getLong("id"));
      rendition.setIsActive(resultSet.getBoolean("is_active"));
      rendition.setBitrate(resultSet.getLong("bitrate"));
      rendition.setCodecs(resultSet.getString("codecs"));
      rendition.setWidth(resultSet.getLong("width"));
      rendition.setUrl(resultSet.getString("url"));
      rendition.setContainer(resultSet.getString("container"));
      return rendition;
    }
  }

  /** {@inheritDoc} */
  @Override
  public Communication create(final Communication communication) {
    LOGGER.debug("Creating communication in DB.");

    String sql = "INSERT INTO `communication` SET " + COMMUNICATION_CREATE_FIELDS_TO_SET;

    final KeyHolder generatedKeyHolder = new GeneratedKeyHolder();

    MapSqlParameterSource params = getCommunicationParamsMapForCreate(communication);

    jdbcTemplate.update(sql, params, generatedKeyHolder);

    Long communicationId = generatedKeyHolder.getKey().longValue();
    communication.setId(communicationId);

    LOGGER.debug("Communication [" + communicationId + "] in channel [" + communication.getMasterChannelId()
        + "] successfully created in DB.");
    return communication;
  }

  /**
   * Creates channel asset record.
   * 
   * @param communication The supplied communication.
   */
  @Override
  public void createChannelAsset(final Communication communication) {
    LOGGER.debug("Creating channel asset for communication [" + communication.getId() + "].");

    String sql = "INSERT INTO `asset` SET " + CHANNEL_ASSET_FILEDS_TO_SET;

    jdbcTemplate.update(sql, getChannelAssetParamsMap(communication));
  }

  @Override
  public boolean isOverlapping(final Communication communication) {
    LOGGER.info("Checking if communication is overlapping in channel [" + communication.getMasterChannelId() + "].");

    Long masterChannelId = communication.getMasterChannelId();
    Date startTime = communication.getScheduledDateTime();
    Date endTime = new Date(startTime.getTime() + communication.getDuration());

    String sql = IS_OVERLAPPING_COMMUNICATION;

    if (communication.getId() != null) {
      sql = sql.concat(" AND id <> :communicationId ");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("masterChannelId", masterChannelId);
    params.addValue("startTime", startTime.getTime() / 1000);
    params.addValue("endTime", endTime.getTime() / 1000);
    if (communication.getId() != null) {
      params.addValue("communicationId", communication.getId());
    }
    // CHECKSTYLE:ON

    long communicationsCount = 0L;
    try {
      communicationsCount = queryForLong(jdbcTemplate, sql.toString(), params);
    } catch (EmptyResultDataAccessException e) {
      return false;
    }
    return communicationsCount > 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isActiveAsset(final Communication communication) {
    LOGGER.debug("Checking if communication [" + communication.getId() + "] is an active asset of channel ["
        + communication.getChannelId() + "].");

    MapSqlParameterSource params = new MapSqlParameterSource();
    // CHECKSTYLE:OFF
    params.addValue("channelId", communication.getChannelId());
    params.addValue("communicationId", communication.getId());
    // CHECKSTYLE:ON

    long communicationsCount = 0L;
    try {
      communicationsCount = queryForLong(jdbcTemplate, IS_COMMUNICATION_ACTIVE_ASSET, params);
    } catch (EmptyResultDataAccessException e) {
      return false;
    }
    return communicationsCount == 1;
  }

  /**
   * Retrieves a populated parameter map for creating a communication.
   * 
   * @param communication the communication to populate the map with
   * @return the populated map.
   */
  private MapSqlParameterSource getCommunicationParamsMapForCreate(final Communication communication) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(TITLE_SQL_PARAMETER, communication.getTitle());
    params.addValue(DESCRIPTION_SQL_PARAMETER, communication.getDescription());
    params.addValue(KEYWORDS_SQL_PARAMETER, communication.getKeywords());
    params.addValue(PRESENTER_SQL_PARAMETER, communication.getAuthors());
    params.addValue(DURATION_SQL_PARAMETER, communication.getDuration());
    params.addValue(SCHEDULED_SQL_PARAMETER, communication.getScheduledDateTime());
    params.addValue(TIMEZONE_SQL_PARAMETER, communication.getTimeZone());
    params.addValue(STATUS_SQL_PARAMETER, Communication.Status.UPCOMING.name());
    params.addValue(PUBLISH_STATUS_SQL_PARAMETER, Communication.PublishStatus.PUBLISHED.name());
    params.addValue(CREATED_SQL_PARAMETER, new Date());
    params.addValue(LAST_UPDATED_SQL_PARAMETER, new Date());
    params.addValue(MASTER_CHANNEL_ID_SQL_PARAMETER, communication.getMasterChannelId());
    params.addValue(PROVIDER_SQL_PARAMETER, communication.getProvider().getName());
    params.addValue(FORMAT_SQL_PARAMETER, communication.getFormat().name());
    params.addValue(BOOKING_DURATION_SQL_PARAMETER, communication.getDuration());
    params.addValue(PIN_NUMBER_SQL_PARAMETER, communication.getPinNumber());
    params.addValue(LIVE_URL_SQL_PARAMETER, communication.getLiveUrl());
    params.addValue(LIVE_PHONE_NUMBER_SQL_PARAMETER, communication.getLivePhoneNumber());
    params.addValue(THUMBNAIL_URL_SQL_PARAMETER, stripUrl(communication.getThumbnailUrl()));
    params.addValue(PREVIEW_URL_SQL_PARAMETER, stripUrl(communication.getPreviewUrl()));
    return params;
  }

  /**
   * Retrieves a populated parameter map for updating a communication.
   * 
   * @param communication the communication to populate the map with
   * @return the populated map.
   */
  private MapSqlParameterSource getCommunicationParamsMapForUpdate(final Communication communication) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(COMMUNICATION_ID_SQL_PARAMETER, communication.getId());
    params.addValue(TITLE_SQL_PARAMETER, communication.getTitle());
    params.addValue(DESCRIPTION_SQL_PARAMETER, communication.getDescription());
    params.addValue(KEYWORDS_SQL_PARAMETER, communication.getKeywords());
    params.addValue(PRESENTER_SQL_PARAMETER, communication.getAuthors());
    params.addValue(DURATION_SQL_PARAMETER, communication.getDuration());
    params.addValue(SCHEDULED_SQL_PARAMETER, communication.getScheduledDateTime());
    params.addValue(TIMEZONE_SQL_PARAMETER, communication.getTimeZone());
    params.addValue(STATUS_SQL_PARAMETER, communication.getStatus().toString().toLowerCase());
    params.addValue(PUBLISH_STATUS_SQL_PARAMETER, communication.getPublishStatus().toString().toLowerCase());
    params.addValue(LAST_UPDATED_SQL_PARAMETER, new Date());
    params.addValue(THUMBNAIL_URL_SQL_PARAMETER, stripUrl(communication.getThumbnailUrl()));
    params.addValue(PREVIEW_URL_SQL_PARAMETER, stripUrl(communication.getPreviewUrl()));
    params.addValue(BOOKING_DURATION_SQL_PARAMETER, communication.getBookingDuration());
    return params;
  }

  /**
   * Retrieves a populated parameter map for creating a channel asset.
   * 
   * @param cat the communication to populate the map with
   * @return the populated map.
   */
  private MapSqlParameterSource getChannelAssetParamsMap(final Communication comm) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(CHANNEL_ID_SQL_PARAMETER, comm.getChannelId());
    params.addValue(TARGET_ID_SQL_PARAMETER, comm.getId());
    params.addValue(TYPE_SQL_PARAMETER, "communication");
    params.addValue(CREATED_SQL_PARAMETER, new Date());
    params.addValue(LAST_UPDATED_SQL_PARAMETER, new Date());
    return params;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Session}.
   */
  private static class PresenterRowMapper implements RowMapper<Session> {

    /** {@inheritDoc} */
    @Override
    public Session mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {

      // map the session
      Session session = new Session(resultSet.getString("session_id"));
      return session;
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link Communication}.
   */
  protected static class CommunicationWithStatisticsRowMapper extends CommunicationRowMapper implements RowMapper<Communication> {
    /**
     * Constructor.
     * 
     * @param domain that can be used to generate urls
     */
    public CommunicationWithStatisticsRowMapper(final String domain) {
      super(domain);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Communication mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
      Communication communication = super.mapRow(resultSet, rowNum);

      String communicationRating = resultSet.getString("cs_rating");
      String communicationViewings = resultSet.getString("cs_viewings");

      if (communicationRating != null || communicationViewings != null) {
        CommunicationStatistics viewingStatistics = new CommunicationStatistics(communication.getId());
        if (communicationRating != null) {
          viewingStatistics.setAverageRating(Float.parseFloat(communicationRating));
        }
        if (communicationViewings != null) {
          viewingStatistics.setNumberOfViewings(Integer.parseInt(communicationViewings));
        }
        communication.setCommunicationStatistics(viewingStatistics);
      }
      return communication;
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link CommunicationStatistics}.
   */
  protected static class CommunicationViewingStatisticsRowMapper implements RowMapper<CommunicationStatistics> {
    /**
     * {@inheritDoc}
     */
    @Override
    public CommunicationStatistics mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
      CommunicationStatistics communicationViewingStatistics = new CommunicationStatistics();
      communicationViewingStatistics.setNumberOfRatings(resultSet.getInt("num_ratings"));
      communicationViewingStatistics.setAverageRating(resultSet.getFloat("cs_rating"));
      communicationViewingStatistics.setNumberOfViewings(resultSet.getInt("cs_viewings"));
      communicationViewingStatistics.setTotalViewingDuration(resultSet.getLong("viewed_for"));
      return communicationViewingStatistics;
    }
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link ChannelStatistics}.
   */
  protected static class ChannelStatisticsRowMapper implements RowMapper<ChannelStatistics> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ChannelStatistics mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
      ChannelStatistics channelStatistics = new ChannelStatistics();
      channelStatistics.setNumberOfUpcomingCommunications(resultSet.getLong("upcoming_count"));
      channelStatistics.setNumberOfLiveCommunications(resultSet.getLong("live_count"));
      channelStatistics.setNumberOfRecordedCommunications(resultSet.getLong("recorded_count"));
      return channelStatistics;
    }
  }
}