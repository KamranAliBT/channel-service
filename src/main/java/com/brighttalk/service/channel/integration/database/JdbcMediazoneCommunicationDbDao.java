/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: JdbcCommunicationDbDao.java 89900 2015-02-13 15:49:13Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.brighttalk.service.channel.business.domain.MediazoneConfig;
import com.brighttalk.service.channel.business.error.CommunicationNotFoundException;

/**
 * Spring JDBC Database DAO implementation of {@link CommunicationDbDao} which uses JDBC via Spring's SimpleJDBCTemplate
 * to access the Database.
 */
@Repository
@Primary
public class JdbcMediazoneCommunicationDbDao extends AbstractJdbcCommunicationDbDao implements MediazoneCommunicationDbDao {

  /** class logger. */
  public static final Logger logger = Logger.getLogger(JdbcMediazoneCommunicationDbDao.class);

  /** id column name represenation */
  private static final String ID = "id";
  /** mediazone config url. */
  private static final String MEDIAZONE_CONFIG_URL = "mediazoneConfigUrl";
  /** mediazone id. */
  private static final String MEDIAZONE_ID = "mediazoneId";

  // CHECKSTYLE:OFF
  // @formatter:off
  private static final String LOAD_MEDIAZONE_ASSET ="SELECT"
      + " c.id AS id, "
      + " a.target_id AS " + MEDIAZONE_ID + ", "
      + " b.target_id AS " + MEDIAZONE_CONFIG_URL + ", "
      + " a.is_active, "
      + " a.last_updated "
      + "FROM "
      + "  communication c "
      + "  LEFT JOIN communication_asset a ON (c.id = a.communication_id AND a.type = '" + MediazoneConfig.ASSET_MEDIAZONE_WEBCAST + "' AND a.is_active = 1) "
      + "  LEFT JOIN communication_asset b ON (c.id = b.communication_id AND b.type = '" + MediazoneConfig.ASSET_MEDIAZONE_CONFIG + "' AND b.is_active = 1) "
      + "WHERE "
      + "  c.id = :id ";
  // @formatter:on  
  // CHECKSTYLE:ON

  /** Logger for this class */
  protected static final Logger LOGGER = Logger.getLogger(JdbcMediazoneCommunicationDbDao.class);

  /** JDBC template used to access the database. */
  protected NamedParameterJdbcTemplate jdbcTemplate;

  /**
   * Sets the DataSource and builds the jdbc templates for channelDataSource.
   * 
   * @param dataSource defined in Spring Config
   */
  @Autowired
  public void setDataSource(@Qualifier("channelDataSource") final DataSource dataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  /** {@inheritDoc} */
  @Override
  public MediazoneConfig findMediazoneConfig(final Long communicationId) throws CommunicationNotFoundException {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Finding mediazone config for communication [" + communicationId + "]");
    }

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(ID, communicationId);

    MediazoneConfig mediazoneConfig = null;

    try {
      mediazoneConfig = jdbcTemplate.queryForObject(LOAD_MEDIAZONE_ASSET, params, new MediazoneConfigRowMapper());
    } catch (EmptyResultDataAccessException e) {
      logger.info("No mediazone config found for communication [" + communicationId + "]");
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Found mediazoneConfig [" + mediazoneConfig + "]");
    }
    return mediazoneConfig;
  }

  /**
   * Implementation of Spring {@link RowMapper} which maps result set to an {@link MediazoneConfig}.
   */
  private static class MediazoneConfigRowMapper implements RowMapper<MediazoneConfig> {

    /** {@inheritDoc} */
    @Override
    public MediazoneConfig mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
      MediazoneConfig mediazoneConfig = null;
      if (StringUtils.isNotBlank(resultSet.getString(MEDIAZONE_CONFIG_URL))) {
        mediazoneConfig = new MediazoneConfig();
        mediazoneConfig.setId(resultSet.getLong(ID));
        mediazoneConfig.setTargetMediazoneConfigUrl(resultSet.getString(MEDIAZONE_CONFIG_URL));
        mediazoneConfig.setTargetMediazoneId(resultSet.getString(MEDIAZONE_ID));
        mediazoneConfig.setIsActive(resultSet.getBoolean("is_active"));
        mediazoneConfig.setLastUpdated(resultSet.getTimestamp("last_updated"));
      }
      return mediazoneConfig;
    }
  }

}