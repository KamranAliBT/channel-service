/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2013.
 * All Rights Reserved.
 * $Id: codetemplates.xml 41756 2012-02-02 11:15:06Z rgreen $
 * ****************************************************************************
 */
/**
 * This package contains the Integration interface and classes to communicate with the Live Event Service.
 */
package com.brighttalk.service.channel.integration.les;