/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: SummitServiceDao.java 91249 2015-03-06 16:38:40Z ssarraj $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.summit;

import java.util.List;

import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.summit.dto.SummitDto;

/**
 * Interface to define the required methods for the interactions with the Summit Service.
 */
public interface SummitServiceDao {

  /**
   * Inform the summit service that a communication has changed.
   * 
   * Either time slot is different (different scheduled time or duration or timezone)
   * 
   * @param communication The rescheduled communication.
   */
  void inform(Communication communication);

  /**
   * Delete a communication from summit service when the webcast has been cancelled.
   * 
   * @param communicationId of the communication to be deleted
   */
  void delete(Long communicationId);

  /**
   * Find the summit details for all the supplied summit Ids.
   * 
   * @param summitIds List of summit Ids to find.
   * @return List of found summits.
   */
  List<SummitDto> findPublicSummitsByIds(List<Long> summitIds);
}