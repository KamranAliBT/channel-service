/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 62293 2013-03-22 16:29:50Z amajewski $
 * ****************************************************************************
 */
/**
 * This package contains the DTO Objects used when interacting with the live state service.
 */
package com.brighttalk.service.channel.integration.livestate.dto;