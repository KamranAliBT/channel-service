/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: EmailRequestConverter.java 88651 2015-01-22 11:55:16Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.integration.email.dto.converter;

import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.domain.channel.Channel;
import com.brighttalk.service.channel.business.domain.communication.Communication;
import com.brighttalk.service.channel.integration.email.dto.ChannelDto;
import com.brighttalk.service.channel.integration.email.dto.CommunicationDto;
import com.brighttalk.service.channel.integration.email.dto.EmailDto;
import com.brighttalk.service.channel.integration.email.dto.EmailRequestDto;

/**
 * Email Request Converter used to create DTO for communicating with the email service.
 */
@Component
public class EmailRequestConverter {

  /**
   * Pattern for a type of email. The {emailGroup} tag needs to be replaced by the channel's pending type email group.
   */
  protected static final String EMAIL_TYPE_PATTERN = "channel_{emailGroup}_request";

  /** Name of the communication scheduled email. */
  protected static final String COMMUNICATION_SCHEDULED_EMAIL_TYPE = "communication_scheduled";

  /** Name of the communication rescheduled email. */
  protected static final String COMMUNICATION_RESCHEDULED_EMAIL_TYPE = "communication_rescheduled";

  /** Name of the communication cancelled email. */
  protected static final String COMMUNICATION_CANCELLED_EMAIL_TYPE = "communication_cancelled";

  /** Name of the communication missed email. */
  protected static final String COMMUNICATION_MISSED_YOU_EMAIL_TYPE = "communication_missed_you";

  /** Name of the communication recording published email type. */
  protected static final String COMMUNICATION_RECORDING_PUBLISHED_EMAIL_TYPE = "communication_recording_published";

  /** Name of the video upload complete email. */
  protected static final String VIDEO_UPLOAD_COMPLETE = "self_service_video_complete";

  /** Name of the video upload error email. */
  protected static final String VIDEO_UPLOAD_ERROR = "self_service_video_error";

  /**
   * Convert the given channel into email request dto.
   * 
   * @param channel to be converted
   * 
   * @return DTO object
   */
  public EmailRequestDto convert(final Channel channel) {

    EmailDto emailDto = new EmailDto();
    emailDto.setType(EMAIL_TYPE_PATTERN.replace("{emailGroup}", channel.getPendingType().getEmailGroup()));

    ChannelDto channelDto = new ChannelDto();
    channelDto.setId(channel.getId());
    channelDto.setTitle(channel.getTitle());
    channelDto.setOwnerUserID(channel.getOwnerUserId());
    channelDto.setType(channel.getType().getId().toString());
    emailDto.setChannel(channelDto);

    EmailRequestDto requestDto = new EmailRequestDto();
    requestDto.setEmail(emailDto);

    return requestDto;
  }

  /**
   * Convert the given communication into email request dto for communication create.
   * 
   * @param communication to be converted
   * 
   * @return EmailRequestDto object
   */
  public EmailRequestDto convertForCreate(final Communication communication) {

    return this.convert(communication, COMMUNICATION_SCHEDULED_EMAIL_TYPE);
  }

  /**
   * Convert the given communication into email request dto for communication update.
   * 
   * @param communication to be converted
   * 
   * @return EmailRequestDto object
   */
  public EmailRequestDto convertForUpdate(final Communication communication) {

    return this.convert(communication, COMMUNICATION_RESCHEDULED_EMAIL_TYPE);
  }

  /**
   * Convert the given communication into email request dto for communication cancelled.
   * 
   * @param communication to be converted
   * 
   * @return EmailRequestDto object
   */
  public EmailRequestDto convertForCancel(final Communication communication) {

    return this.convert(communication, COMMUNICATION_CANCELLED_EMAIL_TYPE);
  }

  /**
   * Convert the given communication into email request dto for communication missed you.
   * 
   * @param communication to be converted
   * 
   * @return EmailRequestDto object
   */
  public EmailRequestDto convertForMissedYou(final Communication communication) {

    return this.convert(communication, COMMUNICATION_MISSED_YOU_EMAIL_TYPE);
  }

  /**
   * Convert the given communication into email request dto for communication recording published.
   * 
   * @param communication to be converted
   * 
   * @return EmailRequestDto object
   */
  public EmailRequestDto convertForRecordingPublished(final Communication communication) {

    return this.convert(communication, COMMUNICATION_RECORDING_PUBLISHED_EMAIL_TYPE);
  }

  /**
   * Convert the given communication into email request dto for video upload complete.
   * 
   * @param communication to be converted
   * 
   * @return EmailRequestDto object
   */
  public EmailRequestDto convertForVideoUploadComplete(final Communication communication) {

    return this.convert(communication, VIDEO_UPLOAD_COMPLETE);
  }

  /**
   * Convert the given communication into email request dto for video upload complete.
   * 
   * @param communication to be converted
   * 
   * @return EmailRequestDto object
   */
  public EmailRequestDto convertForVideoUploadError(final Communication communication) {

    return this.convert(communication, VIDEO_UPLOAD_ERROR);
  }

  /**
   * Convert the commnuication object into an emailrequestdto.
   * 
   * @param communication to be converted
   * @param emailType for create or update
   * @return the email request dto object
   */
  private EmailRequestDto convert(final Communication communication, final String emailType) {

    CommunicationDto communicationDto = setCommunicationDto(communication, emailType);

    EmailDto emailDto = new EmailDto();
    emailDto.setType(emailType);
    emailDto.setCommunication(communicationDto);

    EmailRequestDto requestDto = new EmailRequestDto();
    requestDto.setEmail(emailDto);

    return requestDto;
  }

  /**
   * Set the communicationdto object based on the given communication and email type.
   * 
   * @param communication to be converted
   * @param emailType needed to define which communication property is needed.
   * @return communication dto object.
   */
  private CommunicationDto setCommunicationDto(final Communication communication, final String emailType) {
    CommunicationDto communicationDto = new CommunicationDto();
    communicationDto.setId(communication.getId());
    communicationDto.setChannel(new ChannelDto(communication.getChannelId()));

    switch (emailType) {
      case COMMUNICATION_MISSED_YOU_EMAIL_TYPE:
      case COMMUNICATION_RECORDING_PUBLISHED_EMAIL_TYPE:
        return communicationDto;
      default:
        communicationDto.setTitle(communication.getTitle());
        communicationDto.setStatus(communication.getStatus().name().toLowerCase());
        communicationDto.setScheduled(communication.getScheduledDateTime().getTime() / 1000);
        communicationDto.setDuration(communication.getDuration());
        communicationDto.setTimezone(communication.getTimeZone());
        communicationDto.setPresenter(communication.getAuthors());
        communicationDto.setCustomUrl(communication.getConfiguration().getCustomUrl());
        return communicationDto;
    }

  }
}
