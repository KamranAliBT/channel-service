/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: ScheduledPublicationQueuer.java 94590 2015-05-06 12:05:06Z ikerbib $
 * ****************************************************************************
 */
package com.brighttalk.service.channel.cron;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.brighttalk.service.channel.business.service.communication.ScheduledPublicationUpdateService;

/**
 * Represents a queuer responsible for triggering scheduled publication and unpublication of communications.
 */
@Component
public class ScheduledPublicationQueuer {

  /** Logger for this class */
  private static final Logger LOGGER = Logger.getLogger(ScheduledPublicationQueuer.class);

  @Value("${cron.webcast.published.enable}")
  private boolean enabled;

  @Autowired
  private ScheduledPublicationUpdateService scheduledPublicationUpdateService;

  /**
   * Modify communication publish status based on timed events.
   * 
   * The method is exposed vi jmx so that we can control when we want to lookup for recently updated channels.
   * 
   * The method is using spring scheduler with cron expression to run the process periodically (every minute).
   */
  @Scheduled(cron = "${cron.webcast.published.cron}")
  public void modifyPublicationStatus() {

    if (isEnabled()) {

      LOGGER.debug("START Log - publish/unpublish scheduled communications [" + new Date() + "]. ");
      scheduledPublicationUpdateService.performScheduledPublicationStatusUpdate();
      LOGGER.debug("FINISH Log - publish/unpublish scheduled communications [" + new Date() + "].");

    } else {

      LOGGER.debug("Publish scheduled communications feature is currently OFF.");
    }
  }

  /**
   * This is purely for a purpose of making it visible through jmx.
   * 
   * @return True when watch is enabled
   */
  public boolean isEnabled() {
    return enabled;
  }

  /**
   * 
   * @param enabled true when watcher is enabled
   */
  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
  }
}