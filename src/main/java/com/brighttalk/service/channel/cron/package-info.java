/**
 * ****************************************************************************
 * Copyright BrightTALK Ltd, 2012.
 * All Rights Reserved.
 * $Id: package-info.java 70933 2013-11-15 10:33:42Z msmith $
 * ****************************************************************************
 */
/**
 * This package contains the cron files to perform scheduled actions on channel service.
 */
package com.brighttalk.service.channel.cron;